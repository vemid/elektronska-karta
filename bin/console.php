<?php

define('APP_PATH', dirname(__DIR__) . '/');

require_once APP_PATH . 'vendor/autoload.php';
require_once APP_PATH . 'src/base/autoload.php';
require_once APP_PATH . 'src/warehouseOrder/autoload.php';
require_once APP_PATH . 'src/warehouseOrderMp/autoload.php';
require_once APP_PATH . 'src/warehouseOrderVp/autoload.php';

$cliApp = new \Vemid\Application\Cli();
$cliApp->initialize();

$app = new \Vemid\Console\Application('1.0.0');
$app->run();
