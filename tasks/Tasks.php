<?php

use Crunz\Schedule;

include_once __DIR__ . '/../app/tools/ToolHelper.php';

$console = realpath(APP_PATH . '/bin/console.php');

$schedule = new Schedule();

$schedule
    ->run("php $console", ['userNotificationMessages:reactivate-deactivated-messages'])
    ->description('Re activate user deactivated messages')
    ->minute(0)
    ->hour(6);

$schedule
    ->run("php $console", ['userNotificationMessages:reactivate-postponed-messages'])
    ->description('Re activate user postponed messages')
    ->minute(15)
    ->hour(6);

$schedule
    ->run("php $console", ['import:classifications'])
    ->description('Import classifications from MIS')
    ->every('hour', 2);

$schedule
    ->run("php $console", ['import:attributes'])
    ->description('Import attributes from MIS')
    ->every('hour', 2)
    ->between('06:30', '18:30');

$schedule
    ->run("php $console", ['import:codes'])
    ->description('Import codes from MIS')
    ->every('hour', 2)
    ->between('06:30', '18:30');

$schedule
    ->run("php $console", ['import:priceLists'])
    ->description('Import price lists from MIS')
    ->minute(15)
    ->hour(1);

$schedule
    ->run("php $console", ['import:warrant-shops'])
    ->description('Import warrant lists from MIS')
    ->minute(05)
    ->hour(22);

$schedule
    ->run("php $console", ['import:warrant-shops'])
    ->description('Import warrant lists from MIS')
    ->every('hour', 1)
    ->between('07:30', '11:30');

$schedule
    ->run("php $console", ['import:production-orders'])
    ->description('Import warrant lists from MIS')
    ->every('hour', 1)
    ->between('05:30', '19:30');

$schedule
    ->run("php $console", ['operatingListItem:finish'])
    ->description('Set finished operating list items')
    ->everyMinute();

$schedule
    ->run("php $console", ['operatingList:finish'])
    ->description('Set finished operating lists')
    ->everyMinute();

$schedule
    ->run("php $console", ['report:weekly-payment-report'])
    ->description('Send report')
    ->mondays()
    ->minute(10)
    ->hour(12);

$schedule
    ->run("php $console", ['report:dealership-payment-report'])
    ->description('Send report')
    ->minute(00)
    ->hour(12);

$schedule
    ->run("php $console", ['product:import-classifications-and-sizes'])
    ->description('Import classifications, attributes and barcode into MIS')
    ->everyMinute();

//$schedule
//    ->run("php $console", ['checkins:import-checkins'])
//    ->description('Import checkins of production workers')
//    ->every('minute',2)
//    ->between('07:00','09:30');

$schedule
    ->run("php $console", ['checkins:import-checkins'])
    ->description('Import checkins of production workers')
    ->every('hour',1)
    ->between('18:00','19:00');

$schedule
    ->run("php $console", ['checkins:import-checkins'])
    ->description('Import checkins of production workers')
    ->every('hour',1)
    ->between('13:00','16:00');

$schedule
    ->run("php $console", ['checkins:import-checkins'])
    ->description('Import checkins of production workers')
    ->every('hour',1)
    ->between('06:00','09:00');

$schedule
    ->run("php $console", ['email:outbound-email-sender'])
    ->description('Sends un-sent outbound emails')
    ->every('minute', 1)
    ->between('07:00','20:00');

$schedule
    ->run("php $console", ['product:import-produce-prices'])
    ->description('Import prices into MIS')
    ->everyMinute();

$schedule
    ->run("php $console", ['product:import-merchandise-prices'])
    ->description('Import prices into MIS')
    ->everyMinute();

$schedule
    ->run("php $console", ['report:payment-report-by-sector-and-worker'])
    ->description('Reports for operations')
    ->minute(15)
    ->hour(22);

//$schedule
//    ->run("php $console", ['import:failures'])
//    ->description('Import failure type')
//    ->every('hour',2)
//    ->between('08:00','22:00');

//$schedule
//    ->run("php $console", ['import:failure-values'])
//    ->description('Import failure values')
//    ->every('hour',2)
//    ->between('08:00','22:00');
//
//$schedule
//    ->run("php $console", ['storned:failures'])
//    ->description('Delete storned failure values')
//    ->every('hour',2)
//    ->between('08:00','22:00');

$schedule
    ->run("php $console", ['product:print-label-result'])
    ->description('Set printed label lists')
    ->everyMinute();

$schedule
    ->run("php $console", ['report:checkin-workers'])
    ->description('Send report')
    ->dayOfMonth(03)
    ->minute(00)
    ->hour(9);

$schedule
    ->run("php $console", ['report:failure-report'])
    ->description('Send report')
    ->minute(00)
    ->hour(9);

$schedule
    ->run("php $console", ['import:invoices'])
    ->description('Import invoices')
    ->every('minute',15)
    ->between('08:00','18:00');

$schedule
    ->run("php $console", ['import:invoice-items'])
    ->description('Import invoices')
    ->every('minute',15)
    ->between('08:00','18:00');

//$schedule
//    ->run("php $console", ['warehouse-order:mis-sync'])
//    ->every('minute',15)
//    ->between('08:00','18:00');

$schedule
    ->run("php $console", ['report:production-order-mismatch'])
    ->description('Send report')
    ->mondays()
    ->minute(15)
    ->hour(16);

$schedule
    ->run("php $console", ['warehouse-order:mis-sync'])
    ->description('Send wrehouse orders')
    ->every('seconds',10)
    ->between('03:00','23:00');

$schedule
    ->run("php $console", ['warehouse-order:mis-sync-group'])
    ->description('Send wrehouse orders')
    ->every('seconds',30)
    ->between('03:00','23:00');

$schedule
    ->run("php $console", ['order:finish-order'])
    ->description('Finish order')
    ->minute(50)
    ->hour(23);

$schedule
    ->run("php $console", ['warehouse-order:group-status'])
    ->description('Send wrehouse orders')
    ->every('seconds',10);

$schedule
    ->run("php $console", ['efectus:triger-efectus'])
    ->description('Triger Efectus')
    ->mondays()
    ->minute(00)
    ->hour(8);

$schedule
    ->run("php $console", ['report:packed-quantity'])
    ->description('Send report')
    ->minute(00)
    ->hour(17);

return $schedule;