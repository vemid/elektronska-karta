<?php

class getPrenos
{

    /**
     * @var \DateTime $datumOd
     */
    protected $datumOd = null;

    /**
     * @var \DateTime $datumDo
     */
    protected $datumDo = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return \DateTime
     */
    public function getDatumOd()
    {
      if ($this->datumOd == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->datumOd);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $datumOd
     * @return getPrenos
     */
    public function setDatumOd(\DateTime $datumOd = null)
    {
      if ($datumOd == null) {
       $this->datumOd = null;
      } else {
        $this->datumOd = $datumOd->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatumDo()
    {
      if ($this->datumDo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->datumDo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $datumDo
     * @return getPrenos
     */
    public function setDatumDo(\DateTime $datumDo = null)
    {
      if ($datumDo == null) {
       $this->datumDo = null;
      } else {
        $this->datumDo = $datumDo->format(\DateTime::ATOM);
      }
      return $this;
    }

}
