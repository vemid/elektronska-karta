<?php

class prenosResponseCustom extends docResponseCustom2
{

    /**
     * @var prenos[] $prenosi
     */
    protected $prenosi = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return prenos[]
     */
    public function getPrenosi()
    {
      return $this->prenosi;
    }

    /**
     * @param prenos[] $prenosi
     * @return prenosResponse
     */
    public function setPrenosi(array $prenosi = null)
    {
      $this->prenosi = $prenosi;
      return $this;
    }

}
