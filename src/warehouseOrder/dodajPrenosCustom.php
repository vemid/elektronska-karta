<?php

class dodajPrenosCustom
{

    /**
     * @var prenos[] $prenosi
     */
    protected $prenosi = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return prenos[]
     */
    public function getPrenosi()
    {
      return $this->prenosi;
    }

    /**
     * @param prenos[] $prenosi
     * @return dodajPrenos
     */
    public function setPrenosi(array $prenosi = null)
    {
      $this->prenosi = $prenosi;
      return $this;
    }

}
