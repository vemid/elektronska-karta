<?php

class dodajNalogZaPrenosResponse extends docResponseCustom
{

    /**
     * @var prenosResponse $return
     */
    protected $return = null;

    
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return prenosResponse
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param prenosResponse $return
     * @return dodajNalogZaPrenosResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
