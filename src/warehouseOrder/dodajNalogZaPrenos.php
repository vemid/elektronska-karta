<?php

class dodajNalogZaPrenos
{

    /**
     * @var prenos[] $prenosi
     */
    protected $prenosi = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return prenos[]
     */
    public function getPrenosi()
    {
      return $this->prenosi;
    }

    /**
     * @param prenos[] $prenosi
     * @return dodajNalogZaPrenos
     */
    public function setPrenosi(array $prenosi = null)
    {
      $this->prenosi = $prenosi;
      return $this;
    }

}
