<?php

class prenosCustom
{

    /**
     * @var \DateTime $datumDokumenta
     */
    protected $datumDokumenta = null;

    /**
     * @var string $logname
     */
    protected $logname = null;

    /**
     * @var string $napomena
     */
    protected $napomena = null;

    /**
     * @var string $oznakaDokumenta
     */
    protected $oznakaDokumenta = null;

    /**
     * @var string $prenosnicaIzlaza
     */
    protected $prenosnicaIzlaza = null;

    /**
     * @var string $sifraMagacinaIzlaza
     */
    protected $sifraMagacinaIzlaza = null;

    /**
     * @var string $sifraMagacinaUlaza
     */
    protected $sifraMagacinaUlaza = null;

    /**
     * @var prenosStavka[] $stavke
     */
    protected $stavke = null;

    /**
     * @var string $storno
     */
    protected $storno = null;

    /**
     * @var vrstaKnjizenja $vrstaKnjizenja
     */
    protected $vrstaKnjizenja = null;

    /**
     * @var vrstaPrenosa $vrstaPrenosa
     */
    protected $vrstaPrenosa = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return \DateTime
     */
    public function getDatumDokumenta()
    {
      if ($this->datumDokumenta == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->datumDokumenta);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $datumDokumenta
     * @return prenos
     */
    public function setDatumDokumenta(\DateTime $datumDokumenta = null)
    {
      if ($datumDokumenta == null) {
       $this->datumDokumenta = null;
      } else {
        $this->datumDokumenta = $datumDokumenta->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getLogname()
    {
      return $this->logname;
    }

    /**
     * @param string $logname
     * @return prenos
     */
    public function setLogname($logname)
    {
      $this->logname = $logname;
      return $this;
    }

    /**
     * @return string
     */
    public function getNapomena()
    {
      return $this->napomena;
    }

    /**
     * @param string $napomena
     * @return prenos
     */
    public function setNapomena($napomena)
    {
      $this->napomena = $napomena;
      return $this;
    }

    /**
     * @return string
     */
    public function getOznakaDokumenta()
    {
      return $this->oznakaDokumenta;
    }

    /**
     * @param string $oznakaDokumenta
     * @return prenos
     */
    public function setOznakaDokumenta($oznakaDokumenta)
    {
      $this->oznakaDokumenta = $oznakaDokumenta;
      return $this;
    }

    /**
     * @return string
     */
    public function getPrenosnicaIzlaza()
    {
      return $this->prenosnicaIzlaza;
    }

    /**
     * @param string $prenosnicaIzlaza
     * @return prenos
     */
    public function setPrenosnicaIzlaza($prenosnicaIzlaza)
    {
      $this->prenosnicaIzlaza = $prenosnicaIzlaza;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraMagacinaIzlaza()
    {
      return $this->sifraMagacinaIzlaza;
    }

    /**
     * @param string $sifraMagacinaIzlaza
     * @return prenos
     */
    public function setSifraMagacinaIzlaza($sifraMagacinaIzlaza)
    {
      $this->sifraMagacinaIzlaza = $sifraMagacinaIzlaza;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraMagacinaUlaza()
    {
      return $this->sifraMagacinaUlaza;
    }

    /**
     * @param string $sifraMagacinaUlaza
     * @return prenos
     */
    public function setSifraMagacinaUlaza($sifraMagacinaUlaza)
    {
      $this->sifraMagacinaUlaza = $sifraMagacinaUlaza;
      return $this;
    }

    /**
     * @return prenosStavka[]
     */
    public function getStavke()
    {
      return $this->stavke;
    }

    /**
     * @param prenosStavka[] $stavke
     * @return prenos
     */
    public function setStavke(array $stavke = null)
    {
      $this->stavke = $stavke;
      return $this;
    }

    /**
     * @return string
     */
    public function getStorno()
    {
      return $this->storno;
    }

    /**
     * @param string $storno
     * @return prenos
     */
    public function setStorno($storno)
    {
      $this->storno = $storno;
      return $this;
    }

    /**
     * @return vrstaKnjizenja
     */
    public function getVrstaKnjizenja()
    {
      return $this->vrstaKnjizenja;
    }

    /**
     * @param vrstaKnjizenja $vrstaKnjizenja
     * @return prenos
     */
    public function setVrstaKnjizenja($vrstaKnjizenja)
    {
      $this->vrstaKnjizenja = $vrstaKnjizenja;
      return $this;
    }

    /**
     * @return vrstaPrenosa
     */
    public function getVrstaPrenosa()
    {
      return $this->vrstaPrenosa;
    }

    /**
     * @param vrstaPrenosa $vrstaPrenosa
     * @return prenos
     */
    public function setVrstaPrenosa($vrstaPrenosa)
    {
      $this->vrstaPrenosa = $vrstaPrenosa;
      return $this;
    }

}
