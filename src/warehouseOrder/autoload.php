<?php


 function autoload_393e74647c26d973a0d55fbbbcd734c7($class)
{
    $classes = array(
        'PrenosServisService' => __DIR__ . '/PrenosServisService.php',
        'dodajNalogZaPrenos' => __DIR__ . '/dodajNalogZaPrenos.php',
        'prenos' => __DIR__ . '/prenos.php',
        'prenosStavka' => __DIR__ . '/prenosStavka.php',
        'dodajNalogZaPrenosResponse' => __DIR__ . '/dodajNalogZaPrenosResponse.php',
        'prenosResponse' => __DIR__ . '/prenosResponse.php',
        'docResponseCustom' => __DIR__ . '/docResponseCustom.php',
        'getPrenos' => __DIR__ . '/getPrenos.php',
        'getPrenosResponse' => __DIR__ . '/getPrenosResponse.php',
        'dodajPrenos' => __DIR__ . '/dodajPrenos.php',
        'dodajPrenosResponse' => __DIR__ . '/dodajPrenosResponse.php',
        'vrstaKnjizenja' => __DIR__ . '/vrstaKnjizenja.php',
        'vrstaPrenosa' => __DIR__ . '/vrstaPrenosa.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_393e74647c26d973a0d55fbbbcd734c7');

// Do nothing. The rest is just leftovers from the code generation.
{
}
