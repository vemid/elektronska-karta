<?php

class dodajPrenosResponse extends docResponseCustom
{

    /**
     * @var prenosResponse $return
     */
    protected $return = null;

    
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return prenosResponse
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param prenosResponse $return
     * @return dodajPrenosResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
