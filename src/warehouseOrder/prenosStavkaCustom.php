<?php

class prenosStavkaCustom
{

    /**
     * @var float $kolicina
     */
    protected $kolicina = null;

    /**
     * @var float $nabavnaCena
     */
    protected $nabavnaCena = null;

    /**
     * @var float $prodajnaCenaBezPoreza
     */
    protected $prodajnaCenaBezPoreza = null;

    /**
     * @var string $sifraObelezja
     */
    protected $sifraObelezja = null;

    /**
     * @var string $sifraObelezjaU
     */
    protected $sifraObelezjaU = null;

    /**
     * @var string $sifraRobe
     */
    protected $sifraRobe = null;

    /**
     * @var string $zonaMagacina
     */
    protected $zonaMagacina = null;

    /**
     * @var string $zonaMagacinaU
     */
    protected $zonaMagacinaU = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getKolicina()
    {
      return $this->kolicina;
    }

    /**
     * @param float $kolicina
     * @return prenosStavka
     */
    public function setKolicina($kolicina)
    {
      $this->kolicina = $kolicina;
      return $this;
    }

    /**
     * @return float
     */
    public function getNabavnaCena()
    {
      return $this->nabavnaCena;
    }

    /**
     * @param float $nabavnaCena
     * @return prenosStavka
     */
    public function setNabavnaCena($nabavnaCena)
    {
      $this->nabavnaCena = $nabavnaCena;
      return $this;
    }

    /**
     * @return float
     */
    public function getProdajnaCenaBezPoreza()
    {
      return $this->prodajnaCenaBezPoreza;
    }

    /**
     * @param float $prodajnaCenaBezPoreza
     * @return prenosStavka
     */
    public function setProdajnaCenaBezPoreza($prodajnaCenaBezPoreza)
    {
      $this->prodajnaCenaBezPoreza = $prodajnaCenaBezPoreza;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraObelezja()
    {
      return $this->sifraObelezja;
    }

    /**
     * @param string $sifraObelezja
     * @return prenosStavka
     */
    public function setSifraObelezja($sifraObelezja)
    {
      $this->sifraObelezja = $sifraObelezja;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraObelezjaU()
    {
      return $this->sifraObelezjaU;
    }

    /**
     * @param string $sifraObelezjaU
     * @return prenosStavka
     */
    public function setSifraObelezjaU($sifraObelezjaU)
    {
      $this->sifraObelezjaU = $sifraObelezjaU;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraRobe()
    {
      return $this->sifraRobe;
    }

    /**
     * @param string $sifraRobe
     * @return prenosStavka
     */
    public function setSifraRobe($sifraRobe)
    {
      $this->sifraRobe = $sifraRobe;
      return $this;
    }

    /**
     * @return string
     */
    public function getZonaMagacina()
    {
      return $this->zonaMagacina;
    }

    /**
     * @param string $zonaMagacina
     * @return prenosStavka
     */
    public function setZonaMagacina($zonaMagacina)
    {
      $this->zonaMagacina = $zonaMagacina;
      return $this;
    }

    /**
     * @return string
     */
    public function getZonaMagacinaU()
    {
      return $this->zonaMagacinaU;
    }

    /**
     * @param string $zonaMagacinaU
     * @return prenosStavka
     */
    public function setZonaMagacinaU($zonaMagacinaU)
    {
      $this->zonaMagacinaU = $zonaMagacinaU;
      return $this;
    }

}
