<?php

class getPrenosResponseCustom
{

    /**
     * @var prenosResponse $return
     */
    protected $return = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return prenosResponse
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param prenosResponse $return
     * @return getPrenosResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
