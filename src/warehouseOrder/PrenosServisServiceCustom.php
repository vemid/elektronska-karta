<?php

class PrenosServisServiceCustom extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'dodajNalogZaPrenos' => '\\dodajNalogZaPrenosCustom',
      'prenos' => '\\prenosCustom',
      'prenosStavka' => '\\prenosStavkaCustom',
      'dodajNalogZaPrenosResponse' => '\\dodajNalogZaPrenosResponseCustom',
      'prenosResponse' => '\\prenosResponseCustom',
      'docResponse' => '\\docResponseCustom2',
      'getPrenos' => '\\getPrenosCustom',
      'getPrenosResponse' => '\\getPrenosResponseCustom',
      'dodajPrenos' => '\\dodajPrenosCustom',
      'dodajPrenosResponse' => '\\dodajPrenosResponseCustom',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'http://192.168.100.210:8080/ServisMisWeb/services/PrenosServisPort?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param dodajNalogZaPrenosCustom $parameters
     * @return dodajNalogZaPrenosResponse
     */
    public function dodajNalogZaPrenos($parameters)
    {
      return $this->__soapCall('dodajNalogZaPrenos', array($parameters));
    }

    /**
     * @param getPrenosCustom $parameters
     * @return getPrenosResponse
     */
    public function getPrenos($parameters)
    {
      return $this->__soapCall('getPrenos', array($parameters));
    }

    /**
     * @param dodajPrenosCustom $parameters
     * @return dodajPrenosResponse
     */
    public function dodajPrenos($parameters)
    {
      return $this->__soapCall('dodajPrenos', array($parameters));
    }

}
