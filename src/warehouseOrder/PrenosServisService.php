<?php

class PrenosServisService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'dodajNalogZaPrenos' => '\\dodajNalogZaPrenos',
      'prenos' => '\\prenos',
      'prenosStavka' => '\\prenosStavka',
      'dodajNalogZaPrenosResponse' => '\\dodajNalogZaPrenosResponse',
      'prenosResponse' => '\\prenosResponse',
      'docResponse' => '\\docResponseCustom',
      'getPrenos' => '\\getPrenos',
      'getPrenosResponse' => '\\getPrenosResponse',
      'dodajPrenos' => '\\dodajPrenos',
      'dodajPrenosResponse' => '\\dodajPrenosResponse',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'http://192.168.100.210:8080/ServisMisWeb/services/PrenosServisPort?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param dodajNalogZaPrenos $parameters
     * @return dodajNalogZaPrenosResponse
     */
    public function dodajNalogZaPrenos(dodajNalogZaPrenos $parameters)
    {
      return $this->__soapCall('dodajNalogZaPrenos', array($parameters));
    }

    /**
     * @param getPrenos $parameters
     * @return getPrenosResponse
     */
    public function getPrenos(getPrenos $parameters)
    {
      return $this->__soapCall('getPrenos', array($parameters));
    }

    /**
     * @param dodajPrenos $parameters
     * @return dodajPrenosResponse
     */
    public function dodajPrenos(dodajPrenos $parameters)
    {
      return $this->__soapCall('dodajPrenos', array($parameters));
    }

}
