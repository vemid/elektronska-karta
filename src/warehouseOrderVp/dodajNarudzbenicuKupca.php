<?php

class dodajNarudzbenicuKupca
{

    /**
     * @var narudzbenicaKupca[] $narudzbenice
     */
    protected $narudzbenice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return narudzbenicaKupca[]
     */
    public function getNarudzbenice()
    {
      return $this->narudzbenice;
    }

    /**
     * @param narudzbenicaKupca[] $narudzbenice
     * @return dodajNarudzbenicuKupca
     */
    public function setNarudzbenice(array $narudzbenice = null)
    {
      $this->narudzbenice = $narudzbenice;
      return $this;
    }

}
