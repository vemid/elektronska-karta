<?php

class dodajNarudzbenicuKupcaResponse extends docResponseCustom
{

    /**
     * @var narudzbenicaKupcaResponse $return
     */
    protected $return = null;

    
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return narudzbenicaKupcaResponse
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param narudzbenicaKupcaResponse $return
     * @return dodajNarudzbenicuKupcaResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
