<?php

class nalogZaIzdavanjeResponse extends responseCustom
{

    /**
     * @var nalogZaIzdavanje[] $nalozi
     */
    protected $nalozi = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return nalogZaIzdavanje[]
     */
    public function getNalozi()
    {
      return $this->nalozi;
    }

    /**
     * @param nalogZaIzdavanje[] $nalozi
     * @return nalogZaIzdavanjeResponse
     */
    public function setNalozi(array $nalozi = null)
    {
      $this->nalozi = $nalozi;
      return $this;
    }

}
