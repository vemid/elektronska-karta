<?php

class narudzbenicaKupcaStavka
{

    /**
     * @var float $akcijskaStopaRabata
     */
    protected $akcijskaStopaRabata = null;

    /**
     * @var float $cenaSaPorezom
     */
    protected $cenaSaPorezom = null;

    /**
     * @var float $dodatniRabat
     */
    protected $dodatniRabat = null;

    /**
     * @var float $kolicina
     */
    protected $kolicina = null;

    /**
     * @var float $osnovnaCenaBezporeza
     */
    protected $osnovnaCenaBezporeza = null;

    /**
     * @var float $osnovnaCenaSaRabatom
     */
    protected $osnovnaCenaSaRabatom = null;

    /**
     * @var float $osnovnavrednostSaSaRabatom
     */
    protected $osnovnavrednostSaSaRabatom = null;

    /**
     * @var float $posebanRabat
     */
    protected $posebanRabat = null;

    /**
     * @var \DateTime $rokIsporuke
     */
    protected $rokIsporuke = null;

    /**
     * @var string $sifraMagacina
     */
    protected $sifraMagacina = null;

    /**
     * @var string $sifraObelezja
     */
    protected $sifraObelezja = null;

    /**
     * @var string $sifraRobe
     */
    protected $sifraRobe = null;

    /**
     * @var float $stopaPoreza
     */
    protected $stopaPoreza = null;

    /**
     * @var float $stopaRabata
     */
    protected $stopaRabata = null;

    /**
     * @var float $vrednostSaPorezom
     */
    protected $vrednostSaPorezom = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getAkcijskaStopaRabata()
    {
      return $this->akcijskaStopaRabata;
    }

    /**
     * @param float $akcijskaStopaRabata
     * @return narudzbenicaKupcaStavka
     */
    public function setAkcijskaStopaRabata($akcijskaStopaRabata)
    {
      $this->akcijskaStopaRabata = $akcijskaStopaRabata;
      return $this;
    }

    /**
     * @return float
     */
    public function getCenaSaPorezom()
    {
      return $this->cenaSaPorezom;
    }

    /**
     * @param float $cenaSaPorezom
     * @return narudzbenicaKupcaStavka
     */
    public function setCenaSaPorezom($cenaSaPorezom)
    {
      $this->cenaSaPorezom = $cenaSaPorezom;
      return $this;
    }

    /**
     * @return float
     */
    public function getDodatniRabat()
    {
      return $this->dodatniRabat;
    }

    /**
     * @param float $dodatniRabat
     * @return narudzbenicaKupcaStavka
     */
    public function setDodatniRabat($dodatniRabat)
    {
      $this->dodatniRabat = $dodatniRabat;
      return $this;
    }

    /**
     * @return float
     */
    public function getKolicina()
    {
      return $this->kolicina;
    }

    /**
     * @param float $kolicina
     * @return narudzbenicaKupcaStavka
     */
    public function setKolicina($kolicina)
    {
      $this->kolicina = $kolicina;
      return $this;
    }

    /**
     * @return float
     */
    public function getOsnovnaCenaBezporeza()
    {
      return $this->osnovnaCenaBezporeza;
    }

    /**
     * @param float $osnovnaCenaBezporeza
     * @return narudzbenicaKupcaStavka
     */
    public function setOsnovnaCenaBezporeza($osnovnaCenaBezporeza)
    {
      $this->osnovnaCenaBezporeza = $osnovnaCenaBezporeza;
      return $this;
    }

    /**
     * @return float
     */
    public function getOsnovnaCenaSaRabatom()
    {
      return $this->osnovnaCenaSaRabatom;
    }

    /**
     * @param float $osnovnaCenaSaRabatom
     * @return narudzbenicaKupcaStavka
     */
    public function setOsnovnaCenaSaRabatom($osnovnaCenaSaRabatom)
    {
      $this->osnovnaCenaSaRabatom = $osnovnaCenaSaRabatom;
      return $this;
    }

    /**
     * @return float
     */
    public function getOsnovnavrednostSaSaRabatom()
    {
      return $this->osnovnavrednostSaSaRabatom;
    }

    /**
     * @param float $osnovnavrednostSaSaRabatom
     * @return narudzbenicaKupcaStavka
     */
    public function setOsnovnavrednostSaSaRabatom($osnovnavrednostSaSaRabatom)
    {
      $this->osnovnavrednostSaSaRabatom = $osnovnavrednostSaSaRabatom;
      return $this;
    }


    /**
     * @return \DateTime
     */
    public function getRokIsporuke()
    {
      if ($this->rokIsporuke == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->rokIsporuke);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $rokIsporuke
     * @return narudzbenicaKupcaStavka
     */
    public function setRokIsporuke(\DateTime $rokIsporuke = null)
    {
      if ($rokIsporuke == null) {
       $this->rokIsporuke = null;
      } else {
        $this->rokIsporuke = $rokIsporuke->format(\DateTime::ATOM);
      }
      return $this;
    }


    /**
     * @return string
     */
    public function getSifraObelezja()
    {
      return $this->sifraObelezja;
    }

    /**
     * @param string $sifraObelezja
     * @return narudzbenicaKupcaStavka
     */
    public function setSifraObelezja($sifraObelezja)
    {
      $this->sifraObelezja = $sifraObelezja;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraRobe()
    {
      return $this->sifraRobe;
    }

    /**
     * @param string $sifraRobe
     * @return narudzbenicaKupcaStavka
     */
    public function setSifraRobe($sifraRobe)
    {
      $this->sifraRobe = $sifraRobe;
      return $this;
    }

    /**
     * @return float
     */
    public function getStopaPoreza()
    {
      return $this->stopaPoreza;
    }

    /**
     * @param float $stopaPoreza
     * @return narudzbenicaKupcaStavka
     */
    public function setStopaPoreza($stopaPoreza)
    {
      $this->stopaPoreza = $stopaPoreza;
      return $this;
    }

    /**
     * @return float
     */
    public function getStopaRabata()
    {
      return $this->stopaRabata;
    }

    /**
     * @param float $stopaRabata
     * @return narudzbenicaKupcaStavka
     */
    public function setStopaRabata($stopaRabata)
    {
      $this->stopaRabata = $stopaRabata;
      return $this;
    }

    /**
     * @return float
     */
    public function getVrednostSaPorezom()
    {
      return $this->vrednostSaPorezom;
    }

    /**
     * @param float $vrednostSaPorezom
     * @return narudzbenicaKupcaStavka
     */
    public function setVrednostSaPorezom($vrednostSaPorezom)
    {
      $this->vrednostSaPorezom = $vrednostSaPorezom;
      return $this;
    }

}
