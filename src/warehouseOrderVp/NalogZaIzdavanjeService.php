<?php

class NalogZaIzdavanjeService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'dodajNalogZaIzdavanje' => '\\dodajNalogZaIzdavanje',
      'nalogZaIzdavanje' => '\\nalogZaIzdavanje',
      'nalogZaIzdavanjeStavka' => '\\nalogZaIzdavanjeStavka',
      'dodajNalogZaIzdavanjeResponse' => '\\dodajNalogZaIzdavanjeResponse',
      'nalogZaIzdavanjeResponse' => '\\nalogZaIzdavanjeResponse',
      'response' => '\\responseCustom',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'http://192.168.100.210:8080/ServisMisWeb/services/DodajNalogZaIzdavanjePort?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param dodajNalogZaIzdavanje $parameters
     * @return dodajNalogZaIzdavanjeResponse
     */
    public function dodajNalogZaIzdavanje(dodajNalogZaIzdavanje $parameters)
    {
      return $this->__soapCall('dodajNalogZaIzdavanje', array($parameters));
    }

}
