<?php

class NarudzbenicaKupcaServisService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'stornoNarudzbenicaKupca' => '\\stornoNarudzbenicaKupca',
      'stornoNarudzbenicaKupcaResponse' => '\\stornoNarudzbenicaKupcaResponse',
      'narudzbenicaKupcaResponse' => '\\narudzbenicaKupcaResponse',
      'response' => '\\response',
      'narudzbenicaKupca' => '\\narudzbenicaKupca',
      'narudzbenicaKupcaStavka' => '\\narudzbenicaKupcaStavka',
      'narudzbenicaKupcaUslugaStavka' => '\\narudzbenicaKupcaUslugaStavka',
      'dodajNarudzbenicuKupca' => '\\dodajNarudzbenicuKupca',
      'dodajNarudzbenicuKupcaResponse' => '\\dodajNarudzbenicuKupcaResponse',
      'getNarudzbenicaKupca' => '\\getNarudzbenicaKupca',
      'getNarudzbenicaKupcaResponse' => '\\getNarudzbenicaKupcaResponse',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'http://192.168.100.210:8080/ServisMisWebTest/services/NarudzbenicaKupcaServisPort?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param dodajNarudzbenicuKupca $parameters
     * @return dodajNarudzbenicuKupcaResponse
     */
    public function dodajNarudzbenicuKupca(dodajNarudzbenicuKupca $parameters)
    {
      return $this->__soapCall('dodajNarudzbenicuKupca', array($parameters));
    }

    /**
     * @param stornoNarudzbenicaKupca $parameters
     * @return stornoNarudzbenicaKupcaResponse
     */
    public function stornoNarudzbenicaKupca(stornoNarudzbenicaKupca $parameters)
    {
      return $this->__soapCall('stornoNarudzbenicaKupca', array($parameters));
    }

    /**
     * @param getNarudzbenicaKupca $parameters
     * @return getNarudzbenicaKupcaResponse
     */
    public function getNarudzbenicaKupca(getNarudzbenicaKupca $parameters)
    {
      return $this->__soapCall('getNarudzbenicaKupca', array($parameters));
    }

}
