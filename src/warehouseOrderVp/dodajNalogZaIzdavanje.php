<?php

class dodajNalogZaIzdavanje
{

    /**
     * @var nalogZaIzdavanje[] $dokumenti
     */
    protected $dokumenti = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return nalogZaIzdavanje[]
     */
    public function getDokumenti()
    {
      return $this->dokumenti;
    }

    /**
     * @param nalogZaIzdavanje[] $dokumenti
     * @return dodajNalogZaIzdavanje
     */
    public function setDokumenti(array $dokumenti = null)
    {
      $this->dokumenti = $dokumenti;
      return $this;
    }

}
