<?php

class getNarudzbenicaKupca
{

    /**
     * @var string $brojDokumenta
     */
    protected $brojDokumenta = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getBrojDokumenta()
    {
      return $this->brojDokumenta;
    }

    /**
     * @param string $brojDokumenta
     * @return getNarudzbenicaKupca
     */
    public function setBrojDokumenta($brojDokumenta)
    {
      $this->brojDokumenta = $brojDokumenta;
      return $this;
    }

}
