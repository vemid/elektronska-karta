<?php

class nalogZaIzdavanje
{

    /**
     * @var float $akcijskiRabatVrednost
     */
    protected $akcijskiRabatVrednost = null;

    /**
     * @var float $brojPaleta
     */
    protected $brojPaleta = null;

    /**
     * @var int $brojRata
     */
    protected $brojRata = null;

    /**
     * @var float $cenaPrevoza
     */
    protected $cenaPrevoza = null;

    /**
     * @var \DateTime $datumDokumenta
     */
    protected $datumDokumenta = null;

    /**
     * @var \DateTime $datumOtpreme
     */
    protected $datumOtpreme = null;

    /**
     * @var \DateTime $dpo
     */
    protected $dpo = null;

    /**
     * @var int $gratisPer
     */
    protected $gratisPer = null;

    /**
     * @var float $iznosKasaSkonto
     */
    protected $iznosKasaSkonto = null;

    /**
     * @var float $iznosManipulativnihTroskova
     */
    protected $iznosManipulativnihTroskova = null;

    /**
     * @var float $kasaSkonto
     */
    protected $kasaSkonto = null;

    /**
     * @var string $kreirajAhp
     */
    protected $kreirajAhp = null;

    /**
     * @var string $logname
     */
    protected $logname = null;

    /**
     * @var float $marza
     */
    protected $marza = null;

    /**
     * @var string $mestoIsporuke
     */
    protected $mestoIsporuke = null;

    /**
     * @var string $oznakaCenovnika
     */
    protected $oznakaCenovnika = null;

    /**
     * @var string $oznakaDokumenta
     */
    protected $oznakaDokumenta = null;

    /**
     * @var string $oznakaRacuna
     */
    protected $oznakaRacuna = null;

    /**
     * @var string $realizovano
     */
    protected $realizovano = null;

    /**
     * @var int $rokIsporukeUDanima
     */
    protected $rokIsporukeUDanima = null;

    /**
     * @var string $sifraMagacina
     */
    protected $sifraMagacina = null;

    /**
     * @var string $sifraNacinaPlacanja
     */
    protected $sifraNacinaPlacanja = null;

    /**
     * @var string $sifraOrganizacioneJednice
     */
    protected $sifraOrganizacioneJednice = null;

    /**
     * @var string $sifraOrganizacioneJedniceNarucioca
     */
    protected $sifraOrganizacioneJedniceNarucioca = null;

    /**
     * @var string $sifraPartnera
     */
    protected $sifraPartnera = null;

    /**
     * @var string $sifraPartneraKorisnika
     */
    protected $sifraPartneraKorisnika = null;

    /**
     * @var string $sifraRacuna
     */
    protected $sifraRacuna = null;

    /**
     * @var string $status
     */
    protected $status = null;

    /**
     * @var nalogZaIzdavanjeStavka[] $stavke
     */
    protected $stavke = null;

    /**
     * @var float $stopaManipulativnihTroskova
     */
    protected $stopaManipulativnihTroskova = null;

    /**
     * @var string $storno
     */
    protected $storno = null;

    /**
     * @var string $tipNaloga
     */
    protected $tipNaloga = null;

    /**
     * @var \DateTime $valutaPlacanja
     */
    protected $valutaPlacanja = null;

    /**
     * @var float $vrednost
     */
    protected $vrednost = null;

    /**
     * @var string $vrstaFakturisanja
     */
    protected $vrstaFakturisanja = null;

    /**
     * @var string $vrstaIzjave
     */
    protected $vrstaIzjave = null;

    /**
     * @var string $vrstaPrevoza
     */
    protected $vrstaPrevoza = null;

    /**
     * @param int $brojRata
     * @param int $gratisPer
     * @param int $rokIsporukeUDanima
     */
    public function __construct($brojRata, $gratisPer, $rokIsporukeUDanima)
    {
      $this->brojRata = $brojRata;
      $this->gratisPer = $gratisPer;
      $this->rokIsporukeUDanima = $rokIsporukeUDanima;
    }

    /**
     * @return float
     */
    public function getAkcijskiRabatVrednost()
    {
      return $this->akcijskiRabatVrednost;
    }

    /**
     * @param float $akcijskiRabatVrednost
     * @return nalogZaIzdavanje
     */
    public function setAkcijskiRabatVrednost($akcijskiRabatVrednost)
    {
      $this->akcijskiRabatVrednost = $akcijskiRabatVrednost;
      return $this;
    }

    /**
     * @return float
     */
    public function getBrojPaleta()
    {
      return $this->brojPaleta;
    }

    /**
     * @param float $brojPaleta
     * @return nalogZaIzdavanje
     */
    public function setBrojPaleta($brojPaleta)
    {
      $this->brojPaleta = $brojPaleta;
      return $this;
    }

    /**
     * @return int
     */
    public function getBrojRata()
    {
      return $this->brojRata;
    }

    /**
     * @param int $brojRata
     * @return nalogZaIzdavanje
     */
    public function setBrojRata($brojRata)
    {
      $this->brojRata = $brojRata;
      return $this;
    }

    /**
     * @return float
     */
    public function getCenaPrevoza()
    {
      return $this->cenaPrevoza;
    }

    /**
     * @param float $cenaPrevoza
     * @return nalogZaIzdavanje
     */
    public function setCenaPrevoza($cenaPrevoza)
    {
      $this->cenaPrevoza = $cenaPrevoza;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatumDokumenta()
    {
      if ($this->datumDokumenta == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->datumDokumenta);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $datumDokumenta
     * @return nalogZaIzdavanje
     */
    public function setDatumDokumenta(\DateTime $datumDokumenta = null)
    {
      if ($datumDokumenta == null) {
       $this->datumDokumenta = null;
      } else {
        $this->datumDokumenta = $datumDokumenta->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatumOtpreme()
    {
      if ($this->datumOtpreme == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->datumOtpreme);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $datumOtpreme
     * @return nalogZaIzdavanje
     */
    public function setDatumOtpreme(\DateTime $datumOtpreme = null)
    {
      if ($datumOtpreme == null) {
       $this->datumOtpreme = null;
      } else {
        $this->datumOtpreme = $datumOtpreme->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDpo()
    {
      if ($this->dpo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->dpo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $dpo
     * @return nalogZaIzdavanje
     */
    public function setDpo(\DateTime $dpo = null)
    {
      if ($dpo == null) {
       $this->dpo = null;
      } else {
        $this->dpo = $dpo->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return int
     */
    public function getGratisPer()
    {
      return $this->gratisPer;
    }

    /**
     * @param int $gratisPer
     * @return nalogZaIzdavanje
     */
    public function setGratisPer($gratisPer)
    {
      $this->gratisPer = $gratisPer;
      return $this;
    }

    /**
     * @return float
     */
    public function getIznosKasaSkonto()
    {
      return $this->iznosKasaSkonto;
    }

    /**
     * @param float $iznosKasaSkonto
     * @return nalogZaIzdavanje
     */
    public function setIznosKasaSkonto($iznosKasaSkonto)
    {
      $this->iznosKasaSkonto = $iznosKasaSkonto;
      return $this;
    }

    /**
     * @return float
     */
    public function getIznosManipulativnihTroskova()
    {
      return $this->iznosManipulativnihTroskova;
    }

    /**
     * @param float $iznosManipulativnihTroskova
     * @return nalogZaIzdavanje
     */
    public function setIznosManipulativnihTroskova($iznosManipulativnihTroskova)
    {
      $this->iznosManipulativnihTroskova = $iznosManipulativnihTroskova;
      return $this;
    }

    /**
     * @return float
     */
    public function getKasaSkonto()
    {
      return $this->kasaSkonto;
    }

    /**
     * @param float $kasaSkonto
     * @return nalogZaIzdavanje
     */
    public function setKasaSkonto($kasaSkonto)
    {
      $this->kasaSkonto = $kasaSkonto;
      return $this;
    }

    /**
     * @return string
     */
    public function getKreirajAhp()
    {
      return $this->kreirajAhp;
    }

    /**
     * @param string $kreirajAhp
     * @return nalogZaIzdavanje
     */
    public function setKreirajAhp($kreirajAhp)
    {
      $this->kreirajAhp = $kreirajAhp;
      return $this;
    }

    /**
     * @return string
     */
    public function getLogname()
    {
      return $this->logname;
    }

    /**
     * @param string $logname
     * @return nalogZaIzdavanje
     */
    public function setLogname($logname)
    {
      $this->logname = $logname;
      return $this;
    }

    /**
     * @return float
     */
    public function getMarza()
    {
      return $this->marza;
    }

    /**
     * @param float $marza
     * @return nalogZaIzdavanje
     */
    public function setMarza($marza)
    {
      $this->marza = $marza;
      return $this;
    }

    /**
     * @return string
     */
    public function getMestoIsporuke()
    {
      return $this->mestoIsporuke;
    }

    /**
     * @param string $mestoIsporuke
     * @return nalogZaIzdavanje
     */
    public function setMestoIsporuke($mestoIsporuke)
    {
      $this->mestoIsporuke = $mestoIsporuke;
      return $this;
    }

    /**
     * @return string
     */
    public function getOznakaCenovnika()
    {
      return $this->oznakaCenovnika;
    }

    /**
     * @param string $oznakaCenovnika
     * @return nalogZaIzdavanje
     */
    public function setOznakaCenovnika($oznakaCenovnika)
    {
      $this->oznakaCenovnika = $oznakaCenovnika;
      return $this;
    }

    /**
     * @return string
     */
    public function getOznakaDokumenta()
    {
      return $this->oznakaDokumenta;
    }

    /**
     * @param string $oznakaDokumenta
     * @return nalogZaIzdavanje
     */
    public function setOznakaDokumenta($oznakaDokumenta)
    {
      $this->oznakaDokumenta = $oznakaDokumenta;
      return $this;
    }

    /**
     * @return string
     */
    public function getOznakaRacuna()
    {
      return $this->oznakaRacuna;
    }

    /**
     * @param string $oznakaRacuna
     * @return nalogZaIzdavanje
     */
    public function setOznakaRacuna($oznakaRacuna)
    {
      $this->oznakaRacuna = $oznakaRacuna;
      return $this;
    }

    /**
     * @return string
     */
    public function getRealizovano()
    {
      return $this->realizovano;
    }

    /**
     * @param string $realizovano
     * @return nalogZaIzdavanje
     */
    public function setRealizovano($realizovano)
    {
      $this->realizovano = $realizovano;
      return $this;
    }

    /**
     * @return int
     */
    public function getRokIsporukeUDanima()
    {
      return $this->rokIsporukeUDanima;
    }

    /**
     * @param int $rokIsporukeUDanima
     * @return nalogZaIzdavanje
     */
    public function setRokIsporukeUDanima($rokIsporukeUDanima)
    {
      $this->rokIsporukeUDanima = $rokIsporukeUDanima;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraMagacina()
    {
      return $this->sifraMagacina;
    }

    /**
     * @param string $sifraMagacina
     * @return nalogZaIzdavanje
     */
    public function setSifraMagacina($sifraMagacina)
    {
      $this->sifraMagacina = $sifraMagacina;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraNacinaPlacanja()
    {
      return $this->sifraNacinaPlacanja;
    }

    /**
     * @param string $sifraNacinaPlacanja
     * @return nalogZaIzdavanje
     */
    public function setSifraNacinaPlacanja($sifraNacinaPlacanja)
    {
      $this->sifraNacinaPlacanja = $sifraNacinaPlacanja;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraOrganizacioneJednice()
    {
      return $this->sifraOrganizacioneJednice;
    }

    /**
     * @param string $sifraOrganizacioneJednice
     * @return nalogZaIzdavanje
     */
    public function setSifraOrganizacioneJednice($sifraOrganizacioneJednice)
    {
      $this->sifraOrganizacioneJednice = $sifraOrganizacioneJednice;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraOrganizacioneJedniceNarucioca()
    {
      return $this->sifraOrganizacioneJedniceNarucioca;
    }

    /**
     * @param string $sifraOrganizacioneJedniceNarucioca
     * @return nalogZaIzdavanje
     */
    public function setSifraOrganizacioneJedniceNarucioca($sifraOrganizacioneJedniceNarucioca)
    {
      $this->sifraOrganizacioneJedniceNarucioca = $sifraOrganizacioneJedniceNarucioca;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraPartnera()
    {
      return $this->sifraPartnera;
    }

    /**
     * @param string $sifraPartnera
     * @return nalogZaIzdavanje
     */
    public function setSifraPartnera($sifraPartnera)
    {
      $this->sifraPartnera = $sifraPartnera;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraPartneraKorisnika()
    {
      return $this->sifraPartneraKorisnika;
    }

    /**
     * @param string $sifraPartneraKorisnika
     * @return nalogZaIzdavanje
     */
    public function setSifraPartneraKorisnika($sifraPartneraKorisnika)
    {
      $this->sifraPartneraKorisnika = $sifraPartneraKorisnika;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraRacuna()
    {
      return $this->sifraRacuna;
    }

    /**
     * @param string $sifraRacuna
     * @return nalogZaIzdavanje
     */
    public function setSifraRacuna($sifraRacuna)
    {
      $this->sifraRacuna = $sifraRacuna;
      return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
      return $this->status;
    }

    /**
     * @param string $status
     * @return nalogZaIzdavanje
     */
    public function setStatus($status)
    {
      $this->status = $status;
      return $this;
    }

    /**
     * @return nalogZaIzdavanjeStavka[]
     */
    public function getStavke()
    {
      return $this->stavke;
    }

    /**
     * @param nalogZaIzdavanjeStavka[] $stavke
     * @return nalogZaIzdavanje
     */
    public function setStavke(array $stavke = null)
    {
      $this->stavke = $stavke;
      return $this;
    }

    /**
     * @return float
     */
    public function getStopaManipulativnihTroskova()
    {
      return $this->stopaManipulativnihTroskova;
    }

    /**
     * @param float $stopaManipulativnihTroskova
     * @return nalogZaIzdavanje
     */
    public function setStopaManipulativnihTroskova($stopaManipulativnihTroskova)
    {
      $this->stopaManipulativnihTroskova = $stopaManipulativnihTroskova;
      return $this;
    }

    /**
     * @return string
     */
    public function getStorno()
    {
      return $this->storno;
    }

    /**
     * @param string $storno
     * @return nalogZaIzdavanje
     */
    public function setStorno($storno)
    {
      $this->storno = $storno;
      return $this;
    }

    /**
     * @return string
     */
    public function getTipNaloga()
    {
      return $this->tipNaloga;
    }

    /**
     * @param string $tipNaloga
     * @return nalogZaIzdavanje
     */
    public function setTipNaloga($tipNaloga)
    {
      $this->tipNaloga = $tipNaloga;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getValutaPlacanja()
    {
      if ($this->valutaPlacanja == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->valutaPlacanja);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $valutaPlacanja
     * @return nalogZaIzdavanje
     */
    public function setValutaPlacanja(\DateTime $valutaPlacanja = null)
    {
      if ($valutaPlacanja == null) {
       $this->valutaPlacanja = null;
      } else {
        $this->valutaPlacanja = $valutaPlacanja->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return float
     */
    public function getVrednost()
    {
      return $this->vrednost;
    }

    /**
     * @param float $vrednost
     * @return nalogZaIzdavanje
     */
    public function setVrednost($vrednost)
    {
      $this->vrednost = $vrednost;
      return $this;
    }

    /**
     * @return string
     */
    public function getVrstaFakturisanja()
    {
      return $this->vrstaFakturisanja;
    }

    /**
     * @param string $vrstaFakturisanja
     * @return nalogZaIzdavanje
     */
    public function setVrstaFakturisanja($vrstaFakturisanja)
    {
      $this->vrstaFakturisanja = $vrstaFakturisanja;
      return $this;
    }

    /**
     * @return string
     */
    public function getVrstaIzjave()
    {
      return $this->vrstaIzjave;
    }

    /**
     * @param string $vrstaIzjave
     * @return nalogZaIzdavanje
     */
    public function setVrstaIzjave($vrstaIzjave)
    {
      $this->vrstaIzjave = $vrstaIzjave;
      return $this;
    }

    /**
     * @return string
     */
    public function getVrstaPrevoza()
    {
      return $this->vrstaPrevoza;
    }

    /**
     * @param string $vrstaPrevoza
     * @return nalogZaIzdavanje
     */
    public function setVrstaPrevoza($vrstaPrevoza)
    {
      $this->vrstaPrevoza = $vrstaPrevoza;
      return $this;
    }

}
