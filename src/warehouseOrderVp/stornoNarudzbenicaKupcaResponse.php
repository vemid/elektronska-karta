<?php

class stornoNarudzbenicaKupcaResponse
{

    /**
     * @var narudzbenicaKupcaResponse $return
     */
    protected $return = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return narudzbenicaKupcaResponse
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param narudzbenicaKupcaResponse $return
     * @return stornoNarudzbenicaKupcaResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
