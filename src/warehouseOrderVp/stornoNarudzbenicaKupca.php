<?php

class stornoNarudzbenicaKupca
{

    /**
     * @var string $oznakaNarudzbenice
     */
    protected $oznakaNarudzbenice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getOznakaNarudzbenice()
    {
      return $this->oznakaNarudzbenice;
    }

    /**
     * @param string $oznakaNarudzbenice
     * @return stornoNarudzbenicaKupca
     */
    public function setOznakaNarudzbenice($oznakaNarudzbenice)
    {
      $this->oznakaNarudzbenice = $oznakaNarudzbenice;
      return $this;
    }

}
