<?php

class narudzbenicaKupcaResponse extends response
{

    /**
     * @var narudzbenicaKupca[] $narudzbenice
     */
    protected $narudzbenice = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return narudzbenicaKupca[]
     */
    public function getNarudzbenice()
    {
      return $this->narudzbenice;
    }

    /**
     * @param narudzbenicaKupca[] $narudzbenice
     * @return narudzbenicaKupcaResponse
     */
    public function setNarudzbenice(array $narudzbenice = null)
    {
      $this->narudzbenice = $narudzbenice;
      return $this;
    }

}
