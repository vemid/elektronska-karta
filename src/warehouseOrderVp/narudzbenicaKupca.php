<?php

class narudzbenicaKupca
{

    /**
     * @var \DateTime $datumNaurdzbenice
     */
    protected $datumNaurdzbenice = null;

    /**
     * @var boolean $deviznaNarudzbenica
     */
    protected $deviznaNarudzbenica = null;

    /**
     * @var string $logname
     */
    protected $logname = null;

    /**
     * @var string $napomena
     */
    protected $napomena = null;

    /**
     * @var string $organizacionaJedinica
     */
    protected $organizacionaJedinica = null;

    /**
     * @var string $oznakaCenovnika
     */
    protected $oznakaCenovnika = null;

    /**
     * @var string $oznakaNarudzbenice
     */
    protected $oznakaNarudzbenice = null;

    /**
     * @var string $oznakaNarudzbenicePartnera
     */
    protected $oznakaNarudzbenicePartnera = null;

    /**
     * @var string $oznakaTipaPorudzbenice
     */
    protected $oznakaTipaPorudzbenice = null;

    /**
     * @var string $partnerKorisnik
     */
    protected $partnerKorisnik = null;

    /**
     * @var boolean $realizovano
     */
    protected $realizovano = null;

    /**
     * @var string $saradnik
     */
    protected $saradnik = null;

    /**
     * @var string $sifraNacinaPlacanja
     */
    protected $sifraNacinaPlacanja = null;

    /**
     * @var string $sifraObjektaMaloprodaje
     */
    protected $sifraObjektaMaloprodaje = null;

    /**
     * @var string $sifraPartnera
     */
    protected $sifraPartnera = null;

    /**
     * @var int $sifraValute
     */
    protected $sifraValute = null;

    /**
     * @var narudzbenicaKupcaStavka[] $stavke
     */
    protected $stavke = null;

    /**
     * @var string $storno
     */
    protected $storno = null;

    /**
     * @var \DateTime $valutaPlacanja
     */
    protected $valutaPlacanja = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return \DateTime
     */
    public function getDatumNaurdzbenice()
    {
      if ($this->datumNaurdzbenice == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->datumNaurdzbenice);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $datumNaurdzbenice
     * @return narudzbenicaKupca
     */
    public function setDatumNaurdzbenice(\DateTime $datumNaurdzbenice = null)
    {
      if ($datumNaurdzbenice == null) {
       $this->datumNaurdzbenice = null;
      } else {
        $this->datumNaurdzbenice = $datumNaurdzbenice->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDeviznaNarudzbenica()
    {
      return $this->deviznaNarudzbenica;
    }

    /**
     * @param boolean $deviznaNarudzbenica
     * @return narudzbenicaKupca
     */
    public function setDeviznaNarudzbenica($deviznaNarudzbenica)
    {
      $this->deviznaNarudzbenica = $deviznaNarudzbenica;
      return $this;
    }

    /**
     * @return string
     */
    public function getLogname()
    {
      return $this->logname;
    }

    /**
     * @param string $logname
     * @return narudzbenicaKupca
     */
    public function setLogname($logname)
    {
      $this->logname = $logname;
      return $this;
    }

    /**
     * @return string
     */
    public function getNapomena()
    {
      return $this->napomena;
    }

    /**
     * @param string $napomena
     * @return narudzbenicaKupca
     */
    public function setNapomena($napomena)
    {
      $this->napomena = $napomena;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrganizacionaJedinica()
    {
      return $this->organizacionaJedinica;
    }

    /**
     * @param string $organizacionaJedinica
     * @return narudzbenicaKupca
     */
    public function setOrganizacionaJedinica($organizacionaJedinica)
    {
      $this->organizacionaJedinica = $organizacionaJedinica;
      return $this;
    }

    /**
     * @return string
     */
    public function getOznakaCenovnika()
    {
      return $this->oznakaCenovnika;
    }

    /**
     * @param string $oznakaCenovnika
     * @return narudzbenicaKupca
     */
    public function setOznakaCenovnika($oznakaCenovnika)
    {
      $this->oznakaCenovnika = $oznakaCenovnika;
      return $this;
    }

    /**
     * @return string
     */
    public function getOznakaNarudzbenice()
    {
      return $this->oznakaNarudzbenice;
    }

    /**
     * @param string $oznakaNarudzbenice
     * @return narudzbenicaKupca
     */
    public function setOznakaNarudzbenice($oznakaNarudzbenice)
    {
      $this->oznakaNarudzbenice = $oznakaNarudzbenice;
      return $this;
    }

    /**
     * @return string
     */
    public function getOznakaNarudzbenicePartnera()
    {
      return $this->oznakaNarudzbenicePartnera;
    }

    /**
     * @param string $oznakaNarudzbenicePartnera
     * @return narudzbenicaKupca
     */
    public function setOznakaNarudzbenicePartnera($oznakaNarudzbenicePartnera)
    {
      $this->oznakaNarudzbenicePartnera = $oznakaNarudzbenicePartnera;
      return $this;
    }

//    /**
//     * @return string
//     */
//    public function getOznakaTipaPorudzbenice()
//    {
//      return $this->oznakaTipaPorudzbenice;
//    }
//
//    /**
//     * @param string $oznakaTipaPorudzbenice
//     * @return narudzbenicaKupca
//     */
//    public function setOznakaTipaPorudzbenice($oznakaTipaPorudzbenice)
//    {
//      $this->oznakaTipaPorudzbenice = $oznakaTipaPorudzbenice;
//      return $this;
//    }

    /**
     * @return string
     */
    public function getPartnerKorisnik()
    {
      return $this->partnerKorisnik;
    }

    /**
     * @param string $partnerKorisnik
     * @return narudzbenicaKupca
     */
    public function setPartnerKorisnik($partnerKorisnik)
    {
      $this->partnerKorisnik = $partnerKorisnik;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRealizovano()
    {
      return $this->realizovano;
    }

    /**
     * @param boolean $realizovano
     * @return narudzbenicaKupca
     */
    public function setRealizovano($realizovano)
    {
      $this->realizovano = $realizovano;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraNacinaPlacanja()
    {
      return $this->sifraNacinaPlacanja;
    }

    /**
     * @param string $sifraNacinaPlacanja
     * @return narudzbenicaKupca
     */
    public function setSifraNacinaPlacanja($sifraNacinaPlacanja)
    {
      $this->sifraNacinaPlacanja = $sifraNacinaPlacanja;
      return $this;
    }



    /**
     * @return string
     */
    public function getSifraPartnera()
    {
      return $this->sifraPartnera;
    }

    /**
     * @param string $sifraPartnera
     * @return narudzbenicaKupca
     */
    public function setSifraPartnera($sifraPartnera)
    {
      $this->sifraPartnera = $sifraPartnera;
      return $this;
    }


    /**
     * @return narudzbenicaKupcaStavka[]
     */
    public function getStavke()
    {
      return $this->stavke;
    }

    /**
     * @param narudzbenicaKupcaStavka[] $stavke
     * @return narudzbenicaKupca
     */
    public function setStavke(array $stavke = null)
    {
      $this->stavke = $stavke;
      return $this;
    }

    /**
     * @return string
     */
    public function getStorno()
    {
      return $this->storno;
    }

    /**
     * @param string $storno
     * @return narudzbenicaKupca
     */
    public function setStorno($storno)
    {
      $this->storno = $storno;
      return $this;
    }

//    /**
//     * @return \DateTime
//     */
//    public function getValutaPlacanja()
//    {
//      if ($this->valutaPlacanja == null) {
//        return null;
//      } else {
//        try {
//          return new \DateTime($this->valutaPlacanja);
//        } catch (\Exception $e) {
//          return false;
//        }
//      }
//    }
//
//    /**
//     * @param \DateTime $valutaPlacanja
//     * @return narudzbenicaKupca
//     */
//    public function setValutaPlacanja(\DateTime $valutaPlacanja = null)
//    {
//      if ($valutaPlacanja == null) {
//       $this->valutaPlacanja = null;
//      } else {
//        $this->valutaPlacanja = $valutaPlacanja->format(\DateTime::ATOM);
//      }
//      return $this;
//    }

}
