<?php

class dodajNalogZaIzdavanjeResponse
{

    /**
     * @var nalogZaIzdavanjeResponse $return
     */
    protected $return = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return nalogZaIzdavanjeResponse
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param nalogZaIzdavanjeResponse $return
     * @return dodajNalogZaIzdavanjeResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
