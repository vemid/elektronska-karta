<?php

class getNarudzbenicaKupcaResponse
{

    /**
     * @var narudzbenicaKupcaResponse $return
     */
    protected $return = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return narudzbenicaKupcaResponse
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param narudzbenicaKupcaResponse $return
     * @return getNarudzbenicaKupcaResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
