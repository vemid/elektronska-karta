<?php


 function autoload_c24054a90f012a8fa02d81c629177e8a($class)
{
    $classes = array(
        'NalogZaIzdavanjeService' => __DIR__ .'/NalogZaIzdavanjeService.php',
        'dodajNalogZaIzdavanje' => __DIR__ .'/dodajNalogZaIzdavanje.php',
        'nalogZaIzdavanje' => __DIR__ .'/nalogZaIzdavanje.php',
        'nalogZaIzdavanjeStavka' => __DIR__ .'/nalogZaIzdavanjeStavka.php',
        'dodajNalogZaIzdavanjeResponse' => __DIR__ .'/dodajNalogZaIzdavanjeResponse.php',
        'nalogZaIzdavanjeResponse' => __DIR__ .'/nalogZaIzdavanjeResponse.php',
        'responseCustom' => __DIR__ .'/responseCustom.php',
        'NarudzbenicaKupcaServisService' => __DIR__ .'/NarudzbenicaKupcaServisService.php',
        'stornoNarudzbenicaKupca' => __DIR__ .'/stornoNarudzbenicaKupca.php',
        'stornoNarudzbenicaKupcaResponse' => __DIR__ .'/stornoNarudzbenicaKupcaResponse.php',
        'narudzbenicaKupcaResponse' => __DIR__ .'/narudzbenicaKupcaResponse.php',
        'response' => __DIR__ .'/response.php',
        'narudzbenicaKupca' => __DIR__ .'/narudzbenicaKupca.php',
        'narudzbenicaKupcaStavka' => __DIR__ .'/narudzbenicaKupcaStavka.php',
        'narudzbenicaKupcaUslugaStavka' => __DIR__ .'/narudzbenicaKupcaUslugaStavka.php',
        'dodajNarudzbenicuKupca' => __DIR__ .'/dodajNarudzbenicuKupca.php',
        'dodajNarudzbenicuKupcaResponse' => __DIR__ .'/dodajNarudzbenicuKupcaResponse.php',
        'getNarudzbenicaKupca' => __DIR__ .'/getNarudzbenicaKupca.php',
        'getNarudzbenicaKupcaResponse' => __DIR__ .'/getNarudzbenicaKupcaResponse.php'

    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_c24054a90f012a8fa02d81c629177e8a');

// Do nothing. The rest is just leftovers from the code generation.
{
}
