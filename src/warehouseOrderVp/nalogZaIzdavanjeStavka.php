<?php

class nalogZaIzdavanjeStavka
{

    /**
     * @var float $akcijskaStopaRabata
     */
    protected $akcijskaStopaRabata = null;

    /**
     * @var float $brojPakovanja
     */
    protected $brojPakovanja = null;

    /**
     * @var float $bruto
     */
    protected $bruto = null;

    /**
     * @var float $cenaSaRabatom
     */
    protected $cenaSaRabatom = null;

    /**
     * @var float $deviznaCena
     */
    protected $deviznaCena = null;

    /**
     * @var float $dodatniRabat
     */
    protected $dodatniRabat = null;

    /**
     * @var float $iznosAkcize
     */
    protected $iznosAkcize = null;

    /**
     * @var float $iznosManipulativnihTroskova
     */
    protected $iznosManipulativnihTroskova = null;

    /**
     * @var float $iznosTakse
     */
    protected $iznosTakse = null;

    /**
     * @var float $kolicina
     */
    protected $kolicina = null;

    /**
     * @var float $osnovnaCena
     */
    protected $osnovnaCena = null;

    /**
     * @var float $posebnaStopaRabata
     */
    protected $posebnaStopaRabata = null;

    /**
     * @var float $prodajnaCena
     */
    protected $prodajnaCena = null;

    /**
     * @var float $rabatBezAkciza
     */
    protected $rabatBezAkciza = null;

    /**
     * @var string $sifraObelezja
     */
    protected $sifraObelezja = null;

    /**
     * @var string $sifraRobe
     */
    protected $sifraRobe = null;

    /**
     * @var string $sifraZoneMagacina
     */
    protected $sifraZoneMagacina = null;

    /**
     * @var float $stopaManipulativnihTroskova
     */
    protected $stopaManipulativnihTroskova = null;

    /**
     * @var float $stopaPoreza
     */
    protected $stopaPoreza = null;

    /**
     * @var float $stopaRabata
     */
    protected $stopaRabata = null;

    /**
     * @var float $zahtevanaKolicina
     */
    protected $zahtevanaKolicina = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getAkcijskaStopaRabata()
    {
      return $this->akcijskaStopaRabata;
    }

    /**
     * @param float $akcijskaStopaRabata
     * @return nalogZaIzdavanjeStavka
     */
    public function setAkcijskaStopaRabata($akcijskaStopaRabata)
    {
      $this->akcijskaStopaRabata = $akcijskaStopaRabata;
      return $this;
    }

    /**
     * @return float
     */
    public function getBrojPakovanja()
    {
      return $this->brojPakovanja;
    }

    /**
     * @param float $brojPakovanja
     * @return nalogZaIzdavanjeStavka
     */
    public function setBrojPakovanja($brojPakovanja)
    {
      $this->brojPakovanja = $brojPakovanja;
      return $this;
    }

    /**
     * @return float
     */
    public function getBruto()
    {
      return $this->bruto;
    }

    /**
     * @param float $bruto
     * @return nalogZaIzdavanjeStavka
     */
    public function setBruto($bruto)
    {
      $this->bruto = $bruto;
      return $this;
    }

    /**
     * @return float
     */
    public function getCenaSaRabatom()
    {
      return $this->cenaSaRabatom;
    }

    /**
     * @param float $cenaSaRabatom
     * @return nalogZaIzdavanjeStavka
     */
    public function setCenaSaRabatom($cenaSaRabatom)
    {
      $this->cenaSaRabatom = $cenaSaRabatom;
      return $this;
    }

    /**
     * @return float
     */
    public function getDeviznaCena()
    {
      return $this->deviznaCena;
    }

    /**
     * @param float $deviznaCena
     * @return nalogZaIzdavanjeStavka
     */
    public function setDeviznaCena($deviznaCena)
    {
      $this->deviznaCena = $deviznaCena;
      return $this;
    }

    /**
     * @return float
     */
    public function getDodatniRabat()
    {
      return $this->dodatniRabat;
    }

    /**
     * @param float $dodatniRabat
     * @return nalogZaIzdavanjeStavka
     */
    public function setDodatniRabat($dodatniRabat)
    {
      $this->dodatniRabat = $dodatniRabat;
      return $this;
    }

    /**
     * @return float
     */
    public function getIznosAkcize()
    {
      return $this->iznosAkcize;
    }

    /**
     * @param float $iznosAkcize
     * @return nalogZaIzdavanjeStavka
     */
    public function setIznosAkcize($iznosAkcize)
    {
      $this->iznosAkcize = $iznosAkcize;
      return $this;
    }

    /**
     * @return float
     */
    public function getIznosManipulativnihTroskova()
    {
      return $this->iznosManipulativnihTroskova;
    }

    /**
     * @param float $iznosManipulativnihTroskova
     * @return nalogZaIzdavanjeStavka
     */
    public function setIznosManipulativnihTroskova($iznosManipulativnihTroskova)
    {
      $this->iznosManipulativnihTroskova = $iznosManipulativnihTroskova;
      return $this;
    }

    /**
     * @return float
     */
    public function getIznosTakse()
    {
      return $this->iznosTakse;
    }

    /**
     * @param float $iznosTakse
     * @return nalogZaIzdavanjeStavka
     */
    public function setIznosTakse($iznosTakse)
    {
      $this->iznosTakse = $iznosTakse;
      return $this;
    }

    /**
     * @return float
     */
    public function getKolicina()
    {
      return $this->kolicina;
    }

    /**
     * @param float $kolicina
     * @return nalogZaIzdavanjeStavka
     */
    public function setKolicina($kolicina)
    {
      $this->kolicina = $kolicina;
      return $this;
    }

    /**
     * @return float
     */
    public function getOsnovnaCena()
    {
      return $this->osnovnaCena;
    }

    /**
     * @param float $osnovnaCena
     * @return nalogZaIzdavanjeStavka
     */
    public function setOsnovnaCena($osnovnaCena)
    {
      $this->osnovnaCena = $osnovnaCena;
      return $this;
    }

    /**
     * @return float
     */
    public function getPosebnaStopaRabata()
    {
      return $this->posebnaStopaRabata;
    }

    /**
     * @param float $posebnaStopaRabata
     * @return nalogZaIzdavanjeStavka
     */
    public function setPosebnaStopaRabata($posebnaStopaRabata)
    {
      $this->posebnaStopaRabata = $posebnaStopaRabata;
      return $this;
    }

    /**
     * @return float
     */
    public function getProdajnaCena()
    {
      return $this->prodajnaCena;
    }

    /**
     * @param float $prodajnaCena
     * @return nalogZaIzdavanjeStavka
     */
    public function setProdajnaCena($prodajnaCena)
    {
      $this->prodajnaCena = $prodajnaCena;
      return $this;
    }

    /**
     * @return float
     */
    public function getRabatBezAkciza()
    {
      return $this->rabatBezAkciza;
    }

    /**
     * @param float $rabatBezAkciza
     * @return nalogZaIzdavanjeStavka
     */
    public function setRabatBezAkciza($rabatBezAkciza)
    {
      $this->rabatBezAkciza = $rabatBezAkciza;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraObelezja()
    {
      return $this->sifraObelezja;
    }

    /**
     * @param string $sifraObelezja
     * @return nalogZaIzdavanjeStavka
     */
    public function setSifraObelezja($sifraObelezja)
    {
      $this->sifraObelezja = $sifraObelezja;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraRobe()
    {
      return $this->sifraRobe;
    }

    /**
     * @param string $sifraRobe
     * @return nalogZaIzdavanjeStavka
     */
    public function setSifraRobe($sifraRobe)
    {
      $this->sifraRobe = $sifraRobe;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraZoneMagacina()
    {
      return $this->sifraZoneMagacina;
    }

    /**
     * @param string $sifraZoneMagacina
     * @return nalogZaIzdavanjeStavka
     */
    public function setSifraZoneMagacina($sifraZoneMagacina)
    {
      $this->sifraZoneMagacina = $sifraZoneMagacina;
      return $this;
    }

    /**
     * @return float
     */
    public function getStopaManipulativnihTroskova()
    {
      return $this->stopaManipulativnihTroskova;
    }

    /**
     * @param float $stopaManipulativnihTroskova
     * @return nalogZaIzdavanjeStavka
     */
    public function setStopaManipulativnihTroskova($stopaManipulativnihTroskova)
    {
      $this->stopaManipulativnihTroskova = $stopaManipulativnihTroskova;
      return $this;
    }

    /**
     * @return float
     */
    public function getStopaPoreza()
    {
      return $this->stopaPoreza;
    }

    /**
     * @param float $stopaPoreza
     * @return nalogZaIzdavanjeStavka
     */
    public function setStopaPoreza($stopaPoreza)
    {
      $this->stopaPoreza = $stopaPoreza;
      return $this;
    }

    /**
     * @return float
     */
    public function getStopaRabata()
    {
      return $this->stopaRabata;
    }

    /**
     * @param float $stopaRabata
     * @return nalogZaIzdavanjeStavka
     */
    public function setStopaRabata($stopaRabata)
    {
      $this->stopaRabata = $stopaRabata;
      return $this;
    }

    /**
     * @return float
     */
    public function getZahtevanaKolicina()
    {
      return $this->zahtevanaKolicina;
    }

    /**
     * @param float $zahtevanaKolicina
     * @return nalogZaIzdavanjeStavka
     */
    public function setZahtevanaKolicina($zahtevanaKolicina)
    {
      $this->zahtevanaKolicina = $zahtevanaKolicina;
      return $this;
    }

}
