<?php

class narudzbenicaKupcaUslugaStavka
{

    /**
     * @var float $cenaSaPorezom
     */
    protected $cenaSaPorezom = null;

    /**
     * @var float $kolicina
     */
    protected $kolicina = null;

    /**
     * @var float $osnovnaCenaBezporeza
     */
    protected $osnovnaCenaBezporeza = null;

    /**
     * @var float $osnovnaCenaSaRabatom
     */
    protected $osnovnaCenaSaRabatom = null;

    /**
     * @var float $osnovnavrednostSaSaRabatom
     */
    protected $osnovnavrednostSaSaRabatom = null;

    /**
     * @var string $sifraUsluge
     */
    protected $sifraUsluge = null;

    /**
     * @var float $stopaPoreza
     */
    protected $stopaPoreza = null;

    /**
     * @var float $stopaRabata
     */
    protected $stopaRabata = null;

    /**
     * @var float $vrednostSaPorezom
     */
    protected $vrednostSaPorezom = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getCenaSaPorezom()
    {
      return $this->cenaSaPorezom;
    }

    /**
     * @param float $cenaSaPorezom
     * @return narudzbenicaKupcaUslugaStavka
     */
    public function setCenaSaPorezom($cenaSaPorezom)
    {
      $this->cenaSaPorezom = $cenaSaPorezom;
      return $this;
    }

    /**
     * @return float
     */
    public function getKolicina()
    {
      return $this->kolicina;
    }

    /**
     * @param float $kolicina
     * @return narudzbenicaKupcaUslugaStavka
     */
    public function setKolicina($kolicina)
    {
      $this->kolicina = $kolicina;
      return $this;
    }

    /**
     * @return float
     */
    public function getOsnovnaCenaBezporeza()
    {
      return $this->osnovnaCenaBezporeza;
    }

    /**
     * @param float $osnovnaCenaBezporeza
     * @return narudzbenicaKupcaUslugaStavka
     */
    public function setOsnovnaCenaBezporeza($osnovnaCenaBezporeza)
    {
      $this->osnovnaCenaBezporeza = $osnovnaCenaBezporeza;
      return $this;
    }

    /**
     * @return float
     */
    public function getOsnovnaCenaSaRabatom()
    {
      return $this->osnovnaCenaSaRabatom;
    }

    /**
     * @param float $osnovnaCenaSaRabatom
     * @return narudzbenicaKupcaUslugaStavka
     */
    public function setOsnovnaCenaSaRabatom($osnovnaCenaSaRabatom)
    {
      $this->osnovnaCenaSaRabatom = $osnovnaCenaSaRabatom;
      return $this;
    }

    /**
     * @return float
     */
    public function getOsnovnavrednostSaSaRabatom()
    {
      return $this->osnovnavrednostSaSaRabatom;
    }

    /**
     * @param float $osnovnavrednostSaSaRabatom
     * @return narudzbenicaKupcaUslugaStavka
     */
    public function setOsnovnavrednostSaSaRabatom($osnovnavrednostSaSaRabatom)
    {
      $this->osnovnavrednostSaSaRabatom = $osnovnavrednostSaSaRabatom;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraUsluge()
    {
      return $this->sifraUsluge;
    }

    /**
     * @param string $sifraUsluge
     * @return narudzbenicaKupcaUslugaStavka
     */
    public function setSifraUsluge($sifraUsluge)
    {
      $this->sifraUsluge = $sifraUsluge;
      return $this;
    }

    /**
     * @return float
     */
    public function getStopaPoreza()
    {
      return $this->stopaPoreza;
    }

    /**
     * @param float $stopaPoreza
     * @return narudzbenicaKupcaUslugaStavka
     */
    public function setStopaPoreza($stopaPoreza)
    {
      $this->stopaPoreza = $stopaPoreza;
      return $this;
    }

    /**
     * @return float
     */
    public function getStopaRabata()
    {
      return $this->stopaRabata;
    }

    /**
     * @param float $stopaRabata
     * @return narudzbenicaKupcaUslugaStavka
     */
    public function setStopaRabata($stopaRabata)
    {
      $this->stopaRabata = $stopaRabata;
      return $this;
    }

    /**
     * @return float
     */
    public function getVrednostSaPorezom()
    {
      return $this->vrednostSaPorezom;
    }

    /**
     * @param float $vrednostSaPorezom
     * @return narudzbenicaKupcaUslugaStavka
     */
    public function setVrednostSaPorezom($vrednostSaPorezom)
    {
      $this->vrednostSaPorezom = $vrednostSaPorezom;
      return $this;
    }

}
