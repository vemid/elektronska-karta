<?php

class customSQLServisResponse extends docResponse
{

    /**
     * @var string $xmlString
     */
    protected $xmlString = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getXmlString()
    {
      return $this->xmlString;
    }

    /**
     * @param string $xmlString
     * @return customSQLServisResponse
     */
    public function setXmlString($xmlString)
    {
      $this->xmlString = $xmlString;
      return $this;
    }

}
