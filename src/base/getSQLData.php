<?php

class getSQLData
{

    /**
     * @var string $sql
     */
    protected $sql = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSql()
    {
      return $this->sql;
    }

    /**
     * @param string $sql
     * @return getSQLData
     */
    public function setSql($sql)
    {
      $this->sql = $sql;
      return $this;
    }

}
