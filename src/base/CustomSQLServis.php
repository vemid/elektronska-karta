<?php

class CustomSQLServis extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'getSQLData' => '\\getSQLData',
      'getSQLDataResponse' => '\\getSQLDataResponse',
      'customSQLServisResponse' => '\\customSQLServisResponse',
      'docResponse' => '\\docResponse',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'http://192.168.100.211:8080/ServisMisWeb/services/CustomSQLServisPort?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param getSQLData $parameters
     * @return getSQLDataResponse
     */
    public function getSQLData(getSQLData $parameters)
    {
      return $this->__soapCall('getSQLData', array($parameters));
    }

}
