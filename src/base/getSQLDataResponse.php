<?php

class getSQLDataResponse
{

    /**
     * @var customSQLServisResponse $return
     */
    protected $return = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return customSQLServisResponse
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param customSQLServisResponse $return
     * @return getSQLDataResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
