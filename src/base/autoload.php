<?php


 function autoload_40ff1741b9b7b11763453bd2c0aa01af($class)
{
    $classes = array(
        'CustomSQLServis' => __DIR__ . '/CustomSQLServis.php',
        'getSQLData' => __DIR__ . '/getSQLData.php',
        'getSQLDataResponse' => __DIR__ . '/getSQLDataResponse.php',
        'customSQLServisResponse' => __DIR__ . '/customSQLServisResponse.php',
        'docResponse' => __DIR__ . '/docResponse.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_40ff1741b9b7b11763453bd2c0aa01af');

// Do nothing. The rest is just leftovers from the code generation.
{
}
