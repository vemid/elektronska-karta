<?php

class otpremnicaUMaloprodajuResponseCustom extends docResponseCustom2
{

    /**
     * @var otpremnicaUMaloprodaju $otpremnicaUMaloprodaju
     */
    protected $otpremnicaUMaloprodaju = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return otpremnicaUMaloprodaju
     */
    public function getOtpremnicaUMaloprodaju()
    {
      return $this->otpremnicaUMaloprodaju;
    }

    /**
     * @param otpremnicaUMaloprodaju $otpremnicaUMaloprodaju
     * @return otpremnicaUMaloprodajuResponse
     */
    public function setOtpremnicaUMaloprodaju($otpremnicaUMaloprodaju)
    {
      $this->otpremnicaUMaloprodaju = $otpremnicaUMaloprodaju;
      return $this;
    }

}
