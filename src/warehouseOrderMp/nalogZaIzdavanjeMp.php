<?php

class nalogZaIzdavanjeMp
{

    /**
     * @var string $adresaIsporuke
     */
    protected $adresaIsporuke = null;

    /**
     * @var \DateTime $datumDokumenta
     */
    protected $datumDokumenta = null;

    /**
     * @var \DateTime $dpo
     */
    protected $dpo = null;

    /**
     * @var string $jir
     */
    protected $jir = null;

    /**
     * @var string $kontaktTelefon
     */
    protected $kontaktTelefon = null;

    /**
     * @var string $logname
     */
    protected $logname = null;

    /**
     * @var string $mestoIsporuke
     */
    protected $mestoIsporuke = null;

    /**
     * @var string $napomena
     */
    protected $napomena = null;

    /**
     * @var string $oznakaCenovnika
     */
    protected $oznakaCenovnika = null;

    /**
     * @var string $oznakaDokumenta
     */
    protected $oznakaDokumenta = null;

    /**
     * @var string $oznakaKase
     */
    protected $oznakaKase = null;

    /**
     * @var string $preImeKupca
     */
    protected $preImeKupca = null;

    /**
     * @var string $sifraMagacina
     */
    protected $sifraMagacina = null;

    /**
     * @var string $sifraObjekta
     */
    protected $sifraObjekta = null;

    /**
     * @var string $sifraOrganizacioneJedinice
     */
    protected $sifraOrganizacioneJedinice = null;

    /**
     * @var string $sifraPartnera
     */
    protected $sifraPartnera = null;

    /**
     * @var string $sifraPartneraKorisnik
     */
    protected $sifraPartneraKorisnik = null;

    /**
     * @var int $smena
     */
    protected $smena = null;

    /**
     * @var string $status
     */
    protected $status = null;

    /**
     * @var nalogZaIzdavanjeMpStavka[] $stavke
     */
    protected $stavke = null;

    /**
     * @var string $storno
     */
    protected $storno = null;

    /**
     * @var \DateTime $vreme
     */
    protected $vreme = null;

    /**
     * @var string $zki
     */
    protected $zki = null;

    /**
     * @var string $oznakaInterneKartice
     */
    protected $oznakaInterneKartice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAdresaIsporuke()
    {
      return $this->adresaIsporuke;
    }

    /**
     * @param string $adresaIsporuke
     * @return nalogZaIzdavanjeMp
     */
    public function setAdresaIsporuke($adresaIsporuke)
    {
      $this->adresaIsporuke = $adresaIsporuke;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatumDokumenta()
    {
      if ($this->datumDokumenta == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->datumDokumenta);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $datumDokumenta
     * @return nalogZaIzdavanjeMp
     */
    public function setDatumDokumenta(\DateTime $datumDokumenta = null)
    {
      if ($datumDokumenta == null) {
       $this->datumDokumenta = null;
      } else {
        $this->datumDokumenta = $datumDokumenta->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDpo()
    {
      if ($this->dpo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->dpo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $dpo
     * @return nalogZaIzdavanjeMp
     */
    public function setDpo(\DateTime $dpo = null)
    {
      if ($dpo == null) {
       $this->dpo = null;
      } else {
        $this->dpo = $dpo->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getJir()
    {
      return $this->jir;
    }

    /**
     * @param string $jir
     * @return nalogZaIzdavanjeMp
     */
    public function setJir($jir)
    {
      $this->jir = $jir;
      return $this;
    }

    /**
     * @return string
     */
    public function getKontaktTelefon()
    {
      return $this->kontaktTelefon;
    }

    /**
     * @param string $kontaktTelefon
     * @return nalogZaIzdavanjeMp
     */
    public function setKontaktTelefon($kontaktTelefon)
    {
      $this->kontaktTelefon = $kontaktTelefon;
      return $this;
    }

    /**
     * @return string
     */
    public function getLogname()
    {
      return $this->logname;
    }

    /**
     * @param string $logname
     * @return nalogZaIzdavanjeMp
     */
    public function setLogname($logname)
    {
      $this->logname = $logname;
      return $this;
    }

    /**
     * @return string
     */
    public function getMestoIsporuke()
    {
      return $this->mestoIsporuke;
    }

    /**
     * @param string $mestoIsporuke
     * @return nalogZaIzdavanjeMp
     */
    public function setMestoIsporuke($mestoIsporuke)
    {
      $this->mestoIsporuke = $mestoIsporuke;
      return $this;
    }

    /**
     * @return string
     */
    public function getNapomena()
    {
      return $this->napomena;
    }

    /**
     * @param string $napomena
     * @return nalogZaIzdavanjeMp
     */
    public function setNapomena($napomena)
    {
      $this->napomena = $napomena;
      return $this;
    }

    /**
     * @return string
     */
    public function getOznakaCenovnika()
    {
      return $this->oznakaCenovnika;
    }

    /**
     * @param string $oznakaCenovnika
     * @return nalogZaIzdavanjeMp
     */
    public function setOznakaCenovnika($oznakaCenovnika)
    {
      $this->oznakaCenovnika = $oznakaCenovnika;
      return $this;
    }

    /**
     * @return string
     */
    public function getOznakaDokumenta()
    {
      return $this->oznakaDokumenta;
    }

    /**
     * @param string $oznakaDokumenta
     * @return nalogZaIzdavanjeMp
     */
    public function setOznakaDokumenta($oznakaDokumenta)
    {
      $this->oznakaDokumenta = $oznakaDokumenta;
      return $this;
    }

    /**
     * @return string
     */
    public function getOznakaKase()
    {
      return $this->oznakaKase;
    }

    /**
     * @param string $oznakaKase
     * @return nalogZaIzdavanjeMp
     */
    public function setOznakaKase($oznakaKase)
    {
      $this->oznakaKase = $oznakaKase;
      return $this;
    }

    /**
     * @return string
     */
    public function getPreImeKupca()
    {
      return $this->preImeKupca;
    }

    /**
     * @param string $preImeKupca
     * @return nalogZaIzdavanjeMp
     */
    public function setPreImeKupca($preImeKupca)
    {
      $this->preImeKupca = $preImeKupca;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraMagacina()
    {
      return $this->sifraMagacina;
    }

    /**
     * @param string $sifraMagacina
     * @return nalogZaIzdavanjeMp
     */
    public function setSifraMagacina($sifraMagacina)
    {
      $this->sifraMagacina = $sifraMagacina;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraObjekta()
    {
      return $this->sifraObjekta;
    }

    /**
     * @param string $sifraObjekta
     * @return nalogZaIzdavanjeMp
     */
    public function setSifraObjekta($sifraObjekta)
    {
      $this->sifraObjekta = $sifraObjekta;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraOrganizacioneJedinice()
    {
      return $this->sifraOrganizacioneJedinice;
    }

    /**
     * @param string $sifraOrganizacioneJedinice
     * @return nalogZaIzdavanjeMp
     */
    public function setSifraOrganizacioneJedinice($sifraOrganizacioneJedinice)
    {
      $this->sifraOrganizacioneJedinice = $sifraOrganizacioneJedinice;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraPartnera()
    {
      return $this->sifraPartnera;
    }

    /**
     * @param string $sifraPartnera
     * @return nalogZaIzdavanjeMp
     */
    public function setSifraPartnera($sifraPartnera)
    {
      $this->sifraPartnera = $sifraPartnera;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraPartneraKorisnik()
    {
      return $this->sifraPartneraKorisnik;
    }

    /**
     * @param string $sifraPartneraKorisnik
     * @return nalogZaIzdavanjeMp
     */
    public function setSifraPartneraKorisnik($sifraPartneraKorisnik)
    {
      $this->sifraPartneraKorisnik = $sifraPartneraKorisnik;
      return $this;
    }

    /**
     * @return int
     */
    public function getSmena()
    {
      return $this->smena;
    }

    /**
     * @param int $smena
     * @return nalogZaIzdavanjeMp
     */
    public function setSmena($smena)
    {
      $this->smena = $smena;
      return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
      return $this->status;
    }

    /**
     * @param string $status
     * @return nalogZaIzdavanjeMp
     */
    public function setStatus($status)
    {
      $this->status = $status;
      return $this;
    }

    /**
     * @return nalogZaIzdavanjeMpStavka[]
     */
    public function getStavke()
    {
      return $this->stavke;
    }

    /**
     * @param nalogZaIzdavanjeMpStavka[] $stavke
     * @return nalogZaIzdavanjeMp
     */
    public function setStavke(array $stavke = null)
    {
      $this->stavke = $stavke;
      return $this;
    }

    /**
     * @return string
     */
    public function getStorno()
    {
      return $this->storno;
    }

    /**
     * @param string $storno
     * @return nalogZaIzdavanjeMp
     */
    public function setStorno($storno)
    {
      $this->storno = $storno;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getVreme()
    {
      if ($this->vreme == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->vreme);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $vreme
     * @return nalogZaIzdavanjeMp
     */
    public function setVreme(\DateTime $vreme = null)
    {
      if ($vreme == null) {
       $this->vreme = null;
      } else {
        $this->vreme = $vreme->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getZki()
    {
      return $this->zki;
    }

    /**
     * @param string $zki
     * @return nalogZaIzdavanjeMp
     */
    public function setZki($zki)
    {
      $this->zki = $zki;
      return $this;
    }

    /**
     * @return string
     */
    public function getOznakaInterneKartice()
    {
      return $this->oznakaInterneKartice;
    }

    /**
     * @param string $oznakaInterneKartice
     * @return nalogZaIzdavanjeMp
     */
    public function setOznakaInterneKartice($oznakaInterneKartice)
    {
      $this->oznakaInterneKartice = $oznakaInterneKartice;
      return $this;
    }

}
