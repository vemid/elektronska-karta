<?php

class dodajOtpremnicuUMaloprodajuResponseCustom
{

    /**
     * @var otpremnicaUMaloprodajuResponse $return
     */
    protected $return = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return otpremnicaUMaloprodajuResponse
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param otpremnicaUMaloprodajuResponse $return
     * @return dodajOtpremnicuUMaloprodajuResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
