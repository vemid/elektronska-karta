<?php

class nalogZaIzdavanjeMpStavkaUsluga
{

    /**
     * @var float $kolicina
     */
    protected $kolicina = null;

    /**
     * @var float $osnovnaCenabezPoreza
     */
    protected $osnovnaCenabezPoreza = null;

    /**
     * @var float $prodajnaCenaSaPopustom
     */
    protected $prodajnaCenaSaPopustom = null;

    /**
     * @var float $prodajnaCenaSaPorezom
     */
    protected $prodajnaCenaSaPorezom = null;

    /**
     * @var int $redniBroj
     */
    protected $redniBroj = null;

    /**
     * @var string $sifraTarifneGrupe
     */
    protected $sifraTarifneGrupe = null;

    /**
     * @var string $sifraUsluge
     */
    protected $sifraUsluge = null;

    /**
     * @var float $stopaPopusta
     */
    protected $stopaPopusta = null;

    /**
     * @var float $stopaPoreza
     */
    protected $stopaPoreza = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getKolicina()
    {
      return $this->kolicina;
    }

    /**
     * @param float $kolicina
     * @return nalogZaIzdavanjeMpStavkaUsluga
     */
    public function setKolicina($kolicina)
    {
      $this->kolicina = $kolicina;
      return $this;
    }

    /**
     * @return float
     */
    public function getOsnovnaCenabezPoreza()
    {
      return $this->osnovnaCenabezPoreza;
    }

    /**
     * @param float $osnovnaCenabezPoreza
     * @return nalogZaIzdavanjeMpStavkaUsluga
     */
    public function setOsnovnaCenabezPoreza($osnovnaCenabezPoreza)
    {
      $this->osnovnaCenabezPoreza = $osnovnaCenabezPoreza;
      return $this;
    }

    /**
     * @return float
     */
    public function getProdajnaCenaSaPopustom()
    {
      return $this->prodajnaCenaSaPopustom;
    }

    /**
     * @param float $prodajnaCenaSaPopustom
     * @return nalogZaIzdavanjeMpStavkaUsluga
     */
    public function setProdajnaCenaSaPopustom($prodajnaCenaSaPopustom)
    {
      $this->prodajnaCenaSaPopustom = $prodajnaCenaSaPopustom;
      return $this;
    }

    /**
     * @return float
     */
    public function getProdajnaCenaSaPorezom()
    {
      return $this->prodajnaCenaSaPorezom;
    }

    /**
     * @param float $prodajnaCenaSaPorezom
     * @return nalogZaIzdavanjeMpStavkaUsluga
     */
    public function setProdajnaCenaSaPorezom($prodajnaCenaSaPorezom)
    {
      $this->prodajnaCenaSaPorezom = $prodajnaCenaSaPorezom;
      return $this;
    }

    /**
     * @return int
     */
    public function getRedniBroj()
    {
      return $this->redniBroj;
    }

    /**
     * @param int $redniBroj
     * @return nalogZaIzdavanjeMpStavkaUsluga
     */
    public function setRedniBroj($redniBroj)
    {
      $this->redniBroj = $redniBroj;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraTarifneGrupe()
    {
      return $this->sifraTarifneGrupe;
    }

    /**
     * @param string $sifraTarifneGrupe
     * @return nalogZaIzdavanjeMpStavkaUsluga
     */
    public function setSifraTarifneGrupe($sifraTarifneGrupe)
    {
      $this->sifraTarifneGrupe = $sifraTarifneGrupe;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraUsluge()
    {
      return $this->sifraUsluge;
    }

    /**
     * @param string $sifraUsluge
     * @return nalogZaIzdavanjeMpStavkaUsluga
     */
    public function setSifraUsluge($sifraUsluge)
    {
      $this->sifraUsluge = $sifraUsluge;
      return $this;
    }

    /**
     * @return float
     */
    public function getStopaPopusta()
    {
      return $this->stopaPopusta;
    }

    /**
     * @param float $stopaPopusta
     * @return nalogZaIzdavanjeMpStavkaUsluga
     */
    public function setStopaPopusta($stopaPopusta)
    {
      $this->stopaPopusta = $stopaPopusta;
      return $this;
    }

    /**
     * @return float
     */
    public function getStopaPoreza()
    {
      return $this->stopaPoreza;
    }

    /**
     * @param float $stopaPoreza
     * @return nalogZaIzdavanjeMpStavkaUsluga
     */
    public function setStopaPoreza($stopaPoreza)
    {
      $this->stopaPoreza = $stopaPoreza;
      return $this;
    }

}
