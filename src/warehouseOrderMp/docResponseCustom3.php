0<?php

class docResponseCustom3
{

    /**
     * @var string $errorMessage
     */
    protected $errorMessage = null;

    /**
     * @var int $respResultCount
     */
    protected $respResultCount = null;

    /**
     * @var boolean $responseResult
     */
    protected $responseResult = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     * @return docResponse
     */
    public function setErrorMessage($errorMessage)
    {
      $this->errorMessage = $errorMessage;
      return $this;
    }

    /**
     * @return int
     */
    public function getRespResultCount()
    {
      return $this->respResultCount;
    }

    /**
     * @param int $respResultCount
     * @return docResponse
     */
    public function setRespResultCount($respResultCount)
    {
      $this->respResultCount = $respResultCount;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getResponseResult()
    {
      return $this->responseResult;
    }

    /**
     * @param boolean $responseResult
     * @return docResponse
     */
    public function setResponseResult($responseResult)
    {
      $this->responseResult = $responseResult;
      return $this;
    }

}
