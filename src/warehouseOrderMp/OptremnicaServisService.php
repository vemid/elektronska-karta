<?php

class OptremnicaServisService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'dodajOtpremnicuUMaloprodaju' => '\\dodajOtpremnicuUMaloprodaju',
      'otpremnicaUMaloprodaju' => '\\otpremnicaUMaloprodaju',
      'otpremnicaUMaloprodajuStavka' => '\\otpremnicaUMaloprodajuStavka',
      'dodajOtpremnicuUMaloprodajuResponse' => '\\dodajOtpremnicuUMaloprodajuResponse',
      'otpremnicaUMaloprodajuResponse' => '\\otpremnicaUMaloprodajuResponse',
      'docResponse' => '\\docResponseCustom3',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'http://192.168.100.210:8080/ServisMisWeb/services/OptremnicaServisPort?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param dodajOtpremnicuUMaloprodaju $parameters
     * @return dodajOtpremnicuUMaloprodajuResponse
     */
    public function dodajOtpremnicuUMaloprodaju(dodajOtpremnicuUMaloprodaju $parameters)
    {
      return $this->__soapCall('dodajOtpremnicuUMaloprodaju', array($parameters));
    }

}
