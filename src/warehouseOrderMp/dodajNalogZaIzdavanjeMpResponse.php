<?php

class dodajNalogZaIzdavanjeMpResponse extends docResponseCustom2
{

    /**
     * @var nalogZaIzdavanjeMpResponse $return
     */
    protected $return = null;

    
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return nalogZaIzdavanjeMpResponse
     */
    public function getReturn()
    {
      return $this->return;
    }

    /**
     * @param nalogZaIzdavanjeMpResponse $return
     * @return dodajNalogZaIzdavanjeMpResponse
     */
    public function setReturn($return)
    {
      $this->return = $return;
      return $this;
    }

}
