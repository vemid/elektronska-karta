<?php

class nalogZaIzdavanjeMpStavka
{

    /**
     * @var float $kolicina
     */
    protected $kolicina = null;

    /**
     * @var float $osnovnaCenabezPoreza
     */
    protected $osnovnaCenabezPoreza = null;

    /**
     * @var float $prodajnaCenaSaPopustom
     */
    protected $prodajnaCenaSaPopustom = null;

    /**
     * @var float $prodajnaCenaSaPorezom
     */
    protected $prodajnaCenaSaPorezom = null;

    /**
     * @var int $redniBroj
     */
    protected $redniBroj = null;

    /**
     * @var string $sifraOblezja
     */
    protected $sifraOblezja = null;

    /**
     * @var string $sifraRobe
     */
    protected $sifraRobe = null;

    /**
     * @var string $sifraTarifneGrupe
     */
    protected $sifraTarifneGrupe = null;

    /**
     * @var float $stopaPopusta
     */
    protected $stopaPopusta = null;

    /**
     * @var float $stopaPoreza
     */
    protected $stopaPoreza = null;

    /**
     * @var string $zonaMagacina
     */
    protected $zonaMagacina = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getKolicina()
    {
      return $this->kolicina;
    }

    /**
     * @param float $kolicina
     * @return nalogZaIzdavanjeMpStavka
     */
    public function setKolicina($kolicina)
    {
      $this->kolicina = $kolicina;
      return $this;
    }

    /**
     * @return float
     */
    public function getOsnovnaCenabezPoreza()
    {
      return $this->osnovnaCenabezPoreza;
    }

    /**
     * @param float $osnovnaCenabezPoreza
     * @return nalogZaIzdavanjeMpStavka
     */
    public function setOsnovnaCenabezPoreza($osnovnaCenabezPoreza)
    {
      $this->osnovnaCenabezPoreza = $osnovnaCenabezPoreza;
      return $this;
    }

    /**
     * @return float
     */
    public function getProdajnaCenaSaPopustom()
    {
      return $this->prodajnaCenaSaPopustom;
    }

    /**
     * @param float $prodajnaCenaSaPopustom
     * @return nalogZaIzdavanjeMpStavka
     */
    public function setProdajnaCenaSaPopustom($prodajnaCenaSaPopustom)
    {
      $this->prodajnaCenaSaPopustom = $prodajnaCenaSaPopustom;
      return $this;
    }

    /**
     * @return float
     */
    public function getProdajnaCenaSaPorezom()
    {
      return $this->prodajnaCenaSaPorezom;
    }

    /**
     * @param float $prodajnaCenaSaPorezom
     * @return nalogZaIzdavanjeMpStavka
     */
    public function setProdajnaCenaSaPorezom($prodajnaCenaSaPorezom)
    {
      $this->prodajnaCenaSaPorezom = $prodajnaCenaSaPorezom;
      return $this;
    }

    /**
     * @return int
     */
    public function getRedniBroj()
    {
      return $this->redniBroj;
    }

    /**
     * @param int $redniBroj
     * @return nalogZaIzdavanjeMpStavka
     */
    public function setRedniBroj($redniBroj)
    {
      $this->redniBroj = $redniBroj;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraOblezja()
    {
      return $this->sifraOblezja;
    }

    /**
     * @param string $sifraOblezja
     * @return nalogZaIzdavanjeMpStavka
     */
    public function setSifraOblezja($sifraOblezja)
    {
      $this->sifraOblezja = $sifraOblezja;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraRobe()
    {
      return $this->sifraRobe;
    }

    /**
     * @param string $sifraRobe
     * @return nalogZaIzdavanjeMpStavka
     */
    public function setSifraRobe($sifraRobe)
    {
      $this->sifraRobe = $sifraRobe;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraTarifneGrupe()
    {
      return $this->sifraTarifneGrupe;
    }

    /**
     * @param string $sifraTarifneGrupe
     * @return nalogZaIzdavanjeMpStavka
     */
    public function setSifraTarifneGrupe($sifraTarifneGrupe)
    {
      $this->sifraTarifneGrupe = $sifraTarifneGrupe;
      return $this;
    }

    /**
     * @return float
     */
    public function getStopaPopusta()
    {
      return $this->stopaPopusta;
    }

    /**
     * @param float $stopaPopusta
     * @return nalogZaIzdavanjeMpStavka
     */
    public function setStopaPopusta($stopaPopusta)
    {
      $this->stopaPopusta = $stopaPopusta;
      return $this;
    }

    /**
     * @return float
     */
    public function getStopaPoreza()
    {
      return $this->stopaPoreza;
    }

    /**
     * @param float $stopaPoreza
     * @return nalogZaIzdavanjeMpStavka
     */
    public function setStopaPoreza($stopaPoreza)
    {
      $this->stopaPoreza = $stopaPoreza;
      return $this;
    }

    /**
     * @return string
     */
    public function getZonaMagacina()
    {
      return $this->zonaMagacina;
    }

    /**
     * @param string $zonaMagacina
     * @return nalogZaIzdavanjeMpStavka
     */
    public function setZonaMagacina($zonaMagacina)
    {
      $this->zonaMagacina = $zonaMagacina;
      return $this;
    }

}
