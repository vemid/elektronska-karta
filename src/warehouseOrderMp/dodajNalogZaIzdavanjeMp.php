<?php

class dodajNalogZaIzdavanjeMp
{

    /**
     * @var nalogZaIzdavanjeMp[] $nalozi
     */
    protected $nalozi = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return nalogZaIzdavanjeMp[]
     */
    public function getNalozi()
    {
      return $this->nalozi;
    }

    /**
     * @param nalogZaIzdavanjeMp[] $nalozi
     * @return dodajNalogZaIzdavanjeMp
     */
    public function setNalozi(array $nalozi = null)
    {
      $this->nalozi = $nalozi;
      return $this;
    }

}
