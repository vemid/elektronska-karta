<?php

class dodajOtpremnicuUMaloprodajuCustom
{

    /**
     * @var otpremnicaUMaloprodaju $otpremnicaUMaloprodaju
     */
    protected $otpremnicaUMaloprodaju = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return otpremnicaUMaloprodaju
     */
    public function getOtpremnicaUMaloprodaju()
    {
      return $this->otpremnicaUMaloprodaju;
    }

    /**
     * @param otpremnicaUMaloprodaju $otpremnicaUMaloprodaju
     * @return dodajOtpremnicuUMaloprodaju
     */
    public function setOtpremnicaUMaloprodaju($otpremnicaUMaloprodaju)
    {
      $this->otpremnicaUMaloprodaju = $otpremnicaUMaloprodaju;
      return $this;
    }

}
