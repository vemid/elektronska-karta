<?php

class nalogZaIzdavanjeMpResponse extends docResponseCustom
{

    /**
     * @var nalogZaIzdavanjeMp[] $nalozi
     */
    protected $nalozi = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return nalogZaIzdavanjeMp[]
     */
    public function getNalozi()
    {
      return $this->nalozi;
    }

    /**
     * @param nalogZaIzdavanjeMp[] $nalozi
     * @return nalogZaIzdavanjeMpResponse
     */
    public function setNalozi(array $nalozi = null)
    {
      $this->nalozi = $nalozi;
      return $this;
    }

}
