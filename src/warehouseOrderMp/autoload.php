<?php


 function autoload_366238c8395856b7d705c5f430d46522($class)
{
    $classes = array(
        'docResponseCustom2' => __DIR__ .'/docResponseCustom2.php',
        'docResponseCustom3' => __DIR__ .'/docResponseCustom3.php',
        'dodajNalogZaIzdavanjeMp' => __DIR__ .'/dodajNalogZaIzdavanjeMp.php',
        'dodajNalogZaIzdavanjeMpResponse' => __DIR__ .'/dodajNalogZaIzdavanjeMpResponse.php',
        'dodajOtpremnicuUMaloprodaju' => __DIR__ .'/dodajOtpremnicuUMaloprodaju.php',
        'dodajOtpremnicuUMaloprodajuCustom' => __DIR__ .'/dodajOtpremnicuUMaloprodajuCustom.php',
        'dodajOtpremnicuUMaloprodajuResponse' => __DIR__ .'/dodajOtpremnicuUMaloprodajuResponse.php',
        'dodajOtpremnicuUMaloprodajuResponseCustom' => __DIR__ .'/dodajOtpremnicuUMaloprodajuResponseCustom.php',
        'nalogZaIzdavanjeMp' => __DIR__ .'/nalogZaIzdavanjeMp.php',
        'nalogZaIzdavanjeMpResponse' => __DIR__ .'/nalogZaIzdavanjeMpResponse.php',
        'NalogZaIzdavanjeMpServisService' => __DIR__ .'/NalogZaIzdavanjeMpServisService.php',
        'nalogZaIzdavanjeMpStavka' => __DIR__ .'/nalogZaIzdavanjeMpStavka.php',
        'OptremnicaServisService' => __DIR__ .'/OptremnicaServisService.php',
        'OptremnicaServisServiceCustom' => __DIR__ .'/OptremnicaServisServiceCustom.php',
        'otpremnicaUMaloprodaju' => __DIR__ .'/otpremnicaUMaloprodaju.php',
        'otpremnicaUMaloprodajuCustom' => __DIR__ .'/otpremnicaUMaloprodajuCustom.php',
        'otpremnicaUMaloprodajuResponse' => __DIR__ .'/otpremnicaUMaloprodajuResponse.php',
        'otpremnicaUMaloprodajuResponseCustom' => __DIR__ .'/otpremnicaUMaloprodajuResponseCustom.php',
        'otpremnicaUMaloprodajuStavka' => __DIR__ .'/otpremnicaUMaloprodajuStavka.php',
        'otpremnicaUMaloprodajuStavkaCustom' => __DIR__ .'/otpremnicaUMaloprodajuStavkaCustom.php',
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_366238c8395856b7d705c5f430d46522');

// Do nothing. The rest is just leftovers from the code generation.
{
}
