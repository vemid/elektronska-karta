<?php

class otpremnicaUMaloprodajuStavkaCustom
{

    /**
     * @var float $akcijskaStopaRabata
     */
    protected $akcijskaStopaRabata = null;

    /**
     * @var float $brojPakovanja
     */
    protected $brojPakovanja = null;

    /**
     * @var float $cenaZalihe
     */
    protected $cenaZalihe = null;

    /**
     * @var float $iznosAkcize
     */
    protected $iznosAkcize = null;

    /**
     * @var float $iznosTakse
     */
    protected $iznosTakse = null;

    /**
     * @var float $kolicina
     */
    protected $kolicina = null;

    /**
     * @var float $kolicina1
     */
    protected $kolicina1 = null;

    /**
     * @var float $maloprodajnaMarza
     */
    protected $maloprodajnaMarza = null;

    /**
     * @var string $napomena
     */
    protected $napomena = null;

    /**
     * @var float $osnovnaCena
     */
    protected $osnovnaCena = null;

    /**
     * @var float $posebnaStopaRabata
     */
    protected $posebnaStopaRabata = null;

    /**
     * @var float $prodajnaCena
     */
    protected $prodajnaCena = null;

    /**
     * @var float $prodajnaCenaBezPoreza
     */
    protected $prodajnaCenaBezPoreza = null;

    /**
     * @var float $prodajnaCenaSaRabatom
     */
    protected $prodajnaCenaSaRabatom = null;

    /**
     * @var string $sifraObelezja
     */
    protected $sifraObelezja = null;

    /**
     * @var string $sifraOdeljka
     */
    protected $sifraOdeljka = null;

    /**
     * @var string $sifraPakovanja
     */
    protected $sifraPakovanja = null;

    /**
     * @var string $sifraRobe
     */
    protected $sifraRobe = null;

    /**
     * @var string $sifraTarifneGrupePoreza
     */
    protected $sifraTarifneGrupePoreza = null;

    /**
     * @var string $sifraTarifneGrupeTakse
     */
    protected $sifraTarifneGrupeTakse = null;

    /**
     * @var string $sifraZoneMagacina
     */
    protected $sifraZoneMagacina = null;

    /**
     * @var float $stopaPDV
     */
    protected $stopaPDV = null;

    /**
     * @var float $stopaRabata
     */
    protected $stopaRabata = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getAkcijskaStopaRabata()
    {
      return $this->akcijskaStopaRabata;
    }

    /**
     * @param float $akcijskaStopaRabata
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setAkcijskaStopaRabata($akcijskaStopaRabata)
    {
      $this->akcijskaStopaRabata = $akcijskaStopaRabata;
      return $this;
    }

    /**
     * @return float
     */
    public function getBrojPakovanja()
    {
      return $this->brojPakovanja;
    }

    /**
     * @param float $brojPakovanja
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setBrojPakovanja($brojPakovanja)
    {
      $this->brojPakovanja = $brojPakovanja;
      return $this;
    }

    /**
     * @return float
     */
    public function getCenaZalihe()
    {
      return $this->cenaZalihe;
    }

    /**
     * @param float $cenaZalihe
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setCenaZalihe($cenaZalihe)
    {
      $this->cenaZalihe = $cenaZalihe;
      return $this;
    }

    /**
     * @return float
     */
    public function getIznosAkcize()
    {
      return $this->iznosAkcize;
    }

    /**
     * @param float $iznosAkcize
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setIznosAkcize($iznosAkcize)
    {
      $this->iznosAkcize = $iznosAkcize;
      return $this;
    }

    /**
     * @return float
     */
    public function getIznosTakse()
    {
      return $this->iznosTakse;
    }

    /**
     * @param float $iznosTakse
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setIznosTakse($iznosTakse)
    {
      $this->iznosTakse = $iznosTakse;
      return $this;
    }

    /**
     * @return float
     */
    public function getKolicina()
    {
      return $this->kolicina;
    }

    /**
     * @param float $kolicina
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setKolicina($kolicina)
    {
      $this->kolicina = $kolicina;
      return $this;
    }

    /**
     * @return float
     */
    public function getKolicina1()
    {
      return $this->kolicina1;
    }

    /**
     * @param float $kolicina1
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setKolicina1($kolicina1)
    {
      $this->kolicina1 = $kolicina1;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaloprodajnaMarza()
    {
      return $this->maloprodajnaMarza;
    }

    /**
     * @param float $maloprodajnaMarza
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setMaloprodajnaMarza($maloprodajnaMarza)
    {
      $this->maloprodajnaMarza = $maloprodajnaMarza;
      return $this;
    }

    /**
     * @return string
     */
    public function getNapomena()
    {
      return $this->napomena;
    }

    /**
     * @param string $napomena
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setNapomena($napomena)
    {
      $this->napomena = $napomena;
      return $this;
    }

    /**
     * @return float
     */
    public function getOsnovnaCena()
    {
      return $this->osnovnaCena;
    }

    /**
     * @param float $osnovnaCena
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setOsnovnaCena($osnovnaCena)
    {
      $this->osnovnaCena = $osnovnaCena;
      return $this;
    }

    /**
     * @return float
     */
    public function getPosebnaStopaRabata()
    {
      return $this->posebnaStopaRabata;
    }

    /**
     * @param float $posebnaStopaRabata
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setPosebnaStopaRabata($posebnaStopaRabata)
    {
      $this->posebnaStopaRabata = $posebnaStopaRabata;
      return $this;
    }

    /**
     * @return float
     */
    public function getProdajnaCena()
    {
      return $this->prodajnaCena;
    }

    /**
     * @param float $prodajnaCena
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setProdajnaCena($prodajnaCena)
    {
      $this->prodajnaCena = $prodajnaCena;
      return $this;
    }

    /**
     * @return float
     */
    public function getProdajnaCenaBezPoreza()
    {
      return $this->prodajnaCenaBezPoreza;
    }

    /**
     * @param float $prodajnaCenaBezPoreza
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setProdajnaCenaBezPoreza($prodajnaCenaBezPoreza)
    {
      $this->prodajnaCenaBezPoreza = $prodajnaCenaBezPoreza;
      return $this;
    }

    /**
     * @return float
     */
    public function getProdajnaCenaSaRabatom()
    {
      return $this->prodajnaCenaSaRabatom;
    }

    /**
     * @param float $prodajnaCenaSaRabatom
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setProdajnaCenaSaRabatom($prodajnaCenaSaRabatom)
    {
      $this->prodajnaCenaSaRabatom = $prodajnaCenaSaRabatom;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraObelezja()
    {
      return $this->sifraObelezja;
    }

    /**
     * @param string $sifraObelezja
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setSifraObelezja($sifraObelezja)
    {
      $this->sifraObelezja = $sifraObelezja;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraOdeljka()
    {
      return $this->sifraOdeljka;
    }

    /**
     * @param string $sifraOdeljka
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setSifraOdeljka($sifraOdeljka)
    {
      $this->sifraOdeljka = $sifraOdeljka;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraPakovanja()
    {
      return $this->sifraPakovanja;
    }

    /**
     * @param string $sifraPakovanja
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setSifraPakovanja($sifraPakovanja)
    {
      $this->sifraPakovanja = $sifraPakovanja;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraRobe()
    {
      return $this->sifraRobe;
    }

    /**
     * @param string $sifraRobe
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setSifraRobe($sifraRobe)
    {
      $this->sifraRobe = $sifraRobe;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraTarifneGrupePoreza()
    {
      return $this->sifraTarifneGrupePoreza;
    }

    /**
     * @param string $sifraTarifneGrupePoreza
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setSifraTarifneGrupePoreza($sifraTarifneGrupePoreza)
    {
      $this->sifraTarifneGrupePoreza = $sifraTarifneGrupePoreza;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraTarifneGrupeTakse()
    {
      return $this->sifraTarifneGrupeTakse;
    }

    /**
     * @param string $sifraTarifneGrupeTakse
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setSifraTarifneGrupeTakse($sifraTarifneGrupeTakse)
    {
      $this->sifraTarifneGrupeTakse = $sifraTarifneGrupeTakse;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraZoneMagacina()
    {
      return $this->sifraZoneMagacina;
    }

    /**
     * @param string $sifraZoneMagacina
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setSifraZoneMagacina($sifraZoneMagacina)
    {
      $this->sifraZoneMagacina = $sifraZoneMagacina;
      return $this;
    }

    /**
     * @return float
     */
    public function getStopaPDV()
    {
      return $this->stopaPDV;
    }

    /**
     * @param float $stopaPDV
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setStopaPDV($stopaPDV)
    {
      $this->stopaPDV = $stopaPDV;
      return $this;
    }

    /**
     * @return float
     */
    public function getStopaRabata()
    {
      return $this->stopaRabata;
    }

    /**
     * @param float $stopaRabata
     * @return otpremnicaUMaloprodajuStavka
     */
    public function setStopaRabata($stopaRabata)
    {
      $this->stopaRabata = $stopaRabata;
      return $this;
    }

}
