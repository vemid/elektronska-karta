<?php

class NalogZaIzdavanjeMpServisService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'dodajNalogZaIzdavanjeMp' => '\\dodajNalogZaIzdavanjeMp',
      'nalogZaIzdavanjeMp' => '\\nalogZaIzdavanjeMp',
      'nalogZaIzdavanjeMpStavka' => '\\nalogZaIzdavanjeMpStavka',
      'dodajNalogZaIzdavanjeMpResponse' => '\\dodajNalogZaIzdavanjeMpResponse',
      'nalogZaIzdavanjeMpResponse' => '\\nalogZaIzdavanjeMpResponse',
      'docResponse' => '\\docResponseCustom2',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'http://192.168.100.210:8080/ServisMisWeb/services/NalogZaIzdavanjeMpServisPort?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param dodajNalogZaIzdavanjeMp $parameters
     * @return dodajNalogZaIzdavanjeMpResponse
     */
    public function dodajNalogZaIzdavanjeMp(dodajNalogZaIzdavanjeMp $parameters)
    {
      return $this->__soapCall('dodajNalogZaIzdavanjeMp', array($parameters));
    }

}
