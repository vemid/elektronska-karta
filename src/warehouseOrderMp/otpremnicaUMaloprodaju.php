<?php

class otpremnicaUMaloprodaju
{

    /**
     * @var \DateTime $datumDokumenta
     */
    protected $datumDokumenta = null;

    /**
     * @var string $logname
     */
    protected $logname = null;

    /**
     * @var string $napomena
     */
    protected $napomena = null;

    /**
     * @var string $oznakaCenovnika
     */
    protected $oznakaCenovnika = null;

    /**
     * @var string $oznakaDokumenta
     */
    protected $oznakaDokumenta = null;

    /**
     * @var string $sifraMagacina
     */
    protected $sifraMagacina = null;

    /**
     * @var string $sifraObjektaMaloprodaje
     */
    protected $sifraObjektaMaloprodaje = null;

    /**
     * @var otpremnicaUMaloprodajuStavka[] $stavke
     */
    protected $stavke = null;

    /**
     * @var string $storno
     */
    protected $storno = null;

    /**
     * @var string $vrstaKnjizenja
     */
    protected $vrstaKnjizenja = null;

    /**
     * @param string $vrstaKnjizenja
     */
    public function __construct($vrstaKnjizenja)
    {
      $this->vrstaKnjizenja = $vrstaKnjizenja;
    }

    /**
     * @return \DateTime
     */
    public function getDatumDokumenta()
    {
      if ($this->datumDokumenta == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->datumDokumenta);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $datumDokumenta
     * @return otpremnicaUMaloprodaju
     */
    public function setDatumDokumenta(\DateTime $datumDokumenta = null)
    {
      if ($datumDokumenta == null) {
       $this->datumDokumenta = null;
      } else {
        $this->datumDokumenta = $datumDokumenta->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getLogname()
    {
      return $this->logname;
    }

    /**
     * @param string $logname
     * @return otpremnicaUMaloprodaju
     */
    public function setLogname($logname)
    {
      $this->logname = $logname;
      return $this;
    }

    /**
     * @return string
     */
    public function getNapomena()
    {
      return $this->napomena;
    }

    /**
     * @param string $napomena
     * @return otpremnicaUMaloprodaju
     */
    public function setNapomena($napomena)
    {
      $this->napomena = $napomena;
      return $this;
    }

    /**
     * @return string
     */
    public function getOznakaCenovnika()
    {
      return $this->oznakaCenovnika;
    }

    /**
     * @param string $oznakaCenovnika
     * @return otpremnicaUMaloprodaju
     */
    public function setOznakaCenovnika($oznakaCenovnika)
    {
      $this->oznakaCenovnika = $oznakaCenovnika;
      return $this;
    }

    /**
     * @return string
     */
    public function getOznakaDokumenta()
    {
      return $this->oznakaDokumenta;
    }

    /**
     * @param string $oznakaDokumenta
     * @return otpremnicaUMaloprodaju
     */
    public function setOznakaDokumenta($oznakaDokumenta)
    {
      $this->oznakaDokumenta = $oznakaDokumenta;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraMagacina()
    {
      return $this->sifraMagacina;
    }

    /**
     * @param string $sifraMagacina
     * @return otpremnicaUMaloprodaju
     */
    public function setSifraMagacina($sifraMagacina)
    {
      $this->sifraMagacina = $sifraMagacina;
      return $this;
    }

    /**
     * @return string
     */
    public function getSifraObjektaMaloprodaje()
    {
      return $this->sifraObjektaMaloprodaje;
    }

    /**
     * @param string $sifraObjektaMaloprodaje
     * @return otpremnicaUMaloprodaju
     */
    public function setSifraObjektaMaloprodaje($sifraObjektaMaloprodaje)
    {
      $this->sifraObjektaMaloprodaje = $sifraObjektaMaloprodaje;
      return $this;
    }

    /**
     * @return otpremnicaUMaloprodajuStavka[]
     */
    public function getStavke()
    {
      return $this->stavke;
    }

    /**
     * @param otpremnicaUMaloprodajuStavka[] $stavke
     * @return otpremnicaUMaloprodaju
     */
    public function setStavke(array $stavke = null)
    {
      $this->stavke = $stavke;
      return $this;
    }

    /**
     * @return string
     */
    public function getStorno()
    {
      return $this->storno;
    }

    /**
     * @param string $storno
     * @return otpremnicaUMaloprodaju
     */
    public function setStorno($storno)
    {
      $this->storno = $storno;
      return $this;
    }

    /**
     * @return string
     */
    public function getVrstaKnjizenja()
    {
      return $this->vrstaKnjizenja;
    }

    /**
     * @param string $vrstaKnjizenja
     * @return otpremnicaUMaloprodaju
     */
    public function setVrstaKnjizenja($vrstaKnjizenja)
    {
      $this->vrstaKnjizenja = $vrstaKnjizenja;
      return $this;
    }

}
