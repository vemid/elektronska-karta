ALTER TABLE `cars` DROP `price_per_day`;
ALTER TABLE `cars` ADD `fuel` ENUM('DIESEL','GASOLINE','TNG','CNG','ELECTRIC')  NOT NULL  DEFAULT 'GASOLINE'  AFTER `year`;
ALTER TABLE `reservation` ADD `status` ENUM('PENDING','COMPLETE', 'FAILED')  NOT NULL  DEFAULT 'PENDING'  AFTER `car_id`;
ALTER TABLE `purchases` CHANGE `contract_number` `contract_number` VARCHAR(16)  CHARACTER SET utf8  COLLATE utf8_general_ci  NOT NULL  DEFAULT '';
ALTER TABLE `car_details` CHANGE `installment_date` `day_of_installment` INT(11)  NULL  DEFAULT NULL;
RENAME TABLE `prices` TO `car_prices`;

/*2016-11-23*/
ALTER TABLE `clients` ADD `marker` VARCHAR(32)  NULL  DEFAULT NULL  AFTER `type_of_client`;

/*2016-11-26*/
RENAME TABLE `car_services` TO `car_costs`;
ALTER TABLE `car_costs` CHANGE `type_of_service` `type_of_cost` ENUM('SMALL_SERVICE','FULL_SERVICE','TYRES','REGISTRATION','UNEXPECTED')  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT NULL;
ALTER TABLE `car_costs` CHANGE `type_of_cost` `type_of_cost` ENUM('SMALL_SERVICE','FULL_SERVICE','TYRES','REGISTRATION','UNEXPECTED','KASKO')  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT NULL;
ALTER TABLE `car_costs` ADD `date` DATE  NULL  AFTER `note`;
ALTER TABLE `car_details` CHANGE `purchase_date` `purchase_date` DATE  NULL;

/*2016-11-27*/
ALTER TABLE purchases ADD (prepayment DECIMAL(10,2) UNSIGNED);
ALTER TABLE purchases ADD (prepayment_type ENUM('CACHE','CARD'));
ALTER TABLE purchases ADD (invoice VARCHAR(32));
ALTER TABLE reservation ADD (pre_invoice VARCHAR(32));
ALTER TABLE `clients` ADD `discount` INT AFTER `passport_valid`;
ALTER TABLE purchases ADD (invoice VARCHAR(32));

/* 2016-11-28*/
ALTER TABLE `car_details` ADD `mileage_of_next_service` INT(11)  UNSIGNED  NULL  DEFAULT NULL  AFTER `car_earnings`;
ALTER TABLE `car_details` ADD `current_mileage` INT(64)  NULL  DEFAULT NULL  AFTER `car_earnings`;
ALTER TABLE `cars` DROP `current_km`;

/* 2016-01-19*/
ALTER TABLE `purchases` ADD `discount` INT(11)  UNSIGNED  NULL  DEFAULT NULL  AFTER `price_per_day`;

/* 2017-01-20*/
ALTER TABLE `clients` ADD `company_name` VARCHAR(256);
ALTER TABLE `clients` ADD `company_address` VARCHAR(256);
ALTER TABLE `clients` ADD `company_pib` INT(32);
ALTER TABLE `clients` ADD `company_city` VARCHAR(256) after `company_address`;
ALTER TABLE `clients` ADD `company_postal_code` INT(32) UNSIGNED  NULL  DEFAULT NULL  AFTER `company_city`;
ALTER TABLE `clients` ADD `blacklisted` BOOL NOT NULL;
ALTER TABLE `purchases` ADD `payment_type` enum('CACHE','TRANSFER')  AFTER `prepayment_type`;
ALTER TABLE `purchases` ADD `second_driver_name` VARCHAR(64);
ALTER TABLE `purchases` ADD `second_driver_address` VARCHAR(64);
ALTER TABLE `purchases` ADD `second_driver_city` VARCHAR(64);
ALTER TABLE `purchases` ADD `second_driver_jmbg` VARCHAR(64);
ALTER TABLE `purchases` ADD `second_driver_phone` VARCHAR(64);
ALTER TABLE `purchases` ADD `second_driver_licence` VARCHAR(64);
ALTER TABLE `purchases` ADD `second_driver_id` VARCHAR(64);
ALTER TABLE `purchases` ADD `second_driver_passport` VARCHAR(64);
ALTER TABLE `purchases` ADD `border_crossing` BOOL NOT NULL;
ALTER TABLE `purchases` ADD `damage` VARCHAR(64);
ALTER TABLE `car_details` ADD `damage` VARCHAR(64);
ALTER TABLE `car_details` ADD `damage_note` text;
ALTER TABLE `purchases` ADD `child_seat` bool;
ALTER TABLE `purchases` ADD `booster_seat` bool;
ALTER TABLE `purchases` ADD `gps` bool;
ALTER TABLE `purchases` ADD `wi_fi` bool;
ALTER TABLE `purchases` ADD `chains` bool;

ALTER TABLE purchases MODIFY COLUMN child_seat bool DEFAULT '0';
ALTER TABLE purchases MODIFY COLUMN booster_seat bool DEFAULT '0';
ALTER TABLE purchases MODIFY COLUMN gps bool DEFAULT '0';
ALTER TABLE purchases MODIFY COLUMN wi_fi bool DEFAULT '0';
ALTER TABLE purchases MODIFY COLUMN chains bool DEFAULT '0';

/* 2017-01-24*/

CREATE TABLE invoices (id int NOT NULL AUTO_INCREMENT, invoice_number int NOT NULL, type enum('PREPAYMENT','INVOICE'), date date, fiscal varchar(32), payment_type enum('TRANSFER','CACHE'), PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE invoice_details (id int NOT NULL AUTO_INCREMENT, invoice_id int(10) unsigned NOT NULL, text varchar(256) COLLATE utf8mb4_general_ci, price float(10,2), PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE cars ADD (kasko_ending DATE);
ALTER TABLE cars ADD (kasko_ending DATE);
ALTER TABLE `clients` ADD `date_of_birth` date  NULL  DEFAULT NULL  AFTER `jmbg`;
ALTER TABLE clients MODIFY COLUMN jmbg VARCHAR(16);
ALTER TABLE clients MODIFY COLUMN driver_licence VARCHAR(64);
ALTER TABLE clients MODIFY COLUMN driver_licence_state VARCHAR(64);
ALTER TABLE clients MODIFY COLUMN driver_licence_valid DATE;

/* 2017-01-27 */
RENAME TABLE `invoice_details` TO `invoice_items`;

/* 2017-01-29 */
ALTER TABLE cars ADD `tire_size` VARCHAR(64);
ALTER TABLE cars ADD `car_size` VARCHAR(64);
ALTER TABLE cars ADD `trunk_size` VARCHAR(64);
ALTER TABLE cars ADD `consumption` DECIMAL(10,2);
ALTER TABLE cars ADD `chasis` VARCHAR(64);
ALTER TABLE cars ADD `engine_number` VARCHAR(64);
ALTER TABLE `car_costs` ADD `current_mileage` INT(64)  NULL  DEFAULT NULL  AFTER `type_of_cost`
ALTER TABLE `invoices` CHANGE `invoice_number` `invoice_number` VARCHAR(64)  NOT NULL  DEFAULT '';
ALTER TABLE `cars` ADD `type_of_car` ENUM('TRAVEL','TRANSPORT')  CHARACTER SET utf8  COLLATE utf8_general_ci  NULL  DEFAULT NULL;
ALTER TABLE `invoices` ADD `client_id` int(11) UNSIGNED NOT NULL after `type`;
ALTER TABLE `invoice_items` ADD `qty` int(11) UNSIGNED NOT NULL after `price`;

/* 2017.02.05 */
ALTER TABLE `reservation` CHANGE `purchase_date_from` `purchase_date_from` DATETIME  NOT NULL;
ALTER TABLE `reservation` CHANGE `purchase_date_to` `purchase_date_to` DATETIME  NOT NULL;

/* 2017-06-20 */
ALTER TABLE `car_risk_insurances` ADD `due_day` INT(11)  NULL  DEFAULT NULL  AFTER `end_date`;

