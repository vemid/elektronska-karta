/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table car_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `car_details`;

CREATE TABLE `car_details` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `car_id` int(11) unsigned NOT NULL,
  `purchase_date` date NOT NULL,
  `installment` decimal(10,2) unsigned DEFAULT NULL,
  `installment_date` date DEFAULT NULL,
  `car_earnings` decimal(10,2) unsigned DEFAULT NULL,
  `note` text,
  PRIMARY KEY (`id`),
  KEY `car_details_fk1` (`car_id`),
  CONSTRAINT `fk_car_details_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table car_installments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `car_installments`;

CREATE TABLE `car_installments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `car_id` int(11) unsigned NOT NULL,
  `date_installment` date NOT NULL,
  `amount_installment` decimal(10,2) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `car_id` (`car_id`),
  CONSTRAINT `fk_car_installments_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table car_services
# ------------------------------------------------------------

DROP TABLE IF EXISTS `car_services`;

CREATE TABLE `car_services` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `car_id` int(11) unsigned NOT NULL,
  `type_of_service` enum('SMALL_SERVICE','FULL_SERVICE','TYRES','REGISTRATION','UNEXPECTED') DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `note` text,
  PRIMARY KEY (`id`),
  KEY `car_servis_fk1` (`car_id`),
  CONSTRAINT `fk_car_services_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table cars
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cars`;

CREATE TABLE `cars` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle` varchar(64) NOT NULL,
  `plate` varchar(64) NOT NULL,
  `year` year(4) NOT NULL,
  `price_per_day` decimal(10,2) NOT NULL,
  `current_km` int(64) NOT NULL,
  `registration_ending` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `address` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `state` varchar(64) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `jmbg` varchar(16) NOT NULL DEFAULT '',
  `driver_licence` varchar(64) NOT NULL,
  `driver_licence_state` varchar(64) NOT NULL,
  `driver_licence_valid` date NOT NULL,
  `id_card` varchar(64) DEFAULT NULL,
  `id_card_state` varchar(64) DEFAULT NULL,
  `id_card_valid` date DEFAULT NULL,
  `passport` varchar(64) DEFAULT NULL,
  `passport_state` varchar(64) DEFAULT NULL,
  `passport_valid` date DEFAULT NULL,
  `type_of_client` enum('INDIVIDUAL','LEGAL') NOT NULL DEFAULT 'INDIVIDUAL',
  `note` text,
  `date_created` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table prices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prices`;

CREATE TABLE `prices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `car_id` int(11) unsigned NOT NULL,
  `amount_days` int(11) unsigned DEFAULT NULL,
  `price_per_day` decimal(10,2) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prices_fk1` (`car_id`),
  CONSTRAINT `fk_prices_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table purchases
# ------------------------------------------------------------

DROP TABLE IF EXISTS `purchases`;

CREATE TABLE `purchases` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) unsigned NOT NULL,
  `car_id` int(11) unsigned NOT NULL,
  `reservation_id` int(11) unsigned DEFAULT NULL,
  `starting_datetime` datetime NOT NULL,
  `ending_datetime` datetime NOT NULL,
  `starting_place` varchar(64) DEFAULT NULL,
  `ending_place` varchar(64) DEFAULT NULL,
  `price_per_day` decimal(10,2) unsigned NOT NULL,
  `advance_amount` decimal(10,2) unsigned DEFAULT NULL,
  `contract_number` int(64) NOT NULL,
  `starting_km` int(11) NOT NULL,
  `ending_km` int(11) DEFAULT NULL,
  `outstanding_amount` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) unsigned NOT NULL,
  `total_paid` decimal(10,2) unsigned NOT NULL,
  `note` text,
  PRIMARY KEY (`id`),
  KEY `purchases_fk1` (`client_id`),
  KEY `purchases_fk2` (`reservation_id`),
  KEY `purchases_fk3` (`car_id`),
  CONSTRAINT `fk_purchases_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`),
  CONSTRAINT `fk_purchases_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  CONSTRAINT `fk_purchases_reservation_id` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table reservation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `reservation`;

CREATE TABLE `reservation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) unsigned NOT NULL,
  `car_id` int(11) unsigned NOT NULL,
  `purchase_date_from` date NOT NULL,
  `purchase_date_to` date NOT NULL,
  `price_per_day` decimal(10,2) unsigned DEFAULT NULL,
  `advance_amount` decimal(10,2) unsigned DEFAULT NULL,
  `note` text,
  PRIMARY KEY (`id`),
  KEY `reservation_fk1` (`client_id`),
  KEY `reservation_fk2` (`car_id`),
  CONSTRAINT `fk_reservations_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`),
  CONSTRAINT `fk_reservations_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `avatar` varchar(20) DEFAULT NULL,
  `gender` enum('MALE','FEMALE') DEFAULT NULL,
  `role` enum('MEMBER','ADMIN') DEFAULT NULL,
  `zone_id` int(11) unsigned DEFAULT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `last_ip` varchar(20) DEFAULT NULL,
  `registered_datetime` datetime DEFAULT NULL,
  `last_visit_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `zone_id` (`zone_id`),
  CONSTRAINT `users__zones` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `car_risk_insurances`;

CREATE TABLE `car_risk_insurances` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `amount` decimal(9,2) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `paid_in_full` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;


/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
