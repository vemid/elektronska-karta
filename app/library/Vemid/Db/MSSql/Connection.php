<?php


namespace Vemid\Db\MSSql;
use Phalcon\Config;
use Phalcon\Mvc\User\Component;
use Vemid\Error\Exception;

/**
 * Class Connection
 *
 * @package Vemid\Db\MSSql
 * @author Vemid
 */
class Connection extends Component
{
    /** @var array */
    protected $config = [];

    /**
     * Create a new MailerManager component using $config for configuring
     *
     * @param array $config
     */
    public function __construct($config)
    {
        $this->configure($config);
    }

    /**
     * Configure MailerManager class
     *
     * @param array $config
     */
    protected function configure($config)
    {
        $this->config = (array)$config;
    }

    /**
     * Get option config or the entire array of config, if the parameter $key is not specified.
     *
     * @param null $key
     * @param null $default
     * @return string|array
     */
    protected function getConfig($key = null, $default = null)
    {
        if ($key !== null) {
            if (isset($this->config[$key])) {
                return $this->config[$key];
            } else {
                return $default;
            }
        }

        return $this->config;
    }

    protected function registerConnection()
    {
        $config = $this->getConfig('MSSql');
        if (isset($config['host'])) {
            $server = $config['host'];
        }
        if (isset($config['options'])) {
            $options = (array)$config['options'];

            if(isset($options['Database']) && isset($options['Database']) && isset($options['Uid']) && isset($options['PWD']) && isset($options['CharacterSet'])) {
                $conn = sqlsrv_connect($server, $options);
            }
        }

        return $conn;


    }

    public function createConnection()
    {
        return $this->registerConnection();
    }

}