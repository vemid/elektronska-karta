<?php

namespace Vemid\Db\MSSql;

use Phalcon\Di;
use Vemid\Db\MSSql\Connection;

class Read
{
    /** @var $string */
    protected $string;

    /**
     * Builder constructor.
     * @param $string
     */
    public function __construct($string)
    {
        $this->string = $string;
    }

    /**
     * @return array
     */
    public function getData()
    {
        /** @var \Vemid\Application\Di $di */
        $di = Di::getDefault();
        /** @var Connection $connection */
        $connection = new Connection($di->getConfig());

        $results = [];
        $sql = $connection->createConnection();
        if($sql) {

            $getResults = sqlsrv_query($sql, $this->string);
            if ($getResults == FALSE)
                return $this->getErrors(sqlsrv_errors());


            while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
                $results[] = (object)$row;
            }

        }
        return $results;

    }

    /**
     * @param array $errors
     * @return array $error
     */
    public function getErrors($errors)
    {
        return $errors[0]['message'];
    }
}