<?php

namespace Vemid\Measure;

/**
 * Class Distance
 *
 * @package Vemid\Measure
 * @author Vemid
 */
class Distance
{

    const UNIT_MILES = 'mi';
    const UNIT_KILOMETERS = 'km';

    protected $length;
    protected $unit;

    protected static $displayNames = [
        self::UNIT_MILES => 'miles',
        self::UNIT_KILOMETERS => 'kilometers',
    ];

    /**
     * @param float $length
     * @param string $unit
     * @throws \InvalidArgumentException
     */
    public function __construct($length, $unit = self::UNIT_KILOMETERS)
    {
        if (!in_array($unit, array(self::UNIT_MILES, self::UNIT_KILOMETERS))) {
            throw new \InvalidArgumentException('Distance unit is not valid: ' . $unit);
        }

        $this->length = floatval($length);
        $this->unit = $unit;
    }

    /**
     * @return float
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @return string
     */
    public function format()
    {
        return number_format($this->length, 2) . ' ' . self::$displayNames[$this->unit];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->format();
    }

    /**
     * @return float
     */
    public function toMiles()
    {
        return $this->convertDistance(self::UNIT_MILES);
    }

    /**
     * @return float
     */
    public function toKilometers()
    {
        return $this->convertDistance(self::UNIT_KILOMETERS);
    }

    /**
     * @param string $toUnit
     * @return float
     */
    protected function convertDistance($toUnit)
    {
        if ($toUnit === $this->unit) {
            return $this->length;
        }

        switch ($toUnit) {

            case self::UNIT_MILES :
                return $this->length / 1.609344;

            case self::UNIT_KILOMETERS :
                return $this->length * 1.609344;

            default :
                return $this->length;
        }
    }

}
