<?php

namespace Vemid\Measure;

/**
 * Class Money
 *
 * @package Vemid\Measure
 * @author Vemid
 */
class Money
{

    /** @var float */
    private $amount;

    /** @var \CurrencyInterface */
    private $currency;

    /**
     * @param float $amount
     * @param \CurrencyInterface $currency
     */
    public function __construct($amount, \CurrencyInterface $currency)
    {
        $this->amount = floatval($amount);
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return \CurrencyInterface
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->format();
    }

    /**
     * @return string
     */
    public function format()
    {
        return sprintf('%s%s',
            $this->getCurrency()->getSymbol(),
            $this->amount == round($this->amount) ? number_format($this->amount) : number_format($this->amount, 2)
        );
    }

    /**
     * Add a numeric amount in the same currency
     *
     * @param Money $money
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function addAmount(Money $money)
    {
        if (!$this->getCurrency()->isEqualToEntity($money)) {
            throw new \InvalidArgumentException('Currencies are not equal.');
        }

        $this->amount += $money->getAmount();

        return $this;
    }


    /**
     * Subtract a numeric amount in the same currency
     *
     * @param Money $amount
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function subAmount(Money $amount)
    {
        if (!$this->getCurrency()->isEqualToEntity($amount)) {
            throw new \InvalidArgumentException('Currencies are not equal.');
        }

        $this->amount -= $amount->getAmount();

        return $this;
    }

    /**
     * @param float|int $quantity
     * @return Money
     * @throws \InvalidArgumentException
     */
    public function multiply($quantity)
    {
        if (!is_numeric($quantity)) {
            throw new \InvalidArgumentException('You must specify a numeric argument $quantity.');
        }

        $this->amount = $quantity * $this->amount;

        return $this;
    }

    /**
     * @param float|int $quantity
     * @return Money
     * @throws \InvalidArgumentException
     */
    public function divide($quantity)
    {
        if (!is_numeric($quantity)) {
            throw new \InvalidArgumentException('You must specify a numeric argument $quantity');
        }

        $this->amount = $this->amount / $quantity;

        return $this;
    }

}
