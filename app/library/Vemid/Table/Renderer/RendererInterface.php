<?php

namespace Vemid\Table\Renderer;

use Vemid\Table\Table;

/**
 * Interface RendererInterface
 *
 * @package Vemid\Table\Renderer
 */
interface RendererInterface
{
    /**
     * @throws \Exception
     */
    public function render();

    /**
     * @param string $filePath
     * @throws \Exception
     */
    public function saveToFile($filePath);
}
