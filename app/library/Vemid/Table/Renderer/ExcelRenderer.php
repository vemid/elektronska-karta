<?php

namespace Vemid\Table\Renderer;

use Vemid\Date\DateRange;
use Vemid\Date\DateTime;
use Vemid\Entity\EntityInterface;
use Vemid\Table\Column;
use Vemid\Table\Table;

/**
 * Class Excel
 *
 * @package Vemid\Table\Renderer
 */
class ExcelRenderer implements RendererInterface
{
    const TH_BACKGROUND_COLOR = 'cccccc';
    const TH_COLOR = '000000';

    /** @var \PHPExcel */
    private $excel;

    /** @var Table[] */
    private $tables;

    /** @var Table */
    private $activeTable;

    /** @var string */
    private $name;

    private $columnNumbers = [];

    /**
     * ExcelRenderer constructor.
     * @param array $tables
     * @param string $name
     */
    public function __construct(array $tables, $name = '')
    {
        $this->excel = new \PHPExcel();
        $this->tables = $tables;
        $this->name = $name;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        foreach ($this->tables as $index => $table) {
            $this->createTableSheet($table, $index);
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . $this->name . '.xlsx"');

        $objWriter->save('php://output');
        exit(1);
    }

    /**
     * {@inheritdoc}
     */
    public function saveToFile($saveToPath)
    {
        foreach ($this->tables as $index => $table) {
            $this->createTableSheet($table, $index);
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter->save($saveToPath);
    }

    /**
     * @param Table $table
     * @param int $index
     * @return \PHPExcel_Writer_IWriter
     * @throws \Exception
     */
    private function createTableSheet(Table $table, $index)
    {
        $this->activeTable = $table;
        $this->columnNumbers = [];

        $sheetCount = $this->excel->getSheetCount();
        if ($sheetCount < ($index + 1)) {
            $this->excel->createSheet($index);
        }

        $this->excel->setActiveSheetIndex($index);
        $sheet = $this->excel->getActiveSheet();

        $title = str_replace(['*', ':', '/', '\\', '?', '[', ']'], '', $table->getTitle());
        $sheet->setTitle($title);

        foreach ($table->getVisibleColumns() as $columnName => $column) {
            $this->columnNumbers[$columnName] = \count($this->columnNumbers);
        }

        $currentRowNumber = 0;
        $this->generateTableHead($currentRowNumber);

        $headerRowNumber = $currentRowNumber;
        $this->generateTableBody($currentRowNumber);

        $sheet->setAutoFilterByColumnAndRow(0, $headerRowNumber, \count($this->columnNumbers) - 1, $currentRowNumber);

        for ($i = 0; $i <= \count($this->columnNumbers) - 1; $i++) {
            $sheet->getColumnDimensionByColumn($i)->setAutoSize(true);
        }

        $sheet->calculateColumnWidths();

        for ($i = 0; $i <= \count($this->columnNumbers) - 1; $i++) {
            $colWidth = $sheet->getColumnDimensionByColumn($i)->getWidth();
            if ($colWidth < 20) {
                $colWidth = 20;
            } elseif ($colWidth > 65) {
                $colWidth = 65;
            } else {
                continue;
            }

            $sheet->getColumnDimensionByColumn($i)->setWidth($colWidth);
            $sheet->getColumnDimensionByColumn($i)->setAutoSize(false);
            $columnLetter = \PHPExcel_Cell::stringFromColumnIndex($i);
            $firstDataRowNumber = $headerRowNumber + 1;
            $columnCellRange = $columnLetter . $firstDataRowNumber . ':'
                . $columnLetter . (\count($this->activeTable->getRows()) + $firstDataRowNumber);
            $sheet->getStyle($columnCellRange)->getAlignment()->setWrapText(true);
        }
    }

    /**
     * @param int $currentRowNumber
     * @throws \Exception
     */
    private function generateTableHead(&$currentRowNumber)
    {
        $currentRowNumber++;
        $sheet = $this->excel->getActiveSheet();
        $sheet->getRowDimension('1')->setRowHeight(35);

        foreach ($this->activeTable->getVisibleColumns() as $columnName => $column) {
            $sheet->setCellValueByColumnAndRow(
                $this->columnNumbers[$columnName], $currentRowNumber, $column->getLabel()
            );
        }

        $headerRange = \PHPExcel_Cell::stringFromColumnIndex() . $currentRowNumber . ':'
            . \PHPExcel_Cell::stringFromColumnIndex(\count($this->columnNumbers) - 1) . $currentRowNumber;

        $sheet
            ->getStyle($headerRange)
            ->getFont()
            ->setBold(true);

        $styles = [
            'font'  => [
                'bold'  => true,
                'color' => ['rgb' => self::TH_COLOR],
                'size'  => 14,
                'name'  => 'PT Sans'
            ],
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => self::TH_BACKGROUND_COLOR]
            ),
            'borders' => [
                'bottom' => [
                    'color' => ['rgb' => self::TH_COLOR],
                    'style' => \PHPExcel_Style_Border::BORDER_DOUBLE
                ]
            ],
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ]
        ];

        $sheet
            ->getStyle($headerRange)
            ->applyFromArray($styles);

    }

    /**
     * @param int $currentRowNumber
     * @throws \Exception
     */
    private function generateTableBody(&$currentRowNumber)
    {
        $sheet = $this->excel->getActiveSheet();

        $tableRange = \PHPExcel_Cell::stringFromColumnIndex() . $currentRowNumber . ':'
            . \PHPExcel_Cell::stringFromColumnIndex(\count($this->columnNumbers) - 1)
            . ($currentRowNumber + $this->activeTable->getTotalRows());

        $sheet
            ->getStyle($tableRange)
            ->getFont()
            ->setName('PT Sans');

        foreach ($this->activeTable->getRows() as $row) {
            $cells = $row->getCells();
            $currentRowNumber++;

            foreach ($this->activeTable->getVisibleColumns() as $columnName => $column) {
                if (array_key_exists($columnName, $cells)) {
                    $cell = $cells[$columnName];
                    $columnNumber = $this->columnNumbers[$columnName];
                    $value = $this->generateExcelCellValue($column, $cell->getValue());
                    $excelCell = $sheet->setCellValueByColumnAndRow($columnNumber, $currentRowNumber, $value, true);

                    if ($column->getTextAlign()) {
                        switch ($column->getTextAlign()) {
                            case Column::ALIGN_LEFT:
                                $excelCell
                                    ->getStyle()
                                    ->getAlignment()
                                    ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                                break;

                            case Column::ALIGN_CENTER:
                                $excelCell
                                    ->getStyle()
                                    ->getAlignment()
                                    ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                break;

                            case Column::ALIGN_RIGHT:
                                $excelCell
                                    ->getStyle()
                                    ->getAlignment()
                                    ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                                break;
                        }
                    }

                    if ($column->getDataType() === Column::DATA_TYPE_FLOAT) {
                        $excelCell->setDataType(\PHPExcel_Cell_DataType::TYPE_NUMERIC);
                        $excelCell
                            ->getStyle()
                            ->getNumberFormat()
                            ->setFormatCode($column->getFormat() ?: \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
                    } elseif ($column->getDataType() === Column::DATA_TYPE_DATE) {
                        $format = $column->getFormat() ?: DateTime::FORMAT_SHORT_DAY_OF_WEEK;
                        $excelCell->setDataType(\PHPExcel_Cell_DataType::TYPE_NUMERIC);
                        $excelCell
                            ->getStyle()
                            ->getNumberFormat()
                            ->setFormatCode($this->getDateTimeFormat($format));
                    }

                    if ($cell->getTooltip()) {
                        $comment = strip_tags($cell->getTooltip());
                        $sheet
                            ->getCommentByColumnAndRow($columnNumber, $currentRowNumber)
                            ->setAuthor('Vemid')
                            ->setWidth('200pt')
                            ->setHeight((ceil(\strlen($comment) / 40) * 15) . 'pt')
                            ->getText()
                            ->createTextRun($comment);
                    }

                    if ($cell->getColor()) {
                        $sheet
                            ->getStyleByColumnAndRow($columnNumber, $currentRowNumber)
                            ->getFont()
                            ->setColor($this->generateExcelColor($cell->getColor()));
                    }

                    if ($cell->getBorder()) {
                        $sheet
                            ->getStyleByColumnAndRow($columnNumber, $currentRowNumber)
                            ->getBorders()
                            ->getAllBorders()
                            ->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM)
                            ->setColor($this->generateExcelColor($cell->getBorder()));
                    }

                    if ($fillColor = $cell->getBgrColor()) {
                        $sheet
                            ->getStyleByColumnAndRow($columnNumber, $currentRowNumber)
                            ->getFill()
                            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                            ->setStartColor($this->generateExcelColor($fillColor))
                            ->setEndColor($this->generateExcelColor($fillColor));
                    }
                }
            }
        }
    }

    /**
     * @param Column $column
     * @param $value
     * @return string|float|int
     */
    private function generateExcelCellValue(Column $column, $value)
    {
        if (\is_object($value)) {
            if ($value instanceof DateTime) {
                $value = \PHPExcel_Shared_Date::PHPToExcel($value);
            } elseif ($value instanceof DateRange) {
                $value = $value->getLong();
            } elseif ($value instanceof EntityInterface) {
                $value = $value->getDisplayName();
            }
        } else {
            if ($column->getDataType() === Column::DATA_TYPE_DATE && preg_match('/^\d{4}-\d{2}-\d{2}/', $value)) {
                return $this->generateExcelCellValue($column, new DateTime($value));
            }

            if ($column->getDataType() === Column::DATA_TYPE_BOOL) {
                return $value ? 'Yes' : 'No';
            }

            $value = strip_tags(str_replace('</div>', "</div>\r\n", $value));
        }

        return $value;
    }

    /**
     * @param string $format
     * @return string
     */
    private function getDateTimeFormat($format)
    {
        return str_replace(
            ['m', 'y', 'Y', 'M', 'd', 'jS', 'D', 'H', 'i', 's'],
            ['mm', 'yy', 'yyyy', 'mmm', 'dd', 'dd', 'ddd', 'hh', 'mm', 'ss'],
            $format
        );
    }

    private function generateExcelColor($hexColor)
    {
        $color = 0 === strpos($hexColor, '#') ? substr($hexColor, 1) : $hexColor;
        $color = 'ff' . $color;

        return new \PHPExcel_Style_Color($color);
    }
}
