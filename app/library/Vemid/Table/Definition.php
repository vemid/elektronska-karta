<?php

namespace Vemid\Table;

/**
 * Class Definition
 *
 * @package Table
 */
class Definition
{
    /** @var Column[] */
    private $columns = [];

    /**
     * @return Column|null
     */
    public function getPrimaryKeyColumn()
    {
        foreach ($this->getColumns() as $column) {
            if ($column->isPrimaryKey()) {
                return $column;
            }
        }

        return null;
    }

    /**
     * @return Column[]
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param string $columnName
     * @return Column
     */
    public function addColumn($columnName)
    {
        $column = new Column();
        $this->columns[$columnName] = $column;

        return $column;
    }

    /**
     * @param string $columnName
     * @param string $insertAfterColumnName
     * @return Column
     */
    public function addColumnAfter($columnName, $insertAfterColumnName)
    {
        $column = new Column();
        $position = array_search($insertAfterColumnName, array_keys($this->columns), true) + 1;

        $this->columns = array_merge(
            \array_slice($this->columns, 0, $position),
            [$columnName => $column],
            \array_slice($this->columns, $position)
        );

        return $column;
    }

    /**
     * @param string $columnName
     * @param string $insertBeforeColumnName
     * @return Column
     */
    public function addColumnBefore($columnName, $insertBeforeColumnName)
    {
        $column = new Column();
        $position = array_search($insertBeforeColumnName, array_keys($this->columns), true);

        $this->columns = array_merge(
            \array_slice($this->columns, 0, $position),
            [$columnName => $column],
            \array_slice($this->columns, $position)
        );

        return $column;
    }

    /**
     * @param string $columnName
     * @return Column
     * @throws \Exception
     */
    public function getColumn($columnName)
    {
        if (!$this->hasColumn($columnName)) {
            throw new \InvalidArgumentException(sprintf(
                'Column name %s not specified in table definition %s',
                $columnName, static::class
            ));
        }

        return $this->columns[$columnName];
    }

    /**
     * @param string $columnName
     * @return bool
     */
    public function hasColumn($columnName)
    {
        return array_key_exists($columnName, $this->columns);
    }
}
