<?php

namespace Vemid\Table;

/**
 * Class Table
 *
 * @package Table
 */
class Table
{
    /** @var Definition|null */
    private $definition;

    /** @var Row[] */
    private $rows = [];

    /** @var Column[] */
    private $visibleColumns = [];

    /** @var  string */
    private $title;

    /**
     * @param Definition $definition
     */
    public function __construct(Definition $definition)
    {
        $this->setDefinition($definition);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param Row|null $row
     * @return Row
     */
    public function addRow(Row $row = null)
    {
        if ($row === null) {
            $row = new Row();
        }
        $this->rows[] = $row;

        return $row;
    }

    /**
     * @param Row $removeRow
     */
    public function removeRow(Row $removeRow)
    {
        foreach ($this->rows as $index => $row) {
            if ($row == $removeRow) {
                unset($this->rows[$index]);
            }
        }
    }

    /**
     * @return Row[]
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * @return Column[]
     */
    public function getColumns()
    {
        return $this->definition->getColumns();
    }

    /**
     * @return Column[]
     */
    public function getVisibleColumns()
    {
        return $this->visibleColumns;
    }

    /**
     * @return Definition
     */
    public function getDefinition()
    {
        return $this->definition;
    }

    /**
     * @param Definition $definition
     * @return $this
     */
    public function setDefinition(Definition $definition)
    {
        $this->definition = $definition;

        foreach ($this->definition->getColumns() as $columnName => $column) {
            if ($column->isVisible()) {
                $this->visibleColumns[$columnName] = $column;
            }
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getTotalRows()
    {
        return count($this->rows);
    }
}
