<?php

namespace Vemid\Table;

/**
 * Class Column
 *
 * @package Table
 */
class Column
{
    const ALIGN_LEFT = 'left';
    const ALIGN_CENTER = 'center';
    const ALIGN_RIGHT = 'right';

    const DATA_TYPE_INT = 'int';
    const DATA_TYPE_BOOL = 'bool';
    const DATA_TYPE_FLOAT = 'float';
    const DATA_TYPE_STRING = 'string';
    const DATA_TYPE_DATE = 'date';
    const DATA_TYPE_TIME = 'time';
    const DATA_TYPE_MONEY = 'money';

    const FORMAT_NUMBER_0DP = '0';
    const FORMAT_NUMBER_1DP = '0.0';
    const FORMAT_NUMBER_2DP = '0.00';
    const FORMAT_NUMBER_3DP = '0.000';

    /** @var string */
    private $label;

    /** @var string */
    private $tooltip;

    /** @var int */
    private $flex = 1;

    /** @var string */
    private $textAlign = self::ALIGN_LEFT;

    /** @var string */
    private $format;

    /** @var bool */
    private $isVisible = true;

    /** @var bool */
    private $isPrimaryKey = false;

    /** @var string */
    private $dataType = self::DATA_TYPE_STRING;

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string
     */
    public function getTooltip()
    {
        return $this->tooltip;
    }

    /**
     * @param string $tooltip
     * @return $this
     */
    public function setTooltip($tooltip)
    {
        $this->tooltip = $tooltip;

        return $this;
    }

    /**
     * @return int
     */
    public function getFlex()
    {
        return $this->flex;
    }

    /**
     * @param int $flex
     * @return $this
     */
    public function setFlex($flex)
    {
        $this->flex = $flex;

        return $this;
    }

    /**
     * @return string
     */
    public function getTextAlign()
    {
        return $this->textAlign;
    }

    /**
     * @param string $textAlign
     * @return $this
     */
    public function setTextAlign($textAlign)
    {
        $this->textAlign = $textAlign;

        return $this;
    }

    /**
     * @return bool
     */
    public function isVisible()
    {
        return $this->isVisible;
    }

    /**
     * @param bool $isVisible
     * @return $this
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = (bool)$isVisible;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     * @return $this
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * @return string
     */
    public function getDataType()
    {
        return $this->dataType;
    }

    /**
     * @param string $dataType
     * @return $this
     */
    public function setDataType($dataType)
    {
        $this->dataType = $dataType;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPrimaryKey()
    {
        return $this->isPrimaryKey;
    }

    /**
     * @param bool $isPrimaryKey
     * @return $this
     */
    public function setIsPrimaryKey($isPrimaryKey)
    {
        $this->isPrimaryKey = (bool)$isPrimaryKey;

        return $this;
    }
}
