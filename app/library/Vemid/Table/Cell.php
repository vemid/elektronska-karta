<?php

namespace Vemid\Table;

/**
 * Class Cell
 *
 * @package Table
 */
class Cell
{
    private $bgrColor;
    private $color;
    private $tooltip;
    private $value;
    private $border;

    /**
     * @return string
     */
    public function getBgrColor()
    {
        return $this->bgrColor;
    }

    /**
     * @param string $bgrColor
     * @return $this
     */
    public function setBgrColor($bgrColor)
    {
        $this->bgrColor = $bgrColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return $this
     */
    public function setColor($color)
    {
        $this->color = $color;
        $this->bgrColor = $color;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBorder()
    {
        return $this->border;
    }

    /**
     * @param mixed $border
     */
    public function setBorder($border)
    {
        $this->border = $border;
    }

    /**
     * @return string
     */
    public function getTooltip()
    {
        return $this->tooltip;
    }

    /**
     * @param string $tooltip
     * @return $this
     */
    public function setTooltip($tooltip)
    {
        $this->tooltip = $tooltip;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}
