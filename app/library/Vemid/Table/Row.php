<?php

namespace Vemid\Table;

/**
 * Class Row
 *
 * @package Table
 */
class Row
{
    /** @var Cell[] */
    private $cells = [];

    /**
     * @param string $columnName
     * @param mixed $value
     * @return Cell
     */
    public function addCell($columnName, $value)
    {
        $cell = new Cell();
        $cell->setValue($value);
        $this->cells[$columnName] = $cell;

        return $cell;
    }

    /**
     * @param string $columnName
     * @return Cell|null
     */
    public function getCell($columnName)
    {
        return $this->cells[$columnName] ?? null;
    }

    /**
     * @return Cell[]
     */
    public function getCells()
    {
        return $this->cells;
    }
}
