<?php

namespace Vemid\Mailer;

use Task\Mailer;

/**
 * Class Message
 *
 * @package Vemid\Mailer
 * @author Vemid
 */
class Message
{

    /**
     * content type of PLAIN text.
     */
    const CONTENT_TYPE_PLAIN = 'text/plain';

    /**
     * content type HTML text.
     */
    const CONTENT_TYPE_HTML = 'text/html';

    /**
     * @var Manager
     */
    protected $manager;

    /**
     * @var \Swift_Message
     */
    protected $swiftMessage;

    /**
     * An array of email which failed send to recipients.
     *
     * @var array
     */
    protected $failedRecipients = [];

    /**
     * Create a new Message using $mailer for sending from SwiftMailer
     *
     * @param ManagerInterface $manager
     */
    public function __construct(ManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Set the from address of this message.
     *
     * You may pass an array of addresses if this message is from multiple people.
     * Example: array('receiver@domain.org', 'other@domain.org' => 'A name')
     *
     * If $name is passed and the first parameter is a string, this name will be
     * associated with the address.
     *
     * @param string|array $email
     * @param string|null $name optional
     *
     * @return $this
     *
     * @see \Swift_Message::setFrom()
     */
    public function setFrom($email, $name = null)
    {
        $email = $this->normalizeEmail($email);
        $this->getSwiftMessage()->setFrom($email, $name);

        return $this;
    }

    /**
     * Get the from address of this message.
     *
     * @return string
     * @see \Swift_Message::getFrom()
     */
    public function getFrom()
    {
        return $this->getSwiftMessage()->getFrom();
    }

    /**
     * Set the reply-to address of this message.
     *
     * You may pass an array of addresses if replies will go to multiple people.
     * Example: array('receiver@domain.org', 'other@domain.org' => 'A name')
     *
     * If $name is passed and the first parameter is a string, this name will be
     * associated with the address.
     *
     * @param string|array $email
     * @param string|null $name optional
     * @return $this
     * @see \Swift_Message::setReplyTo()
     */
    public function setReplyTo($email, $name = null)
    {
        $email = $this->normalizeEmail($email);
        $this->getSwiftMessage()->setReplyTo($email, $name);

        return $this;
    }

    /**
     * Get the reply-to address of this message.
     *
     * @return string
     * @see \Swift_Message::getReplyTo()
     */
    public function getReplyTo()
    {
        return $this->getSwiftMessage()->getReplyTo();
    }

    /**
     * Set the to addresses of this message.
     *
     * If multiple recipients will receive the message an array should be used.
     * Example: array('receiver@domain.org', 'other@domain.org' => 'A name')
     *
     * If $name is passed and the first parameter is a string, this name will be
     * associated with the address.
     *
     * @param string|array $email
     * @param string|null $name optional
     * @return $this
     * @see \Swift_Message::setTo()
     */
    public function setTo($email, $name = null)
    {
        $email = $this->normalizeEmail($email);
        $this->getSwiftMessage()->setTo($email, $name);

        return $this;
    }

    /**
     * Get the To addresses of this message.
     *
     * @return array
     * @see \Swift_Message::getTo()
     */
    public function getTo()
    {
        return $this->getSwiftMessage()->getTo();
    }

    /**
     * Set the Cc addresses of this message.
     *
     * If multiple recipients will receive the message an array should be used.
     * Example: array('receiver@domain.org', 'other@domain.org' => 'A name')
     *
     * If $name is passed and the first parameter is a string, this name will be
     * associated with the address.
     *
     * @param string|array $email
     * @param string|null $name optional
     * @return $this
     * @see \Swift_Message::setCc()
     */
    public function setCc($email, $name = null)
    {
        $email = $this->normalizeEmail($email);
        $this->getSwiftMessage()->setCc($email, $name);

        return $this;
    }

    /**
     * Get the Cc address of this message.
     *
     * @return array
     * @see \Swift_Message::getCc()
     */
    public function getCc()
    {
        return $this->getSwiftMessage()->getCc();
    }

    /**
     * Set the Bcc addresses of this message.
     *
     * If multiple recipients will receive the message an array should be used.
     * Example: array('receiver@domain.org', 'other@domain.org' => 'A name')
     *
     * If $name is passed and the first parameter is a string, this name will be
     * associated with the address.
     *
     * @param string|array $email
     * @param string|null $name optional
     * @return $this
     * @see \Swift_Message::setBcc()
     */
    public function setBcc($email, $name = null)
    {
        $email = $this->normalizeEmail($email);
        $this->getSwiftMessage()->setBcc($email, $name);

        return $this;
    }

    /**
     * Get the Bcc addresses of this message.
     *
     * @return array
     * @see \Swift_Message::getBcc()
     */
    public function getBcc()
    {
        return $this->getSwiftMessage()->getBcc();
    }

    /**
     * Set the sender of this message.
     *
     * This does not override the From field, but it has a higher significance.
     *
     * @param string|array $email
     * @param string|null $name optional
     * @return $this
     * @see \Swift_Message::setSender()
     */
    public function setSender($email, $name = null)
    {
        $email = $this->normalizeEmail($email);
        $this->getSwiftMessage()->setSender($email, $name);

        return $this;
    }

    /**
     * Get the sender of this message.
     *
     * @return string
     * @see \Swift_Message::getSender()
     */
    public function getSender()
    {
        return $this->getSwiftMessage()->getSender();
    }

    /**
     * Set the subject of this message.
     *
     * @param string $subject
     * @return $this
     * @see \Swift_Message::setSubject()
     */
    public function setSubject($subject)
    {
        $this->getSwiftMessage()->setSubject($subject);

        return $this;
    }

    /**
     * Get the subject of this message.
     *
     * @return string
     * @see \Swift_Message::getSubject()
     */
    public function getSubject()
    {
        return $this->getSwiftMessage()->getSubject();
    }

    /**
     * Set the body of this message, either as a string, or as an instance of
     * {@link \Swift_OutputByteStream}.
     *
     * @param mixed $content
     * @param string $contentType optional
     * @param string $charset optional
     * @return $this
     * @see \Swift_Message::setBody()
     */
    public function setBody($content, $contentType = self::CONTENT_TYPE_HTML, $charset = null)
    {
        $this->getSwiftMessage()->setBody($content, $contentType, $charset);

        return $this;
    }

    /**
     * Get the body of this message as a string.
     *
     * @return string
     * @see \Swift_Message::getBody()
     */
    public function getBody()
    {
        return $this->getSwiftMessage()->getBody();
    }

    /**
     * Set the Content-type of this message.
     *
     * @param string $contentType
     * @return $this
     * @see \Swift_Message::setContentType()
     */
    public function setContentType($contentType)
    {
        $this->getSwiftMessage()->setContentType($contentType);

        return $this;
    }

    /**
     * Get the Content-type of this message.
     *
     * @return string
     * @see \Swift_Message::getContentType()
     */
    public function getContentType()
    {
        return $this->getSwiftMessage()->getContentType();
    }

    /**
     * Set the character set of this message.
     *
     * @param string $charset
     * @return $this
     * @see \Swift_Message::setCharset()
     */
    public function setCharset($charset)
    {
        $this->getSwiftMessage()->setCharset($charset);

        return $this;
    }

    /**
     * Get the character set of this message.
     *
     * @return string
     * @see \Swift_Message::getCharset()
     */
    public function getCharset()
    {
        return $this->getSwiftMessage()->getCharset();
    }

    /**
     * Set the priority of this message.
     *
     * The value is an integer where 1 is the highest priority and 5 is the lowest.
     *
     * @param int $priority
     * @return $this
     * @see \Swift_Message::setPriority()
     */
    public function setPriority($priority)
    {
        $this->getSwiftMessage()->setPriority($priority);

        return $this;
    }

    /**
     * Get the priority of this message.
     *
     * The returned value is an integer where 1 is the highest priority and 5
     * is the lowest.
     *
     * @return int
     * @see \Swift_Message::getPriority()
     */
    public function getPriority()
    {
        return $this->getSwiftMessage()->getPriority();
    }

    /**
     * Ask for a delivery receipt from the recipient to be sent to $addresses
     *
     * @param array $email
     * @return $this
     * @see \Swift_Message::setReadReceiptTo()
     */
    public function setReadReceiptTo($email)
    {
        $email = $this->normalizeEmail($email);
        $this->getSwiftMessage()->setReadReceiptTo($email);

        return $this;
    }

    /**
     * An array of email which failed send to recipients.
     *
     * @return array
     */
    public function getFailedRecipients()
    {
        return $this->failedRecipients;
    }

    /**
     * Get the addresses to which a read-receipt will be sent.
     *
     * @return string
     * @see \Swift_Message::getReadReceiptTo()
     */
    public function getReadReceiptTo()
    {
        return $this->getSwiftMessage()->getReadReceiptTo();
    }

    /**
     * Set the return-path (the bounce address) of this message.
     *
     * @param string $email
     * @return $this
     * @see \Swift_Message::setReturnPath()
     */
    public function setReturnPath($email)
    {
        $this->getSwiftMessage()->setReturnPath($email);

        return $this;
    }

    /**
     * Get the return-path (bounce address) of this message.
     *
     * @return string
     * @see \Swift_Message::getReturnPath()
     */
    public function getReturnPath()
    {
        return $this->getSwiftMessage()->getReturnPath();
    }

    /**
     * Set the format of this message (flowed or fixed).
     *
     * @param string $format
     * @return string
     * @see \Swift_Message::setFormat()
     */
    public function setFormat($format)
    {
        $this->getSwiftMessage()->setFormat($format);

        return $this;
    }

    /**
     * Get the format of this message (i.e. flowed or fixed).
     *
     * @return string
     * @see \Swift_Message::getFormat()
     */
    public function getFormat()
    {
        return $this->getSwiftMessage()->getFormat();
    }

    /**
     * Attach a file to the message.
     *
     * Events:
     * - mailer:beforeAttachFile
     * - mailer:afterAttachFile
     *
     * @param string $file
     * @param array $options optional
     * @return $this
     * @see Message::createAttachmentFromPath()
     * @see Message::prepareAttachment()
     */
    public function setAttachment($file, $options = [])
    {
        $attachment = $this->createAttachmentFromPath($file);

        return $this->prepareAttachment($attachment, $options);
    }

    /**
     * Attach in-memory data as an attachment.
     *
     * @param string $data
     * @param string $name
     * @param array $options optional
     * @return Message
     * @see Message::createAttachmentFromData()
     * @see Message::prepareAttachment()
     */
    public function setAttachmentData($data, $name, $options = [])
    {
        $attachment = $this->createAttachmentFromData($data, $name);

        return $this->prepareAttachment($attachment, $options);
    }

    /**
     * Embed a file in the message and get the CID.
     *
     * @param string $file
     * @return string
     */
    public function setEmbedFile($file)
    {
        $embed = $this->createEmbedFromPath($file);

        return $this->getSwiftMessage()->embed($embed);
    }

    /**
     * Embed in-memory data in the message and get the CID.
     *
     * @param string $data
     * @param string $name
     * @return string
     */
    public function setEmbedData($data, $name)
    {
        $embed = $this->createEmbedFromData($data, $name);

        return $this->getSwiftMessage()->embed($embed);
    }

    /**
     * Return a {@link \Swift_Message} instance
     *
     * @return \Swift_Message
     */
    public function getSwiftMessage()
    {
        if (!$this->swiftMessage) {
            $this->swiftMessage = $this->manager->getMailer()->createMessage();
        }

        return $this->swiftMessage;
    }

    /**
     * @return bool|int
     */
    public function send()
    {
        $eventManager = $this->manager->getEventsManager();

        if ($eventManager) {
            $result = $eventManager->fire('mailer:beforeSend', $this);
        } else {
            $result = true;
        }

        if ($result !== false) {
            $this->failedRecipients = [];
            $mailer = $this->manager->getMailer();
            $count = $mailer->send($this->getSwiftMessage(), $this->failedRecipients);

            if ($eventManager) {
                $eventManager->fire('mailer:afterSend', $this, [$count, $this->failedRecipients]);
            }

            return $count;
        }

        return false;
    }

    /**
     * 
     */
    public function sendInBackground()
    {
        $task = new Mailer();
        $task->subject = $this->getSubject();
        $task->body = $this->getBody();
        $task->from = $this->getFrom();
        $task->to = $this->getTo();
        $task->cc = $this->getCc();
        $task->bcc = $this->getBcc();
        $task->runInBackground();
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->swiftMessage->toString();
    }

    /**
     * Prepare and attach the given attachment.
     *
     * @param \Swift_Mime_Attachment $attachment
     * @param array $options optional
     * @return $this
     * @see \Swift_Message::attach()
     */
    protected function prepareAttachment(\Swift_Mime_Attachment $attachment, array $options = array())
    {
        if (isset($options['mime'])) {
            $attachment->setContentType($options['mime']);
        }

        if (isset($options['as'])) {
            $attachment->setFilename($options['as']);
        }

        $eventManager = $this->manager->getEventsManager();

        if ($eventManager) {
            $result = $eventManager->fire('mailer:beforeAttachFile', $this, [$attachment]);
        } else {
            $result = true;
        }

        if ($result !== false) {
            $this->getSwiftMessage()->attach($attachment);

            if ($eventManager) {
                $eventManager->fire('mailer:afterAttachFile', $this, [$attachment]);
            }
        }

        return $this;
    }

    /**
     * Create a Swift new Attachment from a filesystem path.
     *
     * @param string $file
     * @return \Swift_Mime_Attachment
     * @see \Swift_Attachment::fromPath()
     */
    protected function createAttachmentFromPath($file)
    {
        return \Swift_Attachment::fromPath($file);
    }

    /**
     * Create a Swift Attachment instance from data.
     *
     * @param string $data
     * @param string $name optional
     * @return \Swift_Mime_Attachment
     * @see \Swift_Attachment::newInstance()
     */
    protected function createAttachmentFromData($data, $name)
    {
        return \Swift_Attachment::newInstance($data, $name);
    }

    /**
     * Create a Swift new Image from a filesystem path.
     *
     * @param string $file
     * @return \Swift_Image
     * @see \Swift_Image::fromPath()
     */
    protected function createEmbedFromPath($file)
    {
        return \Swift_Image::fromPath($file);
    }

    /**
     * Create a Swift new Image.
     *
     * @param string $data
     * @param string|null $name optional
     * @return \Swift_Image
     * @see \Swift_Image::newInstance()
     */
    protected function createEmbedFromData($data, $name = null)
    {
        return \Swift_Image::newInstance($data, $name);
    }

    /**
     * Normalize IDN domains.
     *
     * @param $email
     * @return array|string
     */
    protected function normalizeEmail($email)
    {
        if (is_array($email)) {
            $normalizedEmails = [];
            foreach ($email as $k => $v) {
                if (is_int($k)) {
                    $normalizedEmails[$k] = $this->manager->normalizeEmail($v);
                } else {
                    $k = $this->manager->normalizeEmail($k);
                    $normalizedEmails[$k] = $v;
                }
            }

            return $normalizedEmails;
        }

        return $this->manager->normalizeEmail($email);
    }

}
