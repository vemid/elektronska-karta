<?php

namespace Vemid\Mailer;

/**
 * Interface ManagerInterface
 *
 * @package Vemid\Mailer
 * @author Vemid
 */
interface ManagerInterface
{

    /**
     * Create a new Message instance.
     *
     * @return Message
     */
    public function createMessage();

    /**
     * Create a new Message instance.
     * For the body of the message uses the result of render of view
     *
     * @param string $view
     * @param null|array $params optional
     * @param null|string $viewsDir optional
     * @return Message
     * @see \Vemid\Mailer\Manager::createMessage()
     */
    public function createMessageFromView($view, array $params = null, $viewsDir = null);

    /**
     * Return a {@link \Swift_Mailer} instance
     *
     * @return \Swift_Mailer
     */
    public function getMailer();

    /**
     * Normalize IDN domains.
     *
     * @param string $email
     * @return string
     */
    public function normalizeEmail($email);

}
