<?php

namespace Vemid\Task\WarehouseOrders;

use Vemid\Error\Exception;
use Vemid\Service\WebServis\WebServisStatus;
use Vemid\Task\AbstractTask;

class SendToMisWarehouseOrderTask extends AbstractTask
{
    /**
     * @return mixed
     *
     * @throws \DomainException
     */
    public function execute()
    {

        $entityManager = $this->getDI()->getEntityManager();

        /** @var \WarehouseOrder $warehouseOrders */
        $warehouseOrders = $entityManager->find(\WarehouseOrder::class, [
            \WarehouseOrder::PROPERTY_PUSHED_TO_MIS . ' = :pushed: AND ' .
            \WarehouseOrder::PROPERTY_STATUS . ' =:status:',
            'bind' => [
                'pushed' => false,
                'status' => \WarehouseOrder::LOCKED
            ]
        ]);

        $locked = array_search("0", array_column($warehouseOrders->toArray(), 'locked'));

        if ($locked == 0 && $warehouseOrders->count() > 0) {

            /** @var \WarehouseOrder $warehouseOrder */
            $warehouseOrder = $warehouseOrders->getFirst();
            $warehouseOrder->setLocked(true);
            $entityManager->save($warehouseOrder);

            $warehouseOrderMisPusher = $this->getDI()->getMisDocumentPusher();
            $responseStatus = $warehouseOrderMisPusher($warehouseOrder);

            if ($responseStatus) {
                $warehouseOrder->setPushedToMis($responseStatus);
                $warehouseOrder->setLocked(false);

                if (!$entityManager->save($warehouseOrder)) {
                    return;
                }
            } else {
                $warehouseOrder->setStatus(\WarehouseOrder::EDITABLE);
                $entityManager->save($warehouseOrder);

                $mailManager = $this->getDI()->getMailManager();
                $message = $mailManager->createMessage();


                $message->setSubject('Restart servisa ');
                $message->setBody("ovo je testiranje");
                $message->setTo('admin@bebakids.com');
                $message->send();

                    $connection = ssh2_connect('192.168.100.210', 22);
                    ssh2_auth_password($connection, 'root', 'admin710412');

                    $stream = ssh2_exec($connection, 'service tomcat restart');
                    sleep(15);
                    throw new \DomainException('Cant connect to webservis');
            }

        }
    }

}