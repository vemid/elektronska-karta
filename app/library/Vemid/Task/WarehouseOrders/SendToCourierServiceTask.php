<?php

namespace Vemid\Task\WarehouseOrders;

use Vemid\Task\AbstractTask;

class SendToCourierServiceTask extends AbstractTask
{
    /** @var array */
    public $data;

    /** @var \WarehouseOrder */
    public $warehouseOrders;

    public function execute()
    {
        $logger = $this->getDI()->getCourierLogger();

        /** @var \WarehouseOrder $warehouseOrder */
        foreach ($this->warehouseOrders as $warehouseOrder) {
            $logger->debug("Pokusavam da posaljem ".$warehouseOrder->getPostalBarcode());
        }

        $courierHandler = $this->getDI()->getCourierHandler();
        $response = $courierHandler->addShipment($this->data);

        $entityManager = $this->getDI()->getEntityManager();

        $shipmentStatus = new \ShipmentStatus();
        $shipmentStatus->setReferenceId($this->data['ReferenceID']);

        if (!$entityManager->save($shipmentStatus)) {
            throw new \LogicException('Failed to create shipment status record');
        }

        if ($response) {
            /** @var \WarehouseOrder $warehouseOrder */
            foreach ($this->warehouseOrders as $warehouseOrder) {
                $warehouseOrder->setShipmentStatus($shipmentStatus);
                $warehouseOrder->setPosted(true);
                $entityManager->save($warehouseOrder);

            }
        }
    }
}
