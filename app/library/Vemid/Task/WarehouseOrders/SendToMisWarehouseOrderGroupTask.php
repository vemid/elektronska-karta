<?php

namespace Vemid\Task\WarehouseOrders;

use Vemid\Date\DateTime;
use Vemid\Error\Exception;
use Vemid\Service\WebServis\WebServisStatus;
use Vemid\Task\AbstractTask;

class SendToMisWarehouseOrderGroupTask extends AbstractTask
{
    /**
     * @return mixed
     *
     * @throws \DomainException
     */
    public function execute()
    {
        $entityManager = $this->getDI()->getEntityManager();
        $webServisChecker = new WebServisStatus();
        if($webServisChecker->check()) {
            /** @var \WarehouseOrderGroup $warehouseGroupOrders */
            $warehouseGroupOrders = $entityManager->find(\WarehouseOrderGroup::class, [
                \WarehouseOrderGroup::PROPERTY_PUSHED_TO_MIS . ' = :pushed: AND ' .
                \WarehouseOrderGroup::PROPERTY_STATUS . ' =:status:',
                'bind' => [
                    'pushed' => '0',
                    'status' => 'LOCKED'
                ]
            ]);

            $locked = array_search("0", array_column($warehouseGroupOrders->toArray(), 'locked'));

            if ($locked == 0 && $warehouseGroupOrders->count() > 0) {

                /** @var \WarehouseOrderGroup $warehouseGroupOrder */
                $warehouseGroupOrder = $warehouseGroupOrders->getFirst();
                $warehouseGroupOrder->setLocked(true);
                $entityManager->save($warehouseGroupOrder);

                $warehouseOrderMisGroupPusher = $this->getDI()->getMisDocumentGroupPusher();
                $responseStatus = $warehouseOrderMisGroupPusher($warehouseGroupOrder);


                if ($responseStatus) {
                    $warehouseGroupOrder->setPushedToMis($responseStatus);
                    $warehouseGroupOrder->setLocked(false);

                    $channelSupplyEntity = $warehouseGroupOrder->getChannelSupplyEntity();
                    $supplyChannel = $channelSupplyEntity->getChannelSupply();

                    /** @var \Code|\Client $entity */
                    $entity = $channelSupplyEntity->getEntity();
                    $displayName = $entity->getDisplayName();

                    $emails = [
                        'admin@bebakids.com' => 'Marko Vesic',
                        'knjazevac@bebakids.com' => 'Magacin Knjazevac'
                    ];

                    $warehouse = "MP";

                    if ($supplyChannel->getType() === \ChannelSupply::VP) {
//                    $displayName = $entity->getDisplayName();
                        $emails = [
                            'admin@bebakids.com' => 'Marko Vesic',
                            'knjazevac@bebakids.com' => 'Magacin Knjazevac',
                        ];
                    }

                    $mailManager = $this->getDI()->getMailManager();
                    $message = $mailManager->createMessageFromView('reports/email/warehouse-order-group-sent', [
                        'datas' => $warehouseGroupOrder,
                        'items' => $warehouseGroupOrder->getWarehouseOrderGroupItems(),
                        'displayName' => $displayName
                    ]);

                    $message->setSubject('Zbirna dostava naloga/otpremnica- ' . (new DateTime())->getShortSerbianFormat());
                    $message->setTo($warehouseGroupOrder->getChannelSupplyEntity()->getEmail());
                    $message->setCc($emails);
                    $message->send();

                    if (!$entityManager->save($warehouseGroupOrder)) {
                        return;
                    }
                } else {
                    $warehouseGroupOrder->setStatus(\WarehouseOrder::EDITABLE);
                    $entityManager->save($warehouseGroupOrder);

                    $mailManager = $this->getDI()->getMailManager();
                    $message = $mailManager->createMessage();


                    $message->setSubject('Restart servisa ');
                    $message->setBody("ovo je testiranje");
                    $message->setTo('admin@bebakids.com');
                    $message->send();

                    $connection = ssh2_connect('192.168.100.210', 22);
                    ssh2_auth_password($connection, 'root', 'admin710412');

                    $stream = ssh2_exec($connection, 'service tomcat restart');
                    sleep(15);
                    throw new \DomainException('Cant connect to webservis');
                }

            }
        }
    }

}