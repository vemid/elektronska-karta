<?php

namespace Vemid\Task\WarehouseOrders;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Error\Exception;
use Vemid\Service\WebServis\WebServisStatus;
use Vemid\Task\AbstractTask;

class CheckMisWarehouseOrderGroupStatusTask extends AbstractTask
{
    /** @var array */
    public $row;

    /**
     * @throws \DomainException
     */
    public function execute()
    {

        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');

        /** @var \WarehouseOrderGroup $warehouseOrderGroup */
        $warehouseOrderGroup = $this->getDI()->get('entityManager')->findOne(\WarehouseOrderGroup::class, [
            \WarehouseOrderGroup::PROPERTY_WAREHOUSE_GROUP_ORDER_CODE . ' = :code:',
            'bind' => [
                'code' => $this->row['oznaka']
            ]
        ]);

        if (!$warehouseOrderGroup) {
            throw new \DomainException('WarehouseOrderGroupCode for update status not found!');
        }

        if($this->row['status'] == "1") {
            $warehouseOrderGroup->setStatus(\WarehouseOrderGroup::SENT);
            $warehouseOrderGroup->setGrouped(\WarehouseOrderGroup::LOCKED);
            if (!$entityManager->save($warehouseOrderGroup)) {
                throw new \DomainException('WarehouseOrderGroupCode was not saved!');
            }
        }


    }

}