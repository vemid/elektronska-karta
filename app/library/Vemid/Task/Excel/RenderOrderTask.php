<?php

namespace Vemid\Task\Excel;

use Vemid\Entity\Type;
use Vemid\Task\AbstractTask;

/**
 * Class RenderOrderTask
 * @package Vemid\Task\Excel
 */
class RenderOrderTask extends AbstractTask
{
    /** @var \Order */
    public $order;

    /** @var array */
    public $classifications;

    /** @var array */
    public $clients;

    /** @var array */
    public $shops;

    /** @var string */
    public $name;

    /** @var array */
    public $oldCollections;

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        ob_start();

        \PHPExcel_Settings::setCacheStorageMethod(
            \PHPExcel_CachedObjectStorageFactory::cache_to_sqlite3,
            array('memoryCacheSize' => '1024MB')
        );

        /** @var \Classification[] $oldCollectionClassifications */
        $oldCollectionClassifications = $this->getDI()->getEntityManager()->find(\Classification::class, [
            'code IN ({codes:array})',
            'bind' => [
                'codes' => $this->oldCollections
            ]
        ]);

        $classificationIds = [];
        foreach ($oldCollectionClassifications as $classification) {
            $classificationIds[] = $classification->getId();
        }

        $objPHPExcel = new \PHPExcel();
        foreach ($this->classifications as $index => $classification) {
            if (!$classification['id']) {
                continue;
            }

            $query = $this->modelsManager->createBuilder()
                ->addFrom(\Product::class, 'p')
                ->leftJoin(\ProductClassification::class, 'pc.productId = p.id', 'pc')
                ->leftJoin(\ProductSize::class, 'ps.productId = p.id', 'ps')
                ->leftJoin(\OrderItem::class, 'oi.productSizeId = ps.id', 'oi')
                ->where('pc.entityTypeId = :entityTypeId:', [
                    'entityTypeId' => Type::CLASSIFICATION
                ])
                ->andWhere('oi.orderId = :orderId:', [
                    'orderId' => $this->order->getId()
                ])
                ->andWhere('oi.id IS NOT NULL')
                ->andWhere('pc.entityId IN ({ids:array})', [
                    'ids' => $classificationIds
                ])
                ->andWhere('pc.entityId IN ({ids:array})', [
                    'ids' => $classification['id']
                ])
                ->groupBy('p.id');

            /** @var \Product[] $products */
            $products = $query->getQuery()->execute();

            if ($index !== 0) {
                $objPHPExcel->createSheet($index);
            }

            $objWorkSheet = $objPHPExcel->setActiveSheetIndex($index);

            $sizes = [];
            foreach ($products as $product) {
                foreach ($product->getProductSizes() as $productSize) {
                    $code = $productSize->getCode()->getCode();
                    $sizes[$code] = $code;
                }
            }

            asort($sizes);

            $title = $classification['name'] . ' - ' . $classification['code'];
            $title = str_replace(['*', ':', '/', '\\', '?', '[', ']'], '', $title);

            $objWorkSheet->setTitle($title);
            $objWorkSheet
                ->getStyle()
                ->getFont()
                ->setName('PT Sans');

            $rowPosition = 1;
            foreach ($products as $product) {

                $queryClients = $this->modelsManager->createBuilder()
                    ->addFrom(\OrderItem::class, 'oi')
                    ->columns([
                        'cl.name',
                        'c.code',
                        'SUM(oi.quantityA) as qtyA',
                        'SUM(oi.quantityB) as qtyB'
                    ])
                    ->leftJoin(\ProductSize::class, 'oi.productSizeId = ps.id', 'ps')
                    ->leftJoin(\Code::class, 'ps.codeId = c.id', 'c')
                    ->leftJoin(\Client::class, 'oi.entityId = cl.id', 'cl')
                    ->where('ps.productId = :productId:', [
                        'productId' => $product->getId()
                    ])
                    ->andWhere('oi.entityTypeId = :entityTypeId:', [
                        'entityTypeId' => Type::CLIENT
                    ])
                    ->groupBy(['c.id', 'oi.entityId']);

                $resultClients = $queryClients->getQuery()->execute();

                $queryShops = $this->modelsManager->createBuilder()
                    ->addFrom(\ProductSize::class, 'ps')
                    ->columns([
                        'c2.name',
                        'c.code',
                        'SUM(oi.quantityA) as qtyA',
                        'SUM(oi.quantityB) as qtyB'
                    ])
                    ->leftJoin(\OrderItem::class, 'oi.productSizeId = ps.id', 'oi')
                    ->leftJoin(\Code::class, 'ps.codeId = c.id', 'c')
                    ->leftJoin(\Code::class, 'oi.entityId = c2.id', 'c2')
                    ->where('ps.productId = :productId:', [
                        'productId' => $product->getId()
                    ])
                    ->andWhere('oi.entityTypeId = :entityTypeId:', [
                        'entityTypeId' => Type::CODE
                    ])
                    ->groupBy(['c.id', 'oi.entityId']);

                $resultShops = $queryShops->getQuery()->execute();

                $groupedPartnerOrdersA = $groupedPartnerOrdersB = $groupedMpOrdersA = $groupedMpOrdersB = [];

                foreach ($sizes as $code1) {
                    $groupedPartnerOrdersA['Ukupno'][$code1] = 0;
                    $groupedPartnerOrdersB['Ukupno'][$code1] = 0;
                    $groupedMpOrdersA['Ukupno'][$code1] = 0;
                    $groupedMpOrdersB['Ukupno'][$code1] = 0;
                }

                foreach ($this->clients as $client) {
                    foreach ($sizes as $code) {
                        $groupedPartnerOrdersA[$client['name']][$code] = ' ';
                    }

                    foreach ($sizes as $code) {
                        $groupedPartnerOrdersB[$client['name']][$code] = ' ';
                    }
                }

                foreach ($this->shops as $shop) {
                    foreach ($sizes as $code) {
                        $groupedMpOrdersA[$shop['name']][$code] = ' ';
                    }

                    foreach ($sizes as $code) {
                        $groupedMpOrdersB[$shop['name']][$code] = ' ';
                    }
                }

                foreach ($resultClients as $row) {
                    if ($row['qtyA']) {
                        $groupedPartnerOrdersA[$row['name']][$row['code']] = (int)$row['qtyA'];
                        $groupedPartnerOrdersA['Ukupno'][$row['code']] += (int)$row['qtyA'];
                    }

                    if ($row['qtyB']) {
                        $groupedPartnerOrdersB[$row['name']][$row['code']] = (int)$row['qtyB'];
                        $groupedPartnerOrdersB['Ukupno'][$row['code']] += (int)$row['qtyB'];
                    }
                }

                foreach ($resultShops as $row) {
                    if ($row['qtyA']) {
                        $groupedMpOrdersA[$row['name']][$row['code']] = (int)$row['qtyA'];
                        $groupedMpOrdersA['Ukupno'][$row['code']] += (int)$row['qtyA'];
                    }

                    if ($row['qtyB']) {
                        $groupedMpOrdersB[$row['name']][$row['code']] = (int)$row['qtyB'];
                        $groupedMpOrdersB['Ukupno'][$row['code']] += (int)$row['qtyB'];
                    }
                }

                $totalA = $groupedPartnerOrdersA['Ukupno'];
                $totalB = $groupedPartnerOrdersB['Ukupno'];
                $totalMpA = $groupedMpOrdersA['Ukupno'];
                $totalMpB = $groupedMpOrdersB['Ukupno'];

                unset(
                    $groupedPartnerOrdersA['Ukupno'],
                    $groupedPartnerOrdersB['Ukupno'],
                    $groupedMpOrdersA['Ukupno'],
                    $groupedMpOrdersB['Ukupno']
                );

                $columnIndex = \count($sizes);
                $groupedPartnerOrdersA += ['Ukupno' => $totalA];
                $groupedPartnerOrdersB += ['Ukupno' => $totalB];
                $groupedMpOrdersA += ['Ukupno' => $totalMpA];
                $groupedMpOrdersB += ['Ukupno' => $totalMpB];

                $colorCode = mb_substr(mb_substr($product->getCode(), 0, -2), 3);

                /** @var \Classification $color */
                $color = $this->getDI()->getEntityManager()->findOne(\Classification::class, [
                    \Classification::PROPERTY_CODE . ' = :code:',
                    'bind' => [
                        'code' => $colorCode
                    ]
                ]);

                $objWorkSheet->setCellValueExplicit('B' . $rowPosition, $product->getName());
                $objWorkSheet->getColumnDimension('B')->setWidth(22.0);

                $objWorkSheet->setCellValueExplicit('C' . $rowPosition, '');
                $objWorkSheet->getColumnDimension('C')->setAutoSize(true);

                $objWorkSheet->setCellValueExplicit('D' . ($rowPosition+1), 'Boja:');
                $objWorkSheet->getColumnDimension('D')->setAutoSize(true);

                $objWorkSheet->setCellValueExplicit('E' . ($rowPosition+1), $color ? $color->getShortColorName() : '');
                $colIndex = \PHPExcel_Cell::columnIndexFromString('E');
                $mergedIndex = \PHPExcel_Cell::stringFromColumnIndex($colIndex + 2);
                $objWorkSheet->mergeCells('E' . $rowPosition . ':' . $mergedIndex . $rowPosition);
                $objWorkSheet->getColumnDimension('E')->setAutoSize(true);

                $objWorkSheet->setCellValueExplicit('B' . ($rowPosition + 1), $product->getCode());


                $startingPoint = $columnIndex + 3;
                $secondTableIndex = $startingPoint;

                for ($z = 2; $z < ($secondTableIndex - 1); $z++) {
                    $colString = \PHPExcel_Cell::stringFromColumnIndex($z);
                    $objWorkSheet->getColumnDimension($colString)->setAutoSize(true);
                }

                $colString = \PHPExcel_Cell::stringFromColumnIndex($secondTableIndex);
                $objWorkSheet->getColumnDimension($colString)->setWidth(22.0);

                $objWorkSheet->setCellValueExplicit($colString . $rowPosition, $product->getName());
                $objWorkSheet->setCellValueExplicit($colString . ($rowPosition + 1), $product->getCode());

                $startingB = $colString;
                $others = $secondTableIndex;

                $secondTableIndex++;
                $colString = \PHPExcel_Cell::stringFromColumnIndex($secondTableIndex);
                $objWorkSheet->setCellValueExplicit($colString . $rowPosition, '');
                $objWorkSheet->getColumnDimension($colString)->setAutoSize(true);

                $secondTableIndex++;
                $colString = \PHPExcel_Cell::stringFromColumnIndex($secondTableIndex);
                $objWorkSheet->setCellValueExplicit($colString . ($rowPosition+1), 'Boja:');
                $objWorkSheet->getColumnDimension($colString)->setAutoSize(true);

                $secondTableIndex++;
                $colString = \PHPExcel_Cell::stringFromColumnIndex($secondTableIndex);
                $objWorkSheet->setCellValueExplicit($colString . ($rowPosition+1), $color ? $color->getShortColorName() : '');
                $mergedIndex = \PHPExcel_Cell::stringFromColumnIndex($secondTableIndex + 2);
                $objWorkSheet->mergeCells($colString . $rowPosition . ':' . $mergedIndex . $rowPosition);
                $objWorkSheet->getColumnDimension($colString)->setAutoSize(true);

                for ($z = ($secondTableIndex + 1); $z <= (\count($sizes) + $others); $z++) {
                    $colString = \PHPExcel_Cell::stringFromColumnIndex($z);
                    $objWorkSheet->getColumnDimension($colString)->setAutoSize(true);
                }

                $rowPosition++;
                $rowPosition++;
                $index = 1;

                foreach ($sizes as $size) {
                    $index++;
                    $colStringA = \PHPExcel_Cell::stringFromColumnIndex($index);
                    $objWorkSheet->setCellValueExplicit($colStringA . $rowPosition, $size);
                    $objWorkSheet->getColumnDimension($index)->setAutoSize(true);
                }

                foreach ($sizes as $size) {
                    $startingPoint++;
                    $colStringB = \PHPExcel_Cell::stringFromColumnIndex($startingPoint);
                    $objWorkSheet->setCellValueExplicit($colStringB . $rowPosition, $size);
                    $objWorkSheet->getColumnDimension($index)->setAutoSize(true);
                }

                $index = 0;
                $groupedPartnerOrdersAData = [];
                foreach ($groupedPartnerOrdersA as $key => $row) {
                    $groupedPartnerOrdersAData[$index][] = $key;
                    foreach ($row as $keyData => $codes) {
                        $groupedPartnerOrdersAData[$index][] = $codes;
                    }

                    $index++;
                }

                $index = 0;
                $groupedPartnerOrdersBData = [];
                foreach ($groupedPartnerOrdersB as $key => $row) {
                    $groupedPartnerOrdersBData[$index][] = $key;
                    foreach ($row as $keyData => $codes) {
                        $groupedPartnerOrdersBData[$index][] = $codes;
                    }

                    $index++;
                }

                $rowPosition++;

                $bColumn = \PHPExcel_Cell::stringFromColumnIndex($columnIndex + 3);
                $objWorkSheet->fromArray($groupedPartnerOrdersAData, '0', 'B' . $rowPosition, true);
                $objWorkSheet->fromArray($groupedPartnerOrdersBData, '0', $bColumn . $rowPosition, true);

                $totalRowA = \count($groupedPartnerOrdersA);
                $totalRowB = \count($groupedPartnerOrdersB);
                if ($totalRowA > $totalRowB) {
                    $rows = $totalRowA + 3;
                } else {
                    $rows = $totalRowB + 3;
                }

                $lastRow = $rowPosition + $rows - 2;

                $totalVpA = 0;
                foreach ($totalA as $totalBySize) {
                    if ($totalBySize === 'Ukupno') {
                        continue;
                    }

                    $totalVpA += $totalBySize;
                }

                $totalVpB = 0;
                foreach ($totalB as $totalBySize) {
                    if ($totalBySize === 'Ukupno') {
                        continue;
                    }

                    $totalVpB += $totalBySize;
                }

                $style = [
                    'alignment' => [
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    ],
                    'borders' => [
                        'allborders' => [
                            'style' => \PHPExcel_Style_Border::BORDER_MEDIUM
                        ]
                    ]
                ];

                $objWorkSheet->getStyle('B' . $rowPosition . ':' . $colStringA . ($lastRow - 2))->applyFromArray($style);
                $objWorkSheet->getStyle($startingB . $rowPosition . ':' . $colStringB . ($lastRow - 2))->applyFromArray($style);


                $colStringA = \PHPExcel_Cell::stringFromColumnIndex($columnIndex);
                $colStringA2 = \PHPExcel_Cell::stringFromColumnIndex($columnIndex + 1);
                $objWorkSheet->setCellValueExplicit($colStringA . $lastRow, $totalVpA);
                $objWorkSheet->getStyle($colStringA . $lastRow . ':' . $colStringA2 . $lastRow)->getFont()->setBold(true);
                $objWorkSheet->getStyle($colStringA . $lastRow . ':' . $colStringA2 . $lastRow)->applyFromArray($style);
                $objWorkSheet->mergeCells($colStringA . $lastRow . ':' . $colStringA2 . $lastRow);

                $bColumn = \PHPExcel_Cell::stringFromColumnIndex($columnIndex * 2 + 3);
                $bColumnPrev = \PHPExcel_Cell::stringFromColumnIndex($columnIndex * 2 + 2);
                $objWorkSheet->setCellValueExplicit($bColumnPrev . $lastRow, $totalVpB);
                $objWorkSheet->getStyle($bColumnPrev . $lastRow . ':' . $bColumn . $lastRow)->getFont()->setBold(true);
                $objWorkSheet->getStyle($bColumnPrev . $lastRow . ':' . $bColumn . $lastRow)->applyFromArray($style);
                $objWorkSheet->mergeCells($bColumnPrev . $lastRow . ':' . $bColumn . $lastRow);

                $rowPosition = $rowPosition + \count($groupedPartnerOrdersA) + 3;

                $index = 1;
                foreach ($sizes as $size) {
                    $index++;

                    $colString = \PHPExcel_Cell::stringFromColumnIndex($index);
                    $objWorkSheet->setCellValueExplicit($colString . $rowPosition, $size);
                    $objWorkSheet->getColumnDimension($index)->setAutoSize(true);

                }

                $startingPoint = $columnIndex + 3;

                foreach ($sizes as $size) {
                    $startingPoint++;

                    $colString = \PHPExcel_Cell::stringFromColumnIndex($startingPoint);
                    $objWorkSheet->setCellValueExplicit($colString . $rowPosition, $size);
                    $objWorkSheet->getColumnDimension($index)->setAutoSize(true);

                }

                $rowPosition++;

                $index = 0;
                $groupedMpOrdersAData = [];
                foreach ($groupedMpOrdersA as $key => $row) {
                    $groupedMpOrdersAData[$index][] = $key;
                    foreach ($row as $keyData => $codes) {
                        $groupedMpOrdersAData[$index][] = $codes;
                    }

                    $index++;
                }

                $index = 0;
                $groupedMpOrdersBData = [];
                foreach ($groupedMpOrdersB as $key => $row) {
                    $groupedMpOrdersBData[$index][] = $key;
                    foreach ($row as $keyData => $codes) {
                        $groupedMpOrdersBData[$index][] = $codes;
                    }

                    $index++;
                }

                $bColumn = \PHPExcel_Cell::stringFromColumnIndex($columnIndex + 3);
                $objWorkSheet->fromArray($groupedMpOrdersAData, '0', 'B' . $rowPosition, true);
                $objWorkSheet->fromArray($groupedMpOrdersBData, '0', $bColumn . $rowPosition, true);

                $totalRowA = \count($groupedMpOrdersA);
                $totalRowB = \count($groupedMpOrdersB);
                $rows = $totalRowB + 1;
                if ($totalRowA > $totalRowB) {
                    $rows = $totalRowA + 1;
                }

                $lastRow = $rowPosition + $rows;

                $totalA2 = 0;
                foreach ($totalMpA as $totalBySize) {
                    if ($totalBySize === 'Ukupno') {
                        continue;
                    }

                    $totalA2 += $totalBySize;
                }

                $totalB2 = 0;
                foreach ($totalMpB as $totalBySize) {
                    if ($totalBySize === 'Ukupno') {
                        continue;
                    }

                    $totalB2 += $totalBySize;
                }

                $objWorkSheet->getStyle(sprintf('B%s:%s%s', $rowPosition, $colStringA2, $lastRow - 2))->applyFromArray($style);
                $objWorkSheet->getStyle(sprintf('%s%s:%s%s', $startingB, $rowPosition, $colStringB, $lastRow - 2))->applyFromArray($style);

                $objWorkSheet->setCellValueExplicit($colStringA . $lastRow, $totalA2);
                $objWorkSheet->getStyle($colStringA . $lastRow . ':' . $colStringA2 . $lastRow)->getFont()->setBold(true);
                $objWorkSheet->getStyle($colStringA . $lastRow . ':' . $colStringA2 . $lastRow)->applyFromArray($style);
                $objWorkSheet->mergeCells($colStringA . $lastRow . ':' . $colStringA2 . $lastRow);

                $bColumn = \PHPExcel_Cell::stringFromColumnIndex($columnIndex * 2 + 3);
                $bColumnPrev = \PHPExcel_Cell::stringFromColumnIndex($columnIndex * 2 + 2);
                $objWorkSheet->setCellValueExplicit($bColumnPrev . $lastRow, $totalB2);
                $objWorkSheet->getStyle($bColumnPrev . $lastRow . ':' . $bColumn . $lastRow)->getFont()->setBold(true);
                $objWorkSheet->getStyle($bColumnPrev . $lastRow . ':' . $bColumn . $lastRow)->applyFromArray($style);
                $objWorkSheet->mergeCells($bColumnPrev . $lastRow . ':' . $bColumn . $lastRow);
                $objWorkSheet->setCellValueExplicit('B' . $lastRow, $totalA2 + $totalVpA);

                $objWorkSheet->getStyle(sprintf('B%s:C%s', $lastRow, $lastRow))->applyFromArray([
                    'fill' => [
                        'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => ['rgb' => 'CCCCCC']
                    ]
                ]);

                $objWorkSheet
                    ->getStyle(sprintf('B%s:C%s', $lastRow, $lastRow))
                    ->applyFromArray($style)
                    ->getFont()->setBold(true);

                $objWorkSheet->mergeCells(sprintf('B%s:C%s', $lastRow, $lastRow));

                $bColumnTotal = \PHPExcel_Cell::stringFromColumnIndex($columnIndex + 4);
                $bColumnPrevTotal = \PHPExcel_Cell::stringFromColumnIndex($columnIndex + 3);

                $objWorkSheet->getStyle(sprintf('%s%s:%s%s', $bColumnPrevTotal, $lastRow, $bColumnTotal, $lastRow))->applyFromArray([
                    'fill' => [
                        'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => ['rgb' => 'CCCCCC']
                    ]
                ]);

                $objWorkSheet
                    ->getStyle(sprintf('%s%s:%s%s', $bColumnPrevTotal, $lastRow, $bColumnTotal, $lastRow))
                    ->applyFromArray($style)
                    ->getFont()->setBold(true);

                $objWorkSheet->setCellValueExplicit($bColumnPrevTotal . $lastRow, $totalB2 + $totalVpB);
                $objWorkSheet->mergeCells(sprintf('%s%s:%s%s', $bColumnPrevTotal, $lastRow, $bColumnTotal, $lastRow));

                $rowPosition = $rowPosition + \count($groupedMpOrdersA) + 3;
            }

            $objPHPExcel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        }

        $tempFolder = rtrim(sys_get_temp_dir(), '/') . '/order-products';
        $fileName = $tempFolder . '/' . $this->name;

        if (file_exists($fileName)) {
            unlink($fileName);
        }

        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);

        $mailManager = $this->getDI()->getMailManager();
        $message = $mailManager->createMessageFromView('reports/order-mail-template');

        $name = '';
        /** @var \Classification $colectionName */
        foreach ($oldCollectionClassifications as $colectionName) {
            $name .= $colectionName->getName() . '-';
        }

        $message->setSubject("Slanje Excela ". $name);
        $message->setTo('raster@bebakids.com');
//        $message->setBcc('magacin@bebakids.com');
        $message->setAttachment($fileName);
        $message->send();

        ob_end_clean();
    }
}
