<?php

namespace Vemid\Task\Excel;

use Vemid\Task\AbstractTask;

/**
 * Class MergeExcel
 * @package Vemid\Task\Excel
 */
class MergeExcel extends AbstractTask
{
    /** @var string */
    public $path;

    /** @var int */
    public $totalFiles;

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $newFileName = 'Report_Name' . date('Y-m-d') . '.xlsx';
        $file = APP_PATH . 'public/uploads/' . $newFileName;

        if (file_exists($file)) {
            unlink($file);
        }

        $files = array_filter(scandir($this->path, 1), function($item) use ($newFileName) {
            return !is_dir($this->path . '/' . $item) && $newFileName !== $item;
        });

        $start = microtime(true);

        while ((int)$this->totalFiles !== \count($files)) {
            sleep(10);
            $timeElapsed = microtime(true) - $start;
            $files = array_filter(scandir($this->path, 1), function($item) use ($newFileName) {
                return !is_dir($this->path . '/' . $item) && $newFileName !== $item;
            });

            if ($timeElapsed > 1500) {
                throw new \RuntimeException('Failed to merge excel');
            }
        }

        $bigExcel = new \PHPExcel();
        $bigExcel->removeSheetByIndex(0);

        foreach ($files as $filename) {
            $filename1 = $this->path . '/' . $filename;

            $ext = pathinfo($filename1, PATHINFO_EXTENSION);
            if ($ext === 'xlsx') {
                $inputFileType = 'Excel2007';
            } elseif ($ext === 'xls') {
                $inputFileType = 'Excel5';
            } else {
                continue;
            }

            $inputFileType = \PHPExcel_IOFactory::identify($filename1);

            $reader = \PHPExcel_IOFactory::createReader($inputFileType);
            $excel = $reader->load($filename1);

            foreach ($excel->getAllSheets() as $sheet) {
                $bigExcel->addExternalSheet($sheet);
            }

            foreach ($excel->getNamedRanges() as $namedRange) {
                $bigExcel->addNamedRange($namedRange);
            }
        }

        $writer = \PHPExcel_IOFactory::createWriter($bigExcel, 'Excel2007');
        $writer->save($file);

        $mailManager = $this->getDI()->getMailManager();
        $message = $mailManager->createMessageFromView('reports/mail-template');

        $message->setSubject("Slanje Excela ");
        $message->setTo('admin@bebakids.com');
        $message->setBcc('darkovesic3@gmail.com');
        $message->setAttachment($file);
        $message->send();

        exit(1);
    }
}
