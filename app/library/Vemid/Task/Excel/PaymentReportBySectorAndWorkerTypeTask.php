<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 5/22/19
 * Time: 11:19
 */

namespace Vemid\Task\Excel;

use Vemid\Application\Di;
use Vemid\Entity\Type;
use Vemid\Task\AbstractTask;
use \Vemid\Service\Report\PaymentReport;


/**
 * Class PaymentReportBySectorAndWorkerTypeTask
 * @package Vemid\Task\Excel
 */
class PaymentReportBySectorAndWorkerTypeTask extends AbstractTask
{


    /** @var \Code[] */
    public $sector;

    /** @var \Vemid\Date\DateTime */
    public $dateFrom;

    /** @var \Vemid\Date\DateTime */
    public $dateTo;

    /**
     * {@inheritdoc}
     */

    public function execute()
    {
        ob_start();

        /** @var \ProductionWorker[] $workersData */
        $workersData = [];

        /** @var \Code $sector */
        foreach ($this->sector as $sector) {
            $workersData[] = $sector->getProductionWorkers();
        }

        //$exortData = [];
        $exortDataHand = [];
        $exortDataMachine = [];

        $payment = new PaymentReport($this->getDI()->getEntityManager());

        foreach ($workersData as $workers ) {

            /** @var \ProductionWorker $worker */
            foreach ($workers as $worker) {
                //array_push($exortData,$payment->getPaymentReportByWorkerData($this->dateFrom, $this->dateTo, $worker));
                $exortDataHand[] = $payment->getPaymentReportByWorkerData($this->dateFrom, $this->dateTo, $worker,\ProductionMachine::TYPE_HAND);
                $exortDataMachine[] = $payment->getPaymentReportByWorkerData($this->dateFrom, $this->dateTo, $worker,\ProductionMachine::TYPE_MACHINE);

            }
        }

        //print_r($exortData);
        $exortData = array_merge($exortDataHand,$exortDataMachine);

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()
            ->setCreator("Marko Vesic")
            ->setLastModifiedBy("BEBAKIDS")
            ->setTitle("Export podataka iz Excel-a")
            ->setSubject("Export podataka iz Excel-a")
            ->setDescription("Onog dana kada budete prestali da izmisljate, placam rucak u MB-u")
            ->setKeywords("Template excel");
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

        $styleData = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );

        $styleHeader = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '1C9EC9')
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->applyFromArray($styleHeader);

        $rows = array(
            array(
                'Mesec',
                'Datum',
                'Radnik',
                'Broj radnih dana',
                'Bolovanje',
                'Odmor',
                'Placeno',
                'Rezija',
                'Vrednost rezije',
                'Ostvareno Vreme Oper.',
                'Planirano Vreme Oper.',
                'Vrednost Operacije',
                'Prekovremeno',
                'Total Ostvareno Vreme',
                'Total Planirano Vreme',
                'Total zarada',
                'Sektor',
                'Godina',
                'Sifra radnika',
                'Tip radnika'
            )
        );
        $objPHPExcel->getActiveSheet()->fromArray($rows,null, 'A1');

        $rowPosition = 2;

        foreach ($exortData as $datas) {

            foreach ($datas as $data) {

                $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowPosition, $data['month']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowPosition, $data['date']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowPosition, $data['displayName']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowPosition, $data['numberOfDays']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowPosition, $data['sickDays']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowPosition, $data['vacationDays']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowPosition, $data['vacationPaidDays']);
                $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowPosition, $data['cuttingJobSeconds']/60);
                $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowPosition, $data['cuttingJobMoney']);
                $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowPosition, $data['oliwRealizesSeconds']/60);
                $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowPosition, $data['oliwPlanedTimeSeconds']/60);
                $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowPosition, round($data['oliwMoney']),1);
                $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowPosition, $data['overtimeSeconds']/60);
                $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowPosition, $data['totalRealizedSeconds']/60);
                $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowPosition, $data['totalPlanedSeconds']/60);
                $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowPosition, round($data['totalDataMoney']),1);
                $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowPosition, $data['sector']);
                $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowPosition, $data['year']);
                $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowPosition, $data['worker']);
                $objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowPosition, $data['workerType']);

                $rowPosition++;
            }
        }
        $tempFolder = rtrim(sys_get_temp_dir(), '/') . '/products-list';
        if (!is_dir($tempFolder) && !mkdir($tempFolder, 0700)) {
            throw new \LogicException('Dir can not be created!');
        }
        $fileName = $tempFolder . '/' . 'ExportType.xlsx';

        if (file_exists($fileName)) {
            unlink($fileName);
        }

        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);

        $connId = ftp_connect('192.168.100.9');
        $ftpUserName = "server.elektronska";
        $ftpUserPass = "bk980000!";

        if (!ftp_login($connId, $ftpUserName, $ftpUserPass)) {
            throw new \LogicException(sprintf('Login to FTP failed!'));
        }

        if (!ftp_chdir($connId, '/Razmena/Knjazevac/Izvestaji/izvestaji normiranja/PODACI')) {
            throw new \LogicException(sprintf('DIR do not exist!'));
        }

        if (!ftp_put($connId, 'ExportType.xlsx', $fileName, FTP_BINARY)) {
            throw new \LogicException(sprintf('There was a problem while uploading %s', $fileName));
        }

//        $mailManager = $this->getDI()->getMailManager();
//        $message = $mailManager->createMessageFromView('reports/order-mail-template');
//
//        $message->setSubject("Spisak proizvoda iz Elektronske Karte");
//        $message->setTo('admin@bebakids.com');
//        $message->setAttachment($fileName);
//        $message->send();


        ob_end_clean();
    }

}