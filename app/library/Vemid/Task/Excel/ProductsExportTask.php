<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 11/18/18
 * Time: 21:06
 */

namespace Vemid\Task\Excel;

use Vemid\Application\Di;
use Vemid\Entity\Type;
use Vemid\Task\AbstractTask;
use Vemid\Service\GoogleTranslator\GoogleTranslateClient;

/**
 * Class ProductsExportTask
 * @package Vemid\Task\Excel
 */
class ProductsExportTask extends AbstractTask
{

    /** @var array*/
    public $users;

    /** @var string */
    public $seasonCode;

    /**
     * {@inheritdoc}
     */

    public function execute()
    {
        ob_start();

        $classificationCode = Type::CLASSIFICATION;


        $allProducts = $this->modelsManager->createBuilder()
            ->from(\Product::class)
            ->leftJoin(\ProductClassification::class,"pc.productId = Product.id and pc.entityTypeId = {$classificationCode}", 'pc')
            ->leftJoin(\Classification::class, "pc.entityId = c.id",'c')
            ->leftJoin(\Priority::class,"pc2.id = Product.priorityId",'pc2')
            ->where('c.code IN ({bindParam:array})',
            [
                'bindParam' => $this->seasonCode
            ])
            ->orderBy('pc2.priorityNumber,Product.code');


        $products = $allProducts->getQuery()->execute();

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()
            ->setCreator("Marko Vesic")
            ->setLastModifiedBy("BEBAKIDS")
            ->setTitle("Export podataka iz Excel-a")
            ->setSubject("Export podataka iz Excel-a")
            ->setDescription("Onog dana kada budete prestali da izmisljate, placam rucak u MB-u")
            ->setKeywords("Template excel");
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

        $styleData = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );

        $styleHeader = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '1C9EC9')
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->applyFromArray($styleHeader);

        $rows = array(
            array(
                'Slika',
                'Sifra',
                'Naziv',
                'Boja',
                'Prioritet',
                'Model',
                'Pod-Model',
                'Sezona',
                'Kolekcija',
                'Pol',
                'Velicine',
                'Nabavna Cena',
                'Marza',
                'VP Cena',
                'Marza',
                'MP Cena',
                'VPeur Cena'
            )
        );
        $objPHPExcel->getActiveSheet()->fromArray($rows,null, 'A1');

        /** @var \Pricelist $priceListRSD */
        $priceListRSD = $this->getDI()->getEntityManager()->findOne(\Pricelist::class, [
            'name = :name:',
            'bind' => [
                'name' => "DINARSKI"
            ]
        ]);

        /** @var \Pricelist $priceListEUR */
        $priceListEUR = $this->getDI()->getEntityManager()->findOne(\Pricelist::class, [
            'name = :name:',
            'bind' => [
                'name' => 'DEVIZNI'
            ]
        ]);

        $productPrices = $this->getDI()->getProductPrices();

        //$translator = new GoogleTranslateClient();

        $rowPosition = 2;
        /** @var \Product $product */
        foreach ($products as $product) {
            $hydrator = new \Vemid\Service\Product\Hydrator($product, $this->getDI()->getEntityManager());
            $purchasePrice = $productPrices->getPurchasePrice($priceListRSD,$product) ? $productPrices->getPurchasePrice($priceListRSD,$product) : 0;
            $wholesalePrice = $productPrices->getWholesalePrice($priceListRSD,$product)  ? $productPrices->getWholesalePrice($priceListRSD,$product)  : 0;
            $retailPrice = $productPrices->getRetailPrice($priceListRSD,$product)  ? $productPrices->getRetailPrice($priceListRSD,$product)  : 0;
            $wholesalePriceEUR = $productPrices->getWholesalePrice($priceListEUR,$product) ? $productPrices->getWholesalePrice($priceListEUR,$product)  : 0;
            //$objPHPExcel->getActiveSheet()->getRowDimension($rowPosition)->setRowHeight(120);
            $objPHPExcel->getActiveSheet()->getStyle("A".$rowPosition.":Q".$rowPosition)->applyFromArray($styleData);

            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowPosition, $product->getCode());
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowPosition, $product->getName());
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowPosition, $hydrator->getColor()->getShortColorName());
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowPosition, $product->getPriority());
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowPosition, array_pop(explode(' ', $hydrator->getProductGroup()->getName())));
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowPosition, $hydrator->getSubProductGroup()->getName());
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowPosition, ($hydrator->getSeason() ? $hydrator->getSeason()->getName() : ''));
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowPosition, ($hydrator->getOldCollection() ? $hydrator->getOldCollection()->getName() : ''));
            $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowPosition, ($hydrator->getGenderName()));
            $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowPosition, ($hydrator->getAllProductSizes() ? $hydrator->getAllProductSizes() : ''));
            $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowPosition, $purchasePrice);
            $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowPosition, $purchasePrice> 0 ? number_format($wholesalePrice/$purchasePrice-1,2)*100 .' %' : '' );
            $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowPosition, $wholesalePrice);
            $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowPosition, $wholesalePrice> 0 ? number_format($retailPrice/$wholesalePrice-1,2)*100 .' %' : '' );
            $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowPosition, $retailPrice);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowPosition, $wholesalePriceEUR);
//            $objDrawing = new PHPExcel_Worksheet_Drawing();
//            $objDrawing->setName($product->getCode());
//            $objDrawing->setDescription('test_img');
//            $objDrawing->setPath($product->getUploadPath() . $product->getImage());
//            $objDrawing->setCoordinates('A'.$rowPosition);
//setOffsetX works properly
//            $objDrawing->setOffsetX(5);
//            $objDrawing->setOffsetY(5);
////set width, height
//            //$objDrawing->setWidth(100);
//            $objDrawing->setHeight(150);
//            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

            $rowPosition++;
        }
        $tempFolder = rtrim(sys_get_temp_dir(), '/') . '/products-list';
        $fileName = $tempFolder . '/' . 'Export.xlsx';

        if (file_exists($fileName)) {
            unlink($fileName);
        }

        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);

        $mailManager = $this->getDI()->getMailManager();
        $message = $mailManager->createMessageFromView('reports/order-mail-template');

        $message->setSubject("Spisak proizvoda iz Elektronske Karte");
        $message->setTo($this->users);
        $message->setAttachment($fileName);
        $message->send();

            ob_end_clean();
    }
}