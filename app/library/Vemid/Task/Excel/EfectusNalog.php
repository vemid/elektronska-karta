<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 11/18/18
 * Time: 21:06
 */

namespace Vemid\Task\Excel;

use Vemid\Application\Di;
use Vemid\Date\DateTime;
use Vemid\Entity\Type;
use Vemid\Task\AbstractTask;
use Vemid\Service\GoogleTranslator\GoogleTranslateClient;

/**
 * Class ProductsExportTask
 * @package Vemid\Task\Excel
 */
class EfectusNalog extends AbstractTask
{

    /** @var string*/
    public $user;

    /** @var string */
    public $magacin;

    /** @var DateTime */
    public $date;

    /**
     * {@inheritdoc}
     */

    public function execute()
    {
        ob_start();

        $classificationCode = Type::CLASSIFICATION;


        $allProducts = $this->modelsManager->createBuilder()
            ->from(\Efectus::class)
            ->leftJoin(\ProductSize::class,"ps.id = Efectus.productSizeId", 'ps')
            ->leftJoin(\Code::class, "Efectus.toCodeId = c.id",'c')
            ->leftJoin(\Product::class,"ps.productId = p.id",'p')
            ->where('Efectus.fromCodeId  = :bindParam: ',
            [
                'bindParam' => $this->magacin
            ])
            ->andWhere('Efectus.type = :type: ',
            [
                'type' => 'DOPUNA_SA_CM'
            ])
            ->andWhere('Efectus.date = :date: ',
            [
                'date' => $this->date->getUnixFormat()
            ])
            ->orderBy('p.code,ps.codeId');


        $products = $allProducts->getQuery()->execute();

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()
            ->setCreator("Marko Vesic")
            ->setLastModifiedBy("BEBAKIDS")
            ->setTitle("Export podataka iz Excel-a")
            ->setSubject("Export podataka iz Excel-a")
            ->setDescription("Onog dana kada budete prestali da izmisljate, placam rucak u MB-u")
            ->setKeywords("Template excel");
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

        $styleData = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );

        $styleHeader = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '1C9EC9')
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->applyFromArray($styleHeader);

        $rows = array(
            array(
                'Slika',
                'Naziv',
                'Sifra',
                'Velicina',
                'Kolicina',
                'SifraObjekta',
                'Objekat',
                'Sezona',
                'Kolekcija',
                'Pol',
                'Velicine',
                'Nabavna Cena',
                'Marza',
                'VP Cena',
                'Marza',
                'MP Cena',
                'VPeur Cena'
            )
        );
        $objPHPExcel->getActiveSheet()->fromArray($rows,null, 'A1');

        $rowPosition = 2;
        /** @var \Efectus $product */
        foreach ($products as $product) {
            $hydrator = new \Vemid\Service\Product\Hydrator($product->getProductSize()->getProduct(), $this->getDI()->getEntityManager());

            $objPHPExcel->getActiveSheet()->getStyle("A".$rowPosition.":Q".$rowPosition)->applyFromArray($styleData);

            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowPosition, $product->getProductSize()->getProduct()->getName());
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowPosition, $product->getProductSize()->getProduct()->getCode());
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowPosition, $product->getProductSize()->getCode()->getName());
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowPosition, $product->getQty());
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowPosition, $product->getCodeTo()->getCode());
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowPosition, $product->getCodeTo()->getName());
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowPosition, ($hydrator->getSeason() ? $hydrator->getSeason()->getName() : ''));
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowPosition, ($hydrator->getOldCollection() ? $hydrator->getOldCollection()->getName() : ''));
            $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowPosition, "");
            $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowPosition, "");
            $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowPosition, "");
            $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowPosition, '' );
            $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowPosition, "");
            $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowPosition, "" );
            $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowPosition, "");
            $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowPosition, "");

            $rowPosition++;
        }
        $tempFolder = rtrim(sys_get_temp_dir(), '/');
        $fileName = $tempFolder . '/' . 'Export.xlsx';

        if (file_exists($fileName)) {
            unlink($fileName);
        }

        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);

        $emails = [
            'admin@bebakids.com' => 'Marko Vesic',
            'milos.civric@bebakids.com' => 'Milos Civric',
            'ninoslav.perisic@bebakids.com' => 'Ninoslab Perisic',
            'milos.pancic@bebakids.com' => 'Milos Pancic',
        ];

        $mailManager = $this->getDI()->getMailManager();
        $message = $mailManager->createMessageFromView('reports/email/efectus-mail-template');

        $message->setSubject("Nalog za dopunu sa CM - EFEKTUS");
        $message->setTo($this->user);
        $message->setCc($emails);
        $message->setAttachment($fileName);
        $message->send();

            ob_end_clean();

    }
}