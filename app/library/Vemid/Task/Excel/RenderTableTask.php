<?php

namespace Vemid\Task\Excel;

use Vemid\Table\Renderer\ExcelRenderer;
use Vemid\Table\Table;
use Vemid\Task\AbstractTask;

/**
 * Class RenderTask
 * @package Vemid\Task\Excel
 */
class RenderTableTask extends AbstractTask
{
    /** @var Table[] */
    public $tables;

    /** @var string */
    public $name;

    /** @var string */
    public $folder;

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        if (!is_dir($this->folder) && !mkdir($this->folder, 0777)) {
            throw new \LogicException('Dir can not be created!');
        }

        $fileName = $this->folder . '/' . $this->name;
        if (file_exists($fileName)) {
            unlink($fileName);
        }

        $renderer = new ExcelRenderer($this->tables, $this->name);
        $renderer->saveToFile($fileName);

        $mailManager = $this->getDI()->getMailManager();
        $message = $mailManager->createMessageFromView('reports/order-mail-template');

        $message->setSubject("Slanje Excela ");
        $message->setTo('admin@bebakids.com');
        $message->setBcc('darkovesic3@gmail.com');
        $message->setAttachment($fileName);
        $message->send();
    }
}