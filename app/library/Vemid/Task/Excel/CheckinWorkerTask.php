<?php


namespace Vemid\Task\Excel;

use Vemid\Date\DateTime;
use Vemid\Date\Time;
use Vemid\Entity\Repository\ProductionWorkerCheckinRepository;
use Vemid\Task\AbstractTask;

/**
 * Class PaymentReportBySectorAndWorkerTypeTask
 * @package Vemid\Task\Excel
 */
class CheckinWorkerTask extends AbstractTask
{
    /** @var \ProductionWorker */
    public $manager;

    /** @var \Code[] */
    public $sectors;

    /** @var array */
    public $managerData;

    /** @var \Vemid\Date\DateTime */
    public $dateFrom;

    /** @var \Vemid\Date\DateTime */
    public $dateTo;

    public function execute()
    {
        ob_start();

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()
            ->setCreator("Marko Vesic")
            ->setLastModifiedBy("BEBAKIDS")
            ->setTitle("Export podataka iz Excel-a")
            ->setSubject("Export podataka iz Excel-a")
            ->setDescription("Onog dana kada budete prestali da izmisljate, placam rucak u MB-u")
            ->setKeywords("Template excel");
//        $objPHPExcel->setActiveSheetIndex(0);
//
        $styleData = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );

        $styleHeader = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '1C9EC9')
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );

        $styleCellLate = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'DC143C')
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );
        $styleCellRegular = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FF8C00')
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );

        /**
         * @var  $key
         * @var  $sector
         */
        $i = 0;
        foreach ($this->managerData as $key => $sector) {
            $objPHPExcel->createSheet($i);
            $objWorkSheet = $objPHPExcel->setActiveSheetIndex($i);
            $objWorkSheet->setTitle($key);
            $objWorkSheet->getStyle('A1:F1')->applyFromArray($styleHeader);
            $rows = array(
                array(
                    'Radnik',
                    'Dan',
                    'Start_time',
                    'End_time',
                    'Status',
                    'Fond sati',
                )
            );
            $objWorkSheet->fromArray($rows, null, 'A1');
            $i++;

            $rowPosition = 2;
            $dateRanges = (new \Vemid\Date\DateRange($this->dateFrom, $this->dateTo))->getAllDates();
            /** @var \ProductionWorker $worker */
            foreach ($this->managerData[$key] as $worker) {
                foreach ($dateRanges as $date) {

                    if (date("l", strtotime($date)) <> "Sunday") {

                        $workerData = $worker->getProductionWorkerCheckin(["date = '" . $date->format("Y-m-d") . "'"]);
                        $workerCheckin = $workerData->toArray();
                        $status1 = "";
                        $status2 = "";
                        if ($workerCheckin[0]['checkIn']) {
                            $checkInTime = new DateTime($workerCheckin[0]['checkIn']);
                            if ($checkInTime > new DateTime($worker->getStartTime())) {
                                if ($workerCheckin[0]['checkOut']) {
                                    $checkOutTime = new DateTime($workerCheckin[0]['checkOut']);
                                    $dteDiff = $checkInTime->diff($checkOutTime);
                                    $timeCheck = new DateTime("08:00:00");
                                    if ($dteDiff->h < 8) {
                                        $status2 = "Nepotpun fond sati";
                                    } else $status2 = "Pun fond sati";
                                } else $status2 = "Nije se odjavio";
                                $status1 = "Kasnio";
                            } else
                                $status1 = "Regularno";
                        }
                        $objWorkSheet->getColumnDimension('A')->setAutoSize(true);
                        $objWorkSheet->getColumnDimension('B')->setAutoSize(true);
                        $objWorkSheet->getColumnDimension('C')->setAutoSize(true);
                        $objWorkSheet->getColumnDimension('D')->setAutoSize(true);
                        $objWorkSheet->getColumnDimension('E')->setAutoSize(true);
                        $objWorkSheet->getColumnDimension('F')->setAutoSize(true);

                        $objWorkSheet->getStyle("A" . $rowPosition . ":E" . $rowPosition)->applyFromArray($styleData);
                        $objWorkSheet->SetCellValue('A' . $rowPosition, $worker->getDisplayName());
                        $objWorkSheet->SetCellValue('B' . $rowPosition, $date->getShortSerbianFormat());
                        $objWorkSheet->SetCellValue('C' . $rowPosition, $workerCheckin[0]['checkIn'] ? $workerCheckin[0]['checkIn'] : "");
                        $objWorkSheet->SetCellValue('D' . $rowPosition, $workerCheckin[0]['checkOut'] ? $workerCheckin[0]['checkOut'] : "");
                        $objWorkSheet->SetCellValue('E' . $rowPosition, $status1);
                        $objWorkSheet->SetCellValue('F' . $rowPosition, $status2);
                        if ($status2 == "Nepotpun fond sati" && $status1 == "Kasnio") {
                            $objWorkSheet->getStyle("E" . $rowPosition)->applyFromArray($styleCellLate);
                        } elseif ($status1 == "Kasnio" && $status2 == "Pun fond sati") {
                            $objWorkSheet->getStyle("E" . $rowPosition)->applyFromArray($styleCellRegular);

                        }
                        $rowPosition++;
                    }
                }
            }

            $objWorkSheet->setAutoFilter("A1:F1");

        }
        $tempFolder = rtrim(sys_get_temp_dir(), '/') . '/products-list';
        $fileName = $tempFolder . '/' . $this->manager->getName() . '.xlsx';

        if (file_exists($fileName)) {
            unlink($fileName);
        }

        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);

        $mailManager = $this->getDI()->getMailManager();
        $message = $mailManager->createMessageFromView('reports/order-mail-template');

        $message->setSubject("Izvestaj dolazaka " . $this->dateFrom->format("m") . "-" . $this->dateFrom->format("Y") . " || Manager : " . $this->manager->getDisplayName());
        $message->setTo($this->manager->getEmail());
        $message->setCc('ninoslav.perisic@bebakids.com');
        $message->setBcc('admin@bebakids.com');
        $message->setAttachment($fileName);
        $message->send();

    }

}