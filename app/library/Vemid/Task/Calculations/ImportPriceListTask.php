<?php

namespace Vemid\Task\Calculations;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Task\AbstractTask;

/**
 * Class ImportPriceListTask
 * @package Vemid\Task\Attributes
 */
class ImportPriceListTask extends AbstractTask
{
    /** @var array */
    public $priceListData;
    
    /**
     * @return mixed
     *
     * @throws \DomainException
     */
    public function execute()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->getEntityManager();

        /** @var \Pricelist $priceList */
        $priceList = $entityManager->findOne(\Pricelist::class, [
            \Pricelist::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->priceListData['oznaka']
            ]
        ]);

        if ($priceList) {
            $update = false;
            if ($priceList->getCode() !== $this->priceListData['oznaka']) {
                $update = true;
                $priceList->setCode($this->priceListData['oznaka']);
            }

            if ($priceList->getName() !== $this->priceListData['naziv']) {
                $update = true;
                $priceList->setName($this->priceListData['naziv']);
            }

            if ($priceList->getCurrency() !== $this->priceListData['valuta']) {
                $update = true;
                $priceList->setCurrency($this->priceListData['valuta']);
            }

            if ($update) {
                if (!$entityManager->save($priceList)) {
                    throw new \DomainException('Fail to save price list');
                }
            }
        } else {
            $priceList = new \Pricelist();
            $priceList->setCode($this->priceListData['oznaka']);
            $priceList->setCurrency($this->priceListData['valuta']);
            $priceList->setName($this->priceListData['naziv']);

            if (!$entityManager->save($priceList)) {
                throw new \DomainException('Fail to save price list');
            }
        }
    }
}
