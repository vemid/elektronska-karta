<?php

namespace Vemid\Task\OperatingList;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Task\AbstractTask;

class FinishTask extends AbstractTask
{

    /**
     * @return mixed
     */
    public function execute()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');

        /** @var \OperatingList[] $operatingLists */
        $operatingLists = $entityManager->find(\OperatingList::class, [
            \OperatingList::PROPERTY_FINISHED . ' = :finished: OR ' .
            \OperatingList::PROPERTY_FINISHED . ' IS NULL',
            'bind' => [
                'finished' => false
            ]
        ]);

        foreach ($operatingLists as $operatingList) {
            $totalItems = $operatingList->getOperatingListItems()->count();
            $totalItemsFinished = $operatingList->getOperatingListItems([
                \OperatingListItem::PROPERTY_FINISHED . ' = :finished:',
                'bind' => [
                    'finished' => true
                ]
            ])->count();

            if($totalItems>0) {

                if ($totalItems <= $totalItemsFinished) {
                    $operatingList->setFinished(true);
                    $operatingList->setLocked(true);
                    if (!$entityManager->save($operatingList)) {
                        throw new \DomainException('Fail to finish operation list');
                    }
                }
            }
        }
    }
}