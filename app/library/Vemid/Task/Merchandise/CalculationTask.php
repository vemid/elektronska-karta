<?php

namespace Vemid\Task\Merchandise;

use Vemid\Date\DateTime;
use Vemid\Service\Product\MerchandisePrice;
use Vemid\Task\AbstractTask;

/**
 * Class CalculationTask
 * @package Vemid\Task\Merchandise
 */
class CalculationTask extends AbstractTask
{
    /** @var array */
    public $data;

    /** @var int */
    public $merchandiseCalculationId;

    /**
     * @return mixed
     */
    public function execute()
    {
        $entityManager = $this->getDI()->getEntityManager();

        /** @var \Product $product */
        $product = $entityManager->findOne(\Product::class, [
            \Product::PROPERTY_CODE . ' = :code: ',
            'bind' => [
                'code' => $this->data['productCode']
            ]
        ]);

        /** @var \MerchandiseCalculationItem $merchandiseCalculationItem */
        $merchandiseCalculationItem = new \MerchandiseCalculationItem();
        $merchandiseCalculationItem->setMerchandiseCalculationId($this->merchandiseCalculationId);
        $merchandiseCalculationItem->setProduct($product);
        $merchandiseCalculationItem->setCourse($this->data['course']);
        $merchandiseCalculationItem->setPurchasePrice($this->data['purchasePrice']);
        $merchandiseCalculationItem->setWholesalePrice($this->data['wholesalePrice']);
        $merchandiseCalculationItem->setRetailPrice($this->data['retailPrice']);
        $merchandiseCalculationItem->setCreated((new DateTime())->getUnixFormat());


        if (!$entityManager->save($merchandiseCalculationItem)) {
            throw new \DomainException('Merchandise calculation Item failed to save!');
        }

        $addPrices = new MerchandisePrice($entityManager, $merchandiseCalculationItem, $this->getDI()->getModelsManager());
        $addPrices->addPrices();
    }
}
