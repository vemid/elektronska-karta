<?php

namespace Vemid\Task\Invoices;

use Code;
use CodeType;
use DomainException;
use Invoice;
use InvoiceItem;
use Phalcon\Logger;
use Product;
use ProductSize;
use Vemid\Entity\EntityInterface;
use Vemid\Task\AbstractTask;

/**
 * Class ImportInvoiceItemsTask
 * @package Vemid\Task\Invoices
 */
class ImportInvoiceItemsTask extends AbstractTask
{
    /** @var array */
    public $data;

    /**
     * @return mixed
     * @throws DomainException
     */
    public function execute()
    {
        $this->getDI()->getLogger()->log(Logger::INFO, sprintf('ProductSize with product code: %s and size code: %s!', $this->data['product'], $this->data['size']));
        if ($productSize = $this->findProductSize()) {

            /** @var Invoice $invoice */
            $invoice = $this->getDI()->getEntityManager()->findOne(Invoice:: class, [
                Invoice::PROPERTY_INVOICE . ' = :invoice: ',
                'bind' => [
                    'invoice' => trim($this->data['ozn_rac_izv'])
                ]
            ]);

            if ($invoice) {

                /** @var InvoiceItem $invoiceItem */
                $invoiceItem = $this->getDI()->getEntityManager()->findOne(InvoiceItem:: class, [
                    InvoiceItem::PROPERTY_PRODUCT_SIZE_ID . ' = :productSizeId: AND ' .
                    InvoiceItem::PROPERTY_INVOICE_ID . ' = :invoiceId:',
                    'bind' => [
                        'productSizeId' => $productSize->getId(),
                        'invoiceId' => $invoice->getId()
                    ]
                ]);

                if (!$invoiceItem) {

                    $invoiceItem = new InvoiceItem();
                    $invoiceItem->setInvoice($invoice);
                    $invoiceItem->setProductSize($productSize);
                    $invoiceItem->setPrice($this->data['price']);
                    $invoiceItem->setQty($this->data['kolic']);
                    $invoiceItem->setTariffCode($this->data['tariff_code']);

                } else {

                    if ($invoiceItem->getTariffCode() <> $this->data['tariff_code']) {
                        $invoiceItem->setTariffCode($this->data['tariff_code']);
                    }
                }

                if (!$this->getDI()->getEntityManager()->save($invoiceItem)) {
                    throw new DomainException('InvoicItem not saved!');
                }
            }
        }
    }


    /**
     * @return ProductSize|EntityInterface|null
     * @throws DomainException
     */
    private function findProductSize()
    {
        $product = $this->findProduct();
        $code = $this->findSize();

        if ($product && $code) {
            return $this->getDI()->getEntityManager()->findOne(ProductSize::class, [
                ProductSize::PROPERTY_PRODUCT_ID . ' = :productId: AND ' .
                ProductSize::PROPERTY_CODE_ID . ' = :codeId:',
                'bind' => [
                    'productId' => $product->getId(),
                    'codeId' => $code->getId()
                ]
            ]);
        }
    }

    /**
     * @return Product|EntityInterface|null
     */
    public function findProduct()
    {
        return $this->getDI()->getEntityManager()->findOne(Product::class, [
            Product::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->data['product']
            ]
        ]);
    }

    /**
     * @return Code|EntityInterface|null
     * @throws DomainException
     */
    public function findSize()
    {
        /** @var CodeType $sizeCodeType */
        $sizeCodeType = $this->getDI()->getEntityManager()->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SIZES
            ]
        ]);

        if (!$sizeCodeType) {
            throw new DomainException('CodeType sizes not found!');
        }

        return $this->getDI()->getEntityManager()->findOne(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'codeTypeId' => $sizeCodeType->getId(),
                'code' => $this->data['size']
            ]
        ]);
    }


}
