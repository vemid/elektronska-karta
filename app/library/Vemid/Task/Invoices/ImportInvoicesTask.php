<?php

namespace Vemid\Task\Invoices;

use Phalcon\Logger;
use Vemid\Date\DateTime;
use Vemid\Entity\EntityInterface;
use Vemid\Task\AbstractTask;

/**
 * Class ImportInvoicesTask
 * @package Vemid\Task\Standardization
 */
class ImportInvoicesTask extends AbstractTask
{
    /** @var array */
    public $data;

    /**
     * @return mixed
     * @throws \DomainException
     */
    public function execute()
    {
        $this->getDI()->getLogger()->log(Logger::ERROR, sprintf('Client with invoice %s and code: %s!', $this->data['ozn_rac_izv'], $this->data['sif_par']));
        if ($client = $this->findClient()) {

            /** @var \Invoice $invoice */
            $invoice = $this->getDI()->getEntityManager()->findOne(\Invoice:: class, [
                \Invoice::PROPERTY_INVOICE . ' = :invoice: ',
                'bind' => [
                    'invoice' => trim($this->data['ozn_rac_izv'])
                ]
            ]);

            if (!$invoice) {
                $invoice = new \Invoice();
                $invoice->setClient($client);
                $invoice->setInvoice(trim($this->data['ozn_rac_izv']));
                $invoice->setDate($this->data['dat_rac']);
                $invoice->setBoxQty($this->data['box_qty']);
                $invoice->setColetsQty($this->data['colets_qty']);
                $invoice->setBrutoWeight($this->data['bruto_weight']);
                $invoice->setNetoWeight($this->data['neto_weight']);
                $invoice->setCustom($this->data['custom']);
                if($this->data['statement'] ) {
                    $string = substr($this->data['statement'], 0, strpos($this->data['statement'], "."));
                    $invoice->setStatement($string);
                    }
                else{ $invoice->setStatement(null);}
                $invoice->setDate($this->data['dat_rac']);
                $invoice->setCreated(new DateTime());

            }

            if(trim($this->data['storno']) == "N") {
                $invoice->setStatus(true);
            }
            else {
                $invoice->setStatus(false);
            }
            $invoice->setAmount($this->data['izn_dev']);


            if (!$this->getDI()->getEntityManager()->save($invoice)) {
                throw new \DomainException('Invoice not saved!');
            }
        }
    }


    /**
     * @return \Client|EntityInterface|null
     */
    public function findClient()
    {
        return $this->getDI()->getEntityManager()->findOne(\Client::class, [
            \Client::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->data['sif_par']
            ]
        ]);
    }

}
