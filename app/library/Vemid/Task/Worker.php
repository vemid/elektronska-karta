<?php

namespace Vemid\Task;

use Phalcon\Db\AdapterInterface;
use Vemid\Entity\Manager\EntityManagerInterface;


/**
 * Class Worker
 *
 * @package Vemid\Task
 * @author Vemid
 */
class Worker
{
    /** @var Queue */
    protected $queue;

    /** @var AdapterInterface */
    protected $db;

    protected $terminateAfterCompletingJob = false;

    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * @param Queue $queue
     * @param AdapterInterface $db
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(Queue $queue, AdapterInterface $db, EntityManagerInterface $entityManager)
    {
        $this->queue = $queue;
        $this->db = $db;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        $this->registerSignalHandler();
        $this->queue->watch($this->queue->getTube());

        $timeLimit = time() + 3600; // Kill worker after 1 hour

        do {
            $this->reserveAndExecuteJob();

            if (\memory_get_usage(true) > (200 * 1024 * 1024)) { // 200 Mb
                printf("\n[Shutting down worker with high memory usage of %s", \memory_get_usage(true));
                exit(1);
            }

            pcntl_signal_dispatch(); // If you press CTRL+C (SIGINT) or give SIGTERM this will terminate the worker
        } while (time() < $timeLimit && !$this->terminateAfterCompletingJob);

        exit(2); // Ensures that supervisord restarts this thread because of time limit expiring
    }

    /**
     * @throws \Exception
     */
    private function reserveAndExecuteJob()
    {
        if ($job = $this->queue->reserve(5)) {
            $maxAttempts = 2;
            for ($attempt = 1; $attempt <= $maxAttempts; $attempt++) {
                try {
                    $task = AbstractTask::factoryFromJson($job->getBody());
                    $task->run();
                    $job->delete();
                    break;
                } catch (\PDOException $e) {
                    if ($attempt < $maxAttempts && strpos($e->getMessage(), 'gone away') !== false) {
                        $this->db->connect();
                        sleep(1);
                        continue;
                    }
                } catch (\Exception $e) {
                    $job->bury();
                    throw $e;
                }
            }
        }
    }

    /**
     * Registers TERMINATE and INTERRUPT signals handler
     */
    public function registerSignalHandler()
    {
        if (!function_exists('pcntl_signal')) {
            die('Requires pcntl PHP extension. http://php.net/manual/en/intro.pcntl.php');
        }

        \pcntl_signal(\SIGTERM, [$this, 'signalHandler']);
        \pcntl_signal(\SIGINT, [$this, 'signalHandler']);
    }

    /**
     * @param int $signal
     */
    public function signalHandler($signal)
    {
        switch ($signal) {
            case \SIGTERM:
            case \SIGINT:
                $this->terminateAfterCompletingJob = true;
                printf("\n[Terminating with %s]", $signal === \SIGTERM ? 'SIGTERM' : 'SIGINT');
                break;
            default:
                printf("\n[Terminating with %s signal that was caught]", $signal);
                exit(1);
        }
    }
}
