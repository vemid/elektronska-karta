<?php

namespace Vemid\Task\Emails;

use Vemid\Date\DateTime;
use Vemid\Mailer\Manager;
use Vemid\Task\AbstractTask;

/**
 * Class SendOutboundEmails
 * @package Vemid\Task\Emails
 */
class SendOutboundEmails extends AbstractTask
{
    /** @var \OutboundEmail */
    public $outboundEmail;
    
    /** @var Manager $mailManager */
    public $mailManager;
    
    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $message = $this->mailManager->createMessageFromView('mail-template/default', [
            'body' => $this->outboundEmail->getBody()
        ]);
        $message->setFrom($this->outboundEmail->getFromAddress());
        $message->setTo($this->outboundEmail->getToAddress());
        $message->setCc($this->outboundEmail->getCcAddress());
        $message->setBcc($this->outboundEmail->getBccAddress());
        $message->setSubject($this->outboundEmail->getSubject());

        if ($message->send()) {
            $this->outboundEmail->setSentDatetime(new DateTime())->save();
        }
    }
}
