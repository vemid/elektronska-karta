<?php

namespace Vemid\Task\OperatingListItem;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Repository\OperatingListItemWorksheetRepository;
use Vemid\Entity\Repository\ProductionOrderRepository;
use Vemid\Task\AbstractTask;

/**
 * Class FinishTask
 * @package Vemid\Task\OperatingListItem
 */
class FinishTask extends AbstractTask
{
    public function execute()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');

        /** @var \OperatingList[] $operatingLists */
        $operatingLists = $entityManager->find(\OperatingList::class, [
            \OperatingList::PROPERTY_FINISHED . ' = :finished: OR '.
            \OperatingList::PROPERTY_FINISHED . ' IS NULL',
            'bind' => [
                'finished' => false
            ]
        ]);

        foreach ($operatingLists as $operatingList) {
            if (!$product = $operatingList->getProduct()) {
                continue;
            }

            /** @var ProductionOrderRepository $totals */
            $totals = $entityManager->getRepository(\ProductionOrder::class);
            $totalPlanned = $totals->getTotalPlanned($operatingList->getProduct());

            foreach ($operatingList->getOperatingListItems() as $operatingListItem) {
                if ($operatingListItem->getFinished()) {
                    continue;
                }

                /** @var OperatingListItemWorksheetRepository $operatingListItemWorksheetRepository */
                $operatingListItemWorksheetRepository = $entityManager->getRepository(\OperatingListItemWorksheet::class);
                $totalItemDone = $operatingListItemWorksheetRepository->getTotalPerItem($operatingListItem);

                if($totalPlanned>0) {
                    if ($totalItemDone >= $totalPlanned) {
                        $operatingListItem->setFinished(true);
                        if (!$entityManager->save($operatingListItem)) {
                            throw new \DomainException('Fail to finish operation list item');
                        }
                    }
//                    else {
//                        $operatingListItem->setFinished(false);
//                        if (!$entityManager->save($operatingListItem)) {
//                            throw new \DomainException('Fail to finish operation list item');
//                        }
//                    }
                }

            }
        }
    }
}