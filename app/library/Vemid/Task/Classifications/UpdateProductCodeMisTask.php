<?php

namespace Vemid\Task\Classifications;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Service\MisWsdl\PutProductSize;
use Vemid\Service\MisWsdl\UpdateProduct;
use Vemid\Task\AbstractTask;

/**
 * Class UpdateProductCodeMisTask
 * @package Vemid\Task\Classifications
 */
class UpdateProductCodeMisTask extends AbstractTask
{
    /** @var \Product */
    public $product;

    /** @var  string */
    public $userName;

    /**
     * @return mixed
     */
    public function execute()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->getEntityManager();
        $service = new UpdateProduct($this->product, $entityManager, $this->userName);
        $service->run();
    }
}
