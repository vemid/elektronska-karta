<?php

namespace Vemid\Task\Classifications;

use Vemid\Task\AbstractTask;

/**
 * Class PullFromMis
 * @package Vemid\Task\Classifications
 */
class ImportClassificationsTask extends AbstractTask
{
    /** @var array */
    public $classificationData;

    /**
     * @return mixed
     *
     * @throws \DomainException
     */
    public function execute()
    {
        $classificationTypeCode = $this->findClassificationTypeCode(trim($this->classificationData['sif_kri_kla']));
        $classificationCategoryCode = $this->findClassificationCategoryCode(trim($this->classificationData['vrs_kla']));
        $this->importClassification(
            $classificationTypeCode,
            $classificationCategoryCode,
            trim($this->classificationData['kla_ozn']),
            trim($this->classificationData['naz_kla']),
            !\is_array($this->classificationData['nad_kla_ozn']) ? trim($this->classificationData['nad_kla_ozn']) : $this->classificationData['nad_kla_ozn']
        );
    }

    /**
     * @param $codeString
     * @return \Code
     *
     * @throws \DomainException
     */
    private function findClassificationTypeCode($codeString): \Code
    {
        /** @var \CodeType $codeType */
        $codeType = $this->getDI()->get('entityManager')->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        if (!$codeType) {
            throw new \DomainException('Classification CodeType do not exist');
        }

        /** @var \Code $code */
        $code = $this->getDI()->get('entityManager')->findOne(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' . \Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'codeTypeId' => $codeType->getEntityId(),
                'code' => $codeString
            ]
        ]);

        if (!$code) {
            throw new \DomainException(sprintf('Code was not found (%s)', $codeString));
        }

        return $code;
    }

    /**
     * @param $codeString
     * @return \Code
     *
     * @throws \DomainException
     */
    private function findClassificationCategoryCode($codeString): \Code
    {
        /** @var \CodeType $codeType */
        $codeType = $this->getDI()->get('entityManager')->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        if (!$codeType) {
            throw new \DomainException('Classification Category CodeType do not exist');
        }

        /** @var \Code $code */
        $code = $this->getDI()->get('entityManager')->findOne(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' . \Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'codeTypeId' => $codeType->getEntityId(),
                'code' => $codeString
            ]
        ]);

        if (!$code) {
            throw new \DomainException(sprintf('Code was not found (%s)', $codeString));
        }

        return $code;
    }

    /**
     * @param \Code $classificationTypeCode
     * @param \Code $classificationCategoryCode
     * @param string $code
     * @param $name
     * @param null|string|array $parentClassificationCode
     * @return \Classification|\Vemid\Entity\EntityInterface
     *
     * @throws \DomainException
     */
    private function importClassification(\Code $classificationTypeCode, \Code $classificationCategoryCode, $code, $name, $parentClassificationCode = null): \Classification
    {
        /** @var \Classification $classification */
        $classification = $this->getDI()->get('entityManager')->findOne(\Classification::class, [
            \Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :classificationTypeCodeId: AND ' .
            \Classification::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'classificationTypeCodeId' => $classificationTypeCode->getEntityId(),
                'code' => $code
            ]
        ]);

        if ($classification) {
            if ($classification->getName() !== $name) {
                $classification->setName($name);

                if (!$this->getDI()->get('entityManager')->save($classification)) {
                    throw new \DomainException('Classification name was not updated');
                }
            }

            $parentClassificationOldCode = null;
            if ($parentClassification = $classification->getParentClassification()) {
                $parentClassificationOldCode = $parentClassification->getCode();
            }

            if ($parentClassificationCode && $parentClassificationOldCode !== $parentClassificationCode) {
                $parentClassification = $this->getDI()->get('entityManager')->findOne(\Classification::class, [
                    \Classification::PROPERTY_CODE . ' = :code:',
                    'bind' => [
                        'code' => $parentClassificationCode
                    ]
                ]);

                if ($parentClassification) {
                    $classification->setParentClassification($parentClassification);

                    if (!$this->getDI()->get('entityManager')->save($classification)) {
                        throw new \DomainException('Parent Classification was not updated');
                    }
                }
            }

            return $classification;
        }

        $parentClassification = null;
        if ($parentClassificationCode && !\is_array($parentClassificationCode)) {
            $parentClassification = $this->getDI()->get('entityManager')->findOne(\Classification::class, [
                \Classification::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'code' => $parentClassificationCode
                ]
            ]);
        }

        $classification = new \Classification();
        $classification->setClassificationType($classificationTypeCode);
        $classification->setClassificationCategory($classificationCategoryCode);
        $classification->setCode($code);
        $classification->setName($name);

        if ($parentClassification) {
            $classification->setParentClassification($parentClassification);
        }

        if (!$this->getDI()->get('entityManager')->save($classification)) {
            throw new \DomainException('Classification was not saved');
        }

        return $classification;
    }
}
