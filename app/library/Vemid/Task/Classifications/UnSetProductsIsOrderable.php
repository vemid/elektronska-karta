<?php

namespace Vemid\Task\Classifications;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Task\AbstractTask;

/**
 * Class UnSetProductsIsOrderable
 * @package Vemid\Task\Classifications
 */
class UnSetProductsIsOrderable extends AbstractTask
{
    /** @var \Classification */
    public $classification;

    public function execute()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->getEntityManager();

        /** @var \ProductClassification[] $productClassifications */
        $productClassifications = $entityManager->find(\ProductClassification::class, [
            \ProductClassification::PROPERTY_ENTITY_ID . ' = :entityId: AND '.
            \ProductClassification::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:',
            'bind' => [
                'entityId' => $this->classification->getId(),
                'entityTypeId' => Type::CLASSIFICATION
            ]
        ]);

        $products = [];
        foreach ($productClassifications as $productClassification) {
            $product = $productClassification->getProduct();
            if (!$product || !$product->getIsOrderable()) {
                continue;
            }

            $products[$product->getId()] = $product;
        }

        /** @var \Product $productT */
        foreach ($products as $productT) {
            $productT->setIsOrderable(false);
            $entityManager->save($productT);
        }
    }
}
