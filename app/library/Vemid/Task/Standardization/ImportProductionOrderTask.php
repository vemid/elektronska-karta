<?php

namespace Vemid\Task\Standardization;

use Phalcon\Logger;
use Vemid\Date\DateTime;
use Vemid\Entity\EntityInterface;
use Vemid\Entity\Repository\ProductSizeRepository;
use Vemid\Task\AbstractTask;
use Vemid\Service\ProductionOrder;

/**
 * Class ImportProductionOrderTask
 * @package Vemid\Task\Standardization
 */
class ImportProductionOrderTask extends AbstractTask
{
    /** @var array */
    public $data;

    /**
     * @return mixed
     * @throws \DomainException
     */
    public function execute()
    {
        $this->getDI()->getLogger()->log(Logger::INFO, sprintf('ProductSize with product code: %s and size code: %s!', $this->data['product'], $this->data['size']));
        if ($productSize = $this->findProductSize()) {

            /** @var \ProductionOrder $productionOrder */
            $productionOrder = $this->getDI()->getEntityManager()->findOne(\ProductionOrder:: class, [
                \ProductionOrder::PROPERTY_PRODUCT_SIZE_ID . ' = :productSizeId: AND ' .
                \ProductionOrder::PROPERTY_WORK_ORDER . ' = :workOrder:',
                'bind' => [
                    'productSizeId' => $productSize->getId(),
                    'workOrder' => $this->data['work_order']
                ]
            ]);

            if (!$productionOrder) {
                $productionOrder = new \ProductionOrder();
                $productionOrder->setProductSize($productSize);
                $productionOrder->setEmailSent(false);
            }

            $planedQty = $this->data['real_pla_kol'];
            $realizedQty = $this->data['realized_qty_a']+$this->data['realized_qty_b'];

            $productionOrder->setWorkOrder($this->data['work_order']);
            $productionOrder->setPlanedQty($this->data['pla_kol']);
            $productionOrder->setRealizedAQty($this->data['realized_qty_a']);
            $productionOrder->setRealizedBQty($this->data['realized_qty_b']);
            $productionOrder->setReceivedAQty($this->data['received_qty_a']);
            $productionOrder->setReceivedBQty($this->data['received_qty_b']);
            $productionOrder->setType($this->data['type']);
            if($planedQty <= $realizedQty && $realizedQty>0) {
                $productionOrder->setEmailSent(true);
            }

            if (!$this->getDI()->getEntityManager()->save($productionOrder)) {
                throw new \DomainException('Production Order not saved!');
            }

            $product = $productSize->getProduct();
            $size = $productSize->getCode();
            $workOrder = $productionOrder->getWorkOrder();

            $productSizeRepository = new ProductSizeRepository($this->getDI()->getModelsManager());
            $mpData = $productSizeRepository->getTotalMPOrder($productSize);

            $body = <<<HTML
Postovani,<br><br>
obavestavamo Vas da je za proizvod : $product , velicine : $size, radni nalog : $workOrder <br>
doslo do razlike u raspisanoj i iskrojenoj kolicini.<br><br>

Raspisana kolicina : $planedQty , iskrojena kolicina : $realizedQty<br><br>

MP porucena kolicina: {$productSizeRepository->getTotalMPOrder($productSize)} ,<br>
VP porucena kolicina: {$productSizeRepository->getTotalVPOrder($productSize)}<br><br>
HTML;


            if($planedQty > $realizedQty && $realizedQty>0 && !$productionOrder->getEmailSent()) {
                $outboundEmail = new \OutboundEmail();
                $outboundEmail->setToAddress("krojacnica@bebakids.com");
                $outboundEmail->setFromAddress("server@bebakids.com");
                $outboundEmail->setSubject("Razlika u kolicini ") ;
                $outboundEmail->setBody($body);
                $outboundEmail->setQueuedDatetime(new DateTime());
                $this->getDI()->getEntityManager()->save($outboundEmail);
                $productionOrder->setEmailSent(true);

                if (!$this->getDI()->getEntityManager()->save($productionOrder)) {
                    throw new \DomainException('Production Order not saved!');
                }
            }

            if($realizedQty>0) {
                $check = new ProductionOrder\ValidateProductionOrderQty($this->getDI()->getEntityManager(), $productSize);
                $check->validator();
            }
        }
    }

    /**
     * @return \ProductSize|EntityInterface|null
     * @throws \DomainException
     */
    private function findProductSize()
    {
        $product = $this->findProduct();
        $code = $this->findSize();

        if ($product && $code) {
            return $this->getDI()->getEntityManager()->findOne(\ProductSize::class, [
                \ProductSize::PROPERTY_PRODUCT_ID . ' = :productId: AND ' .
                \ProductSize::PROPERTY_CODE_ID . ' = :codeId:',
                'bind' => [
                    'productId' => $product->getId(),
                    'codeId' => $code->getId()
                ]
            ]);
        }
    }

    /**
     * @return \Product|EntityInterface|null
     */
    public function findProduct()
    {
        return $this->getDI()->getEntityManager()->findOne(\Product::class, [
            \Product::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->data['product']
            ]
        ]);
    }

    /**
     * @return \Code|EntityInterface|null
     * @throws \DomainException
     */
    public function findSize()
    {
        /** @var \CodeType $sizeCodeType */
        $sizeCodeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::SIZES
            ]
        ]);

        if (!$sizeCodeType) {
            throw new \DomainException('CodeType sizes not found!');
        }

        return $this->getDI()->getEntityManager()->findOne(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
            \Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'codeTypeId' => $sizeCodeType->getId(),
                'code' => $this->data['size']
            ]
        ]);
    }
}
