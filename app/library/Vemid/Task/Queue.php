<?php

namespace Vemid\Task;

use Phalcon\Queue\Beanstalk;

/**
 * Class Queue
 *
 * @package Vemid\Task
 * @author Vemid
 */
class Queue extends Beanstalk
{

    protected $tube = 'myTube';

    /**
     * @param TaskInterface $task
     * @param int $priority
     * @param int $delay
     * @param int $ttr
     * @return null|string
     */
    public function enqueue(TaskInterface $task, $priority = 1024, $delay = 0, $ttr = 1800)
    {
        $jobId = null;
        $maxAttempts = 3;

        for ($attempt = 1; $attempt <= $maxAttempts; $attempt++) {
            try {
                $data = json_encode($task->toArray(), $options = 0, $depth = 4096);
                $options = ['priority' => $priority, 'delay' => $delay, 'ttr' => $ttr];
                $this->choose($this->tube);
                $jobId = $this->put($data, $options);
                break;
            } catch (\Exception $e) {
                if ($attempt < $maxAttempts) {
                    usleep(200000);
                    continue;
                }
            }
        }

        return $jobId;
    }

    /**
     * @return string
     */
    public function getTube()
    {
        return $this->tube;
    }

    /**
     * @param string $tube
     */
    public function setTube($tube)
    {
        $this->tube = $tube;
    }

}
