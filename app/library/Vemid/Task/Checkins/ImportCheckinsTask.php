<?php

namespace Vemid\Task\Checkins;

use Vemid\Date\DateTime;
use Vemid\Entity\EntityInterface;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Task\AbstractTask;

/**
 * Class ImportCheckinDataTask
 * @package Vemid\Task\Checkins
 */
class ImportCheckinsTask extends AbstractTask
{
    /** @var array */
    public $dataCheckin;

    /**
     * @throws \DomainException
     */
    public function execute()
    {
        if (!$productionWorker = $this->findUser()) {
//           throw new \DomainException('worker not found!' . $this->dataCheckin['sifra']);

        }


        $this->createOrUpdateCheckin($productionWorker);
    }

    /**
     * @return \ProductionWorker|EntityInterface
     * @throws \DomainException
     */
    private function findUser()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');


       return $entityManager->findOne(\ProductionWorker::class, [
            \ProductionWorker::PROPERTY_CHECKIN_CODE . ' = :checkinCode:',
            'bind' => [
                'checkinCode' => $this->dataCheckin['sifra']
            ]
        ]);
    }


    /**
     * @param \ProductionWorker $productionWorker
     * @throws \DomainException
     */
    private function createOrUpdateCheckin(\ProductionWorker $productionWorker)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');

        /** @var \ProductionWorkerCheckin $productionWorkerCheckin */
        $productionWorkerCheckin = $entityManager->findOne(\ProductionWorkerCheckin::class, [
            \ProductionWorkerCheckin::PROPERTY_WORKER_ID . ' = :worker: AND ' . \ProductionWorkerCheckin::PROPERTY_DATE . ' = :date:',
            'bind' => [
                'worker' => $productionWorker->getId(),
                'date' => $this->dataCheckin['date']
            ]
        ]);


        if (!$productionWorkerCheckin) {
            $productionWorkerCheckin = new \ProductionWorkerCheckin();
        }

        $productionWorkerCheckin->setWorker($productionWorker);
        $productionWorkerCheckin->setDate($this->dataCheckin['date']);
        $productionWorkerCheckin->setCheckIn($this->dataCheckin['check_in']);
        $productionWorkerCheckin->setCheckOut($this->dataCheckin['check_out']);
        $productionWorkerCheckin->setCreated((new DateTime())->getUnixFormat());


        if (!$entityManager->save($productionWorkerCheckin)) {
            throw new \DomainException('Warrant shop was not saved!');
        }
    }
}