<?php

namespace Vemid\Task;

use Vemid\Entity\Serializer;
use Phalcon\Di;

/**
 * Class AbstractTask
 *
 * @method \Vemid\Application\Di getDI()
 *
 * @package Vemid\Task
 * @author Vemid
 */
abstract class AbstractTask extends Di\Injectable implements TaskInterface
{

    const PRIORITY_VERY_LOW = 4096;
    const PRIORITY_LOW = 2048;
    const PRIORITY_NORMAL = 1024;
    const PRIORITY_HIGH = 512;
    const PRIORITY_VERY_HIGH = 256;
    const PRIORITY_URGENT = 128;

    protected $backgroundJobId;
    protected $priority = self::PRIORITY_NORMAL;
    protected $ttr = 1800;

    /**
     * {@inheritdoc}
     */
    public function getBackgroundJobId()
    {
        return $this->backgroundJobId;
    }

    /**
     * {@inheritdoc}
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * {@inheritdoc}
     */
    public function getTtr()
    {
        return $this->ttr;
    }

    /**
     * {@inheritdoc}
     */
    public static function factoryFromJson($json)
    {
        $payload = json_decode($json, true);
        if (isset($payload['task']) && isset($payload['params'])) {
            if (!is_array($payload['params'])) {
                $payload['params'] = [$payload['params']];
            }

            return self::factoryFromParams($payload['task'], $payload['params']);
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public static function factoryFromParams($taskName, array $params)
    {
        $taskName = '\\' . ltrim($taskName, '\\');
        if (!class_exists($taskName)) {
            throw new \RuntimeException(sprintf('Invalid task name %s', $taskName));
        }

        /** @var AbstractTask $task */
        $task = new $taskName;
        $task->setParamsFromArray($params);

        return $task;
    }

    /**
     * {@inheritdoc}
     */
    public function setParamsFromArray(array $params)
    {
        $serializer = new Serializer();
        $params = $serializer->unserializeFromJson($params);
        $serializer->setObjectPropertiesFromParams($this, $params);
    }

    /**
     * {@inheritdoc}
     */
    public function getParamsAsArray()
    {
        $serializer = new Serializer();

        return $serializer->getParamsFromObject($this);
    }

    /**
     * {@inheritdoc}
     */
    public function getParamsAsJson()
    {
        return json_encode($this->getParamsAsArray());
    }

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return [
            'task' => get_called_class(),
            'params' => $this->getParamsAsArray()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function runInBackground($delay = 0)
    {
        /** @var Queue $queue */
        $queue = $this->getDI()->getShared('beanstalkQueue');
        $this->backgroundJobId = $queue->enqueue($this, $this->getPriority(), $delay, $this->getTtr());

        if ($this->backgroundJobId === null) {
            $this->execute();
        }

        return $this->backgroundJobId;
    }

}
