<?php

namespace Vemid\Task\Warrants;

use Vemid\Entity\EntityInterface;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Task\AbstractTask;

/**
 * Class ImportWarrants
 * @package Vemid\Task\Warrants
 */
class ImportWarrantsTask extends AbstractTask
{
    /** @var array */
    public $dataWarrant;

    /**
     * @throws \DomainException
     */
    public function execute()
    {
        if (!$shop = $this->findShop()) {
           throw new \DomainException('Shop not found!');
        }

        if (!$dealerShip = $this->findDealership($shop)) {
            throw new \DomainException('Dealership not found!');
        }

        $this->createOrUpdateShopWarrant($dealerShip);
    }

    /**
     * @return \Code|EntityInterface
     * @throws \DomainException
     */
    private function findShop()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');

        /** @var \CodeType $codeType */
        $codeType = $entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::SHOPS
            ]
        ]);

        if (!$codeType) {
            throw new \DomainException('Code type do not exist!');
        }

       return $entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
            \Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'codeTypeId' => $codeType->getId(),
                'code' => $this->dataWarrant['shop']
            ]
        ]);
    }

    /**
     * @param \Code $code
     * @return \Dealership|bool
     */
    private function findDealership(\Code $code)
    {
        return $this->getDI()->get('entityManager')->findOne(\Dealership::class, [
            \Dealership::PROPERTY_CODE_ID . ' = :codeId:',
            'bind' => [
                'codeId' => $code->getId()
            ]
        ]);
    }

    /**
     * @param \Dealership $dealership
     * @throws \DomainException
     */
    private function createOrUpdateShopWarrant(\Dealership $dealership)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');

        /** @var \DealershipWarrant $warrantShop */
        $warrantShop = $entityManager->findOne(\DealershipWarrant::class, [
            \DealershipWarrant::PROPERTY_WARRANT_NUMBER . ' = :code:',
            'bind' => [
                'code' => $this->dataWarrant['warrant'],
            ]
        ]);

        if ($warrantShop && $warrantShop->getStatus()) {
            return;
        }

        if (!$warrantShop) {
            $warrantShop = new \DealershipWarrant();
            $warrantShop->setStatus(false);
        }

        $warrantShop->setDealership($dealership);
        $warrantShop->setDate($this->dataWarrant['datum']);
        $warrantShop->setAmount($this->dataWarrant['amount']);
        $warrantShop->setWarrantNumber($this->dataWarrant['warrant']);
        $warrantShop->setType($this->dataWarrant['type']);

        if (!$entityManager->save($warrantShop)) {
            throw new \DomainException('Warrant shop was not saved!');
        }
    }
}