<?php

namespace Vemid\Task\Orders;


use mysql_xdevapi\Exception;
use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Task\AbstractTask;

class AddOrderTotalItems extends AbstractTask
{

    /** @var \Product */
    public $product;

    /**
     * @return mixed
     */
    public function execute()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');

        $productsSizes = $entityManager->find(\ProductSize::class,[
            \ProductSize::PROPERTY_PRODUCT_ID . ' = :productId:',
            'bind' => [
                'productId' => $this->product->getId()
            ]
        ]);

        $productsSizess=array_column($productsSizes->toArray(),'id');

        $sectors = [
            'Sivara-pletenina',
            'Sivara-statika',
            'Sivara-statika 2',
            'Pegla',
            'Pakovanje',
            'Dorada',
            'Kontrola'
        ];

        $orderTotals = $entityManager->find(\OrderTotal::class, [
            \OrderTotal::PROPERTY_PRODUCT_SIZE_ID .' IN ({productSizes:array}) ',
            'bind' => [
                'productSizes' => $productsSizess
            ]
        ]);

        $orderItems = $entityManager->find(\OrderItem::class, [
            \OrderItem::PROPERTY_PRODUCT_SIZE_ID .' IN ({productSizes:array}) ',
            'bind' => [
                'productSizes' => $productsSizess
            ]
        ]);

        $orderId = $orderItems->toArray()[0]['orderId'];

        /** @var \Order $order */
        if (!$order = $entityManager->findOne(\Order::class,$orderId)) {
            throw new Exception("ne postoji order");
        }


        if ($orderTotals->count()) {
            $orderTotals->delete();
        }



        /** @var ManagerInterface $modelsManager */
        $modelsManager = $this->getDI()->get('modelsManager');
        $query = $modelsManager->createBuilder()
            ->columns([
                'oi.productSizeId as productSizeId',
                '(SUM(oi.quantityA) + SUM(oi.quantityB)) as total'
            ])
            ->addFrom(\OrderItem::class, 'oi')
            ->where('oi.productSizeId IN ({productSizes:array})', [
                'productSizes' => $productsSizess
            ])
            ->groupBy('oi.productSizeId');

        /** @var array $orderItems */
        $orderItems = $query->getQuery()->execute();

        foreach ($orderItems as $values) {
            $orderTotal = new \OrderTotal();
            $orderTotal->setOrder($order);
            $orderTotal->setProductSizeId($values['productSizeId']);
            $orderTotal->setQuantity($values['total']);
            $orderTotal->setFinalQty(round($values['total']/5) * 5);
            $orderTotal->save();
        }
    }
}
