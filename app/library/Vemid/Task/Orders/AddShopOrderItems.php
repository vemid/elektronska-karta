<?php

namespace Vemid\Task\Orders;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Task\AbstractTask;

/**
 * Class AddShopOrderItems
 * @package Vemid\Task\Orders
 */
class AddShopOrderItems extends AbstractTask
{
    /** @var \Order */
    public $order;
    
    /** @var string */
    public $shopCode;
    
    /** @var string */
    public $productCode;
    
    /** @var string */
    public $sizeCode;
    
    /** @var int */
    public $qtyA;
    
    /**
     * @return mixed
     */
    public function execute()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');
        
        /** @var \CodeType $codeTypeShop */
        $codeTypeShop = $entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::SHOPS
            ]
        ]);

        /** @var \CodeType $codeTypeSize */
        $codeTypeSize = $entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::SIZES
            ]
        ]);

        if (!$codeTypeShop) {
            throw new \LogicException('Code Type Shop not found');
        }

        if (!$codeTypeSize) {
            throw new \LogicException('Code Type Size not found');
        }

        /** @var \Code $codeSize */
        $codeSize = $entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => $this->sizeCode,
                'codeTypeId' => $codeTypeSize->getId()
            ]
        ]);

        /** @var \Code $codeShop */
        $codeShop = $entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => $this->shopCode,
                'codeTypeId' => $codeTypeShop->getId()
            ]
        ]);

        /** @var \Product $product */
        $product = $entityManager->findOne(\Product::class, [
            \Product::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->productCode
            ]
        ]);

        if (!$product) {
            throw new \LogicException(sprintf('Product: (%s) not found', $this->productCode));
        }

        if (!$codeShop) {
            throw new \LogicException(sprintf('MP: (%s) not found', $this->shopCode));
        }

        if (!$codeSize) {
            throw new \LogicException(sprintf('Size: (%s) not found', $this->sizeCode));
        }

        /** @var \ProductSize $productSize */
        $productSize = $entityManager->findOne(\ProductSize::class, [
            \ProductSize::PROPERTY_PRODUCT_ID . ' = :productId: AND ' .
            \ProductSize::PROPERTY_CODE_ID . ' = :codeId:',
            'bind' => [
                'productId' => $product->getId(),
                'codeId' => $codeSize->getId()
            ]
        ]);

        if (!$codeSize) {
            throw new \LogicException(sprintf(
                'Product Size: not found for product (%s) and size (%s)',
                $this->productCode,
                $this->sizeCode
            ));
        }

        /** @var \OrderItem $orderItem */
        $orderItem = $entityManager->findOne(\OrderItem::class, [
            \OrderItem::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId: AND ' .
            \OrderItem::PROPERTY_ENTITY_ID . ' = :entityId: AND ' .
            \OrderItem::PROPERTY_PRODUCT_SIZE_ID . ' = :productSizeId:',
            'bind' => [
                'entityTypeId' => Type::CODE,
                'entityId' => $codeShop->getId(),
                'productSizeId' => $productSize->getId()
            ]
        ]);

        if ($orderItem) {
//            if ($orderItem->getQuantityA() !== $this->qtyA) {
//                throw new \LogicException('Already stored different qty for this product!');
//            }
        } else {
            $orderItem = new \OrderItem();
        }

        $orderItem->setOrder($this->order);
        $orderItem->setProductSize($productSize);
        $orderItem->setQuantityA($this->qtyA);
        $orderItem->setQuantityB(0);
        $orderItem->setEntityTypeId(Type::CODE);
        $orderItem->setEntityId($codeShop->getId());

        if (!$entityManager->save($orderItem)) {
            throw new \DomainException('Failed to save order item');
        }
    }
}
