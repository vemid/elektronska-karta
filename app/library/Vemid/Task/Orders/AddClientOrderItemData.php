<?php

namespace Vemid\Task\Orders;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Task\AbstractTask;

/**
 * Class AddClientOrderItemData
 * @package Vemid\Task\Orders
 */
class AddClientOrderItemData extends AbstractTask
{
    /** @var \ClientOrderItem */
    public $clientOrderItem;

    /**
     * @return mixed
     */
    public function execute()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');

        /** @var \OrderItem $orderItem */
        $orderItem = $entityManager->findOne(\OrderItem::class, [
            \OrderItem::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId: AND ' .
            \OrderItem::PROPERTY_ENTITY_ID . ' = :entityId: AND ' .
            \OrderItem::PROPERTY_PRODUCT_SIZE_ID . ' = :productSizeId:',
            'bind' => [
                'entityTypeId' => Type::CODE,
                'entityId' => $this->clientOrderItem->getEntityId(),
                'productSizeId' => $this->clientOrderItem->getProductSize()->getId()
            ]
        ]);

        if ($orderItem) {
//            if ($orderItem->getQuantityA() !== $this->qtyA) {
//                throw new \LogicException('Already stored different qty for this product!');
//            }
        } else {
            $orderItem = new \OrderItem();
        }

        $orderItem->setOrder($this->clientOrderItem->getOrder());
        $orderItem->setClientOrderItemId($this->clientOrderItem->getId());
        $orderItem->setProductSize($this->clientOrderItem->getProductSize());
        $orderItem->setQuantityA($this->clientOrderItem->getQuantityA());
        $orderItem->setQuantityB($this->clientOrderItem->getQuantityB());
        $orderItem->setEntityTypeId($this->clientOrderItem->getEntityTypeId());
        $orderItem->setEntityId($this->clientOrderItem->getEntityId());

        if (!$entityManager->save($orderItem)) {
            throw new \DomainException('Failed to save order item');
        }

    }

}