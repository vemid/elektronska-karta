<?php

namespace Vemid\Task\Orders;

use Vemid\Date\DateTime;
use Vemid\Error\Exception;
use Vemid\Task\AbstractTask;

class FinishOrderTask extends AbstractTask
{
    /**
     * @return mixed
     *
     * @throws \DomainException
     */
    public function execute()
    {
        $entityManager = $this->getDI()->getEntityManager();

        /** @var \Order $orders */
        $orders = $entityManager->find(\Order::class,[
            \Order::PROPERTY_END_DATE . ' = :endDate:' ,
            'bind' => [
                'endDate' => (new DateTime())->format("Y-m-d")
            ]
        ]);

        /** @var \Order $order */
        foreach ($orders as $order) {

            $orderTotalTask = new AddOrderTotals();
            $orderTotalTask->order = $order;
            $orderTotalTask->runInBackground();

            foreach($order->getOrderClassifications() as $classification) {
                $productOrderable = new \Vemid\Task\Classifications\UnSetProductsIsOrderable();
                $productOrderable->classification = $classification->getClassification();
                $productOrderable->runInBackground();
            }

            $order->setActive(false);
            if (!$entityManager->save($order)) {

                return;
            }
        }

    }

}