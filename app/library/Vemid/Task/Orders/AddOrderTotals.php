<?php

namespace Vemid\Task\Orders;


use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Task\AbstractTask;

class AddOrderTotals extends AbstractTask
{
    /** @var \Order */
    public $order;

    /**
     * @return mixed
     */
    public function execute()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');

        $orderTotals = $entityManager->find(\OrderTotal::class, [
            \OrderTotal::PROPERTY_ORDER_ID . ' = :orderId:',
            'bind' => [
                'orderId' => $this->order->getId()
            ]
        ]);

        if ($orderTotals->count()) {
            $orderTotals->delete();
        }

        /** @var ManagerInterface $modelsManager */
        $modelsManager = $this->getDI()->get('modelsManager');
        $query = $modelsManager->createBuilder()
            ->columns([
                'oi.productSizeId as productSizeId',
                '(SUM(oi.quantityA) + SUM(oi.quantityB)) as total'
            ])
            ->addFrom(\ClientOrderItem::class, 'oi')
            ->where('oi.orderId = :orderId:', [
                'orderId' => $this->order->getId()
            ])
            ->groupBy('oi.productSizeId');

        /** @var array $orderItems */
        $orderItems = $query->getQuery()->execute();

        foreach ($orderItems as $values) {
            $orderTotal = new \OrderTotal();
            $orderTotal->setOrder($this->order);
            $orderTotal->setProductSizeId($values['productSizeId']);
            $orderTotal->setQuantity($values['total']);
            $orderTotal->setFinalQty(round($values['total']/5) * 5);
            $orderTotal->save();
        }
    }
}
