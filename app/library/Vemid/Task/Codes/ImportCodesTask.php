<?php

namespace Vemid\Task\Codes;

use Vemid\Task\AbstractTask;

/**
 * Class ImportAttributes
 * @package Vemid\Task\Codes
 */
class ImportCodesTask extends AbstractTask
{
    /** @var array */
    public $codeData;
    
    /**
     * @return mixed
     *
     * @throws \DomainException
     */
    public function execute()
    {
        $this->importCodes(
            $this->findTypeCode(),
            trim($this->codeData['ozn_par_par']),
            trim($this->codeData['naz_par_par'])
        );
    }

    /**
     * @return \CodeType
     *
     * @throws \DomainException
     */
    private function findTypeCode()
    {
        /** @var \CodeType $codeType */
        $codeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::ATTRIBUTES
            ]
        ]);

        if (!$codeType) {
            throw new \DomainException('CodeType do not exist');
        }


        return $codeType;
    }

        /**
         * @param \CodeType $codeType
         * @param $importCode
         * @param $name
         * @return \Code
         *
         * @throws \DomainException
         */
    private function importCodes(\CodeType $codeType,$importCode,$name)
    {
        /** @var \Code $code */
        $code = $this->getDI()->getEntityManager()->findOne(\Code::class , [
            \Code::PROPERTY_CODE_TYPE_ID .' = :codeTypeCodeId: AND ' .
            \Code::PROPERTY_CODE .' = :code:',
            'bind'=>[
                'codeTypeCodeId' => $codeType->getEntityId(),
                'code' => $importCode
            ]
        ]);

        if($code){
            if($code->getName() !== $name){
                $code->setName($name);

                if(!$this->getDI()->getEntityManager()->save($code)){
                    throw new \DomainException('Code name was not updated');
                }
            }

            return $code;
        }

        $code = new \Code();
        $code->setCodeType($codeType);
        $code->setCode($importCode);
        $code->setName($name);

        if(!$this->getDI()->getEntityManager()->save($code)){
            throw new \DomainException('Code was not saved');
        }

        return $code;
    }
}
