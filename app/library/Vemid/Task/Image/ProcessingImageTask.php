<?php

namespace Vemid\Task\Image;

use Vemid\Helper\ImageHelper;
use Vemid\Task\AbstractTask;

/**
 * Class ProcessingImageTask
 * @package Vemid\Task\Image
 */
class ProcessingImageTask extends AbstractTask
{
    /** @var string */
    public $imagePath;

    /** @var integer */
    public $width;

    /** @var integer */
    public $height;

    /**
     * @return mixed
     */
    public function execute()
    {
        $helper = new ImageHelper($this->imagePath);

        if (!$this->width) {
            $this->width = $helper->getWidth();
        }

        if (!$this->height) {
            $this->height = $helper->getHeight();
        }

        if ($helper->getWidth() > $this->width) {
            $helper->resizeByWidth($this->width);
            $helper->sharpenImage(100);
        }

        if ($helper->getHeight() > $this->height) {
            $helper->resizeByHeight($this->height);
            $helper->sharpenImage(100);
        }
    }
}
