<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 7/31/19
 * Time: 13:25
 */

namespace Vemid\Task\Failure;
use Vemid\Task\AbstractTask;


/**
 * Class ImportFailureTypeTask
 * @package Vemid\Task\Attributes
 */
class ImportFailureTypesTask extends AbstractTask
{
    /** @var array */
    public $codeData;

    /**
     * @return mixed
     *
     * @throws \DomainException
     */
    public function execute()
    {
        if (empty($this->codeData['sif_raz_pov'])) {
            return;
        }

        $failureCodeId = $this->findFailureTypeCodeId();
        $this->importFailureTypeValues($failureCodeId,trim($this->codeData['sif_raz_pov']), trim($this->codeData['naz_raz_pov']));
    }

    /**
     * @return \CodeType
     *
     * @throws \DomainException
     */
    private function findFailureTypeCodeId()
    {

        /** @var \CodeType $codeType */
        $codeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::FAILURE
            ]
        ]);

        if(!$codeType) {
            throw new \DomainException('CodeType do not exist');
        }

        return $codeType;
    }

    /**
     * @param \CodeType $failureTypeCode
     * @param $codeFailure
     * @param $name
     * @return \Attribute
     *
     * @throws \DomainException
     */

    private function importFailureTypeValues(\CodeType $failureTypeCode,$codeFailure, $name)
    {
        /** @var \Code $codes */
        $codes = $this->getDI()->getEntityManager()->findOne(\Code::class , [
            \Code::PROPERTY_CODE_TYPE_ID .' = :codeType: AND ' .
            \Code::PROPERTY_CODE . ' = :code:',
            'bind'=>[
                'codeType' => $failureTypeCode->getId(),
                'code' => $codeFailure
            ]
        ]);

        if($codes){
            return $codes;
        }

        $code = new \Code();
        $code->setCodeTypeId($failureTypeCode->getEntityId());
        $code->setName($name);
        $code->setCode($codeFailure);

        if(!$this->getDI()->getEntityManager()->save($code)){
            throw new \DomainException('Attribute was not saved');
        }

        return $code;
    }


}