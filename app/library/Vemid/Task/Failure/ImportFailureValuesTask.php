<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 7/31/19
 * Time: 13:25
 */

namespace Vemid\Task\Failure;
use Vemid\Date\DateTime;
use Vemid\Task\AbstractTask;


/**
 * Class ImportFailureValuesTask
 * @package Vemid\Task\Attributes
 */
class ImportFailureValuesTask extends AbstractTask
{
    /** @var array */
    public $codeData;

    /**
     * @return mixed
     *
     * @throws \DomainException
     */
    public function execute()
    {
        if (empty($this->codeData['oznaka'])) {
            return;
        }

        $failureCodeId = $this->findFailureCodeId(trim($this->codeData['sif_raz_pov']));
        $failureProductSizeId = $this->findProductSizeId(trim($this->codeData['roba']),trim($this->codeData['velicina']));
        $failureShopId = $this->findShopId(trim($this->codeData['sifra_objekta']));
        $this->importFailureValues($failureCodeId,$failureProductSizeId,$failureShopId,$this->codeData);
    }

    /**
     * @param $codeString
     * @return \Code
     *
     * @throws \DomainException
     */
    private function findFailureCodeId($codeString)
    {

        /** @var \CodeType $codeType */
        $codeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => \CodeType::FAILURE,
            ]
        ]);

        if(!$codeType) {
            throw new \DomainException('CodeType do not exist');
        }

        /** @var \Code $failureCode */
        $failureCode = $this->getDI()->getEntityManager()->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => $codeString,
                'codeTypeId' => $codeType->getEntityId()
            ]
        ]);

        if (!$failureCode) {
            throw new \DomainException(sprintf('Failure (%s) code do not exist', $codeString));
        }


        return $failureCode;
    }

    /**
     * @param $shopString
     * @return \Code
     *
     * @throws \DomainException
     */
    private function findShopId($shopString)
    {

        /** @var \CodeType $codeType */
        $codeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => \CodeType::SHOPS,
            ]
        ]);

        if(!$codeType) {
            throw new \DomainException('CodeType do not exist');
        }

        /** @var \Code $shopCode */
        $shopCode = $this->getDI()->getEntityManager()->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => $shopString,
                'codeTypeId' => $codeType->getEntityId()
            ]
        ]);

        if (!$shopCode) {
            throw new \DomainException(sprintf('Shop (%s) code do not exist', $shopString));
        }


        return $shopCode;
    }

    /**
     * @param $productString
     * @param $sizeString
     * @return \ProductSize
     *
     * @throws \DomainException
     */
    public function findProductSizeId($productString,$sizeString)
    {
        /** @var \Product $product */
        $product = $this->getDI()->getEntityManager()->findOne(\Product::class,[
            \Product::PROPERTY_CODE . ' = :productCode: ',
            'bind' => [
                'productCode' => $productString
            ]
        ]);

        if(!$product) {
            throw new \DomainException(sprintf('Product (%s) do not exist',$productString));
        }

        /** @var \CodeType $sizeTypeCode */
        $codeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => \CodeType::SIZES,
            ]
        ]);

        if(!$codeType) {
            throw new \DomainException('CodeType do not exist');
        }

        /** @var \Code $sizeCode */
        $sizeCode = $this->getDI()->getEntityManager()->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => $sizeString,
                'codeTypeId' => $codeType->getEntityId()
            ]
        ]);

        if (!$sizeCode) {
            throw new \DomainException(sprintf('Failure (%s) code do not exist', $sizeString));
        }


        /** @var \ProductSize $productSize */
        $productSize = $this->getDI()->getEntityManager()->findOne(\ProductSize::class,[
            \ProductSize::PROPERTY_PRODUCT_ID . ' = :productId: AND ' . \ProductSize::PROPERTY_CODE_ID . ' = :codeId: ',
            'bind' => [
                'productId' => $product->getId(),
                'codeId' => $sizeCode->getId(),
            ]
        ]);

        if (!$productSize) {
            throw new \DomainException(sprintf('Product with (%s) and size code (%s) do not exist', $productString,$sizeString));
        }


        return $productSize;

    }

    /**
     * @param \Code $failureCodeId
     * @param \ProductSize $failureProductSizeId
     * @param \Code $failureShopId
     * @param $data[]
     * @return \Failure
     *
     * @throws \DomainException
     */
    public function importFailureValues(\Code $failureCodeId,\ProductSize $failureProductSizeId,\Code $failureShopId,$data)
    {
        /** @var \Failure $failure */
        $failure = $this->getDI()->getEntityManager()->findOne(\Failure::class, [
            \Failure::PROPERTY_SHOP_CODE_ID . ' = :failureShopId: AND ' .
            \Failure::PROPERTY_PRODUCT_SIZE_ID . ' = :failureProductSizeId: AND ' .
            \Failure::PROPERTY_DOCUMENT . ' = :document:',
            'bind' => [
                'failureShopId' => $failureShopId->getId(),
                'failureProductSizeId' => $failureProductSizeId->getId(),
                'document' => trim($data['oznaka'])
            ]
        ]);

        $date = new DateTime($data['datum']);

        if(!$failure){
            $failure = new \Failure();
            $failure->setProductSize($failureProductSizeId);
            $failure->setFailureCode($failureCodeId);
            $failure->setShopCode($failureShopId);
            $failure->setDate($date->getUnixFormat());
            $failure->setDocument(trim($data['oznaka']));
            $failure->setWarehouse($data['magacin'] ? trim($data['magacin']) : "VP");
            $failure->setFailureOrigin(\Failure::ORIGIN_RETAIL);
            $failure->setQty($data['kolicina']);
            if(count($data['napomena'])) {
                $failure->setNote(trim($data['napomena']));
            }

            if (!$this->getDI()->getEntityManager()->save($failure)) {
                throw new \DomainException('FAilure value was not saved');
            }

            return $failure;
        }
    }


}