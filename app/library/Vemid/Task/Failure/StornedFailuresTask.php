<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 7/31/19
 * Time: 13:25
 */

namespace Vemid\Task\Failure;
use Vemid\Task\AbstractTask;


/**
 * Class StornedFailuresTask
 * @package Vemid\Task\Attributes
 */
class StornedFailuresTask extends AbstractTask
{
    /** @var array */
    public $codeData;

    /**
     * @return mixed
     *
     * @throws \DomainException
     */
    public function execute()
    {
        if (empty($this->codeData['oznaka'])) {
            return;
        }

        $failures = $this->findFailure(trim($this->codeData['oznaka']));
        $deleted = $this->deleteFailures($failures);
    }


    /**
     * @return \Failure[]
     *
     * @param $document
     *
     * @throws \DomainException
     */
    public function findFailure($document)
    {
        /** @var \Failure[] $failures */
        $failures = $this->getDI()->getEntityManager()->find(\Failure::class,[
            \Failure::PROPERTY_DOCUMENT . ' = :document: ',
            'bind' => [
                'document' => $document
            ]
        ]);

        if (!$failures) {
            throw new \DomainException('Failure with document doesn found!');
        }

        return $failures;
    }


    /**
     * @param $failures[]
     *
     * @throws \DomainException
     */
    public function deleteFailures($failures)
    {
        /** @var \Failure $failure */
        foreach ($failures as $failure) {
            if(!$failure->delete()) {
                throw new \DomainException('Faliure cant be deleted');
            }
        }
    }

}