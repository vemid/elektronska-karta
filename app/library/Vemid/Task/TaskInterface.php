<?php

namespace Vemid\Task;

/**
 * Interface TaskInterface
 *
 * @package Vemid\Task
 * @author Vemid
 */
interface TaskInterface
{

    /**
     * @return mixed
     */
    public function execute();

    /**
     * @return mixed
     */
    public function run();

    /**
     * @param int $delay
     * @return string|null
     */
    public function runInBackground($delay = 0);

    /**
     * Returns a payload array which could be serialized to json
     *
     * @return array
     */
    public function toArray();

    /**
     * @return string
     */
    public function getBackgroundJobId();

    /**
     * @return int
     */
    public function getPriority();

    /**
     * @return int
     */
    public function getTtr();

    /**
     * Creates the task from a JSON string
     *
     * @param string $json
     * @return TaskInterface|null
     * @throws \RuntimeException
     */
    public static function factoryFromJson($json);

    /**
     * Creates the task from explicit params
     *
     * @param string $taskName
     * @param array $params
     * @return TaskInterface
     * @throws \RuntimeException
     */
    public static function factoryFromParams($taskName, array $params);

    /**
     * Set parameters based on the JSON decoded array. Instantiates models etc.
     *
     * @param array $params
     */
    public function setParamsFromArray(array $params);

    /**
     * Encodes all parameters as simple primitive objects ready to be encoded to JSON
     *
     * @return array
     */
    public function getParamsAsArray();

    /**
     * @return string
     */
    public function getParamsAsJson();

}
