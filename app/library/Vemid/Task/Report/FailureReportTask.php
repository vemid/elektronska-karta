<?php


namespace Vemid\Task\Report;
use Vemid\Date\DateTime;
use Vemid\Task\AbstractTask;
use Vemid\Service\Report\PaymentReport;

class FailureReportTask  extends AbstractTask
{

    /**
     * @return mixed
     */
    public function execute()
    {
        $date = ((new DateTime())->modify('-1 days'));

        $modelsManager = $this->getDI()->getModelsManager();
        $query = $modelsManager->createBuilder()
            ->columns(['p.code as productCode','p.name as productName','c.name as failureName','sum(f.qty) as qty'])
            ->addFrom(\Failure::class, 'f')
            ->leftJoin(\ProductSize::class, 'f.productSizeId = ps.id', 'ps')
            ->leftJoin(\Product::class, 'ps.productId = p.id', 'p')
            ->leftJoin(\Code::class, 'f.failureCodeId = c.id', 'c')
            ->where('date(f.created) = :date:',[
                'date' => $date->format('Y-m-d')
            ])
            ->andWhere('f.failureOrigin = :failureOrigin: ', [
                'failureOrigin' => \Failure::ORIGIN_PRODUCTION
            ])
            ->groupBy('p.code,p.name,c.name');

        /** @var array $failureData */
        $failureData = $query->getQuery()->execute();

        if($failureData->count()) {

            $mailManager = $this->getDI()->getMailManager();
            $message = $mailManager->createMessageFromView('reports/email/failure-report-mail', [
                'datas' => $failureData,
            ]);

            $emails = [
                'feleri@bebakids.com' => 'Feleri'
            ];

            $message->setSubject('Izvestaj felera proizvodnje - ' . $date->getShortSerbianFormat());
            $message->setTo($emails);
            $message->send();
        }

    }

}