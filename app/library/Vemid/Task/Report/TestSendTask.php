<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 1/28/19
 * Time: 16:03
 */

namespace Vemid\Task\Report;
use Vemid\Task\AbstractTask;

class TestSendTask extends AbstractTask
{
    public function execute()
    {

        $mailManager = $this->getDI()->getMailManager();
        $message = $mailManager->createMessage();


        $message->setSubject('Izvestaj ucinka radnika za sektor ');
        $message->setBody("ovo je testiranje");
        $message->setTo('admin@bebakids.com');
        $message->setCc('admin@bebakids.com');
        $message->send();


    }
}