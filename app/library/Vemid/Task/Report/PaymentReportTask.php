<?php

namespace Vemid\Task\Report;

use Vemid\Date\DateTime;
use Vemid\Task\AbstractTask;
use \Vemid\Date\DateRange;
use Vemid\Service\OperationList\ItemCalculator;

/**
 * Class PaymentReportTask
 * @package Vemid\Task\Report
 */
class PaymentReportTask extends AbstractTask
{
    /** @var \Code */
    public $sector;

    /** @var */
    public $startDate;

    /** @var DateTime */
    public $endDate;

    /**
     * @return mixed
     */
    public function execute()
    {
        $entityManager = $this->getDI()->getEntityManager();

        $date = new DateTime();
        $dateRanges = new DateRange($this->startDate,$this->endDate->modify('+1 day'));

        /** @var \Vemid\Entity\Repository\ProductionWorkerRepository $productionWorker */
        $productionWorker = $entityManager->getRepository(\ProductionWorker::class);


        /** @var \Vemid\Entity\Repository\AbsenceWorkRepository $absenceWork */
        $absenceWork = $entityManager->getRepository(\AbsenceWork::class);

        /** @var \Vemid\Entity\Repository\CuttingJobWorksheetRepository $cuttingJob */
        $cuttingJob = $entityManager->getRepository(\CuttingJobWorksheet::class);

        /** @var \Vemid\Entity\Repository\OperatingListItemWorksheetRepository $oliw */
        $oliw = $entityManager->getRepository(\OperatingListItemWorksheet::class);

        $itemCalculator = new ItemCalculator($entityManager);

        $productionWorkers = $entityManager->find(\ProductionWorker::class,[
            \ProductionWorker::PROPERTY_CODE_ID . ' = :codeId:',
            'bind' => [
                'codeId' => $this->sector->getId()
            ]
        ]);

        $dateNow = (new DateTime())->modify('+1 day');
        $dateNowClone = (new DateTime())->modify('+1 day');
        $dateBefore = $dateNowClone->modify('-8 days');

        $testDateRange = (new DateRange($dateBefore,$dateNow))->getAllDates();

        $sectorAllOperations = $productionWorker->getSectorDataForPeriod((int)$this->sector->getId(),$this->startDate,$this->endDate);

        $data = [
            'absenceWork' => $absenceWork,
            'cuttingJob' => $cuttingJob,
            'oliw' => $oliw,
            'productionWorkers' => $productionWorkers,
            'itemCalculator' => $itemCalculator,
            'dateTime' => $date,
            'dateRanges' => $dateRanges->getAllDates(),
            'sectorAllOperations' => $sectorAllOperations,
            'productionWorker' => $productionWorker,
        ];

        $html = $this->getDI()->getSimpleView()->render('reports/print', $data);

        $file = $this->getDI()->getPdfRenderer()->render($html);

        $mailManager = $this->getDI()->getMailManager();
        $message = $mailManager->createMessageFromView('reports/mail-template', [
            'fileName' => md5($html)
        ]);

        $message->setSubject("Izvestaj ucinka radnika za sektor ".$this->sector->getDisplayName()." za mesec ".$this->endDate->format("F"));
        $message->setTo('admin@bebakids.com');
        $message->setBcc('darkovesic3@gmail.com');
        $message->setAttachmentData($file, 'Izvestaj ucinka za sektor '.$this->sector->getDisplayName().'.pdf');

        $message->send();
    }
}