<?php

namespace Vemid\Task\Report;

use Vemid\Date\DateTime;
use Vemid\Task\AbstractTask;
use Vemid\Service\Report\PaymentReport;

class DealershipPaymentReportTask extends AbstractTask
{
    /** @var \Dealership */
    public $dealership;


    /**
     * @return mixed
     */
    public function execute()
    {
        $modelsManager = $this->getDI()->getModelsManager();

        $query = $modelsManager->createBuilder()
            ->columns(['date','type','warrantNumber','amount'])
            ->addFrom(\DealershipWarrant::class, 'dw')
            ->where('dw.status = 0')
            ->andWhere('dw.date <= :date:', [
                'date' => (new DateTime())->modify('-4 day')
            ])
            ->andWhere('dw.dealershipId = :dealershipId: ', [
                'dealershipId' => $this->dealership->getId()
            ]);

        /** @var array $dealershipWarrants */
        $dealershipData = $query->getQuery()->execute();

        $mailManager = $this->getDI()->getMailManager();
        $message = $mailManager->createMessageFromView('reports/dealership-payment-mail', [
            'datas' => $dealershipData, 'dealer' => $this->dealership->getCompanyName()
        ]);

        $emails = [
            'fransize.dugovanja@bebakids.com'=>'Dugovanja Fransiza',
        ];

        $message->setSubject('Izvestaj neuplacenih dugovanja - '.$this->dealership->getCode()->getName() );
        $message->setTo($this->dealership->getEmail());
        $message->setCc($emails);
        $message->send();
    }
}
