<?php

namespace Vemid\Task\Report;

use Vemid\Date\DateTime;
use Vemid\Task\AbstractTask;

class ProductionOrderMismatchTask extends AbstractTask
{

    /**
     * @return mixed
     */
    public function execute()
    {
        $date = ((new DateTime())->modify('-1 days'));

        $modelsManager = $this->getDI()->getModelsManager();
        $query = $modelsManager->createBuilder()
            ->columns(['p.name as proizvod',
                'p.code as sifra',
                'c.name as velicina',
                'oi.quantityA as qty',
                'po.planedQty as planirana_kol',
                'po.realizedAQty+po.realizedBQty as realizovano '])
            ->addFrom(\OrderItem::class, 'oi')
            ->leftJoin(\ProductSize::class, 'oi.productSizeId = ps.id', 'ps')
            ->leftJoin(\Product::class, 'ps.productId = p.id', 'p')
            ->leftJoin(\Code::class, 'ps.codeId = c.id', 'c')
            ->leftJoin(\ProductionOrder::class, 'po.productSizeId = oi.productSizeId', 'po')
            ->where('oi.orderId = :order:',[
                'order' => '27'
            ])
            ->andWhere('oi.entityTypeId = :entityTypeId: ', [
                'entityTypeId' => \Vemid\Entity\Type::CODE
            ])
            ->andWhere('oi.entityId = :entityId: ', [
                'entityId' => '127'
            ])
            ->andWhere('po.realizedAQty > :realizedAQty: ', [
                'realizedAQty' => 0
            ])
            ->andWhere('oi.quantityA < :quantityA: ', [
                'quantityA' => 0
            ]);

        /** @var array $reportData */
        $reportData = $query->getQuery()->execute();

        if($reportData->count()) {

            $mailManager = $this->getDI()->getMailManager();
            $message = $mailManager->createMessageFromView('reports/email/production-order-mismatch-report-mail', [
                'datas' => $reportData,
            ]);

            $emails = [
                'krojacnica@bebakids.com' => 'Krojacnica'
            ];

            $message->setSubject('Izvestaj manjaka u raspodeli rastera MP - ' . $date->getShortSerbianFormat());
            $message->setTo($emails);
            $message->send();
        }

    }

}