<?php

namespace Vemid\Task\Report;

use Vemid\Date\DateTime;
use Vemid\Task\AbstractTask;

class PackedQuantityTask extends AbstractTask
{

    /**
     * @return mixed
     */
    public function execute()
    {
        $date = ((new DateTime())->modify('-30 days'));

        $modelsManager = $this->getDI()->getModelsManager();
        $query = $modelsManager->createBuilder()
            ->columns(['date(woi.created) as datum',
                'sum(woi.quantity) as kolicina'])
            ->addFrom(\WarehouseOrderItem::class, 'woi')
            ->where('date(woi.created) >= :date:',[
                'date' => $date->getUnixFormat()
            ])
            ->groupBy('date(woi.created)')
            ->orderBy('date(woi.created)');

        /** @var array $reportData */
        $reportData = $query->getQuery()->execute();

        if($reportData->count()) {

            $mailManager = $this->getDI()->getMailManager();
            $message = $mailManager->createMessageFromView('reports/email/packed-quantity', [
                'datas' => $reportData,
            ]);

            $emails = [
                'admin@bebakids.com' => 'Marko Vesic',
                'ninoslav.perisic@bebakids.com' => 'Ninoslav Perisic',
                'ivica.vidojevic@bebakids.com' => 'Ivica Vidojevic',
            ];

            $message->setSubject('Izvestaj spakovane kolicine ');
            $message->setTo($emails);
            $message->send();
        }

    }

}