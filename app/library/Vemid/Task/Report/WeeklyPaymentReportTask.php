<?php

namespace Vemid\Task\Report;

use Vemid\Date\DateTime;
use Vemid\Task\AbstractTask;
use Vemid\Service\Report\PaymentReport;

class WeeklyPaymentReportTask extends AbstractTask
{
    /** @var \Code */
    public $sector;

    /** @var */
    public $startDate;

    /** @var DateTime */
    public $endDate;


    /**
     * @return mixed
     */
    public function execute()
    {
        $entityManager = $this->getDI()->getEntityManager();

        if (!$this->endDate) {
            $this->endDate = (new DateTime())->modify('-1 day');
//            $this->endDate = (new DateTime())->modify('-7 day');
        }

        if (!$this->startDate) {
            $this->startDate = clone $this->endDate;
            $this->startDate->modify('-7 days');
        }

        $productionWorkers = $entityManager->find(\ProductionWorker::class, [
            \ProductionWorker::PROPERTY_CODE_ID . ' = :codeId:',
            'bind' => [
                'codeId' => $this->sector->getId()
            ]
        ]);

        $payment = new PaymentReport($entityManager);
        $paymentDatas = $payment->getPaymentReportBySectorData($this->startDate, $this->endDate, $this->sector);
        $totalData = $payment->getPaymentReportBySectorDataTotal($this->startDate, $this->endDate, $this->sector);

        $data = [
            'datas' => $paymentDatas,
            'totalDatas' => $totalData,
            'productionWorkers' => $productionWorkers,
            'dateFrom' => $this->startDate,
            'dateTo' => $this->endDate,
            'sectors' => $this->sector,
            'payment' => $payment
        ];

        $name = sprintf('%s | period: %s-%s', $this->sector->getName(), $this->startDate->getShort(), $this->endDate->getShort());
        $html = $this->getDI()->getSimpleView()->render('reports/print-weekly-payment-report', $data);
        $file = $this->getDI()->getPdfRenderer()->render($html, 'landscape');
        $mailManager = $this->getDI()->getMailManager();
        $message = $mailManager->createMessageFromView('reports/mail-template', [
            'fileName' => md5($html), 'sector' => $this->sector, 'dateFrom' => $this->startDate,
            'dateTo' => $this->endDate,
        ]);

        $emails = [
            'ivica.vidojevic@bebakids.com' => 'Ivica Vidojevic',
            'marko.vidojevic@bebakids.com' => 'Marko Vidojevic',
            'ninoslav.perisic@bebakids.com' => 'Ninoslav Perisic',
        ];

        $message->setSubject('Izvestaj ucinka radnika za sektor ' . $name);
        $message->setTo($emails);
        $message->setCc('admin@bebakids.com');
        $message->setAttachmentData($file, 'Izvestaj ucinka za sektor' . $name . '.pdf');
        $message->send();
    }
}
