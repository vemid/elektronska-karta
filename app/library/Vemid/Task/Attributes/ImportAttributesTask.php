<?php

namespace Vemid\Task\Attributes;

use Vemid\Task\AbstractTask;

/**
 * Class ImportAttributeValues
 * @package Vemid\Task\Attributes
 */
class ImportAttributesTask extends AbstractTask
{
    /** @var array */
    public $codeData;
    
    /**
     * @return mixed
     *
     * @throws \DomainException
     */
    public function execute()
    {
        if (empty($this->codeData['vre_par'])) {
           return;
        }

        $attributeCodeId = $this->findAttributeCodeId(trim($this->codeData['ozn_par_par']));
        $this->importAttributeValues($attributeCodeId, trim($this->codeData['vre_par']));
    }

    /**
     * @param $codeString
     * @return \Code
     *
     * @throws \DomainException
     */
    private function findAttributeCodeId($codeString)
    {

        /** @var \CodeType $codeType */
        $codeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::ATTRIBUTES
            ]
        ]);

        if(!$codeType) {
            throw new \DomainException('CodeType do not exist');
        }

        /** @var \Code $attributeCode */
        $attributeCode = $this->getDI()->getEntityManager()->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => $codeString,
                'codeTypeId' => $codeType->getEntityId()
            ]
        ]);

        if (!$attributeCode) {
            throw new \DomainException(sprintf('Attribute (%s) code do not exist', $codeString));
        }


        return $attributeCode;
    }

    /**
     * @param \Code $attributeCode
     * @param $name
     * @return \Attribute
     *
     * @throws \DomainException
     */

    private function importAttributeValues(\Code $attributeCode, $name)
    {
        /** @var \Attribute $attribute */
        $attribute = $this->getDI()->getEntityManager()->findOne(\Attribute::class , [
            \Attribute::PROPERTY_ATTRIBUTE_CODE_ID .' = :code: AND ' .
            \Attribute::PROPERTY_NAME . ' = :name:',
            'bind'=>[
                'code' => $attributeCode->getId(),
                'name' => $name
            ]
        ]);

        if($attribute){
            return $attribute;
        }

        $attribute = new \Attribute();
        $attribute->setAttributeCodeId($attributeCode->getEntityId());
        $attribute->setName($name);

        if(!$this->getDI()->getEntityManager()->save($attribute)){
            throw new \DomainException('Attribute was not saved');
        }

        return $attribute;
    }
}
