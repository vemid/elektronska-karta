<?php

namespace Vemid\Task\Products;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Service\MisWsdl\PutProductClassificationAttributeSync;
use Vemid\Service\MisWsdl\PutProductClassificationSync;
use Vemid\Service\MisWsdl\PutProductDeclarationSync;
use Vemid\Service\MisWsdl\PutProductGender;
use Vemid\Service\MisWsdl\PutProductSingleProcedure;
use Vemid\Service\MisWsdl\PutProductSizeSync;
use Vemid\Service\MisWsdl\PutProductSync;
use Vemid\Service\MisWsdl\PutProductWeb;
use Vemid\Task\AbstractTask;

/**
 * Class ImportClassificationsAndSizes
 * @package Vemid\Task\Products
 */
class ImportClassificationsAndSizes extends AbstractTask
{
    /**
     * @return mixed
     */
    public function execute()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');

        /** @var \Product[] $products */
        $products = $entityManager->find(\Product::class, [
            \Product::PROPERTY_STATUS . ' = :status:',
            'bind' => [
                'status' => \Product::STATUS_FINISHED
            ]
        ]);

        foreach ($products as $product) {
            if (!$product->getIsSynced()) {
                $importer = new PutProductSync($product, $entityManager, $product->getUser()->getDisplayName());
                $importer->run();

                $importer2 = new PutProductDeclarationSync($product,$entityManager,$product->getUser()->getDisplayName());
                $importer2->run();

            }

            $productClassifications = $product->getProductClassifications([
                \ProductClassification::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId: AND ' .
                \ProductClassification::PROPERTY_IS_SYNCED . ' = :isSynced:',
                'bind' => [
                    'entityTypeId' => Type::CLASSIFICATION,
                    'isSynced' => false,
                ]
            ]);

            foreach ($productClassifications as $productClassification) {
                if (!$classification = $productClassification->getProductClassification()) {
                    continue;
                }

                if (!$code = $classification->getClassificationType()) {
                    continue;
                }

                if ($code->getCode() === '00') {
                    $productClassification->setIsSynced(true);

                    if (!$entityManager->save($productClassification)) {
                        throw new \LogicException(sprintf('Unable to set synced to product classification with ID: %s', $this->productClassification->getId()));
                    }

                    continue;
                }

                $taskClassification = new PutProductClassificationSync($entityManager, $productClassification);
                $taskClassification->run();

                //$taskProductSingleProcedure = new PutProductSingleProcedure($product, $entityManager, $product->getUser()->getDisplayName());
                //$taskProductSingleProcedure->run();
            }

            $productAttributes = $product->getProductClassifications([
                \ProductClassification::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId: AND ' .
                \ProductClassification::PROPERTY_IS_SYNCED . ' = :isSynced:',
                'bind' => [
                    'entityTypeId' => Type::ATTRIBUTE,
                    'isSynced' => false,
                ]
            ]);

            foreach ($productAttributes as $productAttribute) {
                /** @var \Attribute $attribute */
                if (!$attribute = $productAttribute->getProductClassification()) {
                    continue;
                }

                if (!$code = $attribute->getAttributeCode()) {
                    continue;
                }

                $taskClassification = new PutProductClassificationAttributeSync($entityManager, $productAttribute, $product->getUser()->getDisplayName());
                $taskClassification->run();

            }


            $productSizes = $product->getProductSizes([
                \ProductSize::PROPERTY_IS_SYNCED . ' = :isSynced: AND '.
                \ProductSize::PROPERTY_BARCODE . ' IS NOT NULL',
                'bind' => [
                    'isSynced' => false
                ]
            ]);

            foreach ($productSizes as $productSize) {
                if (!$code = $productSize->getCode()) {
                    continue;
                }

                $taskSize = new PutProductSizeSync($entityManager, $productSize);
                $taskSize->run();

            }


            //$taskProductSingleProcedure = new PutProductSingleProcedure($product, $entityManager, $product->getUser()->getDisplayName());
            //$taskProductSingleProcedure->run();

            //$webImporter = new PutProductWeb($product,$entityManager,$product->getUser()->getDisplayName());
            //$webImporter->import();

            //$genderImporter = new PutProductGender($product,$entityManager,$product->getUser()->getDisplayName());
            //$genderImporter->import();
        }
    }
}
