<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 10/24/18
 * Time: 13:10
 */

namespace Vemid\Task\Products;


use Phalcon\Mvc\Model\Transaction;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Service\MisWsdl\PutProducePrice;
use Vemid\Task\AbstractTask;

/**
 * Class ImportMerchandisePrices
 * @package Vemid\Task\Products
 */
class ImportMerchandisePrices extends AbstractTask
{
    /**
     * @return mixed
     */
    public function execute()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');
        $transactionManager = $this->getDI()->getTransactionManager();
        $merchandise = true;


        /** @var \MerchandiseCalculation[] $calculations */
        $calculations = $entityManager->find(\MerchandiseCalculation::class, [
            \MerchandiseCalculation::PROPERTY_TYPE . ' = :type: AND ' . \MerchandiseCalculation::PROPERTY_IS_SYNCED . ' = :sync: ',
            'bind' => [
                'type' => \MerchandiseCalculation::TYPE_FINISHED,
                'sync' => false
            ]
        ]);
        /** @var Transaction $transaction */
        $transaction = $transactionManager->get();
        foreach ($calculations as $calculation) {
            if (!$calculation->getIsSynced()) {
                $calculationItems = $calculation->getMerchandiseCalculationItems();
                try {

                    foreach ($calculationItems as $calculationItem) {

                            $importer = new PutProducePrice($calculationItem->getProduct(), $entityManager, $merchandise);
                            $importer->run();

                            $calculationItem->setIsSynced(true);

                            if (!$entityManager->save($calculationItem)) {
                                $transaction->rollback();
                                continue;
                            }

                    }

                    $query = $this->modelsManager->createBuilder()
                        ->columns([
                            'isSynced'
                        ])
                        ->addFrom(\MerchandiseCalculationItem::class, 'mci')
                        ->where('mci.merchandiseCalculationId = :merchandiseCalculationId:', [
                            'merchandiseCalculationId' => $calculation->getId()
                        ])
                        ->andWhere('mci.isSynced = :sync:',[
                            'sync' => 0
                        ])
                        ->groupBy('isSynced');

                    $checkSync = ($query->getQuery()->execute())->count();


                    if ($checkSync == 0) {
                        $calculation->setIsSynced(true);

                        if (!$entityManager->save($calculation)) {
                            $transaction->rollback();
                            continue;
                        }
                    }
                    $transaction->commit();

                } catch (\Exception $e) {
                    try {
                        $transaction->rollback();
                    } catch (Transaction\Failed $e) {
                    }

                    throw $e;
                }
            }

        }
    }
}
