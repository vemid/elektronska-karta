<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 10/24/18
 * Time: 13:10
 */

namespace Vemid\Task\Products;


use Phalcon\Mvc\Model\Transaction;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Service\MisWsdl\PutProducePrice;
use Vemid\Task\AbstractTask;

/**
 * Class ImportProducePrices
 * @package Vemid\Task\Products
 */
class ImportProducePrices extends AbstractTask
{
    /**
     * @return mixed
     */
    public function execute()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');
        $transactionManager = $this->getDI()->getTransactionManager();
        $merchandise = false;

        /** @var Transaction $transaction */
        $transaction = $transactionManager->get();

        /** @var \ProductCalculation[] $calculations */
        $calculations = $entityManager->find(\ProductCalculation::class, [
            \ProductCalculation::PROPERTY_STATUS . ' = :status: AND ' . \ProductCalculation::PROPERTY_COMPLETED . ' = :completed: ',
            'bind' => [
                'status' => \ProductCalculation::STATUS_FINAL,
                'completed' => true
            ]
        ]);

        foreach ($calculations as $calculation) {
            if (!$calculation->getIsSynced()) {
                try {
                    $importer = new PutProducePrice($calculation->getProduct(), $entityManager,$merchandise);
                    $importer->run();

                    $calculation->setIsSynced(true);

                    if (!$entityManager->save($calculation)) {
                        $transaction->rollback();
                        continue;
                    }

                    $transaction->commit();
                } catch (\Exception $e) {
                    try {
                        $transaction->rollback();
                    } catch (Transaction\Failed $e) {}

                    throw $e;
                }
            }
        }
    }
}
