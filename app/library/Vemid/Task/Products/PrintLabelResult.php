<?php


namespace Vemid\Task\Products;

use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Task\AbstractTask;
use Vemid\Db\MSSql\Read;

class PrintLabelResult extends AbstractTask
{
    /**
     * @return mixed
     * @throws
     */
    public function execute()
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->get('entityManager');

        $config = $this->getDI()->getConfig();

        $date = (new DateTime())->format("Y-m-d");

        $tsql = "select * from printedLabels where created = '$date';";
        $data = (new Read($tsql))->getData();

        if(is_array($data)) {
            foreach ($data as $row) {
                /** @var \PrintLabelItem $printLabelItem */
                $printLabelItem = $entityManager->findOne(\PrintLabelItem::class, [
                    \PrintLabelItem::PROPERTY_ID . ' = :id:',
                    'bind' => [
                        'id' => $row->id
                    ]
                ]);

                if ($printLabelItem) {
                    $printLabelItem->setPrinted(true);
                    $printLabelItem->setPrintedDate(date_format($row->createdTime,"Y-m-d H:i:s"));
                }
                if (!$entityManager->save($printLabelItem)) {
                    throw new \DomainException('PrintLabel value was not saved');
                }
            }
        }
    }

}