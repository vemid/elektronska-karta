<?php

namespace Vemid\Task\Products;

use Google\Cloud\Storage\StorageClient;
use Vemid\Entity\Type;
use Vemid\Mailer\Manager;
use Vemid\Task\AbstractTask;

class ExportProductColor extends AbstractTask
{

    /** @var array*/
    public $users;

    /** @var array */
    public $seasonCode;

    /** @var string */
    public $fileName;

    /**
     * {@inheritdoc}
     */

    public function execute()
    {
        $classificationCode = Type::CLASSIFICATION;


        /** @var \ElectronCard $allProducts */
        $allProducts = $this->modelsManager->createBuilder()
            ->from(\ElectronCard::class)
            ->leftJoin(\Product::class,"p.id = ElectronCard.productId",'p')
            ->leftJoin(\ProductClassification::class,"pc.productId = p.id and pc.entityTypeId = {$classificationCode}", 'pc')
            ->leftJoin(\Classification::class, "pc.entityId = c.id",'c')
            ->where('c.code IN ({bindParam:array})',
                [
                    'bindParam' => $this->seasonCode
                ])
            ->andWhere('ElectronCard.active = 1')
            ->orderBy('p.code');


        $electonCards = $allProducts->getQuery()->execute();

        $products = array();

        /** @var \ElectronCard $electonCard */
        foreach ($electonCards as $electonCard) {
            $hydrator = new \Vemid\Service\Product\Hydrator($electonCard->getProduct(), $this->getDI()->getEntityManager());

            $products[] = [
                'priority' => $electonCard->getProduct()->getPriority(),
                'classification' => stripslashes($hydrator->getProductGroup()->getName()),
                'season' => stripslashes($hydrator->getSeason()->getName()),
                'product' => $electonCard->getProduct(),
                'oldCollection' => stripslashes($hydrator->getOldCollection()->getName()),
                'sizes' => $hydrator->getAllProductSizes(),
                'image' => $electonCard->getProduct()->getImagePrintPath(),
                'electronCardFactories' => $electonCard->getElectronCardFactoriesWithType(\ElectronCardFactory::BASIC)
            ];
        }

        $data = [];
        foreach ($products as $product) {
            $data[$product['season']][$product['classification']][] = $product;
        }

        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $this->fileName);
        $renderer = $this->getDI()->getPdfRenderer();
        $renderer->setPageSize('210mm', '297mm');

        $html = $this->getDI()->getSimpleView()->render('products/partials/product-color', [
            'data' => $data
        ]);
        $file = $renderer->render($html, 'portrait');

        $pdfOne = sprintf(
            '%s/%s1.pdf',
            rtrim(sys_get_temp_dir(), '/'),
            'tmp'
        );

        file_put_contents($pdfOne, $file);

        $config = $this->getDI()->getConfig();
        $google = $config->googleStorage;

        $storage = new StorageClient([
            'keyFile' => json_decode(file_get_contents($google->key), true)
        ]);

        $bucket = $storage->bucket($google->bucket);
        $nameOfObject =$google->folder.$name. '.pdf';

        $bucket->upload(
            fopen($pdfOne, 'r'),
            [
                'predefinedAcl' => 'publicRead',
                'name' => $nameOfObject
            ]
        );

        $objectFile =$google->downloadUrl.$google->bucket.'/'.$nameOfObject;


        /*  STARA FUNKCIONALNOST   */
//        $conn_id = ftp_connect('51.255.80.216');
//        $ftp_user_name = "nbsoft_ftp_usr";
//        $ftp_user_pass = "BYc6d6ff68075ehp";
//
//        if (!ftp_login($conn_id, $ftp_user_name, $ftp_user_pass)) {
//            throw new \LogicException(sprintf('Login to FTP failed!'));
//        }
//
//        if (!ftp_chdir($conn_id, '/httpdocs/elkarta')) {
//            throw new \LogicException(sprintf('DIR do not exist!'));
//        }
//
//        if (!ftp_put($conn_id, $name . '.pdf', $pdfOne, FTP_BINARY)) {
//            throw new \LogicException(sprintf('There was a problem while uploading %s', $pdfOne));
//        }


        $config = $this->getDI()->getConfig()->toArray();
        $mailManager = new Manager($config['mailer']);

        $message = $mailManager->createMessageFromView('mail-template/export-product-pdf', [
            'fileName' => $name,'subject' => 'izbojenja','link' => $objectFile
        ]);
        $message->setSubject('Export izbojenja '.$name);
        $message->setTo($this->users);
        $message->send();

        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="' . $name . '.pdf"');
        unlink($pdfOne);
        //ftp_close($conn_id);
    }
}
