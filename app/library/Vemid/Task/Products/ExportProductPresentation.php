<?php

namespace Vemid\Task\Products;

use Google\Cloud\Storage\StorageClient;
use Symfony\Polyfill\Util\Binary;
use Vemid\Entity\Type;
use Vemid\Mailer\Manager;
use Vemid\Task\AbstractTask;
use \Vemid\Service\Product\Hydrator;

/**
 * Class ExportProductPresentation
 * @package Vemid\Task\Products
 */
class ExportProductPresentation extends AbstractTask
{

    /** @var array */
    public $users;

    /** @var array */
    public $seasonCode;

    /** @var $fileName */
    public $fileName;

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $classificationCode = Type::CLASSIFICATION;

        $query = $this->modelsManager->createBuilder()
            ->from(\Product::class)
            ->leftJoin(\ProductClassification::class, "pc.productId = Product.id and pc.entityTypeId = {$classificationCode}", 'pc')
            ->leftJoin(\Classification::class, "pc.entityId = c.id", 'c')
            ->leftJoin(\Priority::class, "Product.priorityId = pr.id", 'pr')
            ->where('c.code IN ({bindParam:array})',
                [
                    'bindParam' => $this->seasonCode
                ])
            ->orderBy('pr.priorityNumber ASC, Product.code ASC')
            ->limit(400);


        /** @var \Product[] $productCollections */
        $productCollections = $query->getQuery()->execute();

        $products = [];

        /** @var \Product $product */
        foreach ($productCollections as $product) {
            $hydrator = new Hydrator($product, $this->getDI()->getEntityManager());

            $products[] = [
                'priority' => $product->getPriority(),
                'classification' => $hydrator->getProductGroup()->getName(),
                'season' => $hydrator->getSeason()->getName(),
                'product' => $product,
                'oldCollection' => $hydrator->getOldCollection()->getName(),
                'sizes' => $hydrator->getAllProductSizes(),
                'image' => $product->getImagePrintPath(),
                'gender' => $hydrator->getGenderName(),
            ];
        }

        $query2 = $this->modelsManager->createBuilder()
            ->from(\Product::class)
            ->leftJoin(\ProductClassification::class, "pc.productId = Product.id and pc.entityTypeId = {$classificationCode}", 'pc')
            ->leftJoin(\Classification::class, "pc.entityId = c.id", 'c')
            ->leftJoin(\Priority::class, "Product.priorityId = pr.id", 'pr')
            ->where('c.code IN ({bindParam:array})',
                [
                    'bindParam' => $this->seasonCode
                ])
            ->orderBy('Product.code ASC')
            ->limit(400);


        /** @var \Product[] $productCollections */
        $productCollectionsSizes = $query2->getQuery()->execute();

        $productSizes = [];

        /** @var \Product $product */
        foreach ($productCollectionsSizes as $product) {
            $hydrator = new Hydrator($product, $this->getDI()->getEntityManager());

            $productSizes[] = [
                'priority' => $product->getPriority(),
                'classification' => $hydrator->getProductGroup()->getName(),
                'season' => $hydrator->getSeason()->getName(),
                'product' => $product,
                'oldCollection' => $hydrator->getOldCollection()->getName(),
                'sizes' => $hydrator->getAllProductSizes(),
                'image' => $product->getImagePrintPath(),
                'gender' => $hydrator->getGenderName(),
            ];
        }

        $data = [];
        foreach ($products as $product) {
            /** @var \Priority $priority */
            $priority = $product['priority'];
            $data[$product['season']][$product['gender']][$priority->getName() . '-' . $priority->getPriorityDate()->getShortSerbianFormat()][] = $product;
        }

        $dataSizes = array();
        foreach ($productSizes as $product) {
            $dataSizes[$product['season']][$product['gender']][] = $product;

        }

        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $this->fileName);
        $renderer = $this->getDI()->getPdfRenderer();
        $renderer->setPageSize('210mm', '297mm');
        $renderer->setMargins('2mm','5mm','2mm','5mm');

        $html = $this->getDI()->getSimpleView()->render('products/partials/product-presentation', [
            'data' =>$data,
            'dataSizes' =>$dataSizes]
        );
        $file = $renderer->render($html, 'landscape');

        $pdfOne = sprintf(
            '%s/%s1.pdf',
            rtrim(sys_get_temp_dir(), '/'),
            'tmp'
        );

        file_put_contents($pdfOne, $file);

        $config = $this->getDI()->getConfig();
        $google = $config->googleStorage;

        $storage = new StorageClient([
            'keyFile' => json_decode(file_get_contents($google->key), true)
        ]);

        $bucket = $storage->bucket($google->bucket);
        $nameOfObject =$google->folder.$name. '.pdf';

        $bucket->upload(
            fopen($pdfOne, 'r'),
            [
                'predefinedAcl' => 'publicRead',
                'name' => $nameOfObject
            ]
        );

        $objectFile =$google->downloadUrl.$google->bucket.'/'.$nameOfObject;


        $config = $this->getDI()->getConfig()->toArray();
        $mailManager = new Manager($config['mailer']);
        $message = $mailManager->createMessageFromView('mail-template/export-product-pdf', [
            'fileName' => $name,'subject' => 'prezentaciju', 'link' => $objectFile
        ]);
        $message->setSubject('Primer prezentacije '.$name);
        $message->setTo($this->users);

        $message->send();

        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="' . $name . '.pdf"');

        unlink($pdfOne);
        //ftp_close($connId);
    }

}