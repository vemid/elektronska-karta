<?php

namespace Vemid\Task\Products;

use Vemid\Entity\Type;
use Vemid\Mailer\Manager;
use Vemid\Task\AbstractTask;
use Google\Cloud\Storage\StorageClient;

class ExportProductCalculation extends AbstractTask
{

    /** @var array*/
    public $users;

    /** @var array */
    public $seasonCode;

    /** @var string */
    public $fileName;

    /**
     * {@inheritdoc}
     */

    public function execute()
    {

        ob_start();
        $classificationCode = Type::CLASSIFICATION;


        /** @var \ProductCalculation $allProducts */
        $allProducts = $this->modelsManager->createBuilder()
            ->from(\ProductCalculation::class)
            ->leftJoin(\Product::class,"p.id = ProductCalculation.productId",'p')
            ->leftJoin(\ProductClassification::class,"pc.productId = p.id and pc.entityTypeId = {$classificationCode}", 'pc')
            ->leftJoin(\Classification::class, "pc.entityId = c.id",'c')
            ->where('c.code IN ({bindParam:array})',
                [
                    'bindParam' => $this->seasonCode
                ])
            ->andWhere('ProductCalculation.status = "PLANED"')
            ->orderBy('c.code,p.code');


        $calculationProducts = $allProducts->getQuery()->execute();

        $products = array();

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()
            ->setCreator("Marko Vesic")
            ->setLastModifiedBy("BEBAKIDS")
            ->setTitle("Export podataka iz Excel-a")
            ->setSubject("Export podataka iz Excel-a")
            ->setDescription("Onog dana kada budete prestali da izmisljate, placam rucak u MB-u")
            ->setKeywords("Template excel");
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

        $styleData = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );

        $styleHeader = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '1C9EC9')
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->applyFromArray($styleHeader);


        $rows = array(
            array(
                'Kolekcija',
                'Velicine',
                'Sifra',
                'Naziv',
                'Poreklo',
                'Sastav',
                'Kolicina',
                'NAB Eur',
                'VP Eur',
                'NAB Vrednost',
                'VP Vrednost',
                'Marza',
            )
        );
        $objPHPExcel->getActiveSheet()->fromArray($rows,null, 'A1');

        /** @var \Pricelist $priceListEUR */
        $priceListEUR = $this->getDI()->getEntityManager()->findOne(\Pricelist::class, [
            'name = :name:',
            'bind' => [
                'name' => 'DEVIZNI'
            ]
        ]);
        $productPrices = $this->getDI()->getProductPrices();

        $rowPosition = 2;
        /** @var \ProductCalculation $product */
        foreach ($calculationProducts as $product) {
            $hydrator = new \Vemid\Service\Product\Hydrator($product->getProduct(), $this->getDI()->getEntityManager());
            $purchasePriceEUR = $productPrices->getPurchasePrice($priceListEUR, $product->getProduct()) ? $productPrices->getPurchasePrice($priceListEUR, $product->getProduct()) : 0;
            $wholesalePriceEUR = $productPrices->getWholesalePrice($priceListEUR, $product->getProduct()) ? $productPrices->getWholesalePrice($priceListEUR, $product->getProduct()) : 0;
            $objPHPExcel->getActiveSheet()->getStyle("A" . $rowPosition . ":Q" . $rowPosition)->applyFromArray($styleData);

            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowPosition, ($hydrator->getOldCollection() ? $hydrator->getOldCollection()->getName() : ''));
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowPosition, ($hydrator->getAllProductSizes() ? $hydrator->getAllProductSizes() : ''));
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowPosition, $product->getProduct()->getCode());
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowPosition, $product->getProduct()->getName());
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowPosition, $product->getProduct()->getOrigin() ? $product->getProduct()->getOrigin()->getName() : "");
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowPosition, $product->getProduct()->getComposition());
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowPosition, $product->getQty());
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowPosition, $purchasePriceEUR);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowPosition, $wholesalePriceEUR);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowPosition, $purchasePriceEUR*$product->getQty());
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowPosition, $wholesalePriceEUR*$product->getQty());
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowPosition, $wholesalePriceEUR > 0 ? number_format($wholesalePriceEUR / $purchasePriceEUR, 2) * 100 -100 . ' %' : '');

            $rowPosition++;
        }
        $objPHPExcel->getActiveSheet()->setAutoFilter("A1:L1");

        $objPHPExcel->getActiveSheet()->setCellValue("F$rowPosition", "Total");
        $objPHPExcel->getActiveSheet()->setCellValue("G$rowPosition", "=SUBTOTAL(9,G2:G".($rowPosition-1).")");
        $objPHPExcel->getActiveSheet()->setCellValue("J$rowPosition", "=SUBTOTAL(9,J2:J".($rowPosition-1).")");
        $objPHPExcel->getActiveSheet()->setCellValue("K$rowPosition", "=SUBTOTAL(9,K2:K".($rowPosition-1).")");
        $objPHPExcel->getActiveSheet()->setCellValue("J".($rowPosition+2), "=J".$rowPosition."/G$rowPosition");
        $objPHPExcel->getActiveSheet()->setCellValue("K".($rowPosition+2), "=K".$rowPosition."/G$rowPosition");
        $objPHPExcel->getActiveSheet()->setCellValue("L".($rowPosition+2), "=(K".($rowPosition+2)."/J".($rowPosition+2)."*100-100)");

        $tempFolder = rtrim(sys_get_temp_dir(), '/') . '/products-list';
        $fileName = $tempFolder . '/' . $this->fileName. '.xlsx';

        if (file_exists($fileName)) {
            unlink($fileName);
        }

        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);

        $config = $this->getDI()->getConfig();
        $google = $config->googleStorage;

        $path = APP_PATH . '/app/config/elektronska-karta-45f64b9adc48.json';

        $storage = new StorageClient([
            'keyFile' => json_decode(file_get_contents($google->key), true)
        ]);

        $bucket = $storage->bucket($google->bucket);
        $nameOfObject =$google->folder.$this->fileName. '.xlsx';

        $bucket->upload(
            fopen($fileName, 'r'),
            [
                'predefinedAcl' => 'publicRead',
                'name' => $nameOfObject
            ]
        );

        $objectFile =$google->downloadUrl.$google->bucket.'/'.$nameOfObject;



        $mailManager = $this->getDI()->getMailManager();
        $message = $mailManager->createMessageFromView('reports/order-mail-template');

        $message->setSubject("Pregled marze iz Elektronske Karte ,". $objectFile);
        $message->setTo($this->users);
        $message->setAttachment($fileName);
        $message->send();
    }
}
