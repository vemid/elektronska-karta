<?php

namespace Vemid\Printer;

class Manager
{
    /** @var string */
    private $printerUri;

    /** @var string|null */
    private $username;

    /** @var string|null */
    private $password;

    /**
     * @param string $printerUri
     * @param string|null $username
     * @param string|null $password
     */
    public function __construct(string $printerUri, string $username = null, string $password = null)
    {
        $this->printerUri = $printerUri;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @param $file
     */
    public function __invoke($file)
    {
        try {
            $printer = new PrintFile($this->printerUri, $this->username, $this->password);
            if (!$printer($file)) {
                (new FallBackWebDownload)($file);
            }
        } catch (\Exception $e) {
            (new FallBackWebDownload)($file);
        }
    }
}
