<?php

namespace Vemid\Printer;

class FallBackWebDownload
{
    public function __invoke($file)
    {
        if (!is_readable($file)) {
            throw new \LogicException('File not readable');
        }

        $filesize = filesize($file);
        $pathParts = pathinfo($file);
        $ext = strtolower($pathParts["extension"]);

        if ($ext === 'pdf') {
            header("Content-type: application/pdf");
            header("Content-Disposition: attachment; filename=\"" . $pathParts["basename"] . "\"");     // use 'attachment' to force a download
        } else {
            header("Content-type: application/octet-stream");
            header("Content-Disposition: filename=\"" . $pathParts["basename"] . "\"");
        }

        header("Content-length: $filesize");
        header("Cache-control: private");
        readfile($file);
        exit;
    }
}
