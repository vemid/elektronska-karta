<?php

namespace Vemid\Printer;

use Smalot\Cups\Builder\Builder;
use Smalot\Cups\Manager\JobManager;
use Smalot\Cups\Manager\PrinterManager;
use Smalot\Cups\Model\Job;
use Smalot\Cups\Transport\Client;
use Smalot\Cups\Transport\ResponseParser;

class PrintFile
{
    /** @var string */
    private $printerUri;

    /** @var string|null */
    private $username;

    /** @var string|null */
    private $password;

    /** @var Client */
    private $client;

    /** @var Builder */
    private $builder;

    /** @var ResponseParser */
    private $responseParser;

    /**
     * @param string $printerUri
     * @param string|null $username
     * @param string|null $password
     */
    public function __construct(string $printerUri, string $username = null, string $password = null)
    {
        $this->printerUri = $printerUri;
        $this->username = $username;
        $this->password = $password;

        $this->buildConfig();
    }

    /**
     * @param $file
     * @return bool
     */
    public function __invoke($file): bool
    {
        if (!$printer = $this->buildPrinter()) {
            throw new \LogicException('Can not find requested printer');
        }

        $jobManager = new JobManager(
            $this->builder,
            $this->client,
            $this->responseParser
        );

        $job = new Job();
        $job->setName('Elektronska karta');
        $job->setCopies(1);
        $job->addFile($file);
        $job->setUsername($this->username);
        $job->setPageRanges('1-100');
        $job->addAttribute('media', 'A4');
        $job->addAttribute('fit-to-page', true);

        return $jobManager->send($printer, $job);
    }

    /**
     * @return false|\Smalot\Cups\Model\Printer
     */
    private function buildPrinter()
    {
        $printerManager = new PrinterManager(
            $this->builder,
            $this->client,
            $this->responseParser
        );

        return $printerManager->findByUri($this->printerUri);
    }

    private function buildConfig()
    {
        $this->builder = new Builder();
        $this->responseParser = new ResponseParser();
        $this->client = new Client($this->username,$this->password);
    }
}