<?php

namespace Vemid\Exporter\Order;

use Vemid\Date\DateTime;
use Vemid\Table\Column;
use Vemid\Table\Definition;

/**
 * Class TableDefinition
 *
 * @package Vemid\Exporter\SalesInvoices
 */
class TableDefinition extends Definition
{
    const COLUMN_CODE = 'code';
    const COLUMN_SIZE = 'size';
    const COLUMN_NAME = 'name';
    const COLUMN_A = 'totalA';
    const COLUMN_B = 'totalB';
    const COLUMN_TOTAL = 'total';
    const COLUMN_ROUNDED = 'rounded';
    const COLUMN_TOTAL_ROUNDED = 'totalRounded';

    public function __construct()
    {
        $this->addColumn(self::COLUMN_CODE)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel('Šifra');

        $this->addColumn(self::COLUMN_SIZE)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel('Veličina');

        $this->addColumn(self::COLUMN_NAME)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel('Naziv');

        $this->addColumn(self::COLUMN_A)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel('A');

        $this->addColumn(self::COLUMN_B)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel('B');

        $this->addColumn(self::COLUMN_TOTAL)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel('Zbirno');

        $this->addColumn(self::COLUMN_ROUNDED)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel('Zaokruzeno');

        $this->addColumn(self::COLUMN_TOTAL_ROUNDED)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel('Ukupno Zaokruzeno');

    }
}
