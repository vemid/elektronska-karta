<?php

namespace Vemid\Exporter\Order;

use Phalcon\Mvc\Model\Query\BuilderInterface;
use Vemid\Entity\Type;
use Vemid\Exporter\AbstractExporter;
use Vemid\Table\Table;

/**
 * Class Exporter
 * @package Vemid\Exporter\Order
 */
class Exporter extends AbstractExporter
{
    /** @var \Order */
    private $order;

    /** @var \Classification */
    private $classification;

    /**
     * Exporter constructor.
     * @param BuilderInterface $queryBuilder
     * @param \Order $order
     * @param \Classification $classification
     */
    public function __construct(BuilderInterface $queryBuilder, \Order $order, \Classification $classification)
    {
        $this->order = $order;
        $this->classification = $classification;

        parent::__construct($queryBuilder);
    }

    /**
     * @return Table
     */
    public function export()
    {
        $table = new Table(new TableDefinition());
        $table->setTitle(substr($this->classification->getName(), 0, 31));

        $this->populateRows($table);

        return $table;
    }

    /**
     * @param Table $table
     */
    private function populateRows(Table $table)
    {
        $products = [];
        $totalRounded = 0;
        $records = $this->getRecords()->toArray();
        $totalRows = \count($records);
        $counter = 1;

        foreach ($records as $key => $record) {
            $row = $table->addRow();

            $totalRounded += $record[TableDefinition::COLUMN_ROUNDED];
            $nextRecord = $records[$key + 1] ?? $records[1];
            $productCode = $nextRecord[TableDefinition::COLUMN_CODE];

            if (!\in_array($record[TableDefinition::COLUMN_CODE], $products, false)) {
                $products[] = $productCode;
            }

            if ($counter === $totalRows || (\count($products) > 0 && !\in_array($productCode, $products, false))) {
                $row->addCell(TableDefinition::COLUMN_TOTAL_ROUNDED, $totalRounded);
                $totalRounded = 0;
            }

            foreach ($record as $columnName => $value) {
                $row->addCell($columnName, $value);
            }

            $counter++;
        }
    }

    /**
     * @return array
     */
    private function getRecords()
    {
        return $this
            ->queryBuilder
            ->columns([
                'p.code',
                'cos.name as ' . TableDefinition::COLUMN_SIZE,
                'p.name',
                'SUM(oi.quantityA) as totalA',
                'SUM(oi.quantityB) as totalB',
                '(SUM(oi.quantityA) + SUM(oi.quantityB)) as ' . TableDefinition::COLUMN_TOTAL,
                'ROUND(((SUM(oi.quantityA) + SUM(oi.quantityB)))/5)*5 as ' . TableDefinition::COLUMN_ROUNDED
            ])
            ->addFrom(\ClientOrderItem::class, 'oi')
            ->leftJoin(\ProductSize::class, 'ps.id = oi.productSizeId', 'ps')
            ->leftJoin(\Code::class, 'cos.id = ps.codeId', 'cos')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->leftJoin(\ProductClassification::class, 'pc.productId = p.id', 'pc')
            ->leftJoin(\Classification::class, 'c.id = pc.entityId', 'c')
            ->leftJoin(\Code::class, 'co.id = c.classificationTypeCodeId', 'co')
            ->leftJoin(\CodeType::class, 'ct.id = co.codeTypeId', 'ct')
            ->where('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLASSIFICATION
            ])
            ->andWhere('ct.code = :codeTypeCode:', [
                'codeTypeCode' => \CodeType::CLASSIFICATION_TYPE
            ])
            ->andWhere('oi.orderId = :orderId:', [
                'orderId' => $this->order->getId()
            ])
            ->andWhere('c.id = :classification:', [
                'classification' => $this->classification->getId()
            ])
            ->groupBy(['p.code', 'cos.name'])
            ->orderBy('p.code ASC, ps.codeId ASC')
            ->getQuery()
            ->execute();
    }
}