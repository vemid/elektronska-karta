<?php

namespace Vemid\Exporter;

use Vemid\Table\Table;
use Phalcon\Mvc\Model\Query\BuilderInterface;

/**
 * Class AbstractQuery
 *
 * @package Vemid\Exporter
 */
abstract class AbstractExporter
{
    /** @var BuilderInterface */
    protected $queryBuilder;

    /**
     * AbstractExporter constructor.
     * @param BuilderInterface $queryBuilder
     */
    public function __construct(BuilderInterface $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * @return Table
     */
    abstract public function export();
}
