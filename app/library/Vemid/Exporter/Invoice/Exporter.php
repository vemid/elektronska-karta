<?php

namespace Vemid\Exporter\Invoice;

use Google\Cloud\Translate\V2\TranslateClient;
use Phalcon\Mvc\Model\Query\BuilderInterface;
use Vemid\Application\Di;
use Vemid\CourierService\Http\Client;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Exporter\AbstractExporter;
use Vemid\Exporter\Invoice\TableDefinition;
use Vemid\Service\MaterialFactory\MaterialFactoryCareLabel;
use Vemid\Service\Product\Hydrator;
use Vemid\Table\Table;
use Vemid\Service\GoogleTranslator\GoogleTranslateClient;


/**
 * Class Exporter
 * @package Vemid\Exporter\Invoice
 */
class Exporter extends AbstractExporter
{
    /** @var \Vemid\Entity\EntityInterface */
    private $object;

    /** @var \User */
    private $user;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var \Phalcon\Translate\AdapterInterface */
    private $translator;

    /**
     * Exporter constructor.
     * @param BuilderInterface $queryBuilder
     * @param EntityManagerInterface $entityManager
     * @param \Vemid\Entity\EntityInterface $object
     * @param \User $user
     * @param \Phalcon\Translate\AdapterInterface $translator
     */
    public function __construct(BuilderInterface $queryBuilder, \Vemid\Entity\EntityInterface $object,\User $user, EntityManagerInterface $entityManager,\Phalcon\Translate\AdapterInterface $translator )
    {
        $this->object = $object;
        $this->user = $user;
        $this->entityManager = $entityManager;
        $this->queryBuilder = $queryBuilder;
        $this->translator = $translator;

        parent::__construct($queryBuilder);
    }

    /**
     * @return Table
     */
    public function export()
    {
        $table = new Table(new TableDefinition($this->user,$this->translator));
        $table->setTitle("Invoice data");

        $this->populateRows($table);

        return $table;
    }

    /**
     * @param Table $table
     */
    private function populateRows(Table $table)
    {

        $records="";
        $products = [];
        if($this->object->getEntityName() == "Invoice") {
            $records = $this->getRecords()->toArray();
        }
        else {
            $records = $this->getOrderRecords()->toArray();
        }
        $totalRows = \count($records);
        $counter = 1;
        $productCheck = "";
        foreach ($records as $key => $record) {
            $row = $table->addRow();

            $nextRecord = $records[$key + 1] ?? $records[1];
            $productCode = $nextRecord[TableDefinition::COLUMN_CODE];

            if (!\in_array($record[TableDefinition::COLUMN_CODE], $products, false)) {
                $products[] = $productCode;
            }

//            if($productCheck <> $records[$key]['code'] )
//            /** @var \Product $product */
//            $product = $this->entityManager->findOne(\Product::class, [
//                \Product::PROPERTY_CODE . ' = :code:',
//                'bind' => [
//                    'code' => $records[$key]['code']
//                ]
//            ]);
//
//            $productCheck = $product->getCode();
//
//            /** @var Hydrator $hydrator */
//            $hydrator = new \Vemid\Service\Product\Hydrator($product, $this->entityManager);
//
//
            $record['model'] = $records[$key]['model'];
            $record['color'] = $records[$key]['color'];
            $record['gender'] = $records[$key]['gender'];
            $record['priority'] = $records[$key]['priority'];
            $record['season'] = $records[$key]['season'];
//
            $translateKeys = ['model','paperLabel','color','gender','season'];

            foreach ($record as $columnName => $value) {

                if(in_array($columnName,$translateKeys)) {
//                    $translatedText = $this->translateClient->getTranslated($value,$this->user->getLanguage());
                        $translatedText = $this->translator->t($value);
                }
                else {
                $translatedText = $value;
                }

                $row->addCell($columnName, $translatedText);
            }

            $counter++;
        }
    }

    /**
     * @return array
     */
    private function getRecords()
    {
        return $this
            ->queryBuilder
            ->columns([
                'p.code',
                'cos.name as ' . TableDefinition::COLUMN_SIZE,
                'p.name',
                'ii.price',
                'ifnull(f.paperLabel,p.composition) paperLabel',
                'ifnull(f.washLabel,p.washLabel) washLabel',
                'ps.barcode',
                'pt1.tag as model',
                'pt4.tag as season',
                'pr.name as priority',
                'pt2.tag as color',
                'pt3.tag as gender',
                'ii.qty quantity'
            ])
            ->addFrom(\InvoiceItem::class, 'ii')
            ->leftJoin(\ProductSize::class, 'ps.id = ii.productSizeId', 'ps')
            ->leftJoin(\Code::class, 'cos.id = ps.codeId', 'cos')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->leftJoin(\FarbenCard::class, 'p.id = f.productId', 'f')
            ->leftJoin(\ProductTag::class,"p.id = pt1.productId and pt1.category = '".\ProductTag::MODEL."'",'pt1')
            ->leftJoin(\ProductTag::class,"p.id = pt2.productId and pt2.category = '".\ProductTag::COLOR."'",'pt2')
            ->leftJoin(\ProductTag::class,"p.id = pt3.productId and pt3.category = '".\ProductTag::GENDER."'",'pt3')
            ->leftJoin(\ProductTag::class,"p.id = pt4.productId and pt4.category = '".\ProductTag::SEASON."'",'pt4')
            ->leftJoin(\Priority::class,"pr.id = p.priorityId",'pr')
            ->where('ii.invoiceId = :invoiceId:', [
                'invoiceId' => $this->object->getId()
            ])
            ->orderBy('p.code ASC, ps.codeId ASC')
            ->getQuery()
            ->execute();
    }

    /**
     * @return array
     */
    private function getOrderRecords()
    {

        /** @var \Client $client */
        $client = $this->entityManager->findOne(\Client::class, [
            \Client::PROPERTY_USER_ID . ' = :userId:',
            'bind' => [
                'userId' => $this->user->getId()
            ]
        ]);

        if (!$client) {
            return $this->returnNotFound();
        }


        return $this
            ->queryBuilder
            ->columns([
                'p.code',
                'c.name as ' . TableDefinition::COLUMN_SIZE,
                'p.name',
                'ifnull(pp2.wholesalePrice,pp.wholesalePrice) as price',
                'pt1.tag as model',
                'ifnull(f.paperLabel,p.composition) paperLabel',
                'ifnull(f.washLabel,p.washLabel) washLabel',
                'ps.barcode',
                'pt4.tag as season',
                'pr.name as priority',
                'sum(ifnull(quantityA,0)+ifnull(quantityB,0)) quantity',
                'pt2.tag as color',
                'pt3.tag as gender',
            ])
            ->addFrom(\ProductSize::class, 'ps')
            ->leftJoin(\Code::class, 'c.id = ps.codeId', 'c')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->leftJoin(\ProductClassification::class, 'p.id = pc.productId', 'pc')
            ->leftJoin(\OrderClassification::class, 'oc.classificationId = pc.entityId', 'oc')
            ->leftJoin(\FarbenCard::class, 'p.id = f.productId', 'f')
            ->leftJoin(\ClientOrderItem::class, 'ps.id = oi.productSizeId  and oi.entityTypeId = '.
                Type::CLIENT . ' and oi.entityId = ' .
                $client->getId().' and oi.orderId = ' .
                $this->object->getId().'', 'oi')
            ->leftJoin(\ProductTag::class,"p.id = pt1.productId and pt1.category = '".\ProductTag::MODEL."'",'pt1')
            ->leftJoin(\ProductTag::class,"p.id = pt2.productId and pt2.category = '".\ProductTag::COLOR."'",'pt2')
            ->leftJoin(\ProductTag::class,"p.id = pt3.productId and pt3.category = '".\ProductTag::GENDER."'",'pt3')
            ->leftJoin(\ProductTag::class,"p.id = pt4.productId and pt4.category = '".\ProductTag::SEASON."'",'pt4')
            ->leftJoin(\Priority::class,"pr.id = p.priorityId",'pr')
            ->leftJoin(\ProductPrice::class,"p.id = pp.productId and pp.pricelistId = '2' and pp.status = 'PLANED'",'pp')
            ->leftJoin(\ProductPrice::class,"p.id = pp2.productId and pp2.pricelistId = '2' and pp2.status = 'FINAL'",'pp2')
            ->where('oc.orderId = :orderId:', [
                'orderId' => $this->object->getId()
            ])
            ->groupBy('p.code,c.name,p.name,paperLabel,washLabel,barcode')
            ->orderBy('pr.priorityNumber,p.code ASC, ps.codeId ASC')
            ->getQuery()
            ->execute();
    }
}