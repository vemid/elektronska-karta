<?php

namespace Vemid\Exporter\Invoice;

use Vemid\Date\DateTime;
use Vemid\Table\Column;
use Vemid\Table\Definition;
use Vemid\Service\GoogleTranslator\GoogleTranslateClient;



/**
 * Class TableDefinition
 *
 * @package Vemid\Exporter\SalesInvoices
 * @param \User $user
 */
class TableDefinition extends Definition
{
    const COLUMN_CODE = 'code';
    const COLUMN_SIZE = 'size';
    const COLUMN_NAME = 'name';
    const COLUMN_PRICE = 'price';
    const COLUMN_MODEL = 'model';
    const COLUMN_PAPER_LABEL = 'paperLabel';
    const COLUMN_WASH_LABEL = 'washLabel';
    const COLUMN_BARCODE = 'barcode';
    const COLUMN_SEASON = 'season';
    const COLUMN_PRIORITY = 'priority';
    const COLUMN_COLOR = 'color';
    const COLUMN_GENDER = 'gender';
    const COLUMN_QUANTITY = 'quantity';

    const COLUMN_WASH_IMAGE_LABEL_1 = 'washImageLabel1';
    const COLUMN_WASH_IMAGE_LABEL_2 = 'washImageLabel2';
    const COLUMN_WASH_IMAGE_LABEL_3 = 'washImageLabel3';
    const COLUMN_WASH_IMAGE_LABEL_4 = 'washImageLabel4';
    const COLUMN_WASH_IMAGE_LABEL_5 = 'washImageLabel5';
    const COLUMN_WASH_IMAGE_LABEL_6 = 'washImageLabel6';

    /** @var \User */
    private $user;

    /** @var \Phalcon\Translate\AdapterInterface */
    private $translator;

    /**
     * Exporter constructor.
     *
     * @package Vemid\Exporter\SalesInvoices
     * @param \User $user
     * @param \Phalcon\Translate\AdapterInterface $translator
     */
    public function __construct(\User $user, \Phalcon\Translate\AdapterInterface $translator)
    {
        
        $this->user = $user;
        $this->translator = $translator;

        $this->addColumn(self::COLUMN_CODE)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel($this->translator->t('Šifra'));

        $this->addColumn(self::COLUMN_SIZE)
        ->setDataType(Column::DATA_TYPE_STRING)
        ->setLabel($this->translator->t('Veličina'));

        $this->addColumn(self::COLUMN_QUANTITY)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel($this->translator->t('Kolicina'));

        $this->addColumn(self::COLUMN_NAME)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel($this->translator->t('Naziv'));

        $this->addColumn(self::COLUMN_PRICE)
            ->setDataType(Column::DATA_TYPE_MONEY)
            ->setLabel($this->translator->t('Cena'));

        $this->addColumn(self::COLUMN_MODEL)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel($this->translator->t('Model'));

        $this->addColumn(self::COLUMN_COLOR)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel($this->translator->t('Boja'));

        $this->addColumn(self::COLUMN_GENDER)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel($this->translator->t('Pol'));

        $this->addColumn(self::COLUMN_SEASON)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel($this->translator->t('Sezona'));

        $this->addColumn(self::COLUMN_PRIORITY)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel($this->translator->t('Prioritet'));

        $this->addColumn(self::COLUMN_BARCODE)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel('Barcode');

        $this->addColumn(self::COLUMN_PAPER_LABEL)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel($this->translator->t('Sirovinski Sastav'));

        $this->addColumn(self::COLUMN_WASH_LABEL)
            ->setDataType(Column::DATA_TYPE_STRING)
            ->setLabel($this->translator->t('Odrzavanje Tekstila'));



//        $this->addColumn(self::COLUMN_WASH_IMAGE_LABEL_1)
//            ->setDataType(Column::DATA_TYPE_STRING)
//            ->setLabel($this->translator->t('Odrzavanje 1'));
//
//        $this->addColumn(self::COLUMN_WASH_IMAGE_LABEL_2)
//            ->setDataType(Column::DATA_TYPE_STRING)
//            ->setLabel($this->translator->t('Odrzavanje 2'));
//
//        $this->addColumn(self::COLUMN_WASH_IMAGE_LABEL_3)
//            ->setDataType(Column::DATA_TYPE_STRING)
//            ->setLabel($this->translator->t('Odrzavanje 1'));
//
//        $this->addColumn(self::COLUMN_WASH_IMAGE_LABEL_4)
//            ->setDataType(Column::DATA_TYPE_STRING)
//            ->setLabel($this->translator->t('Odrzavanje 2'));
//
//        $this->addColumn(self::COLUMN_WASH_IMAGE_LABEL_5)
//            ->setDataType(Column::DATA_TYPE_STRING)
//            ->setLabel($this->translator->t('Odrzavanje 1'));
//
//        $this->addColumn(self::COLUMN_WASH_IMAGE_LABEL_6)
//            ->setDataType(Column::DATA_TYPE_STRING)
//            ->setLabel($this->translator->t('Odrzavanje 2'));

    }
}
