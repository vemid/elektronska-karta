<?php

namespace Vemid\Database;

use Phalcon\Db\Column;

/**
 * Class Field
 *
 * @package Database
 * @author Vemid
 */
class Field
{

    const TYPE_TINYINT = Column::TYPE_BOOLEAN;
    const TYPE_SMALLINT = Column::TYPE_INTEGER;
    const TYPE_MEDIUMINT = Column::TYPE_INTEGER;
    const TYPE_INT = Column::TYPE_INTEGER;
    const TYPE_BIGINT = Column::TYPE_INTEGER;
    CONST TYPE_DECIMAL = Column::TYPE_DECIMAL;
    const TYPE_FLOAT = Column::TYPE_FLOAT;
    const TYPE_DOUBLE = Column::TYPE_FLOAT;
    const TYPE_REAL = Column::TYPE_FLOAT;
    const TYPE_CHAR = Column::TYPE_CHAR;
    const TYPE_VARCHAR = Column::TYPE_VARCHAR;
    const TYPE_TINYTEXT = Column::TYPE_TEXT;
    const TYPE_MEDIUMTEXT = Column::TYPE_TEXT;
    const TYPE_TEXT = Column::TYPE_TEXT;
    const TYPE_LONGTEXT = Column::TYPE_TEXT;
    const TYPE_DATE = Column::TYPE_DATE;
    const TYPE_DATETIME = Column::TYPE_DATETIME;

    /** @var bool */
    private $isPrimaryKey;

    /** @var int */
    private $maxLength;

    /** @var string */
    private $name;

    /** @var string */
    private $type;

    /** @var bool */
    private $isReverenceKey = false;

    /** @var bool */
    private $isNotNull = false;

    /** @var Table */
    private $referenceTable;


    /**
     * @return bool
     */
    public function isPrimaryKey()
    {
        return (bool)$this->isPrimaryKey;
    }

    /**
     * @param bool $isPrimaryKey
     * @return $this
     */
    public function setIsPrimaryKey($isPrimaryKey)
    {
        $this->isPrimaryKey = (bool)$isPrimaryKey;

        return $this;
    }

    /**
     * @return bool
     */
    public function isNotNull()
    {
        return (bool)$this->isNotNull;
    }

    /**
     * @param bool $isNotNull
     * @return $this
     */
    public function setIsNotNull($isNotNull)
    {
        $this->isNotNull = (bool)$isNotNull;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxLength()
    {
        return $this->maxLength;
    }

    /**
     * @param int $maxLength
     * @return $this
     */
    public function setMaxLength($maxLength)
    {
        $this->maxLength = $maxLength;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return bool
     */
    public function isReferenceKey()
    {
        return (bool)$this->isReverenceKey;
    }

    /**
     * @param bool $isReverenceKey
     * @return $this
     */
    public function setIsReferenceKey($isReverenceKey)
    {
        $this->isReverenceKey = (bool)$isReverenceKey;

        return $this;
    }

    /**
     * @return Table|null
     */
    public function getReferenceTable()
    {
        return $this->referenceTable;
    }

    /**
     * @param Table $referenceTable
     * @return $this
     */
    public function setReferenceTable(Table $referenceTable)
    {
        $this->referenceTable = $referenceTable;

        return $this;
    }

    /**
     * @return string
     */
    public function getPropertyName()
    {
        $propertyName = 'x' . strtolower(trim($this->name));
        $propertyName = ucwords(preg_replace('/[\s_]+/', ' ', $propertyName));

        return substr(str_replace(' ', '', $propertyName), 1);
    }

    /**
     * @return string
     */
    public function getHumanName()
    {
        return ucfirst(strtolower(str_replace('_', ' ', $this->name)));
    }

    /**
     * @return bool
     */
    public function isBoolean()
    {
        $booleans = array(
            self::TYPE_TINYINT,
        );

        return in_array($this->type, $booleans) || (1 == $this->getMaxLength() && $this->isInteger());
    }

    /**
     * @return bool
     */
    public function isInteger()
    {
        $integers = array(
            self::TYPE_SMALLINT,
            self::TYPE_INT,
            self::TYPE_BIGINT,
        );

        return in_array($this->type, $integers);
    }

    /**
     * @return bool
     */
    public function isFloat()
    {
        $floats = array(
            self::TYPE_FLOAT,
            self::TYPE_DECIMAL,
            self::TYPE_REAL,
            self::TYPE_DOUBLE,
        );

        return in_array($this->type, $floats);
    }

    /**
     * @return bool
     */
    public function isString()
    {
        $strings = array(
            self::TYPE_CHAR,
            self::TYPE_VARCHAR,
            self::TYPE_TINYTEXT,
            self::TYPE_MEDIUMTEXT,
            self::TYPE_LONGTEXT,
            self::TYPE_TEXT,
        );

        return in_array($this->type, $strings);
    }

    /**
     * @return bool
     */
    public function isText()
    {
        $strings = array(
            self::TYPE_TINYTEXT,
            self::TYPE_MEDIUMTEXT,
            self::TYPE_LONGTEXT,
            self::TYPE_TEXT,
        );

        return in_array($this->type, $strings);
    }

    /**
     * @return bool
     */
    public function isDate()
    {
        $dates = array(
            self::TYPE_DATE,
            self::TYPE_DATETIME,
        );

        return in_array($this->type, $dates);
    }

    /**
     * @return string
     */
    public function getTypeDisplayName()
    {
        if ($this->isInteger()) {
            return 'integer';
        } elseif ($this->type === self::TYPE_DATE) {
            return 'date';
        } elseif ($this->type === self::TYPE_DATETIME) {
            return 'dateTime';
        } elseif ($this->isFloat()) {
            return 'decimal';
        } else {
            return 'string';
        }
    }

    /**
     * @return string
     */
    public function getFormElementType()
    {
        if ($this->isReferenceKey()) {
            return 'Select';
        } else if ($this->isText()) {
            return 'TextArea';
        } else if ($this->isDate()) {
            return 'Date';
        } else if ($this->isBoolean()) {
            return 'Check';
        } else {
            return 'Text';
        }
    }

}
