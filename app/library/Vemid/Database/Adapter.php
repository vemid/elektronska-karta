<?php

namespace Vemid\Database;

use Vemid\Filter\Plural;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Db\Column;

/**
 * Class Adapter
 *
 * @package Database
 * @author Vemid
 */
class Adapter
{

    private $db;

    /**
     * @param $db
     */
    public function __construct(DbAdapter $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $tableName
     * @return Table
     */
    public function getTable($tableName)
    {
        $table = new Table($this);
        $table->setName($tableName);

        if ($table->getName() === 'order_preparation_product') {
            return;
        }

        if ($this->db->tableExists($tableName)) {
            /** @var Column $dbField */
            foreach ($this->db->describeColumns($tableName) as $dbField) {

                $field = new Field();
                $field->setName($dbField->getName());
                $field->setMaxLength($dbField->getSize());
                $field->setIsPrimaryKey($dbField->isPrimary());
                $field->setType($dbField->getType());
                $field->setIsNotNull($dbField->isNotNull());

                if ($dbField::TYPE_INTEGER === $dbField->getType() && $dbField->getSize() == 11 && strpos($dbField->getName(), '_id') === (strlen($dbField->getName()) - 3)) {
                    $relatedEntityName = ucfirst(substr($dbField->getName(), 0, -3));
                    $plural = new Plural();
                    $relatedTableName = strtolower($plural->filter($relatedEntityName));

                    if ($tableName !== $relatedTableName && $this->db->tableExists($relatedTableName)) {
                        $referencedTable = $this->getTable($relatedTableName);
                        $field->setIsReferenceKey(true);
                        $field->setReferenceTable($referencedTable);
                    }
                }

                $table->addField($field);
            }
        }

        return $table;
    }


    /**
     * @return Table[]
     */
    public function listTables()
    {
        $tables = [];
        foreach ($this->db->listTables() as $tableName) {
            $tables[] = $this->getTable($tableName);
        }

        return $tables;
    }

}
