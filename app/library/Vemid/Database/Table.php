<?php

namespace Vemid\Database;

use Vemid\Filter\Singular;

/**
 * Class Table
 *
 * @package Database
 * @author Vemid
 */
class Table
{
    /** @var Adapter */
    private $adapter;

    /** @var Field[] */
    private $fields = [];

    /** @var Field[] */
    private $primaryFields = [];

    /** @var Field[] */
    private $foreignFields = [];

    /** @var string */
    private $name;

    /**
     * @param Adapter $adapter
     */
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @return Adapter
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * @param Field $field
     * @return $this
     */
    public function addField(Field $field)
    {
        $this->fields[] = $field;

        if ($field->isPrimaryKey()) {
            $this->primaryFields[] = $field;
        }

        if ($field->isReferenceKey()) {
            $this->foreignFields[] = $field;
        }

        return $this;
    }

    /**
     * @return Field[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @return Field[]
     */
    public function getPrimaryFields()
    {
        return $this->primaryFields;
    }

    /**
     * @return Field[]
     */
    public function getForeignFields()
    {
        return $this->foreignFields;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param bool $underscored
     * @return string
     */
    public function getEntityName($underscored = false)
    {
        $singular = new Singular();
        $nameArray = explode('_', strtolower($this->name));

        foreach ($nameArray as &$value) {
            $value = ucfirst($singular->filter($value));
        }

        if ($underscored === true) {
            return strtoupper(implode('_', $nameArray));
        }

        return implode('', $nameArray);
    }

    /**
     * @param bool $showSingular
     * @return string
     */
    public function getHumanTableName($showSingular = true)
    {
        if ($showSingular === false) {
            return ucwords(str_replace('_', ' ', $this->name));
        }

        $singular = new Singular();
        $nameArray = explode('_', strtolower($this->name));
        foreach ($nameArray as &$value) {
            $value = ucfirst($singular->filter($value));
        }

        return implode(' ', $nameArray);
    }

}
