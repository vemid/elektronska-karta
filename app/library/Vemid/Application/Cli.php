<?php

namespace Vemid\Application;

/**
 * Class Cli
 *
 * @package Vemid\Application
 */
final class Cli extends Application
{
    public function initialize()
    {
        $this->initLoader();
        $this->initDi();
        $this->initEnvironment();
    }
}
