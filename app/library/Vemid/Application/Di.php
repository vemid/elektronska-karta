<?php

namespace Vemid\Application;

use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Vemid\Annotations\Initializer;
use Vemid\Annotations\MetaDataInitializer;
use Vemid\Error\Handler;
use Vemid\Error\HandlerInterface;
use Vemid\Error\Logger\FileLogger;
use \Vemid\Logger\FileLogger as LogToFile;
use Vemid\Pdf\Builder;
use Vemid\Pdf\BuilderInterface;
use Vemid\CourierService\Http\PostalClientInterface;
use \Vemid\CourierService\Handler as CourierHandler;
use Vemid\Security\Acl;
use Phalcon\Annotations;
use Phalcon\Config\Adapter\Php;
use Phalcon\Crypt;
use Phalcon\Db;
use Phalcon\Di\FactoryDefault;
use Phalcon\Events;
use Phalcon\Mvc;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use Phalcon\Mvc\Model\Transaction\ManagerInterface as TransactionManagerInterface;
use Phalcon\Text;
use Phalcon\Logger;
use Phalcon\Session;
use Phalcon\Flash;
use Phalcon\Http;
use Vemid\Mailer\Manager;
use Phalcon\Acl as PhalconAcl;
use \Vemid\Entity\Manager\EntityManager;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Service\Acl\AclHelper;
use Vemid\Service\Efectus\Db\Connection;
use Vemid\Service\Efectus\Repository;
use Vemid\Service\MisWsdl\WarehouseOrder\MisPusher;
use Vemid\Service\MisWsdl\WarehouseOrderGroup\MisGroupPusher;
use Vemid\Service\UserNotification\Message;
use Vemid\Service\WarehouseOrders\WarehouseGroupProducts;
use Vemid\Task\Queue;
use \Vemid\Messenger\Manager as MessageManager;
use Vemid\Translate\NativeArray;
use Vemid\Service\Product\PricelistPrices;
use \Vemid\CourierService\Manager as PostalManager;

/**
 * Class Di
 *
 * @package Vemid\Application
 *
 * @method PhalconAcl\AdapterInterface getAcl()
 * @method AclHelper getAclHelper()
 * @method Annotations\AdapterInterface getAnnotations()
 * @method Php|\stdClass getConfig()
 * @method Http\Response\CookiesInterface getCookies()
 * @method Crypt getCrypt()
 * @method Db\AdapterInterface getDb()
 * @method Db\AdapterInterface getDbEfectus()
 * @method Mvc\DispatcherInterface getDispatcher()
 * @method EntityManagerInterface getEntityManager()
 * @method Events\ManagerInterface getEventsManager()
 * @method HandlerInterface getErrorHandler()
 * @method Flash\Direct getFlash()
 * @method Flash\Session getFlashSession()
 * @method \Vemid\Service\Efectus\Manager getEfectusManager()
 * @method \Vemid\Service\Efectus\Notification\Mailer getEfectusMailer()
 * @method LoggerInterface getLogger()
 * @method LoggerInterface getCourierLogger()
 * @method \Swift_Mailer getMailer()
 * @method Manager getMailManager()
 * @method MessageManager getNotificationManager()
 * @method Mvc\Model\MetaDataInterface getModelsMetadata()
 * @method Mvc\Model\ManagerInterface getModelsManager()
 * @method Mvc\Router\RouteInterface getRouter()
 * @method Http\Request getRequest()
 * @method Http\Response getResponse()
 * @method Session\AdapterInterface getSession()
 * @method Mvc\View\Simple getSimpleView()
 * @method TransactionManagerInterface getTransactionManager()
 * @method BuilderInterface getPdfRenderer()
 * @method NativeArray getTranslator()
 * @method Mvc\Url getUrl()
 * @method PricelistPrices getProductPrices()
 * @method Mvc\View getView()
 * @method Mvc\View\Engine\Volt getVolt()
 * @method \User|null getCurrentUser()
 * @method Message getUserNotification()
 * @method MisPusher getMisDocumentPusher()
 * @method MisGroupPusher getMisDocumentGroupPusher()
 * @method PostalClientInterface getPostalClient()
 * @method CourierHandler getCourierHandler()
 * @method PostalManager getPostalManager()
 */
class Di extends FactoryDefault
{
    public function initialize()
    {
        $this->initAcl();
        $this->initAclHelper();
        $this->initAnnotations();
        $this->initBeanstalkQueue();
        $this->initConfig();
        $this->initCookies();
        $this->initCrypt();
        $this->initCurrentUser();
        $this->initDb();
        $this->initDbEfectus();
        $this->initDbInsight();
        $this->initDispatcher();
        $this->initEfectusManager();
        $this->initEfectusMailer();
        $this->initEntityManager();
        $this->initErrorHandler();
        $this->initFlash();
        $this->initFlashSession();
        $this->initLogger();
        $this->initCourierLogger();
        $this->initMailer();
        $this->initMailManager();
        $this->initModelsManager();
        $this->initModelsMetadata();
        $this->initNotificationManager();
        $this->initPdfRenderer();
        $this->initRouter();
        $this->initSession();
        $this->initSimpleView();
        $this->initTransactionManager();
        $this->initTranslator();
        $this->initUrl();
        $this->initProductPrices();
        $this->initUserNotification();
        $this->initView();
        $this->initVolt();
        $this->initMisDocumentPusher();
        $this->initMisDocumentGroupPusher();
        $this->initPostalClient();
        $this->initCourierHandler();
        $this->initPostalManager();
    }

    protected function initConfig()
    {
        $this->set('config', function () {
            /** @var Php|\stdClass $config */
            $config = new Php(APP_PATH . '/app/config/global.php');
            $config->merge(new Php(APP_PATH . '/app/config/local.php'));
            $environment = defined('ENVIRONMENT') ? ENVIRONMENT : $config->application->environment;

            if (file_exists($filename = APP_PATH . '/app/config/' . $environment . '.php')) {
                $config->merge(new Php($filename));
            }

            return $config;
        }, true);
    }

    public function initAcl()
    {
        $this->set('acl', function () {
            $acl = new Acl(new PhalconAcl\Adapter\Memory());
            $acl = $acl->getAcl();
            $acl->setEventsManager($this->getEventsManager());

            return $acl;
        });
    }

    public function initAclHelper()
    {
        $this->set('aclHelper', function () {
            return new AclHelper($this->getAcl(), $this->getCurrentUser());
        });
    }

    protected function initDispatcher()
    {
        $this->set('dispatcher', function () {
            $eventsManager = $this->getEventsManager();
            $eventsManager->attach('dispatch:beforeDispatchLoop',
                function (Events\Event $event, Mvc\Dispatcher $dispatcher) {
                    $actionName = Text::camelize($dispatcher->getActionName());
                    $dispatcher->setActionName(lcfirst($actionName));
                }
            );

            $dispatcher = new Mvc\Dispatcher();
            $dispatcher->setDI($this);
            $dispatcher->setEventsManager($eventsManager);

            return $dispatcher;
        }, true);
    }

    protected function initModelsManager()
    {
        $this->set('modelsManager', function () {
            $eventsManager = $this->getEventsManager();
            $metaDataInitializer = new Initializer();
            $metaDataInitializer->setDI($this);
            $eventsManager->attach('modelsManager', $metaDataInitializer);

            $modelsManager = new Mvc\Model\Manager();
            $modelsManager->setDI($this);
            $modelsManager->setEventsManager($eventsManager);
            Mvc\Model::setup(['exceptionOnFailedSave' => false]);

            return $modelsManager;
        }, true);
    }

    protected function initModelsMetadata()
    {
        $this->set('modelsMetadata', function () {
            $metaData = new Mvc\Model\MetaData\Memory([
                'metaDataDir' => APP_PATH . 'var/cache/metadata/'
            ]);
            $metaData->setDI($this);
            $metaData->setStrategy(new MetaDataInitializer());

            return $metaData;
        }, true);
    }

    protected function initLogger()
    {
        $this->set('logger', function () {
            return new LogToFile(APP_PATH . 'var/logs/import.log');
        }, true);
    }

    protected function initCourierLogger()
    {
        $this->set('courierLogger', function () {
            $logFile = APP_PATH . 'var/logs/courier.log';

            if (!file_exists($logFile)) {
                touch($logFile);
                chmod($logFile, 0777);
            }

            return new LogToFile($logFile);
        }, true);
    }

    protected function initAnnotations()
    {
        $this->set('annotations', function () {
            return new Annotations\Adapter\Memory([
                'annotationsDir' => APP_PATH . 'var/cache/annotations/'
            ]);
        }, true);
    }

    protected function initDb()
    {
        $this->set('db', function () {
            $config = $this->getConfig()->toArray();
            $adapter = $config['database']['adapter'] ?? Db\Adapter\Pdo\Mysql::class;
            unset($config['database']['adapter']);

            return new $adapter($config['database']);
        }, true);
    }

    protected function initDbEfectus()
    {
        $this->set('dbEfectus', function () {
            $config = $this->getConfig()->toArray();
            $adapter = $config['dbEfectus']['adapter'] ?? Db\Adapter\Pdo\Mysql::class;
            unset($config['dbEfectus']['adapter']);

            return new $adapter($config['dbEfectus']);
        }, true);
    }

    protected function initDbInsight()
    {
        $this->set('dbInsight', function () {
            $config = $this->getConfig()->toArray();
            return new Db\Adapter\Pdo\Mysql($config['insight']);
        }, true);
    }

    protected function initView()
    {
        $this->set('view', function () {
            $view = new Mvc\View();
            $view->setDI($this);
            $view->setViewsDir(APP_PATH . 'app/modules/default/views/');
            $view->registerEngines([
                '.volt' => 'volt',
            ]);

            return $view;
        }, true);
    }

    protected function initSimpleView()
    {
        $this->set('simpleView', function () {
            $simpleView = new Mvc\View\Simple();
            $simpleView->setDI($this);
            $simpleView->setViewsDir(APP_PATH . 'app/modules/default/views/');
            $simpleView->registerEngines([
                '.volt' => function ($view, $di) {
                    $volt = new Mvc\View\Engine\Volt($view, $di);
                    $volt->setDI($this);
                    $volt->setOptions(array(
                        'compiledPath' => APP_PATH . 'var/cache/volt/',
                        'compileAlways' => false
                    ));

                    $compiler = $volt->getCompiler();
                    $compiler->addFunction('number_format', 'number_format');
                    $compiler->addFunction('file_get_contents', 'file_get_contents');
                    $compiler->addFunction('substr', 'substr');
                    $compiler->addFunction('in_array', 'in_array');
                    $compiler->addFunction('ceil', 'ceil');
                    $compiler->addFunction('floor', 'floor');
                    $compiler->addFunction('implode', 'implode');
                    $compiler->addFunction('array_values', 'array_values');
                    $compiler->addFunction('asort', 'asort');
                    $compiler->addFunction('json_decode', 'json_decode');
                    $compiler->addFunction('array_key_exists', 'array_key_exists');
                    $compiler->addFunction('base64_encode', 'base64_encode');
                    $compiler->addFunction('explode', 'explode');
                    $compiler->addFunction('list', 'list');
                    $compiler->addFunction('t', function ($resolvedArgs, $exprArgs) use ($compiler) {
                        $firstArgument = $compiler->expression($exprArgs[0]['expr']);
                        $secondArgument = 'null';
                        if (isset($exprArgs[1])) {
                            $secondArgument = $compiler->expression($exprArgs[1]['expr']);
                        }

                        return "\$this->translator->t($firstArgument, $secondArgument)";
                    });

                    return $volt;
                }
            ]);

            return $simpleView;
        }, true);
    }

    protected function initVolt()
    {
        $this->set('volt', function ($view, $di) {
            $config = $this->getConfig();
            $volt = new Mvc\View\Engine\Volt($view, $di);
            $volt->setOptions([
                'compiledPath' => $config->application->cacheDir . 'volt/',
                'compileAlways' => false,
                'compiledSeparator' => '_'
            ]);

            $compiler = $volt->getCompiler();
            $compiler->addFunction('number_format', 'number_format');
            $compiler->addFunction('file_get_contents', 'file_get_contents');
            $compiler->addFunction('substr', 'substr');
            $compiler->addFunction('in_array', 'in_array');
            $compiler->addFunction('ceil', 'ceil');
            $compiler->addFunction('floor', 'floor');
            $compiler->addFunction('implode', 'implode');
            $compiler->addFunction('array_values', 'array_values');
            $compiler->addFunction('asort', 'asort');
            $compiler->addFunction('json_decode', 'json_decode');
            $compiler->addFunction('array_key_exists', 'array_key_exists');
            $compiler->addFunction('base64_encode', 'base64_encode');
            $compiler->addFunction('explode', 'explode');
            $compiler->addFunction('list', 'list');

            $compiler->addFunction('t', function ($resolvedArgs, $exprArgs) use ($compiler) {
                $firstArgument = $compiler->expression($exprArgs[0]['expr']);
                $secondArgument = 'null';
                if (isset($exprArgs[1])) {
                    $secondArgument = $compiler->expression($exprArgs[1]['expr']);
                }

                return "\$this->translator->t($firstArgument, $secondArgument)";
            });

            $aclHelper = $this->getAclHelper();

            $compiler->addFunction('isAllowed', function ($resolvedArgs, $exprArgs) use ($aclHelper, $compiler) {
                $firstArgument = $compiler->expression($exprArgs[0]['expr']);
                $secondArgument = $compiler->expression($exprArgs[1]['expr']);

                return "'1 == ".$aclHelper->isAllowed($firstArgument, $secondArgument) . "'";
            });

            return $volt;
        }, true);
    }

    protected function initSession()
    {
        $this->set('session', function () {
            $session = new Session\Adapter\Files(['uniqueId' => 'crm']);
            $session->start();

            return $session;
        }, true);
    }

    protected function initFlashSession()
    {
        $this->set('flashSession', function () {
            return new Flash\Session([
                'error' => 'alert alert-danger',
                'success' => 'alert alert-success',
                'notice' => 'alert alert-info',
                'warning' => 'alert alert-warning',
            ]);
        }, true);
    }

    protected function initFlash()
    {
        $this->set('flash', function () {
            return new Flash\Direct([
                'error' => 'alert alert-danger',
                'success' => 'alert alert-success',
                'notice' => 'alert alert-info',
                'warning' => 'alert alert-warning',
            ]);
        }, true);
    }

    protected function initMailer()
    {
        $this->set('mailer', function () {
            return $this->getMailManager()->getMailer();
        }, true);
    }

    protected function initCookies()
    {
        $this->set('cookies', function () {
            $cookies = new Http\Response\Cookies();
            $cookies->setDI($this);
            $cookies->useEncryption(true);

            return $cookies;
        }, true);
    }

    protected function initCrypt()
    {
        $this->set('crypt', function () {
            $config = $this->getConfig();
            $crypt = new Crypt();
            $crypt->setKey($config->salts->crypt);

            return $crypt;
        }, true);
    }

    protected function initUrl()
    {
        $this->set('url', function () {
            $url = new Mvc\Url();
            $url->setBaseUri('/');
            return $url;
        }, true);
    }

    protected function initProductPrices()
    {
        $this->set('productPrices', function () {
            return new PricelistPrices($this->getEntityManager());
        }, true);
    }

    protected function initTransactionManager()
    {
        $this->set('transactionManager', function () {
            return new TransactionManager();
        }, true);
    }

    protected function initTranslator()
    {
        $this->set('translator', function () {
            $options = [];

            $session = $this->getSession();

            $locale = 'sr_RS';
            if ($session->has('currentUser')) {
                $currentUer = $session->get('currentUser');
                if ($currentUer['language'] === 'en') {
                    $locale = 'en_GB';
                }
            }

            $filePath = sprintf('%sapp/locale/%s/translations.php',
                APP_PATH, $locale
            );

            if (file_exists($filePath)) {
                $options = (new Php($filePath))->toArray();
            }

            return new NativeArray($options);
        }, true);
    }

    protected function initBeanstalkQueue()
    {
        $this->set('beanstalkQueue', function () {
            $config = $this->getConfig();
            $queue = new Queue([
                'host' => $config->beanstalk->host,
                'port' => $config->beanstalk->port
            ]);
            $queue->setTube($config->beanstalk->tube);

            return $queue;
        }, true);
    }

    protected function initMailManager()
    {
        $this->set('mailManager', function () {
            $config = $this->getConfig()->toArray();
            $mailManager = new Manager($config['mailer']);

            return $mailManager;
        }, true);
    }

    protected function initNotificationManager()
    {
        $this->set('notificationManager', function () {
            return new MessageManager();
        }, true);
    }

    protected function initCurrentUser()
    {
        $this->set('currentUser', function () {
            $session = $this->getSession();
            if (!$session->has('currentUser')) {
                return null;
            }

            $sessionUserData = $session->get('currentUser');
            if (empty($sessionUserData)) {
                return null;
            }

            $user = $this->getEntityManager()->findOne(\User::class, $sessionUserData['id']);

            return $user ?: null;
        }, true);
    }

    protected function initEntityManager()
    {
        $this->set('entityManager', function () {
            return new EntityManager(
                $this->getModelsManager()
            );
        }, true);
    }

    protected function initUserNotification()
    {
        $this->set('userNotification', function () {
            return new Message(
                $this->getEntityManager()
            );
        }, true);
    }

    protected function initPdfRenderer()
    {
        $this->set('pdfRenderer', function () {
            //return new Builder(new TcPdf());
            return new Builder();
        }, true);
    }

    protected function initMisDocumentPusher()
    {
        $this->set('misDocumentPusher', function () {
            return new MisPusher(
                $this->getEntityManager(),
                $this->getProductPrices(),
                $this->getConfig()
            );
        }, true);
    }

    protected function initPostalClient()
    {
        $this->set('postalClient', function () {

            $config = $this->getConfig();
            $client = new Client([
                'base_uri' => $config->postalService->baseUrl,
                'auth' => [$config->postalService->username, $config->postalService->password]
            ]);

            return new \Vemid\CourierService\Http\Client($client);
        }, true);
    }

    protected function initCourierHandler()
    {
        $this->set('courierHandler', function () {
            $client = $this->getPostalClient();

            return new CourierHandler($client);
        }, true);
    }

    protected function initPostalManager()
    {
        $this->set('postalManager', function () {
            return new PostalManager($this->getEntityManager(), $this->getConfig(), $this->getCourierHandler());
        }, true);
    }

    public function initEfectusManager()
    {
        $this->set('efectusManager', function () {
            return new \Vemid\Service\Efectus\Manager(
                $this->getEntityManager(),
                new Repository(
                    new Connection($this->getConfig())
                ),
                $this->getMailManager(),
                $this->getConfig()
            );
        }, true);
    }

    public function initEfectusMailer()
    {
        $this->set('efectusMailer', function () {
            return new \Vemid\Service\Efectus\Notification\Mailer(
                $this->getMailManager(),
                $this->getEntityManager(),
                $this->getConfig()
            );
        }, true);
    }

    protected function initMisDocumentGroupPusher()
    {
        $this->set('misDocumentGroupPusher', function () {
            return new MisGroupPusher(
                (new WarehouseGroupProducts($this->getModelsManager())),
                $this->getConfig(),
                $this->getEntityManager()
            );
        }, true);
    }


    protected function initRouter()
    {
        $this->set('router', function () {
            $router = new Router();
            $router->setDI($this);
            $router->initialize();

            return $router;
        }, true);
    }

    protected function initErrorHandler()
    {
        $this->set('errorHandler', function () {
            $config = $this->getConfig();
            $errorHandler = new Handler();

            if ($config->raven->dsn) {
                $errorHandler->addLogger(
                    new \Vemid\Error\Logger\SentryLogger(
                        new \Raven_ErrorHandler(
                            new \Raven_Client($config->raven->dsn)
                        )
                    )
                );
            }

            $errorHandler->addLogger(
                new FileLogger(
                    new Logger\Adapter\File(APP_PATH . 'var/logs/error.log')
                )
            );

            $errorHandler->registerErrorHandler();
            $errorHandler->registerExceptionHandler();
            $errorHandler->registerShutdownFunction();

            return $errorHandler;
        }, true);
    }
}
