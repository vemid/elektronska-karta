<?php

namespace Vemid\Application;

/**
 * Class Application
 *
 * @package Vemid\Application
 *
 * @method Di getDI()
 */
abstract class Application extends \Phalcon\Mvc\Application
{
    const ENV_PRODUCTION = 'production';
    const ENV_STAGING = 'staging';
    const ENV_TEST = 'test';
    const ENV_DEVELOPMENT = 'development';
    const DEFAULT_TIMEZONE = 'Europe/Belgrade';

    protected function initEnvironment()
    {
        date_default_timezone_set(self::DEFAULT_TIMEZONE);

        $config = $this->getDI()->getConfig();

        defined('ENVIRONMENT') || define('ENVIRONMENT', $config->application->environment);

        $this->getDI()->getErrorHandler()->initEnvironment(ENVIRONMENT);
    }

    protected function initDi()
    {
        $di = new Di();
        $di->initialize();
        $this->setDI($di);
    }

    protected function initLoader()
    {
        $loader = new Loader();
        $loader->initialize();
    }
}
