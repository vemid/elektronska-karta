<?php

namespace Vemid\Application;

/**
 * Class Router
 *
 * @package Vemid\Application
 */
class Router extends \Phalcon\Mvc\Router
{
    public function initialize()
    {
        $this->setUriSource(self::URI_SOURCE_SERVER_REQUEST_URI);
        $this->removeExtraSlashes(true);
        $this->setDefaultModule('default');
        $this->setDefaultController('index');
        $this->setDefaultAction('index');
        $this->initRoutes();
    }

    protected function initRoutes()
    {

        $this->add('/:module/:controller/:action/:params', [
            'module' => 1,
            'controller' => 2,
            'action' => 3,
            'params' => 4,
        ]);

        $this->add('/:controller/:action/:params', [
            'module' => 'default',
            'controller' => 1,
            'action' => 2,
            'params' => 3,
        ]);

        $this->add('/admin/:controller/:action/:params', [
            'module' => 'backend',
            'controller' => 1,
            'action' => 2,
            'params' => 3,
        ]);

        $this->add('/admin/:controller', [
            'module' => 'backend',
            'controller' => 1,
            'action' => 'index',
        ]);

        $this->add('/api/:controller/:action/:params', [
            'module' => 'api',
            'controller' => 1,
            'action' => 2,
            'params' => 3
        ]);
    }
}
