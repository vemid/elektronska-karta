<?php

namespace Vemid\Application;

/**
 * Class Loader
 *
 * @package Vemid\Application
 */
class Loader extends \Phalcon\Loader
{
    public function initialize()
    {
        $this->initNamespaces();
        $this->initDirectories();
    }

    protected function initNamespaces()
    {
        $this->registerNamespaces([]);
    }

    protected function initDirectories()
    {
        $this->registerDirs([
            APP_PATH . '/app/models/',
            APP_PATH . '/tools/',
        ])->register();
    }
}
