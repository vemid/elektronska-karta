<?php

namespace Vemid\Application;

use Api\Module;

/**
 * Class Web
 *
 * @package Vemid\Application
 */
final class Web extends Application
{
    public function initialize()
    {
        $this->initLoader();
        $this->initDi();
        $this->initEnvironment();
        $this->initRouter();
        $this->initModules();
    }

    protected function initRouter()
    {
        $this->getDI()->set('router', function () {
            $router = new Router();
            $router->initialize();

            return $router;
        }, true);
    }

    protected function initModules()
    {
        $this->registerModules([
            'default' => [
                'className' => \Module::class,
                'path' => APP_PATH . '/app/modules/default/Module.php'
            ],
            'api' => [
                'className' => Module::class,
                'path' => APP_PATH . '/app/modules/api/Module.php'
            ],
        ]);

        $this->setDefaultModule('default');
    }
}
