<?php

namespace Vemid\Security;

use Vemid\Filter\Underscore;
use Phalcon\Acl as PhalconAcl;
use Phalcon\Acl\Resource;
use Phalcon\Acl\Adapter\Memory as AclAdapter;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;

/**
 * Security
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 *
 * @package Vemid\Security
 * @author Vemid
 */
class Acl extends Plugin
{
    /** @var PhalconAcl\AdapterInterface */
    private $adapter;

    /**
     * @param PhalconAcl\AdapterInterface $adapter
     */
    public function __construct(PhalconAcl\AdapterInterface $adapter)
    {
        $this->loadAcl($adapter);
    }

    /**
     * @return PhalconAcl\AdapterInterface|PhalconAcl\Adapter
     */
    public function getAcl()
    {
        return $this->adapter;
    }

    /**
     * @param PhalconAcl\AdapterInterface $adapter
     */
    private function loadAcl(PhalconAcl\AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        $this->adapter->setDefaultAction(PhalconAcl::DENY);
        $this->addRoles();
        $this->addResources();
    }

    private function addRoles()
    {
        $this->adapter->addRole(\Role::ROLE_GUEST);
        $this->adapter->addRole(\Role::ROLE_MEMBER, \Role::ROLE_GUEST);
        $this->adapter->addRole(\Role::ROLE_USER, \Role::ROLE_MEMBER);
        $this->adapter->addRole(\Role::ROLE_ADMIN);
        $this->adapter->addRole(\Role::ROLE_CLIENT);
        $this->adapter->addRole(\Role::ROLE_SUPER_ADMIN);
        $this->adapter->addRole(\Role::ROLE_SHOP);
        $this->adapter->addRole(\Role::ROLE_FACTORY);
        $this->adapter->addRole(\Role::ROLE_CENTRAL);
    }

    private function addResources()
    {
        $rules = $this->getRules();
        foreach ($rules as $role => $roleRules) {
            foreach ($roleRules as $accessRule => $roleResources) {
                if (is_array($roleResources)) {
                    foreach ($roleResources as $resource => $accessList) {
                        $this->adapter->addResource(
                            new Resource($resource),
                            $accessList
                        );
                        if ($accessRule === PhalconAcl::ALLOW) {
                            $this->adapter->allow($role, $resource, $accessList);
                        } else {
                            $this->adapter->deny($role, $resource, $accessList);
                        }
                    }
                } else {
                    if ($accessRule === PhalconAcl::ALLOW) {
                        $this->adapter->allow($role, $roleResources, $roleResources);
                    } else {
                        $this->adapter->deny($role, $roleResources, $roleResources);
                    }
                }
            }
        }
    }

    /**
     * @param null $role
     * @return array
     */
    private function getRules($role = null)
    {
        $rules = [
            \Role::ROLE_GUEST => [
                PhalconAcl::ALLOW => [
                    'default/auth' => ['login', 'authy', 'authy-sms','logout'],
                    'default/errors' => '*',
                ]
            ],

            \Role::ROLE_SUPER_ADMIN => [
                PhalconAcl::ALLOW => '*'
            ],

            \Role::ROLE_ADMIN => [
                PhalconAcl::ALLOW => [
                    'default/users' => ['list'],
                    'default/electron-cards' => '*',
                    'default/electron-card-factories' => '*',
                    'default/products' => '*',
                    'default/orders' => '*',
                    'default/material-samples' => '*',
                    'default/material-factories' => '*',
                    'default/index' => '*',
                    'default/suppliers' => '*',
                    'default/questionnaires' => '*',
                    'default/questions' => '*',
                    'default/errors' => '*',
                    'default/shops' => '*',
                ]
            ],
            \Role::ROLE_CLIENT => [
                PhalconAcl::ALLOW => [
                    'default/errors' => '*',
                    'default/orders' => '*',
                    'default/invoices' => '*',
                    'default/order-totals' => '*',
                    'default/order-items' => '*',
                    'default/index' => '*',
                    'default/products' => ['list','get-list-data','overview'],
                ]
            ],

            \Role::ROLE_SHOP => [
                PhalconAcl::ALLOW => [
                    'default/errors' => '*',
                    'default/shops' => '*',
                    'default/index' => '*',
                    'default/dealerships' => '*',
                    'default/warehouse-orders' => ['delivered-items','store-delivered-items','find-by-barcode'],

                ]
            ],
            \Role::ROLE_FACTORY =>[
                PhalconAcl::ALLOW => [
                    'default/index' => '*',
                    'default/operating-list' => '*',
                    'default/product-calculations' => '*',
                    'default/production-workers' => ['create','list','delete','get-create-form','get-update-form','overview','update','delete','list-by-worker','list-by-worker-data'],
                    'default/product-calculation-maps' => '*',
                    'default/absence-works' => '*',
                    'default/operating-lists' => '*',
                    'default/operating-list-items' => ['create','print','find-operating-item','find-all-types','get-base-products','list-prices','get-create-form','delete','edit','update','get-update-form','overview'],
                    'default/operating-list-item-types' => '*',
                    'default/operating-list-item-worksheets' => ['get-update-form','update'],
                    'default/reports' => ['payment-report-by-worker','payment-report-by-worker-data','payment-report-by-sector','payment-report-by-sector-data'],
                    'default/cutting-jobs' => '*',
                    'default/production-machines' => '*',
                    'default/products' => '*',
                    'default/electron-cards' => '*',
                ]
            ],

            \Role::ROLE_MEMBER => [
                PhalconAcl::ALLOW => []
            ]
        ];

        if ($role === null) {
            return $rules;
        }

        if (array_key_exists($role, $rules)) {
            return [$role => $rules[$role]];
        }

        return [];
    }
}
