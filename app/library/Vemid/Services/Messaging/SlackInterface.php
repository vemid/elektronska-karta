<?php

namespace Vemid\Services\Messaging;

/**
 * Interface SlackInterface
 *
 * @package Vemid\Service\Messaging
 */
interface SlackInterface
{
    /**
     * @param string $message
     * @param string $channel
     * @param string $username
     * @param string $icon
     * @param array|null $attachments
     * @return bool
     * @throws \RuntimeException
     */
    public function sendMessage($message, $channel, $username, $icon, array $attachments = null);
}
