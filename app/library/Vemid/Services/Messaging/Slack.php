<?php

namespace Vemid\Services\Messaging;

/**
 * Class Slack
 *
 * @package Vemid\Services\Messaging
 */
class Slack implements SlackInterface
{
    /**
     * {@inheritdoc}
     */
    public function sendMessage($message, $channel = '@slackbot', $username = 'monkey-bot', $icon = ':monkey_face:', array $attachments = null)
    {
        $payload = [
            'channel' => $channel,
            'username' => $username,
            'text' => str_replace('&', '&amp;', $message),
            'icon_emoji' => $icon
        ];

        if ($attachments !== null) {
            $payload['attachments'] = $attachments;
        }

        $data = 'payload=' . urlencode(json_encode($payload));

        $result = $this->sendRequest('', []);

        if ($result === 'ok') {
            return true;
        }

        if ($result === 'Invalid channel specified' || $result === 'channel_not_found') {
            return false;
        }

        throw new \RuntimeException('Not able to post Slack message: ' . $result . ' (Payload: ' . $data . ')');
    }

    /**
     * {@inheritdoc}
     */
    public function sendMessageUsingApi($message, $channel = '@slackbot', $username = 'monkey-bot', $icon = ':monkey_face:', array $attachments = null)
    {
        $payload = [
            'token'=>'',
            'channel' => $channel,
            'username' => $username,
            'text' => str_replace('&', '&amp;', $message),
            'icon_emoji' => $icon
        ];

        if ($attachments !== null) {
            $payload['attachments'] = $attachments;
        }

        $result = $this->sendRequest('https://slack.com/api/chat.postMessage', $payload);

        if (isset($result['ts'])) {
            return ['channel' => $result['channel'], 'ts' => $result['ts']];
        }

        throw new \RuntimeException('Not able to post Slack message: ' . $result . ' (Payload: ' . $data . ')');
    }


    public function addReaction($channel, $messageTimestamp, $emojiName = 'thumbsup')
    {
        $payload = [
            'token' => '',
            'channel' => $channel,
            'timestamp' => $messageTimestamp,
            'name' => $emojiName
        ];

        $result = $this->sendRequest('https://slack.com/api/reactions.add', $payload);

        return (isset($result['ok']) && $result['ok'] === true);
    }

    public function updateMessage($channel, $messageTimestamp, $newText = null, $newAttachments = null)
    {
        $payload = [
            'token' => '',
            'channel' => $channel,
            'ts' => $messageTimestamp,
            'parse'=>'none',
        ];

        if ($newText) {
            $payload['text'] = $newText;
        }
        if ($newAttachments) {
            $payload['attachments'] = $newAttachments;
        }

        $result = $this->sendRequest('https://slack.com/api/chat.update', $payload);

        return (isset($result['ok']) && $result['ok'] === true);
    }

    public function sendMessageWithFile($filePath, $title = '', $channel = '@slackbot', $icon = ':monkey_face:')
    {
        $file = new \CURLFile($filePath, mime_content_type($filePath));

        $payload = [
            'token'=>'xoxp-4108672956-4108673088-286395986983-80d2ffeeae6c5d04d455cbb6b839757a',
            'channels' => $channel,
            'content' => 'hello',
            'file' => $file,
            'filename' => basename($filePath),
            'filetype' => 'png',
            'icon_emoji' => $icon,
            'title' => $title
        ];

        $result = $this->sendRequest('https://slack.com/api/files.upload', $payload);

        if (isset($result['ok'])) {
            return ['file' => $result['namme']];
        }

        throw new \RuntimeException('Not able to post Slack message: ' . $result . ' (Payload: ' . $data . ')');
    }

    /**
     * @param $url
     * @param array $data
     * @return array
     */
    private function sendRequest($url, array $data) : array
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        curl_close($ch);

        return json_decode($result, true);
    }
}
