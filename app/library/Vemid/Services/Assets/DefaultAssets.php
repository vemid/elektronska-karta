<?php

namespace Vemid\Services\Assets;

use \Phalcon\Assets\Manager as Assets;
use Phalcon\Assets\Filters\Cssmin;
use Phalcon\Assets\Filters\Jsmin;

class DefaultAssets
{
    const ASSETS_HEAD_CSS = 'headCss';
    const ASSETS_HEAD_JS = 'headJs';
    const ASSETS_FOOT_CSS = 'footCss';
    const ASSETS_FOOT_JS = 'footJs';
    const ASSETS_CUSTOM_CSS = 'customCss';
    const ASSETS_CUSTOM_JS = 'customJs';

    /** @var Assets */
    private $assets;

    public function __construct(Assets $assets)
    {
        $this->assets = $assets;
    }

    public function initialize()
    {
        $this->assets->collection(self::ASSETS_HEAD_CSS)
            ->setTargetPath(APP_PATH . 'public/assets/head.css')
            ->setTargetUri('/assets/head.css')
            ->join(true)
            ->addFilter(new Cssmin());

        $this->assets->collection(self::ASSETS_HEAD_JS)
            ->setTargetPath(APP_PATH . 'public/assets/head.js')
            ->setTargetUri('/assets/head.js')
            ->join(true)
            ->addFilter(new Jsmin());

        $this->assets->collection(self::ASSETS_FOOT_CSS)
            ->setTargetPath(APP_PATH . 'public/assets/foot.css')
            ->setTargetUri('/assets/foot.css')
            ->join(true)
            ->addFilter(new Cssmin());

        $this->assets->collection(self::ASSETS_FOOT_JS)
            ->setTargetPath(APP_PATH . 'public/assets/foot.js')
            ->setTargetUri('/assets/foot.js')
            ->join(true)
            ->addFilter(new Jsmin());

        $this->assets->collection(self::ASSETS_CUSTOM_CSS)
            ->setTargetPath(APP_PATH . 'public/assets/custom.css')
            ->setTargetUri('/assets/custom.css')
            ->join(true)
            ->addFilter(new Cssmin());

        $this->assets->collection(self::ASSETS_CUSTOM_JS)
            ->setTargetPath(APP_PATH . 'public/assets/custom.js')
            ->setTargetUri('/assets/custom.js')
            ->join(true)
            ->addFilter(new Jsmin());

        $this->assets->collection(self::ASSETS_HEAD_CSS)
            ->addCss(APP_PATH . 'assets/css/jquery-ui.min.css')
            ->addCss(APP_PATH . 'assets/css/bootstrap.css')
            ->addCss(APP_PATH . 'assets/css/font-awesome.css')
            ->addCss(APP_PATH . 'assets/css/bootstrap-datetimepicker.min.css')
            ->addCss(APP_PATH . 'assets/css/date-picker.css')
            ->addCss(APP_PATH . 'assets/css/jqueryui-editable.css')
            ->addCss(APP_PATH . 'assets/css/flatpickr.min.css')
            ->addCss(APP_PATH . 'assets/css/summer-note.css')
            ->addCss(APP_PATH . 'assets/css/summer-note-bs3.css')
            ->addCss(APP_PATH . 'assets/css/sweetalert.css')
            ->addCss(APP_PATH . 'assets/css/data-tables.bootstrap.css')
            ->addCss(APP_PATH . 'assets/css/data-tables.responsive.css')
            ->addCss(APP_PATH . 'assets/css/data-tables.table-tools.min.css')
            ->addCss(APP_PATH . 'assets/css/switchery.css')
            ->addCss(APP_PATH . 'assets/css/chosen.css')
            ->addCss(APP_PATH . 'assets/css/toastr.min.css')
            ->addCss(APP_PATH . 'assets/css/cropper.min.css')
            ->addCss(APP_PATH . 'assets/css/icheck.css')
            ->addCss(APP_PATH . 'assets/css/touchspin.min.css')
            ->addCss(APP_PATH . 'assets/css/animate.css')
            ->addCss(APP_PATH . 'assets/css/star-rating-svg.css')
            ->addCss(APP_PATH . 'assets/css/basicLightbox.min.css')
            ->addCss(APP_PATH . 'assets/css/datatable-fixedheader.css')
            ->addCss(APP_PATH . 'assets/css/froala-style.min.css')
            ->addCss(APP_PATH . 'assets/css/froala-editor.min.css')
            ->addCss(APP_PATH . 'assets/css/webdatarock/webdatarocks.min.css')
            ->addCss(APP_PATH . 'assets/css/data-tables.buttons.min.css')
            ->addCss(APP_PATH . 'assets/css/magiczoomplus.css');;

        $this->assets->collection(self::ASSETS_HEAD_JS)
            ->addJs(APP_PATH . 'assets/js/jquery.min.js')
            ->addJs(APP_PATH . 'assets/js/jquery.ui.min.js')
            ->addJs(APP_PATH . 'assets/js/bootstrap.min.js')
            ->addJs(APP_PATH . 'assets/js/data-tables/jquery.dataTables.js')
            ->addJs(APP_PATH . 'assets/js/vemid/Chart.bundle.js')
            //->addJs(APP_PATH . 'assets/js/basicLightbox.min.js')
            ->addJs(APP_PATH . 'assets/js/data-tables/dataTables.rowsGroup.js')
            ->addJs(APP_PATH . 'assets/js/data-tables/dataTables.bootstrap.js')
            ->addJs(APP_PATH . 'assets/js/data-tables/dataTables.responsive.js')
            ->addJs(APP_PATH . 'assets/js/data-tables/dataTables.tableTools.min.js')
            ->addJs(APP_PATH . 'assets/js/data-tables/dataTables.buttons.min.js')
            ->addJs(APP_PATH . 'assets/js/webdatarock/webdatarocks.js')
            ->addJs(APP_PATH . 'assets/js/webdatarock/webdatarocks.toolbar.min.js')
            ->addJs(APP_PATH . 'assets/js/webdatarock/webdatarocks.toolbar.js')
            ->addJs(APP_PATH . 'assets/js/vemid/chosen-ajax.min.js')
            ->addJs(APP_PATH . 'assets/js/chosen.js')
            ->addJs(APP_PATH . 'assets/js/magiczoomplus.js')
            ->addJs(APP_PATH . 'assets/js/jeditable.js');

        $this->assets->collection(self::ASSETS_FOOT_JS)
            ->addJs(APP_PATH . 'assets/js/toastr.min.js')
            ->addJs(APP_PATH . 'assets/js/switchery.js')
            ->addJs(APP_PATH . 'assets/js/pace.min.js')
            ->addJs(APP_PATH . 'assets/js/flatpickr.js')
            ->addJs(APP_PATH . 'assets/js/metis-menu.js')
            ->addJs(APP_PATH . 'assets/js/slim-scroll.min.js')
            ->addJs(APP_PATH . 'assets/js/summer-note.min.js')
            ->addJs(APP_PATH . 'assets/js/cropper.min.js')
            ->addJs(APP_PATH . 'assets/js/moment.min.js')
            ->addJs(APP_PATH . 'assets/js/sweet-alert.min.js')
            ->addJs(APP_PATH . 'assets/js/icheck.min.js')
            ->addJs(APP_PATH . 'assets/js/touchspin.min.js')
            ->addJs(APP_PATH . 'assets/js/data-tables/dataTables.html5.buttons.min.js')
            ->addJs(APP_PATH . 'assets/js/data-tables/dataTables.buttons.print.min.js')
//            ->addJs(APP_PATH . 'assets/js/data-tables/jszip.min.js')
            ->addJs(APP_PATH . 'assets/js/data-tables/dataTables.fixedHeader.js')
            ->addJs(APP_PATH . 'assets/js/basicLightbox.min.js')
            ->addJs(APP_PATH . 'assets/js/date-picker.js')
            ->addJs(APP_PATH . 'assets/js/content-tool.min.js')
            ->addJs(APP_PATH . 'assets/js/froala-editor.min.js')
            ->addJs(APP_PATH . 'assets/js/froala/align.min.js')
            ->addJs(APP_PATH . 'assets/js/froala/align.min.js')
            ->addJs(APP_PATH . 'assets/js/froala/code-view.min.js')
            ->addJs(APP_PATH . 'assets/js/froala/code-beautifier.min.js')
            ->addJs(APP_PATH . 'assets/js/froala/paragraph-format.min.js')

            ->addJs(APP_PATH . 'assets/js/vemid/bootstrap.js');
    }

    public function afterExecuteRoute()
    {
        $this->assets->collection(self::ASSETS_CUSTOM_CSS)
            ->addCss(APP_PATH . 'assets/css/style.css');
    }

}