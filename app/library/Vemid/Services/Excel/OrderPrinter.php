<?php

namespace Vemid\Services\Excel;

use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Date\Time;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Task\Excel\RenderOrderTask;

class OrderPrinter
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ManagerInterface */
    private $modelsManager;

    /** @var \Order */
    private $order;

    /**
     * OrderPrinter constructor.
     * @param EntityManagerInterface $entityManager
     * @param ManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $entityManager, ManagerInterface $manager, \Order $order)
    {
        $this->entityManager = $entityManager;
        $this->modelsManager = $manager;
        $this->order = $order;
    }
    
    public function printFile(array $oldCollections)
    {
        /** @var \CodeType $codeType */
        $codeType = $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' =>\CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        /** @var \CodeType $codeTypeShops */
        $codeTypeShops = $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' =>\CodeType::SHOPS
            ]
        ]);

        if (!$codeType || !$codeTypeShops) {
            return null;
        }

        /** @var \Code $ageCode */
        $ageCode = $this->entityManager->findOne(\Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '02',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        /** @var \Code $oldCollectionCode */
        $oldCollectionCode = $this->entityManager->findOne(\Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '07',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if (!$ageCode || !$oldCollectionCode) {
            return null;
        }

        /** @var \Classification[] $ageCollections */
        $ageCollections = $this->entityManager->find(\Classification::class, [
            \Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $ageCode->getId()
            ]
        ]);

        $ages = [];
        foreach ($ageCollections as $ageCollection) {
            $ages[] = $ageCollection->getCode();
        }

        /** @var \Code[] $shops */
        $shops = $this->entityManager->find(\Code::class, [
            'codeTypeId = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeTypeShops->getId()
            ],
            'order' => 'code'
        ]);

        $tempFolder = rtrim(sys_get_temp_dir(), '/') . '/order-products';
        if (!is_dir($tempFolder) && !mkdir($tempFolder, 0700)) {
            throw new \LogicException('Dir can not be created!');
        }

        /** @var \Client[] $clients */
        $clients = $this->entityManager->find(\Client::class,[
            'order' => 'name'
        ]);

        $typeCollection = Type::CLASSIFICATION;
        $oldClassifications = implode('\',\'', $oldCollections);

        $sql2 = <<<SQL
select c2.* from
(select distinct p.id,p.code from order_items oi
left join product_sizes ps on ps.id = oi.product_size_id
left join products p  on p.id = ps.product_id
left join product_classifications pc on pc.product_id = p.id 
left join classifications c on c.id = pc.entity_id and classification_type_code_id = {$oldCollectionCode->getId()}
where pc.entity_type_id = {$typeCollection} and oi.order_id = {$this->order->getId()} and c.code in ('{$oldClassifications}')) as A
left join product_classifications pc2 on pc2.product_id = A.id
left join classifications c2 on c2.id = pc2.entity_id and classification_type_code_id = {$ageCode->getId()}
group by c2.id
SQL;

        $ageClassifications = $this->modelsManager->getReadConnection(new \Classification())->fetchAll($sql2);

        $task = new RenderOrderTask();
        $task->order = $this->order;
        $task->classifications = $ageClassifications;
        $task->oldCollections = $oldCollections;
        $task->shops = $shops->toArray();
        $task->clients = $clients->toArray();
        $task->name = 'Order - ' . $this->order->getId() . '.xlsx';
        $task->runInBackground();
    }
}