<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 5/21/19
 * Time: 12:42
 */

namespace Vemid\Services\Excel\Reports;

use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Date\DateTime;
use Vemid\Date\Time;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Task\Excel\PaymentReportBySectorAndWorkerTask;


class PaymentReportBySectorAndWorker
{
    /** @var \Code $sector */
    private $sector;

    /** @var DateTime $dateFrom */
    private $dateFrom;

    /** @var DateTime $dateTo */
    private $dateTo;


    /**
     * PaymentReportBySectorAndWorker constructor.
     * @param \Code $sector
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     */
    public function __construct(\Code $sector,DateTime $dateFrom,DateTime $dateTo)
    {
        $this->sector = $sector;
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    public function printFile()
    {
        $task = new PaymentReportBySectorAndWorkerTask();
        $task->sector = $this->sector;
        $task->dateFrom = $this->dateFrom;
        $task->dateTo = $this->dateTo;
        $task->runInBackground();

    }

}