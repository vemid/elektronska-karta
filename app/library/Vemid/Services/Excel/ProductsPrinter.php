<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 11/18/18
 * Time: 20:46
 */

namespace Vemid\Services\Excel;

use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Date\Time;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Task\Excel\ProductsExportTask;


class ProductsPrinter
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ManagerInterface */
    private $modelsManager;

    /** @var array $seasonCode */
    private $seasonCode;

    /** array $users */
    private $users;

    /**
     * ProductsPrinter constructor.
     * @param EntityManagerInterface $entityManager
     * @param ManagerInterface $manager
     * @param array $seasonCode
     * @param array $users
     */
    public function __construct(EntityManagerInterface $entityManager, ManagerInterface $manager, $seasonCode, $users)
    {
        $this->entityManager = $entityManager;
        $this->modelsManager = $manager;
        $this->seasonCode = $seasonCode;
        $this->users = $users;
    }

    public function printFile()
    {
        $tempFolder = rtrim(sys_get_temp_dir(), '/') . '/products-list';
        if (!is_dir($tempFolder) && !mkdir($tempFolder, 0700)) {
            throw new \LogicException('Dir can not be created!');
        }

        $query = $this->modelsManager->createBuilder()
            ->from(\User::class)
            ->where('id IN ({bindParam:array})',
                [
                    'bindParam' => $this->users
                ]);

        $users = $query->getQuery()->execute();

        $emails =array();

        /** @var \User $user */
        foreach ($users as $user) {
            $emails[$user->getEmail()] = $user->getDisplayName();
        }
        $task = new ProductsExportTask();
        $task->users = $emails;
        $task->seasonCode = $this->seasonCode;
        $task->runInBackground();
    }

}