<?php

namespace Vemid\Services\Converter;

/**
 * Interface PdfToImageInterface
 * @package Vemid\Services\Converter
 */
interface PdfToImageInterface
{
    /**
     * @param $path
     * @param $extension
     * @param null $width
     * @param null $height
     * @return string
     */
    public function convertToImage($path, $extension, $width = null, $height = null);
}