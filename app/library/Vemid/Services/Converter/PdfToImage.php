<?php

namespace Vemid\Services\Converter;

use Spatie\PdfToImage\Exceptions\PdfDoesNotExist;
use \Spatie\PdfToImage\Pdf;
use Vemid\Helper\ImageHelper;
use Vemid\Task\Image\ProcessingImageTask;

class PdfToImage implements PdfToImageInterface
{
    /** @var string */
    private $pathToPdfFile;

    /**
     * PdfToImage constructor.
     * @param $pathToPdfFile
     */
    public function __construct($pathToPdfFile)
    {
        $this->pathToPdfFile = $pathToPdfFile;
    }

    /**
     * {@inheritdoc}
     *
     * @throws PdfDoesNotExist|\RuntimeException
     */
    public function convertToImage($path, $extension, $width = null, $height = null)
    {
        $file = sprintf(
            '%s/%s.%s',
            rtrim($path, '/'),
            substr(sha1(rand()), 0, 8),
            ltrim($extension, '.')
        );

        if (!file_exists($this->pathToPdfFile)) {
            throw new \RuntimeException('File do not exist');
        }

        $image = new Pdf($this->pathToPdfFile);
        if ($image->saveImage($file)) {
            if ($width || $height) {
                $imageTask = new ProcessingImageTask();
                $imageTask->imagePath = $file;
                $imageTask->width = $width;
                $imageTask->height = $height;
                $imageTask->runInBackground();
            }

            return $file;
        }

        return null;
    }
}
