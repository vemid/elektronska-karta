<?php

namespace Vemid\Services\Printer\DataProviders\Invoice;

use Vemid\Date\DateTime;
use Vemid\Entity\EntityInterface;
use Vemid\Services\Printer\Pdf\PdfDataProviderInterface;

class PdfDataProvider implements PdfDataProviderInterface
{
    /**
     * @return string
     */
    public function getPath()
    {
        return 'invoices/print-pdf';
    }

    /**
     * @return string
     */
    public function getInterpreter()
    {
        return null;
    }

    /**
     * @param \Reservation|EntityInterface $entity
     *
     * @return mixed
     */
    public function getData(EntityInterface $entity)
    {
        return [
            'invoice' => $entity,
            'path' => APP_PATH,
            'date'=> new DateTime()

        ];
    }
}