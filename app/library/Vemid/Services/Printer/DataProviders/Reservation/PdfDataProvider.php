<?php

namespace Vemid\Services\Printer\DataProviders\Reservation;

use Vemid\Date\DateTime;
use Vemid\Entity\EntityInterface;
use Vemid\Services\Printer\Pdf\PdfDataProviderInterface;

class PdfDataProvider implements PdfDataProviderInterface
{
    /**
     * @return string
     */
    public function getPath()
    {
        return 'reservations/print-pdf';
    }

    /**
     * @return string
     */
    public function getInterpreter()
    {
        return null;
    }

    /**
     * @param \Reservation|EntityInterface $entity
     *
     * @return mixed
     */
    public function getData(EntityInterface $entity)
    {
        return [
            'reservation' => $entity,
            'path' => APP_PATH,
            'date'=> new DateTime()

        ];
    }
}