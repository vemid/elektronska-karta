<?php

namespace Vemid\Services\Printer\DataProviders\Purchase;

use Vemid\Entity\EntityInterface;
use Vemid\Services\Printer\Pdf\PdfDataProviderInterface;

class PdfDataProvider implements PdfDataProviderInterface
{
    /**
     * @return string
     */
    public function getPath()
    {
        return 'purchases/print-pdf';
    }

    /**
     * @return string
     */
    public function getInterpreter()
    {
        return null;
    }

    /**
     * @param \Purchase|EntityInterface $entity
     *
     * @return mixed
     */
    public function getData(EntityInterface $entity)
    {
        return [
            'purchase' => $entity,
            'path' => APP_PATH
        ];
    }
}