<?php

namespace Vemid\Services\Printer;

use Vemid\Entity\EntityInterface;

interface DocumentPrinterInterface
{
    /**
     * @param EntityInterface $entity
     * @param string $documentType
     * @return string
     */
    public function printDocument(EntityInterface $entity, $documentType = DocumentType::PDF);
}