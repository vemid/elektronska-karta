<?php

namespace Vemid\Services\Printer;

interface DocumentRendererInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function render($data);

}