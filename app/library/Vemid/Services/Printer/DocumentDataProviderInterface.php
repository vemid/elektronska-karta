<?php

namespace Vemid\Services\Printer;

use Vemid\Entity\EntityInterface;

interface DocumentDataProviderInterface
{
    /**
     * @param EntityInterface $entity
     * @return mixed
     */
    public function getData(EntityInterface $entity);
}