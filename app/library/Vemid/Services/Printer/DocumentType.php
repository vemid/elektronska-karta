<?php

namespace Vemid\Services\Printer;

/**
 * Class DocumentType
 * @package Vemid\Services\Printer
 */
class DocumentType
{
    const PDF = 'Pdf';
    const EXCEL = 'Excel';
}