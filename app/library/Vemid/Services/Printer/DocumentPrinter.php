<?php

namespace Vemid\Services\Printer;

use Vemid\Entity\EntityInterface;

class DocumentPrinter implements DocumentPrinterInterface
{
    /**
     * @param EntityInterface $entity
     * @param string $documentType
     * @return string
     */
    public function printDocument(EntityInterface $entity, $documentType = DocumentType::PDF)
    {
        $dataProvider = $this->getDataProvider($entity->getEntityName(), $documentType);
        $factory = $this->getDocumentRendererFactory($documentType);
        $renderer = $factory->create($dataProvider);
        $data = $dataProvider->getData($entity);

        return $renderer->render($data);
    }

    /**
     * @param string $documentType
     * @return \Vemid\Services\Printer\DocumentRendererFactory
     */
    private function getDocumentRendererFactory($documentType)
    {
        $className = "\\Vemid\\Services\\Printer\\{$documentType}\\{$documentType}RendererFactory";

        if (!class_exists($className)) {
            $className = "\\Vemid\\Services\\Printer\\{$documentType}\\GenericRendererFactory";
        }

        return new $className();
    }

    /**
     * @param string $entityName
     * @param string $documentType
     * @return \Vemid\Services\Printer\DocumentDataProviderInterface
     */
    private function getDataProvider($entityName, $documentType)
    {
        $className = "\\Vemid\\Services\\Printer\\DataProviders\\{$entityName}\\{$documentType}DataProvider";

        return new $className();
    }
}