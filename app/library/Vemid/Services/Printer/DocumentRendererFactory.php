<?php

namespace Vemid\Services\Printer;

abstract class DocumentRendererFactory
{
    /**
     * @param DocumentDataProviderInterface $dataProvider
     * @return DocumentRendererInterface
     */
    public abstract function create(DocumentDataProviderInterface $dataProvider);
}