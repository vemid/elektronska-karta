<?php

namespace Vemid\Services\Printer\Pdf;

use Vemid\Services\Printer\DocumentRendererInterface;
use Phalcon\Di;

class PdfRenderer implements DocumentRendererInterface
{
    /** @var string */
    private $path;

    /** @var string */
    private $interpreter;

    /**
     * PdfRenderer constructor.
     * @param $path
     * @param $interpreter
     */
    public function __construct($path, $interpreter)
    {
        $this->path = $path;
        $this->interpreter = $interpreter;
    }

    /**
     * @param $data
     * @return string
     */
    public function render($data)
    {
        /** @var \Phalcon\Mvc\View\Simple $view */
        $view = Di::getDefault()->getShared('simpleView');
        $html = $view->render($this->path, $data);

        $pdf = new WkHtmlToPdf();

        return $pdf->render($html);
    }
}
