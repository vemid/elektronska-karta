<?php

namespace Vemid\Services\Printer\Pdf;

use Vemid\Services\Printer\DocumentDataProviderInterface;
use Vemid\Services\Printer\DocumentRendererFactory;

class PdfRendererFactory extends DocumentRendererFactory
{
    /**
     * @param DocumentDataProviderInterface|PdfDataProviderInterface $dataProvider
     * @return PdfRenderer
     */
    public function create(DocumentDataProviderInterface $dataProvider)
    {
        return new PdfRenderer($dataProvider->getPath(), $dataProvider->getInterpreter());
    }
}