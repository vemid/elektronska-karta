<?php

namespace Vemid\Services\Printer\Pdf;

use \HybridLogic\PhantomJS\Runner;

class JsInterpreter
{

    private $jsRunner;
    private $script;

    /**
     * @param string $script
     */
    public function __construct($script = '')
    {
        $this->jsRunner = new Runner('phantomjs --ignore-ssl-errors=true');
        $this->script = APPROOT . "/public/arbor/js/$script.js";
    }

    /**
     * @param $html
     * @return string
     */
    public function render($html)
    {
        $input = tempnam(sys_get_temp_dir(), 'report') . '.html';
        $fp = fopen($input, 'w');
        fwrite($fp, $html);

        $output = tempnam(sys_get_temp_dir(), 'report') . '.pdf';
        $this->jsRunner->execute($this->script, escapeshellarg($input), escapeshellarg($output));

        return $output;
    }

}
