<?php

namespace Vemid\Services\Printer\Pdf;

use Vemid\Services\Printer\DocumentDataProviderInterface;

interface PdfDataProviderInterface extends DocumentDataProviderInterface
{
    /**
     * @return string
     */
    public function getInterpreter();

    /**
     * @return string
     */
    public function getPath();
}