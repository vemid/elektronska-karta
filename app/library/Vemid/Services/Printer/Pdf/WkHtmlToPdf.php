<?php

namespace Vemid\Services\Printer\Pdf;

use Vemid\Pdf\RendererInterface;
use Vemid\Pdf\RendererTrait;
use Knp\Snappy\Pdf as Snappy;

/**
 * Class WkHtmlToPdf
 *
 * @package Vemid\Services\Printer\Pdf
 * @author Vemid
 */
class WkHtmlToPdf implements RendererInterface
{

    use RendererTrait;

    /**
     * @param string $content
     * @param string $orientation
     * @return string
     */
    public function render($content, $orientation = 'portrait')
    {
        $snappy = new Snappy();
        if (stripos(php_uname(), 'linux') !== false) {
            $snappy->setBinary(APP_PATH . 'vendor/bin/wkhtmltopdf-amd64');
        } else {
            $snappy->setBinary('/usr/local/bin/wkhtmltopdf');
        }

        $snappy->setOption('page-width', $this->pageWidth);
        $snappy->setOption('page-height', $this->pageHeight);
        $snappy->setOption('margin-top', $this->marginTop);
        $snappy->setOption('margin-bottom', $this->marginBottom);
        $snappy->setOption('margin-right', $this->marginRight);
        $snappy->setOption('margin-left', $this->marginLeft);
        $snappy->setOption('print-media-type', true);
        $snappy->setOption('orientation', $orientation);
        $snappy->setTimeout(120);

        return $snappy->getOutputFromHtml($content);
    }

    /**
     * Sets page size
     *
     * @param string $width
     * @param string $height
     */
    public function setPageSize($width, $height)
    {
        $this->setPageWidth($width);
        $this->setPageHeight($height);
    }

    /**
     * Sets documents margins
     *
     * @param string $marginTop
     * @param string $marginRight
     * @param string $marginBottom
     * @param string $marginLeft
     */
    public function setMargins($marginTop, $marginRight, $marginBottom, $marginLeft)
    {
        $this->setMarginTop($marginTop);
        $this->setMarginRight($marginRight);
        $this->setMarginBottom($marginBottom);
        $this->setMarginLeft($marginLeft);
    }

}
