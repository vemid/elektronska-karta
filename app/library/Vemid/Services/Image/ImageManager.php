<?php

namespace Vemid\Services\Image;


use Phalcon\Http\Request\File;
use Vemid\Task\Image\ProcessingImageTask;

class ImageManager
{
    /** @var File  */
    private $file;

    /** @var string */
    private $path;

    /**
     * ImageManager constructor.
     * @param File $file
     * @param string $path
     */
    public function __construct(File $file, $path)
    {
        $this->file = $file;
        $this->path = $path;
    }

    /**
     * @param null $width
     * @param null $height
     * @return string
     *
     * @throws \RuntimeException
     */
    public function uploadImage($width = null, $height = null)
    {
        $filePath = $this->path . substr(sha1(mt_rand()), 0, 8) . '.' . $this->file->getExtension();
        if (!$this->file->moveTo($filePath)) {
            throw new \RuntimeException('File did not uploaded to server!');
        }

        chmod($filePath, 0777);

//        $task = new ProcessingImageTask();
//        $task->imagePath = $filePath;
//        $task->width = $width;
//        $task->height = $height;
//        $task->runInBackground();

        return $filePath;
    }

    /**
     * @param $baseName
     */
    public function unlinkPreviousImage($baseName)
    {
        if (is_file($uploadedFile = $this->path . $baseName)) {
            @unlink($uploadedFile);
        }
    }
}