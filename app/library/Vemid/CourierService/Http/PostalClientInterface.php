<?php

namespace Vemid\CourierService\Http;

interface PostalClientInterface
{
    /**
     * @param $url
     * @param string $method
     * @param array $data
     */
    public function sendRequest($url, string $method = 'GET', array $data = []);
}