<?php

namespace Vemid\CourierService\Http;

class PostalRoutes
{
    public const MUNICIPALITIES = '/ExternalApi/data/municipalities?date=%s';
    public const TOWNS = '/ExternalApi/data/towns?date=%s';
    public const STREETS = '/ExternalApi/data/streets?date=%s';
    public const SHIPMENT = '/ExternalApi/data/addshipment';
}