<?php

namespace Vemid\CourierService\Http;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;

class Client implements PostalClientInterface
{
    /** @var ClientInterface */
    private $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function sendRequest($url, string $method = 'GET', array $data = [])
    {
        $options = [];
        if ($method === 'POST') {
            $options['headers'] = ['Content-Type' => 'application/json; charset=utf-8'];
        }

        try {
            $response = $this->client->send(
                $this->makeRequest($method, $url, $data), $options
            );
            $stream = $response->getBody();

            if ($response->getStatusCode() === 400) {
                throw new \LogicException(sprintf('Failed to sent request with error:%s with data:%s', (string)$stream, json_encode($data)));
            }

        } catch (ClientException $e) {
            $response = $e->getResponse();
            $stream = $response->getBody();

            throw new \LogicException(sprintf('Failed to sent request with error:%s with data:%s', (string)$stream, json_encode($data)));
        }  catch (BadResponseException $e) {
            throw new \LogicException(sprintf('Failed to sent request with error:%s with data:%s', $e->getMessage(), json_encode($data)));
        }

        return json_decode($stream->getContents(), true);
    }

    /**
     * @param string $method
     * @param $url
     * @param array $body
     * @return Request
     */
    private function makeRequest(string $method, $url, array $body = []): Request
    {
        $request = new Request($method, $url, [], json_encode($body));
        $request->withHeader('Content-Type', 'application/json; charset=utf-8');

        return $request;
    }
}
