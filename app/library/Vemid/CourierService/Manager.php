<?php

namespace Vemid\CourierService;

use Phalcon\Config;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Task\WarehouseOrders\SendToCourierServiceTask;

class Manager
{
    private const SHIPMENT_TYPE_URGENT = 1;
    private const SHIPMENT_TYPE_REGULAR = 2;
    private const PAYMENT_BY_SENDER = 0;
    private const PAYMENT_BY_PICK_UP = 1;
    private const PAYMENT_BY_RECIPIENT = 2;
    private const PAYMENT_TYPE_CASH = 1;
    private const PAYMENT_TYPE_INVOICE = 2;
    private const BUY_OUT_FOR_SENDER = 0;
    private const BUY_OUT_FOR_PICKUP = 1;
    private const RETURN_DOC_NO_RETURN = 0;
    private const RETURN_DOC_RETURN_DOCUMENTS = 1;
    private const RETURN_DOC_RETURN_CASH = 2;
    private const RETURN_DOC_RETURN_POD = 3;


    /** @var EntityManagerInterface  */
    private $entityManager;

    /** @var Config  */
    private $config;

    /** @var Handler */
    private $handler;

    /**
     * @param EntityManagerInterface $entityManager
     * @param Config $config
     * @param Handler $handler
     */
    public function __construct(EntityManagerInterface $entityManager, Config $config, Handler $handler)
    {
        $this->entityManager = $entityManager;
        $this->config = $config;
        $this->handler = $handler;
    }

    public function __invoke(\WarehouseOrder $warehouseOrder = null)
    {
        if (!$warehouseOrder) {
            $warehouseOrders = $this->entityManager->find(\WarehouseOrder::class, [
                \WarehouseOrder::PROPERTY_USE_COURIER_SERVICE . ' = :useCourierService: AND ' .
                \WarehouseOrder::PROPERTY_PUSHED_TO_MIS . ' = :pushedToMis: AND ' .
                \WarehouseOrder::PROPERTY_POSTED . ' = :posted:',
                'bind' => [
                    'useCourierService' => true,
                    'pushedToMis' => true,
                    'posted' => false,
                ]
            ]);
        } else {
            if (!$warehouseOrder->getPushedToMis()) {
                throw new \LogicException('Nije izvršena sinhronizacija za MIS-om!');
            }

            if (!$warehouseOrder->getUseCourierService()) {
                throw new \LogicException('Pošiljka nije podložna slanju kurirskom službom!');
            }

            if ($warehouseOrder->getPosted()) {
                throw new \LogicException('Pošiljka već oslata!');
            }

            $warehouseOrders = [$warehouseOrder];
        }

        $channelSupplyEntities = [];
        foreach ($warehouseOrders as $wo) {
            $channelSupplyEntities[$wo->getChannelSupplyEntityId()][] = $wo;
        }

        $towns = $this->handler->getTowns();
        $key = \array_search('Beograd (Voždovac)', array_column($towns, 'Name'), false);
        if (!$key) {
            throw new \LogicException('Unable to find client town from courier list');
        }


        foreach ($channelSupplyEntities as $channelSupplyEntitiesId => $warehouseOrders) {
            $data = [
                'CClientID' => $this->config->postalService->CClientID,
                'CName' => $this->config->postalService->CName,
                'CAddress' => $this->config->postalService->CAddress,
                'CAddressNum' => $this->config->postalService->CAddressNum,
                'CTownID' => $towns[$key]['Id'],
                'CCPhone' => '', //not required
            ];

            /** @var \ChannelSupplyEntity $channelSupplyEntity */
            $channelSupplyEntity = $this->entityManager->findOne(\ChannelSupplyEntity::class, $channelSupplyEntitiesId);
            /** @var \ChannelSupplyEntity $exitChannelSupplyEntity */
            $exitChannelSupplyEntity = $this->entityManager->findOne(\ChannelSupplyEntity::class, $warehouseOrders[0]->getExitChannelSupplyEntityId());
            $data += $this->prepareDate($channelSupplyEntity,$exitChannelSupplyEntity);
            $data += $this->preparePackets($warehouseOrders);

            $task = new SendToCourierServiceTask();
            $task->data = $data;
            $task->warehouseOrders = $warehouseOrders;
            $task->execute(); //TODO Change to background
        }
    }

    /**
     * @param \ChannelSupplyEntity $channelSupplyEntity
     * @param \ChannelSupplyEntity $exitChannelSupplyEntity
     * @return array
     */
    private function prepareDate(\ChannelSupplyEntity $channelSupplyEntity, \ChannelSupplyEntity $exitChannelSupplyEntity): array
    {
        $paymentType = self::PAYMENT_TYPE_INVOICE;

        if(!$channelSupplyEntity->getPostalContract()) {
            $paymentType = self::PAYMENT_TYPE_CASH;
        }

        $data = [
            'CClientID' => $this->config->postalService->CClientID,
            'CName' => "BEBAKIDS ".$exitChannelSupplyEntity->getDisplayName(), //not required
            'CAddress' => $exitChannelSupplyEntity->getPostalAddress(), //not required
            'CAddressNum' => $exitChannelSupplyEntity->getStreetNumber(),
            'CTownID' => $exitChannelSupplyEntity->getPostalServiceTownId(),
            'PuClientID' => $this->config->postalService->CClientID,
            'PuName' => "BEBAKIDS Magacin Knjazevac",
            'PuAddress' => $exitChannelSupplyEntity->getStreet(),
            'PuAddressNum' => $exitChannelSupplyEntity->getStreetNumber(),
            'PuTownID' => $exitChannelSupplyEntity->getPostalServiceTownId(),
            'PuCName' => $exitChannelSupplyEntity->getWorkerName(), //not required
            'PuCPhone' => $exitChannelSupplyEntity->getWorkerPhone(), //not required
            'RClientID' => $channelSupplyEntity->getPostalContract() ? $channelSupplyEntity->getPostalContract() : "", //not required
            'RName' => "BEBAKIDS MP",
            'RAddress' => $channelSupplyEntity->getStreet(),
            'RAddressNum' => $channelSupplyEntity->getStreetNumber(),
            'RTownID' => $channelSupplyEntity->getPostalServiceTownId(),
            'RCName' => $channelSupplyEntity->getWorkerName(), //not required
            'RCPhone' => $channelSupplyEntity->getWorkerPhone(), //not required
            'DlTypeID' => self::SHIPMENT_TYPE_REGULAR,
            'PaymentBy' => self::PAYMENT_BY_RECIPIENT,
            'PaymentType' => $paymentType,
            'BuyOut' => 0,
            'BuyOutFor' => self::BUY_OUT_FOR_SENDER,
            'Value' => 0,
            'Content' => 'Odeca',
            'Mass' => 20000,
            'ReferenceID' => uniqid('', false),
            'ReturnDoc' => self::RETURN_DOC_NO_RETURN,
        ];

        return $data;
    }

    /**
     * @param \WarehouseOrder[] $warehouseOrders
     * @return array
     */
    private function preparePackets($warehouseOrders): array
    {
        $data = [];
        foreach ($warehouseOrders as $key => $warehouseOrder) {
            $data['PackageList'][$key]['Code'] = $warehouseOrder->getPostalBarcode();
            $data['PackageList'][$key]['ReferenceID'] = $warehouseOrder->getId();
        }

        return $data;
    }
}
