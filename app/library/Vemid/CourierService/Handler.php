<?php

namespace Vemid\CourierService;

use Vemid\Date\DateTime;
use Vemid\CourierService\Http\PostalClientInterface;
use Vemid\CourierService\Http\PostalRoutes;

class Handler
{
    /** @var PostalClientInterface */
    private $client;

    /**
     * @param PostalClientInterface $client
     */
    public function __construct(PostalClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getMunicipalities()
    {
        $date = (new DateTime('2020-01-01'));

        return $this->client->sendRequest(sprintf(PostalRoutes::MUNICIPALITIES, $date->format('YmdHis')));
    }

    /**
     * @return mixed
     */
    public function getTowns()
    {
        $date = (new DateTime('2020-01-01'));

        return $this->client->sendRequest(sprintf(PostalRoutes::TOWNS, $date->format('YmdHis')));
    }

    /**
     * @return mixed
     */
    public function getStreets()
    {
        $date = (new DateTime('2020-01-01'));

        return $this->client->sendRequest(sprintf(PostalRoutes::STREETS, $date->format('YmdHis')));
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function addShipment(array $data)
    {
        return $this->client->sendRequest(PostalRoutes::SHIPMENT, 'POST', $data);
    }
}
