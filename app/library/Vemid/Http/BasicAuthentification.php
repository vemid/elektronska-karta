<?php

namespace Vemid\Http;

use Phalcon\Config;
use Phalcon\Http\Request;
use Phalcon\Http\Response;

class BasicAuthentification
{
    public function __invoke(Request $request, Response $response, Config $config)
    {
        $response->setContentType('application/json', 'UTF-8');

        $username = $request->getServer('PHP_AUTH_USER');
        $password = $request->getServer('PHP_AUTH_PW');

        if ($username !== $config->apiCredentials->username || $password !== $config->apiCredentials->password) {
            $response->setHeader('WWW-Authenticate', 'Basic realm="Bebakids"');
            $response->setStatusCode('401', 'Not Authorized');
            $response->setContent('Wrong username and/or password.');

            return false;
        }
    }
}
