<?php

namespace Vemid\Form;
use Vemid\Entity\EntityInterface;

/**
 * Interface BuilderInterface
 *
 * @package Vemid\Form
 */
interface BuilderInterface
{

    /**
     * @param EntityInterface $entity
     * @param array $exclude
     * @return Form
     */
    public function build(EntityInterface $entity, array $exclude = []);

}
