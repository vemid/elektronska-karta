<?php

namespace Vemid\Form;

/**
 * Interface RendererInterface
 *
 * @package Form
 * @author Vemid
 */
interface RendererInterface
{

    /**
     * @param Form $form
     * @return mixed
     */
    public function render(Form $form);

}
