<?php

namespace Vemid\Form\Builder;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Form\BuilderInterface;
use Vemid\Form\Form;
use Vemid\Filter\Humanize;
use Vemid\Date\DateTime;
use Vemid\Entity\EntityInterface;
use Phalcon\Annotations\Adapter\Memory as AnnotationReader;
use Phalcon\Forms\ElementInterface;
use Phalcon\Translate\AdapterInterface as TranslatorInterface;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;

/**
 * Class Annotations
 *
 * @package Vemid\Form\Builder
 * @author Vemid
 */
class Annotations implements BuilderInterface
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var TranslatorInterface */
    private $translator;

    /**
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * @param EntityInterface $entity
     * @param array $exclude
     * @return Form
     */
    public function build(EntityInterface $entity, array $exclude = [])
    {
        $reader = new AnnotationReader();
        $form = new Form($entity);
        $humanize = new Humanize();

        $reflection = $reader->get(get_class($entity));
        $properties = $reflection->getPropertiesAnnotations();
        $translator = $this->translator;

        foreach ($properties as $propertyName => $annotations) {

            if (!in_array($propertyName, $exclude)) {

                if ($annotations->has('Column') && $annotations->has('FormElement')) {

                    $formElementAnnotation = $annotations->get('FormElement');
                    $formElementArguments = $formElementAnnotation->getArguments();

                    if (!isset($formElementArguments['type'])) {
                        continue;
                    }

                    $columnAnnotation = $annotations->get('Column');
                    $columnArguments = $columnAnnotation->getArguments();
                    $namespace = '';

                    if (!strstr($formElementArguments['type'], '\\')) {
                        $namespace = '\\Phalcon\\Forms\\Element\\';
                    }

                    $className = $namespace . $formElementArguments['type'];

                    if (!class_exists($className)) {
                        $className = "\\Vemid\\Form\\Element\\" . $formElementArguments['type'];
                    }

                    if ($formElementArguments['type'] == 'Select') {
                        $formElementNull = [];
                        $formElementOptions = [];

                        if (isset($formElementArguments['relation'])) {
                            foreach ($this->entityManager->find($formElementArguments['relation']) as $elementObj) {
                                $key = $elementObj->getEntityId();
                                $val = (string) $elementObj;
                                if ($key && $val) {
                                    $formElementOptions[$key] = $val;
                                }
                            }
                        } else if (isset($formElementArguments['options']) && is_array($formElementArguments['options'])) {
                            if (array_values($formElementArguments['options']) === $formElementArguments['options']) {
                                $formElementOptions = array_combine($formElementArguments['options'], $formElementArguments['options']);
                            } else {
                                $formElementOptions = $formElementArguments['options'];
                            }
                            array_walk($formElementOptions, function (&$value) use ($translator) {
                                $value = $translator->t($value);
                                return $value;
                            });
                        }
                        $formElementOptions = $formElementNull + $formElementOptions;
                        $element = new $className($propertyName, $formElementOptions);
                        $element->setAttribute('class', 'search');
                    } else {
                        if (class_exists($className)) {
                            $element = new $className($propertyName);
                        }
                    }

                    /**@var ElementInterface $element */
                    $element->clear();

                    if (isset($formElementArguments['label'])) {
                        $element->setLabel($translator->t($formElementArguments['label']));
                        $label = $formElementArguments['label'];
                    } else {
                        $label = $propertyName;
                    }

                    if ($defaultValue = $entity->getProperty($propertyName)) {
                        $element->setDefault($defaultValue instanceof DateTime ? $defaultValue->getLong() : $defaultValue);
                    }

                    if ($formElementArguments['type'] === 'Date') {
                        $element->setAttribute('class', $columnArguments['type'] === 'dateTime' ? 'datetimepicker' : 'datepicker');
                    }

                    if (isset($formElementArguments['required']) && $formElementArguments['required']) {
                        $element->addValidator(new PresenceOf([
                            'message' => $translator->t(
                                'The %s is required.',
                                $translator->t($humanize->filter($label))
                            )
                        ]));

                        $attributeClass = $element->getAttribute('class');
                        $element->setAttribute('class', "$attributeClass required");
                    }

                    if ('email' === strtolower($formElementArguments['type'])) {
                        $element->addValidator(new Email([
                            'message' => $translator->t('Please provide valid Email address.')
                        ]));
                    }

                    if ($annotations->has('FormElementValidator')) {
                        $formElementValidatorAnnotation = $annotations->get('FormElementValidator');
                        $formElementValidatorArguments = $formElementValidatorAnnotation->getArguments();
                        if (count($formElementValidatorArguments)) {
                            if (isset($formElementValidatorArguments['class'])) {
                                $namespace = '';
                                if (!strstr($formElementValidatorArguments['class'], "\\")) {
                                    $namespace = '\\Phalcon\\Validation\\Validator\\';
                                }
                                $classArguments = [];
                                if (isset($formElementValidatorArguments['arguments'])) {
                                    $classArguments = $formElementValidatorArguments['arguments'];
                                }
                                $className = $namespace . $formElementValidatorArguments['class'];
                                $element->addValidator(new $className($classArguments));
                            }
                        }
                    }

                    if ($annotations->has('FormElementAttributes')) {
                        $formElementAttributesAnnotation = $annotations->get('FormElementAttributes');
                        $formElementAttributesArguments = $formElementAttributesAnnotation->getArguments();
                        if (count($formElementAttributesArguments)) {
                            foreach ($formElementAttributesArguments as $attributeName => $attributeValue) {
                                if ($element->getAttribute($attributeName)) {
                                    $attributeValue = $element->getAttribute($attributeName) . ' ' . $attributeValue;
                                }
                                $element->setAttribute($attributeName, $attributeValue);
                            }
                        }
                    }

                    $form->add($element);
                }
            }
        }

        return $form;
    }

}
