<?php

namespace Vemid\Form;

use Vemid\Date\DateTime;
use Vemid\Date\Time;
use Vemid\Entity\EntityInterface;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\ElementInterface;
use Phalcon\Mvc\Model\ResultsetInterface;

/**
 * Class Renderer
 *
 * @package Form
 * @subpackage Form\Renderer
 * @author Vemid
 */
abstract class Renderer
{

    /**
     * @param ElementInterface $element
     * @return array|string
     */
    protected function renderElementDefaultValue(ElementInterface $element)
    {
        $defaultValue = $this->filterValue(
            $element->getForm()->getValue($element->getName())
        );

        if (!$defaultValue) {
            $defaultValue = $this->filterValue($element->getDefault());
        }

        if ($defaultValue && !\is_array($defaultValue)) {
            $defaultValue = htmlentities($defaultValue);
        }

        return $defaultValue ?? '';
    }

    /**
     * @param ElementInterface $element
     * @return string
     */
    protected function renderElementName(ElementInterface $element)
    {
        $attributes = $this->renderElementAttributes($element);
        if (array_key_exists('name', $attributes)) {
            $name = $attributes['name'];
        } else {
            $name = $element->getName();
        }

        return $name ?? '';
    }

    /**
     * @param ElementInterface $element
     * @return array|string
     */
    protected function renderElementOptions(ElementInterface $element)
    {
        if ($element instanceof Select) {
            $options = $element->getOptions();
            if (\is_array($options) || $options instanceof ResultsetInterface || $options instanceof \ArrayObject) {
                return $this->filterValue($options);
            }

            return $this->filterValue([$options]);
        }

        return [];
    }

    /**
     * @param ElementInterface $element
     * @return string
     */
    protected function renderElementLabel(ElementInterface $element)
    {
        $label = $element->getLabel();

        return $label ?? '';
    }

    /**
     * @param ElementInterface $element
     * @return array
     */
    protected function renderElementAttributes(ElementInterface $element)
    {
        return $element->getAttributes();
    }

    /**
     * @param ElementInterface $element
     * @return array
     */
    protected function renderElementMessages(ElementInterface $element)
    {
        $messages = [];

        foreach ($element->getForm()->getMessages() as $message) {
            if ($message->getField() === $element->getName()) {
                $messages[] = $message->getMessage();
                break;
            }
        }

        return $this->filterValue($messages);
    }

    /**
     * @param ElementInterface $element
     * @return string
     */
    protected function renderElementType(ElementInterface $element)
    {
        $className = \get_class($element);
        $classNameArray = explode('\\', $className);

        return strtolower(end($classNameArray));
    }

    /**
     * @param ElementInterface $element
     * @return bool
     */
    protected function isElementRequired(ElementInterface $element)
    {
        if ($elementClass = $element->getAttribute('class')) {
            if (false !== strpos($elementClass, 'required')) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param mixed $value
     * @return array|string
     */
    protected function filterValue($value)
    {
        if ($value === null) {
            return $value;
        }

        if ($value instanceof EntityInterface) {
            return $value->getObjectId();
        }

        if ($value instanceof DateTime) {
            return $value->getUnixFormat();
        }

        if ($value instanceof Time) {
            return $value->getShort();
        }

        if ($value instanceof ResultsetInterface || $value instanceof \ArrayObject) {
            $array = [];
            foreach ($value as $key => $val) {
                if ($val instanceof EntityInterface) {
                    $array[$val->getObjectId()] = $val->getDisplayName();
                }
            }

            return $array;
        }

        if (\is_array($value)) {
            foreach ($value as $key => $val) {
                if ($val instanceof EntityInterface) {
                    $value[$val->getObjectId()] = $val->getDisplayName();
                    unset($value[$key]);
                } else {
                    $value[$key] = $this->filterValue($val);
                }
            }
        }

        return $value;
    }
}
