<?php

namespace Vemid\Form\Element;


use Phalcon\Forms\Element;
use Phalcon\Forms\ElementInterface;
use Phalcon\Tag;

class Color extends Element implements ElementInterface
{
    /**
     * @param null $attributes
     * @return mixed
     */
    public function render($attributes = null)
    {
        return Tag::colorField($this->prepareAttributes($attributes));
    }
}