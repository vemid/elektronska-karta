<?php

namespace Vemid\Form\Renderer;

use Vemid\Form\Form;
use Vemid\Form\Renderer;
use Vemid\Form\RendererInterface;

/**
 * Class Json
 *
 * @package Form
 * @subpackage Form\Renderer
 * @author Vemid
 */
class Json extends Renderer implements RendererInterface
{

    /**
     * {@inheritdoc}
     */
    public function render(Form $form)
    {
        $formArray = [];

        if (empty($form->getElements())) {
            return $formArray;
        }

        foreach ($form->getElements() as $key => $element) {
            $formArray[$key]['name'] = $this->renderElementName($element);
            $formArray[$key]['label'] = $this->renderElementLabel($element);
            $formArray[$key]['default'] = $this->renderElementDefaultValue($element);
            $formArray[$key]['options'] = $this->renderElementOptions($element);
            $formArray[$key]['attributes'] = $this->renderElementAttributes($element);
            $formArray[$key]['messages'] = $this->renderElementMessages($element);
            $formArray[$key]['required'] = $this->isElementRequired($element);
            $formArray[$key]['type'] = $this->renderElementType($element);
        }

        return $formArray;
    }

}
