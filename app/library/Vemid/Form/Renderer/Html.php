<?php

namespace Vemid\Form\Renderer;

use Vemid\Form\Form;
use Vemid\Form\Renderer;
use Vemid\Form\RendererInterface;
use Phalcon\Forms\Element;
use Phalcon\Forms\ElementInterface;

/**
 * Class Html
 *
 * @package Form
 * @subpackage Form\Renderer
 * @author Vemid
 */
class Html extends Renderer implements RendererInterface
{

    /**
     * {@inheritdoc}
     */
    public function render(Form $form, $showLabelInline = true, $class = '', $split = false, $saveButton = true)
    {
        $template = "<form action='{$form->getAction()}' method='post' class='form-horizontal".($class ? ' ' . $class : '')."' enctype='multipart/form-data'>\n";
        $spliter = (int)ceil(count($form->getElements()) / 2);
        if ($split) {
            $template .= '<div class="col-xs-6">';
        }


        $counter = 1;
        foreach ($form->getElements() as $element) {
            $template .= $this->renderElement($element, $showLabelInline);
            if ($split && $spliter === $counter) {
                $template .= '</div><div class="col-xs-6">';
            }

            $counter++;
        }

        if ($split) {
            $template .= '</div>';
        }

        if ($saveButton) {
            $template .= "<div class='col-xs-12'><div class='space-30'></div><div class='form-group'><div class='text-center'><button class='btn btn-success'><i class='fa fa-save'></i> Save</button></div></div></div>\n";
        }

        $template .= "</form>\n";

        return $template;
    }

    /**
     * @param ElementInterface $element
     * @param bool $showLabelInline
     * @return string
     */
    public function renderElement(ElementInterface $element, $showLabelInline = true)
    {
        $isRequired = $this->isElementRequired($element);
        $hasClass = false;
        $multiple = false;

        $type = $this->renderElementType($element);
        $name = $this->renderElementName($element);
        $label = $this->renderElementLabel($element);
        $default = $this->renderElementDefaultValue($element);
        $options = ['' => '-- Izaberite --'] + $this->renderElementOptions($element);
        list($elementClass) = explode('__', $name, 2);

        $attributes = '';
        foreach ($this->renderElementAttributes($element) as $attrName => $attrValue) {
            if ($attrName === 'class') {
                $hasClass = true;
                $attrValue .= " form-control form-element-{$elementClass}" . $type === 'select' ? ' search' : '';
            }
            $attributes .= " $attrName='$attrValue'";

            if ($attrName === 'multiple' && ($attrValue = 'multiple' || $attrValue)) {
                $multiple = true;
            }
        }

        if (!$hasClass) {
            $attributes .= " class='form-control form-element-{$elementClass}" . ($type === 'select' ? ' search' : '') ."'";
        }

        $messages = $this->renderElementMessages($element);

        $template = "<div class='form-group" . ($type === 'hidden' ? ' hidden' : '') . (\count($messages) ? ' has-error' : '') . "'>\n";

        if ($label) {
            $template .= "<label class='" . ($showLabelInline ? 'col-sm-2 control-label no-padding-right' : '')
                . "' for='$name'>$label: "
                . ($isRequired ? "<span>*</span>" : "") . "</label>\n";
        }

        $template .= ($showLabelInline ? "<div class='col-sm-10'>" : '') . "\n";

        switch ($type) {
            case 'textarea' :
                $template .= "<textarea name='$name' id='$name' style='resize:vertical;min-height:150px;width:100%;' $attributes>$default</textarea>\n";
                break;

            case 'date' :
                $template .= "<div class='input-group date'><span class='input-group-addon'><i class='fa fa-calendar'></i></span>"
                    . "<input name='$name' id='$name' type='text' value='$default' $attributes /></div>\n";
                break;

            case 'check' :
                $template .= "<input name='$name' id='$name' type='checkbox' value='$default' class='js-switch' " . ($default ? " checked='checked'" : "") . " />\n";
                break;

            case 'select' :
                $template .= "<select name='$name" . ($multiple ? '[]' : '') . "' id='$name' $attributes>";
                foreach ($options as $value => $option) {
                    $selected = '';
                    if (\is_array($default)) {
                        // If array is not associative use keys
                        if (array_keys($default) !== range(0, \count($default) - 1)) {
                            $default = array_keys($default);
                        }
                        if (\in_array($value, $default, false)) {
                            $selected = "selected='selected'";
                        }
                    } else {
                        if ($value === $default) {
                            $selected = "selected='selected'";
                        }
                    }
                    $template .= "<option value='$value' $selected>$option</option>";
                }
                $template .= "</select>\n";
                break;

            default :
                $template .= "<input type='$type' name='$name' id='$name' $attributes value='$default' />\n";
                break;
        }

        $template .= "<div class='help-block m-b-none'>" . implode('<br/>', $messages) . "</div>\n";
        $template .= $showLabelInline ? "</div>" : "</div>\n";

        return $template . '</div>';
    }

}
