<?php

namespace Vemid\Form;

use Vemid\Entity\EntityInterface;
use Phalcon\Forms\ElementInterface;
use Vemid\Application\Di;

/**
 * Class Form
 *
 * @method Di getDI()
 *
 * @property \Phalcon\Translate\AdapterInterface $translator
 * @property \Vemid\Messenger\Manager $messengerManager
 * @property \Vemid\Entity\Manager\EntityManagerInterface $entityManager
 * @property \User $currentUser
 *
 * @package Vemid\Form
 * @author Vemid
 */
class Form extends \Phalcon\Forms\Form
{

    /** @var EntityInterface[] */
    protected $_entities = [];

    /**
     * @return EntityInterface
     */
    public function getEntity()
    {
        return $this->_entity;
    }

    public function add(\Phalcon\Forms\ElementInterface $element, $postion = null, $type = null)
    {
        parent::add($element, $postion, $type);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function has($name)
    {
        return parent::has($name)
        || ($this->getEntity() && parent::has($name . $this->getElementSuffix($this->getEntity())));
    }

    /**
     * @param string $name
     * @return ElementInterface
     */
    public function get($name)
    {
        if (parent::has($name)) {
            return parent::get($name);
        }

        if ($this->getEntity()) {
            $name .= $this->getElementSuffix($this->getEntity());
            if (parent::has($name)) {
                return parent::get($name);
            }
        }
    }

    /**
     * @param Form $form
     */
    public function mergeForm(Form $form)
    {
        $entity = $form->getEntity();
        $this->_entities[$entity->getEntityIdentifier()] = $entity;

        if ($form->count() > 0) {
            foreach ($form->getElements() as $element) {
                $element = clone $element;
                $element->setName($element->getName() . $this->getElementSuffix($entity));
                $this->add($element);
            }
        }
    }

    /**
     * @param EntityInterface $entity
     * @param bool $filterProperties
     * @return Form
     */
    public function extractForm(EntityInterface $entity, $filterProperties = true)
    {
        $form = new Form();
        $form->setEntity($entity);

        foreach ($this->getElementsForEntity($entity) as $element) {
            $elementName = $element->getName();
            if ($filterProperties) {
                $filteredName = $this->filterPropertyName($elementName, $entity);
                $element->setName($filteredName);
            }

            if ($this->hasMessagesFor($elementName)) {
                $messages = $this->getMessagesFor($elementName);
                $form->_messages[$element->getName()] = $messages;
                $element->setMessages($messages);
            }

            if (isset($this->_data[$elementName])) {
                $form->_data[$element->getName()] = $this->_data[$elementName];
                unset($this->_data[$elementName]);
            }

            $this->remove($elementName);
            $form->add($element);
        }

        if (array_key_exists($entity->getEntityIdentifier(), $this->_entities)) {
            unset($this->_entities[$entity->getEntityIdentifier()]);
        }

        return $form;
    }

    /**
     * @return array
     */
    public function getData()
    {
        $data = [];

        if ($this->count() > 0) {
            foreach ($this->getElements() as $element) {
                $name = $element->getName();
                $value = (isset($this->_data[$name]) && $this->_data[$name]) ? $this->_data[$name] : null;;

                if (is_string($value) && empty($value)) {
                    $value = null;
                }

                $data[$name] = $value;
            }
        }

        return $data;
    }

    /**
     *
     */
    public function synchronizeEntityData()
    {
        $references = [];

        foreach ($this->_entities as $entity) {
            $form = $this->extractForm($entity);
            $form->synchronizeEntityData();
            $references[lcfirst(get_class($entity))] = $entity;
        }

        $entity = $this->getEntity();
        if (is_array($this->_data) && count($this->_data)) {
            foreach ($this->_data as $propertyName => $propertyValue) {
                $entity->setProperty($propertyName, $propertyValue);
            }
        }

        foreach ($references as $referencedEntityName => $referencedEntity) {
            if (is_callable([$entity, $referencedEntityName])) {
                $entity->$referencedEntityName = $referencedEntity;
            }
        }
    }

    /**
     * @param EntityInterface $entity
     * @return ElementInterface[]
     */
    public function getElementsForEntity(EntityInterface $entity)
    {
        $elements = [];

        if (array_key_exists($entity->getEntityIdentifier(), $this->_entities)) {
            foreach ($this->getElements() as $element) {
                $elementName = $element->getName();
                if (stripos(strrev($elementName), strrev($this->getElementSuffix($entity))) === 0) {
                    $elements[] = $element;
                }
            }
        }

        return $elements;
    }

    /**
     * @param null|EntityInterface $entity
     * @param string $propertyName
     * @return string
     */
    protected function filterPropertyName($propertyName, EntityInterface $entity = null)
    {
        if ($entity === null) {
            $entity = $this->getEntity();
        }

        $suffix = $this->getElementSuffix($entity);

        if (stripos(strrev($propertyName), strrev($suffix)) === 0) {
            $propertyName = substr($propertyName, 0, (-1 * strlen($suffix)));
        }

        return $propertyName;
    }

    /**
     * @param EntityInterface $entity
     * @return string
     */
    protected function getElementSuffix(EntityInterface $entity)
    {
        return '__' . $entity->getEntityIdentifier();
    }
}
