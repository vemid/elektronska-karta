<?php

namespace Vemid\Filter;

/**
 * Class Humanize
 *
 * @package Filter
 * @author Vemid
 */
class Humanize implements FilterInterface
{

    protected $_rules = array(
        '/([a-z])([A-Z])/' => '\1 \2',
        '/[\s\-_]+/' => ' '
    );

    /**
     * Takes multiple words separated by spaces, dashes or underscores and humanize them
     *
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        $result = strval($value);

        foreach ($this->_rules as $rule => $replacement) {
            if (preg_match($rule, $result)) {
                $result = preg_replace($rule, $replacement, $result);
            }
        }

        return $result;
    }

}
