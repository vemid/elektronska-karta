<?php

namespace Vemid\Error;

/**
 * Interface LoggerInterface
 *
 * @package Vemid\Error\Logger
 * @author Vemid
 */
interface LoggerInterface
{
    /**
     * @param \Exception $e
     * @param bool $isError
     * @param array|null $vars
     */
    public function log($e, $isError = false, array $vars = null);
}
