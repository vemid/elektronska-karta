<?php

namespace Vemid\Error;

/**
 * Class Exception
 *
 * @package Vemid\Error
 * @author Vemid
 */
class Exception extends \Exception
{

    // custom string representation of object
    public function __toString()
    {
        return get_class($this) . " '{$this->message}' in {$this->file}({$this->line})\n"
        . "{$this->getTraceAsString()}";
    }

}
