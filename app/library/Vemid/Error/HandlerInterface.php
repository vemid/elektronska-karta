<?php

namespace Vemid\Error;

/**
 * Interface HandlerInterface
 *
 * @package Vemid\Error
 * @author Vemid
 */
interface HandlerInterface
{
    /**
     *
     */
    public function registerExceptionHandler();

    /**
     *
     */
    public function registerErrorHandler();

    /**
     *
     */
    public function registerShutdownFunction();

    /**
     * @param \Exception $e
     * @param bool $isError
     * @param null|array $vars
     */
    public function handleException($e, $isError = false, array $vars = null);

    /**
     * @param string $type
     * @param string $message
     * @param string $file
     * @param int $line
     * @param array $context
     */
    public function handleError($type, $message, $file = '', $line = 0, array $context = []);

    /**
     * @param null|array $error
     */
    public function handleFatalError(array $error = null);

    /**
     * @param string $environment
     */
    public function initEnvironment($environment);

    /**
     * @param LoggerInterface $logger
     */
    public function addLogger(LoggerInterface $logger);
}
