<?php

namespace Vemid\Error;

use Vemid\Application\Application;

/**
 * Class Handler
 *
 * @package Vemid\Error
 * @author Vemid
 */
class Handler implements HandlerInterface
{
    const DEBUG = 'DEBUG';
    const INFO = 'INFO';
    const WARNING = 'WARNING';
    const ERROR = 'ERROR';
    const FATAL = 'FATAL';

    protected $validErrorTypes = [
        E_ERROR,
        E_WARNING,
        E_PARSE,
        E_NOTICE,
        E_CORE_ERROR,
        E_CORE_WARNING,
        E_COMPILE_ERROR,
        E_COMPILE_WARNING,
        E_USER_ERROR,
        E_USER_WARNING,
        E_USER_NOTICE,
        E_STRICT,
        E_RECOVERABLE_ERROR,
        E_DEPRECATED,
        E_USER_DEPRECATED,
    ];

    protected $defaultErrorTypes = [
        E_ERROR,
        E_PARSE,
        E_CORE_ERROR,
        E_CORE_WARNING,
        E_COMPILE_ERROR,
        E_COMPILE_WARNING,
        E_STRICT,
    ];

    protected $logSeverities = [];

    /** @var LoggerInterface[] */
    protected $loggers = [];

    /**
     * {@inheritdoc}
     */
    public function registerExceptionHandler()
    {
        set_exception_handler(function ($e) {
            $this->logException($e);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function registerErrorHandler()
    {
        set_error_handler([$this, 'handleError']);
    }

    /**
     * {@inheritdoc}
     */
    public function registerShutdownFunction()
    {
        register_shutdown_function([$this, 'handleFatalError']);
    }

    /**
     * {@inheritdoc}
     */
    public function handleException($e, $isError = false, array $vars = null)
    {
        $this->logException($e, $isError, $vars);
    }

    /**
     * @param int $type
     * @param string $message
     * @param string $file
     * @param int $line
     * @param array $context
     * @return bool
     */
    public function handleError($type, $message, $file = '', $line = 0, array $context = [])
    {
        $errors = 0;
        foreach ($this->getErrorTypesToProcess() as $errorType) {
            $errors |= $errorType;
        }

        if ($type && $errors) {
            $e = new \ErrorException($message, 0, $type, $file, $line);
            $this->handleException($e, true, $context);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function handleFatalError(array $error = null)
    {
        $error = $error ?? error_get_last();

        if ($error === null) {
            return;
        }

        $errors = 0;
        foreach ($this->getErrorTypesToProcess() as $errorType) {
            $errors |= $errorType;
        }

        if ($errors && isset($error['type'])) {
            $e = new \ErrorException(
                @$error['message'], @$error['type'], @$error['type'],
                @$error['file'], @$error['line']
            );
            $this->handleException($e, true);
        }
    }

    /**
     * @param int $severity
     * @return string
     */
    protected function mapSeverity($severity)
    {
        if (PHP_VERSION_ID >= 50300) {
            switch ($severity) {
                case E_DEPRECATED:
                case E_USER_DEPRECATED:
                    return self::WARNING;
            }
        }

        switch ($severity) {
            case E_COMPILE_ERROR:
            case E_CORE_ERROR:
            case E_ERROR:
            case E_PARSE:
            case E_RECOVERABLE_ERROR:
            case E_USER_ERROR:
            default:
                return self::ERROR;

            case E_COMPILE_WARNING:
            case E_CORE_WARNING:
            case E_USER_WARNING:
            case E_WARNING:
                return self::WARNING;

            case E_NOTICE:
            case E_STRICT:
            case E_USER_NOTICE:
                return self::INFO;
        }
    }

    /**
     * @param string $environment
     */
    public function initEnvironment($environment)
    {
        switch ($environment) {
            case Application::ENV_PRODUCTION:
            case Application::ENV_STAGING:
                ini_set('display_errors', 0);
                ini_set('display_startup_errors', 0);
                error_reporting(E_ALL & ~E_NOTICE);
                break;

            default :
            case Application::ENV_DEVELOPMENT:
            case Application::ENV_TEST:
                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);
                break;

        }

        $bit = ini_get('error_reporting');
        while ($bit > 0) {
            for ($i = 0, $n = 0; $i <= $bit; $i = 1 * (2 ** $n), $n++) {
                $end = $i;
            }

            if (null !== $end) {
                $this->logSeverities[] = $end;
                $bit -= $end;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addLogger(LoggerInterface $logger)
    {
        $this->loggers[] = $logger;
    }

    /**
     * @param \Exception $exception
     * @param bool $isError
     * @param array $vars
     */
    protected function logException($exception, $isError = false, array $vars = null)
    {
        if (method_exists($exception, 'getSeverity')) {
            $severity = $exception->getSeverity();
            if (!\in_array($severity, $this->logSeverities, true)) {
                return;
            }

            if ($vars === null) {
                $vars = [];
            }

            $vars['type'] = $this->mapSeverity($severity);
        }

        foreach ($this->loggers as $logger) {
            $logger->log($exception, $isError, $vars);
        }
    }

    /**
     * @return array
     */
    protected function getErrorTypesToProcess()
    {
        return array_unique(array_merge($this->defaultErrorTypes, $this->validErrorTypes));
    }
}
