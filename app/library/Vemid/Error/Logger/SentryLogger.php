<?php

namespace Vemid\Error\Logger;

use Vemid\Error\LoggerInterface;

/**
 * Class SentryLogger
 *
 * @package Vemid\Error\Logger
 * @author Vemid
 */
class SentryLogger implements LoggerInterface
{
    /** @var \Raven_ErrorHandler */
    private $raven;

    /**
     * @param \Raven_ErrorHandler $raven
     */
    public function __construct(\Raven_ErrorHandler $raven)
    {
        $this->raven = $raven;
    }

    /**
     * {@inheritdoc}
     */
    public function log($e, $isError = false, array $vars = null)
    {
        $this->raven->handleException($e, $isError, $vars);
    }
}
