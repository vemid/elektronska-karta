<?php

namespace Vemid\Error\Logger;

use Phalcon\Logger\Adapter\File;
use Phalcon\Logger\Formatter\Line;
use Vemid\Error\Handler;
use Vemid\Error\LoggerInterface;

/**
 * Class FileLogger
 *
 * @package Vemid\Error\Logger
 * @author Vemid
 */
class FileLogger implements LoggerInterface
{
    /** @var File */
    private $logger;

    /**
     * @param File $logger
     */
    public function __construct(File $logger)
    {
        $this->logger = $logger;
        $this->logger->setFormatter(new Line('[%date%] %message%'));
    }

    /**
     * {@inheritdoc}
     */
    public function log($e, $isError = false, array $vars = null)
    {
        $type = Handler::ERROR;
        if (\is_array($vars) && isset($vars['type'])) {
            $type = $vars['type'];
        }

        $this->logger->log($type, sprintf('[%s] %s in %s on line %s',
            $type, $e->getMessage(), $e->getFile(), $e->getLine()
        ));
    }
}
