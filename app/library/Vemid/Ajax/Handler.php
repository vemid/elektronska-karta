<?php

namespace Vemid\Ajax;

use Vemid\Application\Di;
use Vemid\Messenger\Manager as MessengerManager;
use Vemid\Messenger\Message as MessengerMessage;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Vemid\Messenger\Message;

/**
 * Class Handler
 *
 * @package Vemid\Ajax
 * @author Vemid
 */
class Handler extends Plugin
{

    /**
     * @param Dispatcher $dispatcher
     * @return Response
     */
    public function handle(Dispatcher $dispatcher)
    {
        /** @var Di $di */
        $di = $this->getDI();

        /** @var Request $request */
        $request = $di->getShared('request');

        /** @var Response $response */
        $response = $di->getShared('response');
        /** @var MessengerManager $messengerManager */
        $messengerManager = $di->getNotificationManager();
        $flashSession = $di->getFlashSession();

        if ($request->isAjax()) {

            if ($dispatcher->getReturnedValue()) {
                $data = $dispatcher->getReturnedValue();
                foreach ($dispatcher->getParams() as $param => $value) {
                    if (!is_numeric($param)) {
                        $data[$param] = $value;
                    }
                }
            } else {

                $data = [];
                foreach ($dispatcher->getParams() as $param => $value) {
                    if (!is_numeric($param)) {
                        $data[$param] = $value;
                    }
                }

                if ($messengerManager->hasMessages(MessengerMessage::DANGER)) {
                    $data['error'] = true;
                    $data['messages'] = $messengerManager->toArray(MessengerMessage::DANGER);
                } else if ($messengerManager->hasMessages(MessengerMessage::WARNING)) {
                    $data['error'] = true;
                    $data['messages'] = $messengerManager->toArray(MessengerMessage::WARNING);
                } else if ($messengerManager->hasMessages(MessengerMessage::INFO)) {
                    $data['error'] = false;
                    $data['messages'] = $messengerManager->toArray(MessengerMessage::INFO);
                } else if ($messengerManager->hasMessages(MessengerMessage::SUCCESS)) {
                    $data['error'] = false;
                    $data['messages'] = $messengerManager->toArray(MessengerMessage::SUCCESS);
                }
            }

            if (isset($data) && count($data)) {
                $di->getShared('view')->disable();
                if (is_array($data)) {
                    $data = json_encode($data);
                }

                $response->setContentType('application/json', 'UTF-8');
                $response->setContent($data);
                if (!$response->isSent()) {
                    $response->send();
                }
            }
        } else {
            foreach ($messengerManager->getMessages() as $message) {
                if ($message->getType() === Message::SUCCESS) {
                    $flashSession->success($message->getMessage());
                } else if ($message->getType() === Message::DANGER) {
                    $flashSession->error($message->getMessage());
                } else if ($message->getType() === Message::INFO) {
                    $flashSession->notice($message->getMessage());
                } else if ($message->getType() === Message::WARNING) {
                    $flashSession->warning($message->getMessage());
                }
            }
        }
    }

}
