<?php

namespace Vemid\Entity\Behavior;

use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\ModelInterface;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Vemid\Helper\MetaDataHelper;

/**
 * Class CastValuesBehavior
 *
 * @package Vemid\Entity
 * @subpackage Vemid\Entity\Behavior
 * @author Vemid
 */
class CastValuesBehavior extends Behavior implements BehaviorInterface
{
    /**
     * @param string $type
     * @param ModelInterface|AbstractEntity $model
     */
    public function notify($type, ModelInterface $model)
    {
        switch ($type) {
            case 'beforeCreate':
            case 'beforeUpdate':
                $this->castToDatabaseValues($model);
                break;
            case 'postSave':
                $this->castFromDatabaseValues($model, false);
                break;
            case 'postFetch':
                $this->castFromDatabaseValues($model, true);
                break;
        }
    }

    /**
     * Both snapshot data and entity data must be casted using the same principle
     * in order to prevent possible issues with $model->hasChanged() usage
     *
     * @param ModelInterface|AbstractEntity $model
     */
    private function castToDatabaseValues($model)
    {
        $helper = new MetaDataHelper($model->getModelsMetaData());
        foreach ($helper->castToDatabaseValues($model->toArray()) as $property => $value) {
            if (is_bool($value)) {
                $value = intval($value);
            }
            $model->setProperty($property, $value);
        }

        $snapshotData = $model->getSnapshotData();
        if (!empty($snapshotData)) {
            $model->setSnapshotData($helper->castToDatabaseValues($snapshotData));
        }
    }

    /**
     * Both snapshot data and entity data must be casted using the same principle
     * in order to prevent possible issues with $model->hasChanged() usage
     *
     * Properties which are objects must be cloned which will be automatically done with clone $model
     * since it has defined __clone() method
     *
     * @param AbstractEntity $model
     * @param bool $fetched
     */
    private function castFromDatabaseValues($model, $fetched = true)
    {
        $helper = new MetaDataHelper($model->getModelsMetaData());
        foreach ($helper->castFromDatabaseValues($model, $model->toArray()) as $property => $value) {
            $model->setProperty($property, $value);
        }

        if ($fetched === true) {
            $snapshot = clone $model;
            $model->setSnapshotData($snapshot->toArray());
        } else {
            $snapshotData = $model->getSnapshotData();
            if (!empty($snapshotData)) {
                $snapshotData = $helper->castFromDatabaseValues($model, $model->getSnapshotData());
                $model->setSnapshotData($snapshotData);
            }
        }
    }
}