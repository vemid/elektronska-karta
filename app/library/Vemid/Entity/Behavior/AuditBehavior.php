<?php

namespace Vemid\Entity\Behavior;

use Vemid\Helper\MetaDataHelper;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\ModelInterface;

/**
 * Class AuditBehavior
 *
 * @package Arbor\Entity
 * @subpackage Vemid\Entity\Behavior
 * @author Vemid
 */
class AuditBehavior extends Behavior implements BehaviorInterface
{
    /**
     * @param string $type
     * @param ModelInterface $model
     */
    public function notify($type, ModelInterface $model)
    {
        switch ($type) {
            case 'afterCreate':
                $this->logAudit($model, \AuditLog::OPERATION_CREATE);
                break;
            case 'afterUpdate':
                $this->logAudit($model, \AuditLog::OPERATION_UPDATE);
                break;
            case 'afterDelete':
                $this->logAudit($model, \AuditLog::OPERATION_DELETE);
                break;
        }
    }

    /**
     * @param $model
     * @param $operation
     */
    private function logAudit(ModelInterface $model, $operation)
    {
        if ($model instanceof \AuditLog) {
            return;
        }

        if ($model instanceof \User && $operation === \AuditLog::OPERATION_UPDATE) {
            return;
        }

        $helper = new MetaDataHelper($model->getModelsMetaData());
        $oldData = $helper->castToDatabaseValues($model->getSnapshotData());
        $newData = $helper->castToDatabaseValues($model->toArray());

        if ($operation === \AuditLog::OPERATION_UPDATE && $oldData === $newData) {
            return;
        }

        $auditLog = new \AuditLog();
        $auditLog->setModifiedEntityName(\get_class($model));
        $auditLog->setModifiedEntityId($oldData[$model->getIdentityField()]);
        $auditLog->setOperation($operation);
        $auditLog->setOldData($oldData);
        $auditLog->setNewData($newData);
        $auditLog->save();
    }
}