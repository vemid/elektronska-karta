<?php

namespace Vemid\Entity\Behavior;

use Vemid\Application;
use Vemid\Date\DateTime;
use Vemid\Date\Time;
use Vemid\Entity\AbstractEntity;
use Vemid\Entity\EntityInterface;
use Phalcon\Db\Column;
use Phalcon\Mvc\ModelInterface;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;

/**
 * Class DateCastBehavior
 *
 * @package Vemid\Entity
 * @subpackage Vemid\Entity\Behavior
 * @author Vemid
 */
class DateCastBehavior extends Behavior implements BehaviorInterface
{

    /**
     * This method receives the notifications from the EventsManager
     *
     * @param string $type
     * @param ModelInterface $model
     */
    public function notify($type, ModelInterface $model)
    {
        switch ($type) {

            case 'beforeSave':
                $this->beforeSave($model);
                break;

            case 'afterSave':
                $this->afterSave($model);
                break;

            case 'postFetch':
                $this->castAllMySQLDatesToPhpDateTimes($model);
                break;
        }
    }

    /**
     * Ensure that all properties which are instance of DateTime object are saved in GMT timezone
     *
     * @param AbstractEntity|EntityInterface|ModelInterface $model
     */
    private function beforeSave($model)
    {
        if ($modelMetaData = $model->getModelsMetaData()) {
            $columnMap = $modelMetaData->readColumnMap($model);

            foreach ($modelMetaData->getDataTypes($model) as $modelProperty => $dataType) {
                $property = $columnMap[0][$modelProperty];
                if ($dataType == Column::TYPE_DATETIME || $dataType == Column::TYPE_DATE) {
                    if ($valueBeforeSave = $model->getProperty($property)) {
                        if ($valueBeforeSave instanceof DateTime) {
                            $model->setProperty($property, $valueBeforeSave->getGmtDateTime()->getUnixFormat());
                        } else {
                            if (!is_object($valueBeforeSave) && strtotime($valueBeforeSave) && 1 === preg_match('/[0-9]/', $valueBeforeSave)) {
                                $valueBeforeSave = new DateTime($valueBeforeSave);
                                $model->setProperty($property, $valueBeforeSave->getGmtDateTime()->getUnixFormat());
                            } else {
                                $model->setProperty($property, null);
                            }
                        }
                    } else {
                        $model->setProperty($property, null);
                    }
                }

                $valueBeforeSave = $model->getProperty($property);
                if ($valueBeforeSave instanceof Time) {
                    $model->setProperty($property, $valueBeforeSave->getUnixFormat());
                }
            }
        }
    }

    /**
     * @param AbstractEntity|EntityInterface|ModelInterface $model
     */
    private function afterSave($model)
    {
        if ($modelMetaData = $model->getModelsMetaData()) {
            $columnMap = $modelMetaData->readColumnMap($model);

            foreach ($modelMetaData->getDataTypes($model) as $modelProperty => $dataType) {

                if ($dataType == Column::TYPE_DATETIME || $dataType == Column::TYPE_DATE) {
                    $property = $columnMap[0][$modelProperty];
                    if ($date = $model->getProperty($property)) {
                        if ($dataType == Column::TYPE_DATE) {
                            $date = new DateTime($date, null, DateTime::TYPE_DATE);
                        } else {
                            $date = new DateTime($date, null, DateTime::TYPE_DATETIME);
                        }
                        $model->setProperty($property, $date);
                    } else {
                        $model->setProperty($property, null);
                    }
                }
            }
        }
    }

    /**
     * @param AbstractEntity|EntityInterface|ModelInterface $model
     */
    private function castAllMySQLDatesToPhpDateTimes($model)
    {
        if ($modelMetaData = $model->getModelsMetaData()) {
            $columnMap = $modelMetaData->readColumnMap($model);

            foreach ($modelMetaData->getDataTypes($model) as $modelProperty => $dataType) {

                if ($dataType == Column::TYPE_DATETIME || $dataType == Column::TYPE_DATE) {
                    $property = $columnMap[0][$modelProperty];
                    if ($date = $model->getProperty($property)) {
                        if ($dataType == Column::TYPE_DATE) {
                            $date = new DateTime($date, new \DateTimeZone(Application::DEFAULT_TIMEZONE), DateTime::TYPE_DATE);
                        } else {
                            $date = new DateTime($date, new \DateTimeZone(Application::DEFAULT_TIMEZONE), DateTime::TYPE_DATETIME);
                        }
                        $date->setTimezone(new \DateTimeZone(date_default_timezone_get()));
                        $model->setProperty($property, $date);
                    } else {
                        $model->setProperty($property, null);
                    }
                }

            }
        }
    }

}
