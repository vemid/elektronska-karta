<?php

namespace Vemid\Entity;

use Phalcon\Mvc\Model\Resultset;

/**
 * Class Type
 *
 * @package Vemid\Entity
 * @author Vemid
 */
class Type
{
    const ABSENCE_WORK = 57;
    const ATTRIBUTE = 1;
    const AUDIT_LOG = 2;
    const CALCULATION_MAPPING = 56;
    const CLASSIFICATION_OLD_COLLECTION_MAP = 23;
    const CLASSIFICATION_SIZE = 24;
    const CLASSIFICATION = 3;
    const CLIENT = 9;
    const CHANNEL_SUPPLY = 61;
    const CHANNEL_SUPPLY_ENTITY = 62;
    const CODE_TYPE = 4;
    const CODE = 5;
    const COMMENT = 55;
    const CUTTING_JOB = 53;
    const CUTTING_JOB_WORKSHEET = 54;
    const DEALERSHIP_WARRANT = 51;
    const DEALERSHIP = 52;
    const DELIVERY_NOTE = 44;
    const DELIVERY_NOTE_ITEM = 45;
    const EFECTUS = 66;
    const ELECTRON_CARD_FACTORY = 21;
    const ELECTRON_CARD = 22;
    const EMAIL_NOTIFICATION_TEMPLATE = 58;
    const EMAIL_TEMPLATE_RECEIVER = 59;
    const MATERIAL_FACTORY_QUANTITY = 6;
    const MATERIAL_FACTORY = 7;
    const MATERIAL_SAMPLE = 8;
    const MATERIAL_SKETCH = 60;
    const NOTIFICATION_TYPE = 16;
    const ORDER = 17;
    const ORDER_TOTAL = 35;
    const ORDER_CLASSIFICATION = 29;
    const ORDER_ITEM = 28;
    const OPERATING_LIST_ITEM_TYPE = 30;
    const OPERATING_LIST_ITEM_WORKSHEET = 31;
    const OPERATING_LIST_ITEM = 32;
    const OPERATING_LIST = 33;
    const OUTBOUND_EMAIL = 34;
    const QUESTIONNAIRE = 25;
    const QUESTION = 26;
    const PRICELIST = 42;
    const PASSWORD_RESET = 50;
    const PRIORITY = 49;
    const PRIORITY_CLASSIFICATION = 48;
    const PRODUCT_CLASSIFICATION = 10;
    const PRODUCTION_MACHINE = 47;
    const PRODUCTION_WORKER = 36;
    const PRODUCTION_WORKER_CHECKIN = 46;
    const PRODUCTION_ORDER = 37;
    const PRODUCT_PRICE = 43;
    const PRODUCT = 11;
    const PRODUCT_CALCULATION_ITEM = 41;
    const PRODUCT_CALCULATION = 40;
    const PRODUCT_SIZE = 18;
    const SHIPMENT_STATUS = 65;
    const ROLE = 12;
    const SUPPLIER = 13;
    const USER_SECTION = 27;
    const USER_NOTIFICATION_MESSAGE = 20;
    const USER_NOTIFICATION = 19;
    const USER_ROLE_ASSIGNMENT = 14;
    const USER = 15;
    const WAREHOUSE_ORDER = 63;
    const WAREHOUSE_ORDER_ITEM = 64;
    const WARRANT_STATEMENT_ITEM = 38;
    const WARRANT_STATEMENT = 39;

    /*
     * Each Model should have its own unique 'Entity number'
     * When adding new Model, it should have defined 'Entity number'
     * Use first available value
     */

    private static $objectNames = [
        self::ABSENCE_WORK => 'AbsenceWork',
        self::ATTRIBUTE => 'Attribute',
        self::AUDIT_LOG => 'AuditLog',
        self::CALCULATION_MAPPING => 'CalculationMapping',
        self::CLASSIFICATION_OLD_COLLECTION_MAP => 'ClassificationOldCollectionMap',
        self::CLASSIFICATION_SIZE => 'ClassificationSize',
        self::CLASSIFICATION => 'Classification',
        self::CLIENT => 'Client',
        self::CHANNEL_SUPPLY => 'ChannelSupply',
        self::CHANNEL_SUPPLY_ENTITY => 'ChannelSupplyEntity',
        self::CODE_TYPE => 'CodeType',
        self::CODE => 'Code',
        self::COMMENT => 'Comment',
        self::CUTTING_JOB_WORKSHEET => 'CuttingJobWorksheet',
        self::CUTTING_JOB => 'CuttingJob',
        self::DEALERSHIP_WARRANT => 'DealershipWarrant',
        self::DEALERSHIP => 'Dealership',
        self::DELIVERY_NOTE => 'DeliveryNote',
        self::DELIVERY_NOTE_ITEM => 'DeliveryNoteItem',
        self::EFECTUS => 'Efectus',
        self::ELECTRON_CARD_FACTORY => 'ElectronCardFactory',
        self::ELECTRON_CARD => 'ElectronCard',
        self::EMAIL_NOTIFICATION_TEMPLATE => 'EmailNotificationTemplate',
        self::EMAIL_TEMPLATE_RECEIVER => 'EmailTemplateReceiver',
        self::MATERIAL_FACTORY_QUANTITY => 'MaterialFactoryQuantity',
        self::MATERIAL_FACTORY => 'MaterialFactory',
        self::MATERIAL_SAMPLE => 'MaterialSample',
        self::MATERIAL_SKETCH => 'MaterialSketch',
        self::NOTIFICATION_TYPE => 'NotificationType',
        self::OPERATING_LIST_ITEM_TYPE => 'OperatingListItemType',
        self::OPERATING_LIST_ITEM_WORKSHEET => 'OperatingListItemWorksheet',
        self::OPERATING_LIST_ITEM => 'OperatingListItem',
        self::OPERATING_LIST => 'OperatingList',
        self::ORDER => 'Order',
        self::ORDER_TOTAL => 'OrderTotal',
        self::ORDER_CLASSIFICATION => 'OrderClassification',
        self::ORDER_ITEM => 'OrderItem',
        self::OUTBOUND_EMAIL => 'OutboundEmail',
        self::QUESTIONNAIRE => 'Questionnaire',
        self::QUESTION => 'Question',
        self::PASSWORD_RESET => 'PasswordReset',
        self::PRICELIST => 'Pricelist',
        self::PRIORITY => 'Priority',
        self::PRIORITY_CLASSIFICATION => 'PriorityClassification',
        self::PRODUCTION_WORKER => 'ProductionWorker',
        self::PRODUCTION_WORKER_CHECKIN => 'ProductionWorkerCheckin',
        self::PRODUCTION_ORDER => 'ProductionOrder',
        self::PRODUCT_SIZE => 'ProductSize',
        self::PRODUCT_PRICE => 'ProductPrice',
        self::PRODUCT => 'Product',
        self::PRODUCT_CALCULATION_ITEM => 'ProductCalculationItem',
        self::PRODUCT_CALCULATION => 'ProductCalculation',
        self::PRODUCT_CLASSIFICATION => 'ProductClassification',
        self::PRODUCTION_MACHINE => 'ProductionMachine',
        self::SHIPMENT_STATUS => 'ShipmentStatus',
        self::ROLE => 'Role',
        self::SUPPLIER => 'Supplier',
        self::USER_NOTIFICATION_MESSAGE => 'UserNotificationMessage',
        self::USER_NOTIFICATION => 'UserNotification',
        self::USER_ROLE_ASSIGNMENT => 'UserRoleAssignment',
        self::USER_SECTION => 'UserSector',
        self::USER => 'User',
        self::WAREHOUSE_ORDER => 'WarehouseOrder',
        self::WAREHOUSE_ORDER_ITEM => 'WarehouseOrderItem',
        self::WARRANT_STATEMENT_ITEM => 'WarrantStatementItem',
        self::WARRANT_STATEMENT => 'WarrantStatement',
    ];

    /**
     * @return array
     */
    public static function getObjectNames()
    {
        return self::$objectNames;
    }

    /**
     * Get Model convenience method
     * Returns a model instance
     *
     * @param int $objectTypeId
     * @param int $objectId
     * @return null|EntityInterface
     */
    public static function getEntity($objectTypeId, $objectId)
    {
        if (array_key_exists($objectTypeId, self::$objectNames)) {
            $entityName = '\\' . self::$objectNames[$objectTypeId];

            if (class_exists($entityName)) {
                if (\is_callable(array($entityName, 'findFirst'))) {
                    $conditions = array('id = :id:', 'bind' => array('id' => $objectId));
                    return \call_user_func(array($entityName, 'findFirst'), $conditions);
                }
            }
        }

        return null;
    }

    /**
     * @param int $objectTypeId
     * @return Resultset|EntityInterface[]|null
     */
    public static function getEntities($objectTypeId)
    {
        if (array_key_exists($objectTypeId, self::$objectNames)) {
            $entityName = '\\' . self::$objectNames[$objectTypeId];
            if (class_exists($entityName)) {
                if (\is_callable(array($entityName, 'find'))) {
                    return \call_user_func(array($entityName, 'find'));
                }
            }
        }

        return null;
    }

    /**
     * @param $entityType
     * @return bool
     */
    public static function hasEntityType($entityType)
    {
        if (\is_int($entityType) && array_key_exists($entityType, self::$objectNames)) {
            $entityType = self::getObjectName($entityType);
        }

        if (strtoupper($entityType) === $entityType) {
            $entityType = ucfirst(strtolower($entityType));
        }

        return \in_array($entityType, self::$objectNames, true);
    }

    /**
     * @param int $entityType
     * @return string|null
     */
    public static function getObjectName($entityType)
    {
        if (array_key_exists($entityType, self::$objectNames)) {
            return self::$objectNames[$entityType];
        }

        return null;
    }

    /**
     * @param string $objectName
     * @return int|null
     */
    public static function getEntityType($objectName)
    {
        if (\in_array($objectName, self::$objectNames, true)) {
            return array_search($objectName, self::$objectNames, false);
        }

        return null;
    }

    /**
     * @param $index
     * @return EntityInterface|null
     */
    public static function getEntityByIndex($index)
    {
        list($entityTypeId, $entityId) = explode('__', $index);

        return self::getEntity($entityTypeId, $entityId);
    }

}
