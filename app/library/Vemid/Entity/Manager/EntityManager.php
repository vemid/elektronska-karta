<?php

namespace Vemid\Entity\Manager;

use Vemid\Entity\EntityInterface;
use Vemid\Entity\Repository\EntityRepositoryInterface;
use Phalcon\Mvc\Model\ManagerInterface;
use Phalcon\Mvc\Model\Relation;
use Vemid\Entity\Type;

/**
 * Class EntityManager
 *
 * @package Vemid\Entity
 * @author Vemid
 */
class EntityManager implements EntityManagerInterface
{

    /** @var ManagerInterface */
    private $modelManager;

    /** @var EntityRepositoryInterface[] */
    protected $repositories = [];

    /** @var EntityInterface[] */
    private $identityMap = [];

    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        $this->modelManager = $modelManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository($entityName)
    {
        $entityName = explode('\\', $entityName);
        $entityName = end($entityName);

        if (!array_key_exists($entityName, $this->repositories)) {
            $className = "\\Vemid\\Entity\\Repository\\{$entityName}Repository";
            if (!class_exists($className)) {
                $className = "\\Vemid\\Entity\\Repository\\EntityRepository";
            }
            $this->repositories[$entityName] = new $className($this->modelManager, $entityName);
        }

        return $this->repositories[$entityName];
    }

    /**
     * {@inheritdoc}
     */
    public function findOne($entityName, $params)
    {
        return $this->getRepository($entityName)->findOne($params);
    }

    /**
     * {@inheritdoc}
     */
    public function find($entityName, $params = null)
    {
        return $this->getRepository($entityName)->find($params);
    }

    /**
     * {@inheritdoc}
     */
    public function getRelated(EntityInterface $entity, $alias, $arguments = null)
    {
        $className = get_class($entity);
        $relation = $this->modelManager->getRelationByAlias($className, $alias);

        if (!is_object($relation)) {
            throw new \DomainException(sprintf(
                "There is no defined relations for the model '%s' using alias '%s'",
                $className, $alias
            ));
        }

        if ($checkIdentityMap = in_array($relation->getType(), [Relation::BELONGS_TO, Relation::HAS_ONE], true)) {
            $fieldName = (string)$relation->getFields();
            $referencedModelName = $relation->getReferencedModel();
            $referencedModelId = $entity->getProperty($fieldName);

            if ($relatedEntity = $this->getEntityFromIdentityMap($referencedModelName, $referencedModelId)) {
                return $relatedEntity;
            }
        }

        $relationRecords = $this->getRepository($className)->getRelated($entity, $alias, $arguments);

        if ($relationRecords && $checkIdentityMap) {
            $this->addEntityToIdentityMap($relationRecords);
        }

        return $relationRecords;
    }

    /**
     * {@inheritdoc}
     */
    public function save(EntityInterface $entity)
    {
        return $this->getRepository(get_class($entity))->save($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(EntityInterface $entity)
    {
        return $this->getRepository(get_class($entity))->delete($entity);
    }

    /**
     * @param EntityInterface $entity
     */
    private function addEntityToIdentityMap(EntityInterface $entity)
    {
        if (!array_key_exists($entity->getEntityIdentifier(), $this->identityMap)) {
            $this->identityMap[$entity->getEntityIdentifier()] = $entity;
        }
    }

    /**
     * @param $entityName
     * @param $entityId
     * @return EntityInterface|null
     */
    private function getEntityFromIdentityMap($entityName, $entityId)
    {
        $entityIndex = Type::getEntityType($entityName) . '__' . $entityId;
        if (array_key_exists($entityIndex, $this->identityMap)) {
            return $this->identityMap[$entityIndex];
        }

        return null;
    }

}
