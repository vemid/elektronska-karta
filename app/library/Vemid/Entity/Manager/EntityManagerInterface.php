<?php

namespace Vemid\Entity\Manager;

use Vemid\Entity\AbstractEntity;
use Vemid\Entity\EntityInterface;
use Vemid\Entity\Repository\EntityRepositoryInterface;
use Phalcon\Mvc\Model\Resultset;

/**
 * Interface ManagerInterface
 *
 * @package Vemid\Entity
 * @author Vemid
 */
interface EntityManagerInterface
{

    /**
     * @param string $entityName
     * @return EntityRepositoryInterface
     */
    public function getRepository($entityName);

    /**
     * @param string $entityName
     * @param array|string|int $params
     * @return EntityInterface|null
     */
    public function findOne($entityName, $params);

    /**
     * @param $entityName
     * @param array|string|int|null $params
     * @return EntityInterface[]|Resultset
     */
    public function find($entityName, $params = null);

    /**
     * @param EntityInterface $entity
     * @param string $alias
     * @param array|null $arguments
     * @return Resultset|EntityInterface|null
     * @throws \DomainException
     */
    public function getRelated(EntityInterface $entity, $alias, $arguments = null);

    /**
     * @param EntityInterface $entity
     * @return bool
     */
    public function save(EntityInterface $entity);

    /**
     * @param EntityInterface $entity
     * @return bool
     */
    public function delete(EntityInterface $entity);

}