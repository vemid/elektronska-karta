<?php

namespace Vemid\Entity;

use Vemid\Entity\Repository\EntityRepositoryInterface;
use Vemid\Form\Form;
use Phalcon\Mvc\ModelInterface;

/**
 * Interface EntityInterface
 *
 * @package Vemid\Entity
 * @author Vemid
 */
interface EntityInterface extends ModelInterface
{
    /**
     * Gets Entity Class name
     *
     * @return string
     */
    public function getEntityName();


    /**
     * Gets Entity ID (value of primary key)
     *
     * @return int
     */
    public function getEntityId();

    /**
     * @return int
     */
    public function getEntityTypeId();


    /**
     * Generates unique index for this Entity
     *
     * @return string
     */
    public function getEntityIdentifier();

    /**
     * Gets name of primary key
     *
     * @return string
     */
    public function getIdentityField();

    /**
     * Tests if the object is equal (object type id and object id are the same)
     *
     * @param EntityInterface $entity
     * @return bool
     */
    public function isEqualToEntity(EntityInterface $entity);

    /**
     * @return string
     */
    public function getDisplayName();

    /**
     * @param string $fieldName
     * @return bool
     */
    public function isUpdateable($fieldName);

    /**
     * @return bool
     */
    public function isDeletable();

    /**
     * @param array $exclude
     * @return Form
     */
    public function getForm(array $exclude = []);

    /**
     * @param string $property
     * @return mixed
     */
    public function getProperty($property);

    /**
     * @param string $property
     * @param string|int|null $value
     * @return $this
     */
    public function setProperty($property, $value);

    /**
     * @return EntityRepositoryInterface
     */
    public function getRepository();

    /**
     * @param null|array $columns
     * @return array
     */
    public function toArray($columns = null);
}
