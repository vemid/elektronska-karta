<?php

namespace Vemid\Entity;

use Vemid\Application\Di;
use Vemid\Entity\Behavior\AuditBehavior;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Form\Builder\Annotations as FormBuilder;
use Vemid\Form\Form;
use Vemid\Filter\CamelCase;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Mvc\Model;
use Phalcon\Translate\AdapterInterface as TranslatorInterface;
use Vemid\Entity\Behavior\CastValuesBehavior;

/**
 * Class AbstractEntity
 *
 * @method Di getDI()
 *
 * @package Vemid\Entity
 * @author Vemid
 */
abstract class AbstractEntity extends Model implements EntityInterface
{

    protected $identityField = 'id';
    protected $restDefinition = [];

    /**
     * {@inheritdoc}
     */
    public function getEntityName()
    {
        $dummy = explode('\\', static::class);

        return end($dummy);
    }

    /**
     * @deprecated Please use entity manager to retrieve single entity
     *
     * @param null|string|array $parameters
     * @return $this|null
     */
    public static function findFirst($parameters = null)
    {
        if (is_null($parameters)) {
            return null;
        }

        /** @var self $entity */
        if ($entity = parent::findFirst($parameters)) {
            return $entity;
        }

        return null;
    }

    /**
     * @deprecated Please use entity manager to retrieve collection of entities
     *
     * @param null|string|array $parameters
     * @return Resultset|$this[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * @param string $alias
     * @param null|string|int|array $arguments
     * @return EntityInterface|null|Resultset
     * @throws \DomainException
     */
    public function getRelated($alias, $arguments = null)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->getShared('entityManager');

        return $entityManager->getRelated($this, $alias, $arguments);
    }

    /**
     * Gets property value
     *
     * @param string $property
     * @return mixed
     */
    public function getProperty($property)
    {
        $method = 'get' . ucfirst($property);
        if (method_exists($this, $method)) {
            return call_user_func([$this, $method]);
        }

        return null;
    }

    /**
     * @param string $property
     * @param mixed $value
     * @return $this
     */
    public function setProperty($property, $value)
    {
        $method = 'set' . ucfirst($property);
        if (method_exists($this, $method)) {
            if (0 === strlen($value)) {
                $value = null;
            }

            call_user_func_array([$this, $method], [$value]);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityTypeId()
    {
        return Type::getEntityType(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityId()
    {
        return $this->getProperty($this->identityField);
    }

    /**
     * {@inheritdoc}
     */
    public function getObjectTypeId()
    {
        return Type::getEntityType(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function getObjectId()
    {
        return $this->getProperty($this->_objectId);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityIdentifier()
    {
        return $this->getEntityTypeId() . '__' . ($this->getEntityId() ?: 0);
    }

    /**
     * {@inheritdoc}
     */
    public function getObjectIndex()
    {
        return $this->getEntityTypeId() . '__' . ($this->getEntityId() ?: 0);
    }

    /**
     * {@inheritDoc}
     */
    public function getIdentityField()
    {
        return $this->identityField;
    }


    /**
     * {@inheritdoc}
     */
    public function isEntity($entity)
    {
        return Type::getEntityType($entity) === $this->getEntityTypeId();
    }


    /**
     * {@inheritdoc}
     */
    public function isEqualToEntity(EntityInterface $entity = null)
    {
        if (is_object($entity)) {
            return $this->getEntityIdentifier() === $entity->getEntityIdentifier();
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getDisplayName()
    {
        return sprintf('%s %d', get_called_class(), $this->getEntityId());
    }

    /**
     * @param array $exclude
     * @return Form
     */
    public function getForm(array $exclude = [])
    {
        /** @var TranslatorInterface $translator */
        $translator = $this->getDI()->getShared('translator');

        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->getShared('entityManager');

        $builder = new FormBuilder($entityManager, $translator);

        return $builder->build($this, $exclude);
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isUpdateable($fieldName)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepository($repositoryName = null)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->getDI()->getShared('entityManager');

        return $entityManager->getRepository($repositoryName ?: get_called_class());
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getDisplayName();
    }

    /**
     *
     */
    public function __clone()
    {
        $properties = $this->toArray();

        foreach ($properties as $key => $value) {
            if (is_object($value)) {
                $this->setProperty($key, clone $value);
            }
        }
    }

    /**
     * Initializer
     */
    public function initialize()
    {
        $this->addBehavior(new CastValuesBehavior());
        // Enable when we there is a need for audit log
        $this->addBehavior(new AuditBehavior());
        $this->keepSnapshots(true);
        $this->useDynamicUpdate(true);
    }

    /**
     *
     */
    public function afterFetch()
    {
        $this->fireEvent('postFetch');
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (!$this->isDeletable()) {
            return false;
        }

        $camelCase = new CamelCase();
        $modelManager = $this->getModelsManager();
        $hasMany = $modelManager->getHasMany($this);
        $hasOne = $modelManager->getHasOne($this);
        $relations = array_merge($hasOne, $hasMany);

        /** @var \Phalcon\Mvc\Model\Relation $relation */
        foreach ($relations as $relation) {
            if (count($relation->getOptions()) && isset($relation->getOptions()['alias'])) {
                $relatedModel = $relation->getOptions()['alias'];
            } else {
                $relatedModel = $relation->getReferencedModel();
            }

            if (!$instantiatedRelatedModel = $this->getRelated($relatedModel)) {
                continue;
            }

            if ($instantiatedRelatedModel instanceof \Countable) {
                $referencedField = $relation->getReferencedFields();

                /** @var EntityInterface[] $instantiatedRelatedModel */
                foreach ($instantiatedRelatedModel as $model) {
                    $notNull = $this->getModelsMetaData()->getNotNullAttributes($model);
                    array_walk($notNull, function (&$value) use ($camelCase) {
                        $value = $camelCase->filter($value);
                    });

                    if (in_array($referencedField, $notNull)) {
                        $model->delete();
                    } else {
                        $model->setProperty($referencedField, null);
                        $model->save();
                    }
                }
            } else {
                /** @var EntityInterface $instantiatedRelatedModel */
                if ($instantiatedRelatedModel) {
                    $referencedField = $relation->getReferencedFields();
                    $notNull = $this->getModelsMetaData()->getNotNullAttributes($instantiatedRelatedModel);
                    array_walk($notNull, function (&$value) use ($camelCase) {
                        $value = $camelCase->filter($value);
                    });

                    if (in_array($referencedField, $notNull)) {
                        $instantiatedRelatedModel->delete();
                    } else {
                        $instantiatedRelatedModel->setProperty($referencedField, null);
                        $instantiatedRelatedModel->save();
                    }
                }
            }
        }

        return true;
    }

    /**
     * @return null|EntityInterface|\AuditLog
     */
    public function getEntityLogOnCreate()
    {
        return $this->getDI()->getEntityManager()->findOne(\AuditLog::class, [
            'modifiedEntityName = :modifiedEntityName: AND modifiedEntityId = :modifiedEntityId: AND operation = :operation:',
            'bind' => [
                'modifiedEntityName' => $this->getEntityName(),
                'modifiedEntityId' => $this->getEntityId(),
                'operation' => \AuditLog::OPERATION_CREATE
            ]
        ]);
    }

    /**
     * @return null|EntityInterface|\AuditLog
     */
    public function getEntityLogOnUpdate()
    {
        return $this->getDI()->getEntityManager()->findOne(\AuditLog::class, [
            'modifiedEntityName = :modifiedEntityName: AND modifiedEntityId = :modifiedEntityId: AND operation = :operation:',
            'bind' => [
                'modifiedEntityName' => $this->getEntityName(),
                'modifiedEntityId' => $this->getEntityId(),
                'operation' => \AuditLog::OPERATION_UPDATE
            ],
            'order' => 'timestamp DESC'
        ]);
    }

    /**
     * @param string $string
     * @param null|int|string|array $args
     * @return string string
     */
    protected function _translate($string, $args = null)
    {
        /** @var TranslatorInterface $translator */
        $translator = $this->getDI()->getShared('translator');

        return $translator->t($string, $args);
    }

    /**
     * After values are fetched from database, they must be casted to proper value
     * If column type is either date or datetime, those properties will be casted as PHP DateTime objects
     *
     * When inserting data into database, properties must be casted to proper SQL values
     * After data are inserted into database, properties must be casted on the same way as after database fetch
     *
     * It's not possible to use afterUpdate or afterSave events,
     * because some models might have defined methods with these names and they will be executed before Behavior
     * so we must fire another event before those two are triggered and to register for that event in Behavior
     * since it's the only way to cast values back to the value which was before database insert
     *
     * @param bool $success
     * @param bool $exists
     * @return bool
     */
    protected function _postSave($success, $exists)
    {
        $this->fireEvent('postSave');

        return parent::_postSave($success, $exists);
    }

}
