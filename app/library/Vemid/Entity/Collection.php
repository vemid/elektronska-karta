<?php

namespace Vemid\Entity;

/**
 * Class Collection
 *
 * @package Vemid\Entity
 * @author Vemid
 */
class Collection extends \ArrayObject
{

    /**
     * @param null|array $input
     * @param int $flags
     * @param string $iteratorClass
     */
    public function __construct($input = null, $flags = 0, $iteratorClass = 'ArrayIterator')
    {
        $this->setFlags($flags);
        $this->setIteratorClass($iteratorClass);

        if ($input) {
            foreach ($input as $item) {
                $this->append($item);
            }
        }
    }

    /**
     * @param array $criteriaQuantifiers
     * Associative array mapping EntityTypeIDs to callables. the callable needs
     * to accept a single collection element and return a value which will be
     * used to sort this element.
     *
     * @throws \RuntimeException
     * @return void
     */
    public function usort($criteriaQuantifiers)
    {
        $stagingArray = array();

        /** @var EntityInterface $element */
        foreach ($this as $element) {
            if (!array_key_exists($element->getEntityTypeId(), $criteriaQuantifiers) && !isset($criteriaQuantifiers['*'])) {
                throw new \InvalidArgumentException('Invalid Sort criteria quantifier. ' .
                    'The quantifier is missing a definition for Entity Type: '
                    . $element->getEntityTypeId());
            }

            $callback = (isset($criteriaQuantifiers[$element->getEntityTypeId()])
                ? $criteriaQuantifiers[$element->getEntityTypeId()]
                : $criteriaQuantifiers['*']
            );

            $quantifierVal = call_user_func($callback, $element);
            $stagingArray[$quantifierVal][] = $element;
        }

        // Sort by $quantifierVal
        ksort($stagingArray, SORT_NATURAL | SORT_FLAG_CASE);

        // Clear data and import from staging array
        $this->exchangeArray([]);
        foreach ($stagingArray as $quantifierVal => $entities) {
            foreach ($entities as $entity) {
                $this->append($entity);
            }
        }
    }

    /**
     * @return array An array of object type id's which are present in this collection
     */
    public function getEntityTypes()
    {
        $entityTypes = [];

        /** @var $element EntityInterface */
        foreach ($this as $element) {
            $entityTypes[$element->getEntityTypeId()] = $element->getEntityTypeId();
        }

        return array_keys($entityTypes);
    }

    /**
     * @param string $index
     * @param EntityInterface $entity
     * @throws \InvalidArgumentException
     */
    public function offsetSet($index, $entity)
    {
        if (!$entity instanceof EntityInterface) {
            throw new \InvalidArgumentException(
                'Only objects implementing the EntityInterface or objects of class can be added to the Collection'
            );
        }

        if ($index === null) {
            $index = $entity->getEntityIdentifier();
        }

        parent::offsetSet($index, $entity);
    }


    /**
     * This is equivalent to current($collection) EXCEPT that it returns NULL not FALSE.
     * Do not use current() with Collections.
     *
     * @return EntityInterface|null
     */
    public function getFirstEntity()
    {
        foreach ($this as $element) {
            return $element;
        }

        return null;
    }

    /**
     * Gets the last entity in the collection
     *
     * @return EntityInterface|null
     */
    public function getLastEntity()
    {
        if ($this->count() > 0) {
            $arrayCopy = $this->getArrayCopy();

            return end($arrayCopy);
        }

        return null;
    }

    /**
     * Gets a random member of the collection. Useful for data generation.
     *
     * @return EntityInterface|null
     */
    public function getRandomEntity()
    {
        if ($this->count() > 0) {
            $arrayCopy = $this->getArrayCopy();

            return $arrayCopy[array_rand($arrayCopy)];
        }

        return null;
    }

    /**
     * @param EntityInterface $value
     */
    public function append($value)
    {
        $this->offsetSet(null, $value);
    }

    /**
     * Checks if the Collection contains the object.
     *
     * @param EntityInterface $entity
     * @return bool
     */
    public function contains(EntityInterface $entity)
    {
        return $this->offsetExists($entity->getEntityIdentifier());
    }

    /**
     * Merge $collection into this collection.
     *
     * @param Collection $collection
     * @return $this
     */
    public function mergeCollection(Collection $collection)
    {
        foreach ($collection as $entity) {
            $this->append($entity);
        }

        return $this;
    }

    /**
     * The method excludes (removes) items from $excludeCollection from this collection.
     * The collection returned is NOT a copy of the original collection with the items from $excludeCollection removed,
     * but the original collection itself.
     *
     * @param Collection $excludeCollection
     * @return $this
     */
    public function excludeCollection(Collection $excludeCollection)
    {
        foreach ($excludeCollection as $entity) {
            $this->remove($entity);
        }

        return $this;
    }

    /**
     * @param EntityInterface $object
     */
    public function remove(EntityInterface $object)
    {
        if (!is_null($object) && $this->offsetExists($object->getEntityIdentifier())) {
            $this->offsetUnset($object->getEntityIdentifier());
        }
    }

    /**
     * Intersect this collection with the collection of the argument
     * Returns a new entity collection so the called entity collection remains intact
     *
     * @param Collection $collectionToIntersectWith
     * @return Collection
     */
    public function intersectWithCollection(Collection $collectionToIntersectWith)
    {
        $intersectedCollection = new Collection();

        foreach ($this as $entity) {
            if ($collectionToIntersectWith->contains($entity)) {
                $intersectedCollection->append($entity);
            }
        }

        return $intersectedCollection;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return implode(', ', $this->toArray());
    }


    /**
     * Reverses this collection.
     *
     * @return $this
     */
    public function reverse()
    {
        $arrayCopy = $this->getArrayCopy();
        $this->exchangeArray(array_reverse($arrayCopy, true));

        return $this;
    }

    /**
     * Returns TRUE if $collection2 contains identical elements to this collection
     *
     * @param Collection $collection2
     * @return bool
     */
    public function equals(Collection $collection2)
    {
        $collection1 = $this->getArrayCopy();
        $collection2 = $collection2->getArrayCopy();

        if (count(array_diff_key($collection1, $collection2)) == 0 && count($collection1) == count($collection2)) {
            return true;
        }

        return false;
    }

    /**
     * Returns the object for the entity type and ID if it exists in the collection
     *
     * @param string $entityType
     * @param int $id
     * @return EntityInterface
     */
    public function getEntity($entityType, $id)
    {
        $key = $entityType . '__' . $id;

        if ($this->offsetExists($key)) {
            return $this->offsetGet($key);
        }

        return null;
    }

    /**
     * @return Collection
     */
    public function sortByDisplayName()
    {
        $sortQuantifier = [
            '*' => function (EntityInterface $model) {
                return $model->getDisplayName();
            }
        ];

        $this->usort($sortQuantifier);

        return $this;
    }

    /**
     * @param string $entityType
     * @return array
     */
    public function getEntityIds($entityType)
    {
        $objectIds = [];
        foreach ($this as $thisItem) {
            /** @var EntityInterface $thisItem */
            if ($thisItem->getEntityTypeId() === $entityType) {
                $objectIds[$thisItem->getEntityId()] = $thisItem->getEntityId();
            }
        }

        return $objectIds;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = [];

        foreach ($this as $thisItem) {
            /** @var EntityInterface $thisItem */
            $array[$thisItem->getEntityIdentifier()] = $thisItem->getDisplayName();
        }

        return $array;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Collection
     */
    public function extract($limit, $offset = 0)
    {
        return new Collection(array_slice($this->getArrayCopy(), $offset, $limit));
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getDisplayName();
    }

}