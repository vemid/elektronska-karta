<?php

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Phalcon\Mvc\Model\Resultset\Simple;

class ProductTagRepository extends EntityRepository
{
    /** @var ManagerInterface */
    private $modelsManager;

    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        $this->modelsManager = $modelManager;

        parent::__construct($modelManager, \ProductTag::class);
    }

    public function getOrderTags($order,array $selected = [])
    {
        $notIn = sprintf("'%s', '%s', '%s', '%s', '%s'",\ProductTag::SEASON, \ProductTag::BRAND, \ProductTag::SIZE,\ProductTag::COLOR,\ProductTag::COLLECTION);
        $selectedTags = implode("', '", $selected);
        $totalSelected = count($selected);

        $sql = <<<SQL
SELECT pt.* FROM product_tags pt
LEFT JOIN products p ON pt.product_id = p.id
WHERE p.is_orderable = 1
AND pt.category NOT IN ({$notIn})
AND product_id in (
select pc.product_id from product_classifications pc
left join order_classifications oc on oc.classification_id = pc.entity_id
where oc.order_id = {$order->getId()} and pc.entity_type_id = 3
)
SQL;
        if ($totalSelected > 0) {
            $lastValue = array_pop($selected);

            $string = <<<STR
HAVING 
STR;
            foreach ($selected as $tag) {
                $string .= <<<STR
FIND_IN_SET('$tag', GROUP_CONCAT(tag)) > 0 AND 
STR;
            }

            $string .= <<<STR
FIND_IN_SET('$lastValue', GROUP_CONCAT(tag)) > 0
            
STR;

            $sql .= <<<SQL

AND product_id IN (
    SELECT pt2.product_id from product_tags pt2
    LEFT JOIN products p2 ON pt2.product_id = p2.id
    WHERE p2.is_orderable = 1
    GROUP BY pt2.product_id
    {$string}
)
AND product_id in (
select pc.product_id from product_classifications pc
left join order_classifications oc on oc.classification_id = pc.entity_id
where oc.order_id = {$order->getId()} and pc.entity_type_id = 3
)
SQL;
        }

        $sql .= <<<SQL

GROUP BY pt.category, pt.tag
ORDER BY pt.tag
SQL;

        return new Simple(null, new \ProductTag(), $this->modelsManager->getReadConnection(new \ProductTag())->query($sql));
    }

    public function getProductByTags($order,array $selected = [], array $search = null, $limit = null, $offset = null)
    {
        $notIn = sprintf("'%s', '%s', '%s'",\ProductTag::SEASON, \ProductTag::BRAND, \ProductTag::SIZE);
        $selectedTags = implode("', '", $selected);
        $totalSelected = count($selected);

        $sql = <<<SQL
SELECT p.* FROM product_tags pt
LEFT JOIN products p ON pt.product_id = p.id
WHERE p.is_orderable = 1
AND pt.category NOT IN ({$notIn})
AND product_id in (
select pc.product_id from product_classifications pc
left join order_classifications oc on oc.classification_id = pc.entity_id
where oc.order_id = {$order->getId()} and pc.entity_type_id = 3
)
SQL;
        if ($totalSelected > 0) {

            $lastValue = array_pop($selected);

            $string = <<<STR
HAVING 
STR;
            foreach ($selected as $tag) {
                $string .= <<<STR
FIND_IN_SET('$tag', GROUP_CONCAT(tag)) > 0 AND 
STR;
            }

            $string .= <<<STR
FIND_IN_SET('$lastValue', GROUP_CONCAT(tag)) > 0
            
STR;

            $sql .= <<<SQL

AND product_id IN (
    SELECT pt2.product_id from product_tags pt2
    LEFT JOIN products p2 ON pt2.product_id = p2.id
    WHERE p2.is_orderable = 1
    GROUP BY pt2.product_id
    {$string}
)
AND product_id in (
select pc.product_id from product_classifications pc
left join order_classifications oc on oc.classification_id = pc.entity_id
where oc.order_id = {$order->getId()} and pc.entity_type_id = 3
)
SQL;
        }

        if (is_array($search) && $search['value']) {
            $sql .= <<<SQL

AND p.name LIKE '%{$search['value']}%'
SQL;
        }

        $sql .= <<<SQL

GROUP BY p.id
order by p.code
SQL;
        if (null !== $limit && null !== $offset) {
            $sql .= <<<SQL
        
LIMIT {$offset}, {$limit};
SQL;
        }

        return new Simple(null, new \Product(), $this->modelsManager->getReadConnection(new \Product())->query($sql));
    }

    public function  getProductTagModel(\Product $product)
    {
        $query = $this->createQueryBuilder(null, 'pt')
            ->columns([
                'pt.tag as model',
            ])
            ->where('pt.productId = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('pt.category = :productId:', [
                'productId' => \ProductTag::MODEL
            ])
            ->getQuery()
            ->execute();

        if ($query->count() > 0) {
            return $query->toArray()[0]['model'];
        }

        return null;
    }
}
