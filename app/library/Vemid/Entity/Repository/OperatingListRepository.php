<?php

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Date\DateTime;

/**
 * Class OperatingListItemWorksheetRepository
 * @package Vemid\Entity\Repository
 */
class OperatingListRepository extends EntityRepository
{
    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \OperatingList::class);
    }

    /***
     * @param \OperatingList $operatingList
     * @return mixed
     */
    public function getTotalQty(\OperatingList $operatingList)
    {
        $query = $this->createQueryBuilder(null, 'ol')
            ->columns([
                'SUM(COALESCE(oliw.qty, 0)) as total'
            ])
            ->leftJoin(\OperatingListItem::class, 'oli.operatingListId = ol.id', 'oli')
            ->leftJoin(\OperatingListItemWorksheet::class, 'oliw.operatingListItemId = oli.id', 'oliw')
            ->where('oli.id = :id:', [
                'id' => $operatingList->getId()
            ]);

        return $query->getQuery()->execute()[0]['total'];
    }

    /**
     * @param \OperatingList $operatingList
     * @param DateTime $date
     * @return float
     */
    public function getTotalQtyTillDay(\OperatingList $operatingList, DateTime $date): float
    {
        $query2 = $this->createQueryBuilder(null, 'ol')
            ->columns([
                'IF(oliw.id IS NULL, 0, COALESCE(SUM(oliw.qty), 0)) as totalQty'
            ])
            ->leftJoin(\OperatingListItem::class, 'oli.operatingListId = ol.id', 'oli')
            ->leftJoin(\OperatingListItemWorksheet::class, 'oliw.operatingListItemId = oli.id', 'oliw')
            ->where('ol.id = :id:', [
                'id' => $operatingList->getId()
            ])
            ->andWhere('oliw.date <= :date: OR oliw.id IS NULL', [
                'date' => $date->getUnixFormat()
            ])
            ->groupBy('oli.id');

        $result = $query2->getQuery()->execute();

        $total = 0;
        foreach ($result as $row) {
            if ((int)$row['totalQty'] === 0) {
                return (float)0;
            }

            $total += $row['totalQty'];
        }

        return (float)$total;
    }

    /**
     * @param \OperatingList $operatingList
     * @param DateTime $date
     * @return float
     */
    public function getSumQtyGroupeByItemOnDay(\OperatingList $operatingList, DateTime $date)
    {
        $query2 = $this->createQueryBuilder(null, 'ol')
            ->columns([
                'SUM(oliw.qty) * oli.pieceTime as totalProfit',
                'SUM(oliw.qty) as qty',
                'oli.pieceTime'
            ])
            ->leftJoin(\OperatingListItem::class, 'oli.operatingListId = ol.id', 'oli')
            ->leftJoin(\OperatingListItemWorksheet::class, 'oliw.operatingListItemId = oli.id', 'oliw')
            ->where('ol.id = :id:', [
                'id' => $operatingList->getId()
            ])
            ->andWhere('oliw.date = :date:', [
                'date' => $date->getUnixFormat()
            ])
            ->groupBy('oli.id');

        $result = $query2->getQuery()->execute();

        $sum = 0;
        foreach ($result as $row) {
            $sum += $row['totalProfit'];
        }

        return $sum;
    }

    public function getProfitTillDay(\OperatingList $operatingList, DateTime $date)
    {
        $query2 = $this->createQueryBuilder(null, 'ol')
            ->columns([
                'SUM(oliw.qty) * oli.pieceTime * oli.piecePrice as totalProfit',
            ])
            ->leftJoin(\OperatingListItem::class, 'oli.operatingListId = ol.id', 'oli')
            ->leftJoin(\OperatingListItemWorksheet::class, 'oliw.operatingListItemId = oli.id', 'oliw')
            ->where('ol.id = :id:', [
                'id' => $operatingList->getId()
            ])
            ->andWhere('oliw.date = :date:', [
                'date' => $date->getUnixFormat()
            ])
            ->groupBy('oli.id');

        $result = $query2->getQuery()->execute();

        $sum = 0;
        foreach ($result as $row) {
            $sum += $row['totalProfit'];
        }

        return $sum;
    }

    /**
     * @param DateTime $date
     * @return float
     */
    public function getTotalProfitTillDay(DateTime $date)
    {
        $query2 = $this->createQueryBuilder(null, 'ol')
            ->columns([
                'SUM(oliw.qty) * oli.pieceTime as totalProfit'
            ])
            ->leftJoin(\OperatingListItem::class, 'oli.operatingListId = ol.id', 'oli')
            ->leftJoin(\OperatingListItemWorksheet::class, 'oliw.operatingListItemId = oli.id', 'oliw')
            ->where('oliw.date = :date:', [
                'date' => $date->getUnixFormat()
            ])
            ->groupBy('oliw.id');

        $result = $query2->getQuery()->execute();

        $sum = 0;
        foreach ($result as $row) {
            $sum += $row['totalProfit'];
        }

        return $sum;
    }

}
