<?php

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Phalcon\Mvc\View\Simple;
use Vemid\Entity\Type;

class ProductClassificationRepository extends EntityRepository
{
    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \ProductClassification::class);
    }

    /**
     * @param array $classifications
     * @return Simple|array
     */
    public function getProductByEligibleClassifications(array $classifications)
    {
        $query = $this->createQueryBuilder(null, 'pc')
            ->columns([
                'GROUP_CONCAT(p.id) AS productIds'
            ])
            ->leftJoin(\Product::class, 'p.id = pc.productId', 'p')
            ->leftJoin(\Classification::class, 'c.id = pc.entityId', 'c')
            ->where('p.isOrderable = :isOrderable: AND pc.entityTypeId = :entityTypeId:', [
                'isOrderable' => true,
                'entityTypeId' => Type::CLASSIFICATION
            ])
            ->andWhere('pc.entityId IN ({ids:array})', [
                'ids' => $classifications
            ])
            ->groupBy('pc.productId')
            ->having('COUNT(pc.productId) > 1');

        return $query->getQuery()->execute();
    }
}