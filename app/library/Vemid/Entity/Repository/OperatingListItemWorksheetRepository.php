<?php

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Date\DateRange;
use Vemid\Date\DateTime;
use Vemid\Application\Di;

/**
 * Class OperatingListItemWorksheetRepository
 * @package Vemid\Entity\Repository
 */
class OperatingListItemWorksheetRepository extends EntityRepository
{
    /** @var ManagerInterface */
    private $modelsManager;

    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        $this->modelsManager = $modelManager;

        parent::__construct($modelManager, \OperatingListItemWorksheet::class);
    }

    /**
     * @param \OperatingListItem $operatingListItem
     * @return int
     */

    public function getTotalPerItem(\OperatingListItem $operatingListItem): int
    {
        $result = $this->createQueryBuilder(null, 'oliw')
            ->columns([
                'SUM(oliw.qty) as total'
            ])
            ->leftJoin(\OperatingListItem::class, 'oliw.operatingListItemId = oli.id', 'oli')
            ->where('oliw.operatingListItemId = :operatingListItemId:', [
                'operatingListItemId' => $operatingListItem->getId()
            ])
            ->getQuery()
            ->execute();

        return (int)$result->toArray()[0]['total'];
    }


    /**
     * @param $date
     * @param \ProductionWorker $productionWorker
     * @return array
     */
    public function getTotalPaymentPerWorkerAndDate($date, \ProductionWorker $productionWorker)
    {
        $result = $this->createQueryBuilder(null, 'pw')
            ->columns([
                'SUM(pw.qty) as total',
                'SUM(pw.qty*oi.piecePrice*oi.pieceTime) as money'
            ])
            ->leftJoin(\OperatingListItem::class, 'oi.id = pw.operatingListItemId', 'oi')
            ->where('pw.date <= :dateFrom: ', [
                'dateFrom' => $date
            ])
            ->andWhere('pw.date >= :dateTo:', [
                'dateTo' => $date,
            ])
            ->andWhere('pw.productionWorkerId = :workerId:', [
                'workerId' => $productionWorker->getId()
            ])
            ->groupBy('pw.date')
            ->getQuery()
            ->execute();

        if ($result->count()) {
            return $result->toArray()[0]['money'];
        }

        return null;
    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     * @param \ProductionWorker $productionWorker
     * @param $machineType
     * @return array
     */

    public function getTotalPaymentPerWorkerAndPeriod(DateTime $dateFrom, DateTime $dateTo, \ProductionWorker $productionWorker, $machineType = null)
    {
        $result = $this->modelsManager->createBuilder();
        $result
            ->addFrom(\OperatingListItemWorksheet::class,'oliw')
            ->columns([
                'ROUND(SUM(oliw.qty*oi.pieceTime),0) as totalTime',
                'SUM(oliw.qty*oi.piecePrice*oi.pieceTime) as money',
                'IFNULL(sum(time_to_sec(oliw.endTime)-time_to_sec(oliw.startTime)),0) as workTime'
            ])
            ->leftJoin(\OperatingListItem::class, 'oi.id = oliw.operatingListItemId', 'oi')
            ->leftJoin(\OperatingListItemType::class, 'olit.id = oi.operatingListItemTypeId', 'olit')
            ->leftJoin(\ProductionMachine::class, 'pm.id = olit.productionMachineId', 'pm')
            ->betweenWhere('oliw.date', $dateFrom->getUnixFormat(), $dateTo->getUnixFormat())
            ->andWhere('oliw.productionWorkerId = :workerId:', [
                'workerId' => $productionWorker->getId()
            ])
            ->andWhere('oliw.overtime = :overtime:', [
                'overtime' => '0'
            ]);
        if($machineType){
            $result->andWhere('pm.type = :machineType:',[
                'machineType' => $machineType
            ]);
        }
        $result->groupBy('oliw.productionWorkerId');

        $queryForCount = clone $result;
        $allData = $queryForCount->getQuery()->execute();


        if ($allData->count()) {
            $result->getQuery()
                ->execute();
            return $allData->toArray();
        }

        return null;
    }


    /**
     * @param \OperatingList $operatingList
     * @param DateRange $dateRange
     * @return int
     */
    public function getTotalWorkersForPeriod(\OperatingList $operatingList, DateRange $dateRange)
    {
        $startDate = $dateRange->getStartDate()->getUnixFormat();
        $endDate = $dateRange->getEndDate()->getUnixFormat();

        $sqlString = <<<SQL
SELECT COALESCE(SUM(RESULT), 0) AS total
FROM
  (SELECT (COUNT(DISTINCT oliw.production_worker_id)) AS RESULT
   FROM operating_list_item_worksheets oliw
   LEFT JOIN operating_list_items oli ON oli.id = oliw.operating_list_item_id
   LEFT JOIN operating_lists ol ON ol.id = oli.operating_list_id
   WHERE ol.id = {$operatingList->getId()}
     AND oliw.date BETWEEN '{$startDate}' AND '{$endDate}'
   GROUP BY oliw.date) AS A
SQL;

        $result = $this->modelsManager->getWriteConnection(new \OperatingListItem())->fetchAll($sqlString);

        return (int)$result[0]['total'];
    }

    /**
     * @param DateTime $date
     * @param \ProductionWorker $productionWorker
     * @return bool
     */
    public function getApprovedStatusByDate(DateTime $date, \ProductionWorker $productionWorker)
    {
        $results = $this->createQueryBuilder(null, 'oliw')
            ->columns([
                'MAX(oliw.approved) as approved'
            ])
            ->where('oliw.date = :date: ', [
                'date' => $date->getUnixFormat()
            ])
            ->andWhere('oliw.productionWorkerId = :productionWorker:', [
                'productionWorker' => $productionWorker->getId()
            ])
            ->getQuery()
            ->execute();

        return (int)$results->toArray()[0]['approved'] === 1;
    }

    /**
     * @param DateTime $date
     * @param \ProductionWorker $productionWorker
     * @return bool
     */
    public function getOvertimeStatusByDate(DateTime $date, \ProductionWorker $productionWorker)
    {
        $results = $this->createQueryBuilder(null, 'oliw')
            ->columns([
                'MAX(oliw.overtime) as overtime'
            ])
            ->where('oliw.date = :date: ', [
                'date' => $date->getUnixFormat()
            ])
            ->andWhere('oliw.productionWorkerId = :productionWorker:', [
                'productionWorker' => $productionWorker->getId()
            ])
            ->getQuery()
            ->execute();

        return (int)$results->toArray()[0]['overtime'] === 1;
    }

    /**
     * @param $date
     * @param \ProductionWorker $productionWorker
     * @return string
     */
    public function getWorksheetStatus($date, \ProductionWorker $productionWorker)
    {
        $string = "";

        if ($this->getOvertimeStatusByDate($date, $productionWorker)) {
            if ($this->getApprovedStatusByDate($date, $productionWorker)) {
                $string = "Odob., Preko.";
            }

            $string = "Prekovremeno";

        } elseif ($this->getApprovedStatusByDate($date, $productionWorker))
            $string = "Odobreno";


        return $string;
    }

    /**
     * @param DateTime $date
     * @param \ProductionWorker $productionWorker
     * @return int
     */
    public function getOvertimeByDate(DateTime $date, \ProductionWorker $productionWorker)
    {
        $wholeDay = 480 * 60;
        $overTIme = 0;

        if ($this->getOvertimeStatusByDate($date, $productionWorker)) {
            $results = $this->createQueryBuilder(null, 'oliw')
                ->columns([
                    'IFNULL((sum(time_to_sec(oliw.endTime)-time_to_sec(oliw.startTime))),0) as workTime'
                ])
                ->leftJoin(\OperatingListItem::class, 'oi.id = oliw.operatingListItemId', 'oi')
                ->where('oliw.date = :date: ', [
                    'date' => $date->getUnixFormat()
                ])
                ->andWhere('oliw.productionWorkerId = :productionWorker:', [
                    'productionWorker' => $productionWorker->getId()
                ])
                ->getQuery()
                ->execute();

            $result = (int)$results->toArray()[0]['workTime'];

            if ($result > $wholeDay) {
                $overTIme = $result - $wholeDay;
            }
        }

        return $overTIme;
    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     * @param \ProductionWorker $productionWorker
     * @return int
     */
    public function getOvertimeByPeriod(DateTime $dateFrom, DateTime $dateTo, \ProductionWorker $productionWorker)
    {
        $ranges = new DateRange($dateFrom, $dateTo);
        $totalOverTime = 0;

        foreach ($ranges->getAllDates() as $date) {
            $totalOverTime += $this->getOvertimeByDate($date, $productionWorker);
        }

        return $totalOverTime > 0 ? $totalOverTime : 0;
    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     * @param \ProductionWorker $productionWorker
     * @return array
     */
    public function getOvertimePaymentPerWorkerAndPeriod(DateTime $dateFrom, DateTime $dateTo, \ProductionWorker $productionWorker)
    {
        $result = $this->createQueryBuilder(null, 'oliw')
            ->columns([
                'sum(oliw.qty*oi.piecePrice*oi.pieceTime) as overtimeMoney',
            ])
            ->leftJoin(\OperatingListItem::class, 'oi.id = oliw.operatingListItemId', 'oi')
            ->betweenWhere('oliw.date', $dateFrom->getUnixFormat(), $dateTo->getUnixFormat())
            ->andWhere('oliw.productionWorkerId = :workerId:', [
                'workerId' => $productionWorker->getId()
            ])
            ->andWhere('oliw.overtime = :overtime:', [
                'overtime' => '1'
            ])
            ->groupBy('oliw.productionWorkerId')
            ->getQuery()
            ->execute();

        if ($result->count()) {
            return $result->toArray();
        }

        return null;
    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     * @param \Code $sector
     * @return array
     */
    public function getOutTermBySector(\Code $sector, DateTime $dateFrom, DateTime $dateTo)
    {

        $dateFromFormat = $dateFrom->format('Y-m-d');
        $dateToFormat = $dateTo->format('Y-m-d');
        $sectorId = $sector->getId();

        $rawSql= <<<SQL
        select olit.id olitId,olit.name,count(oliw.id) qty,oli.piece_time,oli.pieces_hour,
sum(oliw.qty) oliw_qty,(sum(time_to_sec(oliw.end_time)-time_to_sec(oliw.start_time)))/60 vreme,
((sum(oliw.qty)*oli.piece_time)/((sum(time_to_sec(oliw.end_time)-time_to_sec(oliw.start_time)))/60))*100 percentage
 from operating_list_item_worksheets oliw
left join operating_list_items oli on oli.id = oliw.operating_list_item_id
left join operating_lists ol on ol.id = oli.operating_list_id
left join operating_list_item_types olit on olit.id = oli.operating_list_item_type_id
left join production_workers pw on pw.id = oliw.production_worker_id
where oliw.date >= '$dateFromFormat' and date <= '$dateToFormat' and ol.code_id = '$sectorId' and pw.worker_code <> '999'
group by olit.id,olit.name
order by ((sum(oliw.qty)*oli.piece_time)/((sum(time_to_sec(oliw.end_time)-time_to_sec(oliw.start_time)))/60))*100 desc
SQL;

        /** @var Di $di */
        $di = Di::getDefault();
        $result = $di->getDb()->fetchAll($rawSql);

        return $result;
    }
}
