<?php

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Phalcon\Mvc\Model\Resultset\Simple;
use Vemid\Entity\Type;

/**
 * Class ClassificationRepository
 * @package Vemid\Entity\Repository
 */
class ClassificationRepository extends EntityRepository
{
    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \Classification::class);
    }

    /**
     * @param \Order $order
     * @param \CodeType $codeType
     * @param null|\Client|\Code $client
     * @return Simple|\Classification[]
     */
    public function getClassificationsIncludedInOrders(\Order $order, \CodeType $codeType, $client = null)
    {
        $clientType = Type::CLIENT;
        $typeCode = Type::CODE;

        $query = $this->createQueryBuilder(null, 'c')
            ->leftJoin(\ProductClassification::class, 'c.id = pc.entityId', 'pc')
            ->leftJoin(\ProductSize::class, 'pc.productId = ps.productId', 'ps')
            ->leftJoin(\OrderItem::class, 'oi.productSizeId = ps.id', 'oi')
            ->leftJoin(\Client::class, "oi.entityId = cl.id AND oi.entityTypeId = {$clientType}", 'cl')
            ->leftJoin(\Code::class, "com.id = oi.entityId AND oi.entityTypeId = {$typeCode}", 'com')
            ->leftJoin(\Code::class, 'co.id = c.classificationTypeCodeId', 'co')
            ->where('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLASSIFICATION
            ])
            ->andWhere('oi.id IS NOT NULL')
            ->andWhere('oi.orderId = :orderId:', [
                'orderId' => $order->getId()
            ])
            ->andWhere('co.code = :code: AND co.codeTypeId = :codeTypeId:', [
                'code' => '07',
                'codeTypeId' => $codeType->getId()
            ]);

        if ($client) {
            $query->andWhere('cl.id = :id: OR com.id = :id:', [
                'id' => $client->getId()
            ]);
        }

        $query->groupBy('c.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param array $parentClassifications
     * @param array $products
     * @return Simple|\Classification[]
     */
    public function getProductSubGroupClassificationsIncludedInOrder(array $parentClassifications, array $products)
    {
        $query = $this->createQueryBuilder(null, 'c')
            ->leftJoin(\Code::class, 'co.id = c.classificationTypeCodeId', 'co')
            ->leftJoin(\ProductClassification::class, 'c.id = pc.entityId', 'pc')
            ->leftJoin(\Product::class, 'p.id = pc.productId', 'p')
            ->where('p.isOrderable = :orderable:', [
                'orderable' => true
            ])
            ->andWhere('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLASSIFICATION
            ])
            ->andWhere('p.id IN ({ids:array})', [
                'ids' => $products
            ])
            ->andWhere('c.parentClassificationId IN ({parentClassifications:array})', [
                'parentClassifications' => $parentClassifications
            ])
            ->groupBy('c.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param \Code $code
     * @param $productIds
     * @return Simple|\Classification[]
     */
    public function getAgeCollectionsIncludedInOrders(\Code $code, $productIds)
    {
        $query = $this->createQueryBuilder(null, 'c')
            ->leftJoin(\Code::class, 'co.id = c.classificationTypeCodeId', 'co')
            ->leftJoin(\ProductClassification::class, 'c.id = pc.entityId', 'pc')
            ->leftJoin(\Product::class, 'p.id = pc.productId', 'p')
            ->where('p.isOrderable = :orderable:', [
                'orderable' => true
            ])
            ->andWhere('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLASSIFICATION
            ])
            ->andWhere('co.code = :code:', [
                'code' => '02'
            ])
            ->andWhere('p.id IN ({ids:array})', [
                'ids' => $productIds
            ])
            ->andWhere('c.classificationCategoryCodeId = :classificationCategoryCodeId:', [
                'classificationCategoryCodeId' => $code->getId()
            ])
            ->groupBy('c.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param array $productIds
     * @return Simple|\Classification[]
     */
    public function getOldCollectionsIncludedInOrders(array $productIds)
    {
        $query = $this->createQueryBuilder(null, 'c')
            ->leftJoin(\Code::class, 'co.id = c.classificationTypeCodeId', 'co')
            ->leftJoin(\ProductClassification::class, 'c.id = pc.entityId', 'pc')
            ->leftJoin(\Product::class, 'p.id = pc.productId', 'p')
            ->where('p.isOrderable = :orderable:', [
                'orderable' => true
            ])
            ->andWhere('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLASSIFICATION
            ])
            ->andWhere('co.code = :code:', [
                'code' => '07'
            ])
            ->andWhere('p.id IN ({ids:array})', [
                'ids' => $productIds
            ])
            ->groupBy('c.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param \Classification $classificationParent
     * @return Simple|\Classification[]
     */
    public function getParentClassificationsIncludedInOrders(\Classification $classificationParent)
    {
        $query = $this->createQueryBuilder(null, 'c')
            ->leftJoin(\ProductClassification::class, 'c.id = pc.entityId', 'pc')
            ->leftJoin(\Product::class, 'p.id = pc.productId', 'p')
            ->where('p.isOrderable = :orderable:', [
                'orderable' => true
            ])
            ->andWhere('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLASSIFICATION
            ])
            ->andWhere('c.parentClassificationId = :parentClassificationId:', [
                'parentClassificationId' => $classificationParent->getId()
            ])
            ->groupBy('c.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param \CodeType $codeType
     * @return Simple|\Classification[]
     */
    public function getClassificationsIncludedInProducts(\CodeType $codeType)
    {
        $classificationCode = Type::CLASSIFICATION;

        $query = $this->createQueryBuilder(null, 'c')
            ->leftJoin(\ProductClassification::class, "c.id = pc.entityId AND pc.entityTypeId = {$classificationCode}", 'pc')
            ->leftJoin(\Product::class,'p.id = pc.productId','p')
            ->leftJoin(\Code::class,'c1.id = c.classificationTypeCodeId','c1')
            ->leftJoin(\CodeType::class,'ct.id = c1.codeTypeId','ct')
            ->where('c1.code = :code:', [
                'code' => '07'
            ])
            ->andWhere('ct.id = :codeTypeId:', [
                'codeTypeId' => $codeType->getId()
            ])
            ->andWhere('p.isSynced = :status:', [
                'status' => false
            ]);

        $query->groupBy('c.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param \CodeType $codeType
     * @return Simple|\Classification[]
     */
    public function getClassificationsIncludedInProduceProducts(\CodeType $codeType)
    {
        $classificationCode = Type::CLASSIFICATION;

        $query = $this->createQueryBuilder(null, 'c')
            ->leftJoin(\ProductClassification::class, "c.id = pc.entityId AND pc.entityTypeId = {$classificationCode}", 'pc')
            ->leftJoin(\Product::class,'p.id = pc.productId','p')
            ->leftJoin(\Code::class,'c1.id = c.classificationTypeCodeId','c1')
            ->leftJoin(\CodeType::class,'ct.id = c1.codeTypeId','ct')
            ->where('c1.code = :code:', [
                'code' => '07'
            ])
            ->andWhere('ct.id = :codeTypeId:', [
                'codeTypeId' => $codeType->getId()
            ]);

        $query->groupBy('c.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param \CodeType $codeType
     * @return Simple|\Classification[]
     */
    public function getSeasonIncludedInProducts(\CodeType $codeType)
    {
        $classificationCode = Type::CLASSIFICATION;

        $query = $this->createQueryBuilder(null, 'c')
            ->leftJoin(\ProductClassification::class, "c.id = pc.entityId AND pc.entityTypeId = {$classificationCode}", 'pc')
            ->leftJoin(\Product::class,'p.id = pc.productId','p')
            ->leftJoin(\Code::class,'c1.id = c.classificationTypeCodeId','c1')
            ->leftJoin(\CodeType::class,'ct.id = c1.codeTypeId','ct')
            ->where('c1.code = :code:', [
                'code' => '05'
            ])
            ->andWhere('ct.id = :codeTypeId:', [
                'codeTypeId' => $codeType->getId()
            ]);

        $query->groupBy('c.id');

        return $query->getQuery()->execute();
    }

}
