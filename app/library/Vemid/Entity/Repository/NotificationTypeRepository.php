<?php

namespace Vemid\Entity\Repository;


use Phalcon\Mvc\Model\ManagerInterface;
use Phalcon\Mvc\Model\Resultset;

class NotificationTypeRepository extends EntityRepository
{

    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \NotificationType::class);
    }

    /**
     * @param \User $user
     * @return \NotificationType[]|Resultset
     */
    public function getNotificationTypesByUser(\User $user)
    {
        $query = $this->createQueryBuilder()
            ->leftJoin(\UserNotification::class, 'un.' . \UserNotification::PROPERTY_NOTIFICATION_TYPE_ID . ' = ' . \NotificationType::class . '.id', 'un')
            ->leftJoin(\UserNotificationMessage::class, 'unm.' . \UserNotificationMessage::PROPERTY_USER_NOTIFICATION_ID . ' = un.id', 'unm')
            ->where('unm.id IS NOT NULL');

        return $query->getQuery()->execute();
    }
}