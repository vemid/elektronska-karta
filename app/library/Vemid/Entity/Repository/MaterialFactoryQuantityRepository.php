<?php

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;

/**
 * Class MaterialFactoryRepository
 * @package Vemid\Entity\Repository
 */
class MaterialFactoryQuantityRepository extends EntityRepository
{
    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \MaterialFactoryQuantity::class);
    }

    /**
     * @param \MaterialFactory $materialFactory
     * @return int
     */
    public function getTotalIn(\MaterialFactory $materialFactory)
    {
        $result = $this->createQueryBuilder(null, 'mfq')
            ->columns([
                'SUM(amount) as totalAmount'
            ])
            ->where('mfq.' . \MaterialFactoryQuantity::PROPERTY_MATERIAL_FACTORY_ID . ' = :materialFactoryId:', [
                'materialFactoryId' => $materialFactory->getId()
            ])
            ->andWhere('mfq.' . \MaterialFactoryQuantity::PROPERTY_TYPE . ' = :type:', [
                'type' => \MaterialFactoryQuantity::INCOME
            ])
            ->getQuery()
            ->execute();

        return (int)$result->toArray()[0]['totalAmount'];
    }

    /**
     * @param \MaterialFactory $materialFactory
     * @return int
     */
    public function getTotalOut(\MaterialFactory $materialFactory)
    {
        $result = $this->createQueryBuilder(null, 'mfq')
            ->columns([
                'SUM(amount) as totalAmount'
            ])
            ->where('mfq.' . \MaterialFactoryQuantity::PROPERTY_MATERIAL_FACTORY_ID . ' = :materialFactoryId:', [
                'materialFactoryId' => $materialFactory->getId()
            ])
            ->andWhere('mfq.' . \MaterialFactoryQuantity::PROPERTY_TYPE . ' = :type:', [
                'type' => \MaterialFactoryQuantity::OUTCOME
            ])
            ->getQuery()
            ->execute();

        return (int)$result->toArray()[0]['totalAmount'];
    }

    /**
     * @param \MaterialFactory $materialFactory
     * @return int
     */
    public function getTotal(\MaterialFactory $materialFactory)
    {
        return $this->getTotalIn($materialFactory) - $this->getTotalOut($materialFactory);
    }
}