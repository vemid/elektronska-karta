<?php

namespace Vemid\Entity\Repository;

use OrderTotal;
use Phalcon\Mvc\Model\ManagerInterface;
use \Phalcon\Mvc\Model\Resultset\Simple;
use Phalcon\Mvc\Model\ResultsetInterface;
use Vemid\Entity\Type;
use Vemid\Application\Di;

/**
 * Class ProductRepository
 * @package Vemid\Entity\Repository
 */
class ProductRepository extends EntityRepository
{
    /** @var ManagerInterface */
    private $modelsManager;

    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        $this->modelsManager = $modelManager;

        parent::__construct($modelManager, \Product::class);
    }

    public function getProductEligibleForOrdering(
        \Order $order,
        array $additionFilters,
        array $exclude,
        $totalFilters,
        $priority = null,
        $searchProductType = null,
        array $productSizes = [],
        $limit = null,
        $offset = null
    ) {
        $typeClassification = Type::CLASSIFICATION;
        $sql = <<<SQL
SELECT p.* FROM `products` p
LEFT JOIN `product_sizes` ps ON ps.product_id = p.`id`
LEFT JOIN `priorities` pr ON pr.id = p.`priority_id`
LEFT JOIN `codes` co ON co.id = p.`product_code_type_id`
LEFT JOIN `product_classifications` pc ON p.id = pc.`product_id`
LEFT JOIN `classifications` c ON c.id = pc.`entity_id`
WHERE pc.`entity_type_id` = {$typeClassification}
AND p.is_orderable = 1
AND p.id IN (
	SELECT ps2.product_id FROM `product_classifications` ps2
    LEFT JOIN `order_classifications` oc ON oc.`classification_id` = ps2.`entity_id`
    WHERE oc.`order_id` = {$order->getId()}
)

SQL;
        if ($priority) {
            $sql .= <<<SQL
AND pr.priority_number = '{$priority}'

SQL;

        }

        if ($searchProductType) {
            $sql .= <<<SQL
AND co.code = '{$searchProductType}'

SQL;
        }

        if (\count($additionFilters)) {
            $additionFilter = implode("','", $additionFilters);
            $sql .= <<<SQL
AND c.code IN ('{$additionFilter}') 

SQL;
        }

        if (count($exclude)) {
            $excludeFilter = implode("','", $exclude);

            $sqlExclude = <<<SQL
SELECT p.id FROM `products` p
LEFT JOIN `priorities` pr ON pr.id = p.`priority_id`
LEFT JOIN `codes` co ON co.id = p.`product_code_type_id`
LEFT JOIN `product_classifications` pc ON p.id = pc.`product_id`
LEFT JOIN `classifications` c ON c.id = pc.`entity_id`
WHERE pc.`entity_type_id` = {$typeClassification}
AND p.is_orderable = 1
AND p.id IN (
	SELECT ps2.product_id FROM `product_classifications` ps2
    LEFT JOIN `order_classifications` oc ON oc.`classification_id` = ps2.`entity_id`
    WHERE oc.`order_id` = {$order->getId()}
)

AND c.code IN ('{$excludeFilter}')
SQL;
            $sql .= <<<SQL
AND p.id NOT IN ({$sqlExclude}) 

SQL;
        }

        if (!empty($productSizes)) {
            $sizes = implode(',', $productSizes);
            $sql .= <<<SQL
AND ps.code_id IN ({$sizes}) 

SQL;
        }

        $sql .= <<<SQL
GROUP BY p.id
HAVING COUNT(p.id) > {$totalFilters}
ORDER BY p.name DESC
SQL;

        if (null !== $limit && null !== $offset) {
            $sql .= <<<SQL
        
LIMIT {$offset}, {$limit};
SQL;
        }

        return new Simple(null, new \Product(), $this->modelsManager->getReadConnection(new \Product())->query($sql));
    }

    /**

     * @param \Product $product
     * @return array
     */


    public function getSyncClassificationInformation(\Product $product)
    {
        $typeClassification = Type::CLASSIFICATION;

        $sql = <<<SQL
select "Klasifikacija" type, c.name,c.code kriterijum,cl.code oznaka,cl.name naziv,pc.is_synced from product_classifications pc
left join classifications cl on pc.entity_id = cl.id
left join codes c on c.id = cl.classification_type_code_id
where product_id = {$product->getId()} and entity_type_id = {$typeClassification}
order by type desc,kriterijum,oznaka
SQL;

        /** @var Di $di */
        $di = Di::getDefault();
        $result = $di->getDb()->fetchAll($sql);

        return $result;

    }

    /**
    * @param \Product $product
    * @return array
    */


    public function getSyncAttributesInformation(\Product $product)
    {
        $typeAttribute = Type::ATTRIBUTE;

        $sql = <<<SQL
select 'Atributi' type,c.name,c.code," " kriterijum,a.name naziv,pc.is_synced from product_classifications pc
left join attributes a on a.id = pc.entity_id
left join codes c on c.id = a.attribute_code_id
where entity_type_id = {$typeAttribute} and product_id = {$product->getId()}
order by c.code
SQL;

        /** @var Di $di */
        $di = Di::getDefault();
        $result = $di->getDb()->fetchAll($sql);

        return $result;
    }

    /**
     * @param string $productCode
     * @param \Product|null $product
     * @return ResultsetInterface|\Product[]
     */
    public function
    findPossiblePrentProducts(string $productCode, \Product $product = null)
    {
        $query = $this->createQueryBuilder()
            ->where('code LIKE :code:', [
                'code' => '%' . $productCode . '%'
            ]);

        if ($product) {
            $query->andWhere('parentId != :parentId:', [
                'parentId' => $product->getId()
            ]);
        }

        return $query->getQuery()->execute();
    }

    public function getOrderTotal(\Product $product) {
        $query = $this->modelsManager->createBuilder()
            ->columns([
                'SUM(oi.finalQty) as komada',
            ])
            ->addFrom(\OrderTotal::class,'oi')
            ->leftJoin(\ProductSize::class, 'ps.id = oi.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->where('p.id = :productId:', [
                'productId' => $product->getId()
            ]);

        /** @var OrderTotal|null $orderTotal */
        $orderTotal = $query->getQuery()->execute()->getLast();

        return $orderTotal ? $orderTotal->toArray() : [];
    }

    /**
     * @param \Product $product
     * @param array
     * @return int
     */
    public function getProductionFinishedQty(\Product $product,$array) {

        $processed = 0;

        /** @var \Phalcon\Mvc\Model\Query\Builder $builderListCount */

        $builderListCount = $this->modelsManager->createBuilder();
        $builderListCount->addFrom(\OperatingListItem::class, 'oli');
        $builderListCount->innerJoin(\OperatingList::class, 'ol.id = oli.operatingListId', 'ol');
        $builderListCount->where('ol.productId = :productId:', [
            'productId' => $product->getId()
        ]);
        $builderListCount->andWhere('ol.codeId IN ({sectors:array})', [
            'sectors'=> $array
        ]);
        $opItems = $builderListCount->getQuery()->execute()->count();

        /** @var \Phalcon\Mvc\Model\Query\Builder $opBuilder */
        $opBuilder = $this->modelsManager->createBuilder();
        $opBuilder->columns([
            'SUM(olw.qty) as totalQty'
        ]);
        $opBuilder->addFrom(\OperatingList::class, 'ol');
        $opBuilder->innerJoin(\OperatingListItem::class, 'ol.id = oli.operatingListId', 'oli');
        $opBuilder->innerJoin(\OperatingListItemWorksheet::class, 'oli.id = olw.operatingListItemId', 'olw');
        $opBuilder->where('ol.productId = :productId:', [
            'productId' => $product->getId()
        ]);
        $opBuilder->andWhere('ol.codeId IN ({sectors:array})', [
            'sectors'=> $array
        ]);

        $oliResult = $opBuilder->getQuery()->execute();

        $toArray1 = $oliResult->toArray();
        if (isset($toArray1[0]['totalQty'])) {
            $processed += $toArray1[0]['totalQty'];
        }

        $done = $processed > 0 ? (int)($processed / $opItems) : 0;

        return (int)$done;

    }
}
