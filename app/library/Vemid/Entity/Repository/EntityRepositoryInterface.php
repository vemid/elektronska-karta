<?php

namespace Vemid\Entity\Repository;

use Vemid\Entity\EntityInterface;
use Phalcon\Mvc\Model\Query\BuilderInterface;
use Phalcon\Mvc\Model\Resultset;

/**
 * Interface EntityRepositoryInterface
 *
 * @package Vemid\Entity\Repository
 */
interface EntityRepositoryInterface
{
    /**
     * @return string
     */
    public function getEntityName();

    /**
     * @param int|string|array $params
     * @return EntityInterface|bool
     */
    public function findOne($params);

    /**
     * @param null|string|array $params
     * @return EntityInterface[]|Resultset
     */
    public function find($params = null);

    /**
     * @param EntityInterface $entity
     * @param string $alias
     * @param array|null $arguments
     * @return Resultset|EntityInterface|null
     * @throws \DomainException
     */
    public function getRelated(EntityInterface $entity, $alias, $arguments = null);

    /**
     * @param null|string|array $params
     * @return BuilderInterface
     */
    public function createQueryBuilder($params = null);

    /**
     * @param EntityInterface $entity
     * @return bool
     */
    public function save(EntityInterface $entity);

    /**
     * @param EntityInterface $entity
     * @param bool $forceDelete
     * @return bool
     */
    public function delete(EntityInterface $entity, $forceDelete = false);
}
