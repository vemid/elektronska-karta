<?php

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Phalcon\Mvc\Model\Resultset\Simple;
use Vemid\Entity\Type;

/**
 * Class ProductSizeRepository
 * @package Vemid\Entity\Repository
 */
class ProductSizeRepository extends EntityRepository
{
    /** @var ManagerInterface */
    private $modelsManager;

    /**
     * ProductSizeRepository constructor.
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        $this->modelsManager = $modelManager;

        parent::__construct($modelManager, \ProductSize::class);
    }

    /**
     * @param \Order $order
     * @param \CodeType $codeType
     * @param $clientCode
     * @param $classificationCode
     * @param null $limit
     * @param null $offset
     * @return array
     */
    public function getGroupedOrderedProductSizeByClientCodeAndCollectionCode(\Order $order, \CodeType $codeType, $clientCode, $classificationCode, $limit = null, $offset = null)
    {
        $entityTypeCode = Type::CODE;
        $entityTypeClient = Type::CLIENT;
        $entityTypeClassification = Type::CLASSIFICATION;

        $sql = <<<SQL
SELECT 
p.code AS ProductCode,
p.name AS name,
c.name AS Collection,
GROUP_CONCAT(IF (oi.quantity_a > 0, CONCAT('Velicine ', pco.code, ' - ', (SELECT quantity_a FROM order_items WHERE `product_size_id` = ps.id AND order_id = {$order->getId()} AND entity_id = (IF (entity_type_id = {$entityTypeClient}, cl.id, co.id)))), NULL) separator '<br>') AS totalSizesA, 
GROUP_CONCAT(IF (oi.quantity_b > 0, CONCAT('Velicine ', pco.code, ' - ', (SELECT quantity_b FROM order_items WHERE `product_size_id` = ps.id AND order_id = {$order->getId()} AND entity_id = (IF (entity_type_id = {$entityTypeClient}, cl.id, co.id)))), NULL) separator '<br>') AS totalSizesB 
FROM product_sizes ps
LEFT JOIN products p ON ps.product_id = p.id
LEFT JOIN codes pco ON ps.code_id = pco.id
LEFT JOIN order_items oi ON ps.id = oi.`product_size_id`
LEFT JOIN product_classifications pc ON ps.product_id = pc.product_id
LEFT JOIN classifications c ON pc.entity_id = c.id
LEFT JOIN codes co ON c.`classification_type_code_id` = co.id
LEFT JOIN codes co2 ON oi.`entity_id` = co2.id AND oi.entity_type_id = {$entityTypeCode}
LEFT JOIN clients cl ON oi.`entity_id` = cl.id AND oi.entity_type_id = {$entityTypeClient}
WHERE pc.entity_type_id = {$entityTypeClassification}
AND oi.order_id = {$order->getId()}
AND co.code = '07'
AND co.code_type_id = {$codeType->getId()}
AND (cl.code = '{$clientCode}' OR co2.code = '{$clientCode}')
AND c.code = '{$classificationCode}'
GROUP BY ps.product_id
SQL;
        if (null !== $limit && null !== $offset) {
            $sql .= <<<SQL
        
LIMIT {$offset}, {$limit};
SQL;
        }

        return $this->modelsManager->getReadConnection(new \ProductSize())->fetchAll($sql);
    }

    public function getTotalMPOrder(\ProductSize $productSize)
    {
        $entityTypeCode = Type::CODE;

        $sql = <<<SQL
select sum(quantity_a+quantity_b) mpQty from order_items where entity_type_id = {$entityTypeCode} and product_size_id = {$productSize->getId()}
SQL;

        return $this->modelsManager->getReadConnection(new \ProductSize())->fetchAll($sql)[0]['mpQty'];

    }

    public function getTotalVPOrder(\ProductSize $productSize)
    {
        $entityTypeCode = Type::CLIENT;

        $sql = <<<SQL
select sum(quantity_a+quantity_b) vpQty from order_items where entity_type_id = {$entityTypeCode} and product_size_id = {$productSize->getId()}
SQL;

        return $this->modelsManager->getReadConnection(new \ProductSize())->fetchAll($sql)[0]['vpQty'];

    }

    public function getTotalMPPacked(\ProductSize $productSize)
    {
        $entityTypeCode = Type::CODE;

        $sql = <<<SQL
select sum(woi.quantity) mpQty from warehouse_order_items woi
left join warehouse_orders wo on wo.id = woi.warehouse_order_id
left join channel_supply_entities cse on cse.id = wo.channel_supply_entity_id
where woi.product_size_id = {$productSize->getId()} and cse.entity_type_id = {$entityTypeCode}
SQL;

        return $this->modelsManager->getReadConnection(new \ProductSize())->fetchAll($sql)[0]['mpQty'];

    }

    public function getTotalVPPacked(\ProductSize $productSize)
    {
        $entityTypeCode = Type::CLIENT;

        $sql = <<<SQL
select sum(woi.quantity) vpQty from warehouse_order_items woi
left join warehouse_orders wo on wo.id = woi.warehouse_order_id
left join channel_supply_entities cse on cse.id = wo.channel_supply_entity_id
where woi.product_size_id = {$productSize->getId()} and cse.entity_type_id = {$entityTypeCode}
SQL;

        return $this->modelsManager->getReadConnection(new \ProductSize())->fetchAll($sql)[0]['vpQty'];

    }

    public function getTotalB(\ProductSize $productSize)
    {
        $entityTypeCode = Type::CLIENT;

        $sql = <<<SQL
select sum(quantity_b) qtyB from order_items where entity_type_id = {$entityTypeCode} and product_size_id = {$productSize->getId()}
SQL;

        return $this->modelsManager->getReadConnection(new \ProductSize())->fetchAll($sql)[0]['qtyB'];

    }

    public function getTotalProducedA(\ProductSize $productSize)
    {

        $sql = <<<SQL
select sum(realized_a_qty) qtyA from productions_orders where product_size_id = {$productSize->getId()}
SQL;

        return $this->modelsManager->getReadConnection(new \ProductSize())->fetchAll($sql)[0]['qtyA'];

    }

    public function getTotalProducedB(\ProductSize $productSize)
    {

        $sql = <<<SQL
select sum(realized_b_qty) qtyB from productions_orders where product_size_id = {$productSize->getId()}
SQL;

        return $this->modelsManager->getReadConnection(new \ProductSize())->fetchAll($sql)[0]['qtyB'];

    }

    /**
     * @param \Order $order
     * @return mixed
     */
    public function getPivotRetailReportData( \Order $order)
    {

        $type = Type::CODE;
//        $query = $this->createQueryBuilder(null,'ps')
//            ->columns([
//                'p.id product',
//                'cd.code size',
//                'sum(ifnull(oi.quantityA,0)) as quantityA',
//                'sum(ifnull(oi.quantityB,0)) as quantityB',
//            ])
//            ->leftJoin(\Product::class,'p.id=ps.productId','p')
//            ->leftJoin(\ProductClassification::class,'pc.productId = p.id','pc')
//            ->leftJoin(\Code::class,'cd.id=ps.codeId','cd')
//            ->leftJoin(\Classification::class,'c.id=pc.entityId','c')
//            ->leftJoin(\OrderItem::class,"oi.productSizeId = ps.id and oi.entityTypeId={$type}","oi")
//            ->where('oi.orderId = :order:',[
//                'order' => $order->getId()
//            ])
//            ->groupBy('p.id,cd.code');
//
//        return $query->getQuery()->execute();

        $sql = <<<SQL

select c.name kolekcija,case when right(left(p.code,6),1) = 'M' then "Muski"
when right(left(p.code,6),1) = 'Z' then "Zenski"
when right(left(p.code,6),1) = 'U' then "Unisex" end as pol,p.code sifra,p.name naziv,cd.code velicina,sum(ifnull(oi.quantity_a,0)) kolicinaA,
sum(ifnull(oi.quantity_b,0)) kolicinaB from product_sizes ps
left join products p on p.id = ps.product_id
left join product_classifications pc on pc.product_id = p.id and pc.entity_type_id = 3
left join codes cd on cd.id = ps.code_id
left join classifications c on c.id = pc.entity_id and c.classification_type_code_id = '74'
left join order_items oi on oi.product_size_id = ps.id and oi.entity_type_id = {$type}
where oi.order_id = {$order->getId()} and c.name is not null
group by c.name,pol,p.code,p.name,cd.code
order by p.code,cd.code
SQL;

        return $this->modelsManager->getReadConnection(new \ProductSize())->fetchAll($sql);
    }

    public function fetchAllGrupedSizesPerOrderByProducts(\Order $order)
    {
        $typeClassification = Type::CLASSIFICATION;

        $sql = <<<SQL
SELECT size.* FROM `product_sizes` ps
LEFT JOIN codes size ON ps.code_id = size.id
INNER JOIN `products` p ON p.id = ps.`product_id`
LEFT JOIN `priorities` pr ON pr.id = p.`priority_id`
LEFT JOIN `codes` co ON co.id = p.`product_code_type_id`
LEFT JOIN `product_classifications` pc ON p.id = pc.`product_id`
LEFT JOIN `classifications` c ON c.id = pc.`entity_id`
WHERE pc.`entity_type_id` = {$typeClassification}
AND p.is_orderable = 1
AND p.id IN (
	SELECT ps2.product_id FROM `product_classifications` ps2
    LEFT JOIN `order_classifications` oc ON oc.`classification_id` = ps2.`entity_id`
    WHERE oc.`order_id` = {$order->getId()}
)
GROUP BY ps.code_id
SQL;

        $code = new \Code();

        return new Simple(null, $code, $code->getReadConnection()->query($sql));
    }
}
