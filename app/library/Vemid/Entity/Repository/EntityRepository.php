<?php

namespace Vemid\Entity\Repository;

use Vemid\Entity\EntityInterface;
use Phalcon\Mvc\Model\ManagerInterface;

/**
 * Class EntityRepository
 *
 * @package Vemid\Entity\Repository
 */
class EntityRepository implements EntityRepositoryInterface
{
    /** @var ManagerInterface */
    private $modelManager;

    /** @var string */
    private $entityName;

    /**
     * @param ManagerInterface $modelManager
     * @param string $entityName
     */
    public function __construct(ManagerInterface $modelManager, $entityName)
    {
        $this->modelManager = $modelManager;
        $this->entityName = $entityName;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * {@inheritdoc}
     */
    public function findOne($params)
    {
        if ($params === null) {
            return null;
        }

        if (!\is_array($params)) {
            $params = [$params];
        }

        $builder = $this->createQueryBuilder($params);
        $builder->limit(1);
        $query = $builder->getQuery();

        if (array_key_exists('cache', $params)) {
            $query->cache($params['cache']);
        }

        $query->setUniqueRow(true);

        return $query->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function find($params = null)
    {
        $builder = $this->createQueryBuilder($params);
        $query = $builder->getQuery();

        if (\is_array($params) && array_key_exists('cache', $params)) {
            $query->cache($params['cache']);
        }

        return $query->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function getRelated(EntityInterface $entity, $alias, $arguments = null)
    {
        $className = \get_class($entity);
        $relation = $this->modelManager->getRelationByAlias($className, $alias);

        if (!\is_object($relation)) {
            throw new \DomainException(sprintf(
                "There is no defined relations for the model '%s' using alias '%s'",
                $className, $alias
            ));
        }

        return \call_user_func(
            [$this->modelManager, 'getRelationRecords'],
            $relation, null, $entity, $arguments
        );
    }

    /**
     * {@inheritdoc}
     */
    public function createQueryBuilder($params = null, $alias = null)
    {
        $query = $this->modelManager->createBuilder($params);
        $query->addFrom($this->getEntityName(), $alias);

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    public function save(EntityInterface $entity)
    {
        return $entity->save();
    }

    /**
     * {@inheritdoc}
     */
    public function delete(EntityInterface $entity, $forceDelete = false)
    {
        return $entity->delete($forceDelete);
    }
}
