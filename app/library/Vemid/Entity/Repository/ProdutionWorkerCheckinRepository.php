<?php


namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Date\DateTime;

/**
 * Class AbsenceWorkRepository
 * @package Vemid\Entity\Repository
 */

class ProdutionWorkerCheckinRepository extends EntityRepository
{

    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \ProductionWorkerCheckin::class);
    }

    /**
     * @param $date
     * @param \ProductionWorker $productionWorker
     * @return string|null
     */
    public function getCheckinRecord($date, \ProductionWorker $productionWorker)
    {
        $result = $this->createQueryBuilder(null, 'pwc')
            ->columns([
                'checkIn',
                'checkOut'
            ])
            ->where('pwc.date = :date: ', [
                'date' => $date
            ])
            ->andWhere('pwc.workerId = :productionWorker:',[
                'productionWorker' => $productionWorker->getId()
            ])
            ->getQuery()
            ->execute();

        if ($result->count()) {
            return $result->toArray()[0];
        }

        return null;
    }

    /**
     * @param $date
     * @param \ProductionWorker $productionWorker
     * @return string|null
     */
    public function getCheckIn($date,\ProductionWorker $productionWorker) {
        if($data = $this->getCheckinRecord($date,$productionWorker)) {
            return (new DateTime($data['checkIn']))->getUnixFormat();
        }

        else return (new DateTime("00:00:00"))->getUnixFormat();
    }

    /**
     * @param $date
     * @param \ProductionWorker $productionWorker
     * @return string|null
     */
    public function getCheckOut($date,\ProductionWorker $productionWorker) {
        if($data = $this->getCheckinRecord($date, $productionWorker)) {
            return (new DateTime($data['checkOut']))->getUnixFormat();
        }

        else return (new DateTime("00:00:00"))->getUnixFormat();
    }

}