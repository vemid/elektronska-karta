<?php

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Mvc\Model\Resultset\Simple;
use Vemid\Application\Di;
use Vemid\Entity\Type;
use Vemid\Entity\EntityInterface;

/**
 * Class OrderRepository
 * @package Vemid\Entity\Repository
 */
class ClientOrderItemRepository extends EntityRepository
{
    /** @var ManagerInterface */
    private $modelsManager;

    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        $this->modelsManager = $modelManager;

        parent::__construct($modelManager, \ClientOrderItem::class);
    }

    /**
     * @param \Order $order
     * @param \Client $client
     * @return \ClientOrderItem[]|Resultset
     */
    public function getOrderItems(\Order $order, \Client $client)
    {
        $query = $this->createQueryBuilder(null, 'oi')
            ->where('oi.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLIENT
            ])
            ->andWhere('oi.orderId = :orderId:', [
                'orderId' => $order->getId()
            ])
            ->andWhere('oi.entityId = :clientId:', [
            'clientId' => $client->getId()
        ]);

        return $query->getQuery()->execute();
    }

    /**
     * @param \Order $order
     * @param \Product $product
     * @param \Client|\Code $client
     * @return mixed
     */
    public function getOrdersByProductAndClient(\Order $order, \Product $product, $client)
    {
        $typeClient = Type::CLIENT;
        $typeCode = Type::CODE;

        $query = $this->createQueryBuilder(null, 'oi')
            ->leftJoin(\ProductSize::class, 'ps.id = oi.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->leftJoin(\Client::class, "c.id = oi.entityId AND oi.entityTypeId = {$typeClient}", 'c')
            ->leftJoin(\Code::class, "co.id = oi.entityId AND oi.entityTypeId = {$typeCode}", 'co')
            ->where('p.id = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('oi.orderId = :orderId:', [
                'orderId' => $order->getId()
            ]);

        if ($client) {
            $query->andWhere('c.id = :clientId: OR co.id = :clientId:', [
                'clientId' => $client->getId()
            ]);
        }

        $query->andWhere('oi.id IS NOT NULL');

        return $query->getQuery()->execute();
    }

    /**
     * @param \Order $order
     * @param \CodeType $codeType
     * @param $classificationCode
     * @param $clientCode
     * @param null $limit
     * @param null $offset
     * @return Simple
     */
    public function getOrdersByClientGroupedByCollection(\Order $order, \CodeType $codeType, $classificationCode, $clientCode, $limit = null, $offset = null)
    {
        $entityTypeCode = Type::CODE;
        $entityTypeClient = Type::CLIENT;


        $query = $this->createQueryBuilder(null, 'oi')
            ->columns([
                'c.name as Collection',
                'c.code as CollectionCode',
                'SUM(oi.quantityA) as totalQtyA',
                'SUM(oi.quantityB) as totalQtyB',
                'SUM(oi.quantityA * prc.wholesalePrice) as totalA',
                'SUM(oi.quantityB * prc.wholesalePrice) as totalB',
                'IFNULL(co2.name, cl.name) as Client',
                'IFNULL(co2.code, cl.code) as ClientCode'
            ])
            ->leftJoin(\ProductSize::class, 'ps.id = oi.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->leftJoin(\ProductPrice::class, 'p.id = prc.productId AND prc.status = "PLANED"', 'prc')
            ->leftJoin(\ProductClassification::class, 'pc.productId = ps.productId', 'pc')
            ->leftJoin(\Classification::class, 'c.id = pc.entityId', 'c')
            ->leftJoin(\Code::class, 'co.id = c.classificationTypeCodeId', 'co')
            ->leftJoin(\Code::class, "co2.id = oi.entityId AND oi.entityTypeId = $entityTypeCode", 'co2')
            ->leftJoin(\Client::class, "cl.id = oi.entityId AND oi.entityTypeId = $entityTypeClient", 'cl')
            ->where('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLASSIFICATION
            ])
            ->andWhere('oi.orderId = :orderId:', [
                'orderId' => $order->getId()
            ])
            ->andWhere('co.code = :code:', [
                'code' => '07'
            ])
            ->andWhere('prc.pricelistId = :pricelistId:', [
                'pricelistId' => '2'
            ])
            ->andWhere('co.codeTypeId = :codeTypeId:', [
                'codeTypeId' => $codeType->getId()
            ]);

        if ($classificationCode) {
            $query->andWhere('c.code = :code1:', [
                'code1' => $classificationCode
            ]);
        }

        if ($clientCode) {
            $query->andWhere('cl.code = :code2: OR co2.code = :code2:', [
                'code2' => $clientCode
            ]);
        }

        $query->groupBy(['c.id', 'oi.entityId', 'oi.entityTypeId']);

        if (null !== $limit && null !== $offset) {
            $query->limit($limit, $offset);
        }

        return $query->getQuery()->execute();
    }

       /**
        * @param \Order $order
        * @param \ProductSize $productSize
        * @param EntityInterface $entity
        * @param string $entityType
        * @return mixed
        */
       public function getClientOrderForProductSize(\Order $order, \ProductSize $productSize, EntityInterface $entity, string $entityType)
       {

           $query = $this->createQueryBuilder(null, 'oi')
               ->leftJoin(\ProductSize::class, 'ps.id = oi.productSizeId', 'ps')
               ->leftJoin(\Code::class, 'c.id = ps.codeId', 'c')
               ->where('ps.id = :productSizeId:', [
                   'productSizeId' => $productSize->getId()
               ])
               ->andWhere('oi.entityId = :entityId:', [
                   'entityId' => $entity->getId()
               ])
               ->andWhere('oi.orderId = :orderId:', [
                   'orderId' => $order->getId()
               ])
               ->andWhere('oi.entityTypeId = :entityTypeId:', [
                   'entityTypeId' => $entityType
               ])
               ->orderBy('c.code')
               ->limit(1);

           return $query->getQuery()->execute()->getFirst();
       }

    /**
     * @param \Product $product
     * @return int
     */
    public function getOrderPerProduct(\Product $product)
    {
        $query = $this->createQueryBuilder(null, 'oi')
            ->columns([
                'SUM(oi.quantityA+ifnull(oi.quantityB,0)) as totalQty',
            ])
            ->leftJoin(\ProductSize::class, 'ps.id = oi.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->where('p.id = :productId:', [
                'productId' => $product->getId()
            ])
        ->getQuery()
            ->execute();

        if ($query->count() > 0) {
            return $query->toArray()[0]['totalQty'];
        }

        return null;
    }

    /**
     * @param \Classification $classification
     * @param \Order $order
     * @param int $client
     * @param \User
     * @return mixed
     */
    public function getPivotReportData(\Classification $classification,\Order $order,$client,\User $user) {


        $query = $this->createQueryBuilder(null,'oi')
            ->columns([
                'p.id as product',
                'p.code as productCode',
                'c.code as size',
                'cl.name as client',
                'oi.quantityA',
                'oi.quantityB',
                'oi.quantityA*prc.wholesalePrice as priceValueA',
                'oi.quantityB*prc.wholesalePrice as priceValueB'
            ])
            ->leftJoin(\Client::class,'cl.id = oi.entityId','cl')
            ->leftJoin(\ProductSize::class,'ps.id = oi.productSizeId','ps')
            ->leftJoin(\Code::class,'c.id = ps.codeId','c')
            ->leftJoin(\Product::class,'p.id = ps.productId','p')
            ->leftJoin(\ProductPrice::class, 'p.id = prc.productId AND prc.status = "PLANED"', 'prc')
            ->leftJoin(\User::class,'u.id = cl.userId','u')
            ->leftJoin(\ProductClassification::class,'pcl.productId = p.id','pcl')
            ->where('oi.entityTypeId = :clientType:',[
                'clientType' => Type::CLIENT
                ])
            ->andWhere('prc.pricelistId = :pricelistId:', [
                'pricelistId' => '2'
            ])
            ->andWhere('pcl.entityId = :entityId:',[
                'entityId' => $classification->getId()
            ]);
        if ($user->isClient()) {
            $query->andWhere('oi.entityId = :client:',[
                'client' => $client
            ])
            ->andWhere('oi.orderId = :orderId:',[
                'orderId' => $order->getId()
            ]);
        }

        $query->orderBy('p.code');

        return $query->getQuery()->execute();
    }

    /**
     * @param \Order $order
     * @param \User $user
     * @return mixed
     */
    public function getReportDataAction(\Order $order,\User $user)
    {

        /** @var Di $di */
        $di = Di::getDefault();
        $entityManager = $di->getEntityManager();

        $season = $order->getSeasonAttribute();
        $year = $entityManager->findOne(\Attribute::class,[
            \Attribute::PROPERTY_ID . '= :id: ',
            'bind' => [
                'id' =>$order->getYearAttributeId()
            ]
        ]);

        $currentYear = (int)$year->getName();
        $previousYear = $currentYear-1;

        if ($user->isClient()) {
            /** @var \Client $client */
            $client = $entityManager->findOne(\Client::class, [
                \Client::PROPERTY_USER_ID . ' = :userId:',
                'bind' => [
                    'userId' => $user->getId()
                ]
            ]);
        }
if($user->isClient()) {
    $rawSql = <<<SQL
select cl.name client,concat(p.code,"-",p.name) product_name,pt.tag model,pt2.tag season,pt3.tag gender ,pt4.tag age,
sum(ifnull(oi.quantity_a,0)) quantityA,sum(ifnull(oi.quantity_b,0)) quantityB,ifnull(p1.wholesale_price,p2.wholesale_price) price from client_order_items oi
left join product_sizes ps on ps.id = oi.product_size_id
left join products p on p.id = ps.product_id
left join product_tags pt on pt.product_id=p.id and pt.category = 'MODEL'
left join product_tags pt2 on pt2.product_id=p.id and pt2.category = 'SEASON'
left join product_tags pt3 on pt3.product_id=p.id and pt3.category = 'GENDER'
left join product_tags pt4 on pt4.product_id=p.id and pt4.category = 'AGE'
left join orders o on o.id = oi.order_id
left join clients cl on cl.id = oi.entity_id
left join attributes a1 on a1.id = o.year_attribute_id
left join attributes a2 on a2.id = o.season_attribute_id
left join product_prices p1 on p1.product_id = p.id and p1.status='FINAL' and p1.pricelist_id = 2
left join product_prices p2 on p2.product_id = p.id and p2.status='PLANED' and p2.pricelist_id = 2
where a1.name = '{$currentYear}' and a2.id = '{$season->getId()}' and oi.entity_type_id = '9' 
and oi.entity_id = '{$client->getId()}'
 group by client,product_name,model,season,gender,age
union all
select cl.name client,concat(p.code,"-",p.name) product_name,pt.tag model,pt2.tag season,pt3.tag gender ,pt4.tag age,
sum(ifnull(oi.quantity_a,0)) quantityA,sum(ifnull(oi.quantity_b,0)) quantityB,ifnull(p1.wholesale_price,p2.wholesale_price) price from client_order_items oi
left join product_sizes ps on ps.id = oi.product_size_id
left join products p on p.id = ps.product_id
left join product_tags pt on pt.product_id=p.id and pt.category = 'MODEL'
left join product_tags pt2 on pt2.product_id=p.id and pt2.category = 'SEASON'
left join product_tags pt3 on pt3.product_id=p.id and pt3.category = 'GENDER'
left join product_tags pt4 on pt4.product_id=p.id and pt4.category = 'AGE'
left join orders o on o.id = oi.order_id
left join clients cl on cl.id = oi.entity_id
left join attributes a1 on a1.id = o.year_attribute_id
left join attributes a2 on a2.id = o.season_attribute_id
left join product_prices p1 on p1.product_id = p.id and p1.status='FINAL' and p1.pricelist_id = 2
left join product_prices p2 on p2.product_id = p.id and p2.status='PLANED' and p2.pricelist_id = 2
where convert(a1.name,unsigned integer) = '{$previousYear}' and a2.id = '{$season->getId()}' and oi.entity_type_id = '9' 
and oi.entity_id = '{$client->getId()}'
group by client,product_name,model,season,gender,age
SQL;
}
else {
            $rawSql = <<<SQL
select cl.name client,pt.tag model,pt2.tag season,pt3.tag gender ,pt4.tag age,
sum(ifnull(oi.quantity_a,0)) quantityA,sum(ifnull(oi.quantity_b,0)) quantityB,ifnull(p1.wholesale_price,p2.wholesale_price) price from client_order_items oi
left join product_sizes ps on ps.id = oi.product_size_id
left join products p on p.id = ps.product_id
left join product_tags pt on pt.product_id=p.id and pt.category = 'MODEL'
left join product_tags pt2 on pt2.product_id=p.id and pt2.category = 'SEASON'
left join product_tags pt3 on pt3.product_id=p.id and pt3.category = 'GENDER'
left join product_tags pt4 on pt4.product_id=p.id and pt4.category = 'AGE'
left join orders o on o.id = oi.order_id
left join clients cl on cl.id = oi.entity_id
left join attributes a1 on a1.id = o.year_attribute_id
left join attributes a2 on a2.id = o.season_attribute_id
left join product_prices p1 on p1.product_id = p.id and p1.status='FINAL' and p1.pricelist_id = 2
left join product_prices p2 on p2.product_id = p.id and p2.status='PLANED' and p2.pricelist_id = 2
where a1.name = '{$currentYear}' and a2.id = '{$season->getId()}' and oi.entity_type_id = '9' 
 group by client,model,season,gender,age
union all
select cl.name client,pt.tag model,pt2.tag season,pt3.tag gender ,pt4.tag age,
sum(ifnull(oi.quantity_a,0)) quantityA,sum(ifnull(oi.quantity_b,0)) quantityB,ifnull(p1.wholesale_price,p2.wholesale_price) price from client_order_items oi
left join product_sizes ps on ps.id = oi.product_size_id
left join products p on p.id = ps.product_id
left join product_tags pt on pt.product_id=p.id and pt.category = 'MODEL'
left join product_tags pt2 on pt2.product_id=p.id and pt2.category = 'SEASON'
left join product_tags pt3 on pt3.product_id=p.id and pt3.category = 'GENDER'
left join product_tags pt4 on pt4.product_id=p.id and pt4.category = 'AGE'
left join orders o on o.id = oi.order_id
left join clients cl on cl.id = oi.entity_id
left join attributes a1 on a1.id = o.year_attribute_id
left join attributes a2 on a2.id = o.season_attribute_id
left join product_prices p1 on p1.product_id = p.id and p1.status='FINAL' and p1.pricelist_id = 2
left join product_prices p2 on p2.product_id = p.id and p2.status='PLANED' and p2.pricelist_id = 2
where convert(a1.name,unsigned integer) = '{$previousYear}' and a2.id = '{$season->getId()}' and oi.entity_type_id = '9' 
group by client,model,season,gender,age
union all
select "MP" client,pt.tag model,pt2.tag season,pt3.tag gender ,pt4.tag age,
sum(ifnull(oi.quantity_a,0)) quantityA,sum(ifnull(oi.quantity_b,0)) quantityB,ifnull(p1.wholesale_price,p2.wholesale_price) price from client_order_items oi
left join product_sizes ps on ps.id = oi.product_size_id
left join products p on p.id = ps.product_id
left join product_tags pt on pt.product_id=p.id and pt.category = 'MODEL'
left join product_tags pt2 on pt2.product_id=p.id and pt2.category = 'SEASON'
left join product_tags pt3 on pt3.product_id=p.id and pt3.category = 'GENDER'
left join product_tags pt4 on pt4.product_id=p.id and pt4.category = 'AGE'
left join orders o on o.id = oi.order_id
left join attributes a1 on a1.id = o.year_attribute_id
left join attributes a2 on a2.id = o.season_attribute_id
left join product_prices p1 on p1.product_id = p.id and p1.status='FINAL' and p1.pricelist_id = 2
left join product_prices p2 on p2.product_id = p.id and p2.status='PLANED' and p2.pricelist_id = 2
where convert(a1.name,unsigned integer) = '{$currentYear}' and a2.id = '{$season->getId()}' and oi.entity_type_id = '5' 
group by client,model,season,gender,age
union all
select "MP" client,pt.tag model,pt2.tag season,pt3.tag gender ,pt4.tag age,
sum(ifnull(oi.quantity_a,0)) quantityA,sum(ifnull(oi.quantity_b,0)) quantityB,ifnull(p1.wholesale_price,p2.wholesale_price) price from client_order_items oi
left join product_sizes ps on ps.id = oi.product_size_id
left join products p on p.id = ps.product_id
left join product_tags pt on pt.product_id=p.id and pt.category = 'MODEL'
left join product_tags pt2 on pt2.product_id=p.id and pt2.category = 'SEASON'
left join product_tags pt3 on pt3.product_id=p.id and pt3.category = 'GENDER'
left join product_tags pt4 on pt4.product_id=p.id and pt4.category = 'AGE'
left join orders o on o.id = oi.order_id
left join attributes a1 on a1.id = o.year_attribute_id
left join attributes a2 on a2.id = o.season_attribute_id
left join product_prices p1 on p1.product_id = p.id and p1.status='FINAL' and p1.pricelist_id = 2
left join product_prices p2 on p2.product_id = p.id and p2.status='PLANED' and p2.pricelist_id = 2
where convert(a1.name,unsigned integer) = '{$previousYear}' and a2.id = '{$season->getId()}' and oi.entity_type_id = '5' 
group by client,model,season,gender,age
SQL;
        }


        $result = $di->getDb()->fetchAll($rawSql);

        return $result;

    }
}
