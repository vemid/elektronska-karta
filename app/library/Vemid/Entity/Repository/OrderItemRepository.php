<?php

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Mvc\Model\Resultset\Simple;
use Vemid\Entity\Type;

/**
 * Class OrderRepository
 * @package Vemid\Entity\Repository
 */
class OrderItemRepository extends EntityRepository
{
    /** @var ManagerInterface */
    private $modelsManager;

    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        $this->modelsManager = $modelManager;

        parent::__construct($modelManager, \OrderItem::class);
    }

    /**
     * @param \Order $order
     * @param \Client $client
     * @return \OrderItem[]|Resultset
     */
    public function getOrderItems(\Order $order, \Client $client)
    {
        $query = $this->createQueryBuilder(null, 'oi')
            ->where('oi.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLIENT
            ])
            ->andWhere('oi.orderId = :orderId:', [
                'orderId' => $order->getId()
            ])
            ->andWhere('oi.entityId = :clientId:', [
            'clientId' => $client->getId()
        ]);

        return $query->getQuery()->execute();
    }

    /**
     * @param \Order $order
     * @param \Product $product
     * @param \Client|\Code $client
     * @return mixed
     */
    public function getOrdersByProductAndClient(\Order $order, \Product $product, $client)
    {
        $typeClient = Type::CLIENT;
        $typeCode = Type::CODE;

        $query = $this->createQueryBuilder(null, 'oi')
            ->leftJoin(\ProductSize::class, 'ps.id = oi.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->leftJoin(\Client::class, "c.id = oi.entityId AND oi.entityTypeId = {$typeClient}", 'c')
            ->leftJoin(\Code::class, "co.id = oi.entityId AND oi.entityTypeId = {$typeCode}", 'co')
            ->where('p.id = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('oi.orderId = :orderId:', [
                'orderId' => $order->getId()
            ]);

        if ($client) {
            $query->andWhere('c.id = :clientId: OR co.id = :clientId:', [
                'clientId' => $client->getId()
            ]);
        }

        $query->andWhere('oi.id IS NOT NULL');

        return $query->getQuery()->execute();
    }

    /**
     * @param \Order $order
     * @param \CodeType $codeType
     * @param $classificationCode
     * @param $clientCode
     * @param null $limit
     * @param null $offset
     * @return Simple
     */
    public function getOrdersByClientGroupedByCollection(\Order $order, \CodeType $codeType, $classificationCode, $clientCode, $limit = null, $offset = null)
    {
        $entityTypeCode = Type::CODE;
        $entityTypeClient = Type::CLIENT;


        $query = $this->createQueryBuilder(null, 'oi')
            ->columns([
                'c.name as Collection',
                'c.code as CollectionCode',
                'SUM(oi.quantityA) as totalQtyA',
                'SUM(oi.quantityB) as totalQtyB',
                'SUM(oi.quantityA * prc.wholesalePrice) as totalA',
                'SUM(oi.quantityB * prc.wholesalePrice) as totalB',
                'IFNULL(co2.name, cl.name) as Client',
                'IFNULL(co2.code, cl.code) as ClientCode'
            ])
            ->leftJoin(\ProductSize::class, 'ps.id = oi.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->leftJoin(\ProductPrice::class, 'p.id = prc.productId AND prc.status = "PLANED"', 'prc')
            ->leftJoin(\ProductClassification::class, 'pc.productId = ps.productId', 'pc')
            ->leftJoin(\Classification::class, 'c.id = pc.entityId', 'c')
            ->leftJoin(\Code::class, 'co.id = c.classificationTypeCodeId', 'co')
            ->leftJoin(\Code::class, "co2.id = oi.entityId AND oi.entityTypeId = $entityTypeCode", 'co2')
            ->leftJoin(\Client::class, "cl.id = oi.entityId AND oi.entityTypeId = $entityTypeClient", 'cl')
            ->where('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLASSIFICATION
            ])
            ->andWhere('oi.orderId = :orderId:', [
                'orderId' => $order->getId()
            ])
            ->andWhere('co.code = :code:', [
                'code' => '07'
            ])
            ->andWhere('prc.pricelistId = :pricelistId:', [
                'pricelistId' => '2'
            ])
            ->andWhere('co.codeTypeId = :codeTypeId:', [
                'codeTypeId' => $codeType->getId()
            ]);

        if ($classificationCode) {
            $query->andWhere('c.code = :code1:', [
                'code1' => $classificationCode
            ]);
        }

        if ($clientCode) {
            $query->andWhere('cl.code = :code2: OR co2.code = :code2:', [
                'code2' => $clientCode
            ]);
        }

        $query->groupBy(['c.id', 'oi.entityId', 'oi.entityTypeId']);

        if (null !== $limit && null !== $offset) {
            $query->limit($limit, $offset);
        }

        return $query->getQuery()->execute();
    }

    /**
     * @param \Order $order
     * @param \ProductSize $productSize
     * @param \Client $client
     * @return mixed
     */
    public function getClientOrderForProductSize(\Order $order, \ProductSize $productSize, \Client $client)
    {

        $query = $this->createQueryBuilder(null, 'oi')
            ->leftJoin(\ProductSize::class, 'ps.id = oi.productSizeId', 'ps')
            ->leftJoin(\Code::class, 'c.id = ps.codeId', 'c')
            ->where('ps.id = :productSizeId:', [
                'productSizeId' => $productSize->getId()
            ])
            ->andWhere('oi.entityId = :entityId:', [
                'entityId' => $client->getId()
            ])
            ->andWhere('oi.orderId = :orderId:', [
                'orderId' => $order->getId()
            ])
            ->andWhere('oi.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLIENT
            ])
            ->orderBy('c.code')
            ->limit(1);

        return $query->getQuery()->execute()->getFirst();
    }



    /**
     * @param \Product $product
     * @return int
     */
    public function getOrderPerProduct(\Product $product)
    {
        $query = $this->createQueryBuilder(null, 'oi')
            ->columns([
                'SUM(oi.quantityA+ifnull(oi.quantityB,0)) as totalQty',
            ])
            ->leftJoin(\ProductSize::class, 'ps.id = oi.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->where('p.id = :productId:', [
                'productId' => $product->getId()
            ])
        ->getQuery()
            ->execute();

        if ($query->count() > 0) {
            return $query->toArray()[0]['totalQty'];
        }

        return null;
    }

    /**
     * @param \Classification $classification
     * @param \Order $order
     * @param int $client
     * @param \User
     * @return mixed
     */
    public function getPivotReportData(\Classification $classification,\Order $order,$client,\User $user) {


        $query = $this->createQueryBuilder(null,'oi')
            ->columns([
                'p.id as product',
                'p.code as productCode',
                'c.code as size',
                'cl.name as client',
                'oi.quantityA',
                'oi.quantityB',
                'oi.quantityA*prc.wholesalePrice as priceValueA',
                'oi.quantityB*prc.wholesalePrice as priceValueB'
            ])
            ->leftJoin(\Client::class,'cl.id = oi.entityId','cl')
            ->leftJoin(\ProductSize::class,'ps.id = oi.productSizeId','ps')
            ->leftJoin(\Code::class,'c.id = ps.codeId','c')
            ->leftJoin(\Product::class,'p.id = ps.productId','p')
            ->leftJoin(\ProductPrice::class, 'p.id = prc.productId AND prc.status = "PLANED"', 'prc')
            ->leftJoin(\User::class,'u.id = cl.userId','u')
            ->leftJoin(\ProductClassification::class,'pcl.productId = p.id','pcl')
            ->where('oi.entityTypeId = :clientType:',[
                'clientType' => Type::CLIENT
                ])
            ->andWhere('prc.pricelistId = :pricelistId:', [
                'pricelistId' => '2'
            ])
            ->andWhere('pcl.entityId = :entityId:',[
                'entityId' => $classification->getId()
            ]);
        if ($user->isClient()) {
            $query->andWhere('oi.entityId = :client:',[
                'client' => $client
            ])
            ->andWhere('oi.orderId = :orderId:',[
                'orderId' => $order->getId()
            ]);
        }

        $query->orderBy('p.code');

        return $query->getQuery()->execute();


    }
}
