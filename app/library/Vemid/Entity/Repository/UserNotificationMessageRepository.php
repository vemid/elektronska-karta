<?php

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Entity\Collection;

/**
 * Class UserNotificationMessageRepository
 * @package Vemid\Entity\Repository
 */
class UserNotificationMessageRepository extends EntityRepository
{
    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \UserNotificationMessage::class);
    }

    /**
     * @param \User $user
     * @param null|int $limit
     * @return Collection|\UserNotificationMessage[]
     *
     * @throws \DomainException
     */
    public function getActiveNotificationByUser(\User $user, $limit = null)
    {
        $query = $this->createQueryBuilder()
            ->leftJoin(\UserNotification::class, 'un.id = ' . \UserNotificationMessage::class . '.' . \UserNotificationMessage::PROPERTY_USER_NOTIFICATION_ID, 'un')
            ->where('un.' . \UserNotification::PROPERTY_USER_ID .' = :userId:', [
                'userId' => $user->getId()
            ])
            ->andWhere(\UserNotificationMessage::class . '.' . \UserNotificationMessage::PROPERTY_STATUS . ' NOT IN ({statuses:array})', [
                'statuses' => [
                    \UserNotificationMessage::STATUS_DEACTIVATED,
                    \UserNotificationMessage::STATUS_FINISHED,
                    \UserNotificationMessage::STATUS_POSTPONED_ONE_DAY,
                    \UserNotificationMessage::STATUS_POSTPONED_TWO_DAYS,
                    \UserNotificationMessage::STATUS_POSTPONED_THREE_DAYS,
                    \UserNotificationMessage::STATUS_POSTPONED_FIVE_DAYS
                ]
            ])
            ->orderBy(\UserNotificationMessage::PROPERTY_DATE_CREATED . ' ASC');

        if ($limit) {
            $query->limit($limit);
        }

        return $query->getQuery()->execute();
    }
}
