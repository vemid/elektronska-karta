<?php
/**
 * Created by PhpStorm.
 * User: adminbk
 * Date: 8.5.18.
 * Time: 23.33
 */

namespace Vemid\Entity\Repository;

use Phalcon\Forms\Element\Date;
use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Date\DateTime;
use Vemid\Application\Di;

/**
 * Class ProductionWorkerRepository
 * @package Vemid\Entity\Repository
 */

class ProductionWorkerRepository extends EntityRepository
{
    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \ProductionWorker::class);
    }


    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     * @param int $sectorId
     * @return array
     */

    public function getSectorDataForPeriod($sectorId , DateTime $dateFrom, DateTime $dateTo)
    {
/* rawSql2 moze da se obirse posle testa*/
        $rawSql2 = <<<SQL
select DATE_FORMAT(date , "%d.%m.%Y")as serbianDate, date,sec_to_time(sum(time_to_sec(workTime))) as workTime,sum(money) as money,sum(time_to_sec(workTime)) seconds, count(money) count from (
select date, p.id as workerId, concat(p.name," ",p.last_name) as displayName,ifnull(B.workTime,sec_to_time(0)) as workTime,ifnull(B.totalTime,sec_to_time(0)) as totalTime,ifnull(B.money,0) as money from production_workers as p 
left join (select date,worker_id,sec_to_time(round(sum(workTime)*60,0)) as workTime,sec_to_time(sum(time_to_sec(totalTime))) as totalTime, sum(money) as money from (select oliw.date,pw.id as worker_id,sum(oliw.qty*oli.piece_time) workTime,sum(oliw.qty*oli.piece_time*oli.piece_price) as money,
SEC_TO_TIME(sum(time_to_sec(oliw.end_time)-time_to_sec(oliw.start_time))) as totalTime from operating_list_item_worksheets oliw
left join production_workers pw on pw.id = oliw.production_worker_id
left join codes c on c.id = pw.code_id
left join operating_list_items oli on oli.id = oliw.operating_list_item_id
where pw.code_id = '{$sectorId}' and oliw.date between '{$dateFrom}' and '{$dateTo}'
group by oliw.date,pw.id
union all
select cjw.date,pw.id as worker_id,sum(cjw.qty) workTime,sum(cjw.qty*cj.piece_price) as money,sec_to_time(sum(cjw.qty)*60) as totalTime  from cutting_job_worksheets cjw
left join production_workers pw on pw.id = cjw.production_worker_id
left join cutting_jobs cj on cj.id = cjw.cutting_job_id
left join codes c on c.id = pw.code_id
where pw.code_id = '{$sectorId}' and cjw.date between '{$dateFrom}' and '{$dateTo}'
group by cjw.date,pw.id) as A
group by date,worker_id) as B on B.worker_id = p.id
where p.code_id = '{$sectorId}' and money >0) as C
group by date
SQL;

        $rawSql = <<<SQL
select DATE_FORMAT(date , "%d.%m.%Y")as serbianDate, date,sec_to_time(sum(time_to_sec(oliwTime))) as oliwTime,sec_to_time(sum(time_to_sec(cuttingTime))) as cuttingTime,
sec_to_time(sum(time_to_sec(oliwTime))+sum(time_to_sec(cuttingTime))) workTime,
sum(money) as money,sum(time_to_sec(oliwTime)+time_to_sec(cuttingTime)) seconds,sum(time_to_sec(oliwTime)) oliwSec,sum(time_to_sec(cuttingTime)) cuttingSec, count(money) count from (
select date, p.id as workerId, concat(p.name," ",p.last_name) as displayName,ifnull(B.oliwTime,sec_to_time(0)) as oliwTime,ifnull(B.cuttingTime,sec_to_time(0)) as cuttingTime,ifnull(B.totalTime,sec_to_time(0)) as totalTime,ifnull(B.money,0) as money from production_workers as p 
left join (select date,worker_id,sec_to_time(round(sum(oliwTime)*60,0)) as oliwTime,sec_to_time(round(sum(cuttingTime)*60,0)) as cuttingTime,sec_to_time(sum(time_to_sec(totalTime))) as totalTime, sum(money) as money from 
	(select oliw.date,pw.id as worker_id,sum(oliw.qty*oli.piece_time) oliwTime,0 cuttingTime,sum(oliw.qty*oli.piece_time*oli.piece_price) as money,
SEC_TO_TIME(sum(time_to_sec(oliw.end_time)-time_to_sec(oliw.start_time))) as totalTime from operating_list_item_worksheets oliw
left join production_workers pw on pw.id = oliw.production_worker_id
left join codes c on c.id = pw.code_id
left join operating_list_items oli on oli.id = oliw.operating_list_item_id
where pw.code_id = '{$sectorId}' and oliw.date between '{$dateFrom}' and '{$dateTo}'
group by oliw.date,pw.id
union all
select cjw.date,pw.id as worker_id,0 oliwTime,sum(cjw.qty) cuttingTime,sum(cjw.qty*cj.piece_price) as money,sec_to_time(sum(cjw.qty)*60) as totalTime  from cutting_job_worksheets cjw
left join production_workers pw on pw.id = cjw.production_worker_id
left join cutting_jobs cj on cj.id = cjw.cutting_job_id
left join codes c on c.id = pw.code_id
where pw.code_id = '{$sectorId}' and cjw.date between '{$dateFrom}' and '{$dateTo}'
group by cjw.date,pw.id) as A
group by date,worker_id) as B on B.worker_id = p.id
where p.code_id = '{$sectorId}' and money >0) as C
group by date
SQL;



        /** @var Di $di */
        $di = Di::getDefault();
        $result = $di->getDb()->fetchAll($rawSql);

        return $result;

    }


    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     * @param int $sectorId
     * @return array
     */

    public function getWorkerDataForPeriod($sectorId , DateTime $dateFrom, DateTime $dateTo)
    {

        $rawSql = <<<SQL
select DATE_FORMAT(date , "%d.%m.%Y")as serbianDate, date,sec_to_time(sum(time_to_sec(workTime))) as workTime,sum(money) as money,sum(time_to_sec(workTime)) seconds, count(money) count from (
select date, p.id as workerId, concat(p.name," ",p.last_name) as displayName,ifnull(B.workTime,sec_to_time(0)) as workTime,ifnull(B.totalTime,sec_to_time(0)) as totalTime,ifnull(B.money,0) as money from production_workers as p 
left join (select date,worker_id,sec_to_time(round(sum(workTime)*60,0)) as workTime,sec_to_time(sum(time_to_sec(totalTime))) as totalTime, sum(money) as money from (select oliw.date,pw.id as worker_id,sum(oliw.qty*oli.piece_time) workTime,sum(oliw.qty*oli.piece_time*oli.piece_price) as money,
SEC_TO_TIME(sum(time_to_sec(oliw.end_time)-time_to_sec(oliw.start_time))) as totalTime from operating_list_item_worksheets oliw
left join production_workers pw on pw.id = oliw.production_worker_id
left join codes c on c.id = pw.code_id
left join operating_list_items oli on oli.id = oliw.operating_list_item_id
where pw.code_id = '{$sectorId}' and oliw.date between '{$dateFrom}' and '{$dateTo}'
group by oliw.date,pw.id
union all
select cjw.date,pw.id as worker_id,sum(cjw.qty) workTime,sum(cjw.qty*cj.piece_price) as money,sec_to_time(sum(cjw.qty)*60) as totalTime  from cutting_job_worksheets cjw
left join production_workers pw on pw.id = cjw.production_worker_id
left join cutting_jobs cj on cj.id = cjw.cutting_job_id
left join codes c on c.id = pw.code_id
where pw.code_id = '{$sectorId}' and cjw.date between '{$dateFrom}' and '{$dateTo}'
group by cjw.date,pw.id) as A
group by date,worker_id) as B on B.worker_id = p.id
where p.code_id = '{$sectorId}' and money >0) as C
group by date
SQL;
        /** @var Di $di */
        $di = Di::getDefault();
        $result = $di->getDb()->fetchAll($rawSql);

        return $result;

    }


    /**
     * @param DateTime $date
     * @param int $sectorId
     * @return array
     */

    public function getReportWorkTimeDataAction($sectorId , DateTime $date)
    {

        $rawSql = <<<SQL
select DATE_FORMAT('{$date}' , "%d.%m.%Y")as serbianDate, '{$date}' as date, p.id as workerId, concat(p.name," ",p.last_name) as displayName,ifnull(B.workTime,sec_to_time(0)) as workTime,ifnull(B.totalTime,sec_to_time(0)) as totalTime,ifnull(B.money,0) as money from production_workers as p 
left join (select date,worker_id,sec_to_time(round(sum(workTime)*60,0)) as workTime,sec_to_time(sum(time_to_sec(totalTime))) as totalTime, sum(money) as money from (select oliw.date,pw.id as worker_id,sum(oliw.qty*oli.piece_time) workTime,sum(oliw.qty*oli.piece_time*oli.piece_price) as money,
SEC_TO_TIME(sum(time_to_sec(oliw.end_time)-time_to_sec(oliw.start_time))) as totalTime from operating_list_item_worksheets oliw
left join production_workers pw on pw.id = oliw.production_worker_id
left join codes c on c.id = pw.code_id
left join operating_list_items oli on oli.id = oliw.operating_list_item_id
where oliw.date = '{$date}' and pw.code_id = '{$sectorId}'
group by oliw.date,pw.id
union all
select cjw.date,pw.id as worker_id,sum(cjw.qty) workTime,sum(cjw.qty*cj.piece_price) as money,sec_to_time(sum(cjw.qty)*60) as totalTime  from cutting_job_worksheets cjw
left join production_workers pw on pw.id = cjw.production_worker_id
left join cutting_jobs cj on cj.id = cjw.cutting_job_id
left join codes c on c.id = pw.code_id
where cjw.date = '{$date}' and pw.code_id = '{$sectorId}'
group by cjw.date,pw.id) as A
group by date,worker_id) as B on B.worker_id = p.id
where p.code_id = '{$sectorId}' and ifnull(p.break_up,date('{$date}')) >= '{$date}'
SQL;
        /** @var Di $di */
        $di = Di::getDefault();
        $result = $di->getDb()->fetchAll($rawSql);

        return $result;

    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     * @param int $productionWorkerId
     * @return array
     */
    public function getWorkersWorkDays(int $productionWorkerId, DateTime $dateFrom, DateTime $dateTo)
    {
        $rawSql= <<<SQL
        select count(distinct date) numberOfDays from (
select date from operating_list_item_worksheets where production_worker_id = '{$productionWorkerId}' and date between '{$dateFrom->getUnixFormat()}' and '{$dateTo->getUnixFormat()}'
union all
select date from cutting_job_worksheets where production_worker_id = '{$productionWorkerId}' and date between '{$dateFrom->getUnixFormat()}' and '{$dateTo->getUnixFormat()}'
) as A
SQL;

        /** @var Di $di */
        $di = Di::getDefault();
        $result = $di->getDb()->fetchAll($rawSql);

        return $result;
    }

    /**
     * @param int $productionWorkerId
     * @return array
     */
    public function getSalaries(int $productionWorkerId)
    {
        $rawSql= <<<SQL
        select year,month,month_num,round(sum(salary),0) salary from
(
select year(oliw.date) year, MONTHNAME(oliw.date) month,month(oliw.date) month_num, sum(oli.`piece_price`*oliw.qty*oli.`piece_time`) salary from operating_list_item_worksheets oliw
left join operating_list_items oli on oli.id = oliw.operating_list_item_id
 where production_worker_id = '{$productionWorkerId}'
 group by year,month
 union all
 select year(cjw.date) year,monthname(cjw.date) month,month(cjw.date) month_num,sum(cjw.qty*cj.`piece_price`) salary from cutting_job_worksheets cjw
 left join cutting_jobs cj on cj.id = cjw.cutting_job_id
 where production_worker_id = '{$productionWorkerId}'
 group by year,month
 ) as A
 group by year,month
 order by year,month_num
SQL;

        /** @var Di $di */
        $di = Di::getDefault();
        $result = $di->getDb()->fetchAll($rawSql);

        return $result;
    }

    /**
     * @return array
     */
    public function getYears()
    {
        $rawSql= <<<SQL
        select (year(oliw.`date`)) years from `operating_list_item_worksheets` oliw
group by year(oliw.date);
SQL;

        /** @var Di $di */
        $di = Di::getDefault();
        $result = $di->getDb()->fetchAll($rawSql);

        $data = [];
        foreach ($result as $row){
            $data[] = $row['years'];
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getMonths()
    {
        $rawSql= <<<SQL
        select (monthname(oliw.`date`)) months from `operating_list_item_worksheets` oliw
group by monthname(oliw.date)
order by month(oliw.date)
SQL;

        /** @var Di $di */
        $di = Di::getDefault();
        $result = $di->getDb()->fetchAll($rawSql);

        $data = [];
        foreach ($result as $row){
            $data[] = $row['months'];
        }

        return $data;
    }






}