<?php
/**
 * Created by PhpStorm.
 * User: adminbk
 * Date: 8.5.18.
 * Time: 23.33
 */

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Date\DateTime;

/**
 * Class AbsenceWorkRepository
 * @package Vemid\Entity\Repository
 */

class AbsenceWorkRepository extends EntityRepository
{
    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \AbsenceWork::class);
    }

    /**
     * @param $date
     * @param \ProductionWorker $productionWorker
     * @return string|null
     */
    public function getTypePerDay($date, \ProductionWorker $productionWorker)
    {
        $result = $this->createQueryBuilder(null, 'aw')
            ->columns([
                'type'
            ])
            ->where('aw.startDate <= :dateFrom: ', [
                'dateFrom' => $date
            ])
            ->andWhere('aw.endDate >= :dateTo:', [
                'dateTo' => $date,
            ])
            ->andWhere('aw.productionWorkerId = :productionWorker:',[
                'productionWorker' => $productionWorker->getId()
            ])
            ->getQuery()
            ->execute();

        if ($result->count()) {
            return $result->toArray()[0]['type'];
        }

        return null;
    }

    /**
     * @param $dateFrom
     * @param $dateto
     * @param \ProductionWorker $productionWorker
     * @return array
     */

    public function getTotalSick(\ProductionWorker $productionWorker, DateTime $dateFrom, DateTime $dateTo)
    {
        $result = $this->createQueryBuilder(null, 'aw')
            ->columns([
                'SUM(DATEDIFF(endDate,startDate)+1) SICK'
            ])
            ->betweenWhere('aw.startDate',  $dateFrom->getUnixFormat(), $dateTo->getUnixFormat())
            ->andWhere('aw.productionWorkerId = :productionWorker:',[
                'productionWorker' => $productionWorker->getId()
            ])
            ->andWhere('aw.type = :type:',[
                'type' => \AbsenceWork::TYPE_SICK
            ])
            ->andWhere('aw.onSalary = :onSalary:',[
                'onSalary' => '1'
            ])
            ->getQuery()
            ->execute();

        if ($result->count() > 0) {
            return $result->toArray()[0]['SICK'];
        }

        return null;
    }

    /**
     * @param $dateTo
     * @param \ProductionWorker $productionWorker
     * @return array
     */

    public function checkSickDay(\ProductionWorker $productionWorker, DateTime $dateTo)
    {
        $result = $this->createQueryBuilder(null, 'aw')
            ->columns([
                'count(*) SICK'
            ])
            ->where('aw.endDate = :endDate: ',[
               'endDate' => $dateTo->getUnixFormat()
            ])
            ->andWhere('aw.productionWorkerId = :productionWorker:',[
                'productionWorker' => $productionWorker->getId()
            ])
            ->andWhere('aw.type = :type:',[
                'type' => \AbsenceWork::TYPE_SICK
            ])
            ->getQuery()
            ->execute();

        if ($result->count() > 0) {
            return $result->toArray()[0]['SICK'];
        }

        return null;
    }

    /**
     * @param $dateFrom
     * @param $dateto
     * @param \ProductionWorker $productionWorker
     * @return array
     */

    public function getTotalVacation(\ProductionWorker $productionWorker, DateTime $dateFrom, DateTime $dateTo)
    {
        $result = $this->createQueryBuilder(null, 'aw')
            ->columns([
                'SUM(DATEDIFF(endDate,startDate)+1) VACATION'
            ])
            ->betweenWhere('aw.startDate', $dateFrom->getUnixFormat(), $dateTo->getUnixFormat())
            ->andWhere('aw.productionWorkerId = :productionWorker:',[
                'productionWorker' => $productionWorker->getId()
            ])
            ->andWhere('aw.type = :type:',[
                'type' => \AbsenceWork::TYPE_VACATION
            ])
            ->getQuery()
            ->execute();


        if ($result->count()) {
            return $result->toArray()[0]['VACATION'];
        }

        return null;
    }

    public function getTotalPayedVacation(\ProductionWorker $productionWorker, DateTime $dateFrom, DateTime $dateTo)
    {
        $result = $this->createQueryBuilder(null, 'aw')
            ->columns([
                'SUM(DATEDIFF(endDate,startDate)+1) PAID_VACATION'
            ])
            ->betweenWhere('aw.startDate', $dateFrom->getUnixFormat(), $dateTo->getUnixFormat())
            ->andWhere('aw.productionWorkerId = :productionWorker:',[
                'productionWorker' => $productionWorker->getId()
            ])
            ->andWhere('aw.type = :type:',[
                'type' => \AbsenceWork::TYPE_PAID
            ])
            ->getQuery()
            ->execute();


        if ($result->count()) {
            return $result->toArray()[0]['PAID_VACATION'];
        }

        return null;
    }
}
