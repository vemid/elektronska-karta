<?php

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Date\DateTime;
use Vemid\Application\Di;

/**
 * Class CuttingJobWorksheetRepository
 * @package Vemid\Entity\Repository
 */

class CuttingJobWorksheetRepository extends EntityRepository
{
    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \CuttingJobWorksheet::class);
    }

    /**
     * @param $date
     * @param \ProductionWorker $productionWorker
     * @return array
     */
    public function getTotalPaymentPerWorkerAndDateCuttingJobs($date,\ProductionWorker $productionWorker)
    {
        $result = $this->createQueryBuilder(null, 'cjw')
            ->columns([
                'SUM(cjw.qty) as total',
                'sum(cjw.qty*cj.piecePrice) as money'
            ])
            ->leftJoin(\CuttingJob::class, 'cj.id = cjw.cuttingJobId', 'cj')
            ->where('cjw.date <= :dateFrom: ', [
                'dateFrom' => $date
            ])
            ->andWhere('cjw.date >= :dateTo:', [
                'dateTo' => $date,
            ])
            ->andWhere('cjw.productionWorkerId = :workerId:', [
                'workerId' => $productionWorker->getId()
            ])
            ->groupBy('cjw.date')
            ->getQuery()
            ->execute();

        if ($result->count() > 0) {
            return $result->toArray()[0]['money'];
        }
        else return null;

    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     * @param \ProductionWorker $productionWorker
     * @return array
     */
    public function getTotalPaymentPerWorkerAndPeriod(DateTime $dateFrom, DateTime $dateTo, \ProductionWorker $productionWorker)
    {
        $result = $this->createQueryBuilder(null, 'cjw')
            ->columns([
                'cjw.date',
                'SUM(cjw.qty) as totalTime',
                'sum(cjw.qty*cj.piecePrice) as money'
            ])
            ->leftJoin(\CuttingJob::class, 'cj.id = cjw.cuttingJobId', 'cj')
            ->betweenWhere('cjw.date', $dateFrom->getUnixFormat(), $dateTo->getUnixFormat())
            ->andWhere('cjw.productionWorkerId = :workerId:', [
                'workerId' => $productionWorker->getId()
            ])
            ->groupBy('cjw.productionWorkerId,cjw.date')
            ->getQuery()
            ->execute();

        if ($result->count()) {
            return $result->toArray();
        }

        return null;
    }

    /**
     * @return int
     */
    public function getCuttingJobsData()
    {


        $sqlString = <<<SQL
 SELECT year(cjw.date)                      year,
       Monthname(cjw.date)                  month,
       c.name                               sector,
       Concat(pw.name, ' ', pw.last_name)   worker,
       Concat(pw2.name, ' ', pw2.last_name) manager,
       cj.name                              cutting_job,
       cjw.qty                              quantity,
       round( cj.piece_price * cjw.qty,2 )         value
FROM   `cutting_job_worksheets` cjw
       LEFT JOIN `cutting_jobs` cj
              ON cj.id = cjw.`cutting_job_id`
       LEFT JOIN `production_workers` pw
              ON pw.id = cjw.`production_worker_id`
       LEFT JOIN `production_workers` pw2
              ON pw2.id = cjw.`manager_id`
       LEFT JOIN `codes` c
              ON c.id = pw.`code_id`  
ORDER BY month(cjw.date)
SQL;

        /** @var Di $di */
        $di = Di::getDefault();
        $result = $di->getDb()->fetchAll($sqlString);

        return $result;
    }
}