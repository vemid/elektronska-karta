<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 23.2.18.
 * Time: 22.56
 */

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Phalcon\Mvc\View\Simple;
use Phalcon\Mvc\Model\Resultset;
use Vemid\Entity\Manager\EntityManager;


/**
 * Class WarrantShopRepository
 * @package Vemid\Entity\Repository
 */
class DealershipWarrantRepository extends EntityRepository
{

    /** @var ManagerInterface */
    private $modelsManager;

    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        $this->modelsManager = $modelManager;

        parent::__construct($modelManager, \DealershipWarrant::class);
    }

    /**
     * @param $date
     * @param $shopId
     * @return int
     */
    public function getStatus($date,$shopId)
    {


        $results = $this->createQueryBuilder()
            ->columns([
                'dw.status'
            ])
            ->addFrom(\DealershipWarrant::class, 'dw')
            ->leftJoin(\Dealership::class, 'd.id = dw.dealershipId', 'd')
            ->andWhere('d.codeId = :codeId:', [
                'codeId' => $shopId
            ])
            ->andWhere('dw.date = :date:', [
                'date' => $date
            ])
            ->groupBy('dw.status')
            ->getQuery()
            ->execute();

        $status = (int)$results->toArray()[0]['status'];

        $checkStatus = 0;
        if($status === 1 && \count($results) === 1) {
            $checkStatus = 1;
        } elseif (\count($results) === 2){
            $checkStatus = 2;
        }

        return $checkStatus;
    }
}
