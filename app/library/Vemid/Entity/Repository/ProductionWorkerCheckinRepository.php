<?php
/**
 * Created by PhpStorm.
 * User: adminbk
 * Date: 8.5.18.
 * Time: 23.33
 */

namespace Vemid\Entity\Repository;

use Phalcon\Forms\Element\Date;
use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Date\DateTime;
use Vemid\Application\Di;

/**
 * Class ProductionWorkerCheckinRepository
 * @package Vemid\Entity\Repository
 */

class ProductionWorkerCheckinRepository extends EntityRepository
{
    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \ProductionWorkerCheckin::class);
    }


    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     * @param string $sectorName
     * @return array
     */
    public function getSectorCheckins($sectorName , DateTime $dateFrom, DateTime $dateTo)
    {
        $query = $this->createQueryBuilder(null, 'pwc')
            ->columns([
                'pw.id as worker',
                'pwc.date as date',
                'pwc.checkIn as checkIn',
                'pwc.checkOut as checkOut',
            ])
            ->leftJoin(\ProductionWorker::class, 'pw.id = pwc.workerId', 'pw')
            ->leftJoin(\Code::class, 'c.id = pw.codeId', 'c')
            ->where('c.name = :codeName:', [
                'codeName' => $sectorName
            ])
            ->andWhere('pwc.date >= :dateFrom:',[
                'dateFrom' => $dateFrom
            ])
            ->andWhere('pwc.date <= :dateTo:',[
                'dateTo' => $dateTo
            ])
            ->getQuery()
            ->execute();

        if ($query->count() > 0) {
            return $query->toArray();
        }

        return null;
    }




}