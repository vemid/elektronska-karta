<?php

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;

/**
 * Class ProductionOrderRepository
 * @package Vemid\Entity\Repository
 */
class ProductionOrderRepository extends EntityRepository
{
    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \ProductionOrder::class);
    }

    /**
     * @param \Product $product
     * @return int
     */
    public function getTotalPlanned(\Product $product): int
    {
        $result = $this->createQueryBuilder(null, 'po')
            ->columns([
                'SUM(realizedAQty+realizedBQty) as totalAmount'
            ])
            ->leftJoin(\ProductSize::class, 'ps.id = po.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->where('p.' . \Product::PROPERTY_ID . ' = :id:', [
                'id' => $product->getId()
            ])
            ->getQuery()
            ->execute();

        return (int)$result->toArray()[0]['totalAmount'];
    }

    /**
     * @param \Product $product
     * @return int
     */
    public function getTotalRealizedA(\Product $product): int
    {
        $result = $this->createQueryBuilder(null, 'po')
            ->columns([
                'SUM(realizedAQty) as totalAmount'
            ])
            ->leftJoin(\ProductSize::class, 'ps.id = po.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->where('p.' . \Product::PROPERTY_ID . ' = :id:', [
                'id' => $product->getId()
            ])
            ->getQuery()
            ->execute();

        return (int)$result->toArray()[0]['totalAmount'];
    }

    /**
     * @param \Product $product
     * @return int
     */
    public function getTotalRealizedB(\Product $product): int
    {
        $result = $this->createQueryBuilder(null, 'po')
            ->columns([
                'SUM(realizedBQty) as totalAmount'
            ])
            ->leftJoin(\ProductSize::class, 'ps.id = po.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->where('p.' . \Product::PROPERTY_ID . ' = :id:', [
                'id' => $product->getId()
            ])
            ->getQuery()
            ->execute();

        return (int)$result->toArray()[0]['totalAmount'];
    }


    /**
     * @param \Product $product
     * @return int
     */
    public function getTotalReceived(\Product $product): int
    {
        $result = $this->createQueryBuilder(null, 'po')
            ->columns([
                'SUM(receivedAQty+receivedBQty) as totalAmount'
            ])
            ->leftJoin(\ProductSize::class, 'ps.id = po.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->where('p.' . \Product::PROPERTY_ID . ' = :id:', [
                'id' => $product->getId()
            ])
            ->getQuery()
            ->execute();

        return (int)$result->toArray()[0]['totalAmount'];
    }

    /**
     * @param \Product $product
     * @return int
     */
    public function getTotalReceivedA(\Product $product): int
    {
        $result = $this->createQueryBuilder(null, 'po')
            ->columns([
                'SUM(receivedAQty) as totalAmount'
            ])
            ->leftJoin(\ProductSize::class, 'ps.id = po.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->where('p.' . \Product::PROPERTY_ID . ' = :id:', [
                'id' => $product->getId()
            ])
            ->getQuery()
            ->execute();

        return (int)$result->toArray()[0]['totalAmount'];
    }

    /**
     * @param \Product $product
     * @return int
     */
    public function getTotalReceivedB(\Product $product): int
    {
        $result = $this->createQueryBuilder(null, 'po')
            ->columns([
                'SUM(receivedBQty) as totalAmount'
            ])
            ->leftJoin(\ProductSize::class, 'ps.id = po.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->where('p.' . \Product::PROPERTY_ID . ' = :id:', [
                'id' => $product->getId()
            ])
            ->getQuery()
            ->execute();

        return (int)$result->toArray()[0]['totalAmount'];
    }
}
