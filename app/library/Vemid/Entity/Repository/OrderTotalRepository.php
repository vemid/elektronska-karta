<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 2/23/19
 * Time: 16:50
 */

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Mvc\Model\Resultset\Simple;
use Vemid\Entity\Type;

/**
 * Class OrderTotalRepository
 * @package Vemid\Entity\Repository
 */
class OrderTotalRepository extends EntityRepository
{

    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \OrderTotal::class);
    }

    /**
     * @param \Product $product
     * @return array
     */
    public function getOrderTotalPerProduct(\Product $product)
    {
        $query = $this->createQueryBuilder(null, 'oi')
            ->columns([
                'SUM(oi.finalQty) as komada',
            ])
            ->leftJoin(\ProductSize::class, 'ps.id = oi.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->where('p.id = :productId:', [
                'productId' => $product->getId()
            ])
            ->getQuery()
            ->execute();

        if ($query->count() > 0) {
            return $query;
        }

        return null;
    }
}