<?php
/**
 * Created by PhpStorm.
 * User: adminbk
 * Date: 8.5.18.
 * Time: 23.33
 */

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Date\DateTime;

/**
 * Class CuttingJobRepository
 * @package Vemid\Entity\Repository
 */

class CuttingJobRepository extends EntityRepository
{
    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \CuttingJob::class);
    }


}