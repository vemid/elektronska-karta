<?php

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Phalcon\Mvc\Model\Resultset;

/**
 * Class RoleRepository
 *
 * @package Arbor\Crm\Entity\Repository
 */
class RoleRepository extends EntityRepository
{
    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \Role::class);
    }

    /**
     * @param string $code
     * @return boolean|\Role
     */
    public function findOneByCode($code)
    {
        return $this->findOne([
            'code = :code:',
            'bind' => [
                'code' => $code
            ]
        ]);
    }

    /**
     * @param array|null $excludeRole
     * @return \Role[]|Resultset
     */
    public function findAll(array $excludeRole = null)
    {
        return $this->find([
            'code NOt IN ({excludeRoles:array})',
            'bind' => [
                'excludeRoles' => $excludeRole
            ]
        ]);
    }
}
