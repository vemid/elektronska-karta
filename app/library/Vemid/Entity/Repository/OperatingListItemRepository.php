<?php

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Application\Di;
use Vemid\Date\DateTime;

/**
 * Class OperatingListItemRepository
 * @package Vemid\Entity\Repository
 */
class OperatingListItemRepository extends EntityRepository
{
    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        parent::__construct($modelManager, \OperatingListItem::class);
    }

    /**
     * @param DateTime $date
     * @return array
     */
    public function getTotalQtyTillDay(DateTime $date)
    {
        $query = $this->createQueryBuilder(null, 'oli')
            ->columns([
                'oli.id as operationId',
                'COALESCE(SUM(oliw.qty), 0) as total',
                'concat(p.code,concat("-",p.name)) as code',
                'olit.name',
                'oliw.date',
                'SUM(oliw.qty*oli.pieceTime) as realizedTime',
                'COUNT(distinct oliw.productionWorkerId) as totalWorker'
            ])
            ->leftJoin(\OperatingListItemType::class, 'oli.operatingListItemTypeId = olit.id', 'olit')
            ->leftJoin(\OperatingList::class, 'oli.operatingListId = ol.id', 'ol')
            ->leftJoin(\Product::class, 'ol.productId = p.id', 'p')
            ->leftJoin(\OperatingListItemWorksheet::class, 'oliw.operatingListItemId = oli.id', 'oliw')
            ->where('oliw.date <= :date:', [
                'date' => $date->getUnixFormat()
            ])
            ->orWhere('oliw.id IS NULL')
            ->groupBy(['oli.id', 'oliw.date']);

        return $query->getQuery()->execute();
    }


    /**
     * @param DateTime $date
     * @param \OperatingList $operatingList
     * @return float
     */
    public function getTotalProducedProduct(\OperatingList $operatingList, DateTime $date)
    {
        $query = $this->createQueryBuilder(null, 'oli')
            ->columns([
                'COALESCE(sum(oliw.qty), 0) as total',
                'oli.id as lista'
            ])
            ->leftJoin(\OperatingList::class, 'oli.operatingListId = ol.id', 'ol')
            ->leftJoin(\OperatingListItemWorksheet::class, 'oliw.operatingListItemId = oli.id', 'oliw')
            ->where('ol.id = :operationListId:', [
                'operationListId' => $operatingList->getId()
            ])
            ->andWhere('oliw.date <= :date: AND oliw.id IS NOT NULL', [
                'date' => $date->getUnixFormat()
            ])
            ->groupBy('oli.id');

        $results = $query->getQuery()->execute()->toArray();

        if (\count($results) < $operatingList->getOperatingListItems()->count()) {
            return 0.00;
        }

        $returnData = [];
        foreach ($results as $result) {
            $returnData[] = (float)$result['total'];
        }

        return $operatingList->getFinished() ? max($returnData) : min($returnData);
    }

    /**
     * @param \OperatingList $operatingList
     * @return bool
     */
    public function getFinishedOperatingList(\OperatingList $operatingList)
    {
        $finishedOperations = $this->find([
            'operatingListId = :operatingListId: AND finished = 1',
            'bind' => [
                'operatingListId' => $operatingList->getId()
            ]
        ])->count();

        if ($operatingList->getOperatingListItems()->count() == $finishedOperations) {
            return true;
        } else return false;
    }

    /**
     * @param \OperatingList $operatingList
     * @return string
     */
    public function getStartProductionDate(\OperatingList $operatingList)
    {
        $result = $this->createQueryBuilder(null, 'oli')
            ->columns([
                'MIN(oliw.date) as date'
            ])
            ->leftJoin(\OperatingListItemWorksheet::class, 'oliw.operatingListItemId = oli.id', 'oliw')
            ->where('oli.operatingListId = :operationListId:', [
                'operationListId' => $operatingList->getId()
            ])
            ->getQuery()
            ->execute();

        return (string)$result->toArray()[0]['date'];
    }

    /**
     * @param \OperatingList $operatingList
     * @return string
     */
    public function getEndProductionDate(\OperatingList $operatingList)
    {
        $result = $this->createQueryBuilder(null, 'oli')
            ->columns([
                'MAX(oliw.date) as date'
            ])
            ->leftJoin(\OperatingListItemWorksheet::class, 'oliw.operatingListItemId = oli.id', 'oliw')
            ->where('oli.operatingListId = :operationListId:', [
                'operationListId' => $operatingList->getId()
            ])
            ->getQuery()
            ->execute();

        return (string)$result->toArray()[0]['date'];
    }
}
