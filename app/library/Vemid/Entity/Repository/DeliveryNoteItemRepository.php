<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 11/13/18
 * Time: 15:07
 */

namespace Vemid\Entity\Repository;

use Phalcon\Mvc\Model\ManagerInterface;

/**
 * Class DeliveryNoteItemRepository
 * @package Vemid\Entity\Repository
 */
class DeliveryNoteItemRepository extends EntityRepository
{
    /** @var ManagerInterface */
    private $modelsManager;

    /**
     * @param ManagerInterface $modelManager
     */
    public function __construct(ManagerInterface $modelManager)
    {
        $this->modelsManager = $modelManager;

        parent::__construct($modelManager, \DeliveryNoteItem::class);
    }

    /**
     * @param \DeliveryNote $deliveryNote
     * @return array
     */
    public function getSumItems(\DeliveryNote $deliveryNote)
    {


        $results = $this->createQueryBuilder(null, 'dni')
            ->columns([
                'p.code as code',
                'c.code as size',
                'sum(dni.quantityA) as quantity',
                'dni.purchasePrice as purchasePrice'
            ])
            ->leftJoin(\OrderItem::class, 'oi.id = dni.orderItemId', 'oi')
            ->leftJoin(\ProductSize::class,'ps.id = oi.productSizeId', 'ps')
            ->leftJoin(\Product::class,'p.id = ps.productId','p')
            ->leftJoin(\Code::class,'c.id = ps.codeId','c')
            ->where('dni.deliveryNoteId = :deliveryNoteId:', [
                'deliveryNoteId' => $deliveryNote->getId()
            ])
            ->groupBy(['p.code','c.code'])
            ->getQuery()
            ->execute();

        if ($results->count()) {
            return $results->toArray();
        }

        return null;
    }

    /**
     * @param $productCode
     * @return int
     */
    public function getStatus($productCode)
    {


        $results = $this->createQueryBuilder(null, 'dni')
            ->columns([
                'ifnull(sum(dni.quantityA),0) as qty'
            ])
            ->leftJoin(\OrderItem::class,'oi.id = dni.orderItemId','oi')
            ->leftJoin(\ProductSize::class, 'ps.id = oi.productSizeId', 'ps')
            ->leftJoin(\Product::class,'p.id = ps.productId','p')
            ->andWhere('p.code = :productCode:', [
                'productCode' => $productCode
            ])
            ->getQuery()
            ->execute();

        $qty = (int)$results->toArray()[0]['qty'];

        if ($results->count()) {
            return $qty;
        }

        return null;
    }

}