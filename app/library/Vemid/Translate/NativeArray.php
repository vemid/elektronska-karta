<?php

namespace Vemid\Translate;

use Phalcon\Translate;

/**
 * Class NativeArray
 *
 * @package Vemid\Translate
 * @author Vemid
 */
class NativeArray extends Translate\Adapter implements \ArrayAccess
{

    protected $_translate;

    /**
     * {@inheritdoc}
     */
    public function __construct($options)
    {
        $this->_translate = $options;
        parent::__construct($options);
    }

    /**
     * {@inheritdoc}
     */
    public function _($translateKey, $placeholders = null)
    {
        return $this->query($translateKey, $placeholders);
    }

    /**
     * {@inheritdoc}
     */
    public function t($translateKey, $placeholders = null)
    {
        return $this->query($translateKey, $placeholders);
    }


    /**
     * {@inheritdoc}
     */
    public function query($string, $args = null)
    {
        if (!is_null($args)) {
            if (!is_array($args)) {
                $args = [$args];
            }
            $string = $this->query($string);
            array_unshift($args, $string);
            
            return call_user_func_array('sprintf', $args);
        }

        if (!is_string($string)) {
            return $string;
        }
        
        if (array_key_exists($string, $this->_translate) && strlen($this->_translate[$string])) {
            return $this->_translate[$string];
        }

        return $string;
    }

    /**
     * {@inheritdoc}
     */
    public function exists($index)
    {
        return array_key_exists($index, $this->_translate);
    }

} 
