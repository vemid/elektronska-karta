<?php

namespace Vemid\CodeGenerator;

/**
 * Interface GeneratorInterface
 *
 * @package CodeGenerator
 * @author Vemid
 */
interface GeneratorInterface
{

    /**
     * @return string
     */
    public function getClassName();

    /**
     * @return string
     */
    public function generate();

}
