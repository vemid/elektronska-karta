<?php

namespace Vemid\CodeGenerator;

use Vemid\Filter\CamelCase;

/**
 * Class Controller
 *
 * @package CodeGenerator
 * @author Vemid
 */
class Controller extends AbstractGenerator
{

    /**
     * {@inheritdoc}
     */
    public function getClassName()
    {
        $camelCase = new CamelCase();

        return ucfirst($camelCase->filter($this->getTable()->getName())) . 'Controller';
    }


    /**
     * {@inheritdoc}
     */
    public function generate()
    {
        $output = '<?php' . PHP_EOL;
        $output .= PHP_EOL;
        $output .= 'namespace Backend\Controllers;' . PHP_EOL;
        $output .= PHP_EOL;
        $output .= 'use Vemid\Controller\BaseController;' . PHP_EOL;
        $output .= 'use Vemid\Entity\EntityInterface;' . PHP_EOL;
        $output .= 'use Vemid\Form\Form;' . PHP_EOL;
        $output .= 'use Vemid\Form\Renderer\Json as Renderer;' . PHP_EOL;
        $output .= 'use Vemid\Helper\FormHelper;' . PHP_EOL;
        $output .= 'use Phalcon\Paginator\Adapter\Model as Pagination;' . PHP_EOL;
        $output .= PHP_EOL;

        $output .= '/**' . PHP_EOL;
        $output .= ' * Class ' . $this->getClassName() . PHP_EOL;
        $output .= ' *' . PHP_EOL;
        $output .= ' * @package Backend\Controllers' . PHP_EOL;
        $output .= ' */' . PHP_EOL;

        $output .= 'class ' . $this->getClassName() . ' extends BaseController' . PHP_EOL;
        $output .= '{' . PHP_EOL . PHP_EOL;


        /**
         * Set Index Action
         */
        $this->_generateIndexAction($output);
        $output .= PHP_EOL;


        /**
         * Set Create Action
         */
        $this->_generateCreateAction($output);
        $output .= PHP_EOL;


        /**
         * Set Update Action
         */
        $this->_generateUpdateAction($output);
        $output .= PHP_EOL;


        /**
         * Set Delete Action
         */
        $this->_generateDeleteAction($output);
        $output .= PHP_EOL;


        /**
         * Set Create Form Action
         */
        $this->_generateCreateFormAction($output);
        $output .= PHP_EOL;


        /**
         * Set Update Form Action
         */
        $this->_generateUpdateFormAction($output);
        $output .= PHP_EOL;


        /**
         * Set Get Form Action
         */
        $this->_generateGetFormMethod($output);
        $output .= PHP_EOL;

        /**
         * Set Get Entity Name
         */
        $this->_generateGetEntityNameMethod($output);
        $output .= PHP_EOL;


        /**
         * Set Get Entity
         */
        $this->_generateGetEntityMethod($output);
        $output .= PHP_EOL;

        $output .= '}';
        $output .= PHP_EOL;

        return $output;
    }

    /**
     * @param string $output
     */
    private function _generateIndexAction(&$output)
    {
        $output .= self::INDENT . '/**' . PHP_EOL;
        $output .= self::INDENT . ' * Displays all ' . $this->getTable()->getHumanTableName(false) . PHP_EOL;
        $output .= self::INDENT . ' */' . PHP_EOL;
        $output .= self::INDENT . 'public function indexAction()' . PHP_EOL;
        $output .= self::INDENT . '{' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . '$parameters = [' . PHP_EOL;
        foreach ($this->getTable()->getFields() as $field) {
            $output .= self::INDENT . self::INDENT . self::INDENT . '\\' . $this->getEntityName() . '::PROPERTY_' . strtoupper($field->getName())
                . ' => $this->request->get(\\' . $this->getEntityName() . '::PROPERTY_' . strtoupper($field->getName()) . '),' . PHP_EOL;
        }
        $output .= self::INDENT . self::INDENT . '];' . PHP_EOL;
        $output .= PHP_EOL;

        $output .= self::INDENT . self::INDENT . '$formHelper = new FormHelper(new \\' . $this->getEntityName() . '(), $parameters);' . PHP_EOL;
        $output .= PHP_EOL;

        $output .= self::INDENT . self::INDENT . '$pagination = new Pagination([' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '\'data\' => $formHelper->getCollection(),' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '\'limit\' => 50,' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '\'page\' => $this->request->get(\'page\', \'int\', 1),' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . ']);' . PHP_EOL;
        $output .= PHP_EOL;

        $output .= self::INDENT . self::INDENT . '$this->view->setVar(\'pagination\', $pagination->getPaginate());' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . '$this->view->setVar(\'searchForm\', $formHelper->getSearchForm());' . PHP_EOL;
        $output .= PHP_EOL;

        $output .= self::INDENT . self::INDENT . '$this->_setTitle($this->_translate(\'' . $this->getTable()->getHumanTableName(false) . '\'));' . PHP_EOL;
        $output .= self::INDENT . '}' . PHP_EOL;
    }

    /**
     * @param string $output
     */
    private function _generateCreateAction(&$output)
    {
        $output .= self::INDENT . '/**' . PHP_EOL;
        $output .= self::INDENT . ' * Creates new ' . $this->getHumanEntityName() . PHP_EOL;
        $output .= self::INDENT . ' */' . PHP_EOL;
        $output .= self::INDENT . 'public function createAction()' . PHP_EOL;
        $output .= self::INDENT . '{' . PHP_EOL;

        $output .= self::INDENT . self::INDENT . 'if ($this->request->isPost()) {' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '$form = $this->getForm();' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '$payload = $this->request->getPost();' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . 'if ($form->isValid($payload)) {' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '$form->synchronizeEntityData();' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '$this->entityManager->save($form->getEntity());' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '}' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . '}' . PHP_EOL;
        $output .= self::INDENT . '}' . PHP_EOL;
    }

    /**
     * @param string $output
     */
    private function _generateUpdateAction(&$output)
    {
        $output .= self::INDENT . '/**' . PHP_EOL;
        $output .= self::INDENT . ' * Updates existing ' . $this->getHumanEntityName() . PHP_EOL;
        $output .= self::INDENT . ' * ' . PHP_EOL;
        $output .= self::INDENT . ' * @param int $id' . PHP_EOL;
        $output .= self::INDENT . ' * @return bool' . PHP_EOL;
        $output .= self::INDENT . ' */' . PHP_EOL;
        $output .= self::INDENT . 'public function updateAction($id)' . PHP_EOL;
        $output .= self::INDENT . '{' . PHP_EOL;

        $output .= self::INDENT . self::INDENT . 'if (!$' . lcfirst($this->getEntityName()) . ' = $this->getEntity($id)) {' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . 'return $this->_returnNotFound();' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . '}' . PHP_EOL;
        $output .= PHP_EOL;

        $output .= self::INDENT . self::INDENT . 'if ($this->request->isPost()) {' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '$form = $this->getForm($' . lcfirst($this->getEntityName()) . ');' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '$payload = $this->request->getPost();' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . 'if ($form->isValid($payload)) {' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '$form->synchronizeEntityData();' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '$this->entityManager->save($' . lcfirst($this->getEntityName()) . ');' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '}' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . '}' . PHP_EOL;
        $output .= self::INDENT . '}' . PHP_EOL;
    }

    /**
     * @param string $output
     */
    private function _generateDeleteAction(&$output)
    {
        $output .= self::INDENT . '/**' . PHP_EOL;
        $output .= self::INDENT . ' * Deletes existing ' . $this->getHumanEntityName() . PHP_EOL;
        $output .= self::INDENT . ' * ' . PHP_EOL;
        $output .= self::INDENT . ' * @param int $id' . PHP_EOL;
        $output .= self::INDENT . ' * @return bool' . PHP_EOL;
        $output .= self::INDENT . ' */' . PHP_EOL;
        $output .= self::INDENT . 'public function deleteAction($id)' . PHP_EOL;
        $output .= self::INDENT . '{' . PHP_EOL;

        $output .= self::INDENT . self::INDENT . 'if (!$' . lcfirst($this->getEntityName()) . ' = $this->getEntity($id)) {' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . 'return $this->_returnNotFound();' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . '}' . PHP_EOL;
        $output .= PHP_EOL;

        $output .= self::INDENT . self::INDENT . '$this->entityManager->delete($' . lcfirst($this->getEntityName()) . ');' . PHP_EOL;
        $output .= self::INDENT . '}' . PHP_EOL;
    }

    /**
     * @param string $output
     */
    private function _generateCreateFormAction(&$output)
    {
        $output .= self::INDENT . '/**' . PHP_EOL;
        $output .= self::INDENT . ' * Generates JSON create form' . PHP_EOL;
        $output .= self::INDENT . ' * ' . PHP_EOL;
        $output .= self::INDENT . ' * @return string' . PHP_EOL;
        $output .= self::INDENT . ' */' . PHP_EOL;
        $output .= self::INDENT . 'public function getCreateFormAction()' . PHP_EOL;
        $output .= self::INDENT . '{' . PHP_EOL;

        $output .= self::INDENT . self::INDENT . '$form = $this->getForm();' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . '$renderer = new Renderer();' . PHP_EOL;
        $output .= PHP_EOL;
        $output .= self::INDENT . self::INDENT . 'return $renderer->render($form);' . PHP_EOL;
        $output .= self::INDENT . '}' . PHP_EOL;
    }

    /**
     * @param string $output
     */
    private function _generateUpdateFormAction(&$output)
    {
        $output .= self::INDENT . '/**' . PHP_EOL;
        $output .= self::INDENT . ' * Generates JSON update form' . PHP_EOL;
        $output .= self::INDENT . ' * ' . PHP_EOL;
        $output .= self::INDENT . ' * @param int $id' . PHP_EOL;
        $output .= self::INDENT . ' * @return bool|string' . PHP_EOL;
        $output .= self::INDENT . ' */' . PHP_EOL;
        $output .= self::INDENT . 'public function getUpdateFormAction($id)' . PHP_EOL;
        $output .= self::INDENT . '{' . PHP_EOL;

        $output .= self::INDENT . self::INDENT . 'if (!$' . lcfirst($this->getEntityName()) . ' = $this->getEntity($id)) {' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . 'return $this->_returnNotFound();' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . '}' . PHP_EOL;
        $output .= PHP_EOL;

        $output .= self::INDENT . self::INDENT . '$form = $this->getForm($' . lcfirst($this->getEntityName()) . ');' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . '$renderer = new Renderer();' . PHP_EOL;
        $output .= PHP_EOL;
        $output .= self::INDENT . self::INDENT . 'return $renderer->render($form);' . PHP_EOL;
        $output .= self::INDENT . '}' . PHP_EOL;
    }

    /**
     * @param string $output
     */
    private function _generateGetFormMethod(&$output)
    {
        $output .= self::INDENT . '/**' . PHP_EOL;
        $output .= self::INDENT . ' * @param EntityInterface|null $entity' . PHP_EOL;
        $output .= self::INDENT . ' * @param array $exclude' . PHP_EOL;
        $output .= self::INDENT . ' * @return Form' . PHP_EOL;
        $output .= self::INDENT . ' */' . PHP_EOL;
        $output .= self::INDENT . 'public function getForm(EntityInterface $entity = null, array $exclude = [])' . PHP_EOL;
        $output .= self::INDENT . '{' . PHP_EOL;

        $output .= self::INDENT . self::INDENT . 'if ($entity === null) {' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '$entity = new \\' . $this->getEntityName() . '();' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . '}' . PHP_EOL;
        $output .= PHP_EOL;
        $output .= self::INDENT . self::INDENT . 'return $entity->getForm($exclude);' . PHP_EOL;
        $output .= self::INDENT . '}' . PHP_EOL;
    }

    /**
     * @param string $output
     */
    private function _generateGetEntityNameMethod(&$output)
    {
        $output .= self::INDENT . '/**' . PHP_EOL;
        $output .= self::INDENT . ' * @return string' . PHP_EOL;
        $output .= self::INDENT . ' */' . PHP_EOL;
        $output .= self::INDENT . 'public function getEntityName()' . PHP_EOL;
        $output .= self::INDENT . '{' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . 'return \\' . $this->getEntityName() . '::class;' . PHP_EOL;
        $output .= self::INDENT . '}' . PHP_EOL;
    }

    /**
     * @param string $output
     */
    private function _generateGetEntityMethod(&$output)
    {
        $output .= self::INDENT . '/**' . PHP_EOL;
        $output .= self::INDENT . ' * Gets existing ' . $this->getHumanEntityName() . PHP_EOL;
        $output .= self::INDENT . ' * ' . PHP_EOL;
        $output .= self::INDENT . ' * @param int $id' . PHP_EOL;
        $output .= self::INDENT . ' * @return \\' . $this->getEntityName() . '|EntityInterface|null' . PHP_EOL;
        $output .= self::INDENT . ' */' . PHP_EOL;
        $output .= self::INDENT . 'protected function getEntity($id)' . PHP_EOL;
        $output .= self::INDENT . '{' . PHP_EOL;
        $output .= self::INDENT . self::INDENT . 'return $this->entityManager->findOne(\\' . $this->getEntityName() . '::class, $id);' . PHP_EOL;
        $output .= self::INDENT . '}' . PHP_EOL;
    }

}
