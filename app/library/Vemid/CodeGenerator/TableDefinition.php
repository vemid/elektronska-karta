<?php

namespace Vemid\CodeGenerator;

/**
 * Class TableDefinition
 *
 * @package CodeGenerator
 * @author Vemid
 */
class TableDefinition extends AbstractGenerator
{

    /**
     * {@inheritdoc}
     */
    public function getClassName()
    {
        return $this->getEntityName() . 'TableDefinition';
    }

    /**
     * @return string
     */
    public function generate()
    {
        $output = '<?php' . PHP_EOL . PHP_EOL;
        $output .= 'use \\Table\\Column;' . PHP_EOL . PHP_EOL;
        $output .= '/**' . PHP_EOL;
        $output .= ' * ' . $this->getClassName() . ' Class' . PHP_EOL;
        $output .= ' *' . PHP_EOL;
        $output .= ' * @package Table' . PHP_EOL;
        $output .= ' * @subpackage Definitions' . PHP_EOL;
        $output .= ' */' . PHP_EOL;

        $output .= 'class ' . $this->getClassName() . ' extends \\Table\\Definition' . PHP_EOL;
        $output .= '{' . PHP_EOL . PHP_EOL;


        /**
         * Set Class constants
         */
        $this->_generateConstants($output);
        $output .= PHP_EOL;

        /**
         * Set Class construct method
         */
        $this->_generateConstructor($output);
        $output .= PHP_EOL;

        $output .= '}';

        return $output;

    }

    /**
     * @param string $output
     */
    private function _generateConstants(&$output)
    {
        foreach ($this->getTable()->getFields() as $field) {
            $output .= self::INDENT . 'const ' . strtoupper($field->getName())
                . ' = \'' . $field->getPropertyName() . '\';' . PHP_EOL;
        }
    }

    /**
     * @param string $output
     */
    private function _generateConstructor(&$output)
    {
        $output .= self::INDENT . '/**' . PHP_EOL;
        $output .= self::INDENT . ' * Constructor' . PHP_EOL;
        $output .= self::INDENT . ' */' . PHP_EOL;
        $output .= self::INDENT . 'public function __construct()' . PHP_EOL;
        $output .= self::INDENT . '{' . PHP_EOL;

        foreach ($this->getTable()->getFields() as $field) {
            $output .= self::INDENT . self::INDENT . '$this->addColumn(self::' . strtoupper($field->getName()) . ')' . PHP_EOL;

            if ($field->isPrimaryKey()) {
                $output .= self::INDENT . self::INDENT . self::INDENT . '->setPrimaryKey(true)' . PHP_EOL;
            }

            if ($field->isInteger()) {
                $output .= self::INDENT . self::INDENT . self::INDENT . '->setDataType(Column::DATA_TYPE_INT)' . PHP_EOL;
            } elseif ($field->isFloat()) {
                $output .= self::INDENT . self::INDENT . self::INDENT . '->setDataType(Column::DATA_TYPE_FLOAT)' . PHP_EOL;
            } elseif ($field->isDate()) {
                $output .= self::INDENT . self::INDENT . self::INDENT . '->setDataType(Column::DATA_TYPE_DATETIME)' . PHP_EOL;
            } elseif ($field->isBoolean()) {
                $output .= self::INDENT . self::INDENT . self::INDENT . '->setDataType(Column::DATA_TYPE_BOOLEAN)' . PHP_EOL;
            } else {
                $output .= self::INDENT . self::INDENT . self::INDENT . '->setDataType(Column::DATA_TYPE_STRING)' . PHP_EOL;
            }

            $output .= self::INDENT . self::INDENT . self::INDENT . '->setColumnLabel(\'' . $field->getHumanName() . '\')' . PHP_EOL;
            $output .= PHP_EOL;
        }

        $output .= self::INDENT . '}' . PHP_EOL;
    }

}
