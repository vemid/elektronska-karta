<?php

namespace Vemid\CodeGenerator;

use Vemid\Database\Table;

/**
 * Class AbstractGenerator
 *
 * @package CodeGenerator
 * @author Vemid
 */
abstract class AbstractGenerator implements GeneratorInterface
{

    const INDENT = '    ';

    /** @var Table */
    private $table;

    /**
     * @param Table $table
     */
    public function __construct(Table $table)
    {
        $this->table = $table;
    }

    /**
     * @return Table
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @return string
     */
    protected function getEntityName()
    {
        return $this->getTable()->getEntityName();
    }

    /**
     * @return string
     */
    protected function getHumanEntityName()
    {
        return $this->getTable()->getHumanTableName(true);
    }

}
