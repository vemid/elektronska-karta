<?php

namespace Vemid\CodeGenerator;

use Vemid\Filter\CamelCase;
use Vemid\Filter\Humanize;
use Vemid\Filter\Singular;

/**
 * Class Model
 *
 * @package ClassGenerator
 * @author Vemid
 */
class Model extends AbstractGenerator
{

    /**
     * {@inheritdoc}
     */
    public function getClassName()
    {
        return $this->getEntityName();
    }

    /**
     * @return string
     */
    public function generate()
    {
        $output = '<?php' . PHP_EOL . PHP_EOL;
        $output .= 'use Vemid\Date\DateTime;' . PHP_EOL;
        $output .= 'use Vemid\Entity\AbstractEntity;' . PHP_EOL;
        $output .= 'use Phalcon\Mvc\Model\Resultset;' . PHP_EOL;
        $output .= PHP_EOL;

        /**
         * Generate Class Doc Blocks
         */
        $this->_generateDocBlock($output);


        $output .= 'class ' . $this->getClassName() . ' extends AbstractEntity' . PHP_EOL;
        $output .= '{' . PHP_EOL . PHP_EOL;


        /**
         * Set Class constants
         */
        $this->_generateConstants($output);


        /**
         * Set Class properties
         */
        $this->_generateProperties($output);


        /**
         * Set Class REST definition
         */
        $this->_generateRestRepresentationDefinition($output);


        /**
         * Set Class methods (getters and setters)
         */
        $this->_generateGettersAndSetters($output);


        /**
         * Set get/set related methods
         */
        $this->_generateRelatedEntityGettersAndSetters($output);


        $output .= '}' . PHP_EOL;

        return $output;
    }

    /**
     * @param string $output
     */
    private function _generateDocBlock(&$output)
    {
        $output .= '/**' . PHP_EOL;
        $output .= ' * ' . $this->getClassName() . ' Model Class' . PHP_EOL;
        $output .= ' *' . PHP_EOL;
        $output .= ' * @Source(\'' . $this->getTable()->getName() . '\')' . PHP_EOL;

        // Generates BELONGS_TO relationships
        $foreignFields = $this->getTable()->getForeignFields();
        if (!empty($foreignFields)) {
            $output .= ' *' . PHP_EOL;
            foreach ($foreignFields as $foreignField) {
                $output .= ' * @BelongsTo(\'' . $foreignField->getPropertyName() . '\', \''
                    . $foreignField->getReferenceTable()->getEntityName() . '\', \'id\', {\'alias\':\''
                    . ucfirst($foreignField->getReferenceTable()->getEntityName()) . '\'})' . PHP_EOL;
            }
        }

        // Generate HAS_MANY relationships
        $singular = new Singular();
        $camelCase = new CamelCase();

//        var_dump($this->getTable()->getAdapter()->listTables());die;

        $referencedFieldName = $singular->filter($this->getTable()->getName()) . '_id';
        foreach ($this->getTable()->getAdapter()->listTables() as $table) {
            if (!$table || empty($table->getFields())) {
                continue;
            }

            foreach ($table->getFields() as $field) {
                if ($field->getName() === $referencedFieldName) {
                    $output .= ' * @HasMany(\'id\', \'' . $table->getEntityName() . '\', \''
                        . $camelCase->filter($referencedFieldName) . '\', {\'alias\':\''
                        . ucfirst($camelCase->filter($table->getName())) . '\'})' . PHP_EOL;
                }
            }
        }

        $output .= ' */' . PHP_EOL;
    }

    /**
     * @param string $output
     */
    private function _generateProperties(&$output)
    {
        $humanize = new Humanize();

        foreach ($this->getTable()->getFields() as $field) {
            if ($field->isPrimaryKey()) {
                $output .= self::INDENT . 'protected $_objectId = self::PROPERTY_' . strtoupper($field->getName()) . ';' . PHP_EOL;
            }
            break;
        }

        foreach ($this->getTable()->getFields() as $field) {
            $output .= PHP_EOL . self::INDENT . '/**' . PHP_EOL;

            if ($field->isPrimaryKey()) {
                $output .= self::INDENT . ' * @Primary' . PHP_EOL;
                $output .= self::INDENT . ' * @Identity' . PHP_EOL;
            }

            $output .= self::INDENT . ' * @Column(column="' . $field->getName() . '", type="' . $field->getTypeDisplayName()
                . '", nullable=' . ($field->isNotNull() ? 'false' : 'true') . ')' . PHP_EOL;

            if (!$field->isPrimaryKey()) {
                $output .= self::INDENT . ' * @FormElement(label="' . ucfirst($humanize->filter(str_replace('_id', '', $field->getName())))
                    . '", type="' . $field->getFormElementType()
                    . '", required=' . ($field->isNotNull() ? 'true' : 'false')
                    . ($field->isReferenceKey() ? ', relation="' . $field->getReferenceTable()->getEntityName() . '"' : '')
                    . ')' . PHP_EOL;
            }

            $output .= self::INDENT . ' */' . PHP_EOL;

            $output .= self::INDENT . 'protected $' . $field->getPropertyName() . ';' . PHP_EOL;
        }

        $output .= PHP_EOL;
    }

    /**
     * @param string $output
     */
    private function _generateRestRepresentationDefinition(&$output)
    {
        $output .= self::INDENT . 'protected $_restRepresentationDefinition = array(' . PHP_EOL;

        foreach ($this->getTable()->getFields() as $field) {
            $output .= self::INDENT . self::INDENT . 'self::PROPERTY_' . strtoupper($field->getName()) . ' => true,' . PHP_EOL;
        }

        foreach ($this->getTable()->getForeignFields() as $field) {
            $output .= self::INDENT . self::INDENT . 'self::ALIAS_' . strtoupper($field->getReferenceTable()->getEntityName(true))
                . ' => true,' . PHP_EOL;
        }

        $output .= self::INDENT . ');' . PHP_EOL;
        $output .= PHP_EOL;
    }

    /**
     * @param string $output
     */
    private function _generateConstants(&$output)
    {
        foreach ($this->getTable()->getFields() as $field) {
            $output .= self::INDENT . 'const PROPERTY_' . strtoupper($field->getName())
                . ' = \'' . $field->getPropertyName() . '\';' . PHP_EOL;
        }

        $output .= PHP_EOL;

        /**
         * Get BELONGS TO relationships
         */
        foreach ($this->getTable()->getForeignFields() as $field) {
            $output .= self::INDENT . 'const ALIAS_' . strtoupper($field->getReferenceTable()->getEntityName(true))
                . ' = \'' . ucfirst($field->getReferenceTable()->getEntityName()) . '\';' . PHP_EOL;
        }


        /**
         * Get HAS MANY relationships
         */
        $singular = new Singular();
        $camelCase = new CamelCase();
        $referencedFieldName = $singular->filter($this->getTable()->getName()) . '_id';
        foreach ($this->getTable()->getAdapter()->listTables() as $table) {
            if (!$table || empty($table->getFields())) {
                continue;
            }

            foreach ($table->getFields() as $field) {
                if ($field->getName() === $referencedFieldName) {
                    $output .= self::INDENT . 'const ALIAS_' . strtoupper($table->getName())
                        . ' = \'' . ucfirst($camelCase->filter($table->getName())) . '\';' . PHP_EOL;
                }
            }

        }

        $output .= PHP_EOL;
    }

    /**
     * @param string $output
     */
    private function _generateGettersAndSetters(&$output)
    {
        foreach ($this->getTable()->getFields() as $field) {

            if ($field->isBoolean()) {
                $return = 'bool';
                $param = 'int|bool';
                $getterBody = 'return (bool)$this->' . $field->getPropertyName() . ';';
                $setterBody = '$this->' . $field->getPropertyName() . ' = $' . $field->getPropertyName() . ' ? 1 : 0;';
            } else if ($field->isInteger()) {
                $return = 'int';
                $param = 'int';
                $getterBody = 'return $this->' . $field->getPropertyName() .  ';';
                $setterBody = '$this->' . $field->getPropertyName() . ' = null !== $' . $field->getPropertyName() . ' ? (int)$' . $field->getPropertyName() . ' : null;';
            } else if ($field->isDate()) {
                $return = 'null|DateTime';
                $param = 'string|DateTime';
                $getterBody = 'return $this->' . $field->getPropertyName() . ';';
                $setterBody = '$this->' . $field->getPropertyName() . ' = $' . $field->getPropertyName() . ';';
            } else if ($field->isFloat()) {
                $return = 'float';
                $param = 'float';
                $getterBody = 'return $this->' . $field->getPropertyName() . ';';
                $setterBody = '$this->' . $field->getPropertyName() . ' = null !== $' . $field->getPropertyName() . ' ? (float)$' . $field->getPropertyName() . ' : null;';
            } else {
                $return = 'string';
                $param = 'string';
                $getterBody = 'return $this->' . $field->getPropertyName() . ';';
                $setterBody = '$this->' . $field->getPropertyName() . ' = $' . $field->getPropertyName() . ';';
            }

            // Generate get method
            $output .= self::INDENT . '/**' . PHP_EOL;
            $output .= self::INDENT . ' * @return ' . $return . PHP_EOL;
            $output .= self::INDENT . ' */' . PHP_EOL;
            $output .= self::INDENT . 'public function get' . ucfirst($field->getPropertyName()) . '()' . PHP_EOL;
            $output .= self::INDENT . '{' . PHP_EOL;
            $output .= self::INDENT . self::INDENT . $getterBody . PHP_EOL;
            $output .= self::INDENT . '}' . PHP_EOL . PHP_EOL;

            // Generate set method
            $output .= self::INDENT . '/**' . PHP_EOL;
            $output .= self::INDENT . ' * @param ' . $param . ' $' . $field->getPropertyName() . PHP_EOL;
            $output .= self::INDENT . ' * @return $this' . PHP_EOL;
            $output .= self::INDENT . ' */' . PHP_EOL;
            $output .= self::INDENT . 'public function set' . ucfirst($field->getPropertyName()) . '($' . $field->getPropertyName() . ')' . PHP_EOL;
            $output .= self::INDENT . '{' . PHP_EOL;
            $output .= self::INDENT . self::INDENT . $setterBody . PHP_EOL . PHP_EOL;
            $output .= self::INDENT . self::INDENT . 'return $this;' . PHP_EOL;
            $output .= self::INDENT . '}' . PHP_EOL . PHP_EOL;
        }

    }

    /**
     * @param string $output
     */
    private function _generateRelatedEntityGettersAndSetters(&$output)
    {
        foreach ($this->getTable()->getForeignFields() as $field) {
            if ($referenceTable = $field->getReferenceTable()) {
                $relatedEntityName = $referenceTable->getEntityName();

                // Generate get method
                $output .= self::INDENT . '/**' . PHP_EOL;
                $output .= self::INDENT . ' * @return null|' . $relatedEntityName . '' . PHP_EOL;
                $output .= self::INDENT . ' * ' . PHP_EOL;
                $output .= self::INDENT . ' * @throws \\DomainException' . PHP_EOL;
                $output .= self::INDENT . ' */' . PHP_EOL;
                $output .= self::INDENT . 'public function get' . $relatedEntityName . '()' . PHP_EOL;
                $output .= self::INDENT . '{' . PHP_EOL;
                $output .= self::INDENT . self::INDENT . 'return $this->getRelated(self::ALIAS_' . strtoupper($referenceTable->getEntityName(true)) . ');' . PHP_EOL;
                $output .= self::INDENT . '}' . PHP_EOL . PHP_EOL;

                // Generate set method
                $output .= self::INDENT . '/**' . PHP_EOL;
                $output .= self::INDENT . ' * @param ' . $relatedEntityName . ' $' . lcfirst($relatedEntityName) . PHP_EOL;
                $output .= self::INDENT . ' * @return $this' . PHP_EOL;
                $output .= self::INDENT . ' */' . PHP_EOL;
                $output .= self::INDENT . 'public function set' . $relatedEntityName . '(' . $relatedEntityName . ' $' . lcfirst($relatedEntityName) . ')' . PHP_EOL;
                $output .= self::INDENT . '{' . PHP_EOL;
                $output .= self::INDENT . self::INDENT . '$this->' . $field->getPropertyName() . ' = $' . lcfirst($relatedEntityName) . '->getId();' . PHP_EOL . PHP_EOL;
                $output .= self::INDENT . self::INDENT . 'return $this;' . PHP_EOL;
                $output .= self::INDENT . '}' . PHP_EOL . PHP_EOL;
            }
        }

        $singular = new Singular();
        $referencedFieldName = $singular->filter($this->getTable()->getName()) . '_id';
        $camelCase = new CamelCase();

        foreach ($this->getTable()->getAdapter()->listTables() as $table) {
            if (!$table || empty($table->getFields())) {
                continue;
            }

            foreach ($table->getFields() as $field) {
                if ($field->getName() === $referencedFieldName) {

                    // Generate get method
                    $output .= self::INDENT . '/**' . PHP_EOL;
                    $output .= self::INDENT . ' * @param null|string|array $arguments' . PHP_EOL;
                    $output .= self::INDENT . ' * @return Resultset|' . $table->getEntityName() . '[]' . PHP_EOL;
                    $output .= self::INDENT . ' * ' . PHP_EOL;
                    $output .= self::INDENT . ' * @throws \\DomainException' . PHP_EOL;
                    $output .= self::INDENT . ' */' . PHP_EOL;
                    $output .= self::INDENT . 'public function get' . ucfirst($camelCase->filter($table->getName())) . '($arguments = null)' . PHP_EOL;
                    $output .= self::INDENT . '{' . PHP_EOL;
                    $output .= self::INDENT . self::INDENT . 'return $this->getRelated(self::ALIAS_' . strtoupper($table->getName()) . ', $arguments);' . PHP_EOL;
                    $output .= self::INDENT . '}' . PHP_EOL . PHP_EOL;
                }
            }

        }

    }

}
