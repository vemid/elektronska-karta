<?php

namespace Vemid\CodeGenerator;

/**
 * Class FileWriter
 *
 * @package CodeGenerator
 * @author Vemid
 */
class FileWriter
{

    /** @var GeneratorInterface */
    private $codeGenerator;

    /**
     * @param GeneratorInterface $codeGenerator
     */
    public function __construct(GeneratorInterface $codeGenerator)
    {
        $this->codeGenerator = $codeGenerator;
    }

    /**
     * @param string $path
     * @param bool $overwrite
     * @return bool
     */
    public function write($path, $overwrite = false)
    {
        if (is_dir($path)) {
            $file = $path . $this->codeGenerator->getClassName() . '.php';
            if ($overwrite || !file_exists($file)) {
                $fp = fopen($file, 'w');
                $return = fwrite($fp, $this->codeGenerator->generate());
                fclose($fp);

                return (bool)$return;
            }
        }

        return false;
    }

}
