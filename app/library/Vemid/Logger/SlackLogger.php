<?php

namespace Vemid\Logger;

use Vemid\Services\Messaging\SlackInterface;
use Phalcon\Logger;
use Psr\Log\LoggerInterface;

/**
 * Class SlackLogger
 *
 * @package Vemid\Logger
 */
class SlackLogger extends Logger\Adapter implements LoggerInterface
{
    /** @var SlackInterface */
    protected $slack;
    protected $channel;
    protected $username;
    protected $icon;

    /**
     * @param SlackInterface $slack
     * @param string $channel
     * @param string $username
     * @param string $icon
     */
    public function __construct(SlackInterface $slack, $channel, $username, $icon = ':tesla:')
    {
        $this->slack = $slack;
        $this->channel = $channel;
        $this->username = $username;
        $this->icon = $icon;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormatter()
    {
        if ($this->_formatter === null) {
            $this->_formatter = new Logger\Formatter\Line('%type%: %message%');
        }

        return $this->_formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function close()
    {

    }

    /**
     * @return Logger\AdapterInterface
     * @throws \RuntimeException|\LogicException
     */
    public function commit()
    {
        if (!$this->_transaction) {
            throw new \LogicException('There is no active transaction');
        }

        $this->_transaction = false;

        $message = '';
        $attachments = [];

        foreach ($this->_queue as $index => $item) {
            /** @var Logger\Item $item */
            if (!$itemMessage = $item->getMessage()) {
                continue;
            }

            if ($index === 0) {
                $message = $itemMessage;
                continue;
            }

            $attachments[] = [
                'color' => $this->getColor($item->getType()),
                'footer' => $itemMessage,
            ];
        }

        if ($message) {
            $this->slack->sendMessage(
                $message,
                $this->channel,
                $this->username,
                ':tesla:',
                $attachments
            );
        }

        $this->_queue = [];

        return $this;
    }

    /**
     * @return int
     */
    public function getTotalMessages() : int
    {
        return \count($this->_queue);
    }

    public function __destruct()
    {
        if ($this->_transaction) {
            $this->commit();
        }
    }

    /**
     * @param string $message
     * @param int $type
     * @param int $time
     * @param array $context
     * @throws \RuntimeException
     */
    protected function logInternal($message, $type, $time, array $context)
    {
        $this->slack->sendMessage(
            $this->getFormatter()->format($message, $type, $time, $context),
            $this->channel,
            $this->username,
            $this->icon
        );
    }

    /**
     * @param int $type
     * @return string
     */
    protected function getColor($type)
    {
        switch ($type) {
            case Logger::EMERGENCE:
            case Logger::EMERGENCY:
            case Logger::CRITICAL:
            case Logger::ALERT:
            case Logger::ERROR:
                return '#af124b';
            case Logger::WARNING:
                return '#e07e27';
            default:
                return '#68aa22';
        }
    }
}
