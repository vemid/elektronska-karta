<?php

namespace Vemid\Logger;

use Phalcon\Logger\AdapterInterface;
use Phalcon\Logger\Multiple;

/**
 * Class MultipleLogger
 *
 * @package Vemid\Logger
 */
class MultipleLogger extends Multiple
{
    /**
     * Starts a transaction on all registered loggers
     *
     * @return $this
     */
    public function begin()
    {
        if (empty($this->_loggers)) {
            return $this;
        }

        foreach ($this->_loggers as $logger) {
            /** @var AdapterInterface $logger */
            $logger->begin();
        }

        return $this;
    }

    /**
     * Commits the internal transaction of each registered logger
     *
     * @return $this
     */
    public function commit()
    {
        if (empty($this->_loggers)) {
            return $this;
        }

        foreach ($this->_loggers as $logger) {
            /** @var AdapterInterface $logger */
            $logger->commit();
        }

        return $this;
    }

    /**
     * Rollbacks the internal transaction of each registered logger
     *
     * @return $this
     */
    public function rollback()
    {
        if (empty($this->_loggers)) {
            return $this;
        }

        foreach ($this->_loggers as $logger) {
            /** @var AdapterInterface $logger */
            $logger->rollback();
        }

        return $this;
    }
}
