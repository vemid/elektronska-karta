<?php

namespace Vemid\Logger;

use Phalcon\Logger\Adapter\File;
use Psr\Log\LoggerInterface;

/**
 * Class FileLogger
 *
 * @package Vemid\Logger
 */
class FileLogger extends File implements LoggerInterface
{
    /**
     * @return int
     */
    public function getTotalMessages()
    {
        return \count($this->_queue);
    }

    public function __destruct()
    {
        if ($this->_transaction) {
            $this->commit();
        }
    }
}
