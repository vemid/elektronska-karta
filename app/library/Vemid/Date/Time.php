<?php

namespace Vemid\Date;

/**
 * Class Time
 *
 * @package Vemid\Date
 * @author Vemid
 */
class Time
{
    /** @var int */
    protected $seconds;

    /**
     * @param int $time
     */
    public function __construct($time)
    {
        if (is_numeric($time)) {
            $this->seconds = $time;
        } else {
            $time = preg_replace('/\.\d+/si', '', $time);
            if (preg_match('/^(\d+):(\d{2})$/', $time, $matches)) {
                $this->seconds = ($matches[1] * 3600) + ($matches[2] * 60);
            } elseif (preg_match('/^(\d+):(\d{2}):(\d{2})$/', $time, $matches)) {
                $this->seconds = ($matches[1] * 3600) + ($matches[2] * 60) + $matches[3];
            } else {
                throw new \InvalidArgumentException('Invalid time passed to ' . __CLASS__);
            }
        }
    }

    /**
     * @return int
     */
    public function getHours()
    {
        return floor($this->seconds / 3600);
    }

    /**
     * @return int
     */
    public function getMinutes()
    {
        return floor(($this->seconds - ($this->getHours() * 3600)) / 60);
    }

    /**
     * @return int
     */
    public function getSeconds()
    {
        return $this->seconds - ($this->getHours() * 3600) - ($this->getMinutes() * 60);
    }

    /**
     * @return int
     */
    public function getTotalHours()
    {
        return floor($this->seconds / 3600);
    }

    /**
     * @return int
     */
    public function getTotalMinutes()
    {
        return floor($this->seconds / 60);
    }

    /**
     * @return int
     */
    public function getTotalSeconds()
    {
        return $this->seconds;
    }

    /**
     * @param bool $showSeconds
     * @return string
     */
    public function getShort($showSeconds = false)
    {
        $hours = $this->getHours();
        $minutes = $this->getMinutes();
        $seconds = $this->getSeconds();

        $string = str_pad($hours, 2, '0', STR_PAD_LEFT) . ':' . str_pad($minutes, 2, '0', STR_PAD_LEFT);

        if ($showSeconds) {
            $string .= ':' . str_pad($seconds, 2, '0', STR_PAD_LEFT);
        }

        return $string;
    }

    /**
     * @return string
     */
    public function getLong()
    {
        return $this->getShort(true);
    }

    /**
     * Gets MySQL formatted UTC time string 12:00:00
     *
     * @return string
     */
    public function getUnixFormat()
    {
        return $this->getShort(true);
    }

    /**
     * @param int|string|Time $time
     * @return $this
     */
    public function addTime($time)
    {
        if (is_object($time) && $time instanceof Time) {
            $this->seconds += $time->getTotalSeconds();
        } else {
            $time = new Time($time);
            $this->seconds += $time->getTotalSeconds();
        }

        return $this;
    }

    /**
     * @param $time
     * @return $this
     */
    public function subTime($time)
    {
        if (is_object($time) && $time instanceof Time) {
            $this->seconds -= $time->getTotalSeconds();
        } else {
            $time = new Time($time);
            $this->seconds -= $time->getTotalSeconds();
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getShort(false);
    }

    /**
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if (is_callable(array($this, $name))) {
            return $this->$name;
        }

        if (method_exists($this, $name)) {
            return call_user_func(array($this, $name));
        }

        $getName = 'get' . ucfirst($name);
        if (method_exists($this, $getName)) {
            return call_user_func(array($this, $getName));
        }

        throw new \Exception('Calling undefined property or method ' . __CLASS__ . '::$' . $name);
    }

}
