<?php

namespace Vemid\Date;

/**
 * Class DateRange
 *
 * @package Vemid\Date
 * @author Vemid
 */
class DateRange
{

    /** @var DateTime */
    protected $startDate;

    /** @var DateTime */
    protected $endDate;

    /**
     * @param DateTime $startDate
     * @param DateTime $endDate
     */
    public function __construct(DateTime $startDate = null, DateTime $endDate = null)
    {
        if ($startDate && $endDate && $endDate < $startDate) {
            throw new \InvalidArgumentException('Start date in date range must be before the end date.');
        }

        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return null|DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return null|DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function contains(DateTime $date)
    {
        if (is_null($this->startDate)) {
            if (is_null($this->endDate) || $this->endDate > $date) {
                return true;
            } else {
                return false;
            }
        } elseif (is_null($this->endDate)) {
            if ($this->startDate <= $date) {
                return true;
            } else {
                return false;
            }
        } elseif ($this->startDate <= $date && $this->endDate >= $date) {
            return true;
        }

        return false;
    }

    /**
     * @param DateRange $dateRange
     * @return bool
     */
    public function containsDateRange(DateRange $dateRange)
    {
        $latestStartDate = $this->getLatestDate($this->startDate, $dateRange->startDate);
        $earliestEndDate = $this->getEarliestDate($this->endDate, $dateRange->endDate);

        if ($dateRange->startDate->getTimestamp() === $latestStartDate->getTimestamp()
            && $dateRange->endDate->getTimestamp() === $earliestEndDate->getTimestamp()
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param DateRange $dateRange
     * @return bool
     */
    public function overlapsDateRange(DateRange $dateRange)
    {
        return !is_null($this->intersect($dateRange));
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isBefore(DateTime $date)
    {
        if (is_null($this->endDate)) {
            return false;
        } elseif ($this->endDate <= $date) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isNow()
    {
        return $this->contains(new DateTime());
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isAfter(DateTime $date)
    {
        if (is_null($this->startDate)) {
            return false;
        } elseif ($this->startDate >= $date) {
            return true;
        }

        return false;
    }

    /**
     * @param DateRange $dateRange
     * @return bool
     */
    public function isEqualTo(DateRange $dateRange)
    {
        return $this->startDate == $dateRange->getStartDate() && $this->endDate == $dateRange->getEndDate();
    }

    /**
     * @param DateRange $dateRange
     * @return DateRange|null
     */
    public function intersect(DateRange $dateRange)
    {
        $latestStartDate = $this->getLatestDate($this->startDate, $dateRange->startDate);
        $earliestEndDate = $this->getEarliestDate($this->endDate, $dateRange->endDate);
        if ($latestStartDate && $earliestEndDate && ($latestStartDate > $earliestEndDate)) {
            return null;
        } else {
            return new DateRange($latestStartDate, $earliestEndDate);
        }
    }

    /**
     * @param DateTime|null $date1
     * @param DateTime|null $date2
     * @return DateTime
     */
    protected function getLatestDate(DateTime $date1 = null, DateTime $date2 = null)
    {
        if (is_null($date1)) {
            return $date2;
        } elseif (is_null($date2)) {
            return $date1;
        } elseif ($date1 > $date2) {
            return $date1;
        } else {
            return $date2;
        }
    }

    /**
     * @param DateTime|null $date1
     * @param DateTime|null $date2
     * @return DateTime
     */
    protected function getEarliestDate(DateTime $date1 = null, DateTime $date2 = null)
    {
        if (is_null($date1)) {
            return $date2;
        } elseif (is_null($date2)) {
            return $date1;
        } elseif ($date1 < $date2) {
            return $date1;
        } else {
            return $date2;
        }
    }

    /**
     * @param string $nullPlaceholder
     * @return string
     */
    public function getShort($nullPlaceholder = 'Ongoing')
    {
        if (is_null($this->startDate) && is_null($this->endDate)) {
            return $nullPlaceholder;
        } else {
            $string = (is_null($this->startDate) ? $nullPlaceholder : $this->startDate->getShort());
            if (!($this->startDate && $this->endDate && $this->startDate->getShort() == $this->endDate->getShort())) {
                $string .= ' - ' . (is_null($this->endDate) ? $nullPlaceholder : $this->endDate->getShort());
            }

            return $string;
        }
    }

    /**
     * @param string $nullPlaceholder
     * @return string
     */
    public function getLong($nullPlaceholder = 'Ongoing')
    {
        if ($this->startDate && $this->endDate) {
            if ($this->startDate->getLong() == $this->endDate->getLong()) {
                return $this->startDate->getLong();
            } else if ($this->startDate->getShort() == $this->endDate->getShort()) {
                if ($this->startDate->getDateType() === DateTime::TYPE_DATETIME && $this->endDate->getDateType() === DateTime::TYPE_DATETIME) {
                    return $this->startDate->getLong() . ' - ' . $this->endDate->format(DateTime::FORMAT_TIME);
                } else {
                    return $this->startDate->getLong();
                }
            } else {
                return $this->startDate->getLong() . ' - ' . $this->endDate->getLong();
            }
        }

        if ($this->startDate && is_null($this->endDate)) {
            return $this->startDate->getLong() . ' - ' . $nullPlaceholder;
        }

        if (is_null($this->startDate) && $this->endDate) {
            return $nullPlaceholder . ' - ' . $this->endDate->getLong();
        }

        return $nullPlaceholder;
    }

    /**
     * @return DateTime[]
     */
    public function getAllDates()
    {
        $startDate = $this->startDate;
        $endDate = $this->endDate;
        $referenceDate = clone $startDate;

        $dates = [];
        while ($referenceDate < $endDate) {
            $dates[] = clone $referenceDate;
            $referenceDate->add(new \DateInterval('P1D'));
        }

        return $dates;
    }

    /**
     * @return DateTime[]
     */
    public function getAllEndingDates()
    {
        $startDate = $this->startDate;
        $endDate = $this->endDate;
        $referenceDate = clone $startDate;

        $dates = [];
        while ($referenceDate <= $endDate) {
            $dateTime = clone $referenceDate;
            $dates[] = $dateTime;
            $referenceDate->add(new \DateInterval('P1D'));
        }

        return $dates;
    }

    /**
     * @param DateRange $dateRange
     */
    public function extend(DateRange $dateRange)
    {
        if ($this->overlapsDateRange($dateRange)) {
            $this->startDate = $this->getEarliestDate($this->startDate, $dateRange->startDate);
            $this->endDate = $this->getLatestDate($this->endDate, $dateRange->endDate);
        }
    }

    /**
     * @return Time|null
     */
    public function getDuration()
    {
        if ($this->startDate && $this->endDate) {
            return new Time($this->endDate->getTimestamp() - $this->startDate->getTimestamp());
        }

        return null;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getLong();
    }

    /**
     *
     */
    public function __clone()
    {
        $this->startDate = ($this->startDate ? clone $this->startDate : null);
        $this->endDate = ($this->endDate ? clone $this->endDate : null);
    }

    /**
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if (is_callable(array($this, $name))) {
            return $this->$name;
        }

        if (method_exists($this, $name)) {
            return call_user_func(array($this, $name));
        }

        $getName = 'get' . ucfirst($name);
        if (method_exists($this, $getName)) {
            return call_user_func(array($this, $getName));
        }

        throw new \DomainException('Calling undefined property or method ' . __CLASS__ . '::$' . $name);
    }

}
