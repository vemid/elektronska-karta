<?php

namespace Vemid\Date;

/**
 * Class DateTime
 *
 * @package Vemid\Date
 * @author Vemid
 */
class DateTime extends \DateTime
{

    const ISO_8601_NO_TIMEZONE = 'Y-m-d H:i:s';
    const ISO_8601_DATE_ONLY = 'Y-m-d';

    const FORMAT_LONG = 'd F Y';
    const FORMAT_LONG_DAY_OF_WEEK = 'l, d F Y';
    const FORMAT_SHORT = 'd M Y';
    const FORMAT_SERBIAN_SHORT = 'd.m.Y';
    const FORMAT_SHORT_DAY_OF_WEEK = 'D, d M Y';
    const FORMAT_TIME = 'H:i';

    const TYPE_DATETIME = 'datetime';
    const TYPE_DATE = 'date';

    protected $_dateType;

    /**
     * @param string $time
     * @param \DateTimeZone $timezone
     * @param string $dataType
     */
    public function __construct($time = 'now', \DateTimeZone $timezone = null, $dataType = self::TYPE_DATETIME)
    {
        parent::__construct($time, $timezone);

        $this->setDateType($dataType);
        
        if ($dataType === self::TYPE_DATE) {
            $this->setTime(0, 0);
        }
    }

    /**
     * @return string
     */
    public function getDateType()
    {
        return $this->_dateType;
    }

    /**
     * @param $dateType
     */
    public function setDateType($dateType)
    {
        $this->_dateType = $dateType;
    }

    /**
     * @return string
     */
    public function getShort()
    {
        return $this->format(self::FORMAT_SHORT);
    }

    public function getTime()
    {
        return $this->format(self::FORMAT_TIME);
    }

    /**
     * @return string
     */
    public function getShortSerbianFormat()
    {
        return $this->format(self::FORMAT_SERBIAN_SHORT);
    }

    /**
     * @return string
     */
    public function getLong()
    {
        $format = self::FORMAT_SHORT;
        if ($this->_dateType === self::TYPE_DATETIME) {
            $format .= ', ' . self::FORMAT_TIME;
        }

        return $this->format($format);
    }

    /**
     * @return string
     */
    public function getUnixFormat()
    {
        if ($this->_dateType === self::TYPE_DATE) {
            return $this->format(self::ISO_8601_DATE_ONLY);
        } else {
            return $this->format(self::ISO_8601_NO_TIMEZONE);
        }
    }

    /**
     * @return string
     */
    public function getTimestampFormat()
    {
        if ($this->_dateType === self::TYPE_DATE) {
            return $this->format(self::ISO_8601_DATE_ONLY);
        } else {
            return $this->format(self::ATOM);
        }
    }

    /**
     * @return DateTime
     */
    public function getGmtDateTime()
    {
        $gmtDate = clone $this;
        $gmtDate->setTimezone(new \DateTimeZone('Europe/Belgrade'));

        return $gmtDate;
    }

    /**
     * @param DateTime $dateTime
     * @return bool
     */
    public function isEqualTo(DateTime $dateTime)
    {
        return $this->getTimestamp() === $dateTime->getTimestamp();
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isBefore(DateTime $date)
    {
        return $this->getTimestamp() < $date->getTimestamp();
    }

    /**
     * @return bool
     */
    public function isToday()
    {
        return $this->getShort() === (new DateTime())->getShort();
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isAfter(DateTime $date)
    {
        return $this->getTimestamp() > $date->getTimestamp();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getUnixFormat();
    }

    /**
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if (is_callable(array($this, $name))) {
            return $this->$name;
        }

        if (method_exists($this, $name)) {
            return call_user_func(array($this, $name));
        }

        $getName = 'get' . ucfirst($name);
        if (method_exists($this, $getName)) {
            return call_user_func(array($this, $getName));
        }

        throw new \Exception('Calling undefined property or method ' . __CLASS__ . '::$' . $name);
    }

}
