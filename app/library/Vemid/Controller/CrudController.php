<?php

namespace Vemid\Controller;

use Vemid\Form\Renderer\Json as Renderer;
use Vemid\Messenger\Message as MessengerMessage;
use Vemid\Entity\EntityInterface;

/**
 * Class CrudController
 *
 * @package Vemid\Controller
 * @author Vemid
 */
abstract class CrudController extends BaseController implements CrudInterface
{

    /**
     * {@inheritdoc}
     */
    public function createAction()
    {
        if ($this->request->isPost()) {
            $form = $this->getForm();
            if ($form->isValid($this->request->getPost())) {
                $form->synchronizeEntityData();
                $entity = $form->getEntity();
                $this->saveEntity($entity);
                $this->getForm()->setEntity($entity);
            } else {
                $this->addFlashMessagesFromForm($form);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function overviewAction($id)
    {
        $this->returnNotFound();
    }

    /**
     * {@inheritdoc}
     */
    public function updateAction($id)
    {
        if ($this->request->isPost()) {
            if (!$entity = $this->getEntity($id)) {
                return $this->returnNotFound();
            }

            $form = $this->getForm($entity);
            if ($form->isValid($this->request->getPost())) {
                $form->synchronizeEntityData();
                $this->saveEntity($entity);
            } else {
                $this->addFlashMessagesFromForm($form);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function deleteAction($id)
    {
        if (!$entity = $this->getEntity($id)) {
            return $this->returnNotFound();
        }

        if (!$entity->isDeletable()) {
            $this->addFlashMessage(
                $this->_translate('It is not allowed to delete this record.'),
                null,
                MessengerMessage::WARNING
            );
        } else {
            $this->deleteEntity($entity);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCreateFormAction()
    {
        $renderer = new Renderer();

        return $renderer->render($this->getForm());
    }


    /**
     * {@inheritdoc}
     */
    public function getUpdateFormAction($id)
    {
        if (!$entity = $this->getEntity($id)) {
            return $this->returnNotFound();
        }

        $renderer = new Renderer();
        $form = $this->getForm($entity);

        return $renderer->render($form);
    }

    /**
     * {@inheritdoc}
     */
    public function getForm(EntityInterface $entity = null, array $exclude = [])
    {
        if ($entity === null) {
            $entityName = '\\' . ltrim($this->getEntityName(), '\\');
            /** @var EntityInterface $entity */
            $entity = new $entityName;
        }

        return $entity->getForm($exclude);
    }

    /**
     * @param int|string|array $params
     * @return EntityInterface|null
     */
    protected function getEntity($params)
    {
        return $this->entityManager->findOne($this->getEntityName(), $params);
    }

}
