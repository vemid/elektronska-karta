<?php

namespace Vemid\Controller;

use Vemid\Application\Di;
use Vemid\Entity\EntityInterface;
use Vemid\Entity\Repository\UserNotificationMessageRepository;
use Vemid\Form\Form;
use Vemid\Messenger\Message as MessengerMessage;
use Phalcon\Mvc\Controller;
use Phalcon\Tag;
use Vemid\Services\Printer\DocumentPrinter;

/**
 * Class BaseController
 *
 * @method Di getDI()
 *
 * @property \Phalcon\Translate\AdapterInterface $translator
 * @property \Vemid\Messenger\Manager $messengerManager
 * @property \Vemid\Entity\Manager\EntityManagerInterface $entityManager
 * @property DocumentPrinter $documentPrinter
 * @property \User $currentUser
 *
 * @package Controller
 * @author Vemid
 */
abstract class BaseController extends Controller
{

    public function initialize()
    {
        $codeTypes = $this->entityManager->find(\CodeType::class, [
            \CodeType::PROPERTY_IS_MAIN . ' = :isMain:',
            'bind' => [
                'isMain' => true
            ]
        ]);

        if ($this->currentUser) {
            /** @var UserNotificationMessageRepository $userNotificationMessageRepository */
            $userNotificationMessageRepository = $this->entityManager->getRepository(\UserNotificationMessage::class);
            $notifications = $userNotificationMessageRepository->getActiveNotificationByUser($this->currentUser);
            $topTenNotifications = $userNotificationMessageRepository->getActiveNotificationByUser($this->currentUser, 5);

            $this->view->setVar('activeNotifications', $topTenNotifications);
            $this->view->setVar('totalActiveNotifications', $notifications->count());
        }

        $orders = $this->entityManager->find(\Order::class, [
            'active = :active:',
            'bind' => [
                'active' => true
            ],
            'order' => 'startDate DESC'
        ]);

        $order = false;
        if ((int)$orders->count() === 1) {
            $order = $orders->getLast();
        }


        $id = null;
        if (isset($this->router->getParams()[0])) {
            $id = $this->router->getParams()[0];
        }

        $this->view->setVar('codeTypes', $codeTypes);
        $this->view->setVar('activeOrder', $order);
        $this->view->setVar('channelSuppliesMenu', $this->entityManager->find(\ChannelSupply::class));
        $this->view->setVar('paramId', $id);
    }
    /**
     * @return bool
     */
    protected function returnNotFound()
    {
        if ($this->request->isAjax()) {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('The content you are looking for could not be found.'),
                null,
                MessengerMessage::DANGER
            );

            return;
        }


        $this->response->setStatusCode(404, 'Not found');
        $this->dispatcher->forward(array(
            'controller' => 'errors',
            'action' => 'show404',
        ));

        return false;
    }

    /**
     * @param string $string
     * @param null|string|array $args
     * @return string
     */
    protected function _translate($string, $args = null)
    {
        return $this->getDI()->getTranslator()->t($string, $args);
    }

    /**
     * @param string $title
     */
    protected function _setTitle($title)
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        Tag::setTitle($title . ' | ' . $config->application->siteName);
    }

    /**
     * @return null|string
     */
    protected function _getRedirectUrl()
    {
        return $this->dispatcher->getParam('url', 'string', null);
    }

    /**
     * @param string $url
     */
    protected function _setRedirectUrl($url)
    {
        $this->dispatcher->setParam('url', ltrim($url, '/'));
    }

    /**
     * @param string $message
     * @param string $field
     * @param string $type
     */
    protected function addFlashMessage($message, $field, $type)
    {
        $this->getDI()->getNotificationManager()->appendMessage(
            new MessengerMessage($message, $field, $type)
        );
    }

    /**
     * TODO Move in a proper class
     * @return int
     */
    protected function isMobile()
    {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

    /**
     * @param EntityInterface $entity
     * @return bool
     */
    protected function saveEntity(EntityInterface $entity)
    {
        if ($this->entityManager->save($entity)) {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('Uspesno snimnjeni podaci.'),
                null,
                MessengerMessage::SUCCESS
            );

            return true;
        }

        $this->addFlashMessagesFromEntity($entity);

        return false;
    }

    /**
     * @param EntityInterface $entity
     * @return bool
     */
    protected function deleteEntity(EntityInterface $entity)
    {
        if ($this->entityManager->delete($entity)) {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('Data was deleted successfully.'),
                null,
                MessengerMessage::SUCCESS
            );

            return true;
        }

        $this->addFlashMessagesFromEntity($entity);

        return false;
    }

    /**
     * @param EntityInterface $entity
     */
    protected function addFlashMessagesFromEntity(EntityInterface $entity)
    {
        foreach ($entity->getMessages() as $message) {
            if (empty($message->getMessage())) {
                continue;
            }

            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t($message->getMessage()),
                is_array($message->getField()) ? implode('__', $message->getField()) : $message->getField(),
                MessengerMessage::WARNING
            );
        }
    }

    /**
     * @param Form $form
     */
    protected function addFlashMessagesFromForm(Form $form)
    {
        foreach ($form->getMessages() as $message) {
            $element = $form->get($message->getField());
            $this->addFlashMessage(
                str_replace($message->getField(), $element->getLabel(), $message->getMessage()),
                $message->getField(),
                MessengerMessage::DANGER
            );
        }
    }
}
