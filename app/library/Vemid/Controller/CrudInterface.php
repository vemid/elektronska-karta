<?php

namespace Vemid\Controller;

use Vemid\Form\Form;
use Vemid\Entity\EntityInterface;

/**
 * Interface CrudInterface
 *
 * @package Vemid\Controller
 * @author Vemid
 */
interface CrudInterface
{

    /**
     * @return bool|void
     */
    public function createAction();

    /**
     * @param int $id
     * @return void
     */
    public function overviewAction($id);

    /**
     * @param int $id
     * @return bool|void
     */
    public function updateAction($id);

    /**
     * @param int $id
     * @return bool|void
     */
    public function deleteAction($id);

    /**
     * @return string|bool
     */
    public function getCreateFormAction();

    /**
     * @param int $id
     * @return string|bool
     */
    public function getUpdateFormAction($id);

    /**
     * @param EntityInterface|null $entity
     * @param array $exclude
     * @return Form
     */
    public function getForm(EntityInterface $entity = null, array $exclude = []);

    /**
     * @return string
     */
    public function getEntityName();

}
