<?php

namespace Vemid\Service\Workers;


use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Phalcon\Config;
use Vemid\Date\DateTime;

/**
 * Class Manager
 * @package Vemid\Service\Workers
 */
class Manager
{
    /** @var \ProductionWorker */
    private $productionWorker;

    /** @var string */
    private $hash;

    /** @var ClientInterface */
    private $client;

    /**
     * Manager constructor.
     * @param \ProductionWorker $productionWorker
     * @param Config $config
     * @param ClientInterface|null $client
     */
    public function __construct(\ProductionWorker $productionWorker, Config $config, ClientInterface $client = null)
    {
        $this->productionWorker = $productionWorker;

        if (!$client) {
            $client = new Client(['base_uri' => $config->workerRegistration->url]);
        }

        $this->client = $client;
        $this->hash = md5('bkids_prijava_' . (new DateTime())->format('Y-m-d'));
    }

    /**
     * @return array
     */
    public function put()
    {
        $data = [
            'hash' => $this->hash,
            'name' => $this->productionWorker->getDisplayName(),
            'code' => $this->productionWorker->getCheckinCode(),
            'location' => $this->productionWorker->getLocationCode()->getName()
        ];

        $response = $this->client->post('put.php', ['json' => $data]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param string $oldCode
     * @return array
     */
    public function update(string $oldCode)
    {
        $data = [
            'hash' => $this->hash,
            'name' => $this->productionWorker->getDisplayName(),
            'code' => $this->productionWorker->getCheckinCode(),
            'oldCode' => $oldCode,
            'location' => $this->productionWorker->getLocationCode()->getName()
        ];

        $response = $this->client->post('update.php', ['json' => $data]);

        return json_decode($response->getBody()->getContents(), true);
    }


    public function checkForExisting()
    {

    }

    /**
     * @return array
     */
    public function delete()
    {
        $data = [
            'hash' => $this->hash,
            'code' => $this->productionWorker->getCheckinCode(),
            'location' => $this->productionWorker->getLocationCode()->getName()
        ];

        $response = $this->client->post('delete.php', ['json' => $data]);

        return json_decode($response->getBody()->getContents(), true);
    }
}
