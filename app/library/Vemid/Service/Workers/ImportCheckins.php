<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 9/15/18
 * Time: 23:17
 */

namespace Vemid\Service\Workers;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Vemid\Date\DateRange;
use Vemid\Date\DateTime;
use Phalcon\Config;
use Vemid\Entity\Manager\EntityManagerInterface;
use \Vemid\Task\Checkins\ImportCheckinsTask;

/**
 * Class ImportCheckins
 * @package
 */

class ImportCheckins
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var DateTime */
    private $date;

    /** @var string */
    private $hash;

    /** @var ClientInterface */
    private $client;

    /**
     * ImportWarrants constructor.
     * @param EntityManagerInterface $entityManager
     * @param DateTime|null $date
     * @param Config $config
     * @param ClientInterface|null $client
     */
    public function __construct(EntityManagerInterface $entityManager,Config $config, DateTime $date = null,ClientInterface $client = null)
    {
        $this->entityManager = $entityManager;
        $this->date = $date ?: (new DateTime())->format('Y-m-d');
        $this->hash = md5('bkids_prijava_'.$this->date);

        if (!$client) {
            $client = new Client(['base_uri' => $config->workerRegistration->url]);
        }
        $this->client = $client;


    }

    /**
     * @throws \DomainException|\RuntimeException
     */
    public function import()
    {
        /** @var \CodeType $codeType */
        $codeType = $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CHECKIN_LOCATION
            ]
        ]);

        if (!$codeType) {
            throw new \DomainException('Code type do not exist!');
        }

        /** @var \Code[] $locations */
        $locations = $this->entityManager->find(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: ',
            'bind' => [
                'codeTypeId' => $codeType->getId(),
            ]
        ]);

        $dateRane = new DateRange((new DateTime())->modify('-10 days'),(new DateTime())->modify('+1 day'));

        foreach ($dateRane->getAllDates() as $dateRange) {


            foreach ($locations as $location) {
                $data = [
                    'location' => $location->getName(),
                    'hash' => $this->hash,
                    'date' => $dateRange->format('Y-m-d'),
                    'format' => 'json'
                ];
                $response = $this->client->post('get.php', ['json' => $data]);
                $datas = $response->getBody()->getContents();

                $checkRespsonse = (json_decode($datas, true))['datas'];

                if($checkRespsonse) {

                    foreach ((json_decode($datas, true))['datas'] as $responses) {
                        if (!empty($responses)) {
                            $importTask = new ImportCheckinsTask();
                            $importTask->dataCheckin = $responses['data'];

                            $importTask->run();
                        }
                    }
                }


            }
        }
    }

}