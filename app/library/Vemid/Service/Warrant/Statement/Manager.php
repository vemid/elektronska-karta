<?php

namespace Vemid\Service\Warrant\Statement;

use Vemid\Entity\Manager\EntityManagerInterface;

/**
 * Class Manager
 * @package Vemid\Service\Warrant\Statement
 */
class Manager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var string */
    private $filePath;

    /**
     * Manager constructor.
     * @param EntityManagerInterface $entityManager
     * @param string $filePath
     */
    public function __construct(EntityManagerInterface $entityManager, $filePath)
    {
        $this->entityManager = $entityManager;
        $this->filePath = $filePath;
    }

    /**
     * @throws \RuntimeException|\DomainException
     */
    public function import()
    {
        if (!file_exists($this->filePath)) {
            throw new \RuntimeException('File do not exist');
        }

        $fileContent = file_get_contents($this->filePath);
        $xml = new \SimpleXMLElement($fileContent);

        if (!$warrantStatement = $this->findWarrantStatement(md5($fileContent))) {
            $warrantStatement = $this->addStatement($xml);
        }

        $this->addStatementItems($warrantStatement, $xml);
    }

    /**
     * @param \SimpleXMLElement $xml
     * @return \WarrantStatement
     * @throws \DomainException
     */
    private function addStatement(\SimpleXMLElement $xml): \WarrantStatement
    {
        $date = explode('T', $xml->stmtrs->availbal->dtasof[0])[0];

        $warrantStatement = new \WarrantStatement();
        $warrantStatement->setStatementPath(basename($this->filePath));
        $warrantStatement->setAccount($xml->stmtrs->acctid[0]);
        $warrantStatement->setStatementNumber((int)$xml->stmtrs->stmtnumber[0]);
        $warrantStatement->setFileHash(md5(file_get_contents($this->filePath)));
        $warrantStatement->setDate($date);

        if (!$this->entityManager->save($warrantStatement)) {
            throw new \DomainException('Warrant Statement failed to save!');
        }

        return $warrantStatement;
    }

    /**
     * @param \WarrantStatement $warrantStatement
     * @param \SimpleXMLElement $xml
     * @throws \DomainException
     */
    private function addStatementItems(\WarrantStatement $warrantStatement, \SimpleXMLElement $xml)
    {
        foreach ($xml->stmtrs->trnlist->stmttrn as $item) {
            $warrantNumber = (string)$item->payeerefnumber[0];

            if (!$warrantNumber || $this->checkAlreadyStored($warrantStatement, $warrantNumber)) {
                continue;
            }

            $warrantStatementItem = new \WarrantStatementItem();
            $warrantStatementItem->setWarrantStatement($warrantStatement);
            $warrantStatementItem->setAccount($item->payeeaccountinfo->acctid[0]);
            $warrantStatementItem->setWarrantNumber($warrantNumber);
            $warrantStatementItem->setAmount((float)$item->trnamt[0]);
            $warrantStatementItem->setCompanyName('a');
            $warrantStatementItem->setPurpose($item->purpose[0]);

            if (!$this->entityManager->save($warrantStatementItem)) {
                throw new \DomainException('Warrant Statement Item failed to save!');
            }

            $this->credit($warrantStatementItem);
        }
    }

    /**
     * @param \WarrantStatementItem $warrantStatementItem
     * @throws \DomainException
     */
    private function credit(\WarrantStatementItem $warrantStatementItem)
    {
        /** @var \DealershipWarrant $warrantShop */
        $warrantShop = $this->entityManager->findOne(\DealershipWarrant::class, [
            \DealershipWarrant::PROPERTY_WARRANT_NUMBER . ' = :warrantNumber:',
            'bind' => [
                'warrantNumber' => $warrantStatementItem->getWarrantNumber()
            ]
        ]);

        if ($warrantShop && $warrantShop->getAmount() === $warrantStatementItem->getAmount()) {
            $warrantShop->setWarrantStatementItem($warrantStatementItem);
            $warrantShop->setStatus(true);

            if (!$this->entityManager->save($warrantShop)) {
                throw new \DomainException('Warrant Shop failed to save!');
            }
        }
    }

    /**
     * @param $hash
     * @return null|\Vemid\Entity\EntityInterface|\WarrantStatement
     */
    private function findWarrantStatement($hash)
    {
        return $this->entityManager->findOne(\WarrantStatement::class, [
            \WarrantStatement::PROPERTY_FILE_HASH . ' = :hash:',
            'bind' => [
                'hash' => $hash,
            ]
        ]);
    }

    /**
     * @param \WarrantStatement $warrantStatement
     * @param $warrantNumber
     * @return null|\Vemid\Entity\EntityInterface|\WarrantStatementItem
     */
    private function checkAlreadyStored(\WarrantStatement $warrantStatement, $warrantNumber)
    {
        return $this->entityManager->findOne(\WarrantStatementItem::class, [
            \WarrantStatementItem::PROPERTY_WARRANT_STATEMENT_ID . '=  :warrantStatementId: AND ' .
            \WarrantStatementItem::PROPERTY_WARRANT_NUMBER . ' = :warrantNumber:',
            'bind' => [
                'warrantStatementId' => $warrantStatement->getId(),
                'warrantNumber' => $warrantNumber
            ]
        ]);
    }
}