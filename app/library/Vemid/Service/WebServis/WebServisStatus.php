<?php

namespace Vemid\Service\WebServis;

use Vemid\Application\Di;

/**
 * Class WebServisStatus
 * @package Vemid\Service\WebServis
 */
class WebServisStatus
{
    public function __construct()
    {

    }

    public function getStatus()
    {
        /** @var Di $di */
        $di = Di::getDefault();
        $config = $di->getConfig();
        $app = $config->wsdl->appUrl;
        $what = $app.'/manager/text/list';

        $headers = array(
            'Authorization: Basic '. base64_encode('test:test'),
        );


        $ch = curl_init($what);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


        return $output;
    }

    public function startWebServis()
    {
        /** @var Di $di */
        $di = Di::getDefault();
        $config = $di->getConfig();
        $app = $config->wsdl->appUrl;
        $what = $app.'/manager/text/start?path=/ServisMisWeb';

        $headers = array(
            'Authorization: Basic '. base64_encode('test:test'),
        );


        $ch = curl_init($what);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


        return $output;
    }

    public function check(){
        if(!$this->getStatus()) {
//            $connection = ssh2_connect('192.168.100.210', 22);
//            ssh2_auth_password($connection, 'root', 'admin710412');
//
//            $stream = ssh2_exec($connection, 'systemctl start tomcat.service');

//            /** @var Di $di */
//            $di = Di::getDefault();
//            $mailManager = $di->getMailManager();
//            $message = $mailManager->createMessage();
//
//
//            $message->setSubject('Restart servisa posle provere da je ofline');
//            $message->setBody("ovo je testiranje");
//            $message->setTo('admin@bebakids.com');
//            $message->send();

            sleep(15);


            if(strpos($this->getStatus(),"/ServisMisWeb:running:0:ServisMisWeb")) {
                return true;
            }
            else {
                throw new \DomainException('Cant connect to webservis');
            }
        }
        elseif (strpos($this->getStatus(),"/ServisMisWeb:stopped:0:ServisMisWeb")) {
            $this->startWebServis();
            return true;
        }
        else {
            return true;
        }

    }

}