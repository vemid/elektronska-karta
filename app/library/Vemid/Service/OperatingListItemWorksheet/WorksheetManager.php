<?php

namespace Vemid\Service\OperatingListItemWorksheet;


use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Repository\OperatingListItemWorksheetRepository;

class WorksheetManager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * ItemCalculator constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $date
     * @param \ProductionWorker $productionWorker
     * @return string
     */
    public function getWorksheetStatus($date, \ProductionWorker $productionWorker)
    {
        /** @var OperatingListItemWorksheetRepository $operatingListItemWorksheetRepository */
        $operatingListItemWorksheetRepository  = $this->entityManager->getRepository(\OperatingListItemWorksheet::class);
        $approvedStatusByDate = $operatingListItemWorksheetRepository->getApprovedStatusByDate($date, $productionWorker);
        $overtimeStatusByDate = $operatingListItemWorksheetRepository->getOvertimeStatusByDate($date, $productionWorker);

        if ($overtimeStatusByDate && $approvedStatusByDate) {
            return 'Odob., Preko.';
        }

        if ($overtimeStatusByDate) {
            return 'Prekovremeno';
        }

        if ($approvedStatusByDate) {
            return 'Odobreno';
        }

        return '';
    }
}
