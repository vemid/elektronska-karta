<?php


namespace Vemid\Service\WarehouseOrders;

use Phalcon\Mvc\Model\ManagerInterface;


/**
 * Class WarehouseGroupProducts
 * @package Vemid\Service\WarehouseOrders
 */
class WarehouseGroupProducts
{

    /** @var ManagerInterface */
    private $modelsManager;

    /**
     * WarehouseGroupProducts constructor.
     * @param ManagerInterface $modelsManager
     */
    public function __construct(ManagerInterface $modelsManager)
    {
        $this->modelsManager = $modelsManager;
    }

    /**
     * @param \WarehouseOrderGroup $warehouseOrderGroup
     * @return array
     */
    public function getSumWarehouseOrders(\WarehouseOrderGroup $warehouseOrderGroup,\Pricelist $pricelist) {
        $query = $this->modelsManager->createBuilder()
            ->columns([
                'p.code sku',
                'c.code size',
                'sum(woi.quantity) qty',
                'round(pr.purchasePrice,2) purchasePrice',
                'round(pr.wholesalePrice,2) wholesalePrice',
                'round(pr.retailPrice,1) retailPrice',
            ])
            ->addFrom(\WarehouseOrderGroupItem::class, 'wogi')
            ->leftJoin(\WarehouseOrderGroup::class,'wog.id = wogi.warehouseOrderGroupId','wog')
            ->leftJoin(\WarehouseOrderItem::class,'woi.warehouseOrderId = wogi.warehouseOrderId','woi')
            ->leftJoin(\ProductSize::class,'ps.id = woi.productSizeId','ps')
            ->leftJoin(\Code::class,'c.id = ps.codeId','c')
            ->leftJoin(\Product::class,'p.id = ps.productId','p')
            ->leftJoin(\ProductPrice::class,'p.id = pr.productId','pr')
            ->leftJoin(\Pricelist::class, 'pr.pricelistId = pl.id', 'pl')
            ->andWhere('wog.id = :wogId:', [
                'wogId' => $warehouseOrderGroup->getId()
            ])
            ->andWhere('pr.status = :status:', [
                'status' => \ProductPrice::STATUS_FINAL
            ])
            ->andWhere('pl.code = :plCode:', [
                'plCode' => $pricelist->getCode()
            ])
            ->groupBy('p.code,c.code');

        return $query->getQuery()->execute();
    }

}