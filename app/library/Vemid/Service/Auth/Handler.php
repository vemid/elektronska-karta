<?php

namespace Vemid\Service\Auth;


use Phalcon\Mvc\Dispatcher;
use Vemid\Filter\Underscore;
use Vemid\Service\Acl\AclHelper;

class Handler
{
    /** @var AclHelper  */
    private $aclHelper;

    /** @var Dispatcher */
    private $dispatcher;

    /**
     * Handler constructor.
     * @param Dispatcher $dispatcher
     * @param AclHelper $aclHelper
     */
    public function __construct(Dispatcher $dispatcher, AclHelper $aclHelper)
    {
        $this->aclHelper = $aclHelper;
        $this->dispatcher = $dispatcher;
    }

    public function initialize(): bool
    {
        if (!$this->checkAccessBasedOnDispatcher()) {
            $this->dispatcher->forward(array(
                'controller' => 'errors',
                'action' => 'show401',
            ));

            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    protected function checkAccessBasedOnDispatcher(): bool
    {
        $underscore = new Underscore();

        $resource = $this->dispatcher->getModuleName() ? $underscore->filter($this->dispatcher->getModuleName()) . '/' : '';
        $resource .= $underscore->filter($this->dispatcher->getControllerName());
        $resource = str_replace('_', '-', $resource);
        $access = str_replace('_', '-', $underscore->filter($this->dispatcher->getActionName()));

        return $this->aclHelper->isAllowed($resource, $access);
    }
}