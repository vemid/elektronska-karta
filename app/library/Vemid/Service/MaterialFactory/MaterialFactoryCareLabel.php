<?php


namespace Vemid\Service\MaterialFactory;


use Vemid\Entity\Manager\EntityManagerInterface;

class MaterialFactoryCareLabel
{

    /** @var EntityManagerInterface  */
    private $entityManager;

    /**
     * MaterialFactoryCareLabel constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param \MaterialFactory $materialFactory
     * @return string
     */
    public function getCareLabelsImage(\MaterialFactory $materialFactory = null)
    {
        $html ="";

         if($materialFactory) {

             $washItems = ($this->entityManager)->find(\WashingCareLabel::class);
             $washData = str_split($materialFactory->getWashData());

             /** @var \WashingCareLabel $washItem */
             foreach ($washItems as $washItem) {
                 if (in_array($washItem->getPrintSign(), $washData)) {
                     $html .= "<img src='";
                     $html .= $washItem->getImagePath();
                     $html .="' alt='' width='35px'>";
                 }
             }

         }
        return $html;
    }

    /**
     * @param $careLabel
     * @return array
     */
    public function getCareLabelsImageFromString($careLabel)
    {
        $html ="";
        $data = [];


            $washItems = ($this->entityManager)->find(\WashingCareLabel::class);
            $washData = str_split($careLabel);

            /** @var \WashingCareLabel $washItem */
            foreach ($washItems as $washItem) {
                if (in_array($washItem->getPrintSign(), $washData)) {
//                    $html .= "<img src='";
//                    $html .= $washItem->getImagePath();
//                    $html .="' alt='' width='35px'>";
                    $data[] .= $washItem->getImagePath();
                }
            }

        return $data;
    }

}