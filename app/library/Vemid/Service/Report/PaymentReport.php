<?php

namespace Vemid\Service\Report;

use Vemid\Date\DateTime;
use Vemid\Date\Time;
use Vemid\Date\DateRange;
use Vemid\Entity\Manager\EntityManagerInterface;

/**
 * Class PaymentReport
 * @package Vemid\Service\Report
 */
class PaymentReport
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * ItemCalculator constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     * @param \Code $code
     * @return array
     */
    public function getPaymentReportBySectorData(DateTime $dateFrom, DateTime $dateTo, \Code $code)
    {
        $dateFrom->setTime(00, 00, 00);
        $dateTo->setTime(23, 59, 00);

        /** @var \Vemid\Entity\Repository\CuttingJobWorksheetRepository $cuttingJob */
        $cuttingJob = $this->entityManager->getRepository(\CuttingJobWorksheet::class);

        /** @var \Vemid\Entity\Repository\OperatingListItemWorksheetRepository $oliw */
        $oliw = $this->entityManager->getRepository(\OperatingListItemWorksheet::class);

        /** @var \Vemid\Entity\Repository\ProductionWorkerRepository $productionWorkerRep */
        $productionWorkerRep = $this->entityManager->getRepository(\ProductionWorker::class);

        /** @var \Vemid\Entity\Repository\AbsenceWorkRepository $absenceWork */
        $absenceWork = $this->entityManager->getRepository(\AbsenceWork::class);

        $productionWorkers = $this->entityManager->find(\ProductionWorker::class, [
            \ProductionWorker::PROPERTY_CODE_ID . ' = :codeId: AND '.
            'ifnull(breakUp,date("'.$dateFrom.'")) >= :breakUp:',
            'bind' => [
                'codeId' => $code->getId(),
                'breakUp' => $dateFrom
            ]
        ]);

        $data = [];
        foreach ($productionWorkers as $productionWorker) {
            $cuttinngJobaData = $this->getTotalPaymentPerWorkerAndPeriod($dateFrom, $dateTo, $productionWorker);
            $oliwData = $oliw->getTotalPaymentPerWorkerAndPeriod($dateFrom, $dateTo, $productionWorker);
            $workDaysData = $productionWorkerRep->getWorkersWorkDays($productionWorker->getEntityId(), $dateFrom, $dateTo);
            $sickDays = $absenceWork->getTotalSick($productionWorker, $dateFrom, $dateTo);
            $vacationDays = $absenceWork->getTotalVacation($productionWorker, $dateFrom, $dateTo);
            $vacationPaidDays = $absenceWork->getTotalPayedVacation($productionWorker, $dateFrom, $dateTo);
            $overtimeTime = $oliw->getOvertimeByPeriod($dateFrom, $dateTo, $productionWorker);
            $overtimeData = $oliw->getOvertimePaymentPerWorkerAndPeriod($dateFrom, $dateTo, $productionWorker);

            $pauseSeconds = $workDaysData[0]['numberOfDays'] * 1800;
            $cuttinngJobsSeconds = $cuttinngJobaData ? $cuttinngJobaData[0]['totalTime'] * 60 : 0;
            $cuttinngJobsTotalSeconds = $cuttinngJobaData ? $cuttinngJobaData[0]['totalCuttingTime'] * 60 : 0;
            $oliwRealizedSeconds = $oliwData ? $oliwData[0]['totalTime'] * 60 : 0;
            $oliwWorktimeSeconds = $oliwData ? $oliwData[0]['workTime'] : 0;
            $vacationMoney = ($vacationDays+$vacationPaidDays) * (8 * $productionWorker->getPriceTime());

            $cuttinngJobsHours = new Time($cuttinngJobsSeconds);
            $oliwRealizedHours = new Time($oliwRealizedSeconds); /* Ostvareno vreme */
            $totalRealizedTime = $cuttinngJobsSeconds + $oliwRealizedSeconds; /* Ukupno ostvareno vreme*/
            $totalPlanedTime = $cuttinngJobsTotalSeconds + $oliwWorktimeSeconds - $pauseSeconds; /* Ukupno planirano vreme*/
            $overTimeTotal = new Time($overtimeTime);
            $overtimeMoney = $overtimeData[0]['overtimeMoney'] * 1.1;

            $outerm = 0;
            if ($workDaysData[0]['numberOfDays'] > 0) {
                $outerm = number_format(!$totalPlanedTime ?: ($totalRealizedTime / $totalPlanedTime) * 100, 2);
            }

            $totalMoney = $cuttinngJobaData[0]['money'] + $oliwData[0]['money'] + $vacationMoney + $overtimeMoney;
            $oliwPlanedTimeSeonds = ($workDaysData[0]['numberOfDays']*27000)-$cuttinngJobsSeconds;

            $data[] = [
                'displayName' => $productionWorker->getDisplayName(),
                'numberOfDays' => $workDaysData[0]['numberOfDays'],
                'sickDays' => $sickDays,
                'vacationDays' => $vacationDays,
                'vacationPaidDays' => $vacationPaidDays,
                'vacationMoney' => $vacationMoney,
                'cuttingJobTime' => $cuttinngJobsHours->getUnixFormat(),
                'cuttingJobSeconds' => $cuttinngJobsSeconds,
                'cuttingPercentage' => $cuttinngJobsSeconds>0 ? ($cuttinngJobsSeconds/($workDaysData[0]['numberOfDays']*27000)*100) : 0,
                'cuttingJobMoney' => $cuttinngJobaData[0]['money'],
                'oliwRealizedTime' => $oliwRealizedHours->getUnixFormat(),
                'oliwRealizesSeconds' => $oliwRealizedSeconds,
                'oliwPlanedTime' => (new Time($oliwPlanedTimeSeonds))->getUnixFormat(),
                'oliwPlanedTimeSeconds' => $oliwPlanedTimeSeonds,
                'outTermOperation' => $oliwRealizedSeconds>0 ? ($oliwRealizedSeconds/$oliwPlanedTimeSeonds*100) : 0,
                'oliwMoney' => $oliwData[0]['money'],
                'outerm' => $outerm,
                'totalRealizedTime' => (new Time($totalRealizedTime))->getUnixFormat(),
                'totalRealizedSeconds' => $totalRealizedTime,
                'totalPlanedTime' => (new Time($totalPlanedTime))->getUnixFormat(),
                'totalPlanedSeconds' => $totalPlanedTime,
                'workTime' => (new Time($oliwWorktimeSeconds))->getUnixFormat(),
                'totalDataMoney' => $totalMoney,
                'overtime' => $overTimeTotal->getUnixFormat(),
                'overtimeSeconds' => $overtimeTime,
                'overtimeMoney' => $overtimeMoney,
                'workerId' => $productionWorker->getEntityId(),
            ];
        }

        return $data;
    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     * @param \Code $code
     * @return array
     */
    public function getPaymentReportBySectorDataTotal(DateTime $dateFrom, DateTime $dateTo, \Code $code)
    {
        $paymentDatas = $this->getPaymentReportBySectorData($dateFrom, $dateTo, $code);

        $totalData = [];
        $totalsDays = 0;
        $totalsSikcDays = 0;
        $totalsVacationDays = 0;
        $totalsVacationPaidDays = 0;
        $totalsCuttingTime = 0;
        $totalsCuttingMoney = 0;
        $totalsOliwRealizedTime = 0;
        $totalsOliwPlanedTime = 0;
        $totalsAllRealizedTime = 0;
        $totalsMoney = 0;
        $totalsOliwMoney = 0;
        $totalsOvertime = 0;
        $totalsOvertimeMoney = 0;
        $totalsVacationMoney = 0;

        foreach ($paymentDatas as $paymentData) {

            $totalsDays += $paymentData['numberOfDays'];
            $totalsSikcDays += $paymentData['sickDays'];
            $totalsVacationDays += $paymentData['vacationDays'];
            $totalsVacationPaidDays += $paymentData['vacationPaidDays'];
            $totalsCuttingTime += $paymentData['cuttingJobSeconds'];
            $totalsCuttingMoney += $paymentData['cuttingJobMoney'];
            $totalsOliwMoney += $paymentData['oliwMoney'];
            $totalsOliwRealizedTime += $paymentData['oliwRealizesSeconds'];
            $totalsOliwPlanedTime += $paymentData['oliwPlanedTimeSeconds'];
            $totalsAllRealizedTime += $paymentData['totalRealizedSeconds'];
            $totalsOvertime += $paymentData['overtimeSeconds'];
            $totalsOvertimeMoney += $paymentData['overtimeMoney'];
            $totalsVacationMoney += $paymentData['vacationMoney'];
            $totalsMoney += $paymentData['totalDataMoney'];

        }

        $totalsPlanedTime = $totalsDays*450*60;
        $oliwPlanedTimeSeonds = ($totalsDays*27000)-$totalsCuttingTime;

        $totalData[] = [
            'totalsDays' => $totalsDays,
            'totalsSikcDays' => $totalsSikcDays,
            'totalsVacationDays' => $totalsVacationDays,
            'totalsVacationPaidDays' => $totalsVacationPaidDays,
            'totalsVacationMoney' => $totalsVacationMoney,
            'totalsCuttingTime' => (new Time($totalsCuttingTime))->getUnixFormat(),
            'totalsCuttingMoney' => $totalsCuttingMoney,
            'totalsCuttingPercentage' => number_format(($totalsCuttingTime/($totalsDays*27000))*100,0),
            'totalsOliwRealizedTime' => (new Time($totalsOliwRealizedTime))->getUnixFormat(),
            'totalsOliwPlanedTime' => (new Time($totalsOliwPlanedTime))->getUnixFormat(),
            'totalsOliwPlanedTimeSeconds' => $totalsOliwPlanedTime,
            'totalsOutTermOperation' => $totalsOliwRealizedTime>0 ? number_format($totalsOliwRealizedTime/$oliwPlanedTimeSeonds*100,0) : 0,
            'totalsOliwMoney' => $totalsOliwMoney,
            'totalsOuterm' => number_format(!$totalsPlanedTime ?: ($totalsAllRealizedTime / ($totalsPlanedTime = 0 ? 1 : $totalsPlanedTime)) * 100, 1),
            'totalsAllRealizedTime' => (new Time($totalsAllRealizedTime))->getUnixFormat(),
            'totalsPlanedTime' => (new Time($totalsPlanedTime))->getUnixFormat(),
            'totalsMoney' => number_format($totalsMoney, 0),
            'totalsOvertime' => (new Time($totalsOvertime))->getUnixFormat(),
            'totalsOvertimeMoney' => number_format($totalsOvertimeMoney, 0),
        ];

        return $totalData;
    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     * @param \ProductionWorker $productionWorker
     * @param $workerType
     * @return array
     */
    public function getPaymentReportByWorkerData(DateTime $dateFrom, DateTime $dateTo, \ProductionWorker $productionWorker,$workerType = null)
    {
        $dateRanges = (new DateRange($dateFrom, $dateTo))->getAllDates();

        /** @var \Vemid\Entity\Repository\CuttingJobWorksheetRepository $cuttingJobWorksheetRepository */
        $cuttingJobWorksheetRepository = $this->entityManager->getRepository(\CuttingJobWorksheet::class);

        /** @var \Vemid\Entity\Repository\OperatingListItemWorksheetRepository $operatingListItemWorksheetRepository */
        $operatingListItemWorksheetRepository = $this->entityManager->getRepository(\OperatingListItemWorksheet::class);

        /** @var \Vemid\Entity\Repository\ProductionWorkerRepository $productionWorkerRepository */
        $productionWorkerRepository = $this->entityManager->getRepository(\ProductionWorker::class);

        /** @var \Vemid\Entity\Repository\AbsenceWorkRepository $absenceWork */
        $absenceWork = $this->entityManager->getRepository(\AbsenceWork::class);

        $data = [];
        foreach ($dateRanges as $dateRange) {
            $startDate = clone $dateRange;
            $startDate->setDateType(DateTime::TYPE_DATETIME);
            $startDate->setTime(00, 00, 00);

            $endDate = clone $startDate;
            $endDate->setTime(23, 59, 59);

            $breakUp = $productionWorker->getBreakUp() ? $productionWorker->getBreakUp() : (new DateTime('2100-07-01'));

            if($breakUp <= $endDate)
            {
                continue;
            }

            else {


                $cuttinngJobaData = $this->getTotalPaymentPerWorkerAndPeriod($startDate, $endDate, $productionWorker);
                $oliwData = $operatingListItemWorksheetRepository->getTotalPaymentPerWorkerAndPeriod($startDate, $endDate, $productionWorker, $workerType);
                $workDaysData = $productionWorkerRepository->getWorkersWorkDays($productionWorker->getEntityId(), $startDate, $endDate, $workerType);
                $sickDays = $absenceWork->getTotalSick($productionWorker, $startDate, $endDate);
                $vacationDays = $absenceWork->getTotalVacation($productionWorker, $startDate, $endDate);
                $vacationPaidDays = $absenceWork->getTotalPayedVacation($productionWorker, $startDate, $endDate);
                $overtimeTime = $operatingListItemWorksheetRepository->getOvertimeByPeriod($startDate, $endDate, $productionWorker);
                $overtimeData = $operatingListItemWorksheetRepository->getOvertimePaymentPerWorkerAndPeriod($startDate, $endDate, $productionWorker);

                $pauseSeconds = $workDaysData[0]['numberOfDays'] * 1800;
                $oliwRealizedSeconds = $oliwData[0]['totalTime'] * 60;
                $oliwWorktimeSeconds = $oliwData[0]['workTime'] !== null ? $oliwData[0]['workTime'] : 0;
                $cuttinngJobsSeconds = $cuttinngJobaData ? $cuttinngJobaData[0]['totalTime'] * 60 : 0;
                $cuttinngJobsTotalSeconds = $cuttinngJobaData ? $cuttinngJobaData[0]['totalCuttingTime'] * 60 : 0;
                $cuttinngJobsOutTermSeconds = $cuttinngJobsSeconds > (60 * 60 * 8 - $pauseSeconds) ? $cuttinngJobsSeconds - $pauseSeconds : $cuttinngJobsSeconds;
                $vacationMoney = ($vacationDays + $vacationPaidDays) * (8 * $productionWorker->getPriceTime());

                $cuttinngJobsHours = new Time($cuttinngJobsSeconds);
                $oliwRealizedHours = new Time($oliwRealizedSeconds); /* Ostvareno vreme */
                $totalRealizedTime = $cuttinngJobsOutTermSeconds + $oliwRealizedSeconds; /* Ukupno ostvareno vreme*/
                $totalPlanedTime = $cuttinngJobsTotalSeconds + $oliwWorktimeSeconds - $pauseSeconds; /* Ukupno planirano vreme*/
                $overTimeTotal = new Time($overtimeTime);
                $overtimeMoney = $overtimeData[0]['overtimeMoney'] * 1.1;

                $cuttinngJobsMoney = $cuttinngJobsSeconds > (60 * 60 * 8 - $pauseSeconds) ? $cuttinngJobaData[0]['money'] - 72.5 : $cuttinngJobaData[0]['money'];

                $outerm = 0;
                if ($workDaysData[0]['numberOfDays'] > 0) {
                    $outerm = number_format(!$totalPlanedTime ?: ($totalRealizedTime / $totalPlanedTime) * 100, 2);
                }

                $totalMoney = $cuttinngJobsMoney + $oliwData[0]['money'] + $vacationMoney + $overtimeMoney;
                $oliwPlanedTimeSeonds = ($workDaysData[0]['numberOfDays'] * 27000) - $cuttinngJobsSeconds;

                $data[] = [
                    'workerType' => $productionWorker->getWorkerType(),
                    'displayName' => $productionWorker->getDisplayName(),
                    'month' => $dateRange->format('F'),
                    'year' => $dateRange->format('Y'),
                    'date' => $dateRange->getShortSerbianFormat(),
                    'numberOfDays' => $workDaysData[0]['numberOfDays'],
                    'sickDays' => $sickDays,
                    'vacationDays' => $vacationDays,
                    'vacationPaidDays' => $vacationPaidDays,
                    'vacationMoney' => $vacationDays * (8 * $productionWorker->getPriceTime()),
                    'cuttingJobTime' => $cuttinngJobsHours->getUnixFormat(),
                    'cuttingJobSeconds' => $cuttinngJobsSeconds,
                    'cuttingJobMoney' => $cuttinngJobsMoney,
                    'cuttingPercentage' => $cuttinngJobsSeconds > 0 ? ($cuttinngJobsSeconds / ($workDaysData[0]['numberOfDays'] * 27000) * 100) : 0,
                    'oliwRealizedTime' => $oliwRealizedHours->getUnixFormat(),
                    'oliwPlanedTime' => (new Time($oliwPlanedTimeSeonds))->getUnixFormat(),
                    'oliwPlanedTimeSeconds' => $oliwPlanedTimeSeonds,
                    'oliwRealizesSeconds' => $oliwRealizedSeconds,
                    'outTermOperation' => $oliwRealizedSeconds > 0 && $oliwPlanedTimeSeonds>0 ? ($oliwRealizedSeconds / $oliwPlanedTimeSeonds * 100) : 0,
                    'oliwMoney' => $oliwData[0]['money'],
                    'outerm' => $outerm,
                    'totalRealizedTime' => (new Time($totalRealizedTime))->getUnixFormat(),
                    'totalRealizedSeconds' => $totalRealizedTime,
                    'totalPlanedTime' => (new Time($totalPlanedTime))->getUnixFormat(),
                    'totalPlanedSeconds' => $totalPlanedTime,
                    'workTime' => (new Time($oliwWorktimeSeconds))->getUnixFormat(),
                    'totalDataMoney' => $totalMoney,
                    'overtime' => $overTimeTotal->getUnixFormat(),
                    'overtimeSeconds' => $overtimeTime,
                    'overtimeMoney' => $overtimeMoney,
                    'workerId' => $productionWorker->getEntityId(),
                    'worker' => $productionWorker->getWorkerCode(),
                    'sector' => $productionWorker->getSectorCode()->getName()
                ];
            }
        }

        return $data;
    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     * @param \ProductionWorker $productionWorker
     * @return array
     */
    public function getPaymentReportByWorkerDataTotal(DateTime $dateFrom, DateTime $dateTo, \ProductionWorker $productionWorker)
    {
        $paymentDatas = $this->getPaymentReportByWorkerData($dateFrom, $dateTo, $productionWorker);

        $totalData = [];
        $totalsDays = 0;
        $totalsSickDays = 0;
        $totalsVacationDays = 0;
        $totalsVacationPaidDays = 0;
        $totalsCuttingTime = 0;
        $totalsCuttingMoney = 0;
        $totalsOliwRealizedTime = 0;
        $totalsOliwPlanedTime =0;
        $totalsPlanedTime = 0;
        $totalsAllRealizedTime = 0;
        $totalsMoney = 0;
        $totalsOliwMoney = 0;
        $totalsOvertime = 0;
        $totalsOvertimeMoney = 0;
        $totalsVacationMoney = 0;

        foreach ($paymentDatas as $paymentData) {
            $totalsDays += $paymentData['numberOfDays'];
            $totalsSickDays += $paymentData['sickDays'];
            $totalsVacationDays += $paymentData['vacationDays'];
            $totalsVacationPaidDays += $paymentData['vacationPaidDays'];
            $totalsCuttingTime += $paymentData['cuttingJobSeconds'];
            $totalsCuttingMoney += $paymentData['cuttingJobMoney'];
            $totalsOliwMoney += $paymentData['oliwMoney'];
            $totalsOliwRealizedTime += $paymentData['oliwRealizesSeconds'];
            $totalsOliwPlanedTime += $paymentData['oliwPlanedTimeSeconds'];
            $totalsAllRealizedTime += $paymentData['totalRealizedSeconds'];
            $totalsPlanedTime += $paymentData['totalPlanedSeconds'];
            $totalsOvertime += $paymentData['overtimeSeconds'];
            $totalsOvertimeMoney += $paymentData['overtimeMoney'];
            $totalsVacationMoney += $paymentData['vacationMoney'];
            $totalsMoney += $paymentData['totalDataMoney'];
        }

        $totalData[] = [
            'totalsDays' => $totalsDays,
            'totalsSikcDays' => $totalsSickDays,
            'totalsVacationDays' => $totalsVacationDays,
            'totalsVacationPaidDays' => $totalsVacationPaidDays,
            'totalsVacationMoney' => $totalsVacationMoney,
            'totalsCuttingTime' => (new Time($totalsCuttingTime))->getUnixFormat(),
            'totalsCuttingMoney' => number_format($totalsCuttingMoney, 2),
            'totalsCuttingPercentage' => $totalsCuttingTime > 0 ? number_format(($totalsCuttingTime/($totalsDays*27000))*100,0) : 0,
            'totalsOliwRealizedTime' => (new Time($totalsOliwRealizedTime))->getUnixFormat(),
            'totalsOliwPlanedTime' => (new Time($totalsOliwPlanedTime))->getUnixFormat(),
            'totalsOutTermOperation' => $totalsOliwRealizedTime>0 ? number_format($totalsOliwRealizedTime/$totalsOliwPlanedTime*100,0) : 0,
            'totalsOliwMoney' => number_format($totalsOliwMoney, 2),
            'totalsOuterm' => number_format(!$totalsPlanedTime ?: ($totalsAllRealizedTime / $totalsPlanedTime) * 100, 1),
            'totalsAllRealizedTime' => (new Time($totalsAllRealizedTime))->getUnixFormat(),
            'totalsPlanedTime' => (new Time($totalsPlanedTime))->getUnixFormat(),
            'totalsMoney' => number_format($totalsMoney, 0),
            'totalsOvertime' => (new Time($totalsOvertime))->getUnixFormat(),
            'totalsOvertimeMoney' => number_format($totalsOvertimeMoney, 0),
        ];

        return $totalData;
    }

    /* Metoda koja treba da proveri da li neki radnik ima reziju uracunatu sa pauzom */
    public function getTotalPaymentPerWorkerAndPeriod($dateFrom, $dateTo, $productionWorker) {

        /** @var \Vemid\Entity\Repository\CuttingJobWorksheetRepository $cuttingJob */
        $cuttingJob = $this->entityManager->getRepository(\CuttingJobWorksheet::class);

        $cuttinngJobaData = $cuttingJob->getTotalPaymentPerWorkerAndPeriod($dateFrom, $dateTo, $productionWorker);

        $totalTime = 0;
        $money = 0;
        $totalCuttingTime = 0;
        if($cuttinngJobaData) {
            foreach ($cuttinngJobaData as $data) {
                if ($data['totalTime'] >= 240) {
                    $totalTime += $data['totalTime'] - 30;
                    $money += $data['money'] - 72.5;
                } else {
                    $totalTime += $data['totalTime'];
                    $money += $data['money'];
                }
                $totalCuttingTime += $data['totalTime'];

            }
        }
        $result[] = array('totalTime'=>$totalTime,'money'=>$money,'totalCuttingTime' => $totalCuttingTime);

        return $result;
    }

    public function getSectorPerformanceReview(DateTime $dateFrom,DateTime $dateTo, \Code $sector) {


        $dateRanges = (new DateRange($dateFrom, $dateTo))->getAllDates();

        $data = [];
        foreach ($dateRanges as $dateRange) {

            $startDate = clone $dateRange;
            $startDate->setDateType(DateTime::TYPE_DATETIME);
            $startDate->setTime(00, 00, 00);

            $endDate = clone $startDate;
            $endDate->setTime(23, 59, 59);

            $totalData = $this->getPaymentReportBySectorDataTotal($startDate,$endDate,$sector);

            if($totalData[0]['totalsDays']>0) {
                $oliwRealizedSeconds = (new Time($totalData[0]['totalsOliwRealizedTime']))->getTotalSeconds();
                $data[] = [
                    'date' => $startDate->getShortSerbianFormat(),
                    'totalsDays' => $totalData[0]['totalsDays'],
                    'totalsSikcDays' => $totalData[0]['totalsSikcDays'],
                    'totalsVacationDays' => $totalData[0]['totalsVacationDays'],
                    'totalsVacationPaidDays' => $totalData[0]['totalsVacationPaidDays'],
                    'totalsVacationMoney' => $totalData[0]['totalsVacationMoney'],
                    'totalsCuttingTime' => $totalData[0]['totalsCuttingTime'],
                    'totalsCuttingMoney' => $totalData[0]['totalsCuttingMoney'],
                    'totalsCuttingPercentage' => $totalData[0]['totalsCuttingPercentage'],
                    'totalsOliwRealizedTime' => $totalData[0]['totalsOliwRealizedTime'],
                    'totalsOliwPlanedTime' => (new Time($totalData[0]['totalsOliwPlanedTimeSeconds']))->getUnixFormat(),
                    'totalsOliwMoney' => $totalData[0]['totalsOliwMoney'],
                    'totalsAllRealizedTime' => $totalData[0]['totalsAllRealizedTime'],
                    'totalsOutTermOperation' => $oliwRealizedSeconds>0 ? round($oliwRealizedSeconds/$totalData[0]['totalsOliwPlanedTimeSeconds']*100,0) : 0,
                    'totalsMoney' => $totalData[0]['totalsMoney'],
                    'totalsOuterm' => $totalData[0]['totalsOuterm'],
                    'totalsPlanedTime' => $totalData[0]['totalsPlanedTime'],
                    'sectorId' => $sector->getId(),
                    'dateFrom' => $startDate,
                    'dateTo' =>$endDate
//                    'realOuterm' => number_format((new Time($totalData[0]['totalsOliwRealizedTime']))->getTotalMinutes()/($totalData[0]['totalsDays']*450)*100,1)
                ];
            }
        }

        return $data;
    }

    public function getSectorPerformanceReviewTotal(DateTime $dateFrom,DateTime $dateTo, \Code $sector) {

        $performanceReviewDatas = $this->getSectorPerformanceReview($dateFrom,$dateTo,$sector);

        $totalPerformanceData = [];
        $totalPerformanceDays = 0;
        $totalPerformanceSickDays = 0;
        $totalPerformanceVacationDays = 0;
        $totalPerformanceVacationPaidDays = 0;
        $totalPerformanceVacationMoney = 0;
        $totalPerformanceCuttingTime = 0;
        $totalPerformanceCuttingMoney = 0;
        $totalPerformanceOliwRealizedTime = 0;
        $totalPerformanceTotalsOliwMoney = 0;
        $totalPerformancePlanedTime = 0;
        $totalPerformanceAllRealizedTime = 0;
        $totalPerformanceMoney = 0;

        foreach ($performanceReviewDatas as $performanceReviewData) {

            $totalPerformanceDays += $performanceReviewData['totalsDays'];
            $totalPerformanceSickDays += $performanceReviewData['totalsSikcDays'];
            $totalPerformanceVacationDays += $performanceReviewData['totalsVacationDays'];
            $totalPerformanceVacationPaidDays += $performanceReviewData['totalsVacationPaidDays'];
            $totalPerformanceVacationMoney += $performanceReviewData['totalsVacationMoney'];
            $totalPerformanceCuttingTime += (new Time($performanceReviewData['totalsCuttingTime']))->getTotalSeconds();
            $totalPerformanceCuttingMoney += $performanceReviewData['totalsCuttingMoney'];
            $totalPerformanceOliwRealizedTime += (new Time($performanceReviewData['totalsOliwRealizedTime']))->getTotalSeconds();
            $totalPerformanceTotalsOliwMoney += $performanceReviewData['totalsOliwMoney'];
            $totalPerformancePlanedTime += (new Time($performanceReviewData['totalsOliwPlanedTime']))->getTotalSeconds();
            $totalPerformanceAllRealizedTime += (new Time($performanceReviewData['totalsAllRealizedTime']))->getTotalSeconds();
            $totalPerformanceMoney += (float)str_replace(',','',$performanceReviewData['totalsMoney']);

        }

        $totalPerformanceData[] = [
            'totalOutTerm' => number_format($totalPerformanceAllRealizedTime / (60 * 450 * $totalPerformanceDays)*100,1),
            'totalPerformanceDays' => $totalPerformanceDays,
            'totalPerformanceSickDays' => $totalPerformanceSickDays,
            'totalPerformanceVacationDays' => $totalPerformanceVacationDays,
            'totalPerformanceVacationPaidDays' => $totalPerformanceVacationPaidDays,
            'totalPerformanceVacationMoney' => $totalPerformanceVacationMoney,
            'totalPerformanceCuttingTime' => (new Time($totalPerformanceCuttingTime))->getUnixFormat(),
            'totalPerformanceCuttingMoney' => $totalPerformanceCuttingMoney,
            'totalCuttingPercentage' => $totalPerformanceCuttingTime>0 ? round($totalPerformanceCuttingTime/($totalPerformanceDays*27000)*100,0) : 0,
            'totalPerformanceOliwRealizedTime' => (new Time($totalPerformanceOliwRealizedTime))->getUnixFormat(),
            'totalOliwPlanedTime' => (new Time($totalPerformancePlanedTime))->getUnixFormat(),
            'totalPerformanceTotalsOliwMoney' => $totalPerformanceTotalsOliwMoney,
            'totalOutTermOperation' => round($totalPerformanceOliwRealizedTime/$totalPerformancePlanedTime*100,0),
            'totalPerformanceAllRealizedTime' => (new Time($totalPerformanceAllRealizedTime))->getUnixFormat(),
            'totalPerformanceMoney' => $totalPerformanceMoney,
            'totalPlanedTime' => (new Time($totalPerformanceDays*450*60))->getUnixFormat()

        ];

        return $totalPerformanceData;

    }

}
