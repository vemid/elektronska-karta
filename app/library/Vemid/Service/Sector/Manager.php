<?php


namespace Vemid\Service\Sector;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Phalcon\Config;
use Vemid\Application\Di;

/**
 * Class Manager
 * @package Vemid\Service\Sector
 */
class Manager
{

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * Manager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getProductionSectors() {
        /** @var \CodeType $codeType */
        $codeType = $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CHECKIN_LOCATION
            ]
        ]);

        if (!$codeType) {
            throw new \DomainException('Code type do not exist!');
        }

        /** @var \Code $location */
        $location = $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
            \Code::PROPERTY_NAME .' = :name: ',
            'bind' => [
                'codeTypeId' => $codeType->getId(),
                'name' => 'Knjazevac'
            ]
        ]);

        //$manager = $location->getModelsManager();

        $results = $location->getModelsManager()
            ->createBuilder()
            ->addFrom(\ProductionWorker::class, 'pw')
            ->columns([
                'DISTINCT pw.codeId'
            ])
            ->where('pw.locationCodeId = :locationCode: ', [
                'locationCode' => $location->getId()
            ])
            ->getQuery()
            ->execute();

        $data = [];
        /** @var \ProductionWorker $result */
        foreach ($results->toArray() as $result) {
            $data[] = $result['codeId'];
        }

        return $data;

    }

}