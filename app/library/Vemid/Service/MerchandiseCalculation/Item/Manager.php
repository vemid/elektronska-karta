<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 10/9/18
 * Time: 10:06
 */

namespace Vemid\Service\MerchandiseCalculation\Item;

use Vemid\Entity\Manager\EntityManagerInterface;
use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Service\Product\MerchandisePrice;
use Vemid\Task\Merchandise\CalculationTask;

/**
 * Class Manager
 * @package Vemid\Service\MerchandiseCalculation\Item
 */
class Manager
{
    /** @var string */
    private $filePath;

    /** @var int */
    private $merchandiseCalculationId;

    /**
     * Manager constructor.
     * @param string $filePath
     * @param int $merchandiseCalculationId
     */
    public function __construct($filePath, $merchandiseCalculationId)
    {
        $this->filePath = $filePath;
        $this->merchandiseCalculationId = $merchandiseCalculationId;
    }

    public function import()
    {
        if (!file_exists($this->filePath)) {
            throw new \RuntimeException('File do not exist');
        }

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($this->filePath);
        $objPHPExcel->setActiveSheetIndex();
        $sheetData = $objPHPExcel->getActiveSheet()->toArray();

        $counter = 0;
        $indexModel = 0;
        $indexCourse = 1;
        $indexPurchasePrice = 2;
        $indexWholesalePrice = 3;
        $indexRetailPrice = 4;
        foreach ($sheetData as $row) {
            $array_filter = array_filter($row);
            if (\count($array_filter) !== 5) {
                continue;
            }

            if ($counter === 0) {
                $arrayHeader = array_map('strtolower', $array_filter);
                $arrayHeader = array_map('trim', $arrayHeader);
                $flippedData = array_flip($arrayHeader);
                $indexModel = $flippedData['sifra'];
                $indexCourse = $flippedData['kurs'];
                $indexPurchasePrice = $flippedData['nabavna'];
                $indexWholesalePrice = $flippedData['vp'];
                $indexRetailPrice = $flippedData['mp'];
                $counter++;

                continue;
            }

            $filteredData = array_filter($array_filter);
            $data = [
                'productCode' => $filteredData[$indexModel],
                'course' => $filteredData[$indexCourse],
                'purchasePrice' => $filteredData[$indexPurchasePrice],
                'wholesalePrice' => $filteredData[$indexWholesalePrice],
                'retailPrice' => $filteredData[$indexRetailPrice],
            ];

            $task = new CalculationTask();
            $task->data = $data;
            $task->merchandiseCalculationId = $this->merchandiseCalculationId;
            $task->execute();
        }
    }
}
