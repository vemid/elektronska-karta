<?php

namespace Vemid\Service\DeliveryNote;

/**
 * Class Calculator
 * @package Vemid\Service\DeliveryNote
 */
class Calculator
{
    /**
     * @param \OrderItem $orderItem
     * @param $deliveredQtyA
     * @param $deliveredQtyB
     * @return int
     */
    public function getMaximumToDeliverA(\OrderItem $orderItem, $deliveredQtyA, $deliveredQtyB): int
    {
        $deliveryNoteQtyA = $orderItem->getDeliveryNoteItem() ? $orderItem->getDeliveryNoteItem()->getQuantityA() : 0;

        $diff = $orderItem->getQuantityA() - $deliveredQtyA - $deliveryNoteQtyA;
        $total = ($orderItem->getQuantityA() + $orderItem->getQuantityB()) - ($deliveredQtyA + $deliveredQtyB) - $deliveryNoteQtyA;

        $i = $diff > 0 ? $diff : 0;
        $z = $total > 0 ? $total : 0;

        return min([$i, $z]);
    }

    /**
     * @param \OrderItem $orderItem
     * @param $deliveredQtyA
     * @return int
     */
    public function getMaximumToDeliverB(\OrderItem $orderItem, $deliveredQtyA): int
    {
        $deliveryNoteQtyB = $orderItem->getDeliveryNoteItem() ? $orderItem->getDeliveryNoteItem()->getQuantityB() : 0;

        $diff = $orderItem->getQuantityB() - $deliveredQtyA;
        $total = ($orderItem->getQuantityA() + $orderItem->getQuantityB()) - $deliveredQtyA;

        $i = $diff > 0 ? $diff : 0;
        $z = $total > 0 ? $total : 0;

        return min([$i, $z]);
    }
}
