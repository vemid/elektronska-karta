<?php


namespace Vemid\Service\ProductionOrder;


use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Application\Di;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Service\Product\Hydrator;

class ValidateProductionOrderQty
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var \ProductSize */
    private $productSize;

    /**
     * ProductsPrinter constructor.
     * @param EntityManagerInterface $entityManager
     * @param \ProductSize $productSize
     */
    public function __construct(EntityManagerInterface $entityManager,\ProductSize $productSize)
    {
        $this->entityManager = $entityManager;
        $this->productSize = $productSize;
    }

    public function validator()
    {

        /** @var \ProductionOrder $productionOrder */
        $productionOrder = $this->entityManager->findOne(\ProductionOrder::class, [
            \ProductionOrder::PROPERTY_PRODUCT_SIZE_ID . ' = :productSizeId:',
            'bind' => [
                'productSizeId' => $this->productSize->getId()
            ]
        ]);

        $product = $this->productSize->getProduct();

        /** @var \OrderItem $warehouse */
        $warehouse = $this->entityManager->findOne(\OrderItem::class, [
            \OrderItem::PROPERTY_PRODUCT_SIZE_ID . ' = :productSizeId: AND '.
            \OrderItem::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId: AND '.
            \OrderItem::PROPERTY_ENTITY_ID . '= :entityId:',
            'bind' => [
                'productSizeId' => $this->productSize->getId(),
                'entityTypeId' => \Vemid\Entity\Type::CODE,
                'entityId' => '127'
            ]
        ]);

        if(!$warehouse) {
            $warehouse = new \OrderItem();
            $warehouse->setQuantityA('0');
            $warehouse->setQuantityB('0');
            $warehouse->setProductSize($this->productSize);
            $warehouse->setEntityTypeId(\Vemid\Entity\Type::CODE);
            $warehouse->setEntityId('127');
            $warehouse->setOrderId($this->getProductOrder());
            $this->entityManager->save($warehouse);
        }

        /** @var \Vemid\Entity\Repository\ProductSizeRepository $productSizeRepository */
        $productSizeRepository = $this->entityManager->getRepository(\ProductSize::class);
        $totalOrdered = $productSizeRepository->getTotalMPOrder($this->productSize)+$productSizeRepository->getTotalVPOrder($this->productSize);

        //$productionOrderQty = $productionOrder->getRealizedAQty()+$productionOrder->getRealizedBQty();

        $productionOrderQty = $productSizeRepository->getTotalProducedA($this->productSize)+$productSizeRepository->getTotalProducedB($this->productSize);

        if($totalOrdered <> $productionOrderQty) {
            $qty=$warehouse->getQuantityA()-($totalOrdered-$productionOrderQty);
            $warehouse->setQuantityA($qty);
            $this->entityManager->save($warehouse);
        }
    }

    public function getProductOrder() {
        $hydrator = new Hydrator($this->productSize->getProduct(), $this->entityManager);
        $old = $hydrator->getOldCollection();

        /** @var \OrderClassification $orderClassification */
        $orderClassification = $this->entityManager->findOne(\OrderClassification::class,[
            \OrderClassification::PROPERTY_CLASSIFICATION_ID . ' = :classificationId:',
            'bind' => [
                'classificationId' => $old->toArray()['id']
            ]
        ]);

        return (int)$orderClassification->getOrderId();
    }

}