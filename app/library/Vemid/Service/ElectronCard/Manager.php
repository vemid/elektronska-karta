<?php

namespace Vemid\Service\MaterialFactory;

/**
 * Class Manager
 * @package Vemid\Service\ElectronCard
 */
class Manager
{
    /** @var \ElectronCard */
    private $electronCard;

    public function __construct(\ElectronCard $electronCard)
    {
        $this->electronCard = $electronCard;
    }
}