<?php

namespace Vemid\Service\Acl;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Repository\RoleRepository;
use Vemid\Service\Acl\Exception\RoleNotAssignedException;
use Vemid\Service\Acl\Exception\RoleNotFoundException;
use Vemid\Service\Acl\Exception\RoleNotUnassignedException;

/**
 * Class UserRoleAssignments
 *
 * @package Arbor\Crm\Service\Acl
 */
class UserRoleAssignments
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param \User $user
     * @param array $roleCodes
     * @throws RoleNotFoundException
     * @throws RoleNotAssignedException
     * @throws RoleNotUnassignedException
     */
    public function assignRolesToUser(\User $user, array $roleCodes)
    {
        $roleCodes = array_combine(array_values($roleCodes), array_values($roleCodes));

        foreach ($user->getUserRoleAssignments() as $userRoleAssignment) {
            $roleCode = $userRoleAssignment->getRole()->getCode();
            if (array_key_exists($roleCode, $roleCodes)) {
                unset($roleCodes[$roleCode]);
            } else {
                if (!$this->entityManager->delete($userRoleAssignment)) {
                    throw RoleNotUnassignedException::createFromRoleCode($roleCode);
                }
            }
        }

        /** @var RoleRepository $roleRepository */
        $roleRepository = $this->entityManager->getRepository(\Role::class);

        foreach ($roleCodes as $roleCode) {
            if (!$role = $roleRepository->findOneByCode($roleCode)) {
                throw RoleNotFoundException::createFromRoleCode($roleCode);
            }

            $userRoleAssignment = new \UserRoleAssignment();
            $userRoleAssignment->setUser($user);
            $userRoleAssignment->setRole($role);

            if (!$this->entityManager->save($userRoleAssignment)) {
                throw RoleNotAssignedException::createFromRoleCode($roleCode);
            }
        }
    }
}
