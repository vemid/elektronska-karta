<?php

namespace Vemid\Service\Acl\Exception;

/**
 * Class RoleNotUnassignedException
 *
 * @package Service\Acl\Exception
 */
class RoleNotUnassignedException extends RoleException
{
    /**
     * @param string $roleCode
     * @param \Exception|null $previous
     * @return RoleNotUnassignedException
     */
    public static function createFromRoleCode($roleCode, \Exception $previous = null)
    {
        $message = sprintf(
            'Role %s cannot be unassigned', $roleCode
        );

        $ex = new self($message, 0, $previous);
        $ex->roleCode = $roleCode;

        return $ex;
    }
}
