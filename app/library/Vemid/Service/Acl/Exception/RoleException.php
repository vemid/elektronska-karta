<?php

namespace Vemid\Service\Acl\Exception;

/**
 * Class RoleNotAssignedException
 *
 * @package Service\Acl\Exception
 */
class RoleException extends \LogicException
{
    /** @var string */
    protected $roleCode;

    /**
     * @return string
     */
    public function getRoleCode()
    {
        return $this->roleCode;
    }
}
