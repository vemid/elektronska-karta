<?php

namespace Vemid\Service\Acl\Exception;

/**
 * Class RoleNotAssignedException
 *
 * @package Service\Acl\Exception
 */
class RoleNotAssignedException extends RoleException
{
    /**
     * @param string $roleCode
     * @param \Exception|null $previous
     * @return RoleNotAssignedException
     */
    public static function createFromRoleCode($roleCode, \Exception $previous = null)
    {
        $message = sprintf(
            'Role %s cannot be assigned found', $roleCode
        );

        $ex = new self($message, 0, $previous);
        $ex->roleCode = $roleCode;

        return $ex;
    }
}
