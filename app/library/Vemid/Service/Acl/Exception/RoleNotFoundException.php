<?php

namespace Vemid\Service\Acl\Exception;

/**
 * Class RoleNotFoundException
 *
 * @package Service\Acl\Exception
 */
class RoleNotFoundException extends RoleException
{
    /**
     * @param string $roleCode
     * @param \Exception|null $previous
     * @return RoleNotFoundException
     */
    public static function createFromRoleCode($roleCode, \Exception $previous = null)
    {
        $message = sprintf(
            'Role %s is not defined', $roleCode
        );

        $ex = new self($message, 0, $previous);
        $ex->roleCode = $roleCode;

        return $ex;
    }
}
