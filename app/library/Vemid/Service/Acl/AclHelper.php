<?php

namespace Vemid\Service\Acl;

use Phalcon\Acl\AdapterInterface;
use Vemid\Application\Di;
use Vemid\Entity\Manager\EntityManager;

/**
 * Class AccessAllowance
 * @package Arbor\Crm\Service\Acl
 */
class AclHelper
{
    /** @var \User|null */
    public $user;

    /** @var AdapterInterface */
    private $acl;

    /**
     * AccessAllowance constructor.
     * @param \User|null $user
     * @param AdapterInterface $acl
     */
    public function __construct(AdapterInterface $acl, $user = null)
    {
        $this->user = $user;
        $this->acl = $acl;
    }

    /**
     * @param $resource
     * @param $access
     * @return bool
     */
    public function isAllowed($resource, $access)
    {
        if ($this->user === null) {
            $roles = [\Role::ROLE_GUEST];
        } else {
            $roles = [\Role::ROLE_MEMBER];
            foreach ($this->user->getRoles() as $role) {
                $roles[] = $role->getCode();
            }
        }

        foreach ($roles as $role) {
            if ($this->acl->isAllowed($role, $resource, $access)) {
                return true;
            }
        }

        return false;
    }
}