<?php

namespace Vemid\Service\Email;
use Phalcon\Mvc\DispatcherInterface;
use Phalcon\Text;
use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;

/**
 * Class OutboundEmailManager
 * @package Vemid\Service\Email
 */
class OutboundEmailManager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var DispatcherInterface|\Phalcon\DispatcherInterface */
    private $dispatcher;

    /**
     * OutboundEmailManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param DispatcherInterface $dispatcher
     */
    public function __construct(EntityManagerInterface $entityManager, DispatcherInterface $dispatcher)
    {
        $this->entityManager = $entityManager;
        $this->dispatcher = $dispatcher;
    }

    public function prepareOutbound()
    {
        $controllerName = Text::camelize($this->dispatcher->getControllerName());
        $actionName = lcfirst(Text::camelize($this->dispatcher->getActionName()));

        /** @var \EmailNotificationTemplate $emailNotificationTemplate */
        $emailNotificationTemplate = $this->entityManager->findOne(\EmailNotificationTemplate::class, [
            \EmailNotificationTemplate::PROPERTY_CONTROLLER_NAME . ' = :controllerName: AND ' .
            \EmailNotificationTemplate::PROPERTY_ACTION_NAME . ' = :actionName:',
            'bind' => [
                'controllerName' => ucfirst($controllerName),
                'actionName' => $actionName
            ],
            'order' => 'id DESC'
        ]);

        if (!$emailNotificationTemplate) {
            return;
        }

        $body = sprintf('<html><body>%s</body></html>', $emailNotificationTemplate->getBody());

        /** @var \EmailTemplateReceiver[] $emailTemplateReceivers */
        $emailTemplateReceivers = $this->entityManager->find(\EmailTemplateReceiver::class, [
            \EmailTemplateReceiver::PROPERTY_TEMPLATE_OBJECT_ID . ' = :objectId: AND ' .
            \EmailTemplateReceiver::PROPERTY_TEMPLATE_OBJECT_TYPE_ID . ' = :objectTypeId:',
            'bind' => [
                'objectId' => $emailNotificationTemplate->getObjectId(),
                'objectTypeId' => $emailNotificationTemplate->getObjectTypeId(),
            ]
        ]);

        if ($objectName = $emailNotificationTemplate->getEntityObjectName()) {
            /** @var \AuditLog $auditLog */
            $auditLog = $this->entityManager->findOne(\AuditLog::class, [
                'modifiedEntityName = :modifiedEntityName: AND ' .
                'timestamp > :timestamp:',
                'bind' => [
                    'modifiedEntityName' => $objectName,
                    'timestamp' => (new DateTime('-1 year'))->getUnixFormat()
                ],
                'order' => 'timestamp DESC'
            ]);

            if (!$auditLog) {
                return;
            }

            if (!$model = $this->entityManager->findOne($objectName, $auditLog->getModifiedEntityId())) {
                return;
            }

            $twig = new \Twig\Environment(new \Twig\Loader\ArrayLoader(['body' => $body]));
            $replacementLists = [
                lcfirst($objectName) => $model,
                'currentUser' => $auditLog->getUser()
            ];

            try {
                $body = $twig->render('body', $replacementLists);
            } catch (\Twig_Error $exception) {}
        }

        switch ($emailNotificationTemplate->getProlongOption()) {
            case \EmailNotificationTemplate::ONE_HOUR:
                $queuedTime = new DateTime('+1 hour');
                break;
            case \EmailNotificationTemplate::TWO_HOURS:
                $queuedTime = new DateTime('+2 hour');
                break;
            case \EmailNotificationTemplate::SIX_HOURS:
                $queuedTime = new DateTime('+6 hour');
                break;
            case \EmailNotificationTemplate::TWELVE_HOURS:
                $queuedTime = new DateTime('+12 hour');
                break;
            case \EmailNotificationTemplate::TWENTY_FOUR_HOURS:
                $queuedTime = new DateTime('+24 hour');
                break;
            default:
                $queuedTime = new DateTime();
                break;
        }

        foreach ($emailTemplateReceivers as $emailTemplateReceiver) {
            if (!$email = $emailTemplateReceiver->getUser()->getEmail()) {
                continue;
            }

            $outBoundEmail = new \OutboundEmail();
            $outBoundEmail->setTemplateObjectTypeId($emailTemplateReceiver->getTemplateObjectTypeId());
            $outBoundEmail->setTemplateObjectId($emailTemplateReceiver->getTemplateObjectId());
            $outBoundEmail->setFromAddress($emailNotificationTemplate->getFromAddress());
            $outBoundEmail->setToAddress($email);
            $outBoundEmail->setSubject($emailNotificationTemplate->getSubject());
            $outBoundEmail->setBody($body);
            $outBoundEmail->setQueuedDatetime($queuedTime);

            if (!$this->entityManager->save($outBoundEmail)) {
                throw new \DomainException($outBoundEmail->getMessages()[0]->getMessage());
            }
        }
    }
}
