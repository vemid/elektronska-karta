<?php

namespace Vemid\Service\OperationList;

use Phalcon\Mvc\Model\Resultset;
use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;

/**
 * Class ItemCalculator
 * @package Vemid\Service\OperationList
 */
class ItemCalculator
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * ItemCalculator constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @return float|int
     */
    public function outTermAll(DateTime $from, DateTime $to) {
        /** @var \OperatingListItemWorksheet[]|Resultset $worksheets */
        $worksheets = $this->entityManager->find(\OperatingListItemWorksheet::class, [
            \OperatingListItemWorksheet::PROPERTY_DATE . ' >= :fromDate: AND '.
            \OperatingListItemWorksheet::PROPERTY_DATE . ' <= :toDate:',
            'bind' => [
                'fromDate' => $from->getUnixFormat(),
                'toDate' => $to->getUnixFormat()
            ],
            'group' => \OperatingListItemWorksheet::PROPERTY_PRODUCTION_WORKER_ID
        ]);

        $totalSheets = $worksheets->count();
        $totalPercentage = 0;

        /** @var \OperatingListItemWorksheet $worksheet */
        foreach ($worksheets as $worksheet) {
            $totalPercentage += $this->outTermPerWorker($worksheet->getProductionWorker(), $from, $to);
        }

        if (!$totalPercentage) {
            return number_format(0, 2);
        }

        return number_format($totalPercentage / $totalSheets, 2);
    }



    /**
     * @param \ProductionWorker $worker
     * @param DateTime $from
     * @param DateTime $to
     * @return float|int
     */
    public function outTermPerWorker(\ProductionWorker $worker, DateTime $from, DateTime $to)
    {
        /** @var \OperatingListItemWorksheet[]|Resultset $worksheets */
        $worksheets = $this->entityManager->find(\OperatingListItemWorksheet::class, [
            \OperatingListItemWorksheet::PROPERTY_PRODUCTION_WORKER_ID . ' = :workerId: AND '.
            \OperatingListItemWorksheet::PROPERTY_DATE . ' >= :fromDate: AND '.
            \OperatingListItemWorksheet::PROPERTY_DATE . ' <= :toDate:',
            'bind' => [
                'workerId' => $worker->getId(),
                'fromDate' => $from->getUnixFormat(),
                'toDate' => $to->getUnixFormat(),
            ],
            'group' => \OperatingListItemWorksheet::PROPERTY_DATE
        ]);

        $totalSheets = $worksheets->count();
        $totalPercentage = 0;
        foreach ($worksheets as $worksheet) {
            $totalPercentage += $this->outTermPerWorkerAndDate($worker, $worksheet->getDate());
        }

        if (!$totalPercentage) {
            return number_format(0, 2);
        }

        return number_format($totalPercentage / $totalSheets, 2);
    }

    public function outTermPerWorkerAndDate(\ProductionWorker $worker, $date)
    {
        $sdate = new DateTime($date);
        /** @var \OperatingListItemWorksheet[]|Resultset $worksheets */
        $worksheets = $this->entityManager->find(\OperatingListItemWorksheet::class, [
            \OperatingListItemWorksheet::PROPERTY_PRODUCTION_WORKER_ID . ' = :workerId: AND '.
            \OperatingListItemWorksheet::PROPERTY_DATE . ' = :date:',
            'bind' => [
                'workerId' => $worker->getId(),
                'date' => $sdate->getUnixFormat(),
            ]
        ]);

        $totalPercentage = 0;
        foreach ($worksheets as $worksheet) {
            if (!$operationListItem = $worksheet->getOperatingListItem()) {
                continue;
            }

            $totalPercentage += (($worksheet->getQty() * $operationListItem->getPieceTime())/450)*100;
        }

        return number_format($totalPercentage, 2);
    }
    public function outTermPerWorksheetItem(\OperatingListItemWorksheet $id)
    {
        /** @var \OperatingListItemWorksheet[]|Resultset $worksheets */
        $worksheets = $this->entityManager->find(\OperatingListItemWorksheet::class, [
            \OperatingListItemWorksheet::PROPERTY_ID . ' = :id:',
            'bind' => [
                'id' => $id->getId(),
            ]
        ]);

        $totalPercentage = 0;
        foreach ($worksheets as $worksheet) {
            if (!$operationListItem = $worksheet->getOperatingListItem()) {
                continue;
            }
            $time=450;
            if($worksheet->getTimeBetweenWorksheet()>0)
            {
                $time=$worksheet->getTimeBetweenWorksheet();
            }

            //$totalPercentage = (($worksheet->getQty() * $operationListItem->getPieceTime())/$worksheet->getTimeBetweenWorksheet())*100;
            $totalPercentage = (($worksheet->getQty() * $operationListItem->getPieceTime())/$time)*100;
        }

        return number_format($totalPercentage, 2);
    }

    /**
     * @param \Product $product
     * @return float|int
     */
    public function getFinishedQtyByShewingSector(\Product $product){

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var  $sectors */
        $sectors2 = $this->entityManager->find(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :type: and '.
            \Code::PROPERTY_NAME .' like :name: OR '.
            \Code::PROPERTY_NAME .' like :name2: ',
            'bind' => [
                'type' => $codeType->getId(),
                'name' => '%statika%',
                'name2' => '%pletenina%',
            ]
        ]);

        /** @var \Vemid\Entity\Repository\ProductRepository $productRepository */
        $productRepository = $this->entityManager->getRepository(\Product::class);
        return $productRepository->getProductionFinishedQty($product,array_column($sectors2->toArray(),"id"));
    }

    /**
     * @param \Product $product
     * @return float|int
     */
    public function getFinishedQtyByIronSector(\Product $product){

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var  $sectors */
        $sectors2 = $this->entityManager->find(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :type: and '.
            \Code::PROPERTY_NAME .' like :name: ',
            'bind' => [
                'type' => $codeType->getId(),
                'name' => '%pegla%',
            ]
        ]);

        /** @var \Vemid\Entity\Repository\ProductRepository $productRepository */
        $productRepository = $this->entityManager->getRepository(\Product::class);
        return $productRepository->getProductionFinishedQty($product,array_column($sectors2->toArray(),"id"));
    }
    /**
     * @param \Product $product
     * @return float|int
     */
    public function getFinishedQtyByFinishingSector(\Product $product){

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var  $sectors */
        $sectors2 = $this->entityManager->find(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :type: and '.
            \Code::PROPERTY_NAME .' like :name: ',
            'bind' => [
                'type' => $codeType->getId(),
                'name' => '%dorada%',
            ]
        ]);

        /** @var \Vemid\Entity\Repository\ProductRepository $productRepository */
        $productRepository = $this->entityManager->getRepository(\Product::class);
        return $productRepository->getProductionFinishedQty($product,array_column($sectors2->toArray(),"id"));
    }
    /**
     * @param \Product $product
     * @return float|int
     */
    public function getFinishedQtyByPackingSector(\Product $product){

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var  $sectors */
        $sectors2 = $this->entityManager->find(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :type: and '.
            \Code::PROPERTY_NAME .' like :name: ',
            'bind' => [
                'type' => $codeType->getId(),
                'name' => '%pakovanje%',
            ]
        ]);

        /** @var \Vemid\Entity\Repository\ProductRepository $productRepository */
        $productRepository = $this->entityManager->getRepository(\Product::class);
        return $productRepository->getProductionFinishedQty($product,array_column($sectors2->toArray(),"id"));
    }
    /**
     * @param \Product $product
     * @return float|int
     */
    public function getFinishedQtyByControllingSector(\Product $product){

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var  $sectors */
        $sectors2 = $this->entityManager->find(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :type: and '.
            \Code::PROPERTY_NAME .' like :name: ',
            'bind' => [
                'type' => $codeType->getId(),
                'name' => '%Kontrola%',
            ]
        ]);

        /** @var \Vemid\Entity\Repository\ProductRepository $productRepository */
        $productRepository = $this->entityManager->getRepository(\Product::class);
        return $productRepository->getProductionFinishedQty($product,array_column($sectors2->toArray(),"id"));
    }
}