<?php

namespace Vemid\Service\MisWsdl;

use Vemid\Date\DateTime;
use Psr\Log\LoggerInterface;
use \Vemid\Task\Warrants\ImportWarrantsTask;
use Vemid\Entity\Manager\EntityManagerInterface;

/**
 * Class ImportWarrants
 * @package Vemid\Service\MisWsdl
 */
class ImportWarrants extends BaseWsdl
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var DateTime */
    private $startDate;

    /** @var DateTime */
    private $endDate;

    /**
     * ImportWarrants constructor.
     * @param EntityManagerInterface $entityManager
     * @param DateTime|null $startDate
     * @param DateTime|null $endDate
     */
    public function __construct(EntityManagerInterface $entityManager, DateTime $startDate = null, DateTime $endDate = null, LoggerInterface $logger = null)
    {
        $this->entityManager = $entityManager;
        $this->startDate = $startDate ?: (new DateTime())->modify('-1 month');
        $this->endDate = $endDate ?: new DateTime();

        parent::__construct($logger);
    }

    /**
     * @throws \DomainException|\RuntimeException
     */
    public function run()
    {
        /** @var \CodeType $codeType */
        $codeType = $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::SHOPS
            ]
        ]);

        if (!$codeType) {
            throw new \DomainException('Code type do not exist!');
        }

        /** @var \Code[] $shops */
        $shops = $this->entityManager->find(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $shopIds = [];
        foreach ($shops as $shop) {
            if (!$shop->getDealership()) {
                continue;
            }

            $shopIds[] = $shop->getCode();
        }

        $shopIds = implode("', '", $shopIds);

        $sql = <<<SQL
SELECT * FROM elkarta_warrant WHERE shop IN ('$shopIds') AND datum BETWEEN '2022-01-01' AND "{$this->endDate->getUnixFormat()}"
SQL;

        $data = $this->getData($sql);

        foreach ($data as $row){
            $importTask = new ImportWarrantsTask();
            $importTask->dataWarrant = $row;
            $importTask->runInBackground();
        }
    }
}