<?php

namespace Vemid\Service\MisWsdl\Reports;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Error\LoggerInterface;
use Vemid\Service\MisWsdl\BaseWsdl;

/**
 * Class GetMisWarehouseOrderStatus
 * @package Vemid\Service\MisWsdl
 */
class Warrants extends BaseWsdl
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * ImportWarrants constructor.
     * @param EntityManagerInterface $entityManager

     */
    public function __construct(EntityManagerInterface $entityManager,LoggerInterface $logger = null)
    {
        $this->entityManager = $entityManager;

    }

    /**
     * @throws \DomainException|\RuntimeException
     */
    public function run() {
        $sql = <<<SQL
select * from elkarta_warrant_reports where datum_valute >= '2022-01-01' and sifra_objekta <> '28'
SQL;
//        return $this->getData($sql);

        $db = new \PDO("informix:DSN=misystem", "mis2open", "Psw4mis");

        $db->query("set isolation to dirty read;");
        $stmt = $db->query($sql);
        $res = $stmt->fetchAll( \PDO::FETCH_ASSOC );

        return $res;

    }

}