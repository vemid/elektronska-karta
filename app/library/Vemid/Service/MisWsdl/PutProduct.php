<?php

namespace Vemid\Service\MisWsdl;


use Psr\Log\LoggerInterface;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Service\Product\Hydrator;

class PutProduct extends BaseWsdl
{
    /** @var \Product */
    private $product;

    /** @var EntityManagerInterface  */
    private $entityManager;

    /** @var  string */
    private $userName;

    /**
     * PutProduct constructor.
     * @param \Product $product
     * @param EntityManagerInterface $entityManager
     * @param $userName
     * @param LoggerInterface|null $logger
     */
    public function __construct(\Product $product, EntityManagerInterface $entityManager, $userName, LoggerInterface $logger = null)
    {
        $this->product = $product;
        $this->entityManager = $entityManager;
        $this->userName = $userName;

        parent::__construct($logger);
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $checkForExistingProduct = $this->getData("SELECT * FROM roba WHERE sif_rob = '{$this->product->getCode()}'");

        if (empty($checkForExistingProduct)) {
            $product = new Hydrator($this->product, $this->entityManager);
            $procedure = sprintf(
                'execute procedure test_unos_sifre(\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\')',
                $product->getCode(),
                $product->getCode(),
                $product->getProductCodeType() ? $product->getProductCodeType()->getCode() : '',
                $product->getColor() ? $product->getColor()->getCode() : '',
                $product->getName(),
                $this->userName,
                $product->getWebName()
            );

            $response = $this->getData($procedure);

            if (!$response['success']) {
                throw new \RuntimeException($response['message']);
            }

            if ($parentProduct = $this->product->getParentProduct()) {
                $procedure = sprintf(
                    'execute procedure test_unos_sifre(\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\')',
                    $product->getCode(),
                    $parentProduct->getCode(),
                    $product->getProductCodeType() ? $product->getProductCodeType()->getCode() : '',
                    $product->getColor() ? $product->getColor()->getCode() : '',
                    $product->getName(),
                    $this->userName,
                    $product->getWebName()
                );

                $response = $this->getData($procedure);

                if (!$response['success']) {
                    throw new \RuntimeException($response['message']);
                }
            }

            $productClassifications = $this->product->getProductClassifications([
                \ProductClassification::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:',
                'bind' => [
                    'entityTypeId' => Type::CLASSIFICATION
                ]
            ]);

            foreach ($productClassifications as $productClassification) {

                /** @var \Classification $classification */
                if (!$classification = $productClassification->getProductClassification()) {
                    continue;
                }

                if (!$code = $classification->getClassificationType()) {
                    continue;
                }

                if ($code->getCode() === '00') {
                    $productClassification->setIsSynced(true);

                    if (!$this->entityManager->save($productClassification)) {
                        throw new \LogicException(sprintf('Unable to set synced to product classification with ID: %s', $productClassification->getId()));
                    }

                    continue;
                }

                $classificationProcedure = sprintf(
                    'execute procedure test_import_class_robe(\'%s\', \'%s\', \'%s\')',
                    $product->getCode(),
                    $classification->getCode(),
                    $code->getCode()
                );

                $response = $this->getData($classificationProcedure);

                if (!$response['success']) {
                    throw new \RuntimeException($response['message']);
                }

                $productClassification->setIsSynced(true);

                if (!$this->entityManager->save($productClassification)) {
                    throw new \LogicException(sprintf('Unable to set synced to product classification with ID: %s', $productClassification->getId()));
                }
            }

            $webClassificationProcedure = sprintf(
                'execute procedure test_import_class_robe_web(\'%s\')',
                $product->getCode()
            );

            $response = $this->getData($webClassificationProcedure);

            if (!$response['success']) {
                throw new \RuntimeException($response['message']);
            }


            $productAttributes = $this->product->getProductClassifications([
                \ProductClassification::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:',
                'bind' => [
                    'entityTypeId' => Type::ATTRIBUTE
                ]
            ]);


            foreach ($productAttributes as $productAttribute) {
                /** @var \Attribute $attribute */
                if (!$attribute = $productAttribute->getProductClassification()) {
                    continue;
                }

                if (!$code = $attribute->getAttributeCode()) {
                    continue;
                }

                $attributeProcedure = sprintf(
                    'execute procedure test_import_attributes(\'%s\',\'%s\',\'%s\',\'%s\');',
                    $this->product->getCode(),
                    $code->getCode(),
                    $attribute->getName(),
                    $this->userName
                );

                $response = $this->getData($attributeProcedure);

                if (!$response['success']) {
                    throw new \RuntimeException($response['message']);
                }

                $productAttribute->setIsSynced(true);

                if (!$this->entityManager->save($productAttribute)) {
                    throw new \LogicException(sprintf('Unable to set synced to product attribute with ID: %s', $productAttribute->getId()));
                }
            }

            foreach ($this->product->getProductSizes() as $productSize) {
                $code = $productSize->getCode();

                if ($code && $productSize->getBarcode()) {
                    $procedure = sprintf(
                        'execute procedure test_import_barkod(\'%s\', \'%s\', \'%s\')',
                        $product->getCode(),
                        $productSize->getBarcode(),
                        $code->getCode()
                    );

                    $response = $this->getData($procedure);

                    if (!$response['success']) {
                        throw new \RuntimeException($response['message']);
                    }

                    $productSize->setIsSynced(true);

                    if (!$this->entityManager->save($productSize)) {
                        throw new \LogicException(sprintf('Unable to set synced to product size with ID: %s', $productSize->getId()));
                    }
                }
            }

            $this->product->setStatus(\Product::STATUS_FINISHED);
            $this->product->setIsSynced(true);
            $this->entityManager->save($this->product);
        }
    }
}
