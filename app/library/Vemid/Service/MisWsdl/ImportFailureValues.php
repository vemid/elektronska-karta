<?php

namespace Vemid\Service\MisWsdl;

use Vemid\Task\Failure\ImportFailureValuesTask;

/**
 * Class ImportFailureValues
 * @package Vemid\Service\MisWsdl
 */
class ImportFailureValues extends BaseWsdl
{
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $data = $this->getData("SELECT * from show_feleri where left(feler,2)='MP' and right(left(roba,3),2) in ('19','20','21') and length(trim(roba))=13 and storno='N'");

        foreach ($data as $row){
            $importTask = new ImportFailureValuesTask();
            $importTask->codeData = $row;
            $importTask->runInBackground();
        }
    }
}