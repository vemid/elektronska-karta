<?php

namespace Vemid\Service\MisWsdl;


use Psr\Log\LoggerInterface;
use Vemid\Entity\Manager\EntityManagerInterface;

class PutProductClassificationSync extends BaseWsdl
{
    /** @var \ProductClassification */
    private $productClassification;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * PutProductClassificationSync constructor.
     * @param EntityManagerInterface $entityManager
     * @param \ProductClassification $productClassification
     * @param LoggerInterface|null $logger
     */
    public function __construct(EntityManagerInterface $entityManager, \ProductClassification $productClassification, LoggerInterface $logger = null)
    {
        $this->entityManager = $entityManager;
        $this->productClassification = $productClassification;

        parent::__construct($logger);
    }

    public function run()
    {
        $classification = $this->productClassification->getProductClassification();
        $code = $classification->getClassificationType();

        $classificationQuery = sprintf(
            'SELECT sif_rob from roba_klas_robe WHERE sif_rob = \'%s\' AND sif_kri_kla = \'%s\' AND kla_ozn = \'%s\'',
            $this->productClassification->getProduct()->getCode(),
            $code->getCode(),
            $classification->getCode()
        );

        try {
            $classificationResponse = $this->getData($classificationQuery);

            if (!empty($classificationResponse)) {
                $this->productClassification->setIsSynced(true);

                if (!$this->entityManager->save($this->productClassification)) {
                    throw new \LogicException(sprintf('Unable to set synced to product classification with ID: %s', $this->productClassification->getId()));
                }

                return;
            }
        } catch (\Exception $e) {}

        $classificationProcedure = sprintf(
            'execute procedure test_import_class_robe(\'%s\', \'%s\', \'%s\')',
            $this->productClassification->getProduct()->getCode(),
            $classification->getCode(),
            $code->getCode()
        );

        try {
            $response = $this->getData($classificationProcedure);

            if (!$response['success']) {
                throw new \LogicException(sprintf('Error on Service - %s', $response['error']));
            }

            $this->productClassification->setIsSynced(true);

            if (!$this->entityManager->save($this->productClassification)) {
                throw new \LogicException(sprintf('Unable to set synced to product classification with ID: %s', $this->productClassification->getId()));
            }
        } catch (\Exception $e) {}
    }
}