<?php

namespace Vemid\Service\MisWsdl\WarehouseOrderGroup;

use Phalcon\Config;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Service\WarehouseOrders\WarehouseGroupProducts;

final class MisGroupPusher
{
    /**
     * @var WarehouseGroupProducts
     */
    private $warehouseGroupProducts;

    /** @var Config */
    private $config;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    /**
     * @param WarehouseGroupProducts $warehouseGroupProducts
     * @param Config $config
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(WarehouseGroupProducts $warehouseGroupProducts, Config $config,EntityManagerInterface $entityManager)
    {
        $this->warehouseGroupProducts = $warehouseGroupProducts;
        $this->config = $config;
        $this->entityManager = $entityManager;
    }

    /**
     * @param \WarehouseOrderGroup $warehouseOrderGroup
     * @return bool
     */
    public function __invoke(\WarehouseOrderGroup $warehouseOrderGroup): bool
    {
        /** @var MisGroupPusherInterface $misDocumentGroupPusherClass */
        $misDocumentGroupPusherClass = $this->getMisDocumentGroupPusherType($warehouseOrderGroup);
        $wsdl = $this->config->wsdl->{$misDocumentGroupPusherClass};

        /** @var MisGroupPusherInterface $misDocumentGroupPusherService */
        $misDocumentGroupPusherService = new $misDocumentGroupPusherClass($this->warehouseGroupProducts, $wsdl,$this->entityManager);

        return $misDocumentGroupPusherService($warehouseOrderGroup);
    }

    /**
     * @param \WarehouseOrderGroup $warehouseOrderGroup
     * @return string
     */
    private function getMisDocumentGroupPusherType(\WarehouseOrderGroup $warehouseOrderGroup): string
    {
        $channelSupplyEntity = $warehouseOrderGroup->getChannelSupplyEntity();
        $type = $channelSupplyEntity->getMisDocumentGroupType();
        $documentTypeMap = $this->getMisDocumentGroupPusherMap();

        return $documentTypeMap[$type] ?? NullablePusher::class;
    }

    /**
     * @return array
     */
    public function getMisDocumentGroupPusherMap(): array
    {
        return [
            \ChannelSupplyEntity::BK_STORE_GROUP => PushMpStoresGroupPusher::class,
            \ChannelSupplyEntity::FRANCHISE_GROUP => PushFranchiseGroupPusher::class,
            \ChannelSupplyEntity::WAREHOUSE_GROUP => PushWarehouseGroupPusher::class,
            \ChannelSupplyEntity::DM_CLIENTS_GROUP => PushDomesticFranchiseGroupPusher::class,
        ];
    }
}