<?php

namespace Vemid\Service\MisWsdl\WarehouseOrderGroup;

interface MisGroupPusherInterface
{
    /**
     * @param \WarehouseOrderGroup $warehouseOrderGroup
     * @return bool
     */
    public function __invoke(\WarehouseOrderGroup $warehouseOrderGroup): bool;
}
