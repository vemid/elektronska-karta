<?php

namespace Vemid\Service\MisWsdl\WarehouseOrderGroup;

use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Service\WarehouseOrders\WarehouseGroupProducts;

class PushDomesticFranchiseGroupPusher implements MisGroupPusherInterface
{
    /**
     * @var WarehouseGroupProducts
     */
    private $warehouseGroupProducts;

    /** @var string|null */
    private $wsdl;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param WarehouseGroupProducts $warehouseGroupProducts
     * @param string|null $wsdl
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(WarehouseGroupProducts $warehouseGroupProducts, string $wsdl = null,EntityManagerInterface $entityManager)
    {
        $this->warehouseGroupProducts = $warehouseGroupProducts;
        $this->wsdl = $wsdl;
        $this->entityManager = $entityManager;
    }

    /**
     * @param \WarehouseOrderGroup $warehouseOrderGroup
     * @return bool
     */
    public function __invoke(\WarehouseOrderGroup $warehouseOrderGroup): bool
    {
        $orderVp = new \dodajNalogZaIzdavanje();
        $order = $orderVp->setDokumenti([$this->prepareOrder($warehouseOrderGroup)]);

        $service = new \NalogZaIzdavanjeService([], $this->wsdl);
        $response = $service->dodajNalogZaIzdavanje($order);

        return $this->getResponse($response);
    }

    /**
     * @param \WarehouseOrderGroup $warehouseOrderGroup
     * @return \nalogZaIzdavanje
     */
    private function prepareOrder(\WarehouseOrderGroup $warehouseOrderGroup): \nalogZaIzdavanje
    {

        $channelSupplyEntity = $warehouseOrderGroup->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();

        /** @var \Code|\Client $entity */
        $entity = $channelSupplyEntity->getEntity();
        $note = $entity->getDisplayName();
        $partnerCode = "000000";

        $warehouseOrderCodes = "";
        foreach ($warehouseOrderGroup->getWarehouseOrderGroupItems() as $item) {
            $warehouseOrderCodes.= $item->getWarehouseOrder()->getWarehouseOrderCode().", ";
        }

        $cenovnik = '01/150000004';
        if ($supplyChannel->getType() === \ChannelSupply::VP) {
            $cenovnik = '01/140000002';

        }

        if($warehouseOrderGroup->getType() == "A") {
            $warehouseExit = 'KP';
        }
        elseif($warehouseOrderGroup->getType() == "B") {
            $warehouseExit = 'XKP';
        }

        $order = new \nalogZaIzdavanje(0,0,0);
        $order->setBrojRata(0);
        $order->setBrojRata(0);
        $order->setCenaPrevoza(0);
        $order->setGratisPer(0);
        $order->setDatumDokumenta(new DateTime());
        $order->setDatumOtpreme(new DateTime());
        $order->setDpo(new DateTime());
        $order->setLogname('elektronska.servis');
        $order->setMarza(0);
        $order->setOznakaCenovnika($cenovnik);
        $order->setOznakaDokumenta($warehouseOrderGroup->getWarehouseOrderGroupCode());
        $order->setRealizovano("N");
        $order->setRokIsporukeUDanima(7);
        $order->setSifraMagacina($warehouseExit);
        $order->setSifraNacinaPlacanja(1);
        $order->setSifraOrganizacioneJednice("013");
        $order->setSifraOrganizacioneJedniceNarucioca("013");
        $order->setStatus("0");
        $order->setKreirajAhp("0");
        $order->setIznosKasaSkonto("0");
        $order->setIznosManipulativnihTroskova("0");
        $order->setKasaSkonto("0");
        $order->setStopaManipulativnihTroskova("0");
        $order->setStorno('N');
        $order->setValutaPlacanja(new DateTime());
        $order->setVrstaFakturisanja(1);
        $order->setVrstaIzjave(2);
        $order->setSifraRacuna('RB');
        $order->setTipNaloga(0);
        $order->setTipNaloga(0);
        $order->setSifraPartnera($channelSupplyEntity->getClientCode());
        $order->setSifraPartneraKorisnika($channelSupplyEntity->getClientCode());

        $order->setStavke($this->prepareOrderItems($warehouseOrderGroup));

        return $order;
    }

    private function prepareOrderItems(\WarehouseOrderGroup $warehouseOrderGroup): array
    {
        $channelSupplyEntity = $warehouseOrderGroup->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();
        $cenovnik = '01/140000001';
        if ($supplyChannel->getType() === \ChannelSupply::VP) {
            $cenovnik = '01/140000002';
        }


        /** @var \Pricelist $pricelist */
        $pricelist = $this->entityManager->findOne(\Pricelist::class, [
            \Pricelist::PROPERTY_CODE . ' = :priceList: ',
            'bind' => [
                'priceList' => $cenovnik
            ]
        ]);

        $items = [];
        $data = $this->warehouseGroupProducts->getSumWarehouseOrders($warehouseOrderGroup,$pricelist);
        foreach ($data as $warehouseOrderGroupItem) {
            $osnovnaCenabezPoreza = 0;
            if ($supplyChannel->getType() === \ChannelSupply::VP) {
                $osnovnaCenabezPoreza=$warehouseOrderGroupItem['wholesalePrice'];
            }
            else {
                $osnovnaCenabezPoreza = round($warehouseOrderGroupItem['retailPrice']*0.83333,3);
            }
            $item = new \nalogZaIzdavanjeStavka();
            $item->setAkcijskaStopaRabata(0);
            $item->setBrojPakovanja(0);
            $item->setBruto($warehouseOrderGroupItem['qty']);
            $item->setDodatniRabat(0);
            $item->setIznosAkcize(0);
            $item->setIznosManipulativnihTroskova(0);
            $item->setIznosTakse(0);
            $item->setPosebnaStopaRabata(0);
            $item->setRabatBezAkciza(0);
            $item->setKolicina($warehouseOrderGroupItem['qty']);
            $item->setCenaSaRabatom($osnovnaCenabezPoreza);
            $item->setOsnovnaCena($osnovnaCenabezPoreza);
            $item->setDeviznaCena($osnovnaCenabezPoreza);
            $item->setProdajnaCena(round($osnovnaCenabezPoreza*1.2,0));
            $item->setSifraRobe($warehouseOrderGroupItem['sku']);
            $item->setSifraObelezja($warehouseOrderGroupItem['size']);
            $item->setStopaManipulativnihTroskova(0);
            $item->setStopaPoreza('20');
            $item->setStopaRabata('0');
            $item->setZahtevanaKolicina($warehouseOrderGroupItem['qty']);

            $items[] = $item;
        }

        return $items;
    }

    /**
     * @param \dodajNalogZaIzdavanjeResponse $response
     * @return bool
     * @throws \LogicException
     */
    private function getResponse(\dodajNalogZaIzdavanjeResponse $response): bool
    {
        /** @var \nalogZaIzdavanjeResponse $return */
        if ($return = $response->getReturn()) {

            if (!$return->getResponseResult()) {
                if ($return->getErrorMessage()) {
                    return $return->getResponseResult();
                }

                return false;
            }

            return $return->getResponseResult();
        }

        if (method_exists($return, 'getErrorMessage') && $return->getErrorMessage()) {
            throw new \LogicException($return->getErrorMessage());
        }

        return false;
    }
}