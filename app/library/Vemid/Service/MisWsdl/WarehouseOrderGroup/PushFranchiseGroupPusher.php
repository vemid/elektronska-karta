<?php

namespace Vemid\Service\MisWsdl\WarehouseOrderGroup;

use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Service\WarehouseOrders\WarehouseGroupProducts;

class PushFranchiseGroupPusher implements MisGroupPusherInterface
{
    /**
     * @var WarehouseGroupProducts
     */
    private $warehouseGroupProducts;

    /** @var string|null */
    private $wsdl;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param WarehouseGroupProducts $warehouseGroupProducts
     * @param string|null $wsdl
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(WarehouseGroupProducts $warehouseGroupProducts, string $wsdl = null,EntityManagerInterface $entityManager)
    {
        $this->warehouseGroupProducts = $warehouseGroupProducts;
        $this->wsdl = $wsdl;
        $this->entityManager = $entityManager;
    }

    /**
     * @param \WarehouseOrderGroup $warehouseOrderGroup
     * @return bool
     */
    public function __invoke(\WarehouseOrderGroup $warehouseOrderGroup): bool
    {
        $orderVp = new \dodajNarudzbenicuKupca();
        $order = $orderVp->setNarudzbenice([$this->prepareOrder($warehouseOrderGroup)]);

        $service = new \NarudzbenicaKupcaServisService([], $this->wsdl);
        $response = $service->dodajNarudzbenicuKupca($order);

        return $this->getResponse($response);
    }

    /**
     * @param \WarehouseOrderGroup $warehouseOrderGroup
     * @return \narudzbenicaKupca
     */
    private function prepareOrder(\WarehouseOrderGroup $warehouseOrderGroup): \narudzbenicaKupca
    {

        $channelSupplyEntity = $warehouseOrderGroup->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();

        /** @var \Code|\Client $entity */
        $entity = $channelSupplyEntity->getEntity();
        $note = $entity->getDisplayName();
        $partnerCode = "000000";

        $warehouseOrderCodes = "";
        foreach ($warehouseOrderGroup->getWarehouseOrderGroupItems() as $item) {
            $warehouseOrderCodes.= $item->getWarehouseOrder()->getWarehouseOrderCode().", ";
        }

        $order = new \narudzbenicaKupca();
        $order->setDatumNaurdzbenice(new DateTime());
        $order->setDeviznaNarudzbenica(0);
        $order->setLogname('elektronska.servis');
        $order->setNapomena('Narudzbina za :'.$note. ", dzakovi : ".$warehouseOrderCodes);
        $order->setOrganizacionaJedinica('013');
        $order->setOznakaCenovnika('01/140000001');
        $order->setOznakaNarudzbenice($warehouseOrderGroup->getWarehouseOrderGroupCode());
        $order->setOznakaNarudzbenicePartnera($warehouseOrderGroup->getType().$warehouseOrderGroup->getWarehouseOrderGroupCode());
        $order->setPartnerKorisnik($channelSupplyEntity->getClientCode());
        $order->setSifraPartnera($channelSupplyEntity->getClientCode());
        $order->setRealizovano(0);
        $order->setSifraNacinaPlacanja('1');
        $order->setStavke($this->prepareOrderItems($warehouseOrderGroup));
        $order->setStorno('N');

        return $order;
    }

    private function prepareOrderItems(\WarehouseOrderGroup $warehouseOrderGroup): array
    {
        $channelSupplyEntity = $warehouseOrderGroup->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();
        $cenovnik = '01/140000001';
        if ($supplyChannel->getType() === \ChannelSupply::VP) {
            $cenovnik = '01/140000002';
        }


        /** @var \Pricelist $pricelist */
        $pricelist = $this->entityManager->findOne(\Pricelist::class, [
            \Pricelist::PROPERTY_CODE . ' = :priceList: ',
            'bind' => [
                'priceList' => $cenovnik
            ]
        ]);

        $items = [];
        $data = $this->warehouseGroupProducts->getSumWarehouseOrders($warehouseOrderGroup,$pricelist);
        foreach ($data as $warehouseOrderGroupItem) {

            $item = new \narudzbenicaKupcaStavka();
            $item->setAkcijskaStopaRabata(0);
            $item->setCenaSaPorezom($warehouseOrderGroupItem['retailPrice']);
            $item->setDodatniRabat('0');
            $item->setKolicina($warehouseOrderGroupItem['qty']);
            $item->setOsnovnaCenaSaRabatom($warehouseOrderGroupItem['purchasePrice']);
            $item->setOsnovnaCenaBezporeza($warehouseOrderGroupItem['purchasePrice']);
            $item->setOsnovnavrednostSaSaRabatom($warehouseOrderGroupItem['purchasePrice']);
            $item->setSifraRobe($warehouseOrderGroupItem['sku']);
            $item->setSifraObelezja($warehouseOrderGroupItem['size']);
            $item->setStopaPoreza('20');
            $item->setStopaRabata('0');
            $item->setRokIsporuke((new DateTime())->modify("+1 year"));
            $item->setVrednostSaPorezom($warehouseOrderGroupItem['retailPrice']);

            $items[] = $item;
        }

        return $items;
    }

    /**
     * @param \dodajNarudzbenicuKupcaResponse $response
     * @return bool
     * @throws \LogicException
     */
    private function getResponse(\dodajNarudzbenicuKupcaResponse $response): bool
    {
        /** @var \prenosResponse $return */
        if ($return = $response->getReturn()) {

            if (!$return->getResponseResult()) {
                if ($return->getErrorMessage()) {
                    return $return->getResponseResult();
                }

                return false;
            }

            return $return->getResponseResult();
        }

        if (method_exists($return, 'getErrorMessage') && $return->getErrorMessage()) {
            throw new \LogicException($return->getErrorMessage());
        }

        return false;
    }
}