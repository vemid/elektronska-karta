<?php

namespace Vemid\Service\MisWsdl\WarehouseOrderGroup;

use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Service\WarehouseOrders\WarehouseGroupProducts;

class PushMpStoresGroupPusher implements MisGroupPusherInterface
{
    /**
     * @var WarehouseGroupProducts
     */
    private $warehouseGroupProducts;

    /** @var string|null */
    private $wsdl;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param WarehouseGroupProducts $warehouseGroupProducts
     * @param string|null $wsdl
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(WarehouseGroupProducts $warehouseGroupProducts, string $wsdl = null,EntityManagerInterface $entityManager)
    {
        $this->warehouseGroupProducts = $warehouseGroupProducts;
        $this->wsdl = $wsdl;
        $this->entityManager = $entityManager;
    }

    /**
     * @param \WarehouseOrderGroup $warehouseOrderGroup
     * @return bool
     */
    public function __invoke(\WarehouseOrderGroup $warehouseOrderGroup): bool
    {
        $orderMp = new \dodajOtpremnicuUMaloprodaju();
        $order = $orderMp->setOtpremnicaUMaloprodaju($this->prepareOrders($warehouseOrderGroup));

        $service = new \OptremnicaServisService([], $this->wsdl);
        $response = $service->dodajOtpremnicuUMaloprodaju($order);

        return $this->getResponse($response);
    }

    /**
     * @param \WarehouseOrderGroup $warehouseOrderGroup
     * @return \otpremnicaUMaloprodaju
     */
    private function prepareOrders(\WarehouseOrderGroup $warehouseOrderGroup): \otpremnicaUMaloprodaju
    {
        $channelSupplyEntity = $warehouseOrderGroup->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();

        $warehouseOrderCodes = "";
        foreach ($warehouseOrderGroup->getWarehouseOrderGroupItems() as $item) {
            $warehouseOrderCodes.= $item->getWarehouseOrder()->getWarehouseOrderCode().", ";
        }

        /** @var \Code|\Client $entity */
        $entity = $channelSupplyEntity->getEntity();
        $note = $channelSupplyEntity->getDisplayName();

        $postalAddress = $channelSupplyEntity->getPostalAddress();
        if ($supplyChannel->getType() === \ChannelSupply::VP) {
            $postalAddress = $entity->getAddress();
            $note = $entity->getDisplayName();
        }

        $order = new \otpremnicaUMaloprodaju(2);
        $order->setDatumDokumenta(new DateTime());
        $order->setLogname('elektronska.servis');
        $order->setNapomena('Porudzbina za: ' . $note . ", dzakovi : ".$warehouseOrderCodes);
        $order->setOznakaCenovnika('01/140000001');
        $order->setOznakaDokumenta($warehouseOrderGroup->getWarehouseOrderGroupCode());
        $order->setSifraMagacina('KP');
        $order->setSifraObjektaMaloprodaje($entity->getCode());
        $order->setStorno('N');
        $order->setStavke($this->prepareItems($warehouseOrderGroup));
        $order->setVrstaKnjizenja(2);

        return $order;
    }

    /**
     * @return array|\otpremnicaUMaloprodajuStavka[]
     */
    private function prepareItems(\WarehouseOrderGroup $warehouseOrderGroup): array
    {
        $channelSupplyEntity = $warehouseOrderGroup->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();
        $cenovnik = '01/140000001';
        if ($supplyChannel->getType() === \ChannelSupply::VP) {
            $cenovnik = '01/140000002';
        }


        /** @var \Pricelist $pricelist */
        $pricelist = $this->entityManager->findOne(\Pricelist::class, [
            \Pricelist::PROPERTY_CODE . ' = :priceList: ',
            'bind' => [
                'priceList' => $cenovnik
            ]
        ]);

        $items = [];
        $counter = 1;
        $data = $this->warehouseGroupProducts->getSumWarehouseOrders($warehouseOrderGroup,$pricelist);
        foreach ($data as $warehouseOrderGroupItem) {

            $item = new \otpremnicaUMaloprodajuStavka();
            $item->setAkcijskaStopaRabata(0);
            $item->setCenaZalihe($warehouseOrderGroupItem['purchasePrice']);
            $item->setKolicina($warehouseOrderGroupItem['qty']);
            $item->setKolicina1($warehouseOrderGroupItem['qty']);
            $item->setOsnovnaCena($warehouseOrderGroupItem['purchasePrice']);
            $item->setProdajnaCenaBezPoreza(round($warehouseOrderGroupItem['retailPrice']*0.83333,3));
            $item->setProdajnaCena($warehouseOrderGroupItem['retailPrice']);
            $item->setProdajnaCenaSaRabatom($warehouseOrderGroupItem['retailPrice']);
            $item->setSifraRobe($warehouseOrderGroupItem['sku']);
            $item->setSifraObelezja($warehouseOrderGroupItem['size']);
            $item->setSifraTarifneGrupePoreza('100');
            $item->setStopaPDV(20);
            $item->setStopaRabata(0);
            $item->setPosebnaStopaRabata(0);
            $item->setMaloprodajnaMarza(0);
            $item->setBrojPakovanja(0);

            $counter++;
            $items[] = $item;
        }

        return $items;
    }

    /**
     * @param \dodajOtpremnicuUMaloprodajuResponse $response
     * @return bool
     * @throws \LogicException
     */
    private function getResponse(\dodajOtpremnicuUMaloprodajuResponse $response)
    {
        /** @var \otpremnicaUMaloprodajuResponse $return */
        if ($return = $response->getReturn()) {

            if (!$return->getResponseResult()) {
                if ($return->getErrorMessage()) {
                    return $return->getResponseResult();
                }

                return false;
            }

            return $return->getResponseResult();
        }

        if (method_exists($return, 'getErrorMessage') && $return->getErrorMessage()) {
            throw new \LogicException($return->getErrorMessage());
        }

        return false;
    }
}
