<?php

namespace Vemid\Service\MisWsdl\WarehouseOrderGroup;

use Vemid\Service\WarehouseOrders\WarehouseGroupProducts;

class NullablePusher implements MisGroupPusherInterface
{
    /**
     * @param WarehouseGroupProducts $warehouseGroupProducts
     * @param string|null $wsdl
     */
    public function __construct(WarehouseGroupProducts $warehouseGroupProducts, string $wsdl = null){}

    /**
     * @param \WarehouseOrderGroup $warehouseOrderGroup
     * @return bool
     */
    public function __invoke(\WarehouseOrderGroup $warehouseOrderGroup): bool
    {
        return false;
    }
}
