<?php

namespace Vemid\Service\MisWsdl\WarehouseOrderGroup;

use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Service\WarehouseOrders\WarehouseGroupProducts;

class PushWarehouseGroupPusher implements MisGroupPusherInterface
{
    /**
     * @var WarehouseGroupProducts
     */
    private $warehouseGroupProducts;

    /** @var string|null */
    private $wsdl;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param WarehouseGroupProducts $warehouseGroupProducts
     * @param string|null $wsdl
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(WarehouseGroupProducts $warehouseGroupProducts, string $wsdl = null,EntityManagerInterface $entityManager)
    {
        $this->warehouseGroupProducts = $warehouseGroupProducts;
        $this->wsdl = $wsdl;
        $this->entityManager = $entityManager;
    }

    /**
     * @param \WarehouseOrderGroup $warehouseOrderGroup
     * @return bool
     */
    public function __invoke(\WarehouseOrderGroup $warehouseOrderGroup): bool
    {
        $orderMp = new \dodajPrenos();
        $order = $orderMp->setPrenosi([$this->prepareOrders($warehouseOrderGroup)]);

        $service = new \PrenosServisService([], $this->wsdl);
        $response = $service->dodajPrenos($order);

        return $this->getResponse($response);
    }

    /**
     * @param \WarehouseOrderGroup $warehouseOrderGroup
     * @return \prenos
     */
    private function prepareOrders(\WarehouseOrderGroup $warehouseOrderGroup): \prenos
    {
        $channelSupplyEntity = $warehouseOrderGroup->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();

        /** @var \Code|\Client $entity */
        $entity = $channelSupplyEntity->getEntity();

        $note = $channelSupplyEntity->getDisplayName();

        $warehouseExit = 'KP';
        $warehouseEntry = 'VP';

        if($warehouseOrderGroup->getType() == "A") {
            $warehouseExit = 'KP';
            $warehouseEntry = $channelSupplyEntity->getEntryWarehouse();
        }
        elseif($warehouseOrderGroup->getType() == "B") {
            $warehouseExit = 'XKP';
            $warehouseEntry= 'XMP';
        }

        $order = new \prenos();
        $order->setStavke($this->prepareItems($warehouseOrderGroup));
        $order->setVrstaKnjizenja(\vrstaKnjizenja::SamoIzlaz);
        $order->setVrstaPrenosa(\vrstaPrenosa::externiPrenos);
        $order->setNapomena("Porudzbenica za : ".$note);
        $order->setDatumDokumenta(new DateTime());
        $order->setOznakaDokumenta($warehouseOrderGroup->getWarehouseOrderGroupCode());
        $order->setSifraMagacinaIzlaza($warehouseExit);
        $order->setSifraMagacinaUlaza($warehouseEntry);
        $order->setStorno('N');
        $order->setLogname('elektronska.servis');

        return $order;
    }

    /**
     * @return array|\prenosStavka[]
     */
    private function prepareItems(\WarehouseOrderGroup $warehouseOrderGroup): array
    {
        $channelSupplyEntity = $warehouseOrderGroup->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();
        $cenovnik = '01/140000001';
        if ($supplyChannel->getType() === \ChannelSupply::VP) {
            $cenovnik = '01/140000002';
        }


        /** @var \Pricelist $pricelist */
        $pricelist = $this->entityManager->findOne(\Pricelist::class, [
            \Pricelist::PROPERTY_CODE . ' = :priceList: ',
            'bind' => [
                'priceList' => $cenovnik
            ]
        ]);

        $items = [];
        $data = $this->warehouseGroupProducts->getSumWarehouseOrders($warehouseOrderGroup,$pricelist);
        foreach ($data as $warehouseOrderGroupItem) {

            $item = new \prenosStavka();
            $item->setSifraRobe($warehouseOrderGroupItem['sku']);
            $item->setSifraObelezja($warehouseOrderGroupItem['size']);
            $item->setSifraObelezjaU($warehouseOrderGroupItem['size']);
            $item->setNabavnaCena($warehouseOrderGroupItem['retailPrice']);
            $item->setKolicina($warehouseOrderGroupItem['qty']);

            $items[] = $item;
        }

        return $items;
    }

    /**
     * @param \dodajNalogZaPrenosResponse $response
     * @return bool
     * @throws \LogicException
     */
    private function getResponse(\dodajPrenosResponse $response)
    {
        /** @var \dodajPrenosResponse $return */
        if ($return = $response->getReturn()) {

            if (!$return->getResponseResult()) {
                if ($return->getErrorMessage()) {
                    return $return->getResponseResult();
                }

                return false;
            }

            return $return->getResponseResult();
        }

        if (method_exists($return, 'getErrorMessage') && $return->getErrorMessage()) {
            throw new \LogicException($return->getErrorMessage());
        }

        return false;
    }
}
