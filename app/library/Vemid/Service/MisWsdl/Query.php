<?php

namespace Vemid\Service\MisWsdl;

use Psr\Log\LoggerInterface;

/**
 * Class Query
 * @package Vemid\Service\MisWsdl
 */
class Query extends BaseWsdl
{
    /** @var string */
    protected $query;

    /**
     * Query constructor.
     * @param string $query
     * @param LoggerInterface|null $logger
     */
    public function __construct(string $query, LoggerInterface $logger = null)
    {
        $this->query = $query;

        parent::__construct($logger);
    }

    /**
     * @throws \DomainException|\RuntimeException
     */
    public function run()
    {
        return $this->getData($this->query);
    }
}