<?php

namespace Vemid\Service\MisWsdl;

use Vemid\Task\Invoices\ImportInvoicesTask;

/**
 * Class ImportInvoices
 * @package Vemid\Service\MisWsdl
 */
class ImportInvoices extends BaseWsdl
{

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $data = $this->getData("SELECT * FROM elkarta_invoices where dat_rac >= '2021-01-01';");

        foreach ($data as $row){
            $importTask = new ImportInvoicesTask();
            $importTask->data = $row;
            $importTask->run();
        }
    }
}