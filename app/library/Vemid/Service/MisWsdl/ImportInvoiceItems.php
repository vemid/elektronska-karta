<?php

namespace Vemid\Service\MisWsdl;

use Vemid\Task\Invoices\ImportInvoiceItemsTask;

/**
 * Class ImportInvoiceItems
 * @package Vemid\Service\MisWsdl
 */
class ImportInvoiceItems extends BaseWsdl
{
    public function run()
    {
        $data = $this->getData("SELECT * FROM elkarta_invoice_items;");

        foreach ($data as $row){
            $importTask = new ImportInvoiceItemsTask();
            $importTask->data = $row;
            $importTask->execute();
        }
    }
}