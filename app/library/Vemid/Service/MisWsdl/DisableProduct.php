<?php

namespace Vemid\Service\MisWsdl;

use Psr\Log\LoggerInterface;

/**
 * Class DisableProduct
 * @package Vemid\Service\MisWsdl
 */
class DisableProduct extends BaseWsdl
{
    /** @var \Product */
    private $product;

    /**
     * DisableProduct constructor.
     * @param \Product $product
     * @param LoggerInterface|null $logger
     */
    public function __construct(\Product $product, LoggerInterface $logger = null)
    {
        $this->product = $product;

        parent::__construct($logger);
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $checkForExistingProduct = $this->getData("SELECT * FROM roba WHERE sif_rob = '{$this->product->getCode()}'");

        if (!empty($checkForExistingProduct)) {
            $procedure = sprintf("execute procedure test_setproductdeactivated('%s');", $this->product->getCode());
            $response = $this->getData($procedure);

            if (!$response['success']) {
                throw new \RuntimeException($response['message']);
            }

            $this->product->setStatus(\Product::STATUS_CANCELED);
            $this->product->save();
        }
    }
}