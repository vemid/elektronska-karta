<?php

namespace Vemid\Service\MisWsdl;

use Vemid\Task\Standardization\ImportProductionOrderTask;

/**
 * Class ImportProductionOrders
 * @package Vemid\Service\MisWsdl
 */
class ImportProductionOrders extends BaseWsdl
{
    public function run()
    {
        $data = $this->getData("SELECT * FROM productions_orders where left(product,4) in ('1231','2231','5231','3221','4221','6221');");

        foreach ($data as $row){
            $importTask = new ImportProductionOrderTask();
            $importTask->data = $row;
            $importTask->execute();
        }
    }
}