<?php

namespace Vemid\Service\MisWsdl;

use Psr\Log\LoggerInterface;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Service\Product\Hydrator;

class PutProductDeclarationSync extends BaseWsdl
{
    /** @var \Product */
    private $product;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var  string */
    private $userName;

    /**
     * PutProduct constructor.
     * @param \Product $product
     * @param EntityManagerInterface $entityManager
     * @param $userName
     * @param LoggerInterface|null $logger
     */
    public function __construct(\Product $product, EntityManagerInterface $entityManager, $userName, LoggerInterface $logger = null)
    {
        $this->product = $product;
        $this->entityManager = $entityManager;
        $this->userName = $userName;

        parent::__construct($logger);
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $declaration = str_split($this->product->getWashLabel());

        $dek1 = $declaration[0] ?? "";
        $dek2 = $declaration[1] ?? "";
        $dek3 = $declaration[2] ?? "";
        $dek4 = $declaration[3] ?? "";
        $dek5 = $declaration[4] ?? "";
        $dek6 = $declaration[5] ?? "";
        $dek7 = $declaration[6] ?? "";


        try {
            $procedure = sprintf(
                'execute procedure test_deklaracija(\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\')',
                $this->product->getCode(),
                $this->product->getComposition(),
                $dek1,
                $dek2,
                $dek3,
                $dek4,
                $dek5,
                $dek6,
                $dek7
            );

            $response = $this->getData($procedure);

            if ($response['success']) {

                $this->product->setIsSynced(true);

                if (!$this->entityManager->save($this->product)) {
                    throw new \LogicException(sprintf('Unable to set synced for product with ID: %s', $this->product->getId()));
                }
            }

        } catch (\Exception $e) {
        }
    }
}
