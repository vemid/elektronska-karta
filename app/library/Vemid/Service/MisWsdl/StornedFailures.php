<?php

namespace Vemid\Service\MisWsdl;

use Vemid\Task\Failure\StornedFailuresTask;

/**
 * Class StornedFailures
 * @package Vemid\Service\MisWsdl
 */
class StornedFailures extends BaseWsdl
{
    /**
     * {@inheritdoc}
     */
    public function run()
    {

        $data = $this->getData("SELECT oznaka from show_feleri where left(feler,2)='MP' and right(left(roba,3),2) in ('19','20','21') and length(trim(roba))=13 and storno='D'");

        foreach ($data as $row){
            $importTask = new StornedFailuresTask();
            $importTask->codeData = $row;
            $importTask->runInBackground();
        }
    }
}