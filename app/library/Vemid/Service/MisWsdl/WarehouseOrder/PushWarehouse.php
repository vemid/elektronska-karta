<?php

namespace Vemid\Service\MisWsdl\WarehouseOrder;

use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Service\Product\PricelistPrices;
use dodajPrenosCustom;

class PushWarehouse implements MisPusherInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PricelistPrices
     */
    private $productPrices;

    /** @var string|null */
    private $wsdl;

    /**
     * @param EntityManagerInterface $entityManager
     * @param PricelistPrices $productPrices
     * @param string|null $wsdl
     */
    public function __construct(EntityManagerInterface $entityManager, PricelistPrices $productPrices, string $wsdl = null)
    {
        $this->entityManager = $entityManager;
        $this->productPrices = $productPrices;
        $this->wsdl = $wsdl;
    }

    /**
     * @param \WarehouseOrder $warehouseOrder
     * @return bool
     */
    public function __invoke(\WarehouseOrder $warehouseOrder): bool
    {
        $orderMp = new \dodajNalogZaPrenos();
        $order = $orderMp->setPrenosi([$this->prepareOrders($warehouseOrder)]);

        $service = new \PrenosServisService([], $this->wsdl);
        $response = $service->dodajNalogZaPrenos($order);

        return $this->getResponse($response);
    }

    /**
     * @param \WarehouseOrder $warehouseOrder
     * @return \prenos
     */
    private function prepareOrders(\WarehouseOrder $warehouseOrder): \prenos
    {
        $channelSupplyEntity = $warehouseOrder->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();

        /** @var \Code|\Client $entity */
        $entity = $channelSupplyEntity->getEntity();
        $note = $entity->getDisplayName();

        $warehouseExit = 'KP';
        $warehouseEntry = 'XMP';

        if($warehouseOrder->getType() == "A") {
            $warehouseExit = $warehouseOrder->getExitChannelSupplyEntity()->getEntryWarehouse();
            $warehouseEntry = $channelSupplyEntity->getEntryWarehouse();
        }
        elseif($warehouseOrder->getType() == "B") {
            $warehouseExit = 'XKP';
            $warehouseEntry= 'XMP';
        }

        $order = new \prenos();
        $order->setStavke($this->prepareItems($warehouseOrder));
        $order->setVrstaKnjizenja(\vrstaKnjizenja::SamoIzlaz);
        $order->setVrstaPrenosa(\vrstaPrenosa::externiPrenos);
        $order->setNapomena("Porudzbenica za : ".$note);
        $order->setDatumDokumenta(new DateTime());
        $order->setOznakaDokumenta($warehouseOrder->getWarehouseOrderCode());
        $order->setSifraMagacinaIzlaza($warehouseExit);
        $order->setSifraMagacinaUlaza($warehouseEntry);
        $order->setStorno('N');
        $order->setLogname('elektronska.servis');

        return $order;
    }

    /**
     * @return array|\prenosStavka[]
     */
    private function prepareItems(\WarehouseOrder $warehouseOrder): array
    {
        /** @var \Pricelist $pricelist */
        $pricelist = $this->entityManager->findOne(\Pricelist::class, [
            \Pricelist::PROPERTY_CODE . ' = :priceList: ',
            'bind' => [
                'priceList' => '01/140000001'
            ]
        ]);

        $items = [];

        foreach ($warehouseOrder->getWarehouseOrderItems() as $warehouseOrderItem) {
            $osnovnaCenabezPoreza = $this->productPrices->getPurchasePrice($pricelist, $warehouseOrderItem->getProductSize()->getProduct());
            $prodajnaCenaSaPorezom = $this->productPrices->getRetailPrice($pricelist, $warehouseOrderItem->getProductSize()->getProduct());

            $item = new \prenosStavka();
            $item->setSifraRobe($warehouseOrderItem->getProductSize()->getProduct()->getCode());
            $item->setSifraObelezja($warehouseOrderItem->getProductSize()->getCode()->getCode());
            $item->setSifraObelezjaU($warehouseOrderItem->getProductSize()->getCode()->getCode());
            $item->setNabavnaCena($prodajnaCenaSaPorezom);
            $item->setKolicina($warehouseOrderItem->getQuantity());

            $items[] = $item;
        }

        return $items;
    }

    /**
     * @param \dodajNalogZaPrenosResponse $response
     * @return bool
     * @throws \LogicException
     */
    private function getResponse(\dodajNalogZaPrenosResponse $response)
    {
        /** @var \dodajNalogZaPrenosResponse $return */
        if ($return = $response->getReturn()) {

            if (!$return->getResponseResult()) {
                if ($return->getErrorMessage()) {
                    return $return->getResponseResult();
                }

                return false;
            }

            return $return->getResponseResult();
        }

        if (method_exists($return, 'getErrorMessage') && $return->getErrorMessage()) {
            throw new \LogicException($return->getErrorMessage());
        }

        return false;
    }
}
