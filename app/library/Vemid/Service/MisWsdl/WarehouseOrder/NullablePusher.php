<?php

namespace Vemid\Service\MisWsdl\WarehouseOrder;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Service\Product\PricelistPrices;

class NullablePusher implements MisPusherInterface
{
    /**
     * @param EntityManagerInterface $entityManager
     * @param PricelistPrices $productPrices
     * @param string|null $wsdl
     */
    public function __construct(EntityManagerInterface $entityManager, PricelistPrices $productPrices, string $wsdl = null){}

    /**
     * @param \WarehouseOrder $warehouseOrder
     * @return bool
     */
    public function __invoke(\WarehouseOrder $warehouseOrder): bool
    {
        return false;
    }
}
