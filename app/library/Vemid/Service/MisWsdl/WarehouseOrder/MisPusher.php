<?php

namespace Vemid\Service\MisWsdl\WarehouseOrder;

use Phalcon\Config;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Service\Product\PricelistPrices;

final class MisPusher
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PricelistPrices
     */
    private $productPrices;

    /** @var Config */
    private $config;

    /**
     * @param EntityManagerInterface $entityManager
     * @param PricelistPrices $productPrices
     * @param Config $config
     */
    public function __construct(EntityManagerInterface $entityManager, PricelistPrices $productPrices, Config $config)
    {
        $this->entityManager = $entityManager;
        $this->productPrices = $productPrices;
        $this->config = $config;
    }

    /**
     * @param \WarehouseOrder $warehouseOrder
     * @return bool
     */
    public function __invoke(\WarehouseOrder $warehouseOrder): bool
    {
        /** @var MisPusherInterface $misDocumentPusherClass */
        $misDocumentPusherClass = $this->getMisDocumentPusherType($warehouseOrder);
        $wsdl = $this->config->wsdl->{$misDocumentPusherClass};

        /** @var MisPusherInterface $misDocumentPusherService */
        $misDocumentPusherService = new $misDocumentPusherClass($this->entityManager, $this->productPrices, $wsdl);

        return $misDocumentPusherService($warehouseOrder);
    }

    /**
     * @param \WarehouseOrder $warehouseOrder
     * @return string
     */
    private function getMisDocumentPusherType(\WarehouseOrder $warehouseOrder): string
    {
        $channelSupplyEntity = $warehouseOrder->getChannelSupplyEntity();
        $type = $channelSupplyEntity->getMisDocumentType();
        $documentTypeMap = $this->getMisDocumentPusherMap();

        return $documentTypeMap[$type][$warehouseOrder->getType()] ?? ($documentTypeMap[$type] ?? NullablePusher::class);
    }

    /**
     * @return array
     */
    public function getMisDocumentPusherMap(): array
    {
        return [
            \ChannelSupplyEntity::BK_STORE => PushMpStores::class,
            \ChannelSupplyEntity::FRANCHISE => PushFranchise::class,
            \ChannelSupplyEntity::WAREHOUSE => PushWarehouse::class,
            \ChannelSupplyEntity::DM_CLIENTS => PushDomesticFranchise::class,
            \ChannelSupplyEntity::FG_CLIENTS => PushFranchise::class,
        ];
    }
}