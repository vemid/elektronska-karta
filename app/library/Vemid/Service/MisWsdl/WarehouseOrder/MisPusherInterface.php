<?php

namespace Vemid\Service\MisWsdl\WarehouseOrder;

interface MisPusherInterface
{
    /**
     * @param \WarehouseOrder $warehouseOrder
     * @return bool
     */
    public function __invoke(\WarehouseOrder $warehouseOrder): bool;
}
