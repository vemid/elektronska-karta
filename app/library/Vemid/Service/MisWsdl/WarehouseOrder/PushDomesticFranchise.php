<?php

namespace Vemid\Service\MisWsdl\WarehouseOrder;

use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Service\Product\PricelistPrices;

class PushDomesticFranchise implements MisPusherInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PricelistPrices
     */
    private $productPrices;

    /** @var string|null */
    private $wsdl;

    /**
     * @param EntityManagerInterface $entityManager
     * @param PricelistPrices $productPrices
     * @param string|null $wsdl
     */
    public function __construct(EntityManagerInterface $entityManager, PricelistPrices $productPrices, string $wsdl = null)
    {
        $this->entityManager = $entityManager;
        $this->productPrices = $productPrices;
        $this->wsdl = $wsdl;

    }

    public function __invoke(\WarehouseOrder $warehouseOrder): bool
    {
//        var_dump($this->wsdl);die;
        $orderVp = new \dodajNalogZaIzdavanje();
        $order = $orderVp->setDokumenti([$this->prepareOrder($warehouseOrder)]);

        $service = new \NalogZaIzdavanjeService([], $this->wsdl);
        $response = $service->dodajNalogZaIzdavanje($order);

        return $this->getResponse($response);
    }

    /**
     * @param \WarehouseOrder $warehouseOrder
     * @return \nalogZaIzdavanje
     */
    private function prepareOrder(\WarehouseOrder $warehouseOrder): \nalogZaIzdavanje
    {

        $channelSupplyEntity = $warehouseOrder->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();

        /** @var \Code|\Client $entity */
        $entity = $channelSupplyEntity->getEntity();
        $note = $channelSupplyEntity->getDisplayName();
        $cenovnik = '01/150000004';
        if ($supplyChannel->getType() === \ChannelSupply::VP) {
            $cenovnik = '01/140000002';

        }

        if($warehouseOrder->getType() == "A") {
            $warehouseExit = 'KP';
        }
        elseif($warehouseOrder->getType() == "B") {
            $warehouseExit = 'XKP';
        }

        $order = new \nalogZaIzdavanje(0,0,0);
        $order->setBrojRata(0);
        $order->setBrojRata(0);
        $order->setCenaPrevoza(0);
        $order->setGratisPer(0);
        $order->setDatumDokumenta(new DateTime());
        $order->setDatumOtpreme(new DateTime());
        $order->setDpo(new DateTime());
        $order->setLogname('elektronska.servis');
        $order->setMarza(0);
        $order->setOznakaCenovnika($cenovnik);
        $order->setOznakaDokumenta($warehouseOrder->getWarehouseOrderCode());
        $order->setRealizovano("N");
        $order->setRokIsporukeUDanima(7);
        $order->setSifraMagacina($warehouseExit);
        $order->setSifraNacinaPlacanja(1);
        $order->setSifraOrganizacioneJednice("013");
        $order->setSifraOrganizacioneJedniceNarucioca("013");
        $order->setStatus("0");
        $order->setKreirajAhp("0");
        $order->setIznosKasaSkonto("0");
        $order->setIznosManipulativnihTroskova("0");
        $order->setKasaSkonto("0");
        $order->setStopaManipulativnihTroskova("0");
        $order->setStorno('N');
        $order->setValutaPlacanja(new DateTime());
        $order->setVrstaFakturisanja(1);
        $order->setVrstaIzjave(2);
        $order->setSifraRacuna('RB');
        $order->setTipNaloga(0);
        $order->setTipNaloga(0);
        $order->setSifraPartnera($channelSupplyEntity->getClientCode());
        $order->setSifraPartneraKorisnika($channelSupplyEntity->getClientCode());

        $order->setStavke($this->prepareOrderItems($warehouseOrder));

        return $order;
    }

    private function prepareOrderItems(\WarehouseOrder $warehouseOrder): array
    {
        $channelSupplyEntity = $warehouseOrder->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();
        $cenovnik = '01/140000001';
        if ($supplyChannel->getType() === \ChannelSupply::VP) {
            $cenovnik = '01/140000002';
        }

        /** @var \Pricelist $pricelist */
        $pricelist = $this->entityManager->findOne(\Pricelist::class, [
            \Pricelist::PROPERTY_CODE . ' = :priceList: ',
            'bind' => [
                'priceList' => $cenovnik
            ]
        ]);

        $items = [];
        foreach ($warehouseOrder->getWarehouseOrderItems() as $warehouseOrderItem) {
            $osnovnaCenabezPoreza = 0;
            if ($supplyChannel->getType() === \ChannelSupply::VP) {
                $osnovnaCenabezPoreza = $this->productPrices->getWholesalePrice($pricelist, $warehouseOrderItem->getProductSize()->getProduct());
            }
            else {
                $osnovnaCenabezPoreza = round($this->productPrices->getRetailPrice($pricelist, $warehouseOrderItem->getProductSize()->getProduct())* 0.833333,3);
            }
            $item = new \nalogZaIzdavanjeStavka();
            $item->setAkcijskaStopaRabata(0);
            $item->setBrojPakovanja(0);
            $item->setBruto($warehouseOrderItem->getQuantity());
            $item->setDodatniRabat(0);
            $item->setIznosAkcize(0);
            $item->setIznosManipulativnihTroskova(0);
            $item->setIznosTakse(0);
            $item->setPosebnaStopaRabata(0);
            $item->setRabatBezAkciza(0);
            $item->setKolicina($warehouseOrderItem->getQuantity());
            $item->setCenaSaRabatom($osnovnaCenabezPoreza);
            $item->setOsnovnaCena($osnovnaCenabezPoreza);
            $item->setDeviznaCena($osnovnaCenabezPoreza);
            $item->setProdajnaCena(round($osnovnaCenabezPoreza*1.2,0));
            $item->setSifraRobe($warehouseOrderItem->getProductSize()->getProduct()->getCode());
            $item->setSifraObelezja($warehouseOrderItem->getProductSize()->getCode()->getName());
            $item->setStopaManipulativnihTroskova(0);
            $item->setStopaPoreza('20');
            $item->setStopaRabata('0');
            $item->setZahtevanaKolicina($warehouseOrderItem->getQuantity());


            $items[] = $item;
        }

        return $items;
    }

    /**
     * @param \dodajNalogZaIzdavanjeResponse $response
     * @return bool
     * @throws \LogicException
     */
    private function getResponse(\dodajNalogZaIzdavanjeResponse $response): bool
    {
        /** @var \nalogZaIzdavanjeResponse $return */
        if ($return = $response->getReturn()) {

            if (!$return->getResponseResult()) {
                if ($return->getErrorMessage()) {
                    return $return->getResponseResult();
                }

                return false;
            }

            return $return->getResponseResult();
        }

        if (method_exists($return, 'getErrorMessage') && $return->getErrorMessage()) {
            throw new \LogicException($return->getErrorMessage());
        }

        return false;
    }
}