<?php

namespace Vemid\Service\MisWsdl\WarehouseOrder;

use Psr\Log\LoggerInterface;
use Vemid\Service\MisWsdl\BaseWsdl;


class WarehouseOrderCancellation extends BaseWsdl
{
    /** @var \WarehouseOrder */
    private $warehouseOrder;

    /**
     * @param \WarehouseOrder $warehouseOrder
     * @param LoggerInterface|null $logger
     */
    public function __construct(\WarehouseOrder $warehouseOrder, LoggerInterface $logger = null)
    {
        $this->warehouseOrder = $warehouseOrder;

        parent::__construct($logger);
    }

    public function run()
    {
        $procedure = sprintf(
            "execute procedure elk_provera_dokumenta('%s', '%s');",
            $this->warehouseOrder->getWarehouseOrderCode(),
            $this->warehouseOrder->getChannelSupplyEntity()->getMisDocumentType()
        );

        $response = $this->getData($procedure);

        if (!$response['success']) {
            throw new \RuntimeException($response['message']);
        }

        return $response;
    }
}
