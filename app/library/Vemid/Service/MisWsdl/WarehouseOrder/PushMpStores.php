<?php

namespace Vemid\Service\MisWsdl\WarehouseOrder;

use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Service\Product\PricelistPrices;

class PushMpStores implements MisPusherInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PricelistPrices
     */
    private $productPrices;

    /** @var string|null */
    private $wsdl;

    /**
     * @param EntityManagerInterface $entityManager
     * @param PricelistPrices $productPrices
     * @param string|null $wsdl
     */
    public function __construct(EntityManagerInterface $entityManager, PricelistPrices $productPrices, string $wsdl = null)
    {
        $this->entityManager = $entityManager;
        $this->productPrices = $productPrices;
        $this->wsdl = $wsdl;
    }

    /**
     * @param \WarehouseOrder $warehouseOrder
     * @return bool
     */
    public function __invoke(\WarehouseOrder $warehouseOrder): bool
    {
        $orderMp = new \dodajNalogZaIzdavanjeMp();
        $order = $orderMp->setNalozi([$this->prepareOrders($warehouseOrder)]);

        $service = new \NalogZaIzdavanjeMpServisService([], $this->wsdl);
        $response = $service->dodajNalogZaIzdavanjeMp($order);

        return $this->getResponse($response);
    }

    /**
     * @param \WarehouseOrder $warehouseOrder
     * @return \nalogZaIzdavanjeMp
     */
    private function prepareOrders(\WarehouseOrder $warehouseOrder): \nalogZaIzdavanjeMp
    {
        $channelSupplyEntity = $warehouseOrder->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();

        /** @var \Code|\Client $entity */
        $entity = $channelSupplyEntity->getEntity();
        $note = $channelSupplyEntity->getDisplayName();

        $postalAddress = $channelSupplyEntity->getPostalAddress();
        if ($supplyChannel->getType() === \ChannelSupply::VP) {
            $postalAddress = $entity->getAddress();
            $note = $entity->getDisplayName();
        }

        $order = new \nalogZaIzdavanjeMp();
        $order->setStorno('N');
        $order->setOznakaCenovnika('01/140000001');
        $order->setNapomena('Porudzbina za: '.$note);
        $order->setLogname('elektronska.servis');
        $order->setStavke($this->prepareItems($warehouseOrder));
        $order->setStatus(0);
        $order->setAdresaIsporuke($postalAddress);
        $order->setDatumDokumenta(new DateTime());
        $order->setOznakaDokumenta($warehouseOrder->getWarehouseOrderCode());
        $order->setOznakaKase(1);
        $order->setSifraMagacina('KP');
        $order->setSifraObjekta($entity->getCode());
        $order->setSifraOrganizacioneJedinice('013');
        $order->setDpo(new \DateTime());

        return $order;
    }

    /**
     * @return array|\nalogZaIzdavanjeMpStavka[]
     */
    private function prepareItems(\WarehouseOrder $warehouseOrder): array
    {
        /** @var \Pricelist $pricelist */
        $pricelist = $this->entityManager->findOne(\Pricelist::class, [
            \Pricelist::PROPERTY_CODE . ' = :priceList: ',
            'bind' => [
                'priceList' => '01/140000001'
            ]
        ]);

        $items = [];
        $counter = 1;
        foreach ($warehouseOrder->getWarehouseOrderItems() as $warehouseOrderItem) {
            $osnovnaCenabezPoreza = $this->productPrices->getPurchasePrice($pricelist, $warehouseOrderItem->getProductSize()->getProduct());
            $prodajnaCenaSaPorezom = $this->productPrices->getRetailPrice($pricelist, $warehouseOrderItem->getProductSize()->getProduct());

            $item = new \nalogZaIzdavanjeMpStavka();
            $item->setKolicina($warehouseOrderItem->getQuantity());
            $item->setOsnovnaCenabezPoreza($osnovnaCenabezPoreza);
            $item->setProdajnaCenaSaPopustom($prodajnaCenaSaPorezom);
            $item->setProdajnaCenaSaPorezom($prodajnaCenaSaPorezom);
            $item->setRedniBroj($counter);
            $item->setSifraOblezja($warehouseOrderItem->getProductSize()->getCode()->getName());
            $item->setSifraRobe($warehouseOrderItem->getProductSize()->getProduct()->getCode());
            $item->setSifraTarifneGrupe('100');
            $item->setStopaPopusta(0);
            $item->setStopaPoreza(20);

            $counter++;
            $items[] = $item;
        }

        return $items;
    }

    /**
     * @param \dodajNalogZaIzdavanjeMpResponse $response
     * @return bool
     * @throws \LogicException
     */
    private function getResponse(\dodajNalogZaIzdavanjeMpResponse $response)
    {
        /** @var \nalogZaIzdavanjeMpResponse $return */
        if ($return = $response->getReturn()) {

            if (!$return->getResponseResult()) {
                if ($return->getErrorMessage()) {
                    return $return->getResponseResult();
                }

                return false;
            }

            return $return->getResponseResult();
        }

        if (method_exists($return, 'getErrorMessage') && $return->getErrorMessage()) {
            throw new \LogicException($return->getErrorMessage());
        }

        return false;
    }
}
