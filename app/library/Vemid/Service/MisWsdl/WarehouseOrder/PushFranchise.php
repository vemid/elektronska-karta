<?php

namespace Vemid\Service\MisWsdl\WarehouseOrder;

use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Service\Product\PricelistPrices;

class PushFranchise implements MisPusherInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PricelistPrices
     */
    private $productPrices;

    /** @var string|null */
    private $wsdl;

    /**
     * @param EntityManagerInterface $entityManager
     * @param PricelistPrices $productPrices
     * @param string|null $wsdl
     */
    public function __construct(EntityManagerInterface $entityManager, PricelistPrices $productPrices, string $wsdl = null)
    {
        $this->entityManager = $entityManager;
        $this->productPrices = $productPrices;
        $this->wsdl = $wsdl;

    }

    public function __invoke(\WarehouseOrder $warehouseOrder): bool
    {
//        var_dump($this->wsdl);die;
        $orderVp = new \dodajNarudzbenicuKupca();
        $order = $orderVp->setNarudzbenice([$this->prepareOrder($warehouseOrder)]);

        $service = new \NarudzbenicaKupcaServisService([], $this->wsdl);
        $response = $service->dodajNarudzbenicuKupca($order);

        return $this->getResponse($response);
    }

    /**
     * @param \WarehouseOrder $warehouseOrder
     * @return \narudzbenicaKupca
     */
    private function prepareOrder(\WarehouseOrder $warehouseOrder): \narudzbenicaKupca
    {

        $channelSupplyEntity = $warehouseOrder->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();

        /** @var \Code|\Client $entity */
        $entity = $channelSupplyEntity->getEntity();
        $note = $channelSupplyEntity->getDisplayName();

        if ($supplyChannel->getType() === \ChannelSupply::VP) {
            $note = $entity->getDisplayName();
        }
//        else {
//            if($entity->getDealership()) {
//                /** @var \Dealership $dealerShip */
//                $dealerShip = $entity->getDealership();
//                $partnerCode = $dealerShip[0]->getClientCode();
//            }
//        }

        $order = new \narudzbenicaKupca();
        $order->setDatumNaurdzbenice(new DateTime());
        $order->setDeviznaNarudzbenica(0);
        $order->setLogname('elektronska.servis');
        $order->setNapomena('Narudzbina za :'.$note);
        $order->setOrganizacionaJedinica('013');
        $order->setOznakaCenovnika('01/140000001');
        $order->setOznakaNarudzbenice($warehouseOrder->getWarehouseOrderCode());
        $order->setOznakaNarudzbenicePartnera($warehouseOrder->getType().$warehouseOrder->getWarehouseOrderCode());
        $order->setPartnerKorisnik($channelSupplyEntity->getClientCode());
        $order->setSifraPartnera($channelSupplyEntity->getClientCode());
        $order->setRealizovano(0);
        $order->setSifraNacinaPlacanja('1');
        $order->setStavke($this->prepareOrderItems($warehouseOrder));
        $order->setStorno('N');

        return $order;
    }

    private function prepareOrderItems(\WarehouseOrder $warehouseOrder): array
    {
        /** @var \Pricelist $pricelist */
        $pricelist = $this->entityManager->findOne(\Pricelist::class, [
            \Pricelist::PROPERTY_CODE . ' = :priceList: ',
            'bind' => [
                'priceList' => '01/140000001'
            ]
        ]);

        $items = [];
        foreach ($warehouseOrder->getWarehouseOrderItems() as $warehouseOrderItem) {
            $osnovnaCenabezPoreza = $this->productPrices->getPurchasePrice($pricelist, $warehouseOrderItem->getProductSize()->getProduct());
            $prodajnaCenaSaPorezom = $this->productPrices->getRetailPrice($pricelist, $warehouseOrderItem->getProductSize()->getProduct());

            $item = new \narudzbenicaKupcaStavka();
            $item->setAkcijskaStopaRabata(0);
            $item->setCenaSaPorezom($prodajnaCenaSaPorezom);
            $item->setDodatniRabat('0');
            $item->setKolicina($warehouseOrderItem->getQuantity());
            $item->setOsnovnaCenaSaRabatom($osnovnaCenabezPoreza);
            $item->setOsnovnaCenaBezporeza($osnovnaCenabezPoreza);
            $item->setOsnovnavrednostSaSaRabatom($osnovnaCenabezPoreza);
            $item->setSifraRobe($warehouseOrderItem->getProductSize()->getProduct()->getCode());
            $item->setSifraObelezja($warehouseOrderItem->getProductSize()->getCode()->getName());
            $item->setStopaPoreza('20');
            $item->setStopaRabata('0');
            $item->setRokIsporuke((new DateTime())->modify("+1 year"));
            $item->setVrednostSaPorezom($prodajnaCenaSaPorezom);

            $items[] = $item;
        }

        return $items;
    }

    /**
     * @param \dodajNarudzbenicuKupcaResponse $response
     * @return bool
     * @throws \LogicException
     */
    private function getResponse(\dodajNarudzbenicuKupcaResponse $response): bool
    {
        /** @var \narudzbenicaKupcaResponse $return */
        if ($return = $response->getReturn()) {

            if (!$return->getResponseResult()) {
                if ($return->getErrorMessage()) {
                    return $return->getResponseResult();
                }

                return false;
            }

            return $return->getResponseResult();
        }

        if (method_exists($return, 'getErrorMessage') && $return->getErrorMessage()) {
            throw new \LogicException($return->getErrorMessage());
        }

        return false;
    }
}