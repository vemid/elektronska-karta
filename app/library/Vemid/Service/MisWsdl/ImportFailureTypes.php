<?php

namespace Vemid\Service\MisWsdl;

use Vemid\Task\Failure\ImportFailureTypesTask;

/**
 * Class ImportFailureTypes
 * @package Vemid\Service\MisWsdl
 */
class ImportFailureTypes extends BaseWsdl
{
    /**
     * {@inheritdoc}
     */
    public function run()
    {

        $data2 = $this->getData("SELECT sif_raz_pov,naz_raz_pov FROM raz_pov_rob where left(naz_raz_pov,2)='MP'");

        foreach ($data2 as $row2){
            $importTask2 = new ImportFailureTypesTask();
            $importTask2->codeData = $row2;
            $importTask2->run();
        }
    }
}