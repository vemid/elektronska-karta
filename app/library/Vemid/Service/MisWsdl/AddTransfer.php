<?php

namespace Vemid\Service\MisWsdl;
use Vemid\Entity\Manager\EntityManagerInterface;

/**
 * Class AddTransfer
 * @package Vemid\Service\MisWsdl
 */
class AddTransfer
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var \DeliveryNote
     */
    protected $deliveryNote;

    /**
     * AddTransfer constructor.
     * @param EntityManagerInterface $entityManager
     * @param \DeliveryNote $deliveryNote
     */
    public function __construct(EntityManagerInterface $entityManager, \DeliveryNote $deliveryNote)
    {
        $this->entityManager = $entityManager;
        $this->deliveryNote = $deliveryNote;
    }

    /**
     * @return bool
     */
    public function transfer()
    {
        $wsdl = 'http://192.168.100.210:8080/ServisMisWebTest/services/NalogZaIzdavanjeMpServisPort?wsdl';

        $transferService = new \PrenosServisService($wsdl);
        $addTransfer = new \dodajPrenos();
        $addTransfer->setPrenosi((array)$this->prepareTransfer());
        $response = $transferService->dodajPrenos($addTransfer);

        return $this->getResponse($response);
    }

    /**
     * @return \prenos
     */
    protected function prepareTransfer()
    {
        $transfer = new \prenos();
        $transfer->setStavke($this->prepareItem());
        $transfer->setVrstaKnjizenja(\vrstaKnjizenja::SamoIzlaz);
        $transfer->setVrstaPrenosa(\vrstaPrenosa::externiPrenos);
        $transfer->setNapomena($this->deliveryNote->getMisDeliveryNote());
        $transfer->setDatumDokumenta($this->deliveryNote->getCreated());
        $transfer->setOznakaDokumenta("ELK/".$this->deliveryNote->getId());
        $transfer->setSifraMagacinaIzlaza('MP');
        $transfer->setSifraMagacinaUlaza('VP');
        $transfer->setStorno('N');
        $transfer->setLogname('mis');

        return $transfer;
    }

    /**
     * @return array
     */
    protected function prepareItem()
    {
        $items = [];
        /** @var \Vemid\Entity\Repository\DeliveryNoteItemRepository $deliveryNoteItems */
        $deliveryNoteItems = $this->entityManager->getRepository(\DeliveryNoteItem::class);

        foreach ($deliveryNoteItems->getSumItems($this->deliveryNote) as $deliveryNoteItem) {
            $item = new \prenosStavka();
            $item->setSifraRobe($deliveryNoteItem['code']);
            $item->setSifraObelezja($deliveryNoteItem['size']);
            $item->setSifraObelezjaU($deliveryNoteItem['size']);
            $item->setNabavnaCena($deliveryNoteItem['purchasePrice']);
            $item->setKolicina($deliveryNoteItem['quantity']);

            $items[] = (array)$item;
        }

        return $items;
    }

    /**
     * @param \dodajPrenosResponse $response
     * @return bool
     * @throws \LogicException
     */
    private function getResponse(\dodajPrenosResponse $response)
    {
        /** @var \prenosResponse $return */
        if ($return = $response->getReturn()) {

            if (!$return->getResponseResult()) {
                if ($return->getErrorMessage()) {
                    return $return->getErrorMessage();
                }

                return false;
            }

            return $return->getResponseResult();
        }

        if (method_exists($return, 'getErrorMessage') && $return->getErrorMessage()) {
            throw new \LogicException($return->getErrorMessage());
        }

        return false;
    }
}
