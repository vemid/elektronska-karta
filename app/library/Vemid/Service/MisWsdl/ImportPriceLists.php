<?php

namespace Vemid\Service\MisWsdl;

use Vemid\Task\Calculations\ImportPriceListTask;

/**
 * Class ImportPriceLists
 * @package Vemid\Service\MisWsdl
 */
class ImportPriceLists extends BaseWsdl
{
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $data = $this->getData("SELECT * FROM elkarta_cenovnici;");

        foreach ($data as $row){
            $importTask = new ImportPriceListTask();
            $importTask->priceListData = $row;
            $importTask->runInBackground();
        }
    }
}