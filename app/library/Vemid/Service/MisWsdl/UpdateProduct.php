<?php

namespace Vemid\Service\MisWsdl;

use Psr\Log\LoggerInterface;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Service\Product\Hydrator;

class UpdateProduct extends BaseWsdl
{
    /** @var \Product */
    private $product;

    /** @var EntityManagerInterface  */
    private $entityManager;

    /** @var  string */
    private $userName;

    /**
     * UpdateProduct constructor.
     * @param \Product $product
     * @param EntityManagerInterface $entityManager
     * @param $userName
     * @param LoggerInterface|null $logger
     */
    public function __construct(\Product $product, EntityManagerInterface $entityManager, $userName, LoggerInterface $logger = null)
    {
        $this->product = $product;
        $this->entityManager = $entityManager;
        $this->userName = $userName;

        parent::__construct($logger);
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $checkForExistingProduct = $this->getData("SELECT * FROM roba WHERE sif_rob = '{$this->product->getCode()}'");

        if (!empty($checkForExistingProduct)) {
            $product = new Hydrator($this->product, $this->entityManager);
            $procedure = sprintf(
                'execute procedure test_unos_sifre(\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\')',
                $product->getCode(),
                $product->getCode(),
                $product->getProductCodeType() ? $product->getProductCodeType()->getCode() : '',
                $product->getColor() ? $product->getColor()->getCode() : '',
                $product->getName(),
                $this->userName,
                $product->getWebName()
            );

            $response = $this->getData($procedure);

            if (!$response['success']) {
                throw new \RuntimeException($response['message']);
            }

            if ($parentProduct = $this->product->getParentProduct()) {
                $procedure = sprintf(
                    'execute procedure test_unos_sifre(\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\')',
                    $product->getCode(),
                    $parentProduct->getCode(),
                    $product->getProductCodeType() ? $product->getProductCodeType()->getCode() : '',
                    $product->getColor() ? $product->getColor()->getCode() : '',
                    $product->getName(),
                    $this->userName,
                    $product->getWebName()
                );

                $response = $this->getData($procedure);

                if (!$response['success']) {
                    throw new \RuntimeException($response['message']);
                }
            }

            $productClassifications = $this->product->getProductClassifications([
                \ProductClassification::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:',
                'bind' => [
                    'entityTypeId' => Type::CLASSIFICATION
                ]
            ]);

            foreach ($productClassifications as $productClassification) {

                /** @var \Classification $classification */
                if (!$classification = $productClassification->getProductClassification()) {
                    continue;
                }

                if (!$code = $classification->getClassificationType()) {
                    continue;
                }

                if ($code->getCode() === '00') {
                    continue;
                }

                $classificationProcedure = sprintf(
                    'execute procedure test_import_class_robe(\'%s\', \'%s\', \'%s\')',
                    $this->product->getCode(),
                    $classification->getCode(),
                    $code->getCode()
                );

                $response = $this->getData($classificationProcedure);

                if (!$response['success']) {
                    throw new \RuntimeException($response['message']);
                }
            }

            $productAttributes = $this->product->getProductClassifications([
                \ProductClassification::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:',
                'bind' => [
                    'entityTypeId' => Type::ATTRIBUTE
                ]
            ]);

            $webClassificationProcedure = sprintf(
                'execute procedure test_import_class_robe_web(\'%s\')',
                $product->getCode()
            );

            $response = $this->getData($webClassificationProcedure);

            if (!$response['success']) {
                throw new \RuntimeException($response['message']);
            }

            /** @var \Priority $priority */
            $priority = $this->product->getPriority();

            $entryAttributes = sprintf(
                'execute procedure test_import_entry_attribute(\'%s\',\'%s\',\'%s\',\'%s\');',
                $this->product->getCode(),
                $priority->getPriorityNumber(). "_" .$priority->getPriorityDate(),
                $priority->getName(),
                $this->userName
            );

            $response = $this->getData($entryAttributes);

            if (!$response['success']) {
                throw new \RuntimeException($response['message']);
            }

            $genderProcedure = sprintf(
                'execute procedure test_marko_insert_pol(\'%s\')',
                $product->getCode()
            );

            $response = $this->getData($genderProcedure);

            if (!$response['success']) {
                throw new \RuntimeException($response['message']);
            }


            foreach ($productAttributes as $productAttribute) {
                /** @var \Attribute $attribute */
                if (!$attribute = $productAttribute->getProductClassification()) {
                    continue;
                }

                if (!$code = $attribute->getAttributeCode()) {
                    continue;
                }

                $attributeProcedure = sprintf(
                    'execute procedure test_import_attributes(\'%s\',\'%s\',\'%s\',\'%s\');',
                    $this->product->getCode(),
                    $code->getCode(),
                    $attribute->getName(),
                    $this->userName
                );

                $response = $this->getData($attributeProcedure);

                if (!$response['success']) {
                    throw new \RuntimeException($response['message']);
                }
            }

            foreach ($this->product->getProductSizes() as $productSize) {
                $code = $productSize->getCode();

                if ($code && $productSize->getBarcode()) {
                    $procedure = sprintf(
                        'execute procedure test_import_barkod(\'%s\', \'%s\', \'%s\')',
                        $product->getCode(),
                        $productSize->getBarcode(),
                        $code->getCode()
                    );

                    $response = $this->getData($procedure);

                    if (!$response['success']) {
                        throw new \RuntimeException($response['message']);
                    }
                }
            }
        }
    }
}
