<?php

namespace Vemid\Service\MisWsdl;

use Psr\Log\LoggerInterface;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Service\Product\Hydrator;

class PutProductSync extends BaseWsdl
{
    /** @var \Product */
    private $product;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var  string */
    private $userName;

    /**
     * PutProduct constructor.
     * @param \Product $product
     * @param EntityManagerInterface $entityManager
     * @param $userName
     * @param LoggerInterface|null $logger
     */
    public function __construct(\Product $product, EntityManagerInterface $entityManager, $userName, LoggerInterface $logger = null)
    {
        $this->product = $product;
        $this->entityManager = $entityManager;
        $this->userName = $userName;

        parent::__construct($logger);
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        try {
            $checkForExistingProduct = $this->getData("SELECT sif_rob FROM roba WHERE sif_rob = '{$this->product->getCode()}'");

            if (empty($checkForExistingProduct)) {
                $product = new Hydrator($this->product, $this->entityManager);
                $procedure = sprintf(
                    'execute procedure test_unos_sifre(\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\')',
                    $product->getCode(),
                    $this->product->getParentProduct() ? ($this->product->getParentProduct())->getCode() : $product->getCode(),
                    $product->getProductCodeType() ? $product->getProductCodeType()->getCode() : '',
                    $product->getColor() ? $product->getColor()->getCode() : '',
                    $product->getName(),
                    $this->userName,
                    $product->getWebName()
                );

                $response = $this->getData($procedure);

                if ($response['success']) {

                    $this->product->setIsSynced(true);

                    if (!$this->entityManager->save($this->product)) {
                        throw new \LogicException(sprintf('Unable to set synced for product with ID: %s', $this->product->getId()));
                    }
                }


            } else if (isset($checkForExistingProduct['sif_rob'])) {
                $this->product->setIsSynced(true);

                if (!$this->entityManager->save($this->product)) {
                    throw new \LogicException(sprintf('Unable to set synced for product with ID: %s', $this->product->getId()));
                }
            }
        } catch (\Exception $e) {}
    }
}
