<?php

namespace Vemid\Service\MisWsdl;


use Psr\Log\LoggerInterface;
use Vemid\Entity\Manager\EntityManagerInterface;

class PutProductClassificationAttributeSync extends BaseWsdl
{
    /** @var \ProductClassification */
    private $productClassification;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var null|string */
    private $username;

    /**
     * PutProductClassificationAttributeSync constructor.
     * @param EntityManagerInterface $entityManager
     * @param \ProductClassification $productClassification
     * @param LoggerInterface|null $logger
     * @param null|string $userName
     */
    public function __construct(EntityManagerInterface $entityManager, \ProductClassification $productClassification, $userName = null, LoggerInterface $logger = null)
    {
        $this->entityManager = $entityManager;
        $this->productClassification = $productClassification;
        $this->username = $userName;

        parent::__construct($logger);
    }

    public function run()
    {
        $attribute = $this->productClassification->getProductClassification();
        $code = $attribute->getAttributeCode();

        $attributeQuery = sprintf(
            'SELECT sif_rob from roba_param_par WHERE sif_rob = \'%s\' AND ozn_par_par = \'%s\' AND vre_par = \'%s\'',
            $this->productClassification->getProduct()->getCode(),
            $code->getCode(),
            $attribute->getName()
        );

        try {
            $attributeResponse = $this->getData($attributeQuery);

            if (!empty($attributeResponse)) {
                $this->productClassification->setIsSynced(true);

                if (!$this->entityManager->save($this->productClassification)) {
                    throw new \LogicException(sprintf('Unable to set synced to product attribute with ID: %s', $this->productClassification->getId()));
                }

                return;
            }
        } catch (\Exception $e) {}

        $attributeProcedure = sprintf(
            'execute procedure test_import_attributes(\'%s\',\'%s\',\'%s\',\'%s\');',
            $this->productClassification->getProduct()->getCode(),
            $code->getCode(),
            $attribute->getName(),
            $this->username
        );

        try {
            $response = $this->getData($attributeProcedure);

            if (!$response['success']) {
                throw new \LogicException(sprintf('Error on Service - %s', $response['error']));
            }

            $this->productClassification->setIsSynced(true);

            if (!$this->entityManager->save($this->productClassification)) {
                throw new \LogicException(sprintf('Unable to set synced to product attribute with ID: %s', $this->productClassification->getId()));
            }
        } catch (\Exception $e) {}

    }
}
