<?php

namespace Vemid\Service\MisWsdl;

use Psr\Log\LoggerInterface;

/**
 * Class DisableProduct
 * @package Vemid\Service\MisWsdl
 */
class DisableProductAfterUpdate extends BaseWsdl
{
    /** @var string */
    private $code;

    /**
     * DisableProduct constructor.
     * @param string $code
     * @param LoggerInterface|null $logger
     */
    public function __construct(string $code, LoggerInterface $logger = null)
    {
        $this->code = $code;

        parent::__construct($logger);
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $checkForExistingProduct = $this->getData("SELECT * FROM roba WHERE sif_rob = '{$this->code}'");

        if (!empty($checkForExistingProduct)) {
            $procedure = sprintf("execute procedure test_setproductdeactivated('%s');", $this->code);
            $response = $this->getData($procedure);

            if (!$response['success']) {
                throw new \RuntimeException($response['message']);
            }
        }
    }
}