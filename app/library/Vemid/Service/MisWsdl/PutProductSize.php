<?php

namespace Vemid\Service\MisWsdl;

use Psr\Log\LoggerInterface;

/**
 * Class PutProductSize
 * @package Vemid\Service\MisWsdl
 */
class PutProductSize extends BaseWsdl
{
    /** @var \ProductSize */
    private $productSize;

    /**
     * PutProductSize constructor.
     * @param \ProductSize $productSize
     * @param LoggerInterface|null $logger
     */
    public function __construct(\ProductSize $productSize, LoggerInterface $logger = null)
    {
        $this->productSize = $productSize;

        parent::__construct($logger);
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $product = $this->productSize->getProduct();
        $code = $this->productSize->getCode();

        if ($product && $code) {
            $procedure = sprintf(
                'execute procedure test_import_barkod(\'%s\', \'%s\', \'%s\')',
                $product->getCode(),
                $this->productSize->getBarcode(),
                $code->getCode()
            );

            $response = $this->getData($procedure);

            if (!$response['success']) {
                throw new \RuntimeException($response['message']);
            }
        }

    }
}