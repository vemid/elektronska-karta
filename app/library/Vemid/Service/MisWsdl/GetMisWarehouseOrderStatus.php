<?php

namespace Vemid\Service\MisWsdl;

use Vemid\Date\DateTime;
use Psr\Log\LoggerInterface;
use \Vemid\Task\WarehouseOrders\CheckMisWarehouseOrderGroupStatusTask;
use Vemid\Entity\Manager\EntityManagerInterface;

/**
 * Class GetMisWarehouseOrderStatus
 * @package Vemid\Service\MisWsdl
 */
class GetMisWarehouseOrderStatus extends BaseWsdl
{
    /** @var EntityManagerInterface */
    private $entityManager;


    /**
     * ImportWarrants constructor.
     * @param EntityManagerInterface $entityManager

     */
    public function __construct(EntityManagerInterface $entityManager,LoggerInterface $logger = null)
    {
        $this->entityManager = $entityManager;

        parent::__construct($logger);
    }

    /**
     * @throws \DomainException|\RuntimeException
     */
    public function run()
    {
        /** @var \CodeType $codeType */
        $warehouseOrders = $this->entityManager->find(\WarehouseOrderGroup::class, [
            \WarehouseOrderGroup::PROPERTY_GROUPED . ' = :status:',
            'bind' => [
                'status' => \WarehouseOrderGroup::NON_GROUPED
            ]
        ]);


        $data = [];
        /** @var \WarehouseOrderGroup $item */
        foreach ($warehouseOrders as $item) {

            $data[] = $item->getWarehouseOrderGroupCode();
        }

        $data = implode("', '", $data);

        $sql = <<<SQL
SELECT trim(oznaka) oznaka,trim(status) status,trim(storno) storno FROM elkarta_warehouse_order_group_status WHERE oznaka IN ('$data')
SQL;

        $response = $this->getData($sql);

        foreach ($response as $row){
            $checkTask = new CheckMisWarehouseOrderGroupStatusTask();
            $checkTask->row = $row;
            $checkTask->runInBackground();
        }
    }
}