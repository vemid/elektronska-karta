<?php

namespace Vemid\Service\MisWsdl;

use Psr\Log\LoggerInterface;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Service\Product\Hydrator;

class PutProductGender extends BaseWsdl
{
    /** @var \Product */
    private $product;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var  string */
    private $userName;

    /**
     * PutProduct constructor.
     * @param \Product $product
     * @param EntityManagerInterface $entityManager
     * @param $userName
     * @param LoggerInterface|null $logger
     */
    public function __construct(\Product $product, EntityManagerInterface $entityManager, $userName, LoggerInterface $logger = null)
    {
        $this->product = $product;
        $this->entityManager = $entityManager;
        $this->userName = $userName;

        parent::__construct($logger);
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        try {
            $checkForExistingProduct = $this->getData("SELECT sif_rob FROM roba_klas_robe WHERE sif_kri_kla='11' and sif_rob = '{$this->product->getCode()}'");

            if (empty($checkForExistingProduct)) {
                $product = new Hydrator($this->product, $this->entityManager);
                $webClassificationProcedure = sprintf(
                    'execute procedure test_import_class_robe_pol(\'%s\')',
                    $product->getCode()
                );

                $this->getData($webClassificationProcedure);


            }
        } catch (\Exception $e) {}
    }
}
