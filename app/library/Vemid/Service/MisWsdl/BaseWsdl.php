<?php

namespace Vemid\Service\MisWsdl;

use Psr\Log\LoggerInterface;
use Vemid\Application\Di;
use Vemid\Service\WebServis\WebServisStatus;
use Vemid\Task\AbstractTask;
use \Wsdl2PhpGenerator\Generator;
use \Wsdl2PhpGenerator\Config;

/**
 * Class BaseWsdl
 * @package Vemid\Service\MisWsdl
 */
abstract class BaseWsdl
{
    /**
     * BaseWsdl constructor.
     * @param LoggerInterface|null $logger
     */
    public function __construct(LoggerInterface $logger = null)
    {
    }

    /**
     * @throws \DomainException|\RuntimeException
     */
    abstract public function run();

    /**
     * @param string $sql
     * @return array
     */
    protected function getData($sql)
    {
            /** @var Di $di */
            $di = Di::getDefault();
            $url = $di->getConfig()['wsdl']->baseWsdl;

            $sqlService = new \CustomSQLServis([], $url);
            $sqlData = new \getSQLData($sql);
            $sqlData->setSql($sql);
            $response = $sqlService->getSQLData($sqlData);

            return $this->getRows($response->getReturn(), $sql);
    }



    /**
     * @param \customSQLServisResponse $response
     * @param string $sql
     * @return array
     */
    private function getRows(\customSQLServisResponse $response,string $sql)
    {
        if ($xmlString = $response->getXmlString()) {
            $xml = new \SimpleXMLElement($xmlString);
            $data = json_decode(json_encode($xml), true);

            if (array_key_exists('Row', $data)) {
                if (stripos($sql, 'select') !== false && !isset($data['Row'][0])) {
                    return [0 => $data['Row']];
                }
                return $data['Row'];
            }
        }

        if (method_exists($response, 'getErrorMessage') && $response->getErrorMessage()) {
            throw new \LogicException($response->getErrorMessage());
        }

        return [];
    }
}
