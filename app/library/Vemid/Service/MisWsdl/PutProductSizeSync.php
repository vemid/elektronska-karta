<?php

namespace Vemid\Service\MisWsdl;


use Psr\Log\LoggerInterface;
use Vemid\Entity\Manager\EntityManagerInterface;

class PutProductSizeSync extends BaseWsdl
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var \ProductSize */
    private $productSize;

    /**
     * PutProductSizes constructor.
     * @param EntityManagerInterface $entityManager
     * @param \ProductSize $productSize
     * @param LoggerInterface|null $logger
     */
    public function __construct(EntityManagerInterface $entityManager, \ProductSize $productSize, LoggerInterface $logger = null)
    {
        $this->entityManager = $entityManager;
        $this->productSize = $productSize;

        parent::__construct($logger);
    }

    public function run()
    {
        $code = $this->productSize->getCode();

        $sizeQuery = sprintf(
            'SELECT * from ean_kod WHERE sif_rob = \'%s\' AND sif_ent_rob = \'%s\' AND bar_kod = \'%s\'',
            $this->productSize->getProduct()->getCode(),
            $code->getCode(),
            $this->productSize->getBarcode()
        );

        try {
            $sizeResponse = $this->getData($sizeQuery);

            if (!empty($sizeResponse)) {
                $this->productSize->setIsSynced(true);

                if (!$this->entityManager->save($this->productSize)) {
                    throw new \LogicException(sprintf('Unable to set synced to product size with ID: %s', $this->productSize->getId()));
                }

                return;
            }
        } catch (\Exception $e) {}

        $procedure = sprintf(
            'execute procedure test_import_barkod(\'%s\', \'%s\', \'%s\')',
            $this->productSize->getProduct()->getCode(),
            $this->productSize->getBarcode(),
            $code->getCode()
        );

        try {
            $response = $this->getData($procedure);

            if (!$response['success']) {
                throw new \LogicException(sprintf('Error on Service - %s', $response['error']));
            }

            $this->productSize->setIsSynced(true);

            if (!$this->entityManager->save($this->productSize)) {
                throw new \LogicException(sprintf('Unable to set synced to product size with ID: %s', $this->productSize->getId()));
            }
        } catch (\Exception $e) {}
    }
}
