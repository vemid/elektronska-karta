<?php

namespace Vemid\Service\MisWsdl;


use Psr\Log\LoggerInterface;
use Vemid\Entity\Manager\EntityManagerInterface;

class PutProductSingleProcedure extends BaseWsdl
{
    /** @var \Product */
    private $product;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var  string */
    private $userName;

    /**
     * PutProduct constructor.
     * @param \Product $product
     * @param EntityManagerInterface $entityManager
     * @param $userName
     * @param LoggerInterface|null $logger
     */
    public function __construct(\Product $product, EntityManagerInterface $entityManager, $userName, LoggerInterface $logger = null)
    {
        $this->product = $product;
        $this->entityManager = $entityManager;
        $this->userName = $userName;

        parent::__construct($logger);
    }

    public function run()
    {

        /** @var \Priority $priority */
        $priority = $this->product->getPriority();


        $entryAttributes = sprintf(
            'execute procedure test_import_entry_attribute(\'%s\',\'%s\',\'%s\',\'%s\');',
            $this->product->getCode(),
            $priority->getPriorityNumber(). "_" .$priority->getPriorityDate(),
            $priority->getName(),
            $this->userName
        );


        try {
            $response = $this->getData($entryAttributes);

            if (!$response['success']) {
                throw new \LogicException(sprintf('Error on Service - %s', $response['error']));
            }

            //$this->productClassification->setIsSynced(true);

            if (!$this->entityManager->save($this->$entryAttributes)) {
                throw new \LogicException(sprintf('Unable to set synced to product attribute with ID: %s', $this->productClassification->getId()));
            }
        } catch (\Exception $e) {}
    }
}
