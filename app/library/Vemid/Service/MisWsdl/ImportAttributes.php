<?php

namespace Vemid\Service\MisWsdl;

use Vemid\Task\Attributes\ImportAttributesTask;

/**
 * Class ImportAttributeValue
 * @package Vemid\Service\MisWsdl
 */
class ImportAttributes extends BaseWsdl
{
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $data = $this->getData("SELECT ozn_par_par, vre_par FROM param_par_vre");

        foreach ($data as $row){
            $importTask = new ImportAttributesTask();
            $importTask->codeData = $row;
            $importTask->runInBackground();
        }
    }
}