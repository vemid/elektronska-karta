<?php

namespace Vemid\Service\MisWsdl;

use Vemid\Task\Codes\ImportCodesTask;
use Vemid\Task\Failure\ImportFailureTypesTask;

/**
 * Class ImportCodes
 * @package Vemid\Service\MisWsdl
 */
class ImportCodes extends BaseWsdl
{
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $data = $this->getData("SELECT 'ATRIBUTES' code_type, ozn_par_par, naz_par_par FROM param_par");

        foreach ($data as $row){
            $importTask = new ImportCodesTask();
            $importTask->codeData = $row;
            $importTask->runInBackground();
        }
    }
}