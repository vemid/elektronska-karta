<?php

namespace Vemid\Service\MisWsdl;

use Vemid\Task\Classifications\ImportClassificationsTask;

/**
 * Class ImportClassification
 * @package Vemid\Service\MisWsdl
 */
class ImportClassifications extends BaseWsdl
{
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $data = $this->getData("SELECT sif_kri_kla, kla_ozn, naz_kla, nad_kla_ozn, vrs_kla FROM klas_robe WHERE vid_web = 1 and sif_kri_kla IN ('00','01','02','03','05','07','12','13') AND vrs_kla NOT IN ('2','4') ORDER BY sif_kri_kla,ozn_niv_kla");

        foreach ($data as $row) {
            $importTask = new ImportClassificationsTask();
            $importTask->classificationData = $row;
            $importTask->runInBackground();
        }
    }
}