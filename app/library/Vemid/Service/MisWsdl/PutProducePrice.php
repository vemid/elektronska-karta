<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 10/24/18
 * Time: 13:18
 */

namespace Vemid\Service\MisWsdl;

use Psr\Log\LoggerInterface;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Service\Product\Hydrator;

class PutProducePrice extends BaseWsdl
{

    /** @var \Product */
    private $product;

    /** @var EntityManagerInterface  */
    private $entityManager;

    /** @var  bool */
    private $merchandise;

    /**
     * PutProducePrice constructor.
     * @param \Product $product
     * @param EntityManagerInterface $entityManager
     * @param $merchandise
     * @param LoggerInterface|null $logger
     */
    public function __construct(\Product $product, EntityManagerInterface $entityManager, bool $merchandise, LoggerInterface $logger = null)
    {
        $this->product = $product;
        $this->entityManager = $entityManager;
        $this->merchandise = $merchandise;

        parent::__construct($logger);
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $checkForExistingProduct = $this->getData("SELECT * FROM roba WHERE sif_rob = '{$this->product->getCode()}'");

        if (empty($checkForExistingProduct)) {
            return;
        }

        /** @var \Pricelist $pricelist */
        $pricelist = $this->entityManager->findOne(\Pricelist::class,[
            \Pricelist::PROPERTY_CODE . ' = :priceList: ',
            'bind' => [

                'priceList' => '01/140000001'
            ]
        ]);


        if(!$this->merchandise) {
            /** @var \ProductPrice $prices */
            $prices = $this->entityManager->findOne(\ProductPrice::class, [
                \ProductPrice::PROPERTY_STATUS . ' = :status: AND ' . \ProductPrice::PROPERTY_PRODUCT_ID . ' = :productId: AND ' .
                \ProductPrice::PROPERTY_PRICELIST_ID . ' = :priceList:',
                'bind' => [
                    'status' => \ProductPrice::STATUS_FINAL,
                    'productId' => $this->product->getId(),
                    'priceList' => $pricelist->getId()
                ]
            ]);


            $procedure = sprintf(
                'execute procedure test_import_roba_u_magacin(\'%s\', \'%s\', \'%s\', \'%s\')',
                $this->product->getCode(),
                $prices->getPurchasePrice(),
                $prices->getWholesalePrice(),
                $prices->getRetailPrice()

            );

            $response = $this->getData($procedure);

            if (!$response['success']) {
                throw new \RuntimeException($response['message']);
            }
        }

        /** @var \ProductPrice[] $prices */
        $prices = $this->entityManager->find(\ProductPrice::class, [
            \ProductPrice::PROPERTY_STATUS . ' = :status: AND ' . \ProductPrice::PROPERTY_PRODUCT_ID . ' = :productId: ',
            'bind' => [
                'status' => \ProductPrice::STATUS_FINAL,
                'productId' => $this->product->getId()
            ]
        ]);

        foreach ($prices as $price) {
            if ($price->getIsSynced()) {
                continue;
            }

            $priceProcedure = sprintf(
                'execute procedure test_import_cena_u_cenovnik(\'%s\', \'%s\', \'%s\', \'%s\', \'%s\')',
                $price->getPricelist()->getCode(),
                $this->product->getCode(),
                $price->getPurchasePrice(),
                $price->getWholesalePrice(),
                $price->getRetailPrice()
            );

            $response = $this->getData($priceProcedure);

            if (!$response['success']) {
                throw new \RuntimeException($response['message']);
            }

            $price->setIsSynced(true);

            if (!$this->entityManager->save($price)) {
                throw new \LogicException(sprintf('Unable to set synced to product price with ID: %s', $price->getId()));
            }
        }
    }
}