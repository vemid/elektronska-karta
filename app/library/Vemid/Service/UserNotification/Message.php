<?php

namespace  Vemid\Service\UserNotification;

use Vemid\Date\DateTime;
use Vemid\Entity\EntityInterface;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Helper\TextHelper;
use Zend\EventManager\Exception\DomainException;

class Message
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * Message constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param EntityInterface $entity
     * @param string|null $messageType
     * @return string
     *
     * @throws \DomainException
     */
    public function create(EntityInterface $entity, $messageType = null)
    {
        $code = TextHelper::stringToCode(\get_class($entity));

        /** @var \NotificationType $notificationType */
        $notificationType = $this->entityManager->findOne(\NotificationType::class, [
            \NotificationType::PROPERTY_TYPE . ' = :type:',
            'bind' => [
                'type' => $code
            ]
        ]);

        if (!$notificationType) {
            throw new DomainException('Notification Type do not exist!');
        }

        /** @var \UserNotification[] $userNotifications */
        $userNotifications = $this->entityManager->find(\UserNotification::class, [
            \UserNotification::PROPERTY_NOTIFICATION_TYPE_ID . ' = :notificationTypeId: AND '.
            \UserNotification::PROPERTY_SUBSCRIBED . ' = :subscribed:',
            'bind' => [
                'notificationTypeId' => $notificationType->getId(),
                'subscribed' => true
            ]
        ]);

        $message = Messages::getMessage($messageType);
        foreach ($userNotifications as $userNotification) {
            $this->addNotification($userNotification, $entity, $message);
        }

        return $code;
    }

    /**
     * @param \UserNotification $userNotification
     *
     * @throws \DomainException
     */
    public function unsubscribe(\UserNotification $userNotification)
    {
        $userNotification->setSubscribed(false);

        if (!$this->entityManager->save($userNotification)) {
            throw new \DomainException('User Notification failed to unsubscribe!');
        }

        foreach ($userNotification->getUserNotificationMessages() as $userNotificationMessage) {
            $this->unsubscribeMessage($userNotificationMessage);
        }
    }

    /**
     * @param \UserNotificationMessage $userNotificationMessage
     *
     * @throws \DomainException
     */
    public function unsubscribeMessage(\UserNotificationMessage $userNotificationMessage)
    {
        $userNotificationMessage->setStatus(\UserNotificationMessage::STATUS_DEACTIVATED);

        if (!$this->entityManager->save($userNotificationMessage)) {
            throw new \DomainException('User Notification failed to unsubscribe!');
        }
    }

    /**
     * @param \UserNotification $userNotification
     *
     * @throws \DomainException
     */
    public function subscribe(\UserNotification $userNotification)
    {
        $userNotification->setSubscribed(true);

        if (!$this->entityManager->save($userNotification)) {
            throw new \DomainException('User Notification failed to unsubscribe!');
        }

        foreach ($userNotification->getUserNotificationMessages() as $userNotificationMessage) {
            $this->subscribeMessage($userNotificationMessage);
        }
    }

    /**
     * @param \UserNotificationMessage $userNotificationMessage
     *
     * @throws \DomainException
     */
    public function subscribeMessage(\UserNotificationMessage $userNotificationMessage)
    {
        $userNotificationMessage->setStatus(\UserNotificationMessage::STATUS_ACTIVE);

        if (!$this->entityManager->save($userNotificationMessage)) {
            throw new \DomainException('User Notification failed to unsubscribe!');
        }
    }

    /**
     * @param int $days
     *
     * @throws \DomainException
     */
    public function reActivateMessages($days = 20)
    {
        $deactivatedDate = new DateTime("-$days days");
        $deactivatedDate->setDateType(DateTime::TYPE_DATE);

        $userNotificationMessages = $this->entityManager->find(\UserNotificationMessage::class, [
            \UserNotificationMessage::PROPERTY_DEACTIVATED_DATE . ' <= :deactivatedDate: AND '.
            \UserNotificationMessage::PROPERTY_DEACTIVATED_DATE . ' IS NOT NULL',
            'bind' => [
                'deactivatedDate' => $deactivatedDate->getUnixFormat()
            ]
        ]);

        foreach ($userNotificationMessages as $userNotificationMessage) {
            $this->reactivate($userNotificationMessage);
        }
    }

    /**
     * @throws \DomainException
     */
    public function postponedToActive()
    {
        /** @var \UserNotificationMessage[] $userNotificationMessages */
        $userNotificationMessages = $this->entityManager->find(\UserNotificationMessage::class, [
            \UserNotificationMessage::PROPERTY_STATUS . ' LIKE :status:',
            'bind' => [
                'status' => '%POSTPONE%'
            ]
        ]);

        $today = new DateTime();

        foreach ($userNotificationMessages as $userNotificationMessage) {
            if (!$userNotificationMessage->getPostponedDate()) {
                continue;
            }

            $statusDate = clone $userNotificationMessage->getPostponedDate();

            switch ($userNotificationMessage->getStatus()) {
                case \UserNotificationMessage::STATUS_POSTPONED_ONE_DAY:
                    $statusDate->modify('+1 day');
                    break;
                case \UserNotificationMessage::STATUS_POSTPONED_TWO_DAYS:
                    $statusDate->modify('+2 days');
                    break;
                case \UserNotificationMessage::STATUS_POSTPONED_THREE_DAYS:
                    $statusDate->modify('+3 days');
                    break;
                case \UserNotificationMessage::STATUS_POSTPONED_FIVE_DAYS:
                    $statusDate->modify('+5 days');
                    break;
                case \UserNotificationMessage::STATUS_POSTPONED_TEN_DAYS:
                    $statusDate->modify('+10 days');
                    break;
                default:
                    continue 2;
                    break;
            }

            if ($statusDate <= $today) {
                $userNotificationMessage->setStatus(\UserNotificationMessage::STATUS_ACTIVE);
                $userNotificationMessage->setPostponedDate(null);

                if (!$this->entityManager->save($userNotificationMessage)) {
                    throw new \DomainException('Can not re activate postponed message!');
                }
            }
        }
    }

    /**
     * @param \UserNotification $userNotification
     * @param EntityInterface $entity
     * @param string|null $message
     * @return \UserNotificationMessage
     *
     * @throws \DomainException
     */
    private function addNotification(\UserNotification $userNotification, EntityInterface $entity, $message = null)
    {
        $userNotificationMessage = new \UserNotificationMessage();
        $userNotificationMessage->setUserNotification($userNotification);
        $userNotificationMessage->setEntity($entity);
        $userNotificationMessage->setMessage($message);

        if (!$this->entityManager->save($userNotificationMessage)) {
            throw new \DomainException('User Notification Message failed to save!');
        }

        return $userNotificationMessage;
    }

    /**
     * @param \UserNotificationMessage $userNotificationMessage
     * @return \UserNotificationMessage
     *
     * @throws \DomainException
     */
    private function reactivate(\UserNotificationMessage $userNotificationMessage)
    {
        $userNotificationMessage->setStatus(\UserNotificationMessage::STATUS_ACTIVE);
        $userNotificationMessage->setDeactivatedDate(null);

        if (!$this->entityManager->save($userNotificationMessage)) {
            throw new \DomainException('User Notification Message failed to reactivate!');
        }

        return $userNotificationMessage;
    }
}