<?php

namespace Vemid\Service\UserNotification;

/**
 * Class Messages
 * @package Vemid\Service\UserNotification
 */
class Messages
{
    const MATERIAL_SAMPLE_CREATE = 'MATERIAL_SAMPLE_CREATE';

    /**
     * @var array
     */
    private static $messageTypes = [
        self::MATERIAL_SAMPLE_CREATE => 'Kupon kreiran'
    ];

    /**
     * @param $messageType
     * @return mixed|null
     */
    public static function getMessage($messageType)
    {
        if ($messageType && array_key_exists($messageType, self::$messageTypes)) {
            return self::$messageTypes[$messageType];
        }

        return null;
    }
}
