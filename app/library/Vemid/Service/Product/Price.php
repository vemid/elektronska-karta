<?php

namespace Vemid\Service\Product;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Application\Di;

/**
 * Class Price
 * @package Vemid\Service\Product
 */
class Price
{
    /** @var \ProductCalculation */
    private $productCalculation;

    /** @var EntityManagerInterface  */
    private $entityManager;

    /** @var $wholesalePrice */
    private $wholesalePrice;

    /**
     * Price constructor.
     * @param \ProductCalculation $productCalculation
     * @param float $wholesalePrice
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager, \ProductCalculation $productCalculation, $wholesalePrice)
    {
        $this->entityManager = $entityManager;
        $this->productCalculation = $productCalculation;
        $this->wholesalePrice = (float)$wholesalePrice;
    }

    /**
     * @throws \LogicException|\DomainException
     */
    public function addPrices()
    {
        /** @var \Pricelist[] $priceLists */
        $priceLists = $this->entityManager->find(\Pricelist::class);
        $productCalculationItems = $this->productCalculation->getProductCalculationItems();

        $total = 0;
        foreach ($productCalculationItems as $productCalculationItem)
        {
            $total += $productCalculationItem->getTotal();
        }

        if (!$total) {
            throw new \LogicException('Total material value must be grather then 0');
        }

        /** @var Hydrator $hydrator */
        $hydrator = new Hydrator($this->productCalculation->getProduct(), $this->entityManager);

        foreach ($priceLists as $priceList){
            if($hydrator->getCollection()->getName() == "Basic") {
                $this->addBasicPrice($priceList, $total);
            }
            else {
                $this->addPrice($priceList, $total);
            }
        }
    }

    /**
     * @throws \LogicException|\DomainException
     * @return float|null
     */
    public function getTotal()
    {

        $productCalculationItems = $this->productCalculation->getProductCalculationItems();

        $total = 0;
        foreach ($productCalculationItems as $productCalculationItem)
        {
            $total += $productCalculationItem->getTotal();
        }


    return $total;
    }

    /**
     * @return float|null
     */
    public function getEurWholesalePrice()
    {
        /** @var \Pricelist $eurWholesalePricelist */
        $eurWholesalePricelist = $this->entityManager->findOne(\Pricelist::class, [
            ' code = :code:',
            'bind' => [
                'code' => '01/140000002'
            ]
        ]);

        $eurWholesalePrice = null;
        if ($eurWholesalePricelist) {
            /** @var \ProductPrice $eurWholesalePrice */
            $eurWholesalePrice = $this->entityManager->findOne(\ProductPrice::class, [
                'productId = :productId: AND pricelistId = :pricelist: AND status = :status:',
                'bind' => [
                    'productId' => $this->productCalculation->getProductId(),
                    'pricelist' => $eurWholesalePricelist->getId(),
                    'status' => $this->productCalculation->getStatus()
                ]
            ]);
        }

        return $eurWholesalePrice ? $eurWholesalePrice->getWholesalePrice() : null;
    }

    /**
     * @param \Product $product
     * @return array
     */
    public function getPrices(\Product $product)
    {
        /** @var Di $di */
        $di = Di::getDefault();
        $modelsManager = $di->getModelsManager();

        $query = $modelsManager->createBuilder()
            ->columns([
                'p.name',
                'round(pr.purchasePrice,2) purchasePrice',
                'round(pr.wholesalePrice,2) wholesalePrice',
                'round(pr.retailPrice,1) retailPrice',
                'pr.id'
            ])
            ->addFrom(\ProductPrice::class, 'pr')
            ->leftJoin(\Pricelist::class, 'pr.pricelistId = p.id', 'p')
            ->andWhere('pr.productId = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('pr.status = :status:',[
                'status' =>$this->productCalculation->getStatus()
            ])
            ->groupBy('p.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param \Product $product
     * @return array
     */
    public function getPlanedPrices(\Product $product)
    {
        /** @var Di $di */
        $di = Di::getDefault();
        $modelsManager = $di->getModelsManager();

        $query = $modelsManager->createBuilder()
            ->columns([
                'p.name',
                'round(pr.purchasePrice,2) purchasePrice',
                'round(pr.wholesalePrice,2) wholesalePrice',
                'round(pr.retailPrice,1) retailPrice',
            ])
            ->addFrom(\ProductPrice::class, 'pr')
            ->leftJoin(\Pricelist::class, 'pr.pricelistId = p.id', 'p')
            ->andWhere('pr.productId = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('pr.status = :status:',[
                'status' =>\ProductCalculation::STATUS_PLANED
            ])
            ->groupBy('p.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param \Product $product
     * @return array
     */
    public function getFinalPrices(\Product $product)
    {
        /** @var Di $di */
        $di = Di::getDefault();
        $modelsManager = $di->getModelsManager();

        $query = $modelsManager->createBuilder()
            ->columns([
                'p.name',
                'round(pr.purchasePrice,2) purchasePrice',
                'round(pr.wholesalePrice,2) wholesalePrice',
                'round(pr.retailPrice,1) retailPrice',
            ])
            ->addFrom(\ProductPrice::class, 'pr')
            ->leftJoin(\Pricelist::class, 'pr.pricelistId = p.id', 'p')
            ->andWhere('pr.productId = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('pr.status = :status:',[
                'status' =>\ProductCalculation::STATUS_FINAL
            ])
            ->groupBy('p.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param float $number
     * @return float
     */
    public function calculateCgRetailPrice(float $number)
    {
        $x = $number * 2 * 1.1;
        if (fmod($x,1) < 0.25)
            $x = floor($x)-0.1;
        elseif (fmod($x,1) >= 0.25 && fmod($x,1) < 0.75)
            $x = floor($x)+0.5;
        else $x = ceil($x)-0.1;

        return $x;
    }

    /**
     * @param float $number
     * @return float
     */
    public function calculateBasicCgRetailPrice(float $number)
    {
        $x = $number * 2 ;
        if (fmod($x,1) < 0.25)
            $x = floor($x)-0.1;
        elseif (fmod($x,1) >= 0.25 && fmod($x,1) < 0.75)
            $x = floor($x)+0.5;
        else $x = ceil($x)-0.1;

        return $x;
    }



    /**
     * @param \Pricelist $pricelist
     * @param $total
     * @throws \DomainException
     */
    private function addPrice(\Pricelist $pricelist, $total)
    {
        $markUp = $this->productCalculation->getCalculationMapping() ? $this->productCalculation->getCalculationMapping()->getMarkUp() : 0;
        $eur = $this->productCalculation->getCalculationMapping() ? $this->productCalculation->getCalculationMapping()->getEurCourse() : 0;

        switch ($pricelist->getCode()){
            case '01/140000001':
                $purchasePrice = round($total * ($markUp + 1) * $eur, 3);
                $wholesalePrice = $this->wholesalePrice * $eur;
                //$retailPrice = (round((($this->wholesalePrice-0.2) * $eur * 2)/100)*100-10)+60;
                $retailPrice = round(($this->wholesalePrice * $eur * 2)/100)*100-10;
                //$retailPrice = round(($this->wholesalePrice * $eur * 2)/50)*50;
                break;
            case '60/160000001':
                $purchasePrice = round($total * ($markUp + 1) * 2, 3);
                $wholesalePrice = round($this->wholesalePrice * 2.1, 2);
                $retailPrice = round($this->wholesalePrice * 4.4);
                break;
            case '01/140000002':
                $purchasePrice = round($total * ($markUp + 1), 3);
                $wholesalePrice =  $this->wholesalePrice;
                $retailPrice = $this->wholesalePrice * 2;
                break;
            case '03/160000001':
                $purchasePrice = round($total * ($markUp + 1), 3);
                $wholesalePrice = $this->wholesalePrice ;
                $retailPrice = $this->calculateCgRetailPrice($this->wholesalePrice);
                break;
            default:
                $purchasePrice = 0;
                $wholesalePrice = 0;
                $retailPrice = 0;
                break;
        }

        if ($purchasePrice && $wholesalePrice && $retailPrice) {
            /** @var \ProductPrice $productPrice */
            $productPrice = $this->entityManager->findOne(\ProductPrice::class, [
                \ProductPrice::PROPERTY_PRODUCT_ID . ' = :productId: AND '.
                \ProductPrice::PROPERTY_PRICELIST_ID . ' = :priceListId: AND '.
                \ProductPrice::PROPERTY_STATUS . ' = :status:',
                'bind' => [
                    'productId' => $this->productCalculation->getProductId(),
                    'priceListId' => $pricelist->getId(),
                    'status' => $this->productCalculation->getStatus()
                ]
            ]);

            if (!$productPrice) {
                $productPrice = new \ProductPrice();
                $productPrice->setProduct($this->productCalculation->getProduct());
                $productPrice->setPricelist($pricelist);
            }

            $productPrice->setPurchasePrice($purchasePrice);
            $productPrice->setWholesalePrice($wholesalePrice);
            $productPrice->setRetailPrice($retailPrice);
            $productPrice->setStatus($this->productCalculation->getStatus());
            $productPrice->setIsSynced(false);

            if (!$this->entityManager->save($productPrice)) {
                throw new \DomainException('Price failed to save!');
            }
        }
    }

    /**
     * @param \Pricelist $pricelist
     * @param $total
     * @throws \DomainException
     */
    private function addBasicPrice(\Pricelist $pricelist, $total)
    {
        $markUp = $this->productCalculation->getCalculationMapping() ? $this->productCalculation->getCalculationMapping()->getMarkUp() : 0;
        $eur = $this->productCalculation->getCalculationMapping() ? $this->productCalculation->getCalculationMapping()->getEurCourse() : 0;

        switch ($pricelist->getCode()){
            case '01/140000001':
                $this->wholesalePrice = round($this->wholesalePrice/$eur/1.8,2);
                $purchasePrice = round($total * ($markUp + 1) * $eur, 3);
                $wholesalePrice = $this->wholesalePrice * $eur;
                //$retailPrice = (round((($this->wholesalePrice-0.2) * $eur * 2)/100)*100-10)+60;
                $retailPrice = round(($this->wholesalePrice * $eur * 1.8)/100)*100-10;
                //$retailPrice = round(($this->wholesalePrice * $eur * 2)/50)*50;
                break;
            case '60/160000001':
                $purchasePrice = round($total * ($markUp + 1) * 2, 3);
                $wholesalePrice = round($this->wholesalePrice * 2, 2);
                $retailPrice = round($this->wholesalePrice * 4);
                break;
            case '01/140000002':
                $purchasePrice = round($total * ($markUp + 1), 3);
                $wholesalePrice =  $this->wholesalePrice;
                $retailPrice = $this->wholesalePrice * 1.8;
                break;
            case '03/160000001':
                $purchasePrice = round($total * ($markUp + 1), 3);
                $wholesalePrice = $this->wholesalePrice ;
                $retailPrice = $this->calculateBasicCgRetailPrice($this->wholesalePrice);
                break;
            default:
                $purchasePrice = 0;
                $wholesalePrice = 0;
                $retailPrice = 0;
                break;
        }

        if ($purchasePrice && $wholesalePrice && $retailPrice) {
            /** @var \ProductPrice $productPrice */
            $productPrice = $this->entityManager->findOne(\ProductPrice::class, [
                \ProductPrice::PROPERTY_PRODUCT_ID . ' = :productId: AND '.
                \ProductPrice::PROPERTY_PRICELIST_ID . ' = :priceListId: AND '.
                \ProductPrice::PROPERTY_STATUS . ' = :status:',
                'bind' => [
                    'productId' => $this->productCalculation->getProductId(),
                    'priceListId' => $pricelist->getId(),
                    'status' => $this->productCalculation->getStatus()
                ]
            ]);

            if (!$productPrice) {
                $productPrice = new \ProductPrice();
                $productPrice->setProduct($this->productCalculation->getProduct());
                $productPrice->setPricelist($pricelist);
            }

            $productPrice->setPurchasePrice($purchasePrice);
            $productPrice->setWholesalePrice($wholesalePrice);
            $productPrice->setRetailPrice($retailPrice);
            $productPrice->setStatus($this->productCalculation->getStatus());
            $productPrice->setIsSynced(false);

            if (!$this->entityManager->save($productPrice)) {
                throw new \DomainException('Price failed to save!');
            }
        }
    }
}
