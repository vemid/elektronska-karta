<?php

namespace Vemid\Service\Product;

use Vemid\Entity\Manager\EntityManagerInterface;

class Tags
{
    /** @var EntityManagerInterface  */
    private $entityManager;
    
    /** @var \Product */
    private $product;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param \Product $product
     */
    public function __invoke(\Product $product)
    {
        $this->product = $product;
        $productHyd = new Hydrator($product, $this->entityManager);
        
        $this->setBrandTag($productHyd);
        $this->setGenderTag($productHyd);
        $this->setCollectionTag($productHyd);
        $this->setModelTag($productHyd);
        $this->setSeasonTag($productHyd);
        $this->setSizeTags($productHyd);
        $this->setAgeTag($productHyd);
        $this->setColorTag($productHyd);
    }
    
    private function setGenderTag(Hydrator $hydProduct)
    {
        if (!$genderClassification = $hydProduct->getGender()) {
            return;
        }

        $gender = $genderClassification->getName();
        $tag = explode(' ', $gender);
        $gender = $tag[1] ?? $gender;

        if ($this->checkIfTagExist(\ProductTag::GENDER, $gender)) {
            return;
        }
        
        $this->setTag(\ProductTag::GENDER, $gender);
    }

    private function setSizeTags(Hydrator $hydProduct)
    {
        $sizes = explode(',', $hydProduct->getAllProductSizes());
        foreach ($sizes as $size) {
            if ($this->checkIfTagExist(\ProductTag::SIZE, $size)) {
                continue;
            }

            $this->setTag(\ProductTag::SIZE, $size);
        }
    }

    private function setCollectionTag(Hydrator $hydProduct)
    {
        if (!$collection = $hydProduct->getCollection()) {
            return;
        }

        if ($this->checkIfTagExist(\ProductTag::COLLECTION, $collection->getName())) {
            return;
        }

        $this->setTag(\ProductTag::COLLECTION, $collection->getName());
    }

    private function setBrandTag(Hydrator $hydProduct)
    {
        if (!$brand = $hydProduct->getBrand()) {
            return;
        }

        if ($this->checkIfTagExist(\ProductTag::BRAND, $brand->getName())) {
            return;
        }

        $this->setTag(\ProductTag::BRAND, $brand->getName());
    }

    private function setSeasonTag(Hydrator $hydProduct)
    {
        if (!$season = $hydProduct->getDoubleSeason()) {
            return;
        }

        if ($this->checkIfTagExist(\ProductTag::SEASON, $season->getName())) {
            return;
        }

        $this->setTag(\ProductTag::SEASON, $season->getName());
    }

    private function setAgeTag(Hydrator $hydProduct)
    {
        if (!$age = $hydProduct->getAge()) {
            return;
        }

        if ($this->checkIfTagExist(\ProductTag::AGE, $age->getName())) {
            return;
        }

        $this->setTag(\ProductTag::AGE, $age->getName());
    }

    private function setColorTag(Hydrator $hydProduct)
    {
        if (!$color = $hydProduct->getColor()) {
            return;
        }

        if ($this->checkIfTagExist(\ProductTag::COLOR, $color->getShortColorName())) {
            return;
        }

        $this->setTag(\ProductTag::COLOR, $color->getShortColorName());
    }

    private function setModelTag(Hydrator $hydProduct)
    {
        if (!$attribute = $hydProduct->getAttributeModel()) {
            return;
        }

        if ($this->checkIfTagExist(\ProductTag::MODEL, $attribute->getName())) {
            return;
        }

        $this->setTag(\ProductTag::MODEL, $attribute->getName());
    }

    /**
     * @param \Product $product
     * @param string $category
     * @param string $tag
     * @return \ProductTag|null
     */
    private function checkIfTagExist(string $category, string $tag)
    {
        return $this->entityManager->findOne(\ProductTag::class, [
            \ProductTag::PROPERTY_PRODUCT_ID . ' = :productId: AND ' .
            \ProductTag::PROPERTY_CATEGORY . ' = :category: AND ' .
            \ProductTag::PROPERTY_TAG . ' = :tag:',
            'bind' => [
                'productId' => $this->product->getId(),
                'category' => $category,
                'tag' => $tag
            ]
        ]);
    }

    /**
     * @param string $category
     * @param string $tag
     */
    private function setTag(string $category, string $tag)
    {
        $productTag = new \ProductTag();
        $productTag->setProduct($this->product);
        $productTag->setCategory($category);
        $productTag->setTag($tag);
        $this->entityManager->save($productTag);
    }
}
