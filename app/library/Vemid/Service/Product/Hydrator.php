<?php

namespace Vemid\Service\Product;

use Phalcon\Mvc\Model\Resultset;
use Vemid\Entity\EntityInterface;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;

class Hydrator
{
    /** @var \Product */
    private $product;

    /** @var EntityManagerInterface  */
    private $entityManager;

    /** @var  \CodeType */
    private $codeTypeClassificationType;

    /** @var  \CodeType */
    private $codeTypeAttribute;

    /**
     * Hydrator constructor.
     * @param \Product $product
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(\Product $product, EntityManagerInterface $entityManager)
    {
        $this->product = $product;
        $this->entityManager = $entityManager;
        $this->codeTypeClassificationType = $this->getCodeTypeClassificationType();
        $this->codeTypeAttribute = $this->getCodeTypeAttribute();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->product->getId();
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->product->getCode();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->product->getName();
    }
    /**
     * @return string
     */
    public function getWebName()
    {
        return $this->product->getWebName();
    }
    /**
     * @return string
     */
    public function getProduceType()
    {
        return $this->product->getProduceType();
    }


    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->product->getStatus();
    }

    /**
     * @return int
     */
    public function getImage()
    {
        return $this->product->getImage();
    }

    /**
     * @return null|\User
     *
     * @throws \DomainException
     */
    public function getUser()
    {
        return $this->product->getUser();
    }

    /**
     * @return null|\Code
     *
     * @throws \DomainException
     */
    public function getProductCodeType()
    {
        return $this->product->getProductCodeType();
    }

    /**
     * @return \ProductClassification[]|Resultset
     *
     * @throws \DomainException
     */
    public function getProductClassifications()
    {
        return $this->product->getProductClassifications();
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return $this->product->getUploadPath();
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->product->getImagePath();
    }

    /**
     * @return null|\Classification
     * @throws \DomainException
     */
    public function getSeason()
    {
        if (!$this->codeTypeClassificationType) {
            return null;
        }

        /** @var \Code $seasonCode */
        $seasonCode = $this->entityManager->findOne(\Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '05',
                'codeTypeId' => $this->codeTypeClassificationType->getId()
            ]
        ]);

        return $this->getProductClassification($seasonCode, $this->getProductCodeType());
    }

    /**
     * @return \Classification|null
     * @throws \DomainException
     */
    public function getProductProgram()
    {
        return $this->getGender() ? $this->getGender()->getParentClassification() : null;
    }

    /**
     * @return \Classification|null
     * @throws \DomainException
     */
    public function getGender()
    {
        return $this->getProductGroup() ? $this->getProductGroup()->getParentClassification() : null;
    }

    /**
     * @return \Classification|null
     * @throws \DomainException
     */
    public function getGenderName()
    {
        $gender = $this->getProductGroup()->getParentClassification();
        $array = explode(' ', $gender);
        $genderName = array_pop($array);

        if($genderName === 'Muska'){
            $genderName = 'Muski';
        } elseif ($genderName === 'Zenska'){
            $genderName = 'Zenski';
        }

        return $genderName;
    }

    /**
     * @return \Classification|null
     * @throws \DomainException
     */
    public function getProductGroup()
    {
        return $this->getSubProductGroup() ? $this->getSubProductGroup()->getParentClassification() : null;
    }

    /**
     * @return \Classification|null
     * @throws \DomainException
     */
    public function getSubProductGroup()
    {
        return $this->getDubSubProductGroup() ? $this->getDubSubProductGroup()->getParentClassification() : null;
    }

    /**
     * @return \Classification|null
     * @throws \DomainException
     */
    public function getDubSubProductGroup()
    {
        $productClassifications = $this->entityManager->find(\ProductClassification::class, [
            'productId = :productId: AND entityId = :entityId: AND entityTypeId = :entityTypeId:',
            'bind' => [
                'productId' => $this->product->getId(),
                'entityId' => $this->getColor() ? $this->getColor()->getId() : -1,
                'entityTypeId' => Type::CLASSIFICATION
            ]
        ]);

        if ((int)$productClassifications->count() === 2) {
            return $this->getColor();
        }

        return $this->getColor() ? $this->getColor()->getParentClassification(): null;
    }

    /**
     * @return null|\Classification
     */
    public function getColor()
    {
        $colorCode = mb_substr(mb_substr($this->product->getCode(), 0, -2), 3);

        return $this->entityManager->findOne(\Classification::class, [
            \Classification::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $colorCode
            ]
        ]);
    }

    /**
     * @return null|\Classification
     * @throws \DomainException
     */
    public function getBrand()
    {
        if (!$this->codeTypeClassificationType) {
            return null;
        }

        /** @var \Code $brandCode */
        $brandCode = $this->entityManager->findOne(\Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '01',
                'codeTypeId' => $this->codeTypeClassificationType->getId()
            ]
        ]);

        return $this->getProductClassification($brandCode, $this->getProductCodeType());
    }

    /**
     * @return null|\Classification
     * @throws \DomainException
     */
    public function getAge()
    {
        if (!$this->codeTypeClassificationType) {
            return null;
        }

        /** @var \Code $ageCode */
        $ageCode = $this->entityManager->findOne(\Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '02',
                'codeTypeId' => $this->codeTypeClassificationType->getId()
            ]
        ]);

        return $this->getProductClassification($ageCode, $this->getProductCodeType());
    }

    /**
     * @return null|\Classification
     * @throws \DomainException
     */
    public function getMaterial()
    {
        if (!$this->codeTypeClassificationType) {
            return null;
        }

        /** @var \Code $materialCode */
        $materialCode = $this->entityManager->findOne(\Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '03',
                'codeTypeId' => $this->codeTypeClassificationType->getId()
            ]
        ]);

        return $this->getProductClassification($materialCode, $this->getProductCodeType());
    }

    /**
     * @return null|\Classification
     * @throws \DomainException
     */
    public function getCollection()
    {
        if (!$this->codeTypeClassificationType) {
            return null;
        }

        /** @var \Code $collectionCode */
        $collectionCode = $this->entityManager->findOne(\Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '12',
                'codeTypeId' => $this->codeTypeClassificationType->getId()
            ]
        ]);

        return $this->getProductClassification($collectionCode, $this->getProductCodeType());
    }

    /**
     * @return null|\Classification
     * @throws \DomainException
     */
    public function getOldCollection()
    {
        if (!$this->codeTypeClassificationType) {
            return null;
        }

        /** @var \Code $oldCollectionCode */
        $oldCollectionCode = $this->entityManager->findOne(\Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '07',
                'codeTypeId' => $this->codeTypeClassificationType->getId()
            ]
        ]);

        return $this->getProductClassification($oldCollectionCode, $this->getProductCodeType());
    }

    /**
     * @return null|\Classification
     * @throws \DomainException
     */
    public function getDoubleSeason()
    {
        if (!$this->codeTypeClassificationType) {
            return null;
        }

        /** @var \Code $doubleSeasonCode */
        $doubleSeasonCode = $this->entityManager->findOne(\Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '13',
                'codeTypeId' => $this->codeTypeClassificationType->getId()
            ]
        ]);

        return $this->getProductClassification($doubleSeasonCode, $this->getProductCodeType());
    }

    /**
     * @return null|\Attribute
     * @throws \DomainException
     */
    public function getDesignType()
    {
        /** @var \Code $designTypeCode */
        $designTypeCode = $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '05',
                'codeTypeId' => $this->codeTypeAttribute->getId()
            ]
        ]);

        return $this->getProductAttribute($designTypeCode);
    }

    /**
     * @return null|\Attribute
     * @throws \DomainException
     */
    public function getPrintType()
    {
        /** @var \Code $printTypeCode */
        $printTypeCode = $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '08',
                'codeTypeId' => $this->codeTypeAttribute->getId()
            ]
        ]);

        return $this->getProductAttribute($printTypeCode);
    }

    /**
     * @return null|\Attribute
     * @throws \DomainException
     */
    public function getApplication()
    {
        /** @var \Code $applicationCode */
        $applicationCode = $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '09',
                'codeTypeId' => $this->codeTypeAttribute->getId()
            ]
        ]);

        return $this->getProductAttribute($applicationCode);
    }

    /**
     * @return null|\Attribute
     * @throws \DomainException
     */
    public function getModelType()
    {
        /** @var \Code $modelTypeCode */
        $modelTypeCode = $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '04',
                'codeTypeId' => $this->codeTypeAttribute->getId()
            ]
        ]);

        return $this->getProductAttribute($modelTypeCode);
    }

    public function getAllProductSizes()
    {
        $allProductSizes = [];
        $productSizes = $this->product->getProductSizes();

        foreach ($productSizes as $productSize)
        {
            $allProductSizes[] = $productSize->getCode();
        }

        return implode(',', $allProductSizes);
    }

    /**
     * @return null|\Attribute
     * @throws \DomainException
     */
    public function getMaterialType()
    {
        /** @var \Code $materialTypeCode */
        $materialTypeCode = $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '20',
                'codeTypeId' => $this->codeTypeAttribute->getId()
            ]
        ]);

        return $this->getProductAttribute($materialTypeCode);
    }

    /**
     * @return null|\Attribute
     * @throws \DomainException
     */
    public function getProductType()
    {
        /** @var \Code $modelProductTypeCode */
        $modelProductTypeCode = $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '24',
                'codeTypeId' => $this->codeTypeAttribute->getId()
            ]
        ]);

        return $this->getProductAttribute($modelProductTypeCode);
    }

    /**
     * @return null|\Attribute
     * @throws \DomainException
     */
    public function getAttributeModel()
    {
        /** @var \Code $materialTypeCode */
        $modelCode = $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '25',
                'codeTypeId' => $this->codeTypeAttribute->getId()
            ]
        ]);

        return $this->getProductAttribute($modelCode);
    }

    /**
     * @return null|\Attribute
     * @throws \DomainException
     */
    public function getAttributeInsane()
    {
        /** @var \Code $modelInsaneCode */
        $modelInsaneCode = $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '27',
                'codeTypeId' => $this->codeTypeAttribute->getId()
            ]
        ]);

        return $this->getProductAttribute($modelInsaneCode);
    }

    /**
     * @return null|\Attribute
     * @throws \DomainException
     */
    public function getSeasonCollection()
    {
        /** @var \Code $seasonCollectionCode */
        $seasonCollectionCode = $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '03',
                'codeTypeId' => $this->codeTypeAttribute->getId()
            ]
        ]);

        return $this->getProductAttribute($seasonCollectionCode);
    }

    /**
     * @return null|\Attribute
     * @throws \DomainException
     */
    public function getManufacturer()
    {
        /** @var \Code $manufacturerCode */
        $manufacturerCode = $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '13',
                'codeTypeId' => $this->codeTypeAttribute->getId()
            ]
        ]);

        return $this->getProductAttribute($manufacturerCode);
    }

    /**
     * @return null|\Attribute
     * @throws \DomainException
     */
    public function getProductTypeCode()
    {
        /** @var \CodeType $productCodeType */
        $productCodeType = $this->entityManager->findOne(\CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => \CodeType::PRODUCT_TYPE
            ]
        ]);

        /** @var \Code $manufacturerCode */
        $manufacturerCode = $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE . ' = :code: AND ' .
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '13',
                'codeTypeId' => $productCodeType->getId()
            ]
        ]);

        return $this->getProductAttribute($manufacturerCode);
    }

    /**
     * @param \Code $classificationTypeCode
     * @param \Code $classificationCategoryCode
     * @return Resultset
     */
    private function getProductClassification(\Code $classificationTypeCode, \Code $classificationCategoryCode)
    {
        $query = $this->product->getModelsManager()
                ->createBuilder()->addFrom(\Classification::class, 'cl')
                ->leftJoin(\ProductClassification::class, 'pc.' . \ProductClassification::PROPERTY_ENTITY_ID . '= cl.id', 'pc')
                ->where('pc.' . \ProductClassification::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:', [
                    'entityTypeId' => Type::CLASSIFICATION
                ])
                ->andWhere('pc.' . \ProductClassification::PROPERTY_PRODUCT_ID . ' = :productId:', [
                    'productId' => $this->product->getId()
                ])
                ->andWhere('cl.' . \Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :classificationTypeCodeId:', [
                    'classificationTypeCodeId' => $classificationTypeCode->getId()
                ])
                ->andWhere('cl.' . \Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :classificationCategoryCodeId:', [
                    'classificationCategoryCodeId' => $classificationCategoryCode->getId()
                ])
                ->limit(1);

        return $query->getQuery()->execute()->getFirst();
    }

    /**
     * @param \Code $code
     * @return Resultset
     */
    private function getProductAttribute(\Code $code)
    {
        $query = $this->product->getModelsManager()
            ->createBuilder()->addFrom(\Attribute::class, 'a')
            ->leftJoin(\ProductClassification::class, 'pc.' . \ProductClassification::PROPERTY_ENTITY_ID . '= a.id', 'pc')
            ->where('pc.' . \ProductClassification::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:', [
                'entityTypeId' => Type::ATTRIBUTE
            ])
            ->andWhere('pc.' . \ProductClassification::PROPERTY_PRODUCT_ID . ' = :productId:', [
                'productId' => $this->product->getId()
            ])
            ->andWhere('a.' . \Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:', [
                'attributeCodeId' => $code->getId()
            ])
            ->limit(1);

        return $query->getQuery()->execute()->getFirst();
    }

    /**
     * @return Resultset
     */
    public function getProductCode()
    {
        $query = $this->product->getModelsManager()
            ->createBuilder()->addFrom(\Code::class, 'c')
            ->leftJoin(\ProductClassification::class, 'pc.' . \ProductClassification::PROPERTY_ENTITY_ID . '= c.id', 'pc')
            ->leftJoin(\CodeType::class, 'ct.' . \CodeType::PROPERTY_ID . '= c.codeTypeId', 'ct')
            ->where('pc.' . \ProductClassification::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:', [
                'entityTypeId' => Type::CODE
            ])
            ->andWhere('pc.' . \ProductClassification::PROPERTY_PRODUCT_ID . ' = :productId:', [
                'productId' => $this->product->getId()
            ])
            ->andWhere('ct.' . \CodeType::PROPERTY_CODE . ' = :codeType:', [
                'codeType' => \CodeType::PRODUCT_TYPE
            ])
            ->limit(1);

        return $query->getQuery()->execute()->getFirst();
    }

    /**
     * @return \CodeType|EntityInterface
    */
    private function getCodeTypeClassificationType()
    {
        return  $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' =>\CodeType::CLASSIFICATION_TYPE
            ]
        ]);
    }

    /**
     * @return \CodeType|EntityInterface
     */
    private function getCodeTypeAttribute()
    {
        return  $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' =>\CodeType::ATTRIBUTES
            ]
        ]);
    }
}