<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 4/18/19
 * Time: 12:37
 */

namespace Vemid\Service\Product;

use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Application\Di;

/**
 * Class PricelistPrices
 * @package Vemid\Service\Product
 */
class PricelistPrices
{

    /** @var EntityManagerInterface  */
    private $entityManager;

    /**
     * Price constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param \Pricelist $pricelist
     * @return array
     */
    public function getFinalPricesByPriceList(\Pricelist $pricelist,\Product $product) {

        /** @var Di $di */
        $di = Di::getDefault();
        $modelsManager = $di->getModelsManager();

        $query = $modelsManager->createBuilder()
            ->columns([
                'round(pr.purchasePrice,2) purchasePrice',
                'round(pr.wholesalePrice,2) wholesalePrice',
                'round(pr.retailPrice,2) retailPrice',
            ])
            ->addFrom(\ProductPrice::class, 'pr')
            ->andWhere('pr.productId = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('pr.status = :status:',[
                'status' =>\ProductCalculation::STATUS_FINAL
            ])
            ->andWhere('pr.pricelistId = :pricelistId:',[
                'pricelistId' => $pricelist->getId()
            ]);

        return $query->getQuery()->execute();

    }

    /**
     * @param \Pricelist $pricelist
     * @return array
     */
    public function getPlanedPricesByPriceList(\Pricelist $pricelist,\Product $product) {

        /** @var Di $di */
        $di = Di::getDefault();
        $modelsManager = $di->getModelsManager();

        $query = $modelsManager->createBuilder()
            ->columns([
                'round(pr.purchasePrice,2) purchasePrice',
                'round(pr.wholesalePrice,2) wholesalePrice',
                'round(pr.retailPrice,1) retailPrice',
            ])
            ->addFrom(\ProductPrice::class, 'pr')
            ->andWhere('pr.productId = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('pr.status = :status:',[
                'status' =>\ProductCalculation::STATUS_PLANED
            ])
            ->andWhere('pr.pricelistId = :pricelistId:',[
                'pricelistId' => $pricelist->getId()
            ]);

        return $query->getQuery()->execute();

    }

    /**
     * @param \Pricelist $pricelist
     * @param \Product $product
     * @return string
     */
    public function getPurchasePrice(\Pricelist $pricelist,\Product $product) {

        $prices = $this->getFinalPricesByPriceList($pricelist,$product);
        $planedPrices = $this->getPlanedPricesByPriceList($pricelist,$product);

        if(count($prices)) {
            return $prices[0]['purchasePrice'] ;
        }
        elseif (count($planedPrices))
            return $planedPrices[0]['purchasePrice'];
        else
            return null;
    }

    /**
     * @param \Pricelist $pricelist
     * @param \Product $product
     * @return string
     */
    public function getWholesalePrice(\Pricelist $pricelist,\Product $product) {

        $prices = $this->getFinalPricesByPriceList($pricelist,$product);
        $planedPrices = $this->getPlanedPricesByPriceList($pricelist,$product);

        if(count($prices)) {
            return $prices[0]['wholesalePrice'] ;
        }
        elseif (count($planedPrices))
            return $planedPrices[0]['wholesalePrice'];
        else
            return null;
    }

    /**
     * @param \Pricelist $pricelist
     * @param \Product $product
     * @return string
     */
    public function getRetailPrice(\Pricelist $pricelist,\Product $product) {

        $prices = $this->getFinalPricesByPriceList($pricelist,$product);
        $planedPrices = $this->getPlanedPricesByPriceList($pricelist,$product);

        if(count($prices)) {
            return $prices[0]['retailPrice'] ;
        }
        elseif (count($planedPrices))
            return $planedPrices[0]['retailPrice'];
        else
            return null;
    }

}