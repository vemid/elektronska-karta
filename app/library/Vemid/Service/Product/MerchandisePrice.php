<?php

namespace Vemid\Service\Product;

use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;
use Phalcon\Mvc\Model\ManagerInterface;

/**
 * Class MerchandisePrice
 * @package Vemid\Service\Product
 */
class MerchandisePrice
{
    /** @var \MerchandiseCalculationItem */
    private $merchandiseCalculationItem;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ManagerInterface */
    private $modelsManager;

    /**
     * Price constructor.
     * @param \MerchandiseCalculationItem $merchandiseCalculationItem
     * @param EntityManagerInterface $entityManager
     * @param ManagerInterface $modelsManager
     */
    public function __construct(EntityManagerInterface $entityManager, \MerchandiseCalculationItem $merchandiseCalculationItem, $modelsManager)
    {
        $this->entityManager = $entityManager;
        $this->merchandiseCalculationItem = $merchandiseCalculationItem;
        $this->modelsManager = $modelsManager;
    }

    /**
     * @throws \LogicException|\DomainException
     */
    public function addPrices()
    {
        /** @var \Pricelist[] $priceLists */
        $priceLists = $this->entityManager->find(\Pricelist::class);

        foreach ($priceLists as $priceList) {
            $this->addPrice($priceList, $this->merchandiseCalculationItem);
        }
    }

    /**
     * @param \Product $product
     * @return array
     */
    public function getPrices(\Product $product)
    {
        $query = $this->modelsManager->createBuilder()
            ->columns([
                'p.name',
                'round(pr.purchasePrice,2) purchasePrice',
                'round(pr.wholesalePrice,2) wholesalePrice',
                'round(pr.retailPrice,1) retailPrice',
            ])
            ->addFrom(\ProductPrice::class, 'pr')
            ->leftJoin(\Pricelist::class, 'pr.pricelistId = p.id', 'p')
            ->andWhere('pr.productId = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('pr.status = :status:', [
                'status' => \ProductPrice::STATUS_FINAL
            ])
            ->groupBy('p.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param \Product $product
     * @return array
     */
    public function getSerbianPrices(\Product $product)
    {
        $query = $this->modelsManager->createBuilder()
            ->columns([
                'p.name',
                'round(pr.purchasePrice,2) purchasePrice',
                'round(pr.wholesalePrice,2) wholesalePrice',
                'round(pr.retailPrice,1) retailPrice',
            ])
            ->addFrom(\ProductPrice::class, 'pr')
            ->leftJoin(\Pricelist::class, 'pr.pricelistId = p.id', 'p')
            ->andWhere('pr.productId = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('pr.status = :status:', [
                'status' => \ProductPrice::STATUS_FINAL
            ])
            ->andWhere('p.code = :pricelist:',[
                'pricelist' => '01/140000001'
            ])
            ->groupBy('p.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param \Product $product
     * @return array
     */
    public function getWholesalePrices(\Product $product)
    {
        $query = $this->modelsManager->createBuilder()
            ->columns([
                'p.name',
                'round(pr.purchasePrice,2) purchasePrice',
                'round(pr.wholesalePrice,2) wholesalePrice',
                'round(pr.retailPrice,1) retailPrice',
            ])
            ->addFrom(\ProductPrice::class, 'pr')
            ->leftJoin(\Pricelist::class, 'pr.pricelistId = p.id', 'p')
            ->andWhere('pr.productId = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('pr.status = :status:', [
                'status' => \ProductPrice::STATUS_FINAL
            ])
            ->andWhere('p.code = :pricelist:',[
                'pricelist' => '01/140000002'
            ])
            ->groupBy('p.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param \Product $product
     * @return array
     */
    public function getMontenegroPrices(\Product $product)
    {
        $query = $this->modelsManager->createBuilder()
            ->columns([
                'p.name',
                'round(pr.purchasePrice,2) purchasePrice',
                'round(pr.wholesalePrice,2) wholesalePrice',
                'round(pr.retailPrice,1) retailPrice',
            ])
            ->addFrom(\ProductPrice::class, 'pr')
            ->leftJoin(\Pricelist::class, 'pr.pricelistId = p.id', 'p')
            ->andWhere('pr.productId = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('pr.status = :status:', [
                'status' => \ProductPrice::STATUS_FINAL
            ])
            ->andWhere('p.code = :pricelist:',[
                'pricelist' => '03/160000001'
            ])
            ->groupBy('p.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param \Product $product
     * @return array
     */
    public function getBosnianPrices(\Product $product)
    {
        $query = $this->modelsManager->createBuilder()
            ->columns([
                'p.name',
                'round(pr.purchasePrice,2) purchasePrice',
                'round(pr.wholesalePrice,2) wholesalePrice',
                'round(pr.retailPrice,1) retailPrice',
            ])
            ->addFrom(\ProductPrice::class, 'pr')
            ->leftJoin(\Pricelist::class, 'pr.pricelistId = p.id', 'p')
            ->andWhere('pr.productId = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('pr.status = :status:', [
                'status' => \ProductPrice::STATUS_FINAL
            ])
            ->andWhere('p.code = :pricelist:',[
                'pricelist' => '60/160000001'
            ])
            ->groupBy('p.id');

        return $query->getQuery()->execute();
    }

    /**
     * @param float $number
     * @return float
     */
    public function calculateCgRetailPrice(float $number)
    {
        $x = $number * 2 * 1.1;
        if (fmod($x,1) < 0.25)
            $x = floor($x)-0.1;
        elseif (fmod($x,1) >= 0.25 && fmod($x,1) < 0.75)
            $x = floor($x)+0.5;
        else $x = ceil($x)-0.1;

        return $x;
    }

    /**
     * @param \Pricelist $pricelist
     * @param \MerchandiseCalculationItem $items
     * @param $total
     * @throws \DomainException
     */
    private function addPrice(\Pricelist $pricelist, $items)
    {
        $items->getWholesalePrice();

        $wholesaleEurPrice = round($items->getWholesalePrice() / $items->getCourse(),3);

        switch ($pricelist->getCode()) {
            case '01/140000001':
                $purchasePrice = round($items->getPurchasePrice(), 3);
                $wholesalePrice = round($items->getWholesalePrice(), 3);
                $retailPrice = round($items->getRetailPrice());
                break;
            case '60/160000001':
                $purchasePrice = round($items->getPurchasePrice() / $items->getCourse() * 2, 3);
                $wholesalePrice = round($wholesaleEurPrice * 2.2, 3);
                $retailPrice = round($wholesaleEurPrice * 4.4);
                break;
            case '01/140000002':
                $purchasePrice = round($items->getPurchasePrice() / $items->getCourse(), 3);
                $wholesalePrice = round($wholesaleEurPrice, 3);
                $retailPrice = round($items->getRetailPrice() / $items->getCourse());
                break;
            case '03/160000001':
                $purchasePrice = round($items->getPurchasePrice() / $items->getCourse(), 3);
                $wholesalePrice = round($items->getWholesalePrice() / $items->getCourse(), 3);
                $retailPrice = $this->calculateCgRetailPrice($wholesaleEurPrice);
                break;
            default:
                $purchasePrice = 0;
                $wholesalePrice = 0;
                $retailPrice = 0;
                break;
        }

        if ($purchasePrice && $wholesalePrice && $retailPrice) {
            /** @var \ProductPrice $productPrice */
            $productPrice = $this->entityManager->findOne(\ProductPrice::class, [
                \ProductPrice::PROPERTY_PRODUCT_ID . ' = :productId: AND ' .
                \ProductPrice::PROPERTY_PRICELIST_ID . ' = :priceListId: AND ' .
                \ProductPrice::PROPERTY_STATUS . ' = :status:',
                'bind' => [
                    'productId' => $items->getProductId(),
                    'priceListId' => $pricelist->getId(),
                    'status' => \ProductPrice::STATUS_FINAL
                ]
            ]);

            if (!$productPrice) {
                $productPrice = new \ProductPrice();
                $productPrice->setProduct($items->getProduct());
                $productPrice->setPricelist($pricelist);
                $productPrice->setCreated((new DateTime())->getUnixFormat());
            }

            $productPrice->setPurchasePrice($purchasePrice);
            $productPrice->setWholesalePrice($wholesalePrice);
            $productPrice->setRetailPrice($retailPrice);
            $productPrice->setStatus(\ProductPrice::STATUS_FINAL);
            $productPrice->setEdited((new DateTime())->getUnixFormat());

            if (!$this->entityManager->save($productPrice)) {
                throw new \DomainException('Price failed to save!');
            }
        }

    }
}
