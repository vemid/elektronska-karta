<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 12/17/18
 * Time: 14:10
 */

namespace Vemid\Service\Product;

use Phalcon\Mvc\Model\ManagerInterface;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Task\Products\ExportProductCalculation;
use Vemid\Task\Products\ExportProductColor;
use Vemid\Task\Products\ExportProductPresentation;


class ProductData
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ManagerInterface */
    private $modelsManager;

    /** @var array $seasonCode */
    private $seasonCode;

    /** array $users */
    private $users;

    /** string $fileName */
    private $fileName;

    /**
     * ProductsPrinter constructor.
     * @param EntityManagerInterface $entityManager
     * @param ManagerInterface $manager
     * @param array $seasonCode
     * @param array $users
     */
    public function __construct(EntityManagerInterface $entityManager, ManagerInterface $manager, $seasonCode, $users,$fileName)
    {
        $this->entityManager = $entityManager;
        $this->modelsManager = $manager;
        $this->seasonCode = $seasonCode;
        $this->users = $users;
        $this->fileName = $fileName;
    }

    public function exportData()
    {
        $tempFolder = rtrim(sys_get_temp_dir(), '/') . '/products-color';
        if (!is_dir($tempFolder) && !mkdir($tempFolder, 0700)) {
            throw new \LogicException('Dir can not be created!');
        }
        $query = $this->modelsManager->createBuilder()
            ->from(\User::class)
            ->where('id IN ({bindParam:array})',
                [
                    'bindParam' => $this->users
                ]);

        $users = $query->getQuery()->execute();

        $emails =array();
        /** @var \User $user */
        foreach ($users as $user) {
            $emails[$user->getEmail()] = $user->getDisplayName();
        }

        $task = new ExportProductColor();
        $task->users = $emails;
        $task->seasonCode = $this->seasonCode;
        $task->fileName = $this->fileName;
        $task->runInBackground();
    }


    public function exportPresentationData()
    {
        $tempFolder = rtrim(sys_get_temp_dir(), '/') . '/products-color';
        if (!is_dir($tempFolder) && !mkdir($tempFolder, 0700)) {
            throw new \LogicException('Dir can not be created!');
        }
        $query = $this->modelsManager->createBuilder()
            ->from(\User::class)
            ->where('id IN ({bindParam:array})',
                [
                    'bindParam' => $this->users
                ]);

        $users = $query->getQuery()->execute();

        $emails =array();
        /** @var \User $user */
        foreach ($users as $user) {
            $emails[$user->getEmail()] = $user->getDisplayName();
        }

        $task = new ExportProductPresentation();
        $task->users = $emails;
        $task->seasonCode = $this->seasonCode;
        $task->fileName = $this->fileName;
        $task->runInBackground();
    }

    public function exportCalculation()
    {
        $tempFolder = rtrim(sys_get_temp_dir(), '/') . '/products-calculation';
        if (!is_dir($tempFolder) && !mkdir($tempFolder, 0700)) {
            throw new \LogicException('Dir can not be created!');
        }
        $query = $this->modelsManager->createBuilder()
            ->from(\User::class)
            ->where('id IN ({bindParam:array})',
                [
                    'bindParam' => $this->users
                ]);

        $users = $query->getQuery()->execute();

        $emails =array();
        /** @var \User $user */
        foreach ($users as $user) {
            $emails[$user->getEmail()] = $user->getDisplayName();
        }

        $task = new ExportProductCalculation();
        $task->users = $emails;
        $task->seasonCode = $this->seasonCode;
        $task->fileName = $this->fileName;
        $task->runInBackground();
    }
}
