<?php

namespace Vemid\Service\Product;


use Vemid\Date\DateTime;

class TimeLine
{
    /** @var \Product  */
    private $product;

    /**
     * Timeline constructor.
     * @param \Product $product
     */
    public function __construct(\Product $product)
    {
        $this->product = $product;
    }

    public function getAll()
    {
        $timeLine = [];
        $procuctCreatedDate = $this->product->getCreated();

        $timeLine[$procuctCreatedDate->getShort()][$procuctCreatedDate->getTime()] = [
            'entity' => \Product::class,
            'action' => 'active',
            'text' => 'Kreiran Proizvod',
            'user' => $this->product->getUser() ? $this->product->getUser()->getDisplayName() : 'NN'
        ];


        $timeLineArray = array_merge_recursive($timeLine,$this->getElectronCreatedCardTimeline(),$this->getElectronClosedCardTimeline());
        //$timeLine += $this->getElectroncCardTimeline();
        krsort($timeLineArray);
        foreach ($timeLineArray as $date => $array) {
            krsort($array);
            $timeLineArray[$date] = $array;
        }

        return $timeLineArray;
    }

    private function getElectronCreatedCardTimeline()
    {
        $timeline = [];


        foreach ($this->product->getElectronCards() as $electronCard) {
            $designer = $electronCard->getEntityLogOnCreate() ? $electronCard->getEntityLogOnCreate()->getUser() : '';
            $dateTime = $electronCard->getCreated();
            $timeline[$dateTime->getShort()][$dateTime->getTime()] = [
                'entity' => \ElectronCard::class,
                'action' => $electronCard->getActive() ? 'active' : 'nonActive',
                'text' => 'Kreirana El. Karta',
                'user' => $electronCard->getEntityLogOnCreate() ? $electronCard->getEntityLogOnCreate()->getUser() : ''
            ];
        }

        return $timeline;
    }

    private function getElectronClosedCardTimeline()
    {
        $timeline = [];
        $defaultDate = new DateTime();

        foreach ($this->product->getElectronCards() as $electronCard) {
            if($electronCard->getFinished()) {
                $designer = $electronCard->getEntityLogOnCreate() ? $electronCard->getEntityLogOnCreate()->getUser() : '';
                $dateTime = $electronCard->getCardClosedTime() ? $electronCard->getCardClosedTime() : $defaultDate;
                $timeline[$dateTime->getShort()][$dateTime->getTime()] = [
                    'entity' => \ElectronCard::class,
                    'action' => $electronCard->getActive() ? 'active' : 'nonActive',
                    'text' => 'Zatvorena El. Karta',
                    'user' => $electronCard->getEntityLogOnCreate() ? $electronCard->getEntityLogOnCreate()->getUser() : ''
                ];
            }
        }

        return $timeline;
    }

    public function getQuestionnaireTimeline()
    {

    }
}