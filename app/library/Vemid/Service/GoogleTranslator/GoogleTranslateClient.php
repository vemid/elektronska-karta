<?php


namespace Vemid\Service\GoogleTranslator;

use Google\Cloud\Translate\V2\TranslateClient;
use Phalcon\Config;
use Vemid\Application\Di;
use Vemid\Entity\Manager\EntityManagerInterface;


/**
 * Class GoogleTranslateClient
 * @package Vemid\Service\GoogleTranslator
 */
class GoogleTranslateClient
{

    /** @var GoogleTranslateClient */
    private $translateClient;

    /**
     * GoogleTranslateClient constructor.
     */
    public function __construct()
    {

        /** @var Di $di */
        $di = Di::getDefault();
        $config = $di->getConfig();
        $file = $config->googleStorage->key;

        $this->translateClient = new TranslateClient([
            'keyFile' => json_decode(file_get_contents($file), true),
        ]);

    }

    public function getTranslated($text,$targetLanguage) {

        $translate = $this->translateClient;

        if(!$targetLanguage) {
            $targetLanguage = 'en';
        }

        $string = str_replace('_', ' ',  $text);

        $result = $translate->translate($string, [
            'source' => 'sr',
            'target' => $targetLanguage,

        ]);

        return $result['text'];

    }

}