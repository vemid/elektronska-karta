<?php

namespace Vemid\Service\Efectus;

use Vemid\Date\DateTime;

interface RepositoryInterface
{
    /**
     * @return array
     */
    public function fetchAll(): array;

    /**
     * @param DateTime $dateTime
     * @return array
     */
    public function fetchByDate(DateTime $dateTime): array;

    /**
     * @param \Code $code
     * @return array
     */
    public function fetchByFromShop(\Code $code): array;

    /**
     * @param \Code $code
     * @return array
     */
    public function fetchByToShop(\Code $code): array;

    /**
     * @param \Code $fromCode
     * @param \Code $toCode
     * @return array
     */
    public function fetchByFromShopAndToShop(\Code $fromCode, \Code $toCode): array;

    /**
     * @param \Product $product
     * @return array
     */
    public function fetchByProductCode(\Product $product): array;

    /**
     * @param \Product $product
     * @param \Code $code
     * @return array
     */
    public function fetchByProductCodeAndSize(\Product $product, \Code $code): array;

    /**
     * @param DateTime $dateTime
     * @return array
     */
    public function fetchEfectusLog(DateTime $dateTime): array;
}