<?php

namespace Vemid\Service\Efectus\Db;

use Phalcon\Config;
use Phalcon\Db\Adapter\Pdo\Mysql;

class Connection implements ConnectionInterface
{
    /** @var Config */
    private $config;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @return Mysql
     */
    public function __invoke(): Mysql
    {
        return new Mysql($this->config->dbEfectus->toArray());
    }
}