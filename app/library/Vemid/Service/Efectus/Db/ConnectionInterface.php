<?php

namespace Vemid\Service\Efectus\Db;

use Phalcon\Db\Adapter\Pdo\Mysql;

interface ConnectionInterface
{
    public function __invoke(): Mysql;
}