<?php

namespace Vemid\Service\Efectus\Notification;

use Phalcon\Config;
use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Entity\Type;
use Vemid\Helper\Crypto;
use Vemid\Mailer\ManagerInterface;
use Vemid\Task\Excel\EfectusNalog;
use Vemid\Task\Excel\ProductsExportTask;

class Mailer
{
    /** @var ManagerInterface */
    private $manager;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var Config */
    private $config;

    /**
     * @param ManagerInterface $manager
     * @param EntityManagerInterface $entityManager
     * @param Config $config
     */
    public function __construct(ManagerInterface $manager, EntityManagerInterface $entityManager, Config $config)
    {
        $this->manager = $manager;
        $this->entityManager = $entityManager;
        $this->config = $config;
    }

    public function __invoke(DateTime $dateTime)
    {
        $toDate = new DateTime($dateTime->getUnixFormat());
        $toDate->modify('+6 days');

        /** @var \Efectus[] $efectus */
        $efectus = $this->entityManager->find(\Efectus::class, [
            \Efectus::PROPERTY_DATE . ' BETWEEN :start: AND :end:',
            'bind' => [
                'start' => $dateTime->getUnixFormat(),
                'end' => $toDate->getUnixFormat(),
            ]
        ]);

        $data = [];
        foreach ($efectus as $row) {
            $data[$row->getFromCodeId()][] = $row;
        }

        $crypto = new Crypto($this->config);
        foreach ($data as $shopId => $dataPerShop) {
            /** @var \ChannelSupply $channelSupply */
            $channelSupply = $this->entityManager->findOne(\ChannelSupplyEntity::class, [
                \ChannelSupplyEntity::PROPERTY_ENTITY_ID . ' = :entityId: AND ' .
                \ChannelSupplyEntity::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:',
                'bind' => [
                    'entityId' => $shopId,
                    'entityTypeId' => Type::CODE
                ]
            ]);

            if (!$channelSupply || !$channelSupply->getEmail()) {
                throw new \LogicException('Efectus: Can\'t send email cause cannot find in source');
            }

            $task = new EfectusNalog();
            $task->user = $channelSupply->getEmail();
            $task->magacin = $shopId;
            $task->date = $dateTime;
            $task->run();

            $message = $this->manager->createMessageFromView('mail-template/efectus', [
                'data' => $dataPerShop,
                'link' => sprintf(
                    '%s/efectus/view-order/%s/%s',
                    $this->config->application->baseUri,
                    $shopId,
                    $crypto->encrypt($dateTime->getUnixFormat())
                )
            ]);

//            $message->setTo($channelSupply->getEmail());
            $message->setTo('admin@bebakids.com');
            $message->setSubject(sprintf('Efektus report za datum:%s', $dateTime->getShort()));
            return $message->send();
        }
    }
}
