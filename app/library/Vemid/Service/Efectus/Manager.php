<?php

namespace Vemid\Service\Efectus;

use Phalcon\Config;
use Vemid\Application\Di;
use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Helper\Crypto;
use Vemid\Mailer\ManagerInterface;

class Manager
{
    /** @var RepositoryInterface */
    private $repository;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ManagerInterface */
    private $mailer;

    /** @var Config  */
    private $config;

    /**
     * @param EntityManagerInterface $entityManager
     * @param RepositoryInterface $repository
     * @param ManagerInterface $mailer
     * @param Config $config
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        RepositoryInterface $repository,
        ManagerInterface $mailer,
        Config $config
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $repository;
        $this->mailer = $mailer;
        $this->config = $config;
    }

    /**
     * @param DateTime $dateTime
     * @return false|void
     */
    public function __invoke(DateTime $dateTime)
    {
        if (!$this->isEligibleToPull($dateTime)) {
            die();
        }

        $records = $this->repository->fetchByDate($dateTime);
        $shouldNotifyManager = false;
        foreach ($records as $record) {
            if ($record['magacin_izlaza'] === 'MP' || $record['magacin_izlaza'] === 'MR') {
                $record['magacin_izlaza'] = 99;
            }

            if (!$product = $this->findProductByBarcode($record['sif_rob'])) {
                continue;
                throw new \LogicException(sprintf('Product not found:sif_rob in efektus is %s:', $record['sif_rob']));
            }

            if (!$codeSize = $this->findSizeByCode($record['sif_ent_rob'])) {
                continue;
                throw new \LogicException(sprintf('Size not found:sif_ent_rob in efektus is %s:', $record['sif_ent_rob']));
            }

            if (!$productSize = $this->findProductSizeByProductAndCodeSize($product, $codeSize)) {
                throw new \LogicException(sprintf('Product size record not found for barcode %s and size %s', $product->getCode(), $codeSize->getCode()));
            }

            if (!$warehouseInput = $this->fingShopByCode($record['magacin_ulaza'])) {
                throw new \LogicException(sprintf('Shop not found for %s:magacin_ulaza', $record['magacin_ulaza']));
            }

            if (!$warehouseOutput = $this->fingShopByCode($record['magacin_izlaza'])) {
                throw new \LogicException(sprintf('Shop not found for %s:magacin_izlaza', $record['magacin_izlaza']));
            }

            if (!$this->checkIfALreadyStoredInInternalDb($productSize, $warehouseInput, $warehouseOutput)) {
                $efectus = new \Efectus();
                $efectus->setProductSize($productSize);
                $efectus->setCodeFrom($warehouseOutput);
                $efectus->setCodeTo($warehouseInput);
                $efectus->setQty($record['kolicina']);
                $efectus->setTypeS($record['tip']);
                $efectus->setExclude(0);
                $efectus->setDate($dateTime);

                if (!$this->entityManager->save($efectus)) {
                    throw new \LogicException($efectus->getMessages()[0]->getMessage());
                }

                $shouldNotifyManager = true;
            }
        }

        if ($shouldNotifyManager) {

//            $crypto = new Crypto($this->config);
//            $message = $this->mailer->createMessageFromView('mail-template/efectus-weekly-manage', [
//                'link' => sprintf(
//                    '%s/efectus/manage-weekly-orders/%s',
//                    $this->config->application->baseUri,
//                    $crypto->encrypt($dateTime->getUnixFormat())
//                )
//            ]);
//
//            $message->setTo('ubuntu88.vesic@gmail.com');
//            $message->setSubject(sprintf('Efektus import za datum:%s', $dateTime->getShort()));
//            $message->send();

            $di = Di::getDefault();
            $efectusMailer = $di->getEfectusMailer();
            $efectusMailer($dateTime);
        }
    }

    private function isEligibleToPull(DateTime $dateTime)
    {
        return $this->repository->fetchEfectusLog($dateTime);
    }

    /**
     * @param string $barcode
     * @return \Product|null
     */
    private function findProductByBarcode(string $barcode)
    {
        return $this->entityManager->findOne(\Product::class, [
            \Product::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $barcode
            ]
        ]);
    }

    /**
     * @param string $code
     * @return Code|null
     */
    private function findSizeByCode(string $code)
    {
        $codeType = $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::SIZES
            ]
        ]);

        return $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
            \Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'codeTypeId' => $codeType->getEntityId(),
                'code' => $code
            ]
        ]);
    }

    /**
     * @param \Product $product
     * @param \Code $code
     * @return \ProductSize|null
     */
    private function findProductSizeByProductAndCodeSize(\Product $product, \Code $code)
    {
        return $this->entityManager->findOne(\ProductSize::class, [
            \ProductSize::PROPERTY_PRODUCT_ID . ' = :productId: AND ' .
            \ProductSize::PROPERTY_CODE_ID . ' = :codeId:',
            'bind' => [
                'productId' => $product->getId(),
                'codeId' => $code->getId()
            ]
        ]);
    }

    private function fingShopByCode(string $code)
    {
        /** @var \CodeType $codeType */
        $codeType = $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::SHOPS
            ]
        ]);

        return $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
            \Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'codeTypeId' => $codeType->getEntityId(),
                'code' => $code
            ]
        ]);
    }

    private function checkIfALreadyStoredInInternalDb(\ProductSize $productSize, \Code $codeIn, \Code $codeOut)
    {
        return $this->entityManager->findOne(\Efectus::class, [
            \Efectus::PROPERTY_PRODUCT_SIZE_ID . ' = :productSizeId: AND ' .
            \Efectus::PROPERTY_TO_CODE_ID . ' = :codeIdTo: AND ' .
            \Efectus::PROPERTY_FROM_CODE_ID . ' = :codeIdFrom:',
            'bind' => [
                'productSizeId' => $productSize->getId(),
                'codeIdTo' => $codeIn->getId(),
                'codeIdFrom' => $codeOut->getId()
            ]
        ]);
    }
}
