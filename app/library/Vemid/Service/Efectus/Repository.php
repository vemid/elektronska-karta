<?php

namespace Vemid\Service\Efectus;

use Vemid\Date\DateTime;
use Vemid\Service\Efectus\Db\ConnectionInterface;

class Repository implements RepositoryInterface
{
    /** @var \Phalcon\Db\Adapter\Pdo\Mysql */
    private $connection;

    /**
     * @param ConnectionInterface $connection
     */
    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection();
    }

    /**
     * @return array
     */
    public function fetchAll(): array
    {
        $sql = <<<SQL
SELECT * FROM nalog;
SQL;

        return $this->connection->query($sql)->fetchAll();
    }

    /**
     * @param DateTime $dateTime
     * @return array
     */
    public function fetchByDate(DateTime $dateTime): array
    {
        $sql = <<<SQL
SELECT * FROM nalog
WHERE CAST(created AS DATE) = :date;
SQL;

        return $this->connection->query($sql, [
            'date' => $dateTime->format(DateTime::ISO_8601_DATE_ONLY)
        ])->fetchAll();
    }

    /**
     * @param \Code $code
     * @return array
     */
    public function fetchByFromShop(\Code $code): array
    {
        $sql = <<<SQL
SELECT * FROM nalog
WHERE magacin_izlaza = :magacin_izlaza;
SQL;

        return $this->connection->query($sql, [
            'magacin_izlaza' => $code->getCode()
        ])->fetchAll();
    }

    /**
     * @param \Code $code
     * @return array
     */
    public function fetchByToShop(\Code $code): array
    {
        $sql = <<<SQL
SELECT * FROM nalog
WHERE magacin_ulaza = :magacin_ulaza;
SQL;

        return $this->connection->query($sql, [
            'magacin_ulaza' => $code->getCode()
        ])->fetchAll();
    }

    /**
     * @param \Code $fromCode
     * @param \Code $toCode
     * @return array
     */
    public function fetchByFromShopAndToShop(\Code $fromCode, \Code $toCode): array
    {
        $sql = <<<SQL
SELECT * FROM nalog
WHERE magacin_izlaza = :magacin_izlaza 
AND magacin_ulaza = :magacin_ulaza;
SQL;

        return $this->connection->query($sql, [
            'magacin_izlaza' => $fromCode->getCode(),
            'magacin_ulaza' => $toCode->getCode()
        ])->fetchAll();
    }

    /**
     * @param \Product $product
     * @return array
     */
    public function fetchByProductCode(\Product $product): array
    {
        $sql = <<<SQL
SELECT * FROM nalog
WHERE sif_rob = :sif_rob;
SQL;

        return $this->connection->query($sql, [
            'sif_rob' => $product->getCode()
        ])->fetchAll();
    }

    /**
     * @param \Product $product
     * @param \Code $code
     * @return array
     */
    public function fetchByProductCodeAndSize(\Product $product, \Code $code): array
    {
        $sql = <<<SQL
SELECT * FROM nalog
WHERE sif_rob = :sif_rob
AND sif_ent_rob = :sif_ent_rob;
SQL;

        return $this->connection->query($sql, [
            'sif_rob' => $product->getCode(),
            'sif_ent_rob' => $code->getCode()
        ])->fetchAll();
    }

    /**
     * @param DateTime $dateTime
     * @return array
     */
    public function fetchEfectusLog(DateTime $dateTime): array
    {
        $sql = <<<SQL
SELECT * FROM efektus_log
WHERE date(created) = :created limit 1;
SQL;

        return $this->connection->query($sql, [
            'created' => $dateTime->getUnixFormat(),
        ])->fetchAll();
    }
}
