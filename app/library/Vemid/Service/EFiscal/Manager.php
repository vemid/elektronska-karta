<?php

namespace Vemid\Service\EFiscal;

use Vemid\Date\DateTime;
use Vemid\Entity\Manager\EntityManagerInterface;

/**
 * Class Manager
 * @package Vemid\Service\Warrant\Statement
 */
class Manager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var string */
    private $filePath;

    /**
     * Manager constructor.
     * @param EntityManagerInterface $entityManager
     * @param string $filePath
     */
    public function __construct(EntityManagerInterface $entityManager, $filePath)
    {
        $this->entityManager = $entityManager;
        $this->filePath = $filePath;
    }

    /**
     * @throws \RuntimeException|\DomainException
     */
    public function import()
    {
        if (!file_exists($this->filePath)) {
            throw new \RuntimeException('File do not exist');
        }

        $fileContent = file_get_contents($this->filePath);

        $json = json_decode($fileContent, true);

        foreach ($json as $row) {
            if (!$eFiscal = $this->checkAlreadyStored($row['InvoiceNumber'])) {
                $eFiscal = $this->addFiscal($row);
                $this->addFiscalItem($eFiscal, $row['Items']);
            }

            continue;
        }

    }

    /**
     * @param array $eFiscalRow
     * @return \EFiscal
     * @throws \DomainException
     */
    private function addFiscal(array $eFiscalRow): \EFiscal
    {
        $dateTime = DateTime::createFromFormat('d.m.Y. H:i:s', $eFiscalRow['SDCTime_ServerTimeZone']);

        $eFiscal = new \EFiscal();
        $eFiscal->setInvoiceNumber($eFiscalRow['InvoiceNumber']);
        $eFiscal->setCashier($eFiscalRow['Cashier']);
        $eFiscal->setCode($this->findStore($eFiscalRow['SignedBy']));
        $eFiscal->setFiscalTime($dateTime->format("Y-m-d H:i:s"));
        $eFiscal->setInvoiceType($eFiscalRow['InvoiceType']);
        $eFiscal->setTaxAmount($eFiscalRow['TaxItems'][0]['Amount']);
        $eFiscal->setTotalAmount($eFiscalRow['TotalAmount']);
        $eFiscal->setTransactionType($eFiscalRow['TransactionType']);

        if (!$this->entityManager->save($eFiscal)) {
            throw new \DomainException('eFiscal Statement failed to save!');
        }

        return $eFiscal;

    }

    /**
     * @param \EFiscal $eFiscal
     * @param array $eFiscalItem
     * @throws \DomainException
     */
    private function addFiscalItem(\EFiscal $eFiscal, array $eFiscalItem)
    {
        foreach ($eFiscalItem as $item) {

            if (!$this->checkAlreadyStoredItem($eFiscal, $item['Name'])) {
                $eFiscalItem = new \EFiscalItem();
                $eFiscalItem->setEFiscalId($eFiscal->getId());
                $eFiscalItem->setGtin($item['GTIN']);
                $eFiscalItem->setName($item['Name']);
                $eFiscalItem->setQuantity($item['Quantity']);
                $eFiscalItem->setUnitPrice($item['UnitPrice']);

                if (!$this->entityManager->save($eFiscalItem)) {
                    throw new \DomainException('eFical Item failed to save!');
                }
            }

            continue;
        }
    }

    /**
     * @param $invoiceNumber
     * @return null|\Vemid\Entity\EntityInterface|\EFiscal
     */
    private function checkAlreadyStored($invoiceNumber)
    {
        return $this->entityManager->findOne(\EFiscal::class, [
            \EFiscal::PROPERTY_INVOICE_NUMBER . '=  :invoiceNumber: ',
            'bind' => [
                'invoiceNumber' => $invoiceNumber
            ]
        ]);
    }

    /**
     * @param \EFiscal $eFiscal
     * @param $item
     * @return null|\Vemid\Entity\EntityInterface|\EFiscalItem
     */
    private function checkAlreadyStoredItem(\EFiscal $eFiscal, $item)
    {
        return $this->entityManager->findOne(\EFiscalItem::class, [
            \EFiscalItem::PROPERTY_E_FISCAL_ID . '=  :eFiscalId: AND ' .
            \EFiscalItem::PROPERTY_NAME . ' = :item:',
            'bind' => [
                'eFiscalId' => $eFiscal->getId(),
                'item' => $item
            ]
        ]);
    }

    /**
     * @param
     * @param $eFicalStoreCode
     * @return null|\Vemid\Entity\EntityInterface|\Code
     */
    private function findStore($eFicalStoreCode)
    {
        /** @var \CodeType $codeType */
        $codeType = $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::FISCAL_CARD
            ]
        ]);

        if (!$codeType) {
            throw new \DomainException('Code type do not exist!');
        }

        return $this->entityManager->findOne(\Code::class, [
            \Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
            \Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'codeTypeId' => $codeType->getId(),
                'code' => $eFicalStoreCode
            ]
        ]);
    }
}