<?php

namespace Vemid\Console\Command\Message;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;

/**
 * Class ReActivateMessagesCommand
 * @package Vemid\Cosnole\Command\Message
 */
class ReActivateMessagesCommand extends Command
{
    const DAYS = 'DAYS';

    private static $descriptions = [
        self::DAYS => 'Maximalan broj dana koji jedozvoljen da notifikacija bude deaktivirana'
    ];

    private static $defaults = [
        self::DAYS => 20
    ];

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('userNotificationMessages:reactivate-deactivated-messages')
            ->addOption(self::DAYS, null, InputOption::VALUE_OPTIONAL, self::$descriptions[self::DAYS], self::$defaults[self::DAYS])
            ->setDescription('Re activate user deactivated messages');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();
        $userNotificationService = $di->getUserNotification();
        try {
            $userNotificationService->reActivateMessages($input->getOption(self::DAYS));

            $output->writeln('Reaktivirane poruke!');
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }
}