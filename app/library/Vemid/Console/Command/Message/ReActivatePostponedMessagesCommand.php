<?php

namespace Vemid\Console\Command\Message;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;

/**
 * Class ReActivateMessagesCommand
 * @package Vemid\Cosnole\Command\Message
 */
class ReActivatePostponedMessagesCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('userNotificationMessages:reactivate-postponed-messages')
            ->setDescription('Re activate user postponed messages');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();
        $userNotificationService = $di->getUserNotification();
        try {
            $userNotificationService->postponedToActive();

            $output->writeln('Reaktivirane poruke!');
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }
}