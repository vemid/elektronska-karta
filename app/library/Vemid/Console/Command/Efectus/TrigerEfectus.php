<?php

namespace Vemid\Console\Command\Efectus;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Date\DateTime;
use Vemid\Service\MisWsdl\GetMisWarehouseOrderStatus;
use Vemid\Task\WarehouseOrders\SendToMisWarehouseOrderTask;

class TrigerEfectus extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('efectus:triger-efectus')
            ->setDescription('Import warrants from MIS');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();
        try {
            $date = new DateTime();
//            $date = new DateTime('2022-05-09');
            $efectusManager = $di->getEfectusManager();
            $efectus = $efectusManager($date);

            $output->writeln('Efektus pokrenut');
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }
}