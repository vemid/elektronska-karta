<?php

namespace Vemid\Console\Command\OperatingList;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Task\OperatingList\FinishTask;

/**
 * Class FinishCommand
 * @package Vemid\Console\Command\OperatingList
 */
class FinishCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('operatingList:finish')
            ->setDescription('Set finished items');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $finishTask = new FinishTask();
            $finishTask->runInBackground();
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }
}