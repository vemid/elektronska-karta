<?php

namespace Vemid\Console\Command\Storned;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use \Vemid\Service\MisWsdl\StornedFailures;

/**
 * Class PullStornedFailuresCommand
 * @package Vemid\Cosnole\Command\Message
 */
class PullStornedFailuresCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('storned:failures')
            ->setDescription('Import attributes from MIS');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            /** @var Di $di */
            $di = Di::getDefault();
            $material = new StornedFailures($di->getLogger());
            $material->run();

            $output->writeln('Storned failura uradjen!');
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }
}