<?php

namespace Vemid\Console\Command\User;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Helper\HashHelper;

/**
 * Class ReActivateMessagesCommand
 * @package Vemid\Cosnole\Command\Message
 */
class GeneratePasswordCommand extends Command
{
    const PASSWORD = 'pass';

    const USERNAME = 'username';

    private static $descriptions = [
        self::PASSWORD => 'Šifra koja treba da se generišse',
        self::USERNAME => 'User nad kojim treba da se generiše password'
    ];

    private static $defaults = [
        self::PASSWORD => '123456'
    ];

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('user:generate-password')
            ->addOption(self::PASSWORD, null, InputOption::VALUE_REQUIRED, self::$descriptions[self::PASSWORD], self::$defaults[self::PASSWORD])
            ->addOption(self::USERNAME, null, InputOption::VALUE_OPTIONAL, self::$descriptions[self::USERNAME])
            ->setDescription('Generate password using phalcon salt');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();
        $entityManager = $di->getEntityManager();
        $hashHelper = new HashHelper();
        $hash = $hashHelper->generatePasswordHash($input->getOption(self::PASSWORD));

        try {
            if ($username = $input->getOption(self::USERNAME)) {

                /** @var \User $user */
                $user = $entityManager->findOne(\User::class, [
                    \User::PROPERTY_USERNAME . ' = :username:',
                    'bind' => [
                        'username' => $username
                    ]
                ]);

                if ($user) {
                    $user->setPassword($hash);
                    $entityManager->save($user);
                }
            }
            $output->writeln('Šifra: ' .$hash );

        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }
}