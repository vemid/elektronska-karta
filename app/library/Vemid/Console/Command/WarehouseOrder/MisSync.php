<?php

namespace Vemid\Console\Command\WarehouseOrder;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Task\WarehouseOrders\SendToMisWarehouseOrderTask;

class MisSync extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('warehouse-order:mis-sync')
            ->setDescription('Warehouse order synchronisation with MIS');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();

        try {
            $finishTask = new SendToMisWarehouseOrderTask();
            $finishTask->execute();
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }

    }
}