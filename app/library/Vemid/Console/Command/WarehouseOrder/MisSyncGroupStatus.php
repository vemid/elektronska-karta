<?php

namespace Vemid\Console\Command\WarehouseOrder;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Service\MisWsdl\GetMisWarehouseOrderStatus;
use Vemid\Task\WarehouseOrders\SendToMisWarehouseOrderTask;

class MisSyncGroupStatus extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('warehouse-order:group-status')
            ->setDescription('Import warrants from MIS');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();
        try {
            $material = new GetMisWarehouseOrderStatus($di->get('entityManager'), null, null, $di->getLogger());
            $material->run();

            $output->writeln('Provera status uradjena!');
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }
}