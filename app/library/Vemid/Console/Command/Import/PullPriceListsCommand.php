<?php

namespace Vemid\Console\Command\Import;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Service\MisWsdl\ImportPriceLists;

/**
 * Class ReActivateMessagesCommand
 * @package Vemid\Cosnole\Command\Message
 */
class PullPriceListsCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('import:priceLists')
            ->setDescription('Import price list from MIS');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            /** @var Di $di */
            $di = Di::getDefault();

            $material = new ImportPriceLists($di->getLogger());
            $material->run();

            $output->writeln('Import cenovnika uradjen!');
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }
}