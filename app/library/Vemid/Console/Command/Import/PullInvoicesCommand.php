<?php

namespace Vemid\Console\Command\Import;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use \Vemid\Service\MisWsdl\ImportAttributes;
use Vemid\Service\MisWsdl\ImportInvoices;

/**
 * Class PullInvoicesCommand
 * @package Vemid\Cosnole\Command\Message
 */
class PullInvoicesCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('import:invoices')
            ->setDescription('Import invoices from MIS');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            /** @var Di $di */
            $di = Di::getDefault();
            $material = new ImportInvoices($di->getLogger());
            $material->run();

            $output->writeln('Import production orders-a uradjen!');
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }
}