<?php

namespace Vemid\Console\Command\Import;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use \Vemid\Service\MisWsdl\ImportClassifications;

/**
 * Class ReActivateMessagesCommand
 * @package Vemid\Cosnole\Command\Message
 */
class PullClassificationsCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('import:classifications')
            ->setDescription('Import classification from MIS');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            /** @var Di $di */
            $di = Di::getDefault();

            $material = new ImportClassifications($di->getLogger());
            $material->run();

            $output->writeln('Import klasificakacija uradjen!');
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }
}