<?php

namespace Vemid\Console\Command\Import;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use \Vemid\Service\MisWsdl\ImportAttributes;

/**
 * Class ReActivateMessagesCommand
 * @package Vemid\Cosnole\Command\Message
 */
class PullAttributesCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('import:attributes')
            ->setDescription('Import attributes from MIS');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            /** @var Di $di */
            $di = Di::getDefault();
            $material = new ImportAttributes($di->getLogger());
            $material->run();

            $output->writeln('Import atributa uradjen!');
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }
}