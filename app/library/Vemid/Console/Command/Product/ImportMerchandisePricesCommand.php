<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 10/24/18
 * Time: 12:31
 */

namespace Vemid\Console\Command\Product;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Task\Products\ImportClassificationsAndSizes;
use Vemid\Task\Products\ImportMerchandisePrices;

/**
 * Class ImportMerchandisePrices
 * @package Vemid\Console\Command\Product
 */
class ImportMerchandisePricesCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('product:import-merchandise-prices')
            ->setDescription('Import to MIS prices from merchandise');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $finishTask = new ImportMerchandisePrices();
            $finishTask->execute();
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }

}