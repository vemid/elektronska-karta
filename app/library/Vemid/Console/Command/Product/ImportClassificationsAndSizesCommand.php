<?php

namespace Vemid\Console\Command\Product;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Task\Products\ImportClassificationsAndSizes;

/**
 * Class ImportClassificationsAndSizes
 * @package Vemid\Console\Command\Product
 */
class ImportClassificationsAndSizesCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('product:import-classifications-and-sizes')
            ->setDescription('Import to MIS attributes, classifications and sizes');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $finishTask = new ImportClassificationsAndSizes();
            $finishTask->execute();
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }
}