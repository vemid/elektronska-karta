<?php


namespace Vemid\Console\Command\Product;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Task\Products\PrintLabelResult;

/**
 * Class PrintLabelResultCommand
 * @package Vemid\Console\Command\Product
 */
class PrintLabelResultCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('product:print-label-result')
            ->setDescription('Update information about printed labels');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $finishTask = new PrintLabelResult();
            $finishTask->runInBackground();
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }
}