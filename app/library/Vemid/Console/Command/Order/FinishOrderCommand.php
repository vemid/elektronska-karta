<?php

namespace Vemid\Console\Command\Order;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Task\Orders\FinishOrderTask;

class FinishOrderCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('order:finish-order')
            ->setDescription('Closing active order');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();

        try {
            $finishTask = new FinishOrderTask();
            $finishTask->execute();
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }

    }
}