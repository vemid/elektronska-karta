<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 5/23/19
 * Time: 09:21
 */

namespace Vemid\Console\Command\Report;

use Phalcon\Mvc\Model\ManagerInterface;
use PhpParser\Builder\Property;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Date\DateTime;
use Vemid\Task\Excel\CheckinWorkerTask;
use Vemid\Task\Excel\PaymentReportBySectorAndWorkerTask;
use Vemid\Task\Excel\PaymentReportBySectorAndWorkerTypeTask;

/**
 * Class CheckinWorkersCommand
 * @package Vemid\Console\Command\Report
 */
class CheckinWorkersCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('report:checkin-workers')
            ->setDescription('Send complete report');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();

        $dateFrom = new DateTime("first day of last month");
        $dateTo = new DateTime("last day of last month");

        /** @var ManagerInterface $modelsManager */
        $modelsManager = $di->getModelsManager();
        $query = $modelsManager->createBuilder()
            ->addFrom(\ProductionWorker::class, 'pw1')
            ->leftJoin(\ProductionWorker::class, 'pw1.id = pw2.headManagerId','pw2')
            ->where('pw2.breakUp is null')
//            ->andWhere('pw1.id ="224"')
            ->andWhere('pw2.headManagerId is not null')
            ->groupBy('pw2.headManagerId');

        /** @var \CodeType $codeType */
        $codeType = $di->getEntityManager()->findOne(\CodeType::class,[
            \CodeType::PROPERTY_CODE . '= :code:',
            'bind' => [
                'code' => 'PRODUCTION_SECTORS'
            ],
        ]);

        /** @var array $headManagers */
        $headManagers = $query->getQuery()->execute();

        $managers=[];
        $managerSectors=[];
        /** @var \ProductionWorker $headManager */
        foreach ($headManagers as $headManager) {

            $datas =$headManager->getManagerWorkers(["breakUp is null"]);

            /** @var \ProductionWorker $data */
            foreach ($datas as $data) {
                $managers[$headManager->getId()][$data->getSectorCode()->getName()][$data->getId()] = $data;
                $managerSectors[$data->getSectorCode()->getId()] = $data->getSectorCode();

            }

        }

        /** @var \ProductionWorker $manager */
        foreach ($headManagers as $manager) {
            try {
                $finishTask = new CheckinWorkerTask();
                $finishTask->manager = $manager;
                $finishTask->managerData = $managers[$manager->getId()];
                $finishTask->sectors = $managerSectors;
                $finishTask->dateFrom = $dateFrom;
                $finishTask->dateTo = $dateTo;
                $finishTask->run();
            } catch (\Exception $ex) {
                $output->writeln("<alert>$ex</alert>");
            }
        }


    }

}