<?php


namespace Vemid\Console\Command\Report;

use Phalcon\Mvc\Model\ManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Date\DateTime;
use Vemid\Task\Report\DealershipPaymentReportTask;
use Vemid\Task\Report\FailureReportTask;


/**
 * Class FailureReportCommand
 * @package Vemid\Console\Command\Report
 */
class FailureReportCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('report:failure-report')
            ->setDescription('Send report');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();

        try {
            $failureTask = new FailureReportTask();
            $failureTask->execute();
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }

    }
}