<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 5/23/19
 * Time: 09:21
 */

namespace Vemid\Console\Command\Report;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Date\DateTime;
use Vemid\Task\Excel\PaymentReportBySectorAndWorkerTask;
use Vemid\Task\Excel\PaymentReportBySectorAndWorkerTypeTask;

/**
 * Class PaymentReportBySectorAndWorkerTypeCommand
 * @package Vemid\Console\Command\Report
 */
class PaymentReportBySectorAndWorkerTypeCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('report:payment-report-by-sector-and-worker-type')
            ->setDescription('Send complete report');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();

        $sectors = [
            'Sivara-pletenina',
            'Sivara-statika',
            'Sivara-statika 2'
        ];

        /** @var \Code[] $sector */
        $sector = $di->getEntityManager()->find(\Code::class, [
            \Code::PROPERTY_NAME . ' IN ({codeName:array})',
            'bind' => [
                'codeName' => $sectors
            ]
        ]);



        $dateFrom = new DateTime('2019-01-01');
        $dateTo = new DateTime();

        try {
            $finishTask = new PaymentReportBySectorAndWorkerTypeTask();
            $finishTask->sector = $sector;
            $finishTask->dateFrom = $dateFrom;
            $finishTask->dateTo = $dateTo;
            $finishTask->runInBackground();
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }


    }

}