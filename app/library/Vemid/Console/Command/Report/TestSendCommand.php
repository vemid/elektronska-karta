<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 1/28/19
 * Time: 15:58
 */

namespace Vemid\Console\Command\Report;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Date\DateTime;
use Vemid\Task\Report\TestSendTask;
use Vemid\Task\Report\WeeklyPaymentReportTask;

/**
 * Class TestSendCommand
 * @package Vemid\Console\Command\Report
 */

class TestSendCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('report:test-send')
            ->setDescription('Send test email');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();

        try {
            $finishTask = new TestSendTask();
            $finishTask->execute();
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }

}