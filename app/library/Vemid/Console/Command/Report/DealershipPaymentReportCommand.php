<?php

namespace Vemid\Console\Command\Report;

use Phalcon\Mvc\Model\ManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Date\DateTime;
use Vemid\Task\Report\DealershipPaymentReportTask;


/**
 * Class SendReportCommand
 * @package Vemid\Console\Command\Report
 */
class DealershipPaymentReportCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('report:dealership-payment-report')
            ->setDescription('Send report');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();

        /** @var ManagerInterface $modelsManager */
        $modelsManager = $di->getModelsManager();
        $query = $modelsManager->createBuilder()
            ->columns(['dealershipId'])
            ->addFrom(\DealershipWarrant::class, 'dw')
            ->where('status = 0')
            ->andWhere('date <= :date:', [
                'date' => (new DateTime())->modify('-5 days')
            ])
            ->groupBy('dw.dealershipId');

        /** @var array $dealershipWarrants */
        $dealershipWarrants = $query->getQuery()->execute();


        foreach ($dealershipWarrants as $dealership) {

            /** @var \Dealership $dealershipObject */
            $dealershipObject = $di->getEntityManager()->findOne(\Dealership::class, [
                \Dealership::PROPERTY_ID . ' = :dealershipId:',
                'bind' => [
                    'dealershipId' => $dealership['dealershipId']
                ]
            ]);

//            if (!$dealerships) {
//                throw new \LogicException(sprintf('Dealship (%s) not found!', $dealership));
//            }

            try {
                $finishTask = new DealershipPaymentReportTask();
                $finishTask->dealership = $dealershipObject;
                $finishTask->execute();
            } catch (\Exception $ex) {
                $output->writeln("<alert>$ex</alert>");
            }
        }

    }
}