<?php

namespace Vemid\Console\Command\Report;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Task\Report\FailureReportTask;
use Vemid\Task\Report\PackedQuantityTask;
use Vemid\Task\Report\ProductionOrderMismatchTask;

/**
 * Class ProductionOrderMismatchCommand
 * @package Vemid\Console\Command\Report
 */
class PackedQuantityCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('report:packed-quantity')
            ->setDescription('Send report');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();

        try {
            $failureTask = new PackedQuantityTask();
            $failureTask->execute();
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }

    }
}