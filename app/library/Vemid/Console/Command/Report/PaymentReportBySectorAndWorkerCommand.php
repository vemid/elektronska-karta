<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 5/23/19
 * Time: 09:21
 */

namespace Vemid\Console\Command\Report;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Date\DateTime;
use Vemid\Task\Excel\PaymentReportBySectorAndWorkerTask;

/**
 * Class PaymentReportBySectorAndWorkerCommand
 * @package Vemid\Console\Command\Report
 */
class PaymentReportBySectorAndWorkerCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('report:payment-report-by-sector-and-worker')
            ->setDescription('Send complete report');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();

        $sectors = [
            'Sivara-pletenina',
            'Sivara-statika',
            'Sivara-statika 2',
            'Pegla',
            'Pakovanje',
            'Dorada',
            'Kontrola'
        ];

        /** @var \Code[] $sector */
        $sector = $di->getEntityManager()->find(\Code::class, [
            \Code::PROPERTY_NAME . ' IN ({codeName:array})',
            'bind' => [
                'codeName' => $sectors
            ]
        ]);



        $dateFrom = new DateTime('2019-01-01');
        $dateTo = new DateTime();

        try {
            $finishTask = new PaymentReportBySectorAndWorkerTask();
            $finishTask->sector = $sector;
            $finishTask->dateFrom = $dateFrom;
            $finishTask->dateTo = $dateTo;
            $finishTask->runInBackground();
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }


    }

}