<?php

namespace Vemid\Console\Command\Report;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Date\DateTime;
use Vemid\Task\Report\WeeklyPaymentReportTask;

/**
 * Class SendReportCommand
 * @package Vemid\Console\Command\Report
 */
class WeeklyPaymentReportCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('report:weekly-payment-report')
            ->setDescription('Send report');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();

        $sectors = [
          'Sivara-pletenina',
          'Sivara-statika',
          'Sivara-statika 2'
        ];

        foreach ($sectors as $sector) {

            /** @var \Code $sector */
            $sector = $di->getEntityManager()->findOne(\Code::class, [
                \Code::PROPERTY_NAME . ' = :codeName:',
                'bind' => [
                    'codeName' => $sector
                ]
            ]);

            if (!$sector) {
                throw new \LogicException(sprintf('Sector (%s) not found!', $sector));
            }

            try {
                $finishTask = new WeeklyPaymentReportTask();
                $finishTask->sector = $sector;
                $finishTask->execute();
            } catch (\Exception $ex) {
                $output->writeln("<alert>$ex</alert>");
            }
        }

    }
}