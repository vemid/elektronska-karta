<?php
/**
 * Created by PhpStorm.
 * User: markovesic
 * Date: 9/15/18
 * Time: 23:13
 */

namespace Vemid\Console\Command\Checkins;

use GuzzleHttp\ClientInterface;
use Phalcon\Config;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Service\Workers\ImportCheckins;
use Vemid\Application\Di;

class ImportCheckinsCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('checkins:import-checkins')
            ->setDescription('Import checkins data');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();
        $config = new Config();

        try {
            $material = new ImportCheckins($di->get('entityManager'), $di->getConfig(), null, null);
            $material->import();

            $output->writeln('Import checkin podataka uradjen!');
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }

}