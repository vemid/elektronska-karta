<?php

namespace Vemid\Console\Command\OperatingListItem;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Task\OperatingListItem\FinishTask;

/**
 * Class FinishCommand
 * @package Vemid\Console\Command\OperatingListItem
 */
class FinishCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('operatingListItem:finish')
            ->setDescription('Set finished items');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $finishTask = new FinishTask();
            $finishTask->runInBackground();
        } catch (\Exception $ex) {
            $output->writeln("<alert>$ex</alert>");
        }
    }
}