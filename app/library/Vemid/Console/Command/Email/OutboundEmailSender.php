<?php

namespace Vemid\Console\Command\Email;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Vemid\Application\Di;
use Vemid\Date\DateTime;
use Vemid\Task\Emails\SendOutboundEmails;

/**
 * Class OutboundEmailSender
 * @package Vemid\Console\Command\Email
 */
class OutboundEmailSender extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('email:outbound-email-sender')
            ->setDescription('Sends un-sent outbound emails');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Di $di */
        $di = Di::getDefault();

        $entityManager = $di->getEntityManager();
        $mailManager = $di->getMailManager();

        /** @var \OutboundEmail[] $outboundEmails */
        $outboundEmails = $entityManager->find(\OutboundEmail::class, [
            \OutboundEmail::PROPERTY_SENT_DATETIME . ' IS NULL',
        ]);

        foreach ($outboundEmails as $outboundEmail) {
            $task = new SendOutboundEmails();
            $task->outboundEmail = $outboundEmail;
            $task->mailManager = $mailManager;
            $task->runInBackground();
        }
    }
}
