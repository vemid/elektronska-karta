<?php

namespace Vemid\Console;

use \Vemid\Helper\DirectoryHelper;
use Symfony\Component\Console\Command\Command;

/**
 * Class Application
 *
 * @package Vemid\Console
 */
class Application extends \Symfony\Component\Console\Application
{
    /**
     * @param string $version
     */
    public function __construct($version)
    {
        parent::__construct('Vemid Console', $version);

        $this->addCommands($this->getCommands());
    }

    /**
     * @return array
     */
    protected function getCommands()
    {
        $commands = [];
        $dir = __DIR__ . DIRECTORY_SEPARATOR . 'Command';

        foreach (DirectoryHelper::listClasses($dir, true) as $className) {
            if (!class_exists($className)) {
                continue;
            }

            $command = new $className;
            if (!$command instanceof Command) {
                continue;
            }

            $commands[] = $command;
        }

        return $commands;
    }
}
