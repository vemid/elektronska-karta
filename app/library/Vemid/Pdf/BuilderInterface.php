<?php

namespace Vemid\Pdf;

/**
 * Interface BuilderInterface
 * @package Vemid\Pdf
 */
interface BuilderInterface
{
    /**
     * @param string $html
     * @param string $orientation
     * @return string
     */
    public function render($html, $orientation = 'portrait');

    /**
     * @param mixed $width
     * @param mixed $height
     */
    public function setPageSize($width, $height);

    /**
     * @param string $marginTop
     * @param string $marginRight
     * @param string $marginBottom
     * @param string $marginLeft
     */
    public function setMargins($marginTop, $marginRight, $marginBottom, $marginLeft);
}