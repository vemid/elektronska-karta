<?php


namespace Vemid\Pdf\Renderer;


use Vemid\Pdf\RendererTrait;

class TcPdf implements RendererInterface
{

    use RendererTrait;

    /**
     * Creates PDF document from the content
     *
     * @param string $content
     * @param string $orientation
     * @return string
     */
    public function create($content, $orientation)
    {
        if ($orientation === 'landscape') {
            $orientation = 'L';
        } else {
            $orientation = 'P';
        }

        $tcpdf = new \TCPDF($orientation, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $tcpdf->SetCreator(PDF_CREATOR);
        $tcpdf->SetAuthor('Bebakids');
        $tcpdf->SetTitle('');
        $tcpdf->SetMargins($this->marginLeft, $this->marginTop, $this->marginRight);
        $tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $tcpdf->AddPage();
//        $tcpdf->writeHTML($content, true, false, true, false, '');
        $tcpdf->writeHTML($content,true, false,false,false,'left');

        return $tcpdf->Output($content, 'I');
    }

    /**
     * Sets page size
     *
     * @param string $width
     * @param string $height
     */
    public function setPageSize($width, $height)
    {
        $this->setPageWidth($width);
        $this->setPageHeight($height);
    }

    /**
     * Sets documents margins
     *
     * @param string $marginTop
     * @param string $marginRight
     * @param string $marginBottom
     * @param string $marginLeft
     */
    public function setMargins($marginTop, $marginRight, $marginBottom, $marginLeft)
    {
        $this->setMarginTop($marginTop);
        $this->setMarginRight($marginRight);
        $this->setMarginBottom($marginBottom);
        $this->setMarginLeft($marginLeft);
    }
}
