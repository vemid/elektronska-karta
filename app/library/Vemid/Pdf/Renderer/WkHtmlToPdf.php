<?php

namespace Vemid\Pdf\Renderer;

use Vemid\Application\Di;
use Vemid\Pdf\RendererTrait;
use Knp\Snappy\Pdf as Snappy;

/**
 * Class WkHtmlToPdf
 *
 * @package Vemid\Pdf\Renderer
 * @author Vemid
 */
class WkHtmlToPdf implements RendererInterface
{
    use RendererTrait;

    /**
     * @param string $content
     * @param string $orientation
     * @return string
     * @throws \InvalidArgumentException
     */
    public function create($content, $orientation = 'portrait')
    {
        /** @var Di $di */
        $di = Di::getDefault();
        $session = $di->getSession();

        $snappy = new Snappy();
        if (stripos(PHP_OS, 'linux') !== false) {
            $snappy->setBinary(APP_PATH . 'vendor/bin/wkhtmltopdf-amd64 --enable-local-file-access');
        } else {
            $snappy->setBinary('/usr/local/bin/wkhtmltopdf --enable-local-file-access');
        }

        //$snappy->setOption('cookie', [$session->getName() => $session->getId()]);
        $snappy->setOption('page-width', $this->pageWidth);
        $snappy->setOption('page-width', $this->pageWidth);
        $snappy->setOption('page-height', $this->pageHeight);
        $snappy->setOption('margin-top', $this->marginTop);
        $snappy->setOption('margin-bottom', $this->marginBottom);
        $snappy->setOption('margin-right', $this->marginRight);
        $snappy->setOption('margin-left', $this->marginLeft);
        $snappy->setOption('print-media-type', true);
        //$snappy->setOption('dpi', '96');
        $snappy->setOption('orientation', $orientation);
        $snappy->setOption('enable-forms',true);
        $snappy->setOption('disable-smart-shrinking', true);
        $snappy->setTimeout(1200);

        return $snappy->getOutputFromHtml($content);
    }

    /**
     * Sets page size
     *
     * @param string $width
     * @param string $height
     */
    public function setPageSize($width, $height)
    {
        $this->setPageWidth($width);
        $this->setPageHeight($height);
    }

    /**
     * Sets documents margins
     *
     * @param string $marginTop
     * @param string $marginRight
     * @param string $marginBottom
     * @param string $marginLeft
     */
    public function setMargins($marginTop, $marginRight, $marginBottom, $marginLeft)
    {
        $this->setMarginTop($marginTop);
        $this->setMarginRight($marginRight);
        $this->setMarginBottom($marginBottom);
        $this->setMarginLeft($marginLeft);
    }
}
