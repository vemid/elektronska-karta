<?php

namespace Vemid\Pdf;

use Vemid\Pdf\Renderer\RendererInterface;
use Vemid\Pdf\Renderer\WkHtmlToPdf;

/**
 * Class Creator
 *
 * @package Vemid\Pdf
 * @author Vemid
 */
class Builder implements BuilderInterface
{
    /** @var RendererInterface */
    private $renderer;

    /**
     * Builder constructor.
     * @param RendererInterface|null $renderer
     */
    public function __construct(RendererInterface $renderer = null)
    {
        if (!$renderer) {
            $renderer = new WkHtmlToPdf();
        }

        $this->renderer = $renderer;
    }

    /**
     * {@inheritdoc}
     */
    public function render($html, $orientation = 'portrait')
    {
        return $this->renderer->create($html, $orientation);
    }

    /**
     * {@inheritdoc}
     */
    public function setPageSize($width, $height)
    {
        $this->renderer->setPageSize($width, $height);
    }

    /**
     *
     * {@inheritdoc}
     */
    public function setMargins($marginTop, $marginRight, $marginBottom, $marginLeft)
    {
        $this->renderer->setMargins($marginTop, $marginRight, $marginBottom, $marginLeft);
    }
}
