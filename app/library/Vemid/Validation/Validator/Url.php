<?php

namespace Vemid\Validation\Validator;

use Phalcon\Validation;

/**
 * Class Url
 *
 * @package Vemid\Validation\Validator
 * @author Darko Vesic
 */
class Url extends Validation\Validator implements Validation\ValidatorInterface
{

    /**
     * @param Validation $validation
     * @param string $attribute
     * @return bool
     * @throws Validation\Exception
     */
    public function validate(Validation $validation, $attribute)
    {
        $value = filter_var($validation->getValue($attribute), FILTER_SANITIZE_URL);

        if (false === is_string($attribute)) {
            throw new Validation\Exception('Field name must be a string');
        }

        if ($this->getOption('allowEmpty') && empty($value)) {
            return true;
        }

        if (!preg_match("@^https?://@", $value, $matches)) {
            $value = 'http://' . $value;
        }

        $parsedUrl = parse_url($value);
        if (filter_var(gethostbyname($parsedUrl['host']), FILTER_VALIDATE_IP) === false) {
            $messageText = $this->getOption('message') ?: sprintf('%s is not valid url', $attribute);
            $message = new Validation\Message($messageText, $attribute);
            $validation->appendMessage($message);

            return false;
        }

        return true;
    }

}
