<?php

namespace Vemid\Validation\Validator;

use Phalcon\Validation;

/**
 * Class PresenceOf
 *
 * @package Vemid\Validation\Validator
 * @author Vemid
 */
class PresenceOf extends Validation\Validator\PresenceOf implements Validation\ValidatorInterface
{

    /**
     * Executes the validation
     *
     * @param  Validation $validator
     * @param  string $attribute
     * @return boolean
     */
    public function validate(Validation $validator, $attribute)
    {
        if (is_null($attribute)) {
            return false;
        }

        $nameArray = explode('[', $attribute);
        $value = $validator->getValue($nameArray[0]);
        $message = $this->getOption('message');

        if (is_array($value)) {
            if (count($nameArray) > 1) {
                array_shift($nameArray);
                do {
                    $key = str_replace(']', '', current($nameArray));
                    next($nameArray);
                    if (isset($value[$key])) {
                        if (is_string($value[$key]) && strlen($value[$key]) > 0) {
                            return true;
                        } else {
                            $value = $value[$key];
                        }
                    } else {
                        $validator->appendMessage(new Validation\Message($message, $attribute));
                        return false;
                    }
                } while (count($nameArray));
            }
        } else {
            return parent::validate($validator, $attribute);
        }

        return false;
    }

}
