<?php

namespace Vemid\Helper;

use Vemid\Form\Form;
use Vemid\Entity\EntityInterface;
use Phalcon\Forms\Element;
use Phalcon\Forms\ElementInterface;
use Phalcon\Mvc\Model\Resultset;

/**
 * Class FormHelper
 *
 * @package Vemid\Helper
 * @author Vemid
 */
class FormHelper
{

    /** @var EntityInterface */
    protected $entity;

    /** @var Form */
    protected $form;

    protected $parameters;

    /**
     * @param EntityInterface $entity
     * @param array $parameters
     */
    public function __construct(EntityInterface $entity, array $parameters)
    {
        $this->entity = $entity;
        $this->parameters = $parameters;

        $this->form = $this->entity->getForm(array_diff(
            array_keys($this->entity->toArray()),
            array_keys($this->parameters)
        ));
    }

    /**
     * @return Form
     */
    public function getSearchForm()
    {
        foreach ($this->form->getElements() as $element) {

            /** @var ElementInterface $element */

            if (array_key_exists($element->getName(), $this->parameters)) {
                $element->setDefault($this->parameters[$element->getName()]);
            }

            $element->setAttribute('class', 'form-control');

            if ($element instanceof Element\Select) {
                $element->setAttribute('multiple', 'multiple');
                $element->setAttribute('class', 'chosen-select form-control');
                $element->setAttribute('name', $element->getName() . '[]');
            } else if ($element instanceof Element\TextArea) {
                $elem = new Element\Text($element->getName());
                $elem->addValidators($element->getValidators());
                $elem->setAttributes($element->getAttributes());
                $elem->setDefault($element->getDefault());
                $elem->setLabel($element->getLabel());
                $this->form->add($elem);
            }
        }

        return $this->form;
    }

    /**
     * @param null|string $order
     * @return Resultset
     */
    public function getCollection($order = null)
    {
        $conditions = '1 = 1';
        $bind = [];

        foreach ($this->parameters as $property => $value) {
            if (is_array($value) && count($value)) {
                array_walk($value, function (&$val) {
                    $val = preg_quote($val, '"');
                    return $val;
                });
                $conditions .= " AND {$property} IN (\"" . implode('","', $value) . "\")";
            } else if ($value) {
                if (!is_numeric($value)) {
                    $conditions .= " AND {$property} LIKE :{$property}:";
                    $bind[$property] = '%' . preg_quote($value, "'") . '%';
                } else {
                    $conditions .= " AND {$property} = :{$property}:";
                    $bind[$property] = $value;
                }
            }
        }

        $arguments = [$conditions, 'bind' => $bind, 'OrderItem' => $order];

        return call_user_func([get_class($this->entity), 'find'], $arguments);
    }

}
