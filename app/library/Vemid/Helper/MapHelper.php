<?php

namespace Vemid\Helper;

use Geocoder\Model\Address;
use Geocoder\Provider\GoogleMaps as HttpProvider;
use Ivory\GoogleMap;
use Ivory\GoogleMap\Events;
use Ivory\GoogleMap\Exception;
use Ivory\GoogleMap\Helper;
use Ivory\GoogleMap\Overlays;
use Ivory\HttpAdapter\CurlHttpAdapter as HttpAdapter;

/**
 * Class MapHelper
 *
 * @package Vemid\Helper
 * @author Darko Vesic
 */
class MapHelper
{

    /** @var GoogleMap\Map */
    private $map;

    /**
     * @param array $options
     * @param GoogleMap\Map|null $map
     * @throws Exception\AssetException
     * @throws Exception\MapException
     */
    public function __construct(array $options = [], GoogleMap\Map $map = null)
    {
        $defaultOptions = [
            'height' => '200px',
            'width' => '100%',
            'mapTypeId' => GoogleMap\MapTypeId::ROADMAP,
            'draggable' => false,
        ];

        $options = array_merge($defaultOptions, $options);

        if ($map === null) {
            $map = new GoogleMap\Map();
        }

        $this->map = $map;
        $this->map->setPrefixJavascriptVariable('map_');
        $this->map->setHtmlContainerId('map_canvas');
        $this->map->setAsync(false);
        $this->map->setAutoZoom(false);
        $this->map->setMapOptions($options);
        $this->map->setStylesheetOption('height', $options['height']);
        $this->map->setStylesheetOption('width', $options['width']);

        if ($options['draggable']) {
            $this->map->getEventManager()->addDomEvent($this->setMovableMarker($this->map));
        }
    }

    /**
     * @param GoogleMap\Map $map
     * @return Events\Event
     * @throws Exception\EventException
     */
    private function setMovableMarker(GoogleMap\Map $map)
    {
        $event = new Events\Event();
        $event->setInstance($map->getJavascriptVariable());
        $event->setEventName(Events\MouseEvent::CLICK);
        $event->setHandle(sprintf('function(event) {

            var map = %s;
            marker = new google.maps.Marker({
                position: event.latLng,
                map: map,
                draggable: true
            });

            if (typeof oldMarker !== "undefined"){
                oldMarker.setMap(null);
            }

            var newLat = event.latLng.lat();
            var newLng = event.latLng.lng();

            document.getElementById("latitude").value = newLat;
            document.getElementById("longitude").value = newLng;

            oldMarker = marker;
            map.setCenter(event.latLng);

            google.maps.event.addListener(marker, "dragend", function (event) {
                document.getElementById("latitude").value = this.getPosition().lat();
                document.getElementById("longitude").value = this.getPosition().lng();
            });
        }',
            $map->getJavascriptVariable()
        ));

        $event->setCapture(true);

        return $event;
    }

    /**
     * @param string $ip
     */
    public function setMarkerFromIp($ip)
    {
        if (filter_var(gethostbyname($ip), FILTER_VALIDATE_IP) === true) {
            $geocoder = new HttpProvider(new HttpAdapter());

            /** @var Address $address */
            $address = $geocoder->geocode($address)->first();
            $this->setMarker($address->getLatitude(), $address->getLongitude());
        }
    }

    /**
     * @param double $latitude
     * @param double $longitude
     * @param Overlays\MarkerImage $markerImage
     * @return Overlays\Marker
     * @throws Exception\AssetException
     * @throws Exception\OverlayException
     */
    public function setMarker($latitude, $longitude, Overlays\MarkerImage $markerImage = null)
    {
        $marker = new Overlays\Marker();
        $marker->setPrefixJavascriptVariable('marker_');
        $marker->setPosition($latitude, $longitude, true);
        $marker->setAnimation(Overlays\Animation::DROP);

        $marker->setOptions([
            'clickable' => true,
            'flat' => true,
        ]);

        if ($markerImage) {
            $marker->setIcon($markerImage);
        }

        $this->map->addMarker($marker);
        $this->map->getBound()->extend($marker);

        return $marker;
    }

    /**
     * @param string $address
     * @return Overlays\Marker
     */
    public function setMarkerFromAddress($address)
    {
        $geocoder = new HttpProvider(new HttpAdapter());

        /** @var Address $address */
        $address = $geocoder->geocode($address)->first();

        return $this->setMarker($address->getLatitude(), $address->getLongitude());
    }

    /**
     * @param string $imageUrl
     * @return Overlays\MarkerImage
     * @throws Exception\AssetException
     * @throws Exception\OverlayException
     */
    public function setMarkerImage($imageUrl)
    {
        $markerImage = new Overlays\MarkerImage();

        $markerImage->setPrefixJavascriptVariable('marker_image_');
        $markerImage->setUrl($imageUrl);
        $markerImage->setAnchor(20, 34);
        $markerImage->setOrigin(0, 0);
        $markerImage->setSize(20, 34, 'px', 'px');
        $markerImage->setScaledSize(20, 34, 'px', 'px');

        return $markerImage;
    }

    /**
     * @return GoogleMap\Map
     * @throws Exception\AssetException
     * @throws Exception\EventException
     * @throws Exception\MapException
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * @return string
     */
    public function render()
    {
        $mapHelper = new Helper\MapHelper();

        if ($this->getCenter()) {
            $this->map->setCenter($this->getCenter());
        }

        return $mapHelper->render($this->map);
    }

    /**
     * @return GoogleMap\Base\Coordinate|null
     */
    public function getCenter()
    {
        if (count($this->map->getMarkers()) === 1) {
            /** @var Overlays\Marker $marker */
            $marker = current($this->map->getMarkers());
            return $marker->getPosition();
        } elseif ($this->map->getBound()->hasCoordinates()) {
            return $this->map->getBound()->getCenter();
        }

        return null;
    }

}
