<?php

namespace Vemid\Helper;

use Vemid\Entity\EntityInterface;

/**
 * Class ModelHelper
 *
 * @package Vemid\Helper
 * @author Vemid
 */
class ModelHelper
{

    /** @var EntityInterface */
    private $entity;

    /**
     * @param EntityInterface|null $entity
     */
    public function __construct(EntityInterface $entity = null)
    {
        $this->entity = $entity;
    }

    /**
     * @param array|string $arguments
     * @param array $defaultArguments
     * @return array
     */
    public function mergeArguments($arguments, $defaultArguments)
    {
        $arguments = $this->bindArguments($arguments);

        if (is_array($arguments)) {

            if (isset($arguments['conditions'])) {
                $conditions = $arguments['conditions'];
                unset($arguments['conditions']);
            } else if (isset($arguments[0])) {
                $conditions = $arguments[0];
                unset($arguments[0]);
            }

            if (isset($conditions)) {
                if (is_array($conditions)) {
                    $defaultArguments['conditions'] .= ' AND ' . implode(' AND ', $conditions);
                } else {
                    $defaultArguments['conditions'] .= ' AND ' . $conditions;
                }
            }

            foreach ($arguments as $index => $value) {
                if (array_key_exists($index, $defaultArguments)) {
                    $defaultArguments[$index] = array_merge($arguments[$index], $defaultArguments[$index]);
                    unset($arguments[$index]);
                }
            }

            $arguments = array_merge($arguments, $defaultArguments);

        } else {
            $arguments = $defaultArguments;
        }

        return $arguments;
    }

    /**
     * @param string $entityTypeIdPropertyName
     * @param string $entityIdPropertyName
     * @return array
     */
    public function generateDefaultArguments($entityTypeIdPropertyName, $entityIdPropertyName)
    {
        if ($this->entity === null) {
            return [];
        }

        return [
            'conditions' =>
                $entityTypeIdPropertyName . ' = :objectTypeId: AND ' .
                $entityIdPropertyName . ' = :objectId:',
            'bind' => [
                'objectTypeId' => $this->entity->getObjectTypeId(),
                'objectId' => $this->entity->getObjectId(),
            ]
        ];
    }

    /**
     * @param array|string|int $arguments
     * @return array|int|string
     */
    public function bindArguments($arguments)
    {
        if (is_numeric($arguments)) {
            $arguments = [
                'conditions' => 'id = :id:',
                'bind' => ['id' => $arguments]
            ];
        } else if (is_string($arguments)) {
            $arguments = [$arguments];
        }

        return $arguments;
    }

}
