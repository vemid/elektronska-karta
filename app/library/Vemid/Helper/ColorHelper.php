<?php

namespace Vemid\Helper;

/**
 * Class ColorHelper
 *
 * @package Vemid\Helper
 * @author Vemid
 */
class ColorHelper
{

    /** @var \ArrayObject */
    protected $colors;
    
    /** @var \ArrayIterator */
    protected $iterator;


    /**
     * @return string
     */
    public function getRandomColor()
    {
        if (is_null($this->colors)) {
            $this->initColors();
        }

        if (!$this->iterator->valid()) {
            $this->iterator->rewind();
        }

        $color = $this->iterator->current();
        $this->iterator->next();

        return $color;
    }

    /**
     * @param $hex
     * @return null|string
     */
    public function hex2rgb($hex)
    {
        if (!$this->isValidHex($hex)) {
            return null;
        }

        $hex = str_replace('#', '', $hex);

        if (strlen($hex) == 3) {
            $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $r = hexdec(substr($hex, 0, 2));
            $g = hexdec(substr($hex, 2, 2));
            $b = hexdec(substr($hex, 4, 2));
        }

        return implode(',', [$r, $g, $b]);
    }

    /**
     * @param string|array $rgb
     * @return string
     */
    public function rgb2hex($rgb)
    {
        if (!$this->isValidRgb($rgb)) {
            return null;
        }

        $rgb = $this->rgbToArray($rgb);

        $hex = '#';
        $hex .= str_pad(dechex($rgb[0]), 2, '0', STR_PAD_LEFT);
        $hex .= str_pad(dechex($rgb[1]), 2, '0', STR_PAD_LEFT);
        $hex .= str_pad(dechex($rgb[2]), 2, '0', STR_PAD_LEFT);

        return $hex;
    }

    /**
     * @param string $hex
     * @return bool
     */
    public function isValidHex($hex)
    {
        return preg_match('/^#?+[0-9a-f]{3}(?:[0-9a-f]{3})?$/i', $hex);
    }

    /**
     * @param string|array $rgb
     * @return bool
     */
    public function isValidRgb($rgb)
    {
        $rgb = $this->rgbToArray($rgb);
        if (count($rgb) === 3) {
            foreach ($rgb as $color) {
                if (!is_numeric($color) || $color < 0 || $color > 255) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @param $color
     * @return string
     */
    public function toHex($color)
    {
        if ($this->isValidHex($color)) {
            return $color;
        }

        return $this->rgb2hex($color);
    }

    /**
     * @param $color
     * @return string
     */
    public function toRgb($color)
    {
        if ($this->isValidRgb($color)) {
            return implode(',', $this->rgbToArray($color));
        }

        return $this->hex2rgb($color);
    }

    /**
     * @param $rgb
     * @return array
     */
    protected function rgbToArray($rgb)
    {
        if (is_string($rgb)) {
            $rgb = explode(',', $rgb);
        }

        return $rgb;
    }

    /**
     *
     */
    protected function initColors()
    {
        $colors = [];
        for ($i = 0; $i <= 5; $i++) {
            $colors[] = (string)rand(0, 255) . ',' . (string)rand(0, 255) . ',' . (string)rand(0, 255);
        }

        shuffle($colors);
        $this->colors = new \ArrayObject($colors);
        $this->iterator = $this->colors->getIterator();
    }

}
