<?php

namespace Vemid\Helper;

use Phalcon\Image;
use Phalcon\Image\Adapter\Imagick;
use Phalcon\Image\AdapterInterface as ImageInterface;

/**
 * Class ImageHelper
 *
 * @author Vemid
 * @package Vemid\Helper
 */
class ImageHelper
{

    /** @var ImageInterface */
    private $image;

    /**
     * @param string $imagePath
     * @param ImageInterface|null $image
     */
    public function __construct($imagePath, ImageInterface $image = null)
    {
        if ($image === null) {
            $image = new Imagick($imagePath);
        }

        $this->image = $image;
    }

    /**
     * @param int $width
     * @param int $height
     * @return ImageInterface
     */
    public function resizeAndCrop($width, $height)
    {
        $offsetX = null;
        $offsetY = null;

        if ($this->getWidth() > $this->getHeight()) {
            $master = Image::HEIGHT;
            $offsetX = ($height * $this->getWidth() / $this->getHeight() - $width) / 2;
        } else {
            $master = Image::WIDTH;
            $offsetY = ($width * $this->getHeight() / $this->getWidth() - $height) / 2;
        }

        return $this->image->resize($width, $height, $master)->crop($width, $height, $offsetX, $offsetY)->save();
    }

    /**
     * @param $amount
     */
    public function sharpenImage($amount)
    {
        $this->image->sharpen($amount)->save();
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->image->getWidth();
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->image->getHeight();
    }

    /**
     * @param null|int $width
     * @return Image\Adapter
     */
    public function resizeByWidth($width)
    {
        return $this->image->resize($width, null, Image::WIDTH)->save();
    }

    /**
     * @param null|int $height
     * @return Image\Adapter
     */
    public function resizeByHeight($height)
    {
        return $this->image->resize(null, $height, Image::HEIGHT)->save();
    }
}
