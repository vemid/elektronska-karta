<?php

namespace Vemid\Helper;

/**
 * Class SiteHelper
 *
 * @package Vemid\Helper
 * @author Vemid
 */
class SiteHelper
{

    /** @var array */
    private $settings;

    /**
     * SiteHelper constructor.
     * @param array $settings
     */
    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @return string
     */
    public function getSiteName()
    {
        return $this->getSetting('siteName');
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return rtrim($this->getSetting('baseUri'), '/') . '/';
    }

    /**
     * @param string $url
     * @param string $link
     * @return string
     */
    public function linkTo($url, $link = null)
    {
        $url = $this->prepUrl($url);

        if (is_null($link)) {
            $link = $url;
        }

        return '<a href="' . $url . '">' . $link . '</a>';
    }

    /**
     * @param string $url
     * @return string
     */
    public function prepUrl($url)
    {
        if (0 !== strpos($url, 'http')) {
            $url = $this->getBaseUrl() . ltrim($url, '/');
        }

        return $url;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->settings;
    }

    /**
     * @return \stdClass
     */
    public function toSimpleObject()
    {
        return $this->arrayToObject($this->settings);
    }

    /**
     * @param array $input
     * @return \stdClass
     */
    protected function arrayToObject($input)
    {
        $obj = new \stdClass();
        foreach ($input as $k => $v) {
            if (is_array($v)) {
                $obj->{$k} = $this->arrayToObject($v);
            } else {
                $obj->{$k} = $v;
            }
        }

        return $obj;
    }

    /**
     * @param string $key
     * @return string|array|null
     */
    protected function getSetting($key)
    {
        if (array_key_exists($key, $this->settings)) {
            return $this->settings[$key];
        }

        return null;
    }

}
