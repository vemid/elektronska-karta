<?php

namespace Vemid\Helper;

use Phalcon\Text;

/**
 * Class TextHelper
 *
 * @package Vemid\Helper
 * @author Vemid
 */
class TextHelper
{

    /** @var string */
    private $locale;

    /**
     * @param string $locale
     */
    public function __construct($locale = 'en_US')
    {
        $this->locale = $locale;
    }

    /**
     * @param string $string
     * @param string $separator
     * @return string $string
     */
    public function stripTags($string, $separator = '-')
    {
        $quotedSeparator = preg_quote($separator);

        $patterns = [
            '&.+?;' => '',
            '[^a-z0-9 _-]' => '',
            '\s+' => $separator,
            '(' . $quotedSeparator . ')+' => $separator
        ];

        if ($this->locale = 'sr_RS') {
            $string = $this->serbianToEnglish(strip_tags($string));
        }

        foreach ($patterns as $pattern => $replacement) {
            $string = preg_replace("#" . $pattern . "#i", $replacement, $string);
        }

        return trim(strtolower($string), $separator);
    }

    /**
     * @param string $serbian
     * @return string
     */
    public function serbianToEnglish($serbian)
    {
        $serbian = $this->cyrillicToLatin($serbian);

        $replaceSerbian = [
            'š' => 's', 'đ' => 'dj', 'č' => 'c', 'ć' => 'c', 'ž' => 'z',
            'Š' => 'S', 'Đ' => 'Dj', 'Č' => 'C', 'Ć' => 'C', 'Ž' => 'Z',
        ];

        return strtr($serbian, $replaceSerbian);
    }

    /**
     *
     * @param string $latin
     * @return string
     */
    public function latinToCyrillic($latin)
    {
        $replaceLat = [
            "A" => "А", "B" => "Б", "V" => "В", "G" => "Г", "D" => "Д", "Đ" => "Ђ", "DJ" => "Ђ", "Dj" => "Ђ",
            "E" => "Е", "Ž" => "Ж", "Z" => "З", "I" => "И", "J" => "Ј", "K" => "К", "L" => "Л", "LJ" => "Љ",
            "M" => "М", "N" => "Н", "NJ" => "Њ", "O" => "О", "P" => "П", "R" => "Р", "S" => "С", "T" => "Т",
            "Ć" => "Ћ", "U" => "У", "F" => "Ф", "H" => "Х", "C" => "Ц", "Č" => "Ч", "DŽ" => "Џ", "Š" => "Ш",
            "a" => "а", "b" => "б", "v" => "в", "g" => "г", "d" => "д", "đ" => "ђ", "dj" => "ђ",
            "e" => "е", "ž" => "ж", "z" => "з", "i" => "и", "j" => "ј", "k" => "к", "l" => "л", "lj" => "љ",
            "m" => "м", "n" => "н", "nj" => "њ", "o" => "о", "p" => "п", "r" => "р", "s" => "с", "t" => "т",
            "ć" => "ћ", "u" => "у", "f" => "ф", "h" => "х", "c" => "ц", "č" => "ч", "dž" => "џ", "š" => "ш",
            "Nj" => "Њ", "Lj" => "Љ", "Dž" => "Џ"
        ];

        return strtr($latin, $replaceLat);
    }

    /**
     *
     * @param string $cyrillic
     * @return string
     */
    public function cyrillicToLatin($cyrillic)
    {
        $replaceCyr = [
            "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D", "Ђ" => "Đ",
            "Е" => "E", "Ж" => "Ž", "З" => "Z", "И" => "I", "Ј" => "J", "К" => "K", "Л" => "L", "Љ" => "Lj",
            "М" => "M", "Н" => "N", "Њ" => "Nj", "О" => "O", "П" => "P", "Р" => "R", "С" => "S", "Т" => "T",
            "Ћ" => "Ć", "У" => "U", "Ф" => "F", "Х" => "H", "Ц" => "C", "Ч" => "Č", "Џ" => "Dž", "Ш" => "Š",
            "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d", "ђ" => "đ",
            "е" => "e", "ж" => "ž", "з" => "z", "и" => "i", "ј" => "j", "к" => "k", "л" => "l", "љ" => "lj",
            "м" => "m", "н" => "n", "њ" => "nj", "о" => "o", "п" => "p", "р" => "r", "с" => "s", "т" => "t",
            "ћ" => "ć", "у" => "u", "ф" => "f", "х" => "h", "ц" => "c", "ч" => "č", "џ" => "dž", "ш" => "š",
        ];

        return strtr($cyrillic, $replaceCyr);
    }

    /**
     * @param $string
     * @return string
     */
    public static function stringToCode($string)
    {
        return strtoupper(Text::uncamelize($string, '_'));
    }

    /**
     * @param $code
     * @return string
     */
    public static function codeToString($code)
    {
        return ucwords(Text::uncamelize(Text::camelize($code), ' '));
    }

    /**
     * @param string $truncate
     * @param int $amount
     * @param bool $echo
     * @param string $allowed
     * @return string
     */
    public static function truncateText($truncate, $amount, $echo = true, $allowed = '<a>')
    {
        if (strlen($truncate) <= $amount) {
            $suffix = '';
        } else {
            $suffix = '...';
        }

        $truncate = preg_replace('@<script[^>]*?>.*?</script>@si', '', $truncate);
        $truncate = preg_replace('@<style[^>]*?>.*?</style>@si', '', $truncate);
        $truncate = strip_tags($truncate, $allowed);

        if ($suffix == '...') {
            $truncate = mb_substr($truncate, 0, strrpos(substr($truncate, 0, $amount), ' '));
        } else {
            $truncate = mb_substr($truncate, 0, $amount);
        }

        if (!$echo) {
            return ($truncate . $suffix);
        }

        echo $truncate, $suffix;
    }

}
