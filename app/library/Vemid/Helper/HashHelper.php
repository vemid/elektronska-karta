<?php

namespace Vemid\Helper;

/**
 * Class HashHelper
 *
 * @package Vemid\Helper
 * @author Vemid
 */
class HashHelper
{
    private $salt = '1#2Fds1CR.&b7CfQz-9BwX*R&xC3R+Mn';

    /**
     * @param string $input
     * @return string
     */
    public function generatePasswordHash($input)
    {
        if (\function_exists('password_hash')) {
            return password_hash($input, PASSWORD_BCRYPT);
        }

        return sha1($this->salt . $input);
    }

    /**
     * @param $input
     * @param $hash
     * @return bool|string
     */
    public function verifyPassword($input, $hash)
    {
        if (\function_exists('password_verify')) {
            return password_verify($input, $hash);
        }

        return sha1($this->salt . $input);
    }

    /**
     * @param string $input
     * @param string $algorithm
     * @return string
     */
    public function generateHash($input, $algorithm)
    {
        if (\function_exists('hash') && \in_array($algorithm, hash_algos())) {
            return hash($algorithm, $this->salt . $input);
        }

        return sha1($this->salt . $input);
    }
}
