<?php

namespace Vemid\Helper;

use Vemid\Application\Application;
use Vemid\Date\DateTime;
use Vemid\Date\Time;
use Vemid\Entity\EntityInterface;
use Phalcon\Db\Column;
use Phalcon\Mvc\Model\MetaDataInterface;

/**
 * Class MetaDataHelper
 *
 * @package Vemid\Helper
 * @author Vemid
 */
class MetaDataHelper
{
    /** @var MetaDataInterface */
    private $modelsMetadata;

    /**
     * @param MetaDataInterface $modelsMetadata
     */
    public function __construct(MetaDataInterface $modelsMetadata)
    {
        $this->modelsMetadata = $modelsMetadata;
    }

    /**
     * @param EntityInterface $entity
     * @param array $data
     * @return array
     */
    public function castFromDatabaseValues(EntityInterface $entity, array $data)
    {
        $columnMap = $this->modelsMetadata->getColumnMap($entity);
        foreach ($this->modelsMetadata->getDataTypes($entity) as $field => $dataType) {
            if (!$columnMap) {
                continue;
            }

            if (!array_key_exists($field, $columnMap)) {
                continue;
            }

            $property = $columnMap[$field];
            if (!array_key_exists($property, $data)) {
                continue;
            }

            $value = $data[$property];

            if (is_array($value) || is_object($value)) {
                continue;
            }

            if ($value === null || strlen(trim($value)) === 0) {
                $data[$property] = null;
                continue;
            }

            switch ($dataType) {
                case Column::TYPE_BOOLEAN:
                    $value = boolval($value);
                    break;
                case Column::TYPE_INTEGER:
                case Column::TYPE_BIGINTEGER:
                    $value = intval($value);
                    break;
                case Column::TYPE_DECIMAL:
                case Column::TYPE_FLOAT:
                case Column::TYPE_DOUBLE:
                    $value = floatval($value);
                    break;
                case Column::TYPE_DATE:
                case Column::TYPE_DATETIME:
                    if (!$value instanceof DateTime) {
                        if ($dataType == Column::TYPE_DATE) {
                            $value = new DateTime($value, new \DateTimeZone(Application::DEFAULT_TIMEZONE), DateTime::TYPE_DATE);
                        } else {
                            $value = new DateTime($value, new \DateTimeZone(Application::DEFAULT_TIMEZONE), DateTime::TYPE_DATETIME);
                        }
                        $value->setTimezone(new \DateTimeZone(date_default_timezone_get()));
                    }
                    break;
            }

            $data[$property] = $value;
        }

        return $data;
    }

    /**
     * Casts all objects into proper value
     *
     * @param array $data
     * @return array
     */
    public function castToDatabaseValues(array $data)
    {
        $filteredData = [];

        foreach ($data as $property => $value) {
            if (is_object($value)) {
                if ($value instanceof DateTime) {
                    $value = $value->getGmtDateTime()->getUnixFormat();
                } elseif ($value instanceof Time) {
                    $value = $value->getUnixFormat();
                } else {
                    $value = (string)$value;
                }
            } elseif (is_array($value)) {
                $value = $this->castToDatabaseValues($value);
            }

            $filteredData[$property] = $value;
        }

        return $filteredData;
    }
}
