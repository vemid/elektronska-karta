<?php

namespace Vemid\Helper;

use Phalcon\Config;

class Crypto
{
    private const CRYPT_METHOD = 'AES-256-CBC';

    /** @var Config */
    private $config;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function encrypt(string $input)
    {
        $key = hash('sha256', $this->config->application->secretKey);
        $iv = substr(hash('sha256', $this->config->application->iv), 0, 16);
        $output = \openssl_encrypt($input, self::CRYPT_METHOD, $key, 0, $iv);

        return base64_encode($output);
    }

    public function decrypt(string $input)
    {
        $key = hash('sha256', $this->config->application->secretKey);
        $iv = substr(hash('sha256',  $this->config->application->iv), 0, 16);

        return openssl_decrypt(base64_decode($input), self::CRYPT_METHOD, $key, 0, $iv);
    }
}
