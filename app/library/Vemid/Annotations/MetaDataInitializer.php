<?php

namespace Vemid\Annotations;

use Phalcon\Annotations\Adapter;
use Phalcon\Annotations\Collection;
use Phalcon\DiInterface;
use Phalcon\Db\Column;
use Phalcon\Mvc\ModelInterface;
use Phalcon\Mvc\Model\MetaData;
use Phalcon\Mvc\Model\MetaData\StrategyInterface;

/**
 * Class MetaDataInitializer
 *
 * @package Annotations
 * @author Vemid
 */
class MetaDataInitializer implements StrategyInterface
{

    /**
     * Initializes the model's meta-data
     *
     * @param ModelInterface $model
     * @param DiInterface $di
     * @return array
     * @throws \Exception
     */
    public function getMetaData(ModelInterface $model, DiInterface $di)
    {
        /** @var Adapter $annotationsAdapter */
        $annotationsAdapter = $di->get('annotations');

        $reflection = $annotationsAdapter->get($model);
        $properties = $reflection->getPropertiesAnnotations();

        if (!$properties) {
            throw new \Exception("There are no properties defined on the class");
        }

        $attributes = array();
        $notNulls = array();
        $dataTypes = array();
        $dataTypesBind = array();
        $numericTypes = array();
        $primaryKeys = array();
        $nonPrimaryKeys = array();
        $identity = false;

        foreach ($properties as $propertyName => $annotations) {

            /** @var Collection $annotations */
            if ($annotations->has('Column')) {

                $columnAnnotation = $annotations->get('Column');

                $arguments = $columnAnnotation->getArguments();

                /**
                 * Get the column's name
                 */
                if (isset($arguments['column'])) {
                    $columnName = $arguments['column'];
                } else {
                    $columnName = $propertyName;
                }

                /**
                 * Check for the 'type' parameter in the 'Column' annotation
                 */
                if (isset($arguments['type'])) {
                    switch ($arguments['type']) {

                        case 'integer':
                        case 'int':
                            $dataTypes[$columnName] = Column::TYPE_INTEGER;
                            $dataTypesBind[$columnName] = Column::BIND_PARAM_INT;
                            $numericTypes[$columnName] = true;
                            break;

                        case 'string':
                        default:
                            $dataTypes[$columnName] = Column::TYPE_VARCHAR;
                            $dataTypesBind[$columnName] = Column::BIND_PARAM_STR;
                            break;

                        case 'date':
                            $dataTypes[$columnName] = Column::TYPE_DATE;
                            $dataTypesBind[$columnName] = Column::BIND_PARAM_STR;
                            break;

                        case 'dateTime':
                            $dataTypes[$columnName] = Column::TYPE_DATETIME;
                            $dataTypesBind[$columnName] = Column::BIND_PARAM_STR;
                            break;

                        case 'decimal':
                            $dataTypes[$columnName] = Column::TYPE_DECIMAL;
                            $dataTypesBind[$columnName] = Column::BIND_PARAM_DECIMAL;
                            break;

                        case 'boolean':
                        case 'bool':
                            $dataTypes[$columnName] = Column::TYPE_BOOLEAN;
                            $dataTypesBind[$columnName] = Column::BIND_PARAM_INT;
                            $numericTypes[$columnName] = true;
                            break;
                    }
                } else {
                    $dataTypes[$columnName] = Column::TYPE_VARCHAR;
                    $dataTypesBind[$columnName] = Column::BIND_PARAM_STR;
                }

                /**
                 * Check for the 'allowNull' parameter in the 'Column' annotation
                 */
                if (!$annotations->has('Identity')) {
                    if (isset($arguments['nullable'])) {
                        if (!$arguments['nullable']) {
                            $notNulls[] = $columnName;
                        }
                    }
                }

                $attributes[] = $columnName;

                /**
                 * Check if the attribute is marked as primary
                 */
                if ($annotations->has('Primary')) {
                    $primaryKeys[] = $columnName;
                } else {
                    $nonPrimaryKeys[] = $columnName;
                }

                /**
                 * Check if the attribute is marked as identity
                 */
                if ($annotations->has('Identity')) {
                    $identity = $columnName;
                }

            }

        }

        $metaData = array(

            //Every column in the mapped table
            MetaData::MODELS_ATTRIBUTES => $attributes,

            //Every column part of the primary key
            MetaData::MODELS_PRIMARY_KEY => $primaryKeys,

            //Every column that isn't part of the primary key
            MetaData::MODELS_NON_PRIMARY_KEY => $nonPrimaryKeys,

            //Every column that doesn't allows null values
            MetaData::MODELS_NOT_NULL => $notNulls,

            //Every column and their data types
            MetaData::MODELS_DATA_TYPES => $dataTypes,

            //The columns that have numeric data types
            MetaData::MODELS_DATA_TYPES_NUMERIC => $numericTypes,

            //The identity column, use boolean false if the model doesn't have an identity column
            MetaData::MODELS_IDENTITY_COLUMN => $identity,

            //How every column must be bound/casted
            MetaData::MODELS_DATA_TYPES_BIND => $dataTypesBind,

            //Fields that must be ignored from INSERT SQL statements
            MetaData::MODELS_AUTOMATIC_DEFAULT_INSERT => array(),

            //Fields that must be ignored from UPDATE SQL statements
            MetaData::MODELS_AUTOMATIC_DEFAULT_UPDATE => array(),

            MetaData::MODELS_DEFAULT_VALUES => array(),
        );

        // Required in versions greater than 2.0.2
        if (defined('Phalcon\Mvc\Model\MetaData::MODELS_EMPTY_STRING_VALUES')) {
            $modelsEmptyString = constant('Phalcon\Mvc\Model\MetaData::MODELS_EMPTY_STRING_VALUES');
            $metaData[$modelsEmptyString] = array();
        }

        return $metaData;;
    }

    /**
     * Initializes the model's column map
     *
     * @param ModelInterface $model
     * @param DiInterface $di
     * @return array
     */
    public function getColumnMaps(ModelInterface $model, DiInterface $di)
    {
        /** @var Adapter $annotationsAdapter */
        $annotationsAdapter = $di->get('annotations');
        $reflection = $annotationsAdapter->get($model);

        $columnMap = array();
        $reverseColumnMap = array();

        $renamed = false;
        foreach ($reflection->getPropertiesAnnotations() as $propertyName => $annotations) {

            /** @var Collection $annotations */
            if ($annotations->has('Column')) {

                $arguments = $annotations->get('Column')->getArguments();

                /**
                 * Get the column's name
                 */
                if (isset($arguments['column'])) {
                    $columnName = $arguments['column'];
                } else {
                    $columnName = $propertyName;
                }

                $columnMap[$columnName] = $propertyName;
                $reverseColumnMap[$propertyName] = $columnName;

                if (!$renamed) {
                    if ($columnName != $propertyName) {
                        $renamed = true;
                    }
                }
            }
        }

        if ($renamed) {
            return array(
                MetaData::MODELS_COLUMN_MAP => $columnMap,
                MetaData::MODELS_REVERSE_COLUMN_MAP => $reverseColumnMap
            );
        }

        return null;
    }

}
