<?php

namespace Vemid\Messenger;

/**
 * Class Manager
 *
 * @package Vemid\Messenger
 * @author Vemid
 */
class Manager
{

    protected $messages = [];

    /**
     * @param Message $message
     * @return $this
     */
    public function appendMessage(Message $message)
    {
        $this->messages[$message->getType()][] = $message;

        return $this;
    }

    /**
     * @param null|string $type
     * @return array|null|Message[]
     */
    public function getMessages($type = null)
    {
        if ($type === null) {
            $messages = [];
            foreach ($this->messages as $messageGroup) {
                $messages = array_merge($messages, $messageGroup);
            }

            return $messages;
        }

        if (array_key_exists($type, $this->messages)) {
            return $this->messages[$type];
        }

        return [];
    }

    /**
     * @param $type
     * @return bool
     */
    public function hasMessages($type)
    {
        if (array_key_exists($type, $this->messages)) {
            return count($this->messages[$type]) > 0;
        }

        return false;
    }

    /**
     * @param null|string $type
     * @return array
     */
    public function toArray($type = null)
    {
        $array = [];
        foreach ($this->getMessages($type) as $message) {
            $array[] = $message->toArray();
        }

        return $array;
    }

    /**
     * @return $this
     */
    public function clear()
    {
        $this->messages = [];

        return $this;
    }

}
