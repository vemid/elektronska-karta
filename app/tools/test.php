<?php

use Vemid\Date\DateTime;
use Vemid\Service\Product\Hydrator;

include_once __DIR__ . '/ToolHelper.php';

$entityManager = $application->getDI()->getEntityManager();
$products = $entityManager->find(Product::class,[
    Product::PROPERTY_IS_ORDERABLE . ' = :order: ',
'bind' => [
    'order' => true
]
]);
$tags = new \Vemid\Service\Product\Tags($entityManager);

foreach ($products as $product) {
    try {
        $tags($product);
    } catch (\Exception $e){
        continue;
    }
}

var_dump("done");die;