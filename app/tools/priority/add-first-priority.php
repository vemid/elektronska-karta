<?php

require_once __DIR__ . '/../ToolHelper.php';

$entityManager = $application->getDI()->getEntityManager();

/** @var Priority $priority */
$priority = $entityManager->findOne(Priority::class, [
    'id = :id:',
    'bind' => [
        'id' => 1
    ]
]);
//print_r($productionMachine);
//die;

if ($priority) {
    /** @var Product[] $products */
    $products = $entityManager->find(Product::class);

    foreach ($products as $product) {
        $product->setPriorityId($priority->getId());
        $entityManager->save($product);
    }
}
