<?php

require_once __DIR__ . '/../ToolHelper.php';

$entityManager = $application->getDI()->getEntityManager();

/** @var ProductionMachine $productionMachine */
$productionMachine = $entityManager->findOne(ProductionMachine::class, [
    'id = :id:',
    'bind' => [
        'id' => 1
    ]
]);
//print_r($productionMachine);
//die;

if ($productionMachine) {
    /** @var OperatingListItemType[] $operatingListItemTypes */
    $operatingListItemTypes = $entityManager->find(OperatingListItemType::class);

    foreach ($operatingListItemTypes as $operatingListItemType) {
        $operatingListItemType->setProductionMachine($productionMachine);
        $entityManager->save($operatingListItemType);
    }
}
