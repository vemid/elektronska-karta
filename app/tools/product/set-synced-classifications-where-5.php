<?php

require_once __DIR__ . '/../ToolHelper.php';

$entityManager = $application->getDI()->getEntityManager();

/** @var \Product[] $products */
$products = $entityManager->find(\Product::class, [
    \Product::PROPERTY_STATUS . ' = :status:',
    'bind' => [
        'status' => \Product::STATUS_FINISHED
    ]
]);

foreach ($products as $product) {
    $productClassifications = $product->getProductClassifications([
        \ProductClassification::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId: AND ' .
        \ProductClassification::PROPERTY_IS_SYNCED . ' = :isSynced:',
        'bind' => [
            'entityTypeId' => \Vemid\Entity\Type::CODE,
            'isSynced' => false,
        ]
    ]);

    foreach ($productClassifications as $productClassification) {

        $productClassification->setIsSynced(true);

        if (!$entityManager->save($productClassification)) {
            throw new \LogicException(sprintf('Unable to set synced to product classification with ID: %s', $productClassification->getId()));
        }
    }
}

