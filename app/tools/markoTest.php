<?php


include_once __DIR__ . '/ToolHelper.php';

$entityManager = $application->getDI()->getEntityManager();

$warehouseOrderItems = $entityManager->find(WarehouseOrderItem::class,[
    WarehouseOrderItem::PROPERTY_CREATED . ' =:id: ' ,
    'bind'=> [
        'id' => '2020-01-01 00:00:00'
    ]
]);
/** @var WarehouseOrderItem $warehouseOrderItem */
foreach ($warehouseOrderItems as $warehouseOrderItem) {

//    /** @var AuditLog $products */
//    $products = $entityManager->findOne(AuditLog::class,[
//        AuditLog::PROPERTY_OPERATION . ' = :operatinon: AND ' . AuditLog::PROPERTY_MODIFIED_ENTITY_NAME .
//        ' = :entityName: AND ' . AuditLog::PROPERTY_MODIFIED_ENTITY_ID . ' = :id: ' ,
//        'bind' => [
//            'operatinon' => AuditLog::OPERATION_CREATE,
//            'entityName' => 'WarehouseOrderItem',
//            'id' => $warehouseOrderItem->getId(),
//        ]
//    ]);
//    if($products) {
        $warehouseOrderItem->setCreated($warehouseOrderItem->getWarehouseOrder()->getCreated());
        $warehouseOrderItem->save();
//    }
}
