<?php

require_once __DIR__ . '/../ToolHelper.php';


$entityManager = $application->getDI()->getEntityManager();
/** @var CodeType $codeTypeId */
$codeTypeId = $entityManager->findOne(CodeType::class, [
    'code = :code:',
    'bind' => [
        'code' => CodeType::CHECKIN_LOCATION
    ]
]);

/** @var Code $codeId */
$codeId = $entityManager->findOne(Code::class, [
    'codeTypeId = :codeTypeId: AND name = :name:',
    'bind' => [
        'codeTypeId' => $codeTypeId->getId(),
        'name' => 'knjazevac'
    ]
]);

/** @var ProductionWorker[] $productionWorkers */
$productionWorkers = $entityManager->find(ProductionWorker::class,[
    'locationCodeId = :locationCodeId:',
    'bind' => [
        'locationCodeId' => 0
    ]
]);


if($codeId) {
    foreach ($productionWorkers as $productionWorker) {
        $productionWorker->setLocationCodeId($codeId->getId());
        $entityManager->save($productionWorker);
    }

}