<?php

require_once __DIR__ . '/../ToolHelper.php';


$generator = new \Wsdl2PhpGenerator\Generator();
$generator->setLogger($application->getDI()->getLogger());

$config = $application->getDI()->getConfig();

$generator->generate(
    new \Wsdl2PhpGenerator\Config([
//        'inputFile' => 'http://82.117.205.2:1902/Servis/services/CustomSQLServisPort?wsdl',
        'inputFile' => 'http://192.168.100.210:8080/ServisMisWeb/services/OptremnicaServisPort?wsdl',
        'outputDir' => __DIR__ . '/../../../src/warehouseOrderMp/'
    ])
);