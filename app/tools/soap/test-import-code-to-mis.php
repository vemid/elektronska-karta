<?php

require_once __DIR__ . '/../ToolHelper.php';

$entityManager = $application->getDI()->getEntityManager();
/** @var Product $product */
$product = $entityManager->findOne(Product::class, 521);

$diableProductService = new \Vemid\Service\MisWsdl\DisableProduct($product);
$diableProductService->run();