<?php

require_once __DIR__ . '/../ToolHelper.php';


$generator = new \Wsdl2PhpGenerator\Generator();
$generator->setLogger($application->getDI()->getLogger());

$config = $application->getDI()->getConfig();

$generator->generate(
    new \Wsdl2PhpGenerator\Config([
        'inputFile' => $config->mis->transferServiceMPMis,
        'outputDir' => __DIR__ . '/../../../src/transferServiceMPMis/'
    ])
);