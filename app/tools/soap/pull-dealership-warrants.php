<?php

require_once __DIR__ . '/../ToolHelper.php';

use Vemid\Service\MisWsdl\ImportWarrants;

$entityManager = $application->getDI()->getEntityManager();

$endDate = new \Vemid\Date\DateTime();
$startDate = clone $endDate;
$startDate->modify('-50 days');
try {
    $material = new ImportWarrants($entityManager, $startDate, $endDate);
    $material->run();
}
catch (Exception $e) {
    print_r($e->getMessage());
}
