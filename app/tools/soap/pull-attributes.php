<?php

require_once __DIR__ . '/../ToolHelper.php';

use Vemid\Service\MisWsdl\ImportAttributes;

$entityManager = $application->getDI()->getEntityManager();
try {
    $material = new ImportAttributes();
    $material->run();
}
catch (Exception $e) {
    print_r($e->getMessage());
}
