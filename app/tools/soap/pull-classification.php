<?php

require_once __DIR__ . '/../ToolHelper.php';

use Vemid\Service\MisWsdl\ImportClassifications;


$entityManager = $application->getDI()->getEntityManager();
try{
$material = new ImportClassifications();
$material->run();
}
catch (Exception $e) {
    print_r($e->getMessage());
    }