<?php

require_once __DIR__ . '/../ToolHelper.php';

use Vemid\Service\MisWsdl\ImportProductionOrders;

$entityManager = $application->getDI()->getEntityManager();
try {
    $productionOrder = new ImportProductionOrders();
    $productionOrder->run();
}
catch (Exception $e) {
    print_r($e->getMessage());
}
