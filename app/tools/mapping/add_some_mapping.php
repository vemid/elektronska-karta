<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 26.1.18.
 * Time: 21.24
 */

require_once __DIR__ . '/../ToolHelper.php';


$entityManager = $application->getDI()->getEntityManager();

/** @var ClassificationOldCollectionMap[] $classificationOldCollectionMapItems */
$classificationOldCollectionMapItems = $entityManager->find(ClassificationOldCollectionMap::class);


foreach ($classificationOldCollectionMapItems as $classificationOldCollectionMapItem) {
    $classificationOldCollectionMapItem->setAttributeProducerId('1');
    $classificationOldCollectionMapItem->setAttributeProductTypeId('1');
    $classificationOldCollectionMapItem->setClassificationDoubleSeasonId('1');
    $entityManager->save($classificationOldCollectionMapItem);
}
