<?php

ini_set('memory_limit', '512M');

define('APP_PATH', dirname(__DIR__, 2) . '/');

require_once APP_PATH . 'vendor/autoload.php';
require_once APP_PATH . 'src/base/autoload.php';
require_once APP_PATH . 'src/warehouseOrder/autoload.php';
require_once APP_PATH . 'src/warehouseOrderMp/autoload.php';
require_once APP_PATH . 'src/warehouseOrderVp/autoload.php';

$application = new \Vemid\Application\Cli();
$application->initialize();
