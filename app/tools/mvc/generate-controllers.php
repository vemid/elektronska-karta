<?php

require_once __DIR__ . '/../ToolHelper.php';

$adapter = new Vemid\Database\Adapter($application->getDI()->getShared('db'));

foreach ($adapter->listTables() as $table) {
    $generator = new \Vemid\CodeGenerator\Controller($table);
    $writer = new \Vemid\CodeGenerator\FileWriter($generator);

    if ($writer->write(APP_PATH . 'app/modules/backend/controllers/')) {
        echo "Successfully saved Controller: '" . $generator->getClassName() . "'" . PHP_EOL;
    } else {
        echo "Not saved Model: '" . $generator->getClassName() . "'" . PHP_EOL;
    }
}
