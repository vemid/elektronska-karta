<?php

require_once __DIR__ . '/../ToolHelper.php';

$di = $application->getDI();
$adapter = new Vemid\Database\Adapter($di->get('db'));
$config = $di->get('config');

$objectNames = array();


foreach ($adapter->listTables() as $table) {
    if (empty($table)) {
        continue;
    }

    $generator = new \Vemid\CodeGenerator\Model($table);
    $writer = new \Vemid\CodeGenerator\FileWriter($generator);

    if ($writer->write($config->application->modelsDir)) {
        echo "Successfully saved Model: '{$generator->getClassName()}'" . PHP_EOL;
    } else {
        echo "Not saved Model: '{$generator->getClassName()}'" . PHP_EOL;
    }

    $objectNames[strtoupper($table->getEntityName(true))] = $table->getEntityName();
}

/**
 * Generate Entity Type constants
 */
$i = 0;
foreach ($objectNames as $classConstant => $className) {
    $i++;
    echo "const $classConstant = $i;" . PHP_EOL;
}

foreach ($objectNames as $classConstant => $className) {
    echo "self::$classConstant => '$className'," . PHP_EOL;
}