<?php

include_once __DIR__ . '/../ToolHelper.php';

use Vemid\Entity\Type;
use Phalcon\Annotations\Adapter\Memory as Reader;
use Phalcon\Config\Adapter\Php as ConfigPhp;


_translate("sr_RS");

/**
 * Create translation file
 *
 * @param string $locale
 */
function _translate($locale)
{
    if (!file_exists($filePath = APP_PATH . 'app/locale/' . $locale . '/translations.php')) {
        mkdir(APP_PATH . 'app/locale/' . $locale, 0755, true);
        $existingTranslations = [];
    } else {
        $config = new ConfigPhp($filePath);
        $existingTranslations = $config->toArray();
    }

    $translations = [];

    /**
     * Get labels from model annotations
     */
    $reader = new Reader();
    foreach (Type::getObjectNames() as $entityName) {

        $reflection = $reader->get($entityName);
        $properties = $reflection->getPropertiesAnnotations();
        foreach ($properties as $propertyName => $annotations) {
            if ($annotations->has('FormElement')) {

                $formElementAnnotation = $annotations->get('FormElement');
                $formElementArguments = $formElementAnnotation->getArguments();

                if (isset($formElementArguments['label'])) {
                    $translations[$formElementArguments['label']] = '';
                }

                if (isset($formElementArguments['options']) && is_array($formElementArguments['options'])) {

                    if (array_values($formElementArguments['options']) === $formElementArguments['options']) {
                        $formElementOptions = array_combine($formElementArguments['options'], $formElementArguments['options']);
                    } else {
                        $formElementOptions = $formElementArguments['options'];
                    }
                    foreach ($formElementOptions as $option) {
                        $translations[$option] = '';
                    }
                }
            }

            if ($annotations->has('FormElementValidator')) {
                $formElementValidation = $annotations->get('FormElementValidator');
                $formElementValidationArguments = $formElementValidation->getArguments();

                if (isset($formElementValidationArguments['arguments']['message'])) {
                    $translations[$formElementValidationArguments['arguments']['message']] = '';
                }
            }
        }
    }


    __scanDir(APP_PATH . 'app/', $translations);


    $translations = array_merge($translations, $existingTranslations);

    ksort($translations);

    $text = '<?php' . PHP_EOL
        . 'return array(' . PHP_EOL;
    foreach ($translations as $k => $v) {
        $text .= "    '$k' => '$v'," . PHP_EOL;
    }
    $text .= ');' . PHP_EOL;


    file_put_contents($filePath, $text);
}

/**
 * Scan dirs to find strings which should be translated
 *
 * @param string $dirName
 * @param array $translations
 */
function __scanDir($dirName, &$translations)
{
    if (is_dir($dirName)) {
        if ($fp = @opendir($dirName)) {

            while (false !== ($file = readdir($fp))) {

                if ($file === '.' || $file === '..') {
                    continue;
                }

                is_dir($dirName . $file) && $file .= DIRECTORY_SEPARATOR;

                if (is_dir($dirName . $file)) {
                    __scanDir($dirName . $file, $translations);
                } else {
                    $fileData = file_get_contents($dirName . $file);
                    if (preg_match_all('/[\s|\(]+t\(\'([^\']+)\'/si', $fileData, $matches)) {
                        foreach ($matches[1] as $string) {
                            if (strlen($string)) {
                                $translations[$string] = '';
                            }
                        }
                    }
                    if (preg_match_all('/translate\(\s*\'([^\']+)\'/si', $fileData, $matches)) {
                        foreach ($matches[1] as $string) {
                            $translations[$string] = '';
                        }
                    }
                }
            }

            closedir($fp);
        }
    }
}
