<?php

require_once __DIR__ . '/../ToolHelper.php';

$entityManager = $application->getDI()->getEntityManager();

/** @var ElectronCardFactory[] $electronCardFactories */
$electronCardFactories = $entityManager->find(ElectronCardFactory::class);

foreach ($electronCardFactories as $electronCardFactory) {
    if (!$electronCardFactory->getMaterialSketch()) {
        $materialSketch = new MaterialSketch();
        $materialSketch->setSupplierId($electronCardFactory->getSupplierId());
        $name = $electronCardFactory->getSupplier() ? $electronCardFactory->getSupplier()->getName() : 'xxx';
        $materialSketch->setClassificationCode($electronCardFactory->getClassificationCode());
        $materialSketch->setSupplierName($name);
        $materialSketch->setGrams($electronCardFactory->getGrams());
        $materialSketch->setColor($electronCardFactory->getColor());

        if ($entityManager->save($materialSketch)) {
            $electronCardFactory->setMaterialSketch($materialSketch);
            $entityManager->save($electronCardFactory);
        }
    }
}


