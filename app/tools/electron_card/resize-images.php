<?php

require_once __DIR__ . '/../ToolHelper.php';

$entityManager = $application->getDI()->getEntityManager();

/** @var ElectronCard[] $electronCards */
$electronCards = $entityManager->find(ElectronCard::class);
foreach ($electronCards as $electronCard) {

    if ($electronCard->getImage()) {
        $task = new \Vemid\Task\Image\ProcessingImageTask();
        $task->imagePath = $electronCard->getUploadPath() . $electronCard->getImage();
        $task->width = 1400;
        $task->height = null;
        $task->execute();
    }


    if ($electronCard->getAdditionalSketchImage()) {
        $task = new \Vemid\Task\Image\ProcessingImageTask();
        $task->imagePath = $electronCard->getUploadPath() . $electronCard->getAdditionalSketchImage();
        $task->height = 960;
        $task->execute();
    }


    if ($electronCard->getPrePressImage()) {
        $task = new \Vemid\Task\Image\ProcessingImageTask();
        $task->imagePath = $electronCard->getUploadPath() . $electronCard->getPrePressImage();
        $task->height = 960;
        $task->execute();
    }

    if ($electronCard->getAdditionalSecondSketchImage()) {
        $task = new \Vemid\Task\Image\ProcessingImageTask();
        $task->imagePath = $electronCard->getUploadPath() . $electronCard->getAdditionalSecondSketchImage();
        $task->height = 960;
        $task->execute();
    }
}
