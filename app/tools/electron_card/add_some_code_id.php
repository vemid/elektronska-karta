<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 26.1.18.
 * Time: 21.24
 */

require_once __DIR__ . '/../ToolHelper.php';


$entityManager = $application->getDI()->getEntityManager();
/** @var Classification $classificationId */
$classificationId = $entityManager->findOne(Classification::class, [
    'name = :name:',
    'bind' => [
        'name' => 'Osnovni Materijal'
    ]
]);

/** @var ElectronCardFactory[] $electronCardFactoriesItems */
$electronCardFactoriesItems = $entityManager->find(ElectronCardFactory::class);

print_r($electronCardFactoriesItems);
die;
if($classificationId) {
    foreach ($electronCardFactoriesItems as $electronCardFactoriesItem) {
        $electronCardFactoriesItem->setClassificationCodeId($classificationId->getId());
        $entityManager->save($electronCardFactoriesItem);
    }

}