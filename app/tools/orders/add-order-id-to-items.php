<?php

require_once __DIR__ . '/../ToolHelper.php';


$entityManager = $application->getDI()->getEntityManager();
/** @var Order $order */
$order = $entityManager->findOne(Order::class, [
    'active = :active:',
    'bind' => [
        'active' => true
    ]
]);

/** @var OrderItem[] $orderItems */
$orderItems = $entityManager->find(OrderItem::class);

if ($order) {
    foreach ($orderItems as $orderItem) {
        $orderItem->setOrder($order);
        $entityManager->save($orderItem);
    }
}