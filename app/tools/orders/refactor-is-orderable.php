<?php

require_once __DIR__ . '/../ToolHelper.php';

$entityManager = $application->getDI()->getEntityManager();

/** @var Classification[] $classifications */
$classifications = $entityManager->find(Classification::class, [
    Classification::PROPERTY_IS_ORDERABLE . ' = :isOrderable:',
    'bind' => [
        'isOrderable' => true
    ]
]);

foreach ($classifications as $classification) {
    $orderClassification = new OrderClassification();
    $orderClassification->setOrderId(1);
    $orderClassification->setClassification($classification);
    $entityManager->save($orderClassification);
}
