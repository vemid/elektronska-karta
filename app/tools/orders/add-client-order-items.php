<?php


require_once __DIR__ . '/../ToolHelper.php';


$entityManager = $application->getDI()->getEntityManager();

/** @var OrderItem[] $orderItems */
$orderItems = $entityManager->find(OrderItem::class,[
    OrderItem::PROPERTY_ORDER_ID . " ='27' and ".
    OrderItem::PROPERTY_CLIENT_ORDER_ITEM_ID . ' is null'
]);

foreach ($orderItems as $orderItem) {
    $data = $orderItem->toArray();
    unset($data['id']);

    $clientOrderItem = new ClientOrderItem($data);
    if (!$entityManager->save($clientOrderItem)) {
        var_dump($clientOrderItem->getMessages()[0]->getMessage());
    }

    $orderItem->setClientOrderItem($clientOrderItem);
    if (!$entityManager->save($orderItem)) {
        var_dump($orderItem->getMessages()[0]->getMessage());
    }
}
