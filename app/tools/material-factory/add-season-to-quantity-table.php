<?php

require_once __DIR__ . '/../ToolHelper.php';

$entityManager = $application->getDI()->getEntityManager();

/** @var Classification $classification */
$classification = $entityManager->findOne(Classification::class, [
    'code = :code:',
    'bind' => [
        'code' => 1518
    ]
]);

if ($classification) {
    /** @var MaterialFactoryQuantity[] $materialFactoryQuantities */
    $materialFactoryQuantities = $entityManager->find(MaterialFactoryQuantity::class);

    foreach ($materialFactoryQuantities as $materialFactoryQuantity) {
        $materialFactoryQuantity->setSeasonClassification($classification);
        $entityManager->save($materialFactoryQuantity);
    }
}
