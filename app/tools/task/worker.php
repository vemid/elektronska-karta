<?php

require_once __DIR__ . '/../ToolHelper.php';

/** @var \Vemid\Task\Queue $beanstalkQueue */
$beanstalkQueue = $application->getDI()->getShared('beanstalkQueue');
$db = $application->getDI()->getDb();
$entityManager = $application->getDI()->getEntityManager();
$worker = new \Vemid\Task\Worker($beanstalkQueue, $db, $entityManager);
$worker->run();