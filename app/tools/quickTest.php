<?php

use Vemid\Date\DateRange;
use Vemid\Service\Product\Hydrator;
use Vemid\Service\Workers\Manager;
use Zpl\ZplBuilder;
use Zpl\Fonts\Bematech\Lb1000;
use Vemid\MSSql;
use Vemid\Db\MSSql\Read;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Vemid\Date\DateTime;

include_once __DIR__ . '/ToolHelper.php';

$entityManager = $application->getDI()->getEntityManager();
$config = $application->getDI()->getConfig();

$code = [
    '4231OZ0I10J01'];
$products = $entityManager->find(Product::class,[
    Product::PROPERTY_CODE .' IN ({codes:array})',
    'bind' => [
        'codes' => $code,
    ]
]);

/** @var \Pricelist $eurWholesalePricelist */
$eurWholesalePricelist = $entityManager->findOne(\Pricelist::class, [
    ' code = :code:',
    'bind' => [
        'code' => '01/140000002'
    ]
]);

/** @var \Pricelist $basicPrice */
$basicPrice = $entityManager->findOne(\Pricelist::class, [
    ' code = :code:',
    'bind' => [
        'code' => '01/140000001'
    ]
]);

/** @var Product $product */
foreach ($products as $product) {

    /** @var ProductCalculation $productCalculation */
    $productCalculation = $entityManager->findOne(\ProductCalculation::class,[
        ProductCalculation::PROPERTY_PRODUCT_ID . ' = :product: AND '. ProductCalculation::PROPERTY_STATUS . ' = :status:',
        'bind' => [
            'product' => $product->getId(),
            'status' => ProductCalculation::STATUS_PLANED
        ]
    ]);

    /** @var Hydrator $hydrator */
    $hydrator = new Hydrator($product, $entityManager);

    $prices = $application->getDI()->getProductPrices();

    if($hydrator->getCollection()->getName() == "Basic") {
        $price2 = new Vemid\Service\Product\Price($entityManager, $productCalculation,$prices->getRetailPrice($basicPrice,$product));
        $price2->addPrices();
    }
    else {
        $price2 = new Vemid\Service\Product\Price($entityManager, $productCalculation,$prices->getWholesalePrice($eurWholesalePricelist,$product));
        $price2->addPrices();
    }

}






