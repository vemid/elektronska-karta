<?php

namespace Task;

use Vemid\Mailer\Manager;
use Vemid\Task\AbstractTask;

/**
 * Class Mailer
 *
 * @package Task
 */
class Mailer extends AbstractTask
{

    public $subject;
    public $body;
    public $from;
    public $to;
    public $cc;
    public $bcc;
    public $attachments;


    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        /** @var Manager $mailManager */
        $mailManager = $this->getDI()->get('mailManager');
        $message = $mailManager->createMessage();
        $mailer = $mailManager->getMailer();

        if (\is_array($this->from) && count($this->from) > 0) {
            foreach ($this->from as $fromEmail => $fromName) {
                $message->setFrom($fromEmail, $fromName);
            }
        }

        if (\is_array($this->to) && count($this->to) > 0) {
            foreach ($this->to as $toEmail => $toName) {
                $message->setTo($toEmail, $toName);
            }
        }

        if (\is_array($this->cc) && count($this->cc) > 0) {
            foreach ($this->cc as $ccEmail => $ccName) {
                $message->setCc($ccEmail, $ccName);
            }
        }

        if (\is_array($this->bcc) && count($this->bcc) > 0) {
            foreach ($this->bcc as $bccEmail => $bccName) {
                $message->setBcc($bccEmail, $bccName);
            }
        }

        $message->setSubject($this->subject);
        $message->setBody($this->body);

        if (\is_array($this->attachments) && \count($this->attachments) > 0) {
            foreach ($this->attachments as $attachment) {
                if (file_exists($attachment)) {
                    $message->setAttachment($attachment);
                }
            }
        }

        $message->send();
    }
}
