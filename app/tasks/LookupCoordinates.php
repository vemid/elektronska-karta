<?php

namespace Task;

use Vemid\Entity\EntityInterface;
use Vemid\Task\AbstractTask;
use Geocoder\Model\Address;
use Geocoder\Provider\GoogleMaps as HttpProvider;
use Ivory\HttpAdapter\CurlHttpAdapter as HttpAdapter;

/**
 * Class LookupCoordinates
 *
 * @package Task
 */
class LookupCoordinates extends AbstractTask
{

    /** @var EntityInterface */
    public $entity;

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        if ($this->entity instanceof \TeachingLocation || $this->entity instanceof \Bid) {
            $geocoder = new HttpProvider(new HttpAdapter());
            $location = $this->entity->getLocation();
            if (strlen($location) > 0) {
                /** @var Address $address */
                $address = $geocoder->geocode($location)->first();
                $this->entity->setLatitude($address->getLatitude());
                $this->entity->setLongitude($address->getLongitude());
                $this->entity->save();
            }
        }
    }

}
