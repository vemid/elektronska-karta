<?php

namespace Task;

use Vemid\Entity\EntityInterface;
use Vemid\Task\AbstractTask;
use Geocoder\Model\Address;
use Geocoder\Provider\GoogleMaps as HttpProvider;
use Ivory\HttpAdapter\CurlHttpAdapter as HttpAdapter;

/**
 * Class Geocoder
 *
 * @package Task
 */
class LookupAddress extends AbstractTask
{

    /** @var EntityInterface */
    public $entity;

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        if ($this->entity instanceof \TeachingLocation || $this->entity instanceof \Bid) {
            $geocoder = new HttpProvider(new HttpAdapter());
            $latitude = $this->entity->getLatitude();
            $longitude = $this->entity->getLongitude();

            if ($latitude && $longitude) {
                /** @var Address $address */
                $address = $geocoder->reverse($latitude, $longitude)->first();
                $this->entity->setAddress(trim($address->getStreetName() . ' ' . $address->getStreetNumber()));
                $this->entity->setCity($address->getLocality());
                $country = \Country::findFirst([
                    \Country::PROPERTY_ISO_ALPHA_2 . ' = :code:',
                    'bind' => ['code' => $address->getCountryCode()]
                ]);
                if ($country) {
                    $this->entity->setCountry($country);
                }
                $this->entity->save();
            }
        }

    }

}