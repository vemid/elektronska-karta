<div class="row">
    <div class="col-lg-12">
        <div class="paragraph">
            {{ t('Hi %s,', passwordReset.user.firstName) }}
        </div>
        <div class="paragraph">
            {{ t('We were told that you forgot your password on site %s.', siteSettings.siteName) }}
            {{ t('Do not worry about that, it can happens to everyone.') }}
        </div>
        <div class="divider"></div>
        <div class="paragraph">
            {{ t('To reset your password, please use this url: %s', '<br/>' ~ passwordReset.resetLink) }}
        </div>
    </div>
</div>