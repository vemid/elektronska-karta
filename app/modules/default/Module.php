<?php

use Vemid\Ajax\Handler as AjaxHandler;
use \Vemid\Service\Auth\Handler;
use Vemid\Error\HandlerInterface;
use Vemid\Helper\SiteHelper;
use Vemid\Messenger\Manager as MessengerManager;
use Vemid\Messenger\Message as MessengerMessage;
use Phalcon\DiInterface;
use Phalcon\Events\Event;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Loader;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Mvc\View;
use \Vemid\Service\Email\OutboundEmailManager;
use \Vemid\Entity\Manager\EntityManagerInterface;

/**
 * Class Frontend Module
 *
 * @package Frontend
 */
class Module implements ModuleDefinitionInterface
{

    /**
     * Registers an autoloader related to the module
     *
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new Loader();
        $loader->registerNamespaces([]);
        $loader->registerDirs(array(
            __DIR__ . '/controllers/',
            __DIR__ . '/forms/',
        ));
        $loader->register();
    }

    /**
     * Registers an autoloader related to the module
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di)
    {
        // Init dispatcher
        $this->_initDispatcher($di);

        // Init view
        $this->_initView($di);
    }

    /**
     * @param DiInterface $di
     */
    protected function _initDispatcher(DiInterface $di)
    {
        /** @var EventsManager $eventsManager */
        $eventsManager = $di->getShared('eventsManager');
        /** @var MessengerManager $messengerManager */
        $messengerManager = $di->getShared('notificationManager');
        /** @var HandlerInterface $errorHandler */
        $errorHandler = $di->getShared('errorHandler');

        $eventsManager->attach('dispatch:beforeException',
            function (Event $event, Dispatcher $dispatcher, \Exception $exception) use ($messengerManager, $errorHandler) {
                $messengerManager->appendMessage(new MessengerMessage(
                    $exception->getMessage(),
                    null,
                    MessengerMessage::DANGER
                ));

                switch ($exception->getCode()) {

                    case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND :
                    case Dispatcher::EXCEPTION_ACTION_NOT_FOUND :
                        $dispatcher->forward(array(
                            'controller' => 'errors',
                            'action' => 'show404',
                        ));

                        return false;
                    default :
                        $errorHandler->handleException($exception);

                        $dispatcher->forward(array(
                            'controller' => 'errors',
                            'action' => 'show500',
                        ));

                        return false;
                }
            }
        );

        $currentUser = $di->getShared('currentUser');
        $aclHelper = $di->getShared('aclHelper');
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $di->getShared('entityManager');

        $eventsManager->attach('dispatch:beforeExecuteRoute',
            function (Event $event, Dispatcher $dispatcher) use ($currentUser, $aclHelper) {
                if (!$currentUser && $dispatcher->getActionName() !== 'login') {
                    $dispatcher->forward(array(
                        'controller' => 'auth',
                        'action' => 'login',
                    ));

                    return false;
                }

                $authHandler = new Handler($dispatcher, $aclHelper);
                $authHandler->initialize();
            }
        );

        $eventsManager->attach('dispatch:afterDispatchLoop', function (Event $event, Dispatcher $dispatcher) use ($entityManager) {
            $ajaxHandler = new AjaxHandler();
            $ajaxHandler->handle($dispatcher);

            $outboundEmailManager = new OutboundEmailManager($entityManager, $dispatcher);
            $outboundEmailManager->prepareOutbound();
        });

        $assets = $di->getShared('assets');
        $assetsManager = new \Vemid\Services\Assets\DefaultAssets($assets);
        $eventsManager->attach('dispatch:afterInitialize', function () use ($assetsManager) {
            $assetsManager->initialize();
        });

        $eventsManager->attach('dispatch:afterExecuteRoute', function () use ($assetsManager) {
            $assetsManager->afterExecuteRoute();
        });

        /** @var Dispatcher $dispatcher */
        $dispatcher = $di->getShared('dispatcher');
        $dispatcher->setEventsManager($eventsManager);
    }

    /**
     * @param DiInterface $di
     */
    protected function _initView(DiInterface $di)
    {
        /** @var View $view */
        $view = $di->getShared('view');
        $view->setViewsDir(__DIR__ . '/views/');
        $view->setLayoutsDir('layouts/');
        $view->setTemplateAfter('frontend');
        $view->setPartialsDir('');

        $user = $di->getShared('currentUser');
        $session = $di->getShared('session');
        $currentUser = $session->get('currentUser');
        $locale = $currentUser['locale'] ?? 'en_US';

        /** @var object $config */
        $config = $di->getShared('config');
        $siteHelper = new SiteHelper([
            'baseUri' => $config->application->baseUri,
            'siteName' => $config->application->siteName,
        ]);

        $view->setVar('currentUser', $user);
        $view->setVar('locale', $locale);
        $view->setVar('siteSettings', $siteHelper->toSimpleObject());

        /** @var View\Simple $simpleView */
        $simpleView = $di->getShared('simpleView');
        $simpleView->setViewsDir(__DIR__ . '/views/');
    }

}
