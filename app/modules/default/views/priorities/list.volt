<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj prioritet</h3>
                            <a href="#" onclick="getCreateForm('priorities',event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" id="priorities" >
                        <thead>
                        <tr>
                            <th>{{ t('Naziv Prioriteta') }}</th>
                            <th>{{ t('Broj prioriteta') }}</th>
                            <th>{{ t('Datum') }}</th>
                            <th>{{ t('Kolekcije') }}</th>
                            <th>{{ t('Skica') }}</th>
                            <th>{{ t('Akcija') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for priority in priorities %}
                            <tr>
                                <td>{{ priority.name }}</td>
                                <td>{{ priority.priorityNumber }}</td>
                                <td>{{ priority.priorityDate }}</td>
                                <td>
                                    {% for collection in priority.getPriorityClassifications() %}
                                    {{ collection.getClassification().getName() }} <br>
                                    {% endfor %}
                                </td>
                                <td class="text-center" style="width: 200px !important; vertical-align: center !important;">
                                    {% if priority.getSketch() %}
                                    <img class="thumbnail" src="{{ priority.getSketchPath() }}" width="200px" data-src="{{ priority.getSketchPath() }}">
                                    {% endif %}
                                </td>
                                <td class="text-center" width="11.5%">
                                    <a href="#" onclick="getUpdateForm('priorities', {{ priority.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getDeleteForm('priorities', {{ priority .id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.querySelectorAll('.thumbnail').forEach(function(elem) {

        elem.onclick = function(e) {

            const src = elem.getAttribute('data-src')
            const html = '<img src="' + src + '">'

            basicLightbox.create(html).show()

        }

    })

    $(document).ready( function () {
        $('#priorities').DataTable({
            "pageLength": 50,
            "ordering" : false
        });
    })
</script>