<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Pregled efektus za datum: {{ date.short }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="ibox float-e-margins">
                <div class="row">
                    {% if error %}
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-xs-12 m-t-md">
                                    <div class="alert alert-danger">{{ error }}</div>
                                </div>
                            </div>
                        </div>
                    {% else %}
                        <div class="ibox-title">
                            <h2>Filtriranje<small> Svaka opcija koja bude čeckirana će izbaciti iz liste proizvode koji
                                    su u relaciji</small></h2>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-xs-12 m-t-md">
                                    {{ form }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 m-t-md">
                                    <div class="space-25"></div>
                                    <div class="table-responsive">
                                        <table id="example2" class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th class="col-xs-3">Proizvod</th>
                                                <th class="col-xs-1 text-center">Veličina</th>
                                                <th class="col-xs-3 text-center">{{ t('Za radnju') }}</th>
                                                <th class="col-xs-3 text-center">{{ t('Iz radnje') }}</th>
                                                <th class="col-xs-1 text-center">{{ t('Količina') }}</th>
                                                <th class="col-lg-1 col-sm-2 col-xs-2 text-center">{{ t('Pojedinacno') }}</th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    (function ($) {
        $(document).ready(function () {
            let Storage = window.localStorage;
            let codeIdStorage = JSON.parse(Storage.getItem('codeId'));
            let codeOutStorage = JSON.parse(Storage.getItem('codeOut'));
            let productIdStorage = JSON.parse(Storage.getItem('productId'));
            let seasonStorage = JSON.parse(Storage.getItem('season'));
            let collectionStorage = JSON.parse(Storage.getItem('collection'));
            let priorityStorage = JSON.parse(Storage.getItem('priority'));

            let codeIdEl = $('#codeId'),
                codeOutEl = $('#codeOut'),
                productIdEl = $('#productId'),
                seasonEl = $('#season'),
                collectionEl = $('#collection'),
                priorityEl = $('#priority');

            if (codeIdStorage && codeIdStorage.length > 0 ) {
                $.each(codeIdStorage, function(i,e){
                    $("option[value='" + e + "']", codeIdEl).prop("selected", true);
                });
                codeIdEl.trigger("liszt:updated").trigger("chosen:updated");
            }

            if (codeOutStorage && codeOutStorage.length > 0 ) {
                $.each(codeOutStorage, function(i,e){
                    $("option[value='" + e + "']", codeOutEl).prop("selected", true);
                });
                codeOutEl.trigger("liszt:updated").trigger("chosen:updated");
            }

            if (productIdStorage && productIdStorage.length > 0 ) {
                $.each(productIdStorage, function(i,e){
                    $("option[value='" + e + "']", productIdEl).prop("selected", true);
                });
                productIdEl.trigger("liszt:updated").trigger("chosen:updated");
            }

            if (seasonStorage && seasonStorage.length > 0 ) {
                $.each(seasonStorage, function(i,e){
                    $("option[value='" + e + "']", seasonEl).prop("selected", true);
                });
                seasonEl.trigger("liszt:updated").trigger("chosen:updated");
            }

            if (collectionStorage && collectionStorage.length > 0 ) {
                $.each(collectionStorage, function(i,e){
                    $("option[value='" + e + "']", collectionEl).prop("selected", true);
                });
                collectionEl.trigger("liszt:updated").trigger("chosen:updated");
            }

            if (priorityStorage && priorityStorage.length > 0 ) {
                $.each(priorityStorage, function(i,e){
                    $("option[value='" + e + "']", priorityEl).prop("selected", true);
                });
                priorityEl.trigger("liszt:updated").trigger("chosen:updated");
            }

            const date = '{{ date.format("Y-m-d") }}';
            const table = $("#example2").DataTable({
                "processing": true,
                "serverSide": true,
                "pageLength": 25,
                "ajax": {
                    "url": "/efectus/get-list-data/" + date,
                    // "data":  function ( d ) {
                    //     return $.extend({}, d, {
                    //         "codeId": $('#codeId').val(),
                    //         "codeOut": $('#codeOut').val(),
                    //         "productId": $('#productId').val(),
                    //         "season": $('#season').val(),
                    //         "collection": $('#collection').val(),
                    //         "priority": $('#priority').val()
                    //     });
                    // }
                    "data": function (d) {
                        return $.extend({}, d, {

                        });
                    }
                },
                "columns": [
                    null,
                    {className: "text-center"},
                    {className: "text-center"},
                    {className: "text-center"},
                    {className: "text-center"},
                    {className: "col-lg-1 col-sm-2 col-xs-2 text-center"}
                ],
                "columnDefs": [
                    {
                        "render": function (data, type, row) {
                            return '<input type="checkbox" data-id="' + row[5] + '" name="courier[' + row[5] + ']" value="" id="send-courier" class="form-control bigger-120">';

                        },
                        "targets": 5
                    }
                ],
            });

            $(document).on("change", '.efectus select', function () {
                let codeId = $('#codeId').val(),
                    codeOut = $('#codeOut').val(),
                    productId = $('#productId').val(),
                    season = $('#season').val(),
                    collection = $('#collection').val(),
                    priority = $('#priority').val();

                let form = new FormData();
                form.append('codeId', JSON.stringify(codeId));
                form.append('codeOut', JSON.stringify(codeOut));
                form.append('productId', JSON.stringify(productId));
                form.append('season', JSON.stringify(season));
                form.append('collection', JSON.stringify(collection));
                form.append('priority', JSON.stringify(priority));

                $.ajax({
                    url: "/efectus/set-exclude-by-filter/" + date,
                    type: "post",
                    data: form,
                    dataType: "json",
                    enctype: "multipart/form-data",
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        let Storage = window.localStorage;
                        Storage.setItem('codeId', JSON.stringify(codeId));
                        Storage.setItem('codeOut', JSON.stringify(codeOut));
                        Storage.setItem('productId', JSON.stringify(productId));
                        Storage.setItem('season', JSON.stringify(season));
                        Storage.setItem('collection', JSON.stringify(collection));
                        Storage.setItem('priority', JSON.stringify(priority));

                        table.draw();
                    }
                });
            })
        });

    })(jQuery);
</script>