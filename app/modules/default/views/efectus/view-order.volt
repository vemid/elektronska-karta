<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Efectus za radnju : {{ code }}</h3>
                        </div>
                    </div>
                </div>
                <div class="space-25"></div>
                <div class="row">
                    <div class="col-xs-12 m-t-md">
                        {% if error %}
                        <div class="alert alert-danger">{{ error }}</div>
                        {% else %}
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Proizvod</th>
                                    <th>Veličina</th>
                                    <th class="col-xs-3">Za</th>
                                    <th class="col-xs-2">Količina</th>
                                </tr>
                                <tbody>
                                {% set disable = false %}
                                {% for row in efectus %}
                                    <tr>
                                        <td>{{ row.productSize.product }}</td>
                                        <td>{{ row.productSize.code }}</td>
                                        <td>{{ row.getCodeTo() }}</td>
                                        <td>
                                            {% if row.warehouseOrderItem.warehouseOrder.pushedToMis %}
                                            {% set disable = true %}
                                            <input type="text" value="{{ row.warehouseOrderItem.quantity }}" disabled readonly>
                                            {% else %}
                                                <input class="woi-efectus" data-woi="{{ row.warehouseOrderItem.id }}" type="text" name="qty"
                                                   id="qty" value="{{ row.warehouseOrderItem.quantity }}">
                                            {% endif %}
                                        </td>
                                    </tr>
                                {% endfor %}
                                </tbody>
                                </thead>
                            </table>
                            {% endif %}
                        </div>
                    </div>
                </div>
                {% if not disabled %}
                <div class="row">
                    <div class="col-xs-12 m-t-md">
                        <a href="#"  id="save-efectus-order-items" class="btn btn-xl btn-success">Snimi</a>
                    </div>
                </div>
                {% endif %}
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function ($) {
        $("table input").each(function (index, element) {
            $(element).on("change paste input", function() {
                const qty = $(this).val();
                let $this = $(this);
                $this.removeClass("borderBlink");

                let form = new FormData();
                form.append('id', $(this).attr('data-woi'));
                form.append('quantity', qty);

                $.ajax({
                    url: "/Warehouse-Order-Items/efectus-Item/",
                    type: "post",
                    data: form,
                    dataType: "json",
                    enctype: "multipart/form-data",
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data.error) {
                            renderMessages(data.messages, "danger");
                        } else {
                            console.log($this);
                            $this.addClass("borderBlink");
                        }
                    }
                });
            });
        });

        $(document).on("click", "#save-efectus-order-items", function(){
            let formData = [];

            $("table input").each(function (index, element) {
                formData.push($(element).attr("data-woi"));
            });

            $.post( "/warehouse-order-items/efectus-push-to-mis", { 'woi[]': formData} )
            .done( function( data ) {
                // window.location = data.url;
                // $("#modal").hide();
                // $("body").removeClass("modal-open");
                // initTablsOnLoad();
                // initSwitch();
            });
        });

    })(jQuery);
</script>