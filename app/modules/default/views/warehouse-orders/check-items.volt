<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row" id="loaded-html">
                    <div class="col-xs-12 m-t-md">
                        <div class="feed-activity-list">
                            <input type="hidden" id="warehouse-order-id" name="warehouse-order-id" value="{{ warehouseOrder.id }}">
                            <div class="feed-element">
                                <div class="col-lg-3 col-xs-3 bigger-120"><b>Proizvod</b></div>
                                <div class="col-lg-1 col-xs-1 text-center bigger-120"><b>Veličina</b></div>
                                <div class="col-lg-1 col-xs-1 text-center bigger-120"><b>Brojač</b></div>
                                <div class="col-lg-1 col-xs-1 text-center bigger-120"><b>Poruceno</b></div>
                                <div class="col-lg-1 col-xs-1 text-center bigger-120"><b>Vec spakovano</b></div>
                                <div class="col-lg-1 col-xs-1 text-center bigger-120"><b>Ostalo za pakovanje</b></div>
                                <div class="col-lg-3 col-xs-4 text-center bigger-120"><b>Razlog</b></div>
                            </div>
                            {% for size, item in items %}
                                {% if item['ordered'] == 0 %}
                                    {% continue %}
                                {% endif %}
                                {% if item['ordered'] == 0 %}
                                    {% set class = 'success' %}
                                {% elseif item['ordered'] < 0  %}
                                    {% set class = 'danger' %}
                                {% else %}
                                    {% set class = 'warning' %}
                                {% endif %}
                                <div class="feed-element no-padding alert alert-{{ class }} m-t-none m-b-xs p-xs">
                                    <div class="col-lg-3 col-xs-3">{{ item['name'] }}</div>
                                    <div class="col-lg-1 col-xs-1 text-center">{{ item['size'] }}</div>
                                    <div class="col-lg-1 col-xs-1 text-center">{{ item['item'] }}</div>
                                    <div class="col-lg-1 col-xs-1 text-center">{{ item['qty'] }}</div>
                                    <div class="col-lg-1 col-xs-1 text-center">{{ item['alreadyPacked'] }}</div>
                                    <div class="col-lg-1 col-xs-1 text-center">{{ item['ordered'] }}</div>
                                    <div class="col-lg-4 col-xs-4 text-center">
                                        {% if item['ordered'] != 0 %}
                                            <textarea class="form-control reasonNote" name="reason[{{ item['id'] }}]" id="" rows="2" style="width: 100%"></textarea>
                                        {% endif %}
                                    </div>
                                </div>
                            {% endfor %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>