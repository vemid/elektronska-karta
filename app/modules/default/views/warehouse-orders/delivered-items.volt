<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Zaglavlje za <b>{{ warehouseOrder.getChannelSupplyEntity() }}</b></h3>
                        </div>
                    </div>
                </div>
                <div class="space-25"></div>
                <div class="row">
                    <div class="col-xs-12 m-t-md">
                        <div class="feed-activity-list">
                        {% if warehouseOrder.getWarehouseOrderItems()|length > 0 %}
                            <div class="feed-element">
                                <div class="col-lg-6 col-xs-4 bigger-120"><b>Proizvod</b></div>
                                <div class="col-lg-2 col-xs-2 text-center bigger-120"><b>Veličina</b></div>
                                <div class="col-lg-2 col-xs-2 text-center bigger-120"><b>Poslato</b></div>
                                <div class="col-lg-1 col-xs-2 text-center bigger-120"><b>Očitano</b></div>
                                <div class="col-lg-1 col-xs-2 t-center bigger-120"><b>Validno</b></div>
                            </div>

                                {% for warehouseOrderItem in warehouseOrder.getWarehouseOrderItems() %}
                                <div class="feed-element alert alert-danger m-t-none m-b-xs p-xs" id="{{ warehouseOrderItem.productSize.barcode|lower }}">
                                    <div class="col-lg-6 col-xs-4">{{ warehouseOrderItem.getProductSize().getProduct().name }}</div>
                                    <div class="col-lg-2 col-xs-2 text-center">{{ warehouseOrderItem.getProductSize().getCode().name }}</div>
                                    <div class="col-lg-2 col-xs-2 text-center" id="stored-counter">{{ warehouseOrderItem.quantity }}</div>
                                    <div class="col-lg-1 col-xs-2 text-center" id="row-counter">0</div>
                                    <div class="col-lg-1 col-xs-2 text-center">
                                        <i class="fa fa-times bigger-140 text-danger"></i>
                                    </div>
                                </div>
                            {% endfor %}
                            <div class="feed-element"><div class=""><a onclick="return false;" id="save-delivered-warehouse-order-items" class="btn btn-label btn-success">Potvrdi isporučene stavke</a></div></div>

                        {% endif %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form action="" id="barcode-form">
    <input type="hidden" id="warehouse-order-id" name="warehouse-order-id" value="{{ warehouseOrder.id }}">
    <input type="text" id="deliver-barcode-warehouse" name="deliver-barcode-warehouse">
    <button type="submit" id="barcode-submit">tet</button>
</form>

<script type="text/javascript">
    (function ($) {
        window.onload = function() {
            let element = document.getElementById("deliver-barcode-warehouse");
            if (element) {
                element.focus();
            }
        }

        $(document).on("click", "body", function(event){
            let $barcode = $('#deliver-barcode-warehouse');
            if ($barcode.length > 0 ) {
                $barcode.focus();
            }
        });

        let warehouseOrderId = $("#warehouse-order-id").val();
        let barcodeStorage = window.localStorage;
        let warehouseOrderStorage = barcodeStorage.getItem(warehouseOrderId);
        let storedBarcodes = JSON.parse(warehouseOrderStorage);
        for (let barcode in storedBarcodes) {
            let $1 = $("#" + barcode.toLowerCase());
            let storedItems = $1.find("#stored-counter").html();
            storedItems = parseInt(storedItems);
            $1.find("#row-counter").empty().html(storedBarcodes[barcode]);

            if (storedBarcodes[barcode] > 0) {
                let lastDiv = $("div:last-child", $1);
                let minus = '<i class="fa fa-minus bigger-140 text-danger" onclick="return false;" id="decrease-woi" data-id="'+ barcode +'" style="cursor: pointer">&nbsp;</i>&nbsp;';
                lastDiv.prepend(minus);
            }

            if (storedItems === storedBarcodes[barcode]) {
                $1.removeClass("alert-danger").addClass("alert-success");
                $("div:last-child", $1).find("i").removeClass("fa-times text-danger").addClass("fa-check text-success");
            } else if (storedItems < storedBarcodes[barcode]) {
                $1.removeClass("alert-danger");
                $1.addClass("alert-warning");
                let find = $("div:last-child", $1).find("i");
                find.removeClass("text-danger");
                find.addClass("text-warning");
            }
        }

        $(document).on("focusout", "#deliver-barcode-warehouse", function () {
            let warehouseOrderId = $("#warehouse-order-id").val();
            let barcodeStorage = window.localStorage;
            let warehouseOrderStorage = barcodeStorage.getItem(warehouseOrderId);
            let storedBarcodes = JSON.parse(warehouseOrderStorage);

            let barcode = $(this).val();
            if (!barcode) {
                return false;
            }

            $(this).focus().val("");

            if(!storedBarcodes) {
                storedBarcodes = {};
                storedBarcodes[barcode] = 1;
            } else {
                if (typeof storedBarcodes[barcode] === 'undefined') {
                    storedBarcodes[barcode] = 1;
                } else {
                    storedBarcodes[barcode]++;
                }
            }

            barcodeStorage.setItem(warehouseOrderId, JSON.stringify(storedBarcodes));
            let $parent = $("#" + barcode.toLowerCase());
            $parent.find("#row-counter").empty().html(storedBarcodes[barcode]);
            let storedItems = $parent.find("#stored-counter").html();
            storedItems = parseInt(storedItems);

            let lastDiv = $("div:last-child", $parent);
            let alreadyThere = $("#decrease-woi", lastDiv);
            if (alreadyThere.length === 0) {
                let minus = '<i class="fa fa-minus bigger-140 text-danger" onclick="return false;" data-id="'+ barcode +'" id="decrease-woi" style="cursor: pointer">&nbsp;</i>&nbsp;';
                lastDiv.prepend(minus);
            }

            if (storedItems === storedBarcodes[barcode]) {
                $parent.removeClass("alert-danger").addClass("alert-success");
                $("div:last-child", $parent).find("i").not("#decrease-woi").removeClass("fa-times text-danger").addClass("fa-check text-success");
            } else if (storedItems < storedBarcodes[barcode]) {
                $parent.removeClass("alert-success").addClass("alert-warning");
                $("div:last-child", $parent).find("i").removeClass("fa-check text-success").addClass("fa-times text-warning");
            }

            $(this).focus().val("");
        });


        $(document).on("click", "#save-delivered-warehouse-order-items", function(){
            let modal = $("#modal");

            let body =
                '<div class="form-horizontal">' +
                    '<div class="form-group">' +
                    '<label class="col-sm-2 control-label no-padding-right">Poruka:</label>' +
                        '<div class="col-xs-10">' +
                            '<textarea id="warehouse-items-delivered-note" class="form-control" rows="7"></textarea>' +
                        '</div>' +
                    '</div>' +
                '</div>';

            $(".modal-dialog", modal).css('width', '60%')
            $(modal).find(".modal-title").html("<i class='fa fa-check'></i> Isporučene stavke");
            $(modal).find(".modal-body").html(body);
            $(modal).find("#save-modal").attr("id", "report-delivered-warehouse-order-items");

            $("body").addClass("modal-open");
            $(modal).show();
        });

        $(document).on("click", "#report-delivered-warehouse-order-items", function() {
            let warehouseOrderId = $("#warehouse-order-id").val();
            let barcodeStorage = window.localStorage;
            let warehouseOrderStorage = barcodeStorage.getItem(warehouseOrderId);
            let storedBarcodes = JSON.parse(warehouseOrderStorage);

            let formData = new FormData();
            formData.append('note', $("#warehouse-items-delivered-note").val());

            for ( let key in storedBarcodes ) {
                formData.append('code[' + key + ']', storedBarcodes[key]);
            }

            sendPostAjaxCall('/warehouse-orders/store-delivered-items/' + warehouseOrderId, formData);
            barcodeStorage.removeItem(warehouseOrderId);
        });

        $(document).on("click", "#decrease-woi", function (){
            let warehouseOrderId = $("#warehouse-order-id").val();
            let barcodeStorage = window.localStorage;
            let warehouseOrderStorage = barcodeStorage.getItem(warehouseOrderId);
            let storedBarcodes = JSON.parse(warehouseOrderStorage);
            let barcode = $(this).attr("data-id");
            let $1 = $("#" + barcode.toLowerCase());

            if (!storedBarcodes) {
                alert('Greska se desila!');
            }

            storedBarcodes[barcode]--;
            if (storedBarcodes[barcode] >= 0) {
                $1.find("#row-counter").empty().html(storedBarcodes[barcode]);
                barcodeStorage.setItem(warehouseOrderId, JSON.stringify(storedBarcodes));
            }

            let storedItems = $1.find("#stored-counter").html();
            storedItems = parseInt(storedItems);
            console.log($1, storedItems);

            if (storedBarcodes[barcode] === 0) {
                $(this).remove();
            }

            if (storedItems === storedBarcodes[barcode]) {
                $1.removeClass("alert-warning").addClass("alert-success");

                $("div:last-child", $1).find("i").removeClass("fa-times text-danger").addClass("fa-check text-success");

            } else if (storedItems < storedBarcodes[barcode]) {
                $1.removeClass("alert-danger");
                $1.addClass("alert-warning");
                let find = $("div:last-child", $1).find("i");
                find.removeClass("text-danger");
                find.addClass("text-warning");
            } else if (storedItems > storedBarcodes[barcode]) {
                $1.removeClass("alert-warning");
                $1.addClass("alert-danger");
                let find = $("div:last-child", $1).find("i");
                find.removeClass("text-warning");
                find.addClass("text-danger");
            }
        });

    })(jQuery);
</script>