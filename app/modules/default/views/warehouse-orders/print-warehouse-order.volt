<style>
    h1 {
        color: navy;
        font-family: times;
        font-size: 18pt;
        text-decoration: underline;
    }

    p.first {
        color: #003300;
        font-family: helvetica;
        font-size: 12pt;
    }

    p.first span {
        color: #006600;
        font-style: italic;
    }

    p#second {
        color: rgb(00, 63, 127);
        font-family: times;
        font-size: 12pt;
        text-align: justify;
    }

    p#second > span {
        background-color: #FFFFAA;
    }

    table {
        border-collapse: separate;
        width: 100%;
    }

    table th {
        padding: 8px 5px;
        text-align: right;
        border-bottom: 4px solid #487774;
        background: #E6E7E7;

    }

    table td {
        text-align: right;
        border-right: 1px solid #CCCCCF;
    }


    .currency {
        border-bottom: 4px solid #487774;
        padding: 0 20px;
    }

    .currency span {
        font-size: 11px;
        font-style: italic;
        color: #8b8b8b;
        display: inline-block;
        min-width: 20px;
    }


</style>

<img src="<?php echo APP_PATH; ?>public/img/memo.png" alt="">
<div>
    <table style="text-align: center !important;">
        <tr>
            <td style="text-align: left !important; border-right: none !important;">
                <br/>Posiljalac :
                <br/>KIDS BEBA DOO
                <br/><span>Ignjata Joba 37</span>
                <br/><span>Belgrade Serbia</span>
                <br/><span>PIB: 101743849</span>
                <br/><span><tcpdf method="write1DBarcode" params="{{ params }}"/></span>
                <br/>
                <br/><span><b>Bruto: {{ warehouseOrder.bruto }} kg</b></span>
                <br/><span><b>Neto: {{ warehouseOrder.neto }} kg</b></span>
            </td>
            <td style="border: none !important">
                <span style="font-size: 20px; margin-left: -50px !important;"> {{ warehouseOrder.getType() == "B" ? "D" : "" }} </span>
            </td>
            <td style="border: 2px solid #487774; !important;text-align: center !important;" colspan="2">
                <br/><span style="font-size: 6px;text-align: right !important;">Prevnozik:D Express doo,Zage Malivuk 1,Beograd</span>
                {% if warehouseOrder.getChannelSupplyEntity().getImporter() %}
                    <br/>Uvoznik: {{ warehouseOrder.getChannelSupplyEntity().getImporter() }}
                    <br/>BEBAKIDS: {{ warehouseOrder.getChannelSupplyEntity().getDisplayName() }}
                {% else %}
                    <br/>BEBAKIDS: {{ warehouseOrder.getChannelSupplyEntity().getDisplayName() }}
                    <br/><span>{{ warehouseOrder.getChannelSupplyEntity().getPostalAddress() }}</span>
                    <br/><span>Tel: {{ warehouseOrder.getChannelSupplyEntity().getWorkerPhone() }}</span>
                {% endif %}
                <br/><span>
                        <tcpdf method="write1DBarcode" params="{{ courierParams }}"/>
                    </span>
                <br/><span style="font-size: 8px; padding-top: -15px !important;">Referentni broj:12345
                    <br/>Uslugu placa:primalac/virman
                    <br/>Otkupnina:0
                    <br/>Sadrzaj:garderoba
                    <br/>Masa:cca 20kg
                </span>
            </td>
        </tr>
    </table>
</div>
<div style="text-align: center;font-size: 18px">
    <span>Nalog za otpremu robe : <b>{{ warehouseOrder.warehouseOrderCode }} <br/> ({{ warehouseOrder.getPriority() }})</b></span>
</div>
<div style="clear:both">
    <section id="items">
        <table cellpadding="0" cellspacing="0">

            <tr>
                <th width="10%">#</th> <!-- Dummy cell for the row number and row commands -->
                <th width="25%">Šifra</th>
                <th width="45%">Naziv</th>
                <th width="10%">Velicina</th>
                <th width="10%">Qty</th>
            </tr>
            {% set total = 0 %}
            {% set totalQty = 0 %}
            {% set rbr = 1 %}
            {% for row in data %}
                <tr data-iterate="item" style="font-size: 12px !important;">
                    {# {% set ammount = row["qty"] * row["price"] %} #}
                    <td width="10%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ rbr }}</td>
                    <td width="25%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ row['sku'] }}</td>
                    <td width="45%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ row["name"] }}</td>
                    <td width="10%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ row["size"] }}</td>
                    <td width="10%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ row["qty"] }}</td>
                </tr>
                {% set rbr = rbr+1 %}
                {% set totalQty = totalQty + row["qty"] %}
            {% endfor %}


        </table>

    </section>

    <div class="currency">
        <span></span> <span></span>
    </div>
    <div style="margin-bottom: 5px !important;">
        <table cellpadding="0" cellspacing="0" style="background: none !important">

            <tr style="background: none !important;border-bottom: 2px solid white; !important;">
                <td width="70%" style="text-align:left !important; padding-top: 5px !important;">
                </td>
                <td width="15%"
                    style="background-color: #8BA09E !important;border-bottom: 2px solid white; !important;">Ukupna
                    kolicina
                </td>
                <td width="15%"
                    style="background-color: #8BA09E !important;border-bottom: 2px solid white; !important;">{{ totalQty }}</td>
            </tr>
        </table>
    </div>
</div>
</div>