<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">

                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj zaglavlje</h3>
                            <a href="#"
                               onclick="getCustomCreateForm('warehouse-orders', 'channelSupplyId', {{ paramId }}, event)"
                               class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-20 clear clearfix"></div>
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-table-1">{{ t('Nalozi') }}</a></li>
                        <li><a data-toggle="tab" href="#tab-table-2">{{ t('Nalozi sa slanje kurirskom') }}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-table-1" class="tab-pane active">
                            <div class="panel-body">
                                <input type="hidden" name="channelSupplyId" id="channelSupplyId"
                                       value="{{ channelSupplyId }}">
                                <div class="space-25"></div>
                                <div class="table-responsive">
                                    <table id="example" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-xs-2">{{ t('MP/VP') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('Planirani izlazak') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('MIS kod') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('Status') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('Kurirska') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('Post barcode') }}</th>
                                            <th class="col-lg-1 col-sm-2 col-xs-2 text-center">{{ t('Akcije') }}</th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="tab-table-2" class="tab-pane">
                            <div class="panel-body">
                                <input type="hidden" name="channelSupplyId" id="channelSupplyId"
                                       value="{{ channelSupplyId }}">
                                <input type="hidden" name="courier" id="courier"
                                       value="1">
                                <div class="space-25"></div>
                                <div class="table-responsive">
                                    <table id="example2" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-xs-2">{{ t('MP/VP2') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('Planirani izlazak') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('MIS kod') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('Status') }}</th>
                                            <th class="col-xs-1 text-center">{{ t('Post barcode') }}</th>
                                            <th class="col-lg-1 col-sm-2 col-xs-2 text-center">{{ t('Akcije') }}</th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .switchery > span {
        margin-left: 25px;
        line-height: 28px;
        font-family: tahoma;
        color: white;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        const isSuperAdmin = '{{ currentUser.isSuperAdmin() }}';

        var table = $("#example").DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 25,
            "ajax": {
                "url": "/warehouse-orders/get-list-data",
                "data": function (d) {
                    return $.extend({}, d, {
                        "channelSupplyId": $('#channelSupplyId').val(),
                    });
                }
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (aData[8]) {
                    $('td', nRow).closest('tr').css('background', 'lightcoral');
                }
                else if (aData[9]) {
                    $('td', nRow).closest('tr').css('background', 'lightgreen');
                }

                return nRow;
            },
            "columns": [
                null,
                null,
                {className: "text-center"},
                {className: "text-center"},
                {className: "text-center"},
                {className: "text-center"},
                {className: "col-lg-1 col-sm-2 col-xs-2 text-center"}
            ],
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        let html = '<a href="/warehouse-orders/add-items/' + row[6] + '" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search-plus"></i></button></a>' +
                            '<a href="/warehouse-orders/print-warehouse-order/' + row[6] + '" class="bigger-140 text-danger"><button class="btn btn-danger btn-circle" type="button" title="Stampa"><i class="fa fa-print"></i></button></a>'+
                            '<a href="#" onclick="getForm(\'/warehouse-orders/add-weight-form/' + row[6] + '\', \'/warehouse-orders/add-weight/' + row[6] + '\', event)" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Tezina paketa"><i class="fa fa-balance-scale"></i></button></a>'
                        return html;
                    },
                    "targets": 6
                },
                {
                    "render": function (data, type, row) {
                        if (row[3] === 'SENT') {
                            return 'Poslato';
                        }

                        let checked = row[3] === 'LOCKED';
                        let attributes = '';
                        if (checked) {
                            attributes = ' checked ';
                            if (!isSuperAdmin) {
                                attributes = ' checked disabled ';
                            }
                        }

                        let pushed = row[7];
                        let pushedHtml = "";

                        if (pushed === '0') {
                            pushedHtml = '<span>Otkljucan </span><input type="checkbox" data-id="' + row[6] + '" name="status[' + row[6] + ']" value="" id="wo-status"' + attributes + 'class="form-control js-switch-lock"><span> Zakljucan</span>';
                        } else {
                            pushedHtml = '<span>Kreiran grupni nalog</span>';
                        }
                        return pushedHtml;

                    },
                    "targets": 3
                },
                {
                    "render": function (data, type, row) {
                        let attributes = '';
                        if (row[4]) {
                            attributes = ' checked ';
                            if (!isSuperAdmin) {
                                attributes = ' checked disabled ';
                            }
                        }

                        return '<span>Ne </span><input type="checkbox" data-id="' + row[6] + '" name="useCourierService[' + row[6] + ']" value="" id="useCourierService"' + attributes + 'class="form-control js-switch-lock"><span> Da</span>';
                    },
                    "targets": 4
                }
            ],
            "fnDrawCallback": function () {
                $(".js-switch-lock").each(function (key, elem) {
                    if (!elem.hasAttribute("data-switchery")) {
                        let check = elem;
                        let switchery = new Switchery(elem, {color: "#1AB394"});
                        let warehouseId = $(elem).attr("data-id");

                        elem.onchange = function () {
                            let $1 = $(check);
                            $1.attr('checked', check.checked);
                            $1.val(check.checked ? 1 : 0);
                            let data = new FormData();

                            if ($1.attr('id') === 'wo-status') {
                                data.append('status', check.checked);
                                sendPostAjaxCall("/warehouse-orders/set-status/" + warehouseId, data);
                            } else {
                                data.append('useCourierService', check.checked);
                                sendPostAjaxCall("/warehouse-orders/set-use-courier-service/" + warehouseId, data);
                            }
                        }

                        if (elem.hasAttribute("disabled")) {
                            switchery.disable();
                        }
                    }
                });
            },
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        const isSuperAdmin = '{{ currentUser.isSuperAdmin() }}';

        var table = $("#example2").DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 25,
            "ajax": {
                "url": "/warehouse-orders/get-list-data",
                "data": function (d) {
                    return $.extend({}, d, {
                        "channelSupplyId": $('#channelSupplyId').val(),
                        "courier": $('#courier').val()
                    });
                }
            },
            "columns": [
                null,
                null,
                {className: "text-center"},
                {className: "text-center"},
                {className: "text-center"},
                {className: "col-lg-1 col-sm-2 col-xs-2 text-center"}
            ],
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return row[5];
                    },
                    "targets": 4
                },
                {
                    "render": function (data, type, row) {
                        let html = '<a href="/warehouse-orders/add-items/' + row[6] + '" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search-plus"></i></button></a>' +
                            '<a href="/warehouse-orders/print-warehouse-order/' + row[6] + '" class="bigger-140 text-danger"><button class="btn btn-danger btn-circle" type="button" title="Stampa"><i class="fa fa-print"></i></button></a>';
                        return html;
                    },
                    "targets": 5
                },
                {
                    "render": function (data, type, row) {
                        if (row[3] === 'SENT') {
                            return 'Poslato';
                        }

                        let checked = row[3] === 'LOCKED';
                        let attributes = '';
                        if (checked) {
                            attributes = '';
                            if (!isSuperAdmin) {
                                attributes = ' checked disabled ';
                            }
                        }

                        return '<input type="checkbox" data-id="' + row[6] + '" name="courier[' + row[6] + ']" value="" id="send-courier"' + attributes + 'class="form-control js-switch-lock">';

                    },
                    "targets": 3
                }
            ],
            "fnDrawCallback": function () {
                $(".js-switch-lock").each(function (key, elem) {
                    if (!elem.hasAttribute("data-switchery")) {
                        let check = elem;
                        let switchery = new Switchery(elem, {color: "#1AB394"});
                        let warehouseId = $(elem).attr("data-id");

                        elem.onchange = function () {
                            $(check).attr('checked', check.checked);
                            $(check).val(check.checked ? 1 : 0);
                            let data = new FormData();
                            data.append('courier', check.checked);
                            sendPostAjaxCall("/warehouse-orders/add-shipment/" + warehouseId, data);
                        }

                        if (elem.hasAttribute("disabled")) {
                            switchery.disable();
                        }
                    }
                });
            }
        });
    });
</script>