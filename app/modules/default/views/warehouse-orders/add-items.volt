<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Zaglavlje za <b>{{ warehouseOrder.getChannelSupplyEntity() }}</b></h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <table class="table" id="barcode-table">
                            <tbody>
                            <tr>
                                <td>{{ t('Planned exit date') }}</td>
                                <td>
                                    <span class="bigger-120">{{ warehouseOrder.planedExitDate.short }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td>{{ t('note') }}</td>
                                <td>
                                    <span class="bigger-120">{{ warehouseOrder.note }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td>{{ t('Warehouse order code') }}</td>
                                <td>
                                    <span class="bigger-120">{{ warehouseOrder.warehouseOrderCode }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <span class="bigger-120"></span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6">
                        <div class="">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>{{ t('Mis code') }}</td>
                                    <td>
                                        <span class="bigger-120">{{ warehouseOrder.warehouseOrderCode }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ t('Postal barcode') }}</td>
                                    <td>
                                        <span class="bigger-120">{{ warehouseOrder.postalBarcode }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <span class="bigger-120"></span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="space-25"></div>
                <div class="row">
                    <div class="col-xs-12 m-t-md">
                        <div class="feed-activity-list">
                        {% if warehouseOrder.getWarehouseOrderItems()|length == 0 %}
                            <table class="table warehouse-order-item-table">
                                <tbody>
                                <tr>
                                    <td class="text-center">
                                        <div class="col-xs-12" id="barcode-info-container">
                                            <p>{{ t('Stavke su prazne') }}</p>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="bar-container">
                                                <div class="l1"></div>
                                                <div class="l2"></div>
                                                <div class="l3"></div>
                                                <div class="l4"></div>
                                                <div class="l5"></div>
                                                <div class="l6"></div>
                                                <div class="l7"></div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        {% else %}
                            <div class="feed-element">
                                <div class="col-lg-6 col-xs-5 bigger-120"><b>Proizvod</b></div>
                                <div class="col-lg-2 col-xs-2 text-center bigger-120"><b>Veličina</b></div>
                                <div class="col-lg-2 col-xs-2 text-center bigger-120"><b>Brojač</b></div>
                                <div class="col-lg-2 col-xs-2 text-center bigger-120"><b>Akcija</b></div>
                            </div>

                                {% for warehouseOrderItem in warehouseOrder.getWarehouseOrderItems() %}
                                <div class="feed-element  no-padding" id="{{ warehouseOrderItem.productSize.barcode|lower }}">
                                    <div class="col-lg-6 col-xs-5">{{ warehouseOrderItem.getProductSize().getProduct() }}</div>
                                    <div class="col-lg-2 col-xs-2 text-center">{{ warehouseOrderItem.getProductSize().getCode().name }}</div>
                                    <div class="col-lg-2 col-xs-2 text-center" id="row-counter">{{ warehouseOrderItem.quantity }}</div>
                                    <div class="col-lg-2 col-xs-2 text-center">
                                        {% if warehouseOrder.isEditable() %}
                                            <a id="remove-saved-warehouse-order-item" class="btn btn-danger btn-circle" onclick="getDeleteWarehouseItem({{ warehouseOrder.id }}, '{{ warehouseOrderItem.productSize.barcode }}', {{ warehouseOrderItem.id }}, event)"><i class="fa fa-times"></i></a>
                                        {% endif %}
                                    </div>
                                </div>
                            {% endfor %}
                            {% if warehouseOrder.isEditable() %}
                                <div class="feed-element">
                                    <div class="col-lg-12 m-t-md">
                                        <a onclick="return false;" id="check-warehouse-order-items" class="btn btn-label btn-success">Snimi stavke</a>
                                    </div>
                                </div>
                            {% endif %}
                        {% endif %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{% if warehouseOrder.isEditable() %}
<form action="" id="barcode-form">
    <input type="hidden" id="warehouse-order-id" name="warehouse-order-id" value="{{ warehouseOrder.id }}">
    <input type="text" id="barcode-warehouse" name="barcode-warehouse">
    <button type="submit" id="barcode-submit">tet</button>
</form>
{% endif %}

<script type="text/javascript">
    (function ($) {
        $(document).on("click", "body:not(input, textarea)", function(event){
            if (event.target.tagName.toLowerCase() === 'textarea' || event.target.tagName.toLowerCase() === 'select') {
                return false;
            }

            let $barcode = $('#barcode-warehouse');
            if ($barcode.length > 0 ) {
                $barcode.focus();
            }
        });

        window.onload = function() {
            let element = document.getElementById("barcode-warehouse");
            if (element) {
                element.focus();
            }
        }

        let isEditable = {{ warehouseOrder.isEditable() }};
        let alreadyStored = {{ warehouseOrder.getWarehouseOrderItems()|length }};
        let serverStoredBarcodes = JSON.parse('{{ serverStoredBarcodes }}');
        let warehouseOrderId = $("#warehouse-order-id").val();
        let barcodeStorage = window.localStorage;
        let warehouseOrderStorage = barcodeStorage.getItem(warehouseOrderId);
        let storedBarcodes = JSON.parse(warehouseOrderStorage);
        if (!alreadyStored) {
            if (warehouseOrderStorage && storedBarcodes && Object.keys(storedBarcodes).length > 0) {
                $(".warehouse-order-item-table").remove();
                let header =
                    '<div class="feed-element">' +
                        '<div class="col-lg-6 col-xs-5 bigger-120"><b>Proizvod</b></div>' +
                        '<div class="col-lg-2 col-xs-2 text-center bigger-120"><b>Veličina</b></div>' +
                        '<div class="col-lg-2 col-xs-2 text-center bigger-120"><b>Brojač</b></div>' +
                        '<div class="col-lg-2 col-xs-2 text-center bigger-120"><b>Akcija</b></div>' +
                    '</div>';

                $(".feed-activity-list").append(header);

                for (let barcode in storedBarcodes) {
                    let counter = storedBarcodes[barcode];
                    $.get("/products/get-product-by-barcode/" + barcode, function(response) {
                        let row =
                            '<div class="feed-element no-padding" id="'+ barcode.toLowerCase() + '">' +
                                '<div class="col-lg-6 col-xs-5">' + response.product + '</div>' +
                                '<div class="col-lg-2 col-xs-2 text-center">' + response.size + '</div>' +
                                '<div class="col-lg-2 col-xs-2 text-center" id="row-counter">' + counter + '</div>' +
                                '<div class="col-lg-2 col-xs-2 text-center">' +
                                    '<a id="decrease-warehouse-order-item" class="btn btn-info btn-circle" onclick="return false;" data-id="'+ barcode +'"><i class="fa fa-minus"></i></a>' +
                                    '<a id="remove-warehouse-order-item" class="btn btn-danger btn-circle" onclick="return false;" data-id="'+ barcode +'"><i class="fa fa-times"></i></a>' +
                                '</div>' +
                            '</div>';

                        $(".feed-activity-list > div:eq(0)").after(row);
                    });
                }

                if (isEditable) {
                    let footer =
                        '<div class="feed-element">' +
                            '<div class="col-lg-12 m-t-md">' +
                                '<a onclick="return false;" id="check-warehouse-order-items" class="btn btn-label btn-success">Snimi stavke</a>' +
                            '</div>' +
                        '</div>';

                    $(".feed-activity-list").append(footer);
                }
            }
        } else {
            if (storedBarcodes) {
                for (let barcode in storedBarcodes) {
                    let $1 = $("#" + barcode.toLowerCase());

                    if ($1.length === 0) {
                        let counter = storedBarcodes[barcode];
                        $.get("/products/get-product-by-barcode/" + barcode, function(response) {
                            let row =
                                '<div class="feed-element no-padding" id="'+ barcode.toLowerCase() + '">' +
                                '<div class="col-lg-6 col-xs-5">' + response.product + '</div>' +
                                '<div class="col-lg-2 col-xs-2 text-center">' + response.size + '</div>' +
                                '<div class="col-lg-2 col-xs-2 text-center" id="row-counter">' + counter + '</div>' +
                                '<div class="col-lg-2 col-xs-2 text-center">' +
                                '<a id="decrease-warehouse-order-item" class="btn btn-info btn-circle" onclick="return false;" data-id="'+ barcode +'"><i class="fa fa-minus"></i></a>' +
                                '<a id="remove-warehouse-order-item" class="btn btn-danger btn-circle" onclick="return false;" data-id="'+ barcode +'"><i class="fa fa-times"></i></a>' +
                                    '</div>' +
                                '</div>';

                            $(".feed-activity-list > div:eq(0)").after(row);
                        });
                    } else {
                        $1.find("#row-counter").empty().html(storedBarcodes[barcode]);

                        if (serverStoredBarcodes.hasOwnProperty(barcode.toLowerCase())) {
                            let serverStoredQty = serverStoredBarcodes[barcode.toLowerCase()];
                            if (parseInt(serverStoredQty) !== parseInt(storedBarcodes[barcode])) {
                                let minusDiv = $("#" + barcode).find("#decrease-warehouse-order-item");
                                if (minusDiv.length === 0) {
                                    $("#" + barcode).find("#row-counter").next().append('<a id="decrease-warehouse-order-item" class="btn btn-info btn-circle" onclick="return false;" data-id="'+ barcode +'"><i class="fa fa-minus"></i></a>');
                                }
                            }
                            console.log(serverStoredQty);
                        }
                    }
                }
            } else {
                storedBarcodes = {};
                for (let key in serverStoredBarcodes) {
                    if (serverStoredBarcodes.hasOwnProperty(key)) {
                        storedBarcodes[key.toLowerCase()] = serverStoredBarcodes[key];
                    }
                }
                barcodeStorage.setItem(warehouseOrderId, JSON.stringify(storedBarcodes));
            }
        }

        $(document).on("click", "#check-warehouse-order-items", function(e) {
            let event = e.srcElement || e.target;
            let modal = $("#modal");
            let warehouseOrderId = $("#warehouse-order-id").val();
            let barcodeStorage = window.localStorage;
            let warehouseOrderStorage = barcodeStorage.getItem(warehouseOrderId);
            let storedBarcodes = JSON.parse(warehouseOrderStorage);
            if (!storedBarcodes) {
                alert('Greska se desila!');
            }

            let url = "/warehouse-orders/check-items/" + warehouseOrderId + "/";

            $(modal).find(".modal-title").html("<i class='fa fa-check'></i> Provera stavki");
            $(modal).find("#save-modal").attr("id", "save-warehouse-order-items");
            $(".modal-dialog", modal).css('width', '100%');
            $(".modal-body", modal).load(url + " #loaded-html", storedBarcodes, function () {
                $("body").addClass("modal-open");
                $(modal).show();
            })
        });

        $(document).on("click", "#decrease-warehouse-order-item", function (){
            let warehouseOrderId = $("#warehouse-order-id").val();
            let barcodeStorage = window.localStorage;
            let warehouseOrderStorage = barcodeStorage.getItem(warehouseOrderId);
            let storedBarcodes = JSON.parse(warehouseOrderStorage);
            let barcode = $(this).attr("data-id");
            let $1 = $("#" + barcode.toLowerCase());

            if (!storedBarcodes) {
                alert('Greska se desila!');
            }

            storedBarcodes[barcode]--;
            if (storedBarcodes[barcode] >= 0) {
                $1.find("#row-counter").empty().html(storedBarcodes[barcode]);
                barcodeStorage.setItem(warehouseOrderId, JSON.stringify(storedBarcodes));
            }
        });

        $(document).on("click", "#save-warehouse-order-items", function(e){
            e.preventDefault();

            let validation = [];
            $.each($(".feed-element"), function (index, element){
                let $textbox = $(element).find("textarea");
                if ($textbox.length > 0) {
                    if (!!$textbox.val()) {
                        $textbox.parent().removeClass("has-error");
                    } else {
                        validation.push(element);
                        $textbox.parent().addClass("has-error");
                    }
                }
            });

            if (validation.length > 0) {
                return  false;
            }

            let warehouseOrderId = $("#warehouse-order-id").val();
            let barcodeStorage = window.localStorage;
            let warehouseOrderStorage = barcodeStorage.getItem(warehouseOrderId);
            let storedBarcodes = JSON.parse(warehouseOrderStorage);
            let postData = $(".reasonNote").serializeArray();
            $.each(storedBarcodes, function(key, value) {
                postData.push({'name' : 'codes[' + key.toLowerCase() + ']', 'value' : value})
            });

            if (storedBarcodes === null) {
                alert("Ne postoje stavke za unos!");
                return false;
            }

            $.ajax({
                url: "/warehouse-orders/store-items/" + warehouseOrderId,
                type: "post",
                data: postData,
                dataType: "json",
                success: function (data) {
                    if (data.error) {
                        renderMessages(data.messages, "danger");
                    }else if (typeof data.url !== "undefined") {
                        barcodeStorage.removeItem(warehouseOrderId);
                        window.location = data.url;
                        $("#modal").hide();
                        $("body").removeClass("modal-open");
                        initTablsOnLoad();
                        initSwitch();
                    }
                }
            });
        });

    })(jQuery);
</script>