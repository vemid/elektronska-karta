<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj masinu</h3>
                            <a href="#" onclick="getCreateForm('production-machines',event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" >
                        <thead>
                        <tr>
                            <th>{{ t('Naziv masine') }}</th>
                            <th>{{ t('Skraceni naziv masine') }}</th>
                            <th>{{ t('Tip masine') }}</th>
                            <th>{{ t('Akcija') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for productionMachine in productionMachines %}
                            <tr>
                                <td>{{ productionMachine.name }}</td>
                                <td>{{ productionMachine.shortName }}</td>
                                <td>{{ productionMachine.type }}</td>
                                <td class="text-center" width="11.5%">
                                    <a href="#" onclick="getUpdateForm('production-machines', {{ productionMachine .id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getDeleteForm('production-machines', {{ productionMachine .id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>