<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>{{ t('Datum') }}</th>
                            <th>{{ t('Broj Radnika') }}</th>
                            <th>{{ t('Rezija') }}</th>
                            <th>{{ t('Ostvareno vreme') }}</th>
                            <th>{{ t('Total Ost. vreme') }}</th>
                            <th>{{ t('Vrednost') }}</th>
                            <th>{{ t('Norma') }}</th>

                        </tr>
                        </thead>
                        <tbody>
                        {% for data in datas %}
                            <tr>
                                <td>{{ data['date'] }}</td>
                                <td>{{ data['totalsDays'] }}</td>
                                <td>{{ data['totalsCuttingTime'] }}</td>
                                <td>{{ data['totalsOliwRealizedTime'] }} / {{ data['realOuterm'] }}</td>
                                <td>{{ data['totalsAllRealizedTime'] }}</td>
                                <td>{{ data['totalsMoney'] }}</td>
                                <td>{{ data['totalsOuterm'] }}</td>
                            </tr>
                        {% endfor %}
                        <tr class="bold">
                            <td class="text-center">Ukupno :</td>
                            <td>{{ totalDatas[0]['totalPerformanceDays'] }}</td>
                            <td>{{ totalDatas[0]['totalPerformanceCuttingTime']}}</td>
                            <td>{{ totalDatas[0]['totalPerformanceOliwRealizedTime'] }} / {{ totalDatas[0]['totalRealOutTerm'] }}</td>
                            <td>{{ totalDatas[0]['totalPerformanceAllRealizedTime'] }}</td>
                            <td>{{ number_format(totalDatas[0]['totalPerformanceMoney'],0) }}</td>
                            <td>{{ totalDatas[0]['totalOutTerm'] }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready( function () {
        $('#report').DataTable({
            "pageLength": 50,
            fixedHeader: true,
            "ordering" : true
        });
    })
</script>