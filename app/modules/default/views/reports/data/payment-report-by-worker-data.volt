<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th style="width: 10%">{{ t('Radnik') }}</th>
                            <th>{{ t('Datum') }}</th>
                            <th>{{ t('Radnih dana') }}</th>
                            <th>{{ t('Bolo- vanje') }}</th>
                            <th>{{ t('Odmor') }}</th>
                            <th>{{ t('Placeno') }}</th>
                            <th>{{ t('Odmor Vred') }}</th>
                            <th>{{ t('CuttingJobTime') }}</th>
                            <th style="width: 6%">{{ t('CuttingJobMoney') }}</th>
                            <th>{{ t('% Rezije') }}</th>
                            <th>{{ t('oliwRealizedTime') }}</th>
                            <th>{{ t('Planirano oper.') }}</th>
                            <th>{{ t('Ucinak norme') }}</th>
                            <th>{{ t('OliwMoney') }}</th>
                            <th>{{ t('Prekovr.') }}</th>
                            <th>{{ t('Prekovr. Vred.') }}</th>
                            <th>{{ t('Ostvareno vreme') }}</th>
                            <th>{{ t('Planirano vreme') }}</th>
                            <th style="width: 5%">{{ t('Ucinak') }}</th>
                            <th style="width: 6%">{{ t('Total Zarada') }}</th>
                            <td>{{ t('Detaljnije') }}</td>
                        </tr>
                        </thead>
                        <tbody>
                        {% for data in datas %}
                            <tr>
                                <td>{{ data['displayName'] }}</td>
                                <td>{{ data['date'] }}</td>
                                <td>{{ data['numberOfDays'] }}</td>
                                <td>{{ data['sickDays'] }}</td>
                                <td>{{ data['vacationDays'] }}</td>
                                <td>{{ data['vacationPaidDays'] }}</td>
                                <td>{{ data['vacationMoney'] }}</td>
                                <td class="bg-red">{{ data['cuttingJobTime'] }}</td>
                                <td class="bg-red">{{ number_format(data['cuttingJobMoney'],0) }} RSD</td>
                                <td class="bg-red">{{ number_format(data['cuttingPercentage'],1) }} %</td>
                                <td style="background-color:#00bcd4">{{ data['oliwRealizedTime'] }} </td>
                                <td style="background-color:#00bcd4">{{ data['oliwPlanedTime'] }} </td>
                                <td style="background-color:#00bcd4">{{ number_format(data['outTermOperation'],0) }} %</td>
                                <td style="background-color:#00bcd4">{{ number_format(data['oliwMoney'],0) }} RSD</td>
                                <td>{{ data['overtime'] }}</td>
                                <td>{% if data['overtimeMoney'] %} {{ number_format(data['overtimeMoney'],0) }} RSD {% endif %}</td>
                                <td>{{ data['totalRealizedTime'] }}</td>
                                <td>{{ data['totalPlanedTime'] }}</td>
                                <td {{ data['outerm'] > 100 ? data['outerm'] > 120 ? ' style="background: lightgreen !important;"':' style="background: lightblue !important;" ' : '' }}>
                                    {{ number_format(data['outerm'],1) }} %
                                </td>
                                <td class="bg-green">{{ number_format(data['totalDataMoney'],0) }} RSD</td>
                                <td><a href="/production-workers/list-by-worker-data?workerId={{ data['workerId'] }}&dateTo={{ paramDateTo }}&dateFrom={{ paramDateFrom }}" target="_blank" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search"></i></button></a></td>
                            </tr>
                        {% endfor %}
                        <tr class="bold">
                            <td></td>
                            <td class="text-center">Ukupno :</td>
                            <td>{{ totalDatas[0]['totalsDays'] }}</td>
                            <td>{{ totalDatas[0]['totalsSikcDays'] }}</td>
                            <td>{{ totalDatas[0]['totalsVacationDays'] }}</td>
                            <td>{{ totalDatas[0]['vacationPaidDays'] }}</td>
                            <td>{{ totalDatas[0]['totalsVacationMoney'] }}</td>
                            <td>{{ totalDatas[0]['totalsCuttingTime'] }}</td>
                            <td>{{ number_format(totalDatas[0]['totalsCuttingMoney'],0) }}</td>
                            <td>{{ totalDatas[0]['totalsCuttingPercentage'] }} %</td>
                            <td>{{ totalDatas[0]['totalsOliwRealizedTime'] }}</td>
                            <td>{{ totalDatas[0]['totalsOliwPlanedTime'] }}</td>
                            <td>{{ number_format(totalDatas[0]['totalsOutTermOperation'],0) }} %</td>
                            <td>{{ totalDatas[0]['totalsOliwMoney'] }}</td>
                            <td>{{ totalDatas[0]['totalsOvertime'] }}</td>
                            <td>{{ totalDatas[0]['totalsOvertimeMoney'] }}</td>
                            <td>{{ totalDatas[0]['totalsAllRealizedTime'] }}</td>
                            <td>{{ totalDatas[0]['totalsPlanedTime'] }}</td>
                            <td>{{ totalDatas[0]['totalsOuterm'] }}</td>
                            <td>{{ totalDatas[0]['totalsMoney'] }}</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="/reports/print-payment-report-by-worker-data/{{ paramDateFrom }}/{{ paramDateTo }}/{{ paramWorker }}" target="_blank" style="margin: 0 0 4px 0; padding: 0;">
                            <i class="fa fa-print text-info" style="font-size: 36px"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready( function () {
        $('#report').DataTable({
            "pageLength": 50,
            "fixedHeader" : true,
            "ordering" : true
        });
    })

    var table = $('#report').DataTable();

    new $.fn.dataTable.FixedHeader( table );

</script>