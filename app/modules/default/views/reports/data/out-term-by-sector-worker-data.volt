<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTable" id="report">
                        <script>
                            $(document).ready(function() {
                                $('#report').DataTable( {
                                    "order": [ 3, "desc" ]
                                } );
                            } );
                        </script>
                        <thead>
                        <tr>
                            <th>{{ t('Datum') }}</th>
                            <th>{{ t('Naziv') }}</th>
                            <th>{{ t('Datum') }}</th>
                            <th>{{ t('Ostvarena norma') }}</th>
                            <th>{{ t('Akcija') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for data in datas %}
                            <tr>
                                <td>{{ data['name'].getDisplayName() }}</td>
                                <td><a href="/operating-list-items/overview/{{ data['operation'].getOperatingListItem().getId() }}"> {{ data['operation'].getOperatingListItem().getOperatingItemListType().getDisplayName() }}</a></td>
                                <td>{{ data['operation'].getDate() }}</td>
                                <td>{{ number_format(data['outTerm'],1) }} %</td>
                                <td><a href="/production-workers/list-by-worker-data?workerId={{ data['name'].getId() }}&dateTo={{ data['operation'].getDate() }}&dateFrom={{ data['operation'].getDate() }}" target="_blank" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search"></i></button></a></td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

