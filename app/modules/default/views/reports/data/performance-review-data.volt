<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>{{ t('Datum') }}</th>
                            <th>{{ t('Broj Radnika') }}</th>
                            <th>{{ t('Bol- ovanje') }}</th>
                            <th>{{ t('Odmor') }}</th>
                            <th>{{ t('Placeno') }}</th>
                            <th>{{ t('Odmor Vred') }}</th>
                            <th>{{ t('Rezija') }}</th>
                            <th>{{ t('Vred. Rezija') }}</th>
                            <th>{{ t('% Rezije') }}</th>
                            <th>{{ t('Ostvareno vreme') }}</th>
                            <th>{{ t('Planirano oper.') }}</th>
                            <th>{{ t('Ucinak norme.') }}</th>
                            <th>{{ t('Vred. Operacije') }}</th>
                            <th>{{ t('Total Ost. vreme') }}</th>
                            <th>{{ t('Total Pla. vreme') }}</th>
                            <th>{{ t('Norma') }}</th>
                            <th>{{ t('Vrednost') }}</th>
                            <td>{{ t('Detaljnije') }}</td>
                        </tr>
                        </thead>
                        <tbody>
                        {% for data in datas %}
                            <tr>
                                <td>{{ data['date'] }}</td>
                                <td>{{ data['totalsDays'] }}</td>
                                <td>{{ data['totalsSikcDays'] }}</td>
                                <td>{{ data['totalsVacationDays'] }}</td>
                                <td>{{ data['totalsVacationPaidDays'] }}</td>
                                <td>{{ data['totalsVacationMoney'] }}</td>
                                <td class="bg-red">{{ data['totalsCuttingTime'] }}</td>
                                <td class="bg-red">{{ number_format(data['totalsCuttingMoney'],0) }}</td>
                                <td class="bg-red">{{ data['totalsCuttingPercentage'] }}</td>
                                <td style="background-color:#00bcd4">{{ data['totalsOliwRealizedTime'] }}</td>
                                <td style="background-color:#00bcd4">{{ data['totalsOliwPlanedTime'] }}</td>
                                <td style="background-color:#00bcd4">{{ data['totalsOutTermOperation'] }}</td>
                                <td style="background-color:#00bcd4">{{ number_format(data['totalsOliwMoney'],2) }}</td>
                                <td>{{ data['totalsAllRealizedTime'] }}</td>
                                <td>{{ data['totalsPlanedTime'] }}</td>
                                <td>{{ data['totalsOuterm'] }}</td>
                                <td>{{ data['totalsMoney'] }}</td>
                                <td><a href="/reports/payment-report-by-sector-data?sectorId={{ data['sectorId'] }}&dateTo={{ data['dateTo'] }}&dateFrom={{ data['dateFrom'] }}" target="_blank" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search"></i></button></a></td>
                            </tr>
                        {% endfor %}
                        <tr class="bold">
                            <td class="text-center">Ukupno :</td>
                            <td>{{ totalDatas[0]['totalPerformanceDays'] }}</td>
                            <td>{{ totalDatas[0]['totalPerformanceSickDays'] }}</td>
                            <td>{{ totalDatas[0]['totalPerformanceVacationDays'] }}</td>
                            <td>{{ totalDatas[0]['totalPerformanceVacationPaidDays'] }}</td>
                            <td>{{ totalDatas[0]['totalPerformanceVacationMoney'] }}</td>
                            <td>{{ totalDatas[0]['totalPerformanceCuttingTime']}}</td>
                            <td>{{ number_format(totalDatas[0]['totalPerformanceCuttingMoney']) }}</td>
                            <td>{{ totalDatas[0]['totalCuttingPercentage'] }}</td>
                            <td>{{ totalDatas[0]['totalPerformanceOliwRealizedTime'] }}</td>
                            <td>{{ totalDatas[0]['totalOliwPlanedTime'] }}</td>
                            <td>{{ totalDatas[0]['totalOutTermOperation'] }}</td>
                            <td>{{ number_format(totalDatas[0]['totalPerformanceTotalsOliwMoney'],2) }}</td>
                            <td>{{ totalDatas[0]['totalPerformanceAllRealizedTime'] }}</td>
                            <td>{{ totalDatas[0]['totalPlanedTime'] }}</td>
                            <td>{{ totalDatas[0]['totalOutTerm'] }}</td>
                            <td>{{ number_format(totalDatas[0]['totalPerformanceMoney'],0) }}</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="/reports/print-performance-review-data/{{ paramDateFrom }}/{{ paramDateTo }}/{{ paramSector }}" target="_blank" style="margin: 0 0 4px 0; padding: 0;">
                            <i class="fa fa-print text-info" style="font-size: 36px"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready( function () {
        $('#report').DataTable({
            "pageLength": 50,
            fixedHeader: true,
            "ordering" : true
        });
    })
</script>