<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div id="loaded-html">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="report">
                            <thead>
                            <tr>
                                <th>{{ t('Naziv') }}</th>
                                <th>{{ t('BR Oper.') }}</th>
                                <th>{{ t('Vreme po kom') }}</th>
                                <th>{{ t('Kolicina za 1h') }}</th>
                                <th>{{ t('Kolicina') }}</th>
                                <th>{{ t('Planirano Vreme') }}</th>
                                <th>{{ t('Ucinak') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for data in datas %}
                                <tr>
                                    <td>{{ data['name'] }}</td>
                                    <td>{{ data['qty']}}</td>
                                    <td>{{ data['piece_time']}}</td>
                                    <td>{{ number_format(data['pieces_hour'],1) }}</td>
                                    <td>{{ number_format(data['oliw_qty'],0) }}</td>
                                    <td>{{ number_format(data['vreme'],1) }}</td>
                                    <td>{{ number_format(data['percentage'],1) }} %</td>
                                </tr>
                            {% endfor %}
                            </tbody>
                            <tfoot>
                            <tr>
                                <th data-footer="true"></th>
                                <th data-footer="true"></th>
                                <th data-footer="true"></th>
                                <th data-footer="true"></th>
                                <th data-footer="true" data-column="4"></th>
                                <th data-footer="true" data-column="5"></th>
                                <th data-column="1"></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>