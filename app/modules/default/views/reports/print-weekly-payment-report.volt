<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css" />
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css" />
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
    <style>
        .page-break {
            page-break-after: always;
        }

        .page-breaker {
            clear: both;
            display: block;
            border: 1px solid transparent;
            page-break-before: always;
        }
    </style>
</head>
<body class="pace-done">
<div class="row">
    <div class="pull-right col-xs-4">
        <img src="<?php echo APP_PATH; ?>public/img/logo_bebakids.jpg" height="30px" alt="">
    </div>
    <div class="col-xs-8 pull-right">
        <h3 style="text-align: center" class="bold">
            Izvestaj isplate radnika
        </h3>
    </div>
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" >
                        <tr>
                            <td class="text-center">{{ t('Sektor') }}</td>
                            <td class="text-center">{{ t('Datum Od') }}</td>
                            <td class="text-center">{{ t('Datum Do') }}</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td class="text-center">{{ sectors.getName() }}</td>
                            <td class="text-center">{{ dateFrom.getShortSerbianFormat() }}</td>
                            <td class="text-center">{{ dateTo.getShortSerbianFormat() }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" >
                        <thead>
                        <tr>
                            <th>{{ t('Radnik') }}</th>
                            <th>{{ t('Radnih dana') }}</th>
                            <th>{{ t('Bol.') }}</th>
                            <th>{{ t('Odmor') }}</th>
                            <th>{{ t('Odmor Vred') }}</th>
                            <th>{{ t('CuttingJobTime') }}</th>
                            <th>{{ t('CuttingJobMoney') }}</th>
                            <th>{{ t('oliwRealizedTime') }}</th>
                            <th>{{ t('OliwMoney') }}</th>
                            <th>{{ t('Prekovr.') }}</th>
                            <th>{{ t('Prekovr. Vred.') }}</th>
                            <th style="width: 5%">{{ t('Ucinak') }}</th>
                            <th>{{ t('Ostvareno vreme') }}</th>
                            <th>{{ t('Planirano vreme') }}</th>
                            <th>{{ t('Total Zarada') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for paymentData in datas %}
                            <tr>
                                <td>{{ paymentData['displayName'] }}</td>
                                <td>{{ paymentData['numberOfDays'] }}</td>
                                <td>{{ paymentData['sickDays'] }}</td>
                                <td>{{ paymentData['vacationDays'] }}</td>
                                <td style="background: lightgreen !important;">{{ paymentData['vacationMoney'] }}</td>
                                <td>{{ paymentData['cuttingJobTime'] }}</td>
                                <td style="background: lightgreen !important;">{{ number_format(paymentData['cuttingJobMoney'],0) }} RSD</td>
                                <td>{{ paymentData['oliwRealizedTime'] }} </td>
                                <td style="background: lightgreen !important;">{{ number_format(paymentData['oliwMoney'],0) }} RSD</td>
                                <td>{{ data['overtime'] }}</td>
                                <td style="background: lightgreen !important;">{% if paymentData['overtimeMoney'] %} {{ number_format(paymentData['overtimeMoney'],0) }} RSD {% endif %}</td>
                                <td>
                                    {{ number_format(paymentData['outerm'],1) }} %
                                </td>
                                <td>{{ paymentData['totalRealizedTime'] }}</td>
                                <td>{{ paymentData['totalPlanedTime'] }}</td>
                                <td style="background: lightgreen !important;">{{ number_format(paymentData['totalDataMoney'],0) }} RSD</td>
                            </tr>
                        {% endfor %}
                        <tr class="bold">
                            <td class="text-center">Ukupno :</td>
                            <td>{{ totalDatas[0]['totalsDays'] }}</td>
                            <td>{{ totalDatas[0]['totalsSikcDays'] }}</td>
                            <td>{{ totalDatas[0]['totalsVacationDays'] }}</td>
                            <td>{{ totalDatas[0]['totalsVacationMoney'] }}</td>
                            <td>{{ totalDatas[0]['totalsCuttingTime'] }}</td>
                            <td>{{ totalDatas[0]['totalsCuttingMoney'] }}</td>
                            <td>{{ totalDatas[0]['totalsOliwRealizedTime'] }}</td>
                            <td>{{ totalDatas[0]['totalsOliwMoney'] }}</td>
                            <td>{{ totalDatas[0]['totalsOvertime'] }}</td>
                            <td>{{ totalDatas[0]['totalsOvertimeMoney'] }}</td>
                            <td>{{ totalDatas[0]['totalsOuterm'] }}</td>
                            <td>{{ totalDatas[0]['totalsAllRealizedTime'] }}</td>
                            <td>{{ totalDatas[0]['totalsPlanedTime'] }}</td>
                            <td>{{ totalDatas[0]['totalsMoney'] }}</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-break"></div>
{% for productionWorker in productionWorkers %}
    <div class="row">
        <div class="pull-right col-xs-4">
            <img src="<?php echo APP_PATH; ?>public/img/logo_bebakids.jpg" height="30px" alt="">
        </div>
        <div class="col-xs-8 pull-right">
            <h3 style="text-align: center" class="bold">
                Izvestaj isplate radnika
            </h3>
        </div>
        <div class="col-xs-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="space-30"></div>
                    <div id="loaded-html" class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" >
                            <tr>
                                <td class="text-center">{{ t('Radnik') }}</td>
                                <td class="text-center">{{ t('Datum Od') }}</td>
                                <td class="text-center">{{ t('Datum Do') }}</td>
                            </tr>
                            <tbody>
                            <tr>
                                <td class="text-center">{{ productionWorker }}</td>
                                <td class="text-center">{{ dateFrom.getShortSerbianFormat() }}</td>
                                <td class="text-center">{{ dateTo.getShortSerbianFormat() }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="space-30"></div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" >
                            <thead>
                            <tr>
                                <th>{{ t('Datum') }}</th>
                                <th>{{ t('Radnih dana') }}</th>
                                <th>{{ t('Bol.') }}</th>
                                <th>{{ t('Odmor') }}</th>
                                <th>{{ t('Odmor Vred') }}</th>
                                <th>{{ t('CuttingJobTime') }}</th>
                                <th>{{ t('CuttingJobMoney') }}</th>
                                <th>{{ t('oliwRealizedTime') }}</th>
                                <th>{{ t('OliwMoney') }}</th>
                                <th>{{ t('Prekovr.') }}</th>
                                <th>{{ t('Prekovr. Vred.') }}</th>
                                <th style="width: 5%">{{ t('Ucinak') }}</th>
                                <th>{{ t('Ostvareno vreme') }}</th>
                                <th>{{ t('Planirano vreme') }}</th>
                                <th>{{ t('Total Zarada') }}</th>
                            </tr>
                            <thead>
                            <tbody>
                            {% for datat in payment.getPaymentReportByWorkerData(dateFrom,dateTo,productionWorker) %}
                                <tr>
                                    <td>{{ datat['date'] }}</td>
                                    <td>{{ datat['numberOfDays'] }}</td>
                                    <td>{{ datat['sickDays'] }}</td>
                                    <td>{{ datat['vacationDays'] }}</td>
                                    <td style="background: lightgreen !important;">{{ datat['vacationMoney'] }}</td>
                                    <td>{{ datat['cuttingJobTime'] }}</td>
                                    <td style="background: lightgreen !important;">{{ number_format(datat['cuttingJobMoney'],0) }} RSD</td>
                                    <td>{{ datat['oliwRealizedTime'] }} </td>
                                    <td style="background: lightgreen !important;">{{ number_format(datat['oliwMoney'],0) }} RSD</td>
                                    <td>{{ datat['overtime'] }}</td>
                                    <td style="background: lightgreen !important;">{% if datat['overtimeMoney'] %} {{ number_format(datat['overtimeMoney'],0) }} RSD {% endif %}</td>
                                    <td>
                                        {{ number_format(datat['outerm'],1) }} %
                                    </td>
                                    <td>{{ datat['totalRealizedTime'] }}</td>
                                    <td>{{ datat['totalPlanedTime'] }}</td>
                                    <td style="background: lightgreen !important;">{{ number_format(datat['totalDataMoney'],0) }} RSD</td>
                                </tr>
                            {% endfor %}
                            {% for totalDatas in payment.getPaymentReportByWorkerDataTotal(dateFrom,dateTo,productionWorker) %}
                            <tr class="bold">
                            <td class="text-center">Ukupno :</td>
                            <td>{{ totalDatas['totalsDays'] }}</td>
                            <td>{{ totalDatas['totalsSikcDays'] }}</td>
                            <td>{{ totalDatas['totalsVacationDays'] }}</td>
                            <td>{{ totalDatas['totalsVacationMoney'] }}</td>
                            <td>{{ totalDatas['totalsCuttingTime'] }}</td>
                            <td>{{ totalDatas['totalsCuttingMoney'] }}</td>
                            <td>{{ totalDatas['totalsOliwRealizedTime'] }}</td>
                            <td>{{ totalDatas['totalsOliwMoney'] }}</td>
                            <td>{{ totalDatas['totalsOvertime'] }}</td>
                            <td>{{ totalDatas['totalsOvertimeMoney'] }}</td>
                            <td>{{ totalDatas['totalsOuterm'] }}</td>
                            <td>{{ totalDatas['totalsAllRealizedTime'] }}</td>
                            <td>{{ totalDatas['totalsPlanedTime'] }}</td>
                            <td>{{ totalDatas['totalsMoney'] }}</td>
                            </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div><div class="page-break"></div>
{% endfor %}
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css" />
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>