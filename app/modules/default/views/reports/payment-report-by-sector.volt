<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="space-30"></div>
                        <div class="form-horizontal">
                            <form id="load-worker" method="get" action="/reports/payment-report-by-sector-data">
                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <select data-form="true" name="sectorId" id="sectorId" class="search form-control">
                                            <option value="0">-- Izaberite --</option>
                                            {% for sector in sectors %}
                                                <option  value="{{ sector.id }}">{{ sector.name }}</option>
                                            {% endfor %}
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <input data-form="true" type="date" name="dateFrom" value="" id="dateFrom" class="datepicker"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <div class="">
                                            <input data-form="true" type="date" name="dateTo" value="" id="dateTo" class="datepicker"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <div class="form-group">
                                        <div class="">
                                            <input class="btn btn-success" type="submit" value="Pretraga" style="margin-left: 20px">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="space-20"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div id="load-into"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
