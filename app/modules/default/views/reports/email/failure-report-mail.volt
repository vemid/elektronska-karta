<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>public/assets/head.css"/>
    <style>
        table.blueTable {
            border: 1px solid #1C6EA4;
            background-color: #EEEEEE;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
        }
        table.blueTable td, table.blueTable th {
            border: 1px solid #AAAAAA;
            padding: 3px 2px;
        }
        table.blueTable tbody td {
            font-size: 13px;
        }
        table.blueTable tr:nth-child(even) {
            background: #D0E4F5;
        }
        table.blueTable thead {
            background: #1C6EA4;
            background: -moz-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
            background: -webkit-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
            background: linear-gradient(to bottom, #5592bb 0%, #327cad 66%, #1C6EA4 100%);
            border-bottom: 2px solid #444444;
        }
        table.blueTable thead th {
            font-size: 15px;
            font-weight: bold;
            color: #FFFFFF;
            border-left: 2px solid #D0E4F5;
        }
        table.blueTable thead th:first-child {
            border-left: none;
        }

        table.blueTable tfoot {
            font-size: 14px;
            font-weight: bold;
            color: #FFFFFF;
            background: #D0E4F5;
            background: -moz-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
            background: -webkit-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
            background: linear-gradient(to bottom, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);
            border-top: 2px solid #444444;
        }
        table.blueTable tfoot td {
            font-size: 14px;
        }
        table.blueTable tfoot .links {
            text-align: right;
        }
        table.blueTable tfoot .links a{
            display: inline-block;
            background: #1C6EA4;
            color: #FFFFFF;
            padding: 2px 8px;
            border-radius: 5px;
        }
    </style>
</head>

<p>Postovani,</p>
<p>U nastavku se nalazi spisak proizvoda evidentiran kao felerican proizvod
</p>


<table class="blueTable" width="300">
    <thead>
    <tr>
        <th>{{ t('Sifra') }}</th>
        <th>{{ t('Naziv') }}</th>
        <th>{{ t('Tip') }}</th>
        <th>{{ t('Kolicina') }}</th>
    </tr>
    </thead>
    <tbody>
    {% for data in datas %}
        <tr>
            <td>{{ data['productCode'] }}</td>
            <td>{{ data['productName'] }}</td>
            <td>{{ t(data['failureName']) }}</td>
            <td>{{ data['qty'] }}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>

<br><br>

{#<div class="row">#}
    {#<div class="col-xs-12">#}
        {#<div class="ibox float-e-margins">#}
            {#<div class="ibox-content">#}
                {#<div class="space-30"></div>#}
                {#<div id="loaded-html" class="table-responsive">#}
                    {#<table class="table table-striped table-bordered table-hover" >#}
                        {#<thead>#}
                        {#<tr>#}
                            {#<th>{{ t('Datum') }}</th>#}
                            {#<th>{{ t('Nalog') }}</th>#}
                            {#<th>{{ t('Tip') }}</th>#}
                            {#<th>{{ t('Iznos') }}</th>#}
                        {#</tr>#}
                        {#</thead>#}
                        {#<tbody>#}
                        {#{% for data in datas %}#}
                            {#<tr>#}
                                {#<td>{{ data['date'] }}</td>#}
                                {#<td>{{ data['warrantNumber'] }}</td>#}
                                {#<td>{{ data['type'] }}</td>#}
                                {#<td>{{ data['amount'] }}</td>#}
                            {#</tr>#}
                        {#{% endfor %}#}
                        {#</tbody>#}
                    {#</table>#}
                {#</div>#}
            {#</div>#}
        {#</div>#}
    {#</div>#}
{#</div>#}


<p style="font-family: Helvetica,Arial,sans-serif; font-size: 12px; line-height: 14px;">
    <a href="http://www.bebakids.com" class="clink sig-hide logo-container">
        <img src="http://www.bebakids.com/signature.png" alt="Kids Beba Doo" class="sig-logo" border="0">
    </a>
</p>
<p style="font-family: Helvetica,Arial,sans-serif; font-size: 12px; line-height: 14px; color: rgb(33, 33, 33);"><span style="font-weight: bold; display: inline;" class="txt signature_name-input sig-hide">Razvojni tim BEBAKIDS</span>
    <span style="display: inline;" class="title-sep sep">/</span> <span style="display: inline;" class="txt signature_jobtitle-input sig-hide">IT</span>
    <span style="display: inline;" class="email-sep break"><br></span>
    <a class="link email signature_email-input sig-hide" href="mailto:veleprodaja@bebakids.com" style="display: inline;">admin@bebakids.com</a><span style="display: inline;" class="signature_email-sep sep"> / </span><span style="display: inline;" class="txt signature_mobilephone-input sig-hide">+381648382015</span></p>
<p style="font-family: Helvetica,Arial,sans-serif; font-size: 12px; line-height: 14px;">
    <span style="font-weight: bold; display: inline;" class="txt signature_companyname-input sig-hide">Kids Beba Doo</span>
    <span style="display: inline;" class="company-sep break"><br></span>
    <span style="display: inline;" class="txt office-sep sep">Office: </span> <span style="display: inline;" class="txt signature_officephone-input sig-hide">+381 11 3972 911</span>
    <span style="display: inline;" class="txt fax-sep sep">/ Fax: </span> <span style="display: inline;" class="txt signature_fax-input sig-hide"> +381 11 3975 177</span>
    <span style="display: inline;" class="address-sep break"><br></span> <span style="display: inline;" class="txt signature_address-input sig-hide">
Ignjata Joba 37 , 11050 Beograd Srbija </span>
    <span style="display: inline;" class="website-sep break"><br></span>
    <a class="link signature_website-input sig-hide" href="http://www.bebakids.com" style="display: inline;">http://www.bebakids.com</a>
</p>
<p style="font-family: Helvetica,Arial,sans-serif; font-size: 12px; line-height: 14px;">
    <a style="display: inline;" class="social signature_facebook-input sig-hide" href="https://www.facebook.com/bebakids">
        <img data-filename="facebook.png" src="https://s3.amazonaws.com/htmlsig-assets/round/facebook.png" alt="Facebook" height="24" width="24">
    </a>
    <a style="display: inline;" class="social signature_twitter-input sig-hide" href="http://integram.com/bebakids">
        <img data-filename="instagram.png" src="http://www.bebakids.com/instagram.png" alt="instagram" height="24" width="24">
    </a>
    <a style="display: inline;" class="social signature_youtube-input sig-hide" href="http://www.bebakids.com">
        <img data-filename="bebakids.png" src="http://www.bebakids.com/bk.png" alt="BebaKids" height="24" width="24">
    </a>
</p>
<p style="font-family: Helvetica,Arial,sans-serif; font-size: 12px; line-height: 14px;" class="banner-container"></p>
<p style="font-family: Helvetica,Arial,sans-serif; font-size: 9px; line-height: 14px;" class="txt signature_disclaimer-input">Ova poruka moze da sadrzi poveriljive ili pravno privilegovane informacije koje su namenjene predvidjenom primaocu. Zabranjeno je svako neovlasceno otkrivanje, sirenje ,distribucija, kopiranje ili uzimanje bilo koje akcije u oslanjanju na informacije. Kids Beba doo nije odgovorna za bilo kakve stete nastale koriscenjem ove poste. Svako misljenje i druga izjava sadrzana u ovoj poruci i bilo koji prilog poruke su iskljucivo stavovi posaljioca i ne predstavljaju stav kompanije. Sacuvajmo prirodu! Ako nije neophodno nemojte stampati ovu poruku! </p>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>public/assets/foot.css"/>
</body>
</html>
