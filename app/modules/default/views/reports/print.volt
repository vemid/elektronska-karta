<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>public/assets/head.css"/>
    <style>
        .page-break {
            page-break-after: always;
        }

        .page-breaker {
            clear: both;
            display: block;
            border: 1px solid transparent;
            page-break-before: always;
        }
    </style>
</head>
<body class="pace-done">
<div id="wrapper" style="margin-top: -20px !important; font-size: 12px !important;">
    <div id="content" class="">
        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <tbody>
                        <tr>
                            <td>{{ t('Datum') }}</td>
                            <td>{{ t('Broj radnika') }}</td>
                            <td>{{ t('Ostvareno vreme') }}</td>
                            <td>{{ t('Vrednost') }}</td>
                            <td>{{ t('Ucinak') }}</td>
                        </tr>
                        {% set total = 0 %}
                        {% set totalWorkers = 0 %}
                        {% set totalWorkTime = 0 %}
                        {% for sectorAllOperation in sectorAllOperations %}
                            <tr>
                                <td>{{ sectorAllOperation['serbianDate'] }}</td>
                                <td>{{ sectorAllOperation['count'] }}</td>
                                <td>{{ sectorAllOperation['workTime'] }}</td>
                                <td>{{ number_format(sectorAllOperation['money'],0) }} RSD</td>
                                <td>{{ number_format((sectorAllOperation['seconds']/(sectorAllOperation['count']*27000))*100,2) }}
                                    %
                                </td>
                            </tr>
                            {% set total += sectorAllOperation['money'] %}
                            {% set totalWorker += sectorAllOperation['count'] %}
                            {% set totalWorkTime += sectorAllOperation['seconds'] %}
                        {% endfor %}
                        <tr>
                            <td class="text-center">Ukupno :</td>
                            <td>{{ totalWorker }}</td>
                            <td></td>
                            <td>{{ number_format(total,2) }} RSD</td>
                            <td>{{ number_format(totalWorkTime/(totalWorker*27000),2)*100 }} %</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="page-break"></div>
        {% for productionWorker in productionWorkers %}
            <div class="row">
                <h3 style="text-align: center" class="bold">
                    Izvestaj ucinka radnika
                </h3>
                <div class="col-xs-8">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <tbody>
                            <tr>
                                <td>Radnik : {{ productionWorker }}</td>
                                <td>Sektor : {{ productionWorker.getSectorCode().getName() }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pull-right col-xs-4">
                    <img src="<?php echo APP_PATH; ?>public/img/logo_bebakids.jpg" height="30px" alt="">
                </div>
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover"
                               style="font-size: 9px !important;">
                            <tbody>
                            <tr>
                                <th>{{ t('Radnik') }}</th>
                                <th>{{ t('Datum') }}</th>
                                <th>{{ t('Vrsta Odsustva') }}</th>
                                <th>{{ t('Operacije') }}</th>
                                <th>{{ t('Rezije') }}</th>
                                <th>{{ t('Zarada') }}</th>
                                <th>{{ t('Norma') }}</th>
                            </tr>
                            {% set totalPayment = 0 %}
                            {% set total = 0 %}
                            {% set totalCutting = 0 %}
                            {% set totalOperation = 0 %}
                            {% for dateRange in dateRanges %}
                                {#{% if dateRange.format('N') == 7 %}#}
                                {#{% continue %}#}
                                {#{% endif %}#}
                                {% if (absenceWork.getTypePerDay(dateRange,productionWorker) == null) %}
                                    {% set total = number_format((oliw.getTotalPaymentPerWorkerAndDate(dateRange,productionWorker) +
                                        cuttingJob.getTotalPaymentPerWorkerAndDateCuttingJobs(dateRange,productionWorker)),0) %}
                                {% else %}
                                    {% set total = 0 %}
                                {% endif %}
                                <tr>
                                    <td>{{ productionWorker }}</td>
                                    <td>{{ dateRange.getShortSerbianFormat() }}</td>
                                    <td>
                                        {{ t(absenceWork.getTypePerDay(dateRange,productionWorker)) }}
                                    </td>
                                    <td>
                                        {{ number_format(oliw.getTotalPaymentPerWorkerAndDate(dateRange,productionWorker),0) }}
                                        RSD
                                    </td>
                                    <td>
                                        {{ number_format(cuttingJob.getTotalPaymentPerWorkerAndDateCuttingJobs(dateRange,productionWorker),0) }}
                                        RSD
                                    </td>
                                    <td>{{ total }} RSD</td>
                                    <td>
                                        {{ number_format(itemCalculator.outTermPerWorkerAndDate(productionWorker,dateRange),2) }}
                                        %
                                    </td>
                                </tr>
                                {% set totalCutting += cuttingJob.getTotalPaymentPerWorkerAndDateCuttingJobs(dateRange,productionWorker) %}
                                {% set totalOperation += oliw.getTotalPaymentPerWorkerAndDate(dateRange,productionWorker) %}
                                {% set totalPayment += cuttingJob.getTotalPaymentPerWorkerAndDateCuttingJobs(dateRange,productionWorker) + oliw.getTotalPaymentPerWorkerAndDate(dateRange,productionWorker) %}
                            {% endfor %}
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ totalOperation }}</td>
                                <td>{{ totalCutting }}</td>
                                <td>{{ totalPayment }}</td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="page-breaker"></div>
        {% endfor %}
    </div>
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>public/assets/foot.css"/>
</body>
</html>