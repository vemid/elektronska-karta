<div id="productCalculationContainer">
    <div class="space-30"></div>
    <div class="col-xs-5">
        <div id="product-image">
            <input type="image" src="{{ similarProductCalculation.getProduct().getImagePath() }}" width="100%" style="cursor: pointer; outline: none;"/>
            <input type="file" id="reUpload" style="display: none;" />
        </div>
    </div>

    <div class="col-xs-7">
        <div class="pull-left col-xs-12">
            <table class="table">
                <tbody>
                <tr>
                    <td>Sifra proizvda :</td>
                    <td>
                        <span class="bigger-120 bold">{{ similarProductCalculation.getProduct().code }}</span>
                    </td>
                </tr>
                <tr>
                    <td>Naziv :</td>
                    <td>
                        <span class="bigger-120 bold">{{  similarProductCalculation.getProduct().name }}</span>
                    </td>
                </tr>
                <tr>
                    <td>Kolekcija :</td>
                    <td>
                        <span class="bigger-120 bold">{{  similarOldCollection }}</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-xs-12">
        <div>
            <div class="space-30">
                <h2 class="text-center">Informacije o ceni</h2>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" >
                    <thead>
                    <tr>
                        <th class="col-xs-2 text-center">{{ t('') }}</th>
                        <th class="col-xs-2 text-center">{{ t('Nabavna') }}</th>
                        <th class="col-xs-2 text-center">{{ t('Marza') }}</th>
                        <th class="col-xs-2 text-center">{{ t('VP') }}</th>
                        <th class="col-xs-2 text-center">{{ t('MP') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {% for productPrice in productPrices %}
                        <tr>
                            <td class="text-center">{{ productPrice['name'] }}</td>
                            <td class="text-center">{{ productPrice['purchasePrice'] }}</td>
                            <td class="text-center">{{ number_format((productPrice['wholesalePrice']/productPrice['purchasePrice'])-1,2)*100 }}%</td>
                            <td class="text-center">{{ productPrice['wholesalePrice'] }}</td>
                            <td class="text-center">{{ productPrice['retailPrice'] }}</td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="space-30"></div>
    <div class="table-responsive pull-left" style="width: 100%!important;">
        <table class="table table-bordered" >
            <thead>
            <tr>
                <th class="col-xs-4">{{ t('Kupon') }}</th>
                <th class="col-xs-2">{{ t('Cena') }}</th>
                <th class="col-xs-2">{{ t('Consupmtion') }}</th>
                <th class="col-xs-2">{{ t('Ukpno') }}</th>
            </tr>
            </thead>
            <tbody>
            {% for similarProductCalculationItem in similarProductCalculation.getProductCalculationItems() %}
                <tr>
                    <td>{{ similarProductCalculationItem.getMaterialFactory() ? similarProductCalculationItem.getMaterialFactory().getDisplayName() : similarProductCalculationItem.name }}</td>
                    <td>{{ number_format(similarProductCalculationItem.getCosts(),3) }}</td>
                    <td>{{ similarProductCalculationItem.consumption }}</td>
                    <td>{{ similarProductCalculationItem.total }}</td>
                </tr>
            {% endfor %}
            </tbody>
        </table>
    </div>
</div>