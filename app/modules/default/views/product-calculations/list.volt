<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="text-center">
                                            <a style="margin-right: 5px" href="#" class="btn btn-danger" onclick="getForm('/products/export-products-color-form', '/products/export-product-markup', event, '')">
                                                <i class="fas fa-file-excel"></i> {{ t('Export kalkulacije') }}
                                            </a>
                                            <a href="#" title="Dodavanje" onclick="getCreateForm('product-calculations', event)"  class="btn btn-success">
                                                <i class="fa fa-plus-square"></i>
                                                {{ t('Nova Kalkulacija') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                <div class="space-25"></div>
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="col-xs-2">{{ t('Product Name') }}</th>
                            <th class="col-xs-1">{{ t('SKU') }}</th>
                            <th class="col-xs-2">{{ t('MarkUp') }}</th>
                            <th class="col-xs-2 text-center">{{ t('Eur') }}</th>
                            <th class="col-xs-2 text-center">{{ t('Status') }}</th>
                            <th class="col-xs-2 text-center">{{ t('Akcija') }}</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        const table = $("#example").DataTable( {
            "processing": true,
            "serverSide": true,
            "pageLength": 50,
            "paging": true,
            "ajax": {
                "url": "/product-calculations/get-list-data",
            },
            "fnDrawCallback": function (oSettings) {
                $("a.product-tooltip").tooltip({
                    animated: 'fade',
                    placement: 'top',
                    html: true,
                    container: 'body'
                });
            },
            "columns": [
                null,
                { className: "text-center" },
                { className: "text-center" },
                { className: "text-center" },
                { className: "text-center" },
                { className: "text-center" },
            ],
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return '<a class="product-tooltip text-danger" data-toggle="tooltip"  title="<img src=\'' + row[5] + '\' width=\'100%\'/>">' + row[0] + '</a>';
                    },
                    "targets": 0
                },
                {
                    "render": function (data, type, row) {
                        let html = '<a href="/product-calculation-items/list/' + row[6] + '" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search"></i></button></a> ';


                        html +=  '<a href="#" onclick="getUpdateForm(\'product-calculations\', ' + row[6] + ', event)" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Izmena"><i class="fa fa-edit"></i></button></a> ' +
                            '<a href="#" onclick="getDeleteForm(\'product-calculations\', ' + row[6] + ', event)" class="bigger-140 text-danger"><button class="btn btn-danger btn-circle" type="button" title="Brisanje"><i class="fa fa-times"></i></button></a> '
                        return html;
                    },
                    "targets": 5
                },
            ],
        });

    });
</script>



{#<div class="row">#}
{#    <div class="col-xs-12">#}
{#        <div class="ibox float-e-margins">#}
{#            <div class="ibox-content">#}
{#                <div class="row">#}
{#                    <div class="col-xs-12">#}
{#                        <div class="text-center">#}
{#                            <a style="margin-right: 5px" href="#" class="btn btn-danger" onclick="getForm('/products/export-products-color-form', '/products/export-product-markup', event, '')">#}
{#                                <i class="fas fa-file-excel"></i> {{ t('Export kalkulacije') }}#}
{#                            </a>#}
{#                            <a href="#" title="Dodavanje" onclick="getCreateForm('product-calculations', event)"  class="btn btn-success">#}
{#                                <i class="fa fa-plus-square"></i>#}
{#                                {{ t('Nova Kalkulacija') }}#}
{#                            </a>#}
{#                        </div>#}
{#                    </div>#}
{#                </div>#}
{#                <div class="space-30"></div>#}
{#                <div class="table-responsive">#}
{#                    <table class="table table-striped table-bordered table-hover dataTables" >#}
{#                        <thead>#}
{#                        <tr>#}
{#                            <th>{{ t('Proizvod') }}</th>#}
{#                            <th>{{ t('Sifra') }}</th>#}
{#                            <th>{{ t('Marza') }}</th>#}
{#                            <th>{{ t('Kurs Eura') }}</th>#}
{#                            <th>{{ t('Status') }}</th>#}
{#                            <th class="text-center">{{ t('Akcije') }}</th>#}
{#                        </tr>#}
{#                        </thead>#}
{#                        <tbody>#}
{#                        {% for productCalculation in productCalculations %}#}
{#                            <tr>#}
{#                                <td>{{ productCalculation.getProduct().name}}</td>#}
{#                                <td>{{ productCalculation.getProduct().code}}</td>#}
{#                                <td>{{ productCalculation.getCalculationMapping().markUp * 100  }} %</td>#}
{#                                <td>{{ productCalculation.getCalculationMapping().eurCourse }}</td>#}
{#                                <td>{{ productCalculation.status }}</td>#}
{#                                <td class="text-center" width="11.5%">#}
{#                                    <a href="#" onclick="getUpdateForm('product-calculations', {{ productCalculation.id }}, event)" class="bigger-140 text-danger">#}
{#                                        <button class="btn btn-success btn-circle" type="button" title="Izmena">#}
{#                                            <i class="fa fa-edit"></i>#}
{#                                        </button>#}
{#                                    </a>#}
{#                                    <a href="#" onclick="getDeleteForm('product-calculations', {{ productCalculation.id }}, event)" class="bigger-140 text-danger">#}
{#                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">#}
{#                                            <i class="fa fa-times"></i>#}
{#                                        </button>#}
{#                                    </a>#}
{#                                    <a href="/product-calculation-items/list/{{ productCalculation.id }} " class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search"></i></button></a>#}
{#                                </td>#}
{#                            </tr>#}
{#                        {% endfor %}#}
{#                        </tbody>#}
{#                    </table>#}
{#                </div>#}
{#            </div>#}
{#        </div>#}
{#    </div>#}
{#</div>#}