<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="text-center">{{ t('Lista El. Karti') }}</h3>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="space-30"></div>
                        <div class="form-horizontal">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    {% if form.has('oldCollectionId') %}
                                        {% set oldCollectionId = form.get('oldCollectionId') %}
                                        {{ oldCollectionId }}
                                    {% endif %}
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        {% if form.has('genderClassificationId') %}
                                            {% set genderClassificationId = form.get('genderClassificationId') %}
                                            {{ genderClassificationId }}
                                        {% endif %}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <div class="">
                                        {% if form.has('groupModelId') %}
                                            {% set groupModelId = form.get('groupModelId') %}
                                            {{ groupModelId }}
                                        {% endif %}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="space-25"></div>
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="1%" class="text-center">{{ t('') }}</th>
                            <th style="width: 5%!important;" width="22%" class="col-xs-2 auto text-center">{{ t('Proizvod') }}</th>
                            <th style="width: 18%!important;" width="18%" class="col-xs-2 text-center">{{ t('Osnovni Materijal') }}</th>
                            <th style="width: 18%!important;" width="18%" class="col-xs-2 text-center">{{ t('Konac') }}</th>
                            <th style="width: 18%!important;" width="18%" class="text-center">{{ t('Kopčanje') }}</th>
                            <th style="width: 18%!important;" width="18%" class="text-center">{{ t('Prateći Materijal') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var cls = $("#productCodeId").val();
        var table = $("#example").DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/electron-cards/get-none-material-factory-data",
                "data":  function ( d ) {
                    return $.extend({}, d, {
                        "oldCollectionId": $('#oldCollectionId').val(),
                        "genderClassificationId" : $("#genderClassificationId").val(),
                        "groupModelId" : $("#groupModelId").val()
                    });
                }
            },
            "fnDrawCallback": function( oSettings ) {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '40%'
                });
            },
            "columns": [
                { className: "col-xs-2 text-center" },
                { className: "col-xs-2 text-center" },
                { className: "col-xs-2" },
                { className: "col-xs-2" },
                { className: "col-xs-2" },
                { className: "col-xs-2" },
            ],
            "columnDefs": [
                {
                    "targets": 0,
                    "visible": false
                },
                {
                    "render": function ( data, type, row ) {
                        return '<a class="text-info" target="_blank" href="/electron-cards/overview/'+ row[0] +'">'+ row[1] + '</a>';
                    },
                    "targets": 1
                },
            ]
        });

        $(document).on("change", "#oldCollectionId, #genderClassificationId, #groupModelId", function(){
            table.draw();
        });
    });
</script>