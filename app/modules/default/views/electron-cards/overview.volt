<div class="row">
    <div class="ibox float-e-margins">
        <div class="col-lg-12">
            <div class="ibox product-detail">
                <div class="">
                    <div class="space-15"></div>
                    {% if  electronCard.active or currentUser.isSuperAdmin() %}
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="pull-right">
                                {% if electronCard.active and electronCard.finished %}
                                    <a href="/electron-cards/print/{{ electronCard.id }}" target="_blank" class="btn btn-success">
                                        <i class="fa fa-print"></i>{{ t(' Stampa tehnicke skice') }}
                                    </a>
                                {% endif %}
                                {% if not electronCard.finished and electronCard.active %}
                                    <a href="#" class="btn btn-success" onclick="sendPostAjaxCall('/electron-cards/finish/{{ electronCard.id }}', {})">
                                        {{ 'Završi' }} <i class="fa fa-save"></i>
                                    </a>
                                {% endif %}
                                {% if electronCard.finished %}
                                    <span class="col-xs-1"></span>
                                    <a href="#" class="btn btn-warning" onclick="sendPostAjaxCall('/electron-cards/cancel/{{ electronCard.id }}', {})">
                                        {{ 'Storniraj' }} <i class="fa fa-backward"></i>
                                    </a>
                                {% endif %}
                                {% if currentUser.isSuperAdmin() and not electronCard.active %}
                                    <a href="#" class="btn btn-info" onclick="sendPostAjaxCall('/electron-cards/activate/{{ electronCard.id }}', {})">
                                        {{ 'Aktiviraj ponovo' }} <i class="fa fa-adjust"></i>
                                    </a>
                                {% endif %}
                                {#currentUser.isDesigner()#}
                                {% if electronCard.active and electronCard.finished and electronCard.product.getQuestionnaires()|length == 0 %}
                                    <a href="#" class="btn btn-info" onclick="getCustomCreateForm('questionnaires', 'electron-card-id', {{ electronCard.id }}, event)">
                                        {{ 'Kreiraj Upitnik' }} <i class="fa fa-question-circle"></i>
                                    </a>
                                {% endif %}
                            </div>
                        </div>
                    </div>
                    <div class="space-15"></div>
                    {% endif %}
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tabElectronCard">{{ t('Elektronska Karta') }}</a></li>
                            <li class=""><a data-toggle="tab" href="#tabPrePress">{{ t('Priprema za štampu') }}</a></li>
                            <li class=""><a data-toggle="tab" href="#tabAdditionalSketch">{{ t('Dodatna skica') }}</a></li>
                            <li class=""><a data-toggle="tab" href="#tabAdditionalSecondSketch">{{ t('Dodatna skica 2') }}</a></li>
                            {% if electronCard.finished and electronCard.product.getQuestionnaires(['codeId = :codeId:', 'bind' : ['codeId' : molder.id]])|length > 0 %}
                                <li class=""><a data-toggle="tab" href="#tabQuestionnaire">{{ t('Upitnik') }}</a></li>
                            {% endif %}
                        </ul>
                        <div class="tab-content">
                            <div id="tabElectronCard" class="tab-pane active">
                                {{ partial('electron-cards/partials/electron-card') }}
                            </div>
                            <div id="tabPrePress" class="tab-pane">
                                {{ partial('electron-cards/partials/pre-press') }}
                            </div>
                            <div id="tabAdditionalSketch" class="tab-pane">
                                {{ partial('electron-cards/partials/additional-sketch') }}
                            </div>
                            <div id="tabAdditionalSecondSketch" class="tab-pane">
                                {{ partial('electron-cards/partials/additional-second-sketch') }}
                            </div>
                            {% if electronCard.finished and electronCard.product.getQuestionnaires()|length > 0%}
                            <div id="tabQuestionnaire" class="tab-pane">
                                {{ partial('electron-cards/partials/questionnaire') }}
                            </div>
                            {% endif %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>