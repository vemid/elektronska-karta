<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css" />
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css" />
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
</head>
<body class="pace-done">
<div class="row">
    <div class="col-xs-12">
        {#<img src="{{ siteUrl }}/img/logo_bebakids.jpg" width="300px" alt="">#}
        <h1 style="text-align: center" class="bold">
            Priprema za stampu
        </h1>
        <div class="ibox float-e-margins">
            <div style="max-height: 2016px!important; display: inline-block">
                <table valign="middle" width="100%" cellpadding="0" cellspacing="0">
                    <tbody style="max-height: 2016px!important;">
                    <tr>
                        <td style=" height: auto;width: 100%;  display: block;">
                            <img src="{{ electronCard.getPrePressImagePath() }}" alt="">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css" />
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>