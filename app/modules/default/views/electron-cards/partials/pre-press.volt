<div class="panel-body">
    {% if not electronCard.prePressImage and electronCard.active %}
        <form action="/electron-cards/upload-electron-card-images-and-description" method="post" enctype="multipart/form-data">
            <div class="dropzone dropZoneSkull">
                <input type="hidden" id="electronCardId" name="electronCardId" value="{{ electronCard.id }}"/>
                <div class="fallback">
                    <input name="prePressImage" type="file" />
                </div>
            </div>
            <div class="space-20"></div>

        </form>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4 text-center">
                <a href="#" id="start" class="btn btn-primary btn-rounded btn-block">
                    {{ t('Snimi') }} <i class="fa fa-image"></i>
                </a>
            </div>
            <div class="col-xs-4"></div>
        </div>
    {% else %}
        <form action="/electron-cards/upload-electron-card-images-and-description">
            <input type="hidden" id="electronCardId" name="electronCardId" value="{{ electronCard.id }}"/>
            <input type="image" class="initReUpload" src="{{ electronCard.getPrePressImagePath() }}" width="100%" style="cursor: pointer; outline: none;"/>
            <input type="file" name="prePressImage" class="reUploadImage" style="display: none;" accept="image/x-png,image/gif,image/jpeg" />
        </form>
    {% endif %}
</div>