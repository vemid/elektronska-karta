<div class="panel-body">
    <table valign="middle" width="100%">
        <tbody>
        <tr>
            <td colspan="2">
                <div class="pull-left" style="font-size: 24px">
                    <span>{{ t('Šifra') }}: </span>
                    <span class="bold">{{ electronCard.product.code }}
                        - {{ electronCard.product.name }} </span>
                    <span> || {{ t('Verzija') }}:</span>
                    <span class="bold">{{ electronCard.version }}</span>
                </div>
            </td>
            <td colspan="2">
                <div class="pull-right" style="font-size: 24px">
                    <span>Sezona : </span>
                    <span class="bold">{{ hydrator.getSeason() ? hydrator.getSeason().getName() : '' }}</span>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding: 10px 0; font-size: 18px">
                {{ t('Osnova') }}: {{ electronCard.baseName }}
                {% if electronCard.active and not electronCard.finished %}
                    <a href="#" class="text-success" onclick="getForm('/electron-cards/edit-base-name-form/{{ electronCard.id }}', '/electron-cards/edit-base-name/{{ electronCard.id }}', event)">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                {% endif %}
            </td>
            <td colspan="2">
                <div class="pull-right" style="font-size: 24px">
                    <span>Kolekcija : </span>
                    <span class="bold">{{ hydrator.getCollection() ? hydrator.getCollection().getName() : '' }}</span>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size: 24px; color:red">
                {{ t('Designer') }}: {{ display }}
            </td>
            <td colspan="2">
                <div class="pull-right" style="font-size: 26px">
                    <span>Velicine : </span>
                    <span class="bold">{{ hydrator.getAllProductSizes() }}</span>
                </div>
            </td>
        </tr>
        <tr valign="middle">
            <td colspan="4">
                <div class="space-30"></div>
                <form action="/electron-cards/upload-electron-card-images-and-description">
                    <input type="hidden" id="electronCardId" name="electronCardId" value="{{ electronCard.id }}"/>
                    <input type="image" class="initReUpload" src="{{ electronCard.getImagePath() }}" width="100%" style="cursor: pointer; outline: none;"/>
                    <input type="file" name="image" class="reUploadImage" style="display: none;" accept="image/x-png,image/gif,image/jpeg" />
                </form>
            </td>
        </tr>
        <tr valign="middle">
            <td colspan="4" valign="middle" style="border-top: 1px solid #333;border-left: 1px solid #333;border-right: 1px solid #333; padding: 10px 10px">
                <span style="display: block">
                    {{ electronCard.note }}
                    {% if electronCard.active and not electronCard.finished %}
                        <a href="#" class="bigger-140" onclick="getForm('/electron-cards/edit-note-form/{{ electronCard.id }}', '/electron-cards/edit-note/{{ electronCard.id }}', event)">
                            <i class="fas fa-pencil-alt text-success"></i>
                        </a>
                    {% endif %}
                </span>
            </td>
        </tr>
        <tr valign="middle">
            <td valign="middle" align="center" style="border: 1px solid #333; padding: 10px;">
                <h3 style="margin: 0 10px 0 0;">
                    <b>{{ t('Osnovni Materijal') }}</b>
                    {% if electronCard.active and not electronCard.finished %}

                        <a title="Dodaj Osnovni Materijal" class="pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                            <i class="fa fa-plus"></i>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li>
                                <a onclick="getForm('/electron-cards/add-factory-form/{{ electronCard.id }}/{{ constant("ElectronCardFactory::BASIC") }}/factory', '/electron-cards/add-factory/{{ electronCard.id }}/{{ constant("ElectronCardFactory::BASIC") }}/factory', event)" href="#">{{ t('Kartica Materijala') }}</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a onclick="getForm('/electron-cards/add-factory-form/{{ electronCard.id }}/{{ constant("ElectronCardFactory::BASIC") }}/sketch', '/electron-cards/add-factory/{{ electronCard.id }}/{{ constant("ElectronCardFactory::BASIC") }}/sketch', event)" href="#">{{ t('Pred-Kupon') }}</a>
                            </li>
                        </ul>
                    {% endif %}
                </h3>
            </td>
            <td valign="middle" align="center" style="border: 1px solid #333; padding: 10px;">
                <h3 style="margin: 0 10px 0 0;">
                    <b>{{ t('Konac') }}</b>
                    {% if electronCard.active and not electronCard.finished %}
                        <a title="Dodaj Osnovni Materijal" class="pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                            <i class="fa fa-plus"></i>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li>
                                <a onclick="getForm('/electron-cards/add-factory-form/{{ electronCard.id }}/{{ constant("ElectronCardFactory::STRING") }}/factory', '/electron-cards/add-factory/{{ electronCard.id }}/{{ constant("ElectronCardFactory::STRING") }}/factory', event)" href="#">{{ t('Kartica Materijala') }}</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a onclick="getForm('/electron-cards/add-factory-form/{{ electronCard.id }}/{{ constant("ElectronCardFactory::STRING") }}/sketch', '/electron-cards/add-factory/{{ electronCard.id }}/{{ constant("ElectronCardFactory::STRING") }}/sketch', event)" href="#">{{ t('Pred-Kupon') }}</a>
                            </li>
                        </ul>
                    {% endif %}
                </h3>
            </td>
            <td valign="middle" align="center" style="border: 1px solid #333; padding: 10px;">
                <h3 style="margin: 0 10px 0 0;">
                    <b>{{ t('Kopčanje') }}</b>
                    {% if electronCard.active and not electronCard.finished %}
                        <a title="Dodaj Osnovni Materijal" class="pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                            <i class="fa fa-plus"></i>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li>
                                <a onclick="getForm('/electron-cards/add-factory-form/{{ electronCard.id }}/{{ constant("ElectronCardFactory::STITCH") }}/factory', '/electron-cards/add-factory/{{ electronCard.id }}/{{ constant("ElectronCardFactory::STITCH") }}/factory', event)" href="#">{{ t('Kartica Materijala') }}</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a onclick="getForm('/electron-cards/add-factory-form/{{ electronCard.id }}/{{ constant("ElectronCardFactory::STITCH") }}/sketch', '/electron-cards/add-factory/{{ electronCard.id }}/{{ constant("ElectronCardFactory::STITCH") }}/sketch', event)" href="#">{{ t('Pred-Kupon') }}</a>
                            </li>
                        </ul>
                    {% endif %}
                </h3>
            </td>
            <td valign="middle" align="center" style="border: 1px solid #333; padding: 10px;">
                <h3 style="margin: 0 10px 0 0;">
                    <b>{{ t('Prateći Materijal') }}</b>
                    {% if electronCard.active and not electronCard.finished %}
                        <a title="Dodaj Osnovni Materijal" class="pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                            <i class="fa fa-plus"></i>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li>
                                <a onclick="getForm('/electron-cards/add-factory-form/{{ electronCard.id }}/{{ constant("ElectronCardFactory::ATTENDANT") }}/factory', '/electron-cards/add-factory/{{ electronCard.id }}/{{ constant("ElectronCardFactory::ATTENDANT") }}/factory', event)" href="#">{{ t('Kartica Materijala') }}</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a onclick="getForm('/electron-cards/add-factory-form/{{ electronCard.id }}/{{ constant("ElectronCardFactory::ATTENDANT") }}/sketch', '/electron-cards/add-factory/{{ electronCard.id }}/{{ constant("ElectronCardFactory::ATTENDANT") }}/sketch', event)" href="#">{{ t('Pred-Kupon') }}</a>
                            </li>
                        </ul>
                    {% endif %}
                </h3>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top"
                style="border-bottom: 1px solid #333; border-left: 1px solid #333; border-right: 1px solid #333; padding-top: 10px; padding-bottom: 5px">
                {% for electronCardFactory in electronCard.getElectronCardFactoriesWithType(constant("ElectronCardFactory::BASIC")) %}
                    <span class="text-left block" style="padding: 0px 10px 5px 10px">
                                                            - {{ electronCardFactory }}
                        {% if electronCard.active and not electronCard.finished %}
                            {% set relationType = electronCardFactory.getMaterialSketch() ? 'sketch' : 'factory' %}
                            &nbsp;
                            <a href="#" class="text-success" onclick="getForm('/electron-cards/add-factory-form/{{ electronCard.id }}/BASIC/{{ relationType }}/{{ electronCardFactory.id }}', '/electron-cards/add-factory/{{ electronCard.id }}/BASIC/{{ relationType }}/{{ electronCardFactory.id }}', event)">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            <a href="#" class="text-danger" onclick="getDeleteForm('electron-card-factories', '{{ electronCardFactory.id }}', event)">
                                <i class="fas fa-minus-circle"></i>
                            </a>
                        {% endif %}
                    </span>
                {% endfor %}
            </td>
            <td align="center" valign="top" style="border-bottom: 1px solid #333; border-left: 1px solid #333; border-right: 1px solid #333; padding-top: 10px; padding-bottom: 5px">
                {% for electronCardFactory in electronCard.getElectronCardFactoriesWithType(constant("ElectronCardFactory::STRING")) %}
                    <span class="text-left block" style="padding: 0px 10px 5px 10px"> - {{ electronCardFactory.name }}
                        {% if electronCard.active and not electronCard.finished %}
                            {% set relationType = electronCardFactory.getMaterialSketch() ? 'sketch' : 'factory' %}

                            <a href="#" class="text-success" onclick="getForm('/electron-cards/add-factory-form/{{ electronCard.id }}/STRING/{{ relationType }}/{{ electronCardFactory.id }}', '/electron-cards/add-factory/{{ electronCard.id }}/STRING/{{ relationType }}/{{ electronCardFactory.id }}', event)">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            <a href="#" class="text-danger" onclick="getDeleteForm('electron-card-factories', '{{ electronCardFactory.id }}', event)">
                                <i class="fas fa-minus-circle"></i>
                            </a>
                        {% endif %}
                    </span>
                {% endfor %}
            </td>
            <td align="center" valign="top" style="border-bottom: 1px solid #333; border-left: 1px solid #333; border-right: 1px solid #333; padding-top: 10px; padding-bottom: 5px">
                {% for electronCardFactory in electronCard.getElectronCardFactoriesWithType(constant("ElectronCardFactory::STITCH")) %}
                    <span class="text-left block" style="padding: 0px 10px 5px 10px">- {{ electronCardFactory.name }}
                        {% if electronCard.active and not electronCard.finished %}
                            {% set relationType = electronCardFactory.getMaterialSketch() ? 'sketch' : 'factory' %}

                            <a href="#" class="text-success" onclick="getForm('/electron-cards/add-factory-form/{{ electronCard.id }}/STITCH/{{ relationType }}/{{ electronCardFactory.id }}', '/electron-cards/add-factory/{{ electronCard.id }}/STITCH/{{ relationType }}/{{ electronCardFactory.id }}', event)">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            <a href="#" class="text-danger" onclick="getDeleteForm('electron-card-factories', '{{ electronCardFactory.id }}', event)">
                                <i class="fas fa-minus-circle"></i>
                            </a>
                        {% endif %}
                    </span>
                {% endfor %}
            </td>
            <td align="center" valign="top" style="border-bottom: 1px solid #333; border-left: 1px solid #333; border-right: 1px solid #333; padding-top: 10px; padding-bottom: 5px">
                {% for electronCardFactory in electronCard.getElectronCardFactoriesWithType(constant("ElectronCardFactory::ATTENDANT")) %}
                    <span class="text-left block" style="padding: 0px 10px 5px 10px">- {{ electronCardFactory.name }}
                        {% if electronCard.active and not electronCard.finished %}
                            {% set relationType = electronCardFactory.getMaterialSketch() ? 'sketch' : 'factory' %}

                            <a href="#" class="text-success" onclick="getForm('/electron-cards/add-factory-form/{{ electronCard.id }}/ATTENDANT/{{ relationType }}/{{ electronCardFactory.id }}', '/electron-cards/add-factory/{{ electronCard.id }}/ATTENDANT/{{ relationType }}/{{ electronCardFactory.id }}', event)">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            <a href="#" class="text-danger" onclick="getDeleteForm('electron-card-factories', '{{ electronCardFactory.id }}', event)">
                                <i class="fas fa-minus-circle"></i>
                            </a>
                        {% endif %}
                        </span>
                {% endfor %}
            </td>
        </tr>
        </tbody>
    </table>
</div>