<div class="alert alert-warning text-center">
    <h2>Upitnik za sektor - {{ questionnaire.code.name }}</h2>
</div>
<div class="row">
    <div class="col-xs-12">
        <h2 class="text-center"></h2>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        {% if currentUser.isMolder() and not currentUser.isSuperAdmin() %}
        <form class="form-horizontal" method="post" action="/questions/answer">
            {% else %}
            <div class="form-horizontal">
                {% endif %}
                {% for question in questionnaire.questions %}
                    <input type="hidden" name="questionId[{{ question.Id }}]" id="questionId" value="{{ question.Id }}"/>
                    <div class="form-group">
                        <label for="" class="col-xs-3 control-label">{{ question.question }}: </label>
                        <div class="col-xs-7">
                            {% if question.field == 'INPUT' %}
                                <input type="text" id="answer" name="answer[{{ question.Id }}]" value="{{ question.answers }}" class="form-control"/>
                            {% elseif question.field == 'TEXT_AREA' %}
                                <textarea id="answer" rows="7" name="answer[{{ question.Id }}]" class="form-control">{{ question.answers }}</textarea>
                            {% elseif question.field == 'SELECT' %}
                                {% set options = json_decode(question.possibleAnswers, true) %}
                                <select name="answer[{{ question.Id }}]" id="answer" class="form-control search">
                                    <option value="">-- Izaberite --</option>
                                    {% for option in options %}
                                        <option {{ question.answers == option ? 'selected' : '' }} value="{{ option }}">{{ option }}</option>
                                    {% endfor %}
                                </select>
                            {% elseif question.field == 'MULTIPLE_SELECT' %}
                                {% set options = json_decode(question.possibleAnswers, true) %}
                                {% set Ans = json_decode(question.answers, true) %}
                                <select multiple name="answer[{{ question.Id }}][]" id="answer" class="form-control search">
                                    <option value="">-- Izaberite --</option>
                                    {% for option in options %}
                                        <option value="{{ option }}" {{ Ans|length and in_array(option, Ans) ? 'selected' : '' }}>{{ option }}</option>
                                    {% endfor %}
                                </select>
                            {% elseif question.field == 'RADIO' %}
                                {% set options = json_decode(question.possibleAnswers, true) %}
                                {% for option in options %}
                                    <input type="radio" name="answer[{{ question.Id }}]" value="{{ option }}" {{ option == question.answers ? 'checked' : '' }}/> {{ option }} &nbsp;
                                {% endfor %}
                            {% elseif question.field == 'CHECK' %}
                                {% set options = json_decode(question.possibleAnswers, true) %}
                                {% set Ans = json_decode(question.answers, true) %}
                                {% for option in options %}
                                    <div class="checkbox-inline i-checks">
                                        <label>
                                            <input type="checkbox" value="{{ option }}" {{ Ans|length and in_array(option, Ans) ? 'checked' : '' }} name="answer[{{ question.Id }}][]"/>
                                            <i></i> {{ option }}
                                        </label>
                                    </div>
                                {% endfor %}
                            {% endif %}
                            <div class="help-block m-b-none"></div>
                        </div>
                        {% if currentUser.isDesigner() or currentUser.isSuperAdmin() %}
                            <div class="col-xs-2">
                                <a href="#" onclick="getDeleteForm('questions', '{{ question.id }}', event)" class="pull-right btn btn-danger btn-circle">
                                    <i class="fa fa-remove"></i>
                                </a>
                            </div>
                        {% endif %}
                    </div>
                    {% if question.additionalNote %}
                        <div class="form-group">
                            <label for="" class="col-xs-3 control-label">Dodatno: </label>
                            <div class="col-xs-7">
                                <textarea id="answer" rows="7" name="additionalNote[{{ question.Id }}]" class="form-control">{{ question.answeredNote }}</textarea>
                            </div>
                        </div>
                    {% endif %}
                    {% if not currentUser.isMolder() %}
                        <div class="hr-line-dashed"></div>
                    {% endif %}
                {% endfor %}
                {% if currentUser.isMolder() and not currentUser.isSuperAdmin() %}
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-xs-4 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary" href="#" id="save-modal">Save changes</button>
                    </div>
                </div>
        </form>
        {% else %}
    </div>
    {% endif %}
</div>
{% if currentUser.isDesigner() or currentUser.isSuperAdmin() %}
    <div class="col-xs-12 text-center">
        <div class="space-20"></div>
        <h2>Dodaj pitanje
            <a style="margin-top: -10px" href="#"
               onclick="getCustomCreateForm('questions', 'questionnaireId', '{{ questionnaire.id }}', event)"
               class="bigger-200 btn btn-circle btn-lg text-success">
                <i class="fa fa-plus"></i>
            </a>
        </h2>
    </div>
{% endif %}
</div>