<div class="panel-body">
    {% if not electronCard.additionalSecondSketchImage and electronCard.active %}
        <form action="/electron-cards/upload-electron-card-images-and-description" method="post" enctype="multipart/form-data">
            <div class="dropzone dropZoneSkull">
                <input type="hidden" id="electronCardId" name="electronCardId" value="{{ electronCard.id }}"/>
                <div class="fallback">
                    <input name="additionalSecondSketchImage" type="file" />
                </div>
            </div>
            <div class="space-20"></div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-xs-12">
                        <textarea name="additionalSecondSketchDescription" placeholder="Opis..." class="form-control" id="additionalSecondSketchDescription" cols="30" rows="6"></textarea>
                    </div>
                </div>
            </div>
            <div class="space-20"></div>
        </form>
        <div class="col-xs-12">
            <div class="col-xs-4"></div>
            <div class="col-xs-4 text-center">
                <a href="#" id="start" class="btn btn-primary btn-rounded btn-block">
                    {{ t('Snimi') }} <i class="fa fa-image"></i>
                </a>
            </div>
            <div class="col-xs-4"></div>
        </div>
    {% else %}
        <div class="col-xs-12">
            <form action="/electron-cards/upload-electron-card-images-and-description">
                <input type="hidden" id="electronCardId" name="electronCardId" value="{{ electronCard.id }}"/>
                <input type="image" class="initReUpload" src="{{ electronCard.getAdditionalSecondSketchImagePath() }}" width="100%" style="cursor: pointer; outline: none;"/>
                <input type="file" name="additionalSecondSketchImage" class="reUploadImage" style="display: none;" accept="image/x-png,image/gif,image/jpeg" />
            </form>
        </div>
        <div class="col-xs-12">
            <p>{{ electronCard.additionalSecondSketchDescription }}</p>
        </div>
    {% endif %}
</div>