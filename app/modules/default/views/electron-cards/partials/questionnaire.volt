<div class="panel-body">
    {% if currentUser.isDesigner() or currentUser.isSuperAdmin() %}
        <div class="row">
            <div class="col-xs-12">
                <a href="#" class="btn btn-info pull-right" onclick="getCustomCreateForm('questionnaires', 'electron-card-id', {{ electronCard.id }}, event)">
                    {{ 'Kreiraj novi Upitnik' }} <i class="fa fa-question-circle"></i>
                </a>
            </div>
        </div>
        <div class="space-20"></div>
        {#<div class="alert alert-info">#}
            {#Na ovoj stranici možete kreirati uptnik sa pitanjuima koe želite postaviti modelaru. Uputsvo kako mozete#}
            {#uspešno kreirati upitnik:#}
            {#asdasd asdkal;sdk asldkasdkas alkdas;das alsdklakdla ajdklsda jasdlkasd#}
        {#</div>#}
    {% endif %}
    {% for questionnaire in electronCard.product.getQuestionnaires() %}
        {% if currentUser.isSuperAdmin() or currentUser.isDesigner() or in_array(questionnaire.codeId, userSectors) %}
            {{ partial('electron-cards/partials/questionnaire-single') }}
            <div class="space-20"></div>
        {% endif %}
    {% endfor %}
</div>
