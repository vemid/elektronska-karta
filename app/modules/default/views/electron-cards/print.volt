<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css" />
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css" />
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
</head>
<body class="pace-done">
<div id="wrapper">
    <div id="content" class="">
        <div class="row">
            <div class="col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="col-lg-12">
                        <table valign="middle" width="100%" cellpadding="0" cellspacing="0">
                            <tbody>
                            {#<tr>#}
                                {#<img src="{{ siteUrl }}/img/logo_bebakids.jpg" width="300px" alt="">#}
                            {#</tr>#}
                            {#<tr>#}
                                {#<h1 style="text-align: center" class="bold">#}
                                    {#Tehnicka Skica#}
                                {#</h1>#}
                            {#</tr>#}
                            <tr>
                                <td colspan="2" style=" font-size: 15px">
                                    <span class="bold">{{ electronCard.product.name }} - {{  electronCard.product.code }}</span>
                                    <span class="bold"> || Verzija: {{ electronCard.version }}</span>
                                </td>
                                <td colspan="2" style="text-align: right">
                                    <div style="font-size: 15px; padding-left: 100px">
                                        <span> Sezona : </span>
                                        <span class="bold">{{ hydrator.getSeason() ? hydrator.getSeason().getName() : '' }}</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="font-size: 15px">
                                    {{ t('Osnova') }}: {{ electronCard.baseName }}&nbsp;
                                </td>
                                <td colspan="2" style="text-align: right; font-size: 15px">
                                    <span>{{ t('Kolekcija') }} : </span>
                                    <span class="bold">{{ hydrator.getCollection() ? hydrator.getCollection().getName() : '' }}&nbsp;</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="font-size: 15px; color: red">
                                    {% if electronCard.getEntityLogOnCreate() %}
                                    <span>Designer : {{ electronCard.getEntityLogOnCreate().getUser() }} - {{ electronCard.getEntityLogOnCreate().getTimestamp().getShortSerbianFormat() }}</span>
                                    {% endif %}
                                </td>
                                <td colspan="2" style="text-align: right">
                                    <div style="font-size: 15px; padding-left: 100px">
                                        <span>Velicine : </span>
                                        <span class="bold">{{ hydrator.getAllProductSizes() }}</span>
                                    </div>
                                </td>
                            </tr>
                            <tr valign="middle" style="padding-top: 10px">
                                <td colspan="4" style="max-height: 3660px;">
                                    <img src="{{ electronCard.getImagePrintPath() }}" style="max-height: 3160px" alt="">
                                </td>
                            </tr>
                            {#<tr valign="middle">#}
                                {#<td colspan="4" valign="middle"#}
                                    {#style="border-top: 1px solid #333;border-left: 1px solid #333;border-right: 1px solid #333; padding: 10px 10px">#}
                            {#<span style="display: block">#}
                                {#{{ electronCard.note }}#}
                            {#</span>#}
                                {#</td>#}
                            {#</tr>#}
                            <tr valign="middle">
                                <td valign="middle" align="center" style="border: 1px solid #333;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>{{ t('Osnovni Materijal') }}</b>
                                    </h7>
                                </td>
                                <td valign="middle" align="center" style="border-right: 1px solid #333; border-top: 1px solid #333; border-bottom: 1px solid #333;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>{{ t('Konac') }}</b>
                                    </h7>
                                </td>
                                <td valign="middle" align="center" style="border-top: 1px solid #333;border-bottom: 1px solid #333;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>{{ t('Kopčanje') }}</b>
                                    </h7>
                                </td>
                                <td valign="middle" align="center" style="border: 1px solid #333;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>{{ t('Prateći Materijal') }}</b>
                                    </h7>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top"
                                    style="border-bottom: 1px solid #333; border-left: 1px solid #333; border-right: 1px solid #333; padding-top: 2px; padding-bottom: 2px; text-align: left;">
                                    {% for electronCardFactory in electronCard.getElectronCardFactoriesWithType(constant("ElectronCardFactory::BASIC")) %}
                                        <span class="text-left block"
                                              style="font-size: 11px !important; padding: -8px !important;"> - {{ electronCardFactory }}</span>
                                    {% endfor %}
                                </td>
                                <td align="center" valign="top"
                                    style="border-bottom: 1px solid #333; border-right: 1px solid #333; padding-top: 2px; padding-bottom: 2px; text-align: left;">
                                    {% for electronCardFactory in electronCard.getElectronCardFactoriesWithType(constant("ElectronCardFactory::STRING")) %}
                                        <span class="text-left block"
                                              style="padding: 0px 5px 0px 5px; font-size: 8px !important;"> - {{ electronCardFactory.name }}</span>
                                    {% endfor %}
                                </td>
                                <td align="center" valign="top"
                                    style="border-bottom: 1px solid #333; padding-top: 2px; padding-bottom: 2px; text-align: left;">
                                    {% for electronCardFactory in electronCard.getElectronCardFactoriesWithType(constant("ElectronCardFactory::STITCH")) %}
                                        <span class="text-left block"
                                              style="padding: 0px 5px 0px 5px; font-size: 8px !important;"> - {{ electronCardFactory.name }}</span>
                                    {% endfor %}
                                </td>
                                <td align="center" valign="top"
                                    style="border-bottom: 1px solid #333; border-left: 1px solid #333; border-right: 1px solid #333; padding-top: 2px; padding-bottom: 2px; text-align: left;">
                                    {% for electronCardFactory in electronCard.getElectronCardFactoriesWithType(constant("ElectronCardFactory::ATTENDANT")) %}
                                        <span class="text-left block"
                                              style="padding: 0px 5px 0px 5px; font-size: 8px !important;"> - {{ electronCardFactory.name }}</span>
                                    {% endfor %}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css" />
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>