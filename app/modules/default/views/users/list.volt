<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Novi korisnik</h3>
                            <a href="#" onclick="getCreateForm('users', event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" >
                        <thead>
                        <tr>
                            <th>{{ t('Ime i Prezime') }}</th>
                            <th>{{ t('Username') }}</th>
                            <th>{{ t('Rola') }}</th>
                            <th>{{ t('Pregled') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for user in users %}
                            {% if user.isSuperAdmin() and not currentUser.isSuperAdmin() %}
                                {% continue %}
                            {% endif %}
                            <tr>
                                <td>{{ user }}</td>
                                <td>{{ user.username }}</td>
                                <td>{{ user.getRolesName() }}</td>
                                <td class="text-center" width="15%">
                                    {% if currentUser.isSuperAdmin() %}
                                        <a href="#" class="btn btn-primary white btn-circle" title="{{ t('Dodavanje rola/sektora') }}" onclick="getCustomCreateForm('user-role-assignments', 'user-id', {{ user.id }}, event)">
                                        <i class="fa fa-users"></i>
                                    </a>
                                    <a class="btn btn-success btn-sm" href="/users/overview/{{ user.id }}">
                                        <i class="fa fa-search"></i>
                                    </a>
                                    <a href="#" onclick="getCustomCreateForm('user-notifications', 'user-id', {{ user.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-info btn-circle" type="button" title="Notifikacije">
                                            <i class="fa fa-envelope"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getDeleteForm('users', {{ user.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                    {% endif %}
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>