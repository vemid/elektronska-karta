<div class="row">
    {% for name, notificationTypeMessages in notificationTypes %}

        {% set notification = notificationTypes[name]['notification'] %}
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{ notification }}</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        {% if notification.allowedToUnsubscribe %}
                        <a class="" href="#" onclick="unsubscribeNotification({{ notification.id }}, event)">
                            <i class="fa fa-times"></i>
                        </a>
                        {% endif %}
                    </div>
                </div>
                <div class="ibox-content">
                    {% for status, userNotificationMessages in notificationTypeMessages %}
                        {% if status != 'notification' %}
                            <p>{{ status }}</p>
                            <div class="hr-line-solid"></div>
                        {% endif %}

                        {% for key, userNotificationMessage in userNotificationMessages %}

                            {% set cls = 'alert alert-info' %}
                            {% if userNotificationMessage.isOverDue() %}
                                {% set cls = 'alert alert-danger' %}
                            {% elseif userNotificationMessage.isGonnaBeOverDue() %}
                                {% set cls = 'alert alert-warning' %}
                            {% elseif userNotificationMessage.isFinished() %}
                                {% set cls = 'alert alert-success' %}
                            {% endif %}
                            <div class="{{ cls }}">
                                {{ userNotificationMessage.message }}. {{ userNotificationMessage.dueDate ? 'Završiti do - ' ~ userNotificationMessage.dueDate.short : ''}}

                                {% if userNotificationMessage.isActive() or userNotificationMessage.isPostponed() %}
                                    <a href="#" class="alert-link pull-right" onclick="unsubscribeMessage('{{ userNotificationMessage.id }}', event)">
                                        <i class="fa fa-times"></i>
                                    </a>
                                {% endif %}
                                &nbsp;
                                {% if not userNotificationMessage.isFinished() %}
                                <a style="margin-right: 3px" href="#" class="alert-link pull-right" onclick="getForm('/user-notifications/get-postpone-form/{{ userNotificationMessage.id }}', '/user-notifications/postpone/{{ userNotificationMessage.id }}', event)">
                                    <i class="fa fa-clock-o"></i>
                                </a>
                                {% endif %}
                            </div>
                        {% endfor %}
                    {% endfor %}
                </div>
            </div>
        </div>
    {% endfor %}
</div>