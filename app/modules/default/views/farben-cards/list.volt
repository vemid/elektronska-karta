<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center">
                                <h3>Kreiraj Farben Kartu</h3>
                                <a href="#" onclick="getCreateForm('farben-cards',event)"
                                   class="bigger-200 btn btn-circle btn-lg">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" >
                        <thead>
                        <tr>
                            <th>{{ t('Product') }}</th>
                            <th>{{ t('Nalozi') }}</th>
                            <th>{{ t('Akcija') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for farbenCard in farbenCards %}
                            <tr>
                                <td>{{ farbenCard.getProduct()}}</td>
                                <td>{{ farbenCard.getWorkOrders()}}</td>
                                <td class="text-center" width="11.5%">
                                    <a href="/farben-cards/overview/{{ farbenCard.id }}"  class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Pregled">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getUpdateForm('farbenCards', {{ farbenCard.id}}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getDeleteForm('farbenCards', {{ farbenCard.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>