<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-20"></div>
                <div class="row">
                    <div class="col-xs-12">
                    <div class="col-xs-9">
                        <div class="pull-left">
                            <a href="#" class="btn btn-info pull-left"
                               onclick="getCreateForm('farben-cards',event)">
                                <i class="fa fa-plus"></i>
                                <span class="nav-label">{{ t('Unos farben karte') }}</span>
                            </a>
                        </div>
                        <div class="pull-right">
                            <a href="/farben-cards/print/{{ farbenCard.id }}" class="btn btn-success pull-right">
                                <i class="fa fa-print"></i>
                                <span class="nav-label">{{ t('Stampa farben karte') }}</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-3"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="space-30"></div>
                    <div class="col-xs-9">
                        <div class="ibox float-e-margins">
                            <div class="col-lg-12">
                                <form id="farbenCardSizes" method="post"
                                      action="/farben-cards/update/{{ farbenCard.id }}"
                                      class="form-horizontal">
                                    <table class="table table-striped table-bordered table-hover">
                                        <tbody>
                                        <tr>
                                            <td align="left" style=" padding: 5px;" width="50%">
                                                <b>Kolekcija : {{ hydrator.getOldCollection() }} </b>
                                            </td>
                                            <td align="left" style=" padding: 5px;">
                                                <b>Proizvod : {{ farbenCard.getProduct() }} </b>
                                            </td>
                                        </tr>
                                        <tr valign="center">
                                            <td class="form-group" style="padding-left: 2px !important; padding-right: 2px !important;" colspan="2">
                                                <input data-form="true" type="text"
                                                       name="create[farbenCardPaperLabel]"
                                                       id="farbenCardPaperLabel"
                                                       value="{{ farbenCard.paperLabel }}"
                                                       data-float="true" class="form-control" style="padding: 2px !important;" placeholder="Podaci o sirovinskom sastavu" />
                                            </td>
                                        </tr>
                                        <tr>

                                            <td align="left" style=" padding: 5px;" colspan="2">
                                                <b>Ukupno : {{ farbenCard.qtyTotal }} </b>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="col-xs-12" style="margin-bottom: 20px !important;">
                                        {% for washItem in washItems %}
                                            <div class="col-xs-1 text-center" >
                                                <a class="product-tooltip text-danger" data-toggle="tooltip"  title="{{ washItem.description }}">
                                                    <img src="{{ washItem.getImagePath() }}" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="washed[{{washItem.id}}]" value="{{ washItem.id }}" {% if washItem.printSign in washData %}checked{% endif %}>
                                                </a>
                                            </div>
                                        {% endfor %}
                                    </div>
{#                                    <div class="col-xs-12" style="margin-bottom: 20px !important;">#}
{#                                        <div class="col-xs-1 text-center" >#}
{#                                            <img src="/img/textile/30.png" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="wash[0]" value="a" {% if 'a' in washData %}checked{% endif %}>#}
{#                                        </div>#}
{#                                        <div class="col-xs-1 text-center" >#}
{#                                            <img src="/img/textile/40.png" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="wash[1]" value="b" {% if 'b' in washData %}checked{% endif %}>#}
{#                                        </div>#}
{#                                        <div class="col-xs-1 text-center" >#}
{#                                            <img src="/img/textile/no-iron.png" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="wash[2]" value="$" {% if '$' in washData %}checked{% endif %}>#}
{#                                        </div>#}
{#                                        <div class="col-xs-1 text-center" >#}
{#                                            <img src="/img/textile/iron.png" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="wash[3]" value="!" {% if '!' in washData %}checked{% endif %}>#}
{#                                        </div>#}
{#                                        <div class="col-xs-1 text-center" >#}
{#                                            <img src="/img/textile/not-wash.png" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="wash[4]" value="s" {% if 's' in washData %}checked{% endif %}>#}
{#                                        </div>#}
{#                                        <div class="col-xs-1 text-center" >#}
{#                                            <img src="/img/textile/hand-wash.png" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="wash[5]" value="t" {% if 't' in washData %}checked{% endif %}>#}
{#                                        </div>#}
{#                                        <div class="col-xs-1 text-center" >#}
{#                                            <img src="/img/textile/p.png" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="wash[6]" value="A" {% if 'A' in washData %}checked{% endif %}>#}
{#                                        </div>#}
{#                                        <div class="col-xs-1 text-center" >#}
{#                                            <img src="/img/textile/bleach.png" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="wash[7]" value="x" {% if 'x' in washData %}checked{% endif %}>#}
{#                                        </div>#}
{#                                        <div class="col-xs-1 text-center" >#}
{#                                            <img src="/img/textile/dryer.png" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="wash[8]" value="+" {% if '+' in washData %}checked{% endif %}>#}
{#                                        </div>#}
{#                                        <div class="col-xs-1 text-center" >#}
{#                                            <img src="/img/textile/line.png" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="wash[9]" value="9" {% if '9' in washData %}checked{% endif %}>#}
{#                                        </div>#}
{#                                        <div class="col-xs-1 text-center" >#}
{#                                            <a class="product-tooltip text-danger" data-toggle="tooltip"  title="Sušenje kačenjem">#}
{#                                            <img src="/img/textile/dry-line.png" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="wash[10]" value="\" {% if '\\' in washData %}checked{% endif %}>#}
{#                                            </a>#}
{#                                        </div>#}
{#                                        <div class="col-xs-1 text-center" >#}
{#                                            <a class="product-tooltip text-danger" data-toggle="tooltip"  title="Suvo ciscenje nije dozvoljeno">#}
{#                                            <img src="/img/textile/dry-clean.png" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="wash[11]" value="7" {% if '7' in washData %}checked{% endif %}>#}
{#                                            </a>#}
{#                                        </div>#}
{#                                    </div>#}

                                    <div class="col-lg-9" style="padding: 0px !important;">
                                        <table class="table table-striped table-bordered table-hover total text-center vertical-align">
                                            <thead>
                                            <tr>
                                                <th class="text-center">Velicine:</th>
                                                {% for productSize in farbenCard.getProduct().productSizes %}
                                                    <th class="text-center"
                                                        style="background: slategray !important;">{{ productSize.code.code }}</th>
                                                {% endfor %}
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {% for data in typeData %}
                                                <tr>
                                                    <td class="form-group"
                                                        style="width: 150px !important; vertical-align: center !important;">
                                                        <b>{{ data }}</b>
                                                    </td>
                                                    {% set i = 0 %}
                                                    {% for productSize in farbenCard.getProduct().productSizes %}
                                                        {% if data == "Total Kolicina" %}
                                                            <td style="background: lightgreen !important; ">{{ dataTotal["Total Kolicina"][productSize.codeId] ? dataTotal["Total Kolicina"][productSize.codeId] : "" }}
                                                                <a href="#"
                                                                   onclick="getUpdateForm('order-totals', {{ dataTotal["orderTotalId"][productSize.codeId] }}, event)">
                                                                    <i class="fas fa-pencil-alt"></i>
                                                                </a>
                                                            </td>
                                                        {% elseif data == "Nalog" %}
                                                            <td class="form-group" style="padding-left: 2px !important; padding-right: 2px !important;">
                                                                <input data-form="true" type="text"
                                                                       name="create[{{ data }}][{{ productSize.id }}][farbenCardWarrant]"
                                                                       id="farbenCardWarrant"
                                                                       value="{{ productSize.farbenCardWarrant }}"
                                                                       data-float="true" class="form-control" style="padding: 2px !important;"/>
                                                            </td>
                                                        {% elseif data == "Raspis" %}
                                                            <td class="form-group" style="padding-left: 2px !important; padding-right: 2px !important;">
                                                                <input data-form="true" type="text"
                                                                       name="create[{{ data }}][{{ productSize.id }}][farbenFiqQty]"
                                                                       id="farbenFiqQty"
                                                                       value="{{ productSize.farbenFigQty }}"
                                                                       data-float="true" class="form-control" style="padding: 2px !important;"/>
                                                            </td>
                                                        {% elseif data == "Dodatni Raspis" %}
                                                            <td class="form-group" style="padding-left: 2px !important; padding-right: 2px !important;">
                                                                <input data-form="true" type="text"
                                                                       name="create[{{ data }}][{{ productSize.id }}][farbenRestQty]"
                                                                       id="farbenRestQty"
                                                                       value="{{ productSize.farbenRestQty }}"
                                                                       data-float="true" class="form-control" style="padding: 2px !important;"/>
                                                            </td>
                                                        {% endif %}
                                                        {% set i=i+1 %}
                                                    {% endfor %}
                                                </tr>
                                            {% endfor %}

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-lg-3" style="padding: 0px !important;">
                                        <table class="table table-striped table-bordered table-hover total text-center">
                                            <thead>
                                            <tr style="height: 37px !important;">
                                                <td colspan="{{ farbenCard.getProduct().productSizes|length }}"
                                                    rowspan="1">
                                                    Dopuna:
                                                </td>
                                            </tr>
                                            <tr style="height: 37px !important;">
                                                {% for productSize in farbenCard.getProduct().productSizes %}
                                                    <td style="background: slategray !important;">{{ productSize.code.code }}</td>
                                                {% endfor %}
                                            </tr>
                                            <tr style="height: 51px !important;">
                                                {% for productSize in farbenCard.getProduct().productSizes %}
                                                    <td style="background: lightgreen">{{ productSizeRepository.getTotalB(productSize) }}</td>
                                                {% endfor %}
                                            </tr>
                                            <tr style="height: 51px !important;">
                                                {% for productSize in farbenCard.getProduct().productSizes %}
                                                    <td></td>
                                                {% endfor %}
                                            </tr>
                                            <tr style="height: 51px !important;">
                                                {% for productSize in farbenCard.getProduct().productSizes %}
                                                    <td></td>
                                                {% endfor %}
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-sm btn-success">
                                            <i class="fa fa-save"></i> Sacuvaj naloge i raspis
                                        </button>
                                        </td>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3 pull-left">
                        <div style="height: 350px !important;">
                            <input type="image" src="{{ farbenCard.getProduct().getImagePath() }}" height="100%"
                                   style="cursor: pointer; outline: none;"/>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <form id="formFarbenCardItems">
                        <input type="hidden" name="farbenCardId" value="{{ farbenCard.id }}"/>

                        <table class="table table-striped table-bordered table-hover farbenCardItems">
                            <thead>
                            <tr>
                                <th class="col-xs-5">{{ t('Text') }}</th>
                                <th class="col-xs-6">{{ t('Opis') }}</th>
                                <th class="text-center col-xs-1">{{ t('Akcije') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for farbenCardItem in farbenCardItems %}
                                <tr>
                                    <td class="col-xs-5">
                                        <input type="hidden"
                                               name="edit[{{ farbenCardItem.id }}][materialFactoryId]"
                                               id="materialFactoryId"
                                               value="{{ farbenCardItem.getMaterialFactory().id }}"/>
                                        <input type="hidden" name="edit[{{ farbenCardItem.id }}][name]"
                                               id="name" value="{{ farbenCardItem.name }}"/>
                                        <input type="text" style="width:100%!important" class="form-control"
                                               name="materialFactoryIdSearchFarben" id="materialFactoryIdSearchFarben"
                                               value="{{ farbenCardItem.getMaterialFactory() ? farbenCardItem.getMaterialFactory().getDisplayName() : farbenCardItem.name }}"/>
                                    </td>
                                    <td class="col-xs-6">
                                        <input type="text" style="width:100%!important" class="form-control"
                                               name="edit[{{ farbenCardItem.id }}][description]" id="description"
                                               value="{{ farbenCardItem.description }}"/>
                                    </td>
                                    <td class="text-center col-xs-1" width="11.5%">
                                        <a class="product-tooltip text-danger" data-toggle="tooltip"  title="<p>{{ farbenCardItem.getMaterialFactory().composition }}</p>{{ careLabel.getCareLabelsImage(farbenCardItem.getMaterialFactory()) }}">
                                            <button class="btn btn-success btn-circle" type="button">
                                                <i class="fa fa-info"></i>
                                            </button>
                                        </a>
                                        <a href="#"
                                           onclick="getDeleteForm('farben-card-items', {{ farbenCardItem.id }}, event)"
                                           class="bigger-140 text-danger">
                                            <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </form>
                </div>
                <div class="row">
                    <div class="space-30"></div>
                    <div class="col-xs-12">
                        <div class="col-xs-4 pull-left">
                            <a href="#" title="Dodavanje" data-id="{{ farbenCard.id }}"
                               id="addFarbenCardItem" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus text-info"><span style="font-size: 15px; padding-left: 10px">Dodaj novi red</span></i>
                            </a>
                        </div>
                        <div class="col-xs-4 text-center">
                            <a href="#" class="btn btn-info" id="saveFarbenCardItems">
                                Snimi <i class="fa fa-save"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>