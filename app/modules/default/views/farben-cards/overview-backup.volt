<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-20"></div>
                <div class="row">
                    <div class="col-xs-6"></div>
                    <div class="col-xs-6 pull-right">
                        <div class="col-xs-3 ">
                            <a href="#" class="btn btn-info pull-right"
                               onclick="getCreateForm('farben-cards',event)">
                                <i class="fa fa-plus"></i>
                                <span class="nav-label">{{ t('Unos farben karte') }}</span>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <a href="/farben-cards/print/{{ farbenCard.id }}" class="btn btn-success pull-right">
                                <i class="fa fa-print"></i>
                                <span class="nav-label">{{ t('Stampa farben karte') }}</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="space-30"></div>
                    <div class="col-xs-9">
                        <div class="ibox float-e-margins">
                            <div class="col-lg-12">
                                <table class="table table-striped table-bordered table-hover">
                                    <tbody>
                                    <tr>
                                        <td align="left" style=" padding: 5px;">
                                            <b>Proizvod : {{ farbenCard.getProduct() }} </b>
                                        </td>
                                        <td align="left" style=" padding: 5px;">
                                            <b>Kolekcija : {{ hydrator.getOldCollection() }} </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style=" padding: 5px;" width="25%">
                                            <b>Usivna etiketa : {{ farbenCard.washLabel }} </b>
                                        </td>
                                        <td align="left" style=" padding: 5px;" width="25%">
                                            <b>Papirna etiketa : {{ farbenCard.paperLabel }} </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style=" padding: 5px;" width="25%">
                                            <b>Raspis : {{ farbenCard.cutTotal }} </b>
                                        </td>
                                        <td align="left" style=" padding: 5px;" width="25%">
                                            <b>Ukupno : {{ farbenCard.qtyTotal }} </b>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <form id="farbenCardSizes" method="post"
                                      action="/farben-cards/add-farben-card-sizes/{{ farbenCard.id }}"
                                      class="form-horizontal">
                                    <div class="col-lg-9" style="padding: 0px !important;">
                                        <table class="table table-striped table-bordered table-hover total text-center vertical-align">
                                            <thead>
                                            <tr>
                                                <th class="text-center">Velicine:</th>
                                                {% for productSize in farbenCard.getProduct().productSizes %}
                                                    <th class="text-center"
                                                        style="background: slategray !important;">{{ productSize.code.code }}</th>
                                                {% endfor %}
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {% for data in typeData %}
                                                <tr>
                                                    <td class="form-group"
                                                        style="width: 150px !important; vertical-align: center !important;">
                                                        <b>{{ data }}</b>
                                                    </td>
                                                    {% set i = 0 %}
                                                    {% for productSize in farbenCard.getProduct().productSizes %}
                                                        {% if data == "Total Kolicina" %}
                                                            <td style="background: lightgreen !important; ">{{ dataTotal["Total Kolicina"][productSize.codeId] ? dataTotal["Total Kolicina"][productSize.codeId] : "" }}</td>
                                                        {% elseif data == "Nalog" %}
                                                            <td class="form-group">
                                                                <input data-form="true" type="text"
                                                                       name="create[{{ data }}][{{ productSize.id }}][farbenCardWarrant]"
                                                                       id="farbenCardWarrant"
                                                                       value="{{ productSize.farbenCardWarrant }}"
                                                                       data-float="true" class="form-control"/>
                                                            </td>
                                                        {% elseif data == "Raspis" %}
                                                            <td class="form-group">
                                                                <input data-form="true" type="text"
                                                                       name="create[{{ data }}][{{ productSize.id }}][totalQuantityA]"
                                                                       id="totalQuantityA"
                                                                       value="{{ productSize.totalQuantityA }}"
                                                                       data-float="true" class="form-control"/>
                                                            </td>
                                                        {% endif %}
                                                        {% set i=i+1 %}
                                                    {% endfor %}
                                                </tr>
                                            {% endfor %}

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-lg-3" style="padding: 0px !important;">
                                        <table class="table table-striped table-bordered table-hover total text-center">
                                            <thead>
                                            <tr style="height: 37px !important;">
                                                <td colspan="{{ farbenCard.getProduct().productSizes|length }}"
                                                    rowspan="1">
                                                    Dopuna:
                                                </td>
                                            </tr>
                                            <tr style="height: 37px !important;">
                                                {% for productSize in farbenCard.getProduct().productSizes %}
                                                    <td style="background: slategray !important;">{{ productSize.code.code }}</td>
                                                {% endfor %}
                                            </tr>
                                            <tr style="height: 51px !important;">
                                                {% for productSize in farbenCard.getProduct().productSizes %}
                                                    <td style="background: lightgreen">{{ productSizeRepository.getTotalB(productSize) }}</td>
                                                {% endfor %}
                                            </tr>
                                            <tr style="height: 51px !important;">
                                                {% for productSize in farbenCard.getProduct().productSizes %}
                                                    <td></td>
                                                {% endfor %}
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-sm btn-success">
                                            <i class="fa fa-save"></i> Sacuvaj naloge i raspis
                                        </button>
                                        </td>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3 pull-left">
                        <div style="height: 350px !important;">
                            <input type="image" src="{{ farbenCard.getProduct().getImagePath() }}" height="100%"
                                   style="cursor: pointer; outline: none;"/>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="space-30"></div>
                {% set counter = farbenCardItems|length %}
                <div class="row">
                    <div class="col-xs-12">
                        <form id="addFarbenCard" method="post"
                              action="/farben-cards/add-farben-card/{{ farbenCard.id }}" class="form-horizontal">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th width="5%">Rb</th>
                                    <th width="80%">Materijal</th>
                                    {% if farbenCardItems|length %}
                                        <th class="text-center col-lg-1"></th>
                                    {% endif %}
                                </tr>
                                </thead>
                                <tbody>
                                {% for key, farbenCardItem in farbenCardItems %}
                                    <tr>
                                        <td class="text-center"><span>{{ key + 1 }}</span></td>
                                        <td class="form-group">
                                            <input data-form="true" type="text"
                                                   name="edit[{{ farbenCardItem.id }}][text]"
                                                   value="{{ farbenCardItem.text }}" id="text" class="form-control"
                                                   data-float="true" disabled/>
                                        </td>
                                        <td>
                                            <a href="#"
                                               onclick="getUpdateForm('farben-card-items', {{ farbenCardItem.id }}, event)"
                                               class="bigger-140 text-danger">
                                                <button class="btneport-work-time btn-success btn-circle" type="button"
                                                        title="Izmena">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                            </a>
                                            <a href="#"
                                               onclick="getDeleteForm('farben-card-items', {{ farbenCardItem.id }}, event)"
                                               class="bigger-140 text-danger">
                                                <button class="btn btn-danger btn-circle" type="button"
                                                        title="Brisanje">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                {% endfor %}
                                {% for i in counter..14 %}
                                    <tr>
                                        <td class="text-center"><span>{{ i + 1 }}</span></td>
                                        <td class="form-group">
                                            <input data-form="true" type="text" name="create[{{ i }}][text]" id="text"
                                                   value="" data-float="true" class="form-control"/>
                                        </td>
                                    </tr>
                                {% endfor %}
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-save"></i> Sačuvaj
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>