<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css"/>
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css"/>
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
    <style>
        .page-break {
            page-break-after: always;
        }

        .page-breaker {
            clear: both;
            display: block;
            border: 1px solid transparent;
            page-break-before: always;
        }
    </style>
</head>
<body class="pace-done">
<div class="space-20"></div>
<div class="col-xs-12">
<div class="col-xs-8">
    <div class="space-20"></div>
    <table class="table table-striped table-bordered table-hover no-padding"
           style="margin-bottom: 5px !important;">
        <tbody>
        <tr>
            <td align="left" valign="center" style=" padding: 2px !important;">
                <b>Ukupno: {{ farbenCard.qtyTotal }}</b>
            </td>
            <td>
                <b>Proizvod : {{ farbenCard.getProduct() }} </b>
            </td>
        </tr>
        {#<tr>#}
            {#<td align="left" style=" padding: 2px !important;">#}
                {#<b>Kolekcija : {{ hydrator.getOldCollection() }} </b>#}
            {#</td>#}
        {#</tr>#}
        <tr valign="center">
            <td align="left" valign="center" style=" padding: 2px !important;" colspan="2">
                <b>Papirna etiketa : {{ farbenCard.paperLabel }}</b>
            </td>
        </tr>
        {#<tr>#}

            {#<td align="left" style=" padding: 2px !important;" width="25%" colspan="2">#}
                {#<b>Ukupno : {{ farbenCard.qtyTotal }} </b>#}
            {#</td>#}
        {#</tr>#}
        </tbody>
    </table>

    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-12">
            <div class="col-xs-1 text-center">
                <b>Simboli    </b>
            </div>
                {% for washItem in washItems %}
                    {% if washItem.printSign in washData %}
                        <div class="col-xs-1">
                            <img src="{{washItem.getImagePath()}}" alt="" width="30px">
                        </div>
                    {% endif %}
                {% endfor %}
            </div>
            <div class="space-5"></div>
            <div class="col-lg-12">
                <div class="col-lg-9" style="padding: 0px !important;">
                    <table class="table table-striped table-bordered table-hover total text-center vertical-align no-padding"
                           style="margin-bottom: 5px !important;">
                        <thead>
                        <tr>
                            <th class="text-center" style=" padding: 2px !important;">Velicine:</th>
                            {% for productSize in farbenCard.getProduct().productSizes %}
                                <th class="text-center"
                                    style="background: slategray !important; padding: 2px !important;">{{ productSize.code.code }}</th>
                            {% endfor %}
                        </tr>
                        </thead>
                        <tbody>
                        {% for data in typeData %}
                            <tr style="padding: 2px !important;">
                                <td
                                        style="width: 150px !important; vertical-align: center !important; padding: 2px !important;">
                                    <b>{{ data }}</b>
                                </td>
                                {% set i = 0 %}
                                {% for productSize in farbenCard.getProduct().productSizes %}
                                    {% if data == "Total Kolicina" %}
                                        <td style="padding:2px !important; background: lightgreen !important; ">{{ dataTotal["Total Kolicina"][productSize.codeId] ? dataTotal["Total Kolicina"][productSize.codeId] : "" }}</td>
                                    {#{% elseif data == "Nalog" %}#}
                                        {#<td style="padding: 2px !important;">#}
                                            {#{{ productSize.farbenCardWarrant }}#}
                                        {#</td>#}
                                    {% elseif data == "Raspis" %}
                                        <td style="padding: 2px !important;">
                                            {{ productSize.farbenFigQty }}
                                        </td>
                                    {% elseif data == "Dodatni Raspis" %}
                                        <td style="padding: 2px !important;">
                                            {{ productSize.farbenRestQty }}
                                        </td>
                                    {% elseif data == "Dopuna" %}
                                        <td style="padding: 2px !important; background: lightskyblue !important;">
                                            {{ productSizeRepository.getTotalB(productSize) }}
                                        </td>
                                    {% endif %}
                                    {% set i=i+1 %}
                                {% endfor %}
                            </tr>
                        {% endfor %}

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="col-xs-4 pull-right no-padding">
    <div class="col-xs-2 no-padding" style="width: 80px !important;">
        {% for productSize in farbenCard.getProduct().productSizes %}
            {{ productSize.code.code }} - {{ productSize.farbenCardWarrant }}
            <hr style="margin:0px !important;">
        {% endfor %}
    </div>
    <div class="col-xs-2 no-padding">
        <div style="height: 220px !important; margin-top: -10px;">
            <input type="image" src="{{ farbenCard.getProduct().getImagePrintPath() }}" height="100%"
                   style="cursor: pointer; outline: none;"/>
        </div>
    </div>


</div>
</div>
<div class="space-20"></div>
<div class="col-xs-12">
<div style="height: 30px !important;"><p> </p></div>
<div class="row" style="font-size: 10px !important;">
    <div class="col-xs-12">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover no-padding"
                   style="padding: 2px!important; margin: 2px !important; font-size: 12px !important;">
                <thead>
                <tr>
                    <th style="padding: 2px !important;" colspan="2"
                        class="col-xs-10 text-center">{{ t('Materijali') }}</th>
                </tr>
                </thead>
                <tbody>
                {#{% set columnLength = ceil(farbenCardItems|length / 2) %}#}

                {#{% set i = 0 %}#}
                {#{% set j = 0 %}#}
                {#{% for index,i in 0..columnLength %}#}
                {#<tr>#}
                {#{% for indexJ,j in 0..2 %}#}
                {#{% set arrayIndex = j*columnLength+i %}#}
                {#{% if arrayIndex <= farbenCardItems|length %}#}

                {#<td style="padding: 2px !important;">#}
                {#{{ farbenCardItems[(indexJ+index)].getMaterialFactory() ? farbenCardItems[(indexJ+index)].getMaterialFactory().getDisplayName() : farbenCardItems[(indexJ+index)].name }}#}
                {#</td>#}
                {#{% endif %}#}
                {#{% set j = j+1 %}#}
                {#{% endfor %}#}
                {#</tr>#}
                {#{% set i = i+1 %}#}
                {#{% endfor %}#}

                {% for farbenCardItem in farbenCardItems %}
                    <tr>
                        <td style="padding: 2px !important;">
                            {{ farbenCardItem.getMaterialFactory() ? farbenCardItem.getMaterialFactory().getDisplayName() : farbenCardItem.name }}
                            {% if farbenCardItem.description == null %}
                            {% else %}
                                - {{ farbenCardItem.description }}
                            {% endif %}
                        </td>
                    </tr>
                {% endfor %}

                {#{% for i,items in halved %}#}
                {#<td width="50%">#}
                {#{% for j,item in items %}#}
                {#{{ halved[i][j]['item'] }}#}
                {#<hr style="margin:0px !important;">#}
                {#{% endfor %}#}
                {#</td>#}
                {#{% endfor %}#}

                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</body>
</html>