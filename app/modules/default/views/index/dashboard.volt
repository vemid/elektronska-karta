<div class="row">
    <div class="col-xs-12">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-table-1">{{ currentYear.format('Y') }}</a></li>
                <li><a data-toggle="tab" href="#tab-table-2">{{ previousYear.format('Y') }}</a></li>
                <li><a data-toggle="tab" href="#tab-table-3">{{ twoYearsAgo.format('Y') }}</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-table-1" class="tab-pane active">
                    <div class="panel-body">
                        {{ partial('reports/main-table', ['cars' : cars, 'data' : currentYearData, 'date' : currentYear]) }}
                    </div>
                </div>
                <div id="tab-table-2" class="tab-pane">
                    <div class="panel-body">
                        {{ partial('reports/main-table', ['cars' : cars, 'data' : previousYearData, 'date' : previousYear]) }}
                    </div>
                </div>
                <div id="tab-table-3" class="tab-pane">
                    <div class="panel-body">
                        {{ partial('reports/main-table', ['cars' : cars, 'data' : twoYearsAgoData, 'date' : twoYearsAgo]) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>