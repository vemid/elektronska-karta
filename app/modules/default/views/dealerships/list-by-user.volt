<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dealershipWarrantTables" id="warrant" >
                        <thead>
                        <tr>
                            <th>{{ t('Status') }}</th>
                            <th>{{ t('Datum') }}</th>
                            <th>{{ t('Vrednost') }}</th>
                            <th>{{ t('Pregled') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for dealershipWarrant in dealershipWarrants %}
                            <tr>
                                <td>{{ dealershipWarrantRepository.getStatus(dealershipWarrant.date,dealershipWarrant.codeId)}}</td>
                                <td>{{ dealershipWarrant['date'] }}</td>
                                <td>{{ dealershipWarrant['totalAmount'] }}</td>
                                <td class="text-center" width="11.5%">
                                    <a href="/dealerships/print/{{ dealershipWarrant.date }}/{{ dealershipWarrant.codeId }}"
                                       target="_blank" class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button"
                                                title="Pregled"><i class="fa fa-print"></i></button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready( function () {
        $('#warrant').DataTable({
            "fnRowCallback": function( nRow, aData ) {
                if ( aData[0] === "1" )
                {
                    $('td', nRow).css('background-color', 'lightgreen');
                }
                if ( aData[0] === "2" )
                {
                    $('td', nRow).css('background-color', 'yellow');
                }
            },
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }],
            "aaSorting": [],
            "pageLength": 50
        });
    })
</script>