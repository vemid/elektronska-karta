<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title></title>
    <meta name="generator" content="LibreOffice 5.1.6.2 (Linux)"/>
    <meta name="author" content="biro"/>
    <meta name="created" content="2005-10-24T07:43:38"/>
    <meta name="changed" content="2018-02-16T22:03:27.402177308"/>

    <style type="text/css">
        body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Arial"; font-size:x-small }
        a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  }
        a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  }
        comment { display:none;  }
    </style>

</head>

<body>
{% for dealershipWarrant in dealershipWarrants %}
    <table cellspacing="0" border="0">
        <colgroup width="11"></colgroup>
        <colgroup width="62"></colgroup>
        <colgroup span="2" width="67"></colgroup>
        <colgroup width="78"></colgroup>
        <colgroup width="30"></colgroup>
        <colgroup width="67"></colgroup>
        <colgroup width="22"></colgroup>
        <colgroup width="67"></colgroup>
        <colgroup width="37"></colgroup>
        <colgroup width="67"></colgroup>
        <colgroup width="51"></colgroup>
        <colgroup width="41"></colgroup>
        <tr>
            <td style="border-top: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td style="border-top: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-top: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-top: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-top: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-top: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-top: 1px solid #3c3c3c" colspan=6 align="right"><b>НАЛОГ ЗА ПРЕНОС</b></td>
            <td style="border-top: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #3c3c3c" height="34" align="left"><br></td>
            <td colspan=4 align="left">дужник - налогодавац</td>
            <td align="left"><br></td>
            <td align="left">шифра плаћања</td>
            <td align="left"><br></td>
            <td align="left">валута</td>
            <td align="left"><br></td>
            <td align="left">износ</td>
            <td align="left"><br></td>
            <td style="border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td style="border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" colspan=4 rowspan=2 align="center"><br><strong>{{ dealership }}</strong></td>
            <td align="left"><br></td>
            <td style="border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" rowspan=2 align="center"><br>221</td>
            <td align="left"><br></td>
            <td style="border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" rowspan=2 align="center"><br> RSD</td>
            <td align="left"><br></td>
            <td style="border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" colspan=2 rowspan=2 align="center" sdnum="1033;0;#,##0.00"><br><strong>{{ dealershipWarrant.amount }}</strong></td>
            <td style="border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td style="border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td colspan=4 align="left">сврха плаћања</td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td colspan=4 align="left">рачун дужника - налогодавца</td>
            <td style="border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td style="border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" colspan=4 rowspan=2 align="center"><br>Uplata prema nalogu</td>
            <td align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" rowspan=2 align="left">број модела</td>
            <td align="left"><br></td>
            <td style="border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" colspan=4 align="center"><br><strong>{{ dealership.account }}</strong></td>
            <td style="border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" colspan=4 align="left">позив на број (задужење)</td>
            <td style="border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td colspan=4 align="left">поверилац - прималац</td>
            <td align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" align="center"><br></td>
            <td align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" colspan=4 align="center"><br></td>
            <td style="border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td style="border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" colspan=4 rowspan=2 align="center"><br><strong>Kids Beba DOO, Ignjata Joba 37, 11000 Beograd</strong></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" colspan=4 align="left">рачун повериоца - примаоца</td>
            <td style="border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" rowspan=2 align="left">број модела</td>
            <td align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" colspan=4 align="center"><br><strong>160-327566-85</strong></td>
            <td style="border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td colspan=4 align="left">позив на број (одобрење)</td>
            <td style="border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td colspan=4 align="left">____________________________________</td>
            <td align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" align="center"><br>97</td>
            <td align="left"><br></td>
            <td style="border-top: 1px solid #3c3c3c; border-bottom: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" colspan=4 align="center"><br><strong>{{ dealershipWarrant.warrantNumber }}</strong></td>
            <td style="border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td colspan=4 align="left">печат и потпис налогодавца (ПИН КОД)</td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td rowspan=3 align="left"><br><img src="nalog-za-prenos_html_3a4089ab5523bf8f.png" width=21 height=18 hspace=15 vspace=17>
            </td>
            <td style="border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td align="left"><br></td>
            <td colspan=3 align="center"><br></td>
            <td align="left"><br></td>
            <td colspan=4 align="center">__________________________</td>
            <td align="left"><br></td>
            <td style="border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-left: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td align="left"><br></td>
            <td colspan=3 align="left">место и датум пријема</td>
            <td align="left"><br></td>
            <td colspan=3 align="center">датум валуте</td>
            <td align="left"><br></td>
            <td align="left"><br></td>
            <td style="border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid #3c3c3c; border-left: 1px solid #3c3c3c" height="17" align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c" align="left"><br></td>
            <td style="border-bottom: 1px solid #3c3c3c; border-right: 1px solid #3c3c3c" align="left"><br></td>
        </tr>
    </table>
    {% if not loop.last %}
        <br>
        <br>
    {% endif %}
{% endfor %}
<!-- ************************************************************************** -->
</body>

</html>