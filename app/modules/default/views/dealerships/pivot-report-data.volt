<html>
<head></head>
<body>
<div class="row" style="height: 800px !important;">
    <div class="col-xs-2">
        <button class="btn btn-success" value="Refresh Page" onClick="window.location.href=window.location.href">Refresh</button>
    </div>
    <div id="wdr-component" class="col-xs-12" style="height: 100% !important;">
    </div>
</div>
{#<link href="https://cdn.webdatarocks.com/latest/webdatarocks.min.css" rel="stylesheet"/>#}
{#<script src="https://cdn.webdatarocks.com/latest/webdatarocks.toolbar.min.js"></script>#}
{#<script src="https://cdn.webdatarocks.com/latest/webdatarocks.js"></script>#}
<script>
    const data = JSON.parse('{{ data }}');
    var pivot = new WebDataRocks({
        container: "#wdr-component",
        width: "90%",
        height: "100%",
        toolbar: true,
        report: {
            dataSource: {
                "dataSourceType":"json",
                "data": data
            },
            "slice": {
                "reportFilters": [
                    {
                        "uniqueName": "status",
                        "filter": {
                            "members": [
                                "status.0"
                            ]
                        }
                    }
                ],
                "rows": [
                    {
                        "uniqueName": "objekat"
                    },
                    {
                        "uniqueName": "datumKreiranja.Year"
                    },
                    {
                        "uniqueName": "datumKreiranja.Month"
                    },
                    {
                        "uniqueName": "nalog"
                    },
                    {
                        "uniqueName": "dokument"
                    }
                ],
                "columns": [
                    {
                        "uniqueName": "tip"
                    },
                    {
                        "uniqueName": "Measures"
                    }
                ],
                "measures": [
                    {
                        "uniqueName": "iznos",
                        "aggregation": "sum"
                    }
                ]
            },
            "formats" : [
                {
                    "name": "4rwcbnb6",
                    "thousandsSeparator": " ",
                    "decimalSeparator": ".",
                    "currencySymbol": "€ ",
                    "decimalPlaces": 1,
                    "currencySymbolAlign": "left",
                    "nullValue": "",
                    "textAlign": "right",
                    "isPercent": false
                }
            ],
            "expands": {
                "expandAll": false,
            },
            "drills": {
                "drillAll": false
            },
        }
    });
</script>
</body>
</html>