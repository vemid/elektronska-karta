<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-6">
                            <a style="margin-right: 5px" href="#" class="btn btn-warning" onclick="getForm('/dealerships/update-mis-statements-form', '/dealerships/update-mis-statements', event, '')">
                                <i class="fa fa-retweet"></i> {{ t('Osvezi izvode u MIS-u') }}
                            </a>
                        </div>
                        <div class="col-xs-6">
                            <div class="pull-right">
                                <form action="/dealerships/upload-file" method="post" enctype="multipart/form-data">
                                    <div title="{{ t('Snimi file') }}" class="dropzone dropZoneSkull">
                                        <div class="fallback">
                                            <input name="warrentFile" type="file" />
                                        </div>
                                    </div>
                                    <div class="space-20"></div>

                                </form>
                                <div class="col-xs-12">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-8 text-center">
                                        <a href="#" id="start" class="btn btn-primary btn-rounded btn-block">
                                            {{ t('Snimi') }} <i class="fa fa-upload"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables" id="warrant">
                                <thead>
                                <tr>
                                    <th>{{ t('Nalog') }}</th>
                                    <th>{{ t('Nalog') }}</th>
                                    <th>{{ t('Datum') }}</th>
                                    <th>{{ t('Objekat') }}</th>
                                    <th>{{ t('Vrednost') }}</th>
                                    <th>{{ t('Pregled') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                {% for data in warrantData %}
                                    <tr>
                                        <td>{{ data.status }}</td>
                                        <td>{{ data.warrantNumber }}</td>
                                        <td>{{ data.date }}</td>
                                        <td>{{ data.getDealership().getCode() }}</td>
                                        <td>{{ data.amount }}</td>
                                        <td class="text-center" width="11.5%">
                                            <a href="/dealerships/print/{{ data.date }}/{{ data.getDealership().codeId }}/{{ data.id }}"
                                               target="_blank" class="bigger-140 text-danger">
                                                <button class="btn btn-info btn-circle" type="button"
                                                        title="Stampa"><i class="fa fa-print"></i></button>
                                            </a>
                                            {% if data.status == 0 %}
                                                <a href="#" onclick="sendPostAjaxCall('/dealerships/book/{{ data.id }}', {})">
                                                    <button class="btn btn-success btn-circle" type="button"
                                                            title="Proknjizi"><i class="fa fa-check"></i></button>
                                                </a>
                                            {% endif %}
                                        </td>
                                    </tr>
                                {% endfor %}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready( function () {
        $('#warrant').DataTable({
            "fnRowCallback": function( nRow, aData ) {
                if ( aData[0] == "1" )
                {
                    $('td', nRow).css('background-color', 'lightgreen');
                }
            },
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }],
            "aaSorting": [],
            "pageLength": 100
        });
    })
</script>