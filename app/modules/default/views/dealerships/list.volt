<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Nova Franšiza</h3>
                            <a href="#" onclick="getCreateForm('dealerships', event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" >
                        <thead>
                        <tr>
                            <th>{{ t('Radnja') }}</th>
                            <th>{{ t('Račun') }}</th>
                            <th>{{ t('Ime') }}</th>
                            <th>{{ t('Adresa') }}</th>
                            <th>{{ t('Grad') }}</th>
                            <th>{{ t('Poštanski broj') }}</th>
                            <th>{{ t('Email') }}</th>
                            <th>{{ t('Akcije') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for dealership in dealerships %}
                            <tr>
                                <td>{{ dealership.code }}</td>
                                <td>{{ dealership.account }}</td>
                                <td>{{ dealership.companyName }}</td>
                                <td>{{ dealership.address }}</td>
                                <td>{{ dealership.city }}</td>
                                <td>{{ dealership.postalCode }}</td>
                                <td>{{ dealership.email }}</td>
                                <td class="text-center" width="15%">
                                    {% if currentUser.isSuperAdmin() %}
                                        <a class="btn btn-success btn-circle" onclick="getUpdateForm('dealerships', {{ dealership.id }}, event)">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                        <a href="#" onclick="getDeleteForm('dealerships', {{ dealership.id }}, event)" class="bigger-140 text-danger">
                                            <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </a>
                                    {% endif %}
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>