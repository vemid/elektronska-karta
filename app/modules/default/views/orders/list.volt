<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="text-center">{{ t('Porudžbine') }}</h3>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj</h3>
                            <a href="#" onclick="getCreateForm('orders', event)" title="Kreiranje Order-a"
                               class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables">
                        <thead>
                        <tr>
                            <th class="col-sm-3 m-b-xs">{{ t('Ime') }}</th>
                            <th class="col-sm-4 m-b-xs">{{ t('Stara kolekcija') }}</th>
                            {% if superAdmin == 1 %}
                                <th class="col-sm-1 m-b-xs text-center">{{ t('Aktivna') }}</th>
                            {% endif %}
                            <th class="m-b-xs text-center text-center">{{ t('Trajanje') }}</th>
                            <th class="text-center col-sm-3 m-b-xs">{{ t('Akcije') }}</th>
                        </thead>
                        <tbody>
                        {% for order in orders %}
                            <tr {{ order.active ? 'style="background-color: lightgreen"' : '' }}>
                                <td>{{ order }}</td>
                                <td class="">
                                    {% for orderClassification in order.orderClassifications %}
                                        {{ orderClassification.classification.name }}{{ loop.last ? '' : ',<br>' }}
                                    {% endfor %}
                                </td>
                                {% if superAdmin == 1 %}
                                    <td class="text-center"><input type="checkbox" {{ order.active ? 'checked' : '' }}
                                                                   id="activeOrder" data-id="{{ order.id }}"
                                                                   class="js-switch orderActive"/></td>
                                {% endif %}
                                <td class="text-center">{{ order.getDateRange() }}</td>
                                <td class="text-center">
                                    {% if superAdmin == 1 %}
                                        <a href="#" class="btn btn-circle btn-success"
                                           onclick="getUpdateForm('orders', '{{ order.id }}', event)">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="/orders/add-order-items/{{ order.id }}"
                                           class="btn btn-circle btn-warning">
                                            <i class="fa fa-upload"></i>
                                        </a>
                                        <a href="/orders/pivot-retail-order-report/{{ order.id }}"
                                           class="btn btn-circle btn-danger" title="Pregled porucenosti MP">
                                            <i class="fa fa-calculator"></i>
                                        </a>
                                        <a class="btn btn-success btn-circle"
                                           onclick="getAjaxForm('orders/add-order-total/{{ order.id }}', event)"
                                           title="Ubacivanje totala">
                                            <i class="fa fa-tools"></i>
                                        </a>
                                         <a href="/orders/overview/{{ order.id }}" class="btn btn-circle btn-info">
                                         <i class="fa fa-search"></i>
                                         </a>
                                    {% endif %}
                                    {# <a href="/orders/overview/{{ order.id }}" class="btn btn-circle btn-info"> #}
                                    {# <i class="fa fa-search"></i> #}
                                    {# </a> #}
                                    {# <a href="/orders/chose-collection-pivot-report" data-order-id="{{ order.id }}" id="{{ order.id }}" class="btn btn-circle btn-info chose-collection-pivot-report"> #}
                                    {# <i class="fa fa-calculator"></i> #}
                                    {# </a> #}
                                    <a href="/orders/pivot-report-data/{{ order.id }}" class="btn btn-circle btn-info">
                                        <i class="fa fa-table"></i>
                                    </a>
                                    <a href="/orders/download-order-data/{{ order.id }}"
                                       class="btn btn-circle btn-info">
                                        <i class="fa fa-file-excel"></i>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
