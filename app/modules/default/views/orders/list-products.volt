<input type="hidden" id="orderId" name="orderId" value="{{ order.id }}">
{% if order %}
    <div class="row">
        <div class="col-xs-12">
            <a href="/orders/pivot-report-data/{{ order.id }}" target="_blank">
                <button class="btn btn-info pull-right"><i
                            class="fa fa-table"></i>
                    Order Report
                </button>
            </a>
        </div>
        <form id="filter">
            <div class="col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title col-xs-12">
                        <div class="col-xs-8">
                        <h4 class="">Kategorije</h4>
                        </div>
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content">
                        {% for category, tag in tags %}
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="col-xs-2">
                                    <p>{{ category }}:</p>
                                </div>
                                {% set totalTags = tag|length %}
                                <?php $perColumn = ceil($totalTags / 3); ?>
                                <div class="col-xs-3 text-center">
                                {% set  counting = 0 %}
                                {% for key, value in tag %}
                                {% set counting = counting + 1 %}
                                    <?php $selectedTag = in_array($value, $selected); ?>
                                    <label for="{{ value }}" class="col-xs-8 text-left">{{ t(value) }}</label>
                                    <input class="col-xs-4 pull-right" type="checkbox"{{ selectedTag ? ' checked' : ''}} name="{{ category }}[]" value="{{ value }}" id="{{ value }}">
                                    {% if counting % perColumn == 0 %}
                                </div>
                                <div class="col-xs-3 text-center">
                                    {% endif %}
                                {% endfor %}
                                </div>
                            </div>
                        </div>
                            {% if not loop.last %}
                            <div class="hr-line-solid"></div>
                            {% endif %}
                        {% endfor %}
                    </div>
                </div>
            </div>

        </form>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3 class="text-center">{{ t('Poručivanje Robe') }}</h3>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="form-horizontal">
                            <div class="col-xs-12">
                                <div class="space-25"></div>
                                <div class="priority-catalog"></div>
                                <div class="table-responsive">
                                    <table id="example" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th class="text-center">{{ t('Šifra') }}</th>
                                            <th class="text-center">{{ t('Naziv') }}</th>
                                            <th class="text-center">{{ t('Boja') }}</th>
                                            <th class="text-center">{{ t('Cena') }}</th>
                                            <th class="col-sm-1 text-center">{{ t('Naruči') }}</th>
                                            <th class="col-sm-1 text-center"></th>
                                            <th class="col-sm-1 text-center"></th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% else %}
    <div class="ibox-content">
        <div class="alert alert-warning">
            <h2>Poštovani, {{ client.name }}</h2>
            <p>PorudžrConbine za ovu sezonu su završene.</p>
        </div>
    </div>
{% endif %}
<script type="text/javascript">
    $(document).ready(function () {

        const client = '{{ client or currentUser.isSuperAdmin() ? 1 : 0 }}';
        const table = $("#example").DataTable({
            "processing": true,
            "serverSide": true,
            "searching": true,
            "pageLength": 50,
            "ajax": {
                "url": "/orders/get-list-products-data/{{ order.id }}",
                "data": function (d) {
                    return $.extend({}, d, {
                        "data": $('#filter').serialize(),
                    });
                }
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (aData[5] === 1) {
                    $('td', nRow).closest('tr').css('background', '#c1ff9d');
                }

                return nRow;
            },
            "fnDrawCallback": function (oSettings) {
                $("a.product-tooltip").tooltip({
                    animated: 'fade',
                    placement: 'top',
                    html: true,
                    container: 'body'
                });
            },
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return '<a class="product-tooltip text-danger" data-toggle="tooltip" title="<img src=\'' + row[6] + '\' width=\'100%\'/>">' + row[0] + '</a>';
                    },
                    "targets": 0
                },
                {
                    "render": function (data, type, row) {
                        var link = '<a data-order-id="' + row[4] + '" id="orderCreate" href="#" class="bigger-140 text-danger"><i class="fa fa-shopping-basket"></i></a>';
                        return client == '1' ? link : '';
                    },
                    "targets": 4
                },
                {
                    "targets": 5,
                    "visible": false
                },
                {
                    "targets": 6,
                    "visible": false
                }
            ],
            "columns": [
                null,
                null,
                null,
                null,
                {className: "text-center"},
                null
            ],
        });

        $(document).on("change", 'input:not([type="text"])', function(){
            if ($(this).is(':checkbox')) {
                let data = $('#filter').serialize();
                let currentUrl = window.location.href.split('?')[0];
                window.location.replace(currentUrl + "?" + data);
            }
        });

        $(document).on("click", "#orderCreate", function (e) {
            e.preventDefault();

            var orderItemId = $(this).attr("data-order-id");
            $.get("/order-items/get-order-form/" + orderItemId + "/{{ order.id }}", function (response) {
                var body = $(response).find("#shopping-cart");
                var modal = $("#modal");
                modal.find('.modal-dialog').css('width', '1280px');

                modal.find(".modal-title").html("Porudžbina");
                modal.find(".modal-title").html("Porudžbina");
                modal.find(".modal-body").html(body);
                modal.find("#save-modal").attr("id", "save-order").html(t('Yes'));
                modal.find("#close-modal").html(t('No'));
                $("body").addClass("modal-open");
                modal.show();
            });
        });

        $(document).on("click", "#save-order", function (e) {

            e.preventDefault();

            var modal = $(this).parents("#modal");
            var form = $(modal).find("form");
            var isValid = getFormValidation($(form));
            var button = $(this);
            var formData = new FormData();

            var formElements = form.find("input, select, textarea").not("input[type='file']");
            formElements.each(function () {
                var name = $(this).attr("name");
                if (!(undefined == name && name == null)) {
                    if ($.isArray($(this).val())) {
                        $.each($(this).val(), function (attrKey, attrVal) {
                            formData.append(name, attrVal);
                        });
                    } else {
                        formData.append(name, $(this).val());
                    }
                }
            });

            var files = $(form).find("input[type='file']");
            $(files).each(function () {
                if ("" != $(this).val()) {
                    var name = $(this).attr("name");
                    formData.append(name, $("#" + name)[0].files[0]);
                }
            });

            formData.append('orderId', '{{ order.id }}');

            if (isValid) {
                $(button).addClass("disabled");
                $.ajax({
                    url: $(form).attr("action"),
                    type: "post",
                    data: formData,
                    dataType: "json",
                    enctype: "multipart/form-data",
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (!data.error) {
                            $("#modal").hide();
                            $("body").removeClass("modal-open");
                            table.draw();
                            renderMessages(data.messages, "success");
                        } else {
                            renderMessages(data.messages, "danger");
                        }
                    },
                });
            }
        });
    });
</script>