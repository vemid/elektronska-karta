<div id="choose-order-html" class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="text-center">{{ t('Otvorene Porudžbine') }}</h3>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables">
                                <thead>
                                <tr>
                                    <th class="col-sm-11 m-b-xs">{{ t('Kolekcije') }}</th>
                                    <th class="text-center col-sm-1 m-b-xs">{{ t('Akcije') }}</th>
                                </thead>
                                <tbody>
                                    {% for order in orders %}
                                    <tr>
                                        <td>
<!--                                             {% for orderClassification in order.orderClassifications %} -->
<!--                                                 {{ orderClassification.classification.name }}{{ loop.last ? '' : ',' }} -->
<!--                                             {% endfor %} -->
{{order.name}} ( {{ order.description}} )
                                        </td>
                                        <td class="text-center">
                                            <a class="btn btn-success" href="/orders/list-products/{{ order.id }}">
                                                Poruči <i class="fa fa-shopping-basket"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    {% endfor %}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>