<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tabOverview">{{ t('Pregled poruđžbina') }}</a></li>
                    {% if superAdmin == 1 %}
                    {% if order.getOrderTotals()|length %}
                    <li class=""><a data-toggle="tab" href="#tabTotal">{{ t('Ukupno po veličini') }}</a></li>
                    {% endif %}
                    {% endif %}
                </ul>
                <div class="tab-content">
                    <div id="tabOverview" class="tab-pane active">
                        {{ partial('orders/partials/orders') }}
                    </div>
                    {% if superAdmin == 1 %}
                    {% if order.getOrderTotals()|length %}
                    <div id="tabTotal" class="tab-pane">
                        {{ partial('orders/partials/order-total') }}
                    </div>
                    {% endif %}
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</div>
{{ partial('orders/partials/scripts') }}
