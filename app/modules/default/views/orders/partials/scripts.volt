<script type="text/javascript">

    $(document).ready(function() {

        var table2 = $("#orderTotals").DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'

            ],
            "processing": true,
            "serverSide": true,
            "searching": true,
            "ajax": {
                "url": "/order-totals/get-list-data/{{ order.id }}",
                "data":  function ( d ) {
                    return $.extend({}, d, {
                        "oldCollectionId": $('#oldCollectionId').val(),
                        "genderClassificationId" : $("#genderClassificationId").val(),
                        "groupModelId" : $("#groupModelId").val()
                    });
                }
            },
            "fnDrawCallback": function () {
                $(".jeditable").editable("/order-totals/update-total", {
                    "callback": function( sValue, y ) {
                        var obj = $.parseJSON(sValue);
                        if (obj.error) {
                            renderMessages(obj.messages, "danger");
                        } else {
                            renderMessages(obj.messages, "success");
                            table2.draw();
                        }
                    },
                    data : function(string) {
                        return $.trim(string.replace(/<\/?[^>]+>/gi, ''));
                    },
                    "submitdata": function ( value, settings ) {
                        return {
                            "column": table2.row($(this).closest('tr')).data()[0],
                        };
                    },
                    "height": "25px",
                    "weight": "20%",
                    cancel    : ' <i style="cursor:pointer" class="fa fa-remove text-danger bigger-140"></i>',
                    submit    : '<i style="margin: 0 5px; cursor:pointer" class="fa fa-check text-success bigger-140"></i>',
                });
            },
            "columns": [
                {className: null},
                {className: "col-sm-4 text-center"},
                {className: "col-sm-4 text-center"},
                {className: "col-sm-2 text-center"},
                {className: "col-sm-2 text-center"},
                {className: "col-sm-2 text-center jeditable"},
                {className: "col-sm-2 text-center"}
            ],
            "columnDefs": [
                {
                    "targets": 0,
                    "visible": false
                },
                {
                    "render": function ( data, type, row ) {
                        return row[5] + ' <a href="#"><i class="fa fa-pencil text-success"></i></a> ';
                    },
                    "targets": 5
                },
                {
                    "render": function ( data, type, row ) {
                        return row[6].split("_").pop();
                    },
                    "targets": 6
                }
            ],
            "rowsGroup": [// Always the array (!) of the column-selectors in specified order to which rows groupping is applied
                6
            ],
        });

        var table = $("#example").DataTable( {
            "processing": true,
            "serverSide": true,
            "searching": false,
            "pageLength": 50,
            "ajax": {
                "url": "/orders/get-list-data/{{ order.id }}",
                "data":  function ( d ) {
                    return $.extend({}, d, {
                        "classificationCode": $('#classificationId').val(),
                        "clientCode" : $("#clientId").val()
                    });
                }
            }
            ,
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            let tds = $("*[data-footer='true']", row);
            $( api.column(0).footer() ).html('Ukupno');

            $.each(tds, function($index, $elem){
                if ($elem.hasAttribute("data-column") && $index != 0) {
                    let columnIndexToSearch = $($elem).attr("data-column");

                    let pageTotal = api
                        .column(columnIndexToSearch, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );


                    $( api.column($index).footer() ).html(
                        pageTotal.toFixed(1)
                    );

                }
            });
        },
            "columns": [
                { className: "col-xs-4" },
                { className: "col-xs-2 text-center" },
                { className: "text-center" },
                { className: "text-center" },
                { className: "text-center" },
                { className: "text-center" },
                { className: "text-center" },
                { className: "text-center" },
                { className: "text-center" }
            ],
            "columnDefs": [
                {
                    "render": function ( data, type, row ) {
                        return '<a href="' + row[8] + '" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Izmena"><i class="fa fa-search"></i></button></a> ';
                    },
                    "targets": 8
                }
            ]
        });

        $("#classificationId, #clientId").change( function() {
            table.draw();
        });

        $(document).on("change", "#oldCollectionId, #genderClassificationId, #groupModelId", function(){
            table2.draw();
        });
    });
</script>