<div class="panel-body">
    <div class="row">
        <div class="col-xs-12">
            <div class="space-30"></div>
            <div class="form-horizontal">
                <div class="col-xs-4">
                    <div class="form-group">
                        {% if form.has('oldCollectionId') %}
                            {% set oldCollectionId = form.get('oldCollectionId') %}
                            {{ oldCollectionId }}
                        {% endif %}
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <div class="col-xs-12">
                            {% if form.has('genderClassificationId') %}
                                {% set genderClassificationId = form.get('genderClassificationId') %}
                                {{ genderClassificationId }}
                            {% endif %}
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <div class="">
                            {% if form.has('groupModelId') %}
                                {% set groupModelId = form.get('groupModelId') %}
                                {{ groupModelId }}
                            {% endif %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table id="orderTotals" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th class=""></th>
                    <th class="col-sm-4 m-b-xs text-center">{{ t('Naziv') }}</th>
                    <th class="col-sm-4 m-b-xs text-center">{{ t('Proizvod') }}</th>
                    <th class="col-sm-2 m-b-xs text-center">{{ t('Veličina') }}</th>
                    <th class="col-sm-2 m-b-xs">{{ t('Ukupno') }}</th>
                    <th class="col-sm-2 m-b-xs text-center">{{ t('Ukupno Zaokruženo') }}</th>
                    <th class="col-sm-2 m-b-xs text-center">{{ t('Ukupno po Proizvodu') }}</th>
                </thead>
            </table>
        </div>
    </div>
</div>