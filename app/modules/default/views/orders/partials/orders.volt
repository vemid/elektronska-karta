<div class="panel-body">
    <div class="row">
        <div class="col-xs-12">
            <div class="text-center">
                <div class="form-group">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-4">

                    </div>
                    <div class="col-xs-4 text-right">
                        {% if superAdmin == 1 %}
                        <a href="#" class="btn btn-success" onclick="getForm('/orders/download-form/{{ order.id }}', '/orders/prepare-download/{{ order.id }}', event)">
                            <i class="fa fa-print"></i> {{ t('Klijenti') }}
                        </a>
                        <span style="margin-right: 5px"></span>
                        <a href="#" class="btn btn-warning" onclick="getForm('/orders/download-shops-form/{{ order.id }}', '/orders/prepare-download-shops/{{ order.id }}', event)">
                            <i class="fa fa-print"></i> {{ t('Export inicijlnog') }}
                        </a>
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>
        <div class="space-25"></div>
    </div>
    <div class="row">
        <div class="col-sm-4 m-b-xs">
            <?php echo $this->tag->select(
            [
            "classificationId",
            $classifications,
            "using" => [
            "code",
            "name",
            ],
            "useEmpty" => true,
            "emptyText"  => "-- Izaberite --",
            "class" => "form-control search"
            ]
            ); ?>
        </div>
        <div class="col-sm-1 m-b-xs"></div>
        <div class="col-sm-2 m-b-xs"></div>
        <div class="col-sm-2 m-b-xs no-margins no-padding"></div>
        <div class="col-sm-3 m-b-xs">
            {% if not client or currentUser.isSuperAdmin() %}
                <?php echo $this->tag->select(
                [
                "clientId",
                $options,
                "useEmpty" => true,
                "emptyText"  => "-- Izaberite --",
                "class" => "form-control search"
                ]
                ); ?>
            {% else %}
                <input type="hidden" id="clientId" name="clientId" value="{{ client.code }}"/>
            {% endif %}
        </div>
    </div>
    <div class="table-responsive">
        <table id="example" class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th class="col-xs-4 text-center">{{ t('Kolekcija') }}</th>
                <th class="col-xs-2">{{ t('Kupac') }}</th>
                <th class="text-center">{{ t('Ukupno A') }}</th>
                <th class="text-center">{{ t('Ukupno B') }}</th>
                <th class="text-center">{{ t('Vrednost A') }}</th>
                <th class="text-center">{{ t('Vrednost B') }}</th>
                <th class="text-center">{{ t('Ukupno Kol') }}</th>
                <th class="text-center">{{ t('Ukupno Vred') }}</th>
                <th class="text-center col-sm-1 m-b-xs">{{ t('Detaljnije') }}</th>
            </thead>
            <tfoot>
            <tr>
                <th data-footer="true"></th>
                <th data-footer="true"></th>
                <th data-footer="true" data-column="2"></th>
                <th data-footer="true" data-column="3"></th>
                <th data-footer="true" data-column="4"></th>
                <th data-footer="true" data-column="5"></th>
                <th data-footer="true" data-column="6"></th>
                <th data-footer="true" data-column="7"></th>
                <th data-column="1"></th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>