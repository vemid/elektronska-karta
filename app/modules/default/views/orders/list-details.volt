<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="text-center">{{ t('Detalji Porudžbina') }}</h3>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h5 class="pull-left alert-danger btn">MP/VP : {{ client }}</h5>
                                    </div>
                                    <div class="col-xs-6"></div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h5 class="pull-left alert-danger btn">Kolekcija: {{ classification.name }}</h5>
                                    </div>
                                </div>
                                <div class="col-xs-6"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="space-25"></div>
                <div class="row">
                    <div class="col-sm-2 m-b-xs"></div>
                    <div class="col-sm-2 m-b-xs"></div>
                    <div class="col-sm-2 m-b-xs"></div>
                    <div class="col-sm-2 m-b-xs"></div>
                    <div class="col-sm-2 m-b-xs no-margins no-padding"></div>
                    <div class="col-sm-2 m-b-xs no-margins no-padding"></div>
                </div>
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="col-sm-2 m-b-xs">{{ t('Šifra') }}</th>
                            <th class="col-sm-2 m-b-xs">{{ t('Naziv') }}</th>
                            <th class="col-sm-1 m-b-xs">{{ t('Cena') }}</th>
                            <th class="col-sm-2 m-b-xs">{{ t('Kolekcija') }}</th>
                            <th class="col-sm-2 m-b-xs">{{ t('Količina A') }}</th>
                            <th class="col-sm-2 m-b-xs">{{ t('Količin B') }}</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var table = $("#example").DataTable( {
            "processing": true,
            "serverSide": true,
            "searching": false,
            "ajax": {
                "url": "/orders/get-list-details-data/{{ order.id }}",
                "data":  function ( d ) {
                    return $.extend({}, d, {
                        "classificationCode": '{{ classification.code }}',
                        "clientCode" : '{{ client.code }}'
                    });
                }
            },
            "columns": [
                null,
                null,
                null,
                null,
                null,
                null,
            ],
            "columnDefs": [
                {
//                    "render": function ( data, type, row ) {
//                        return '<a href="' + row[6] + '" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Izmena"><i class="fa fa-search"></i></button></a> ';
//                    },
//                    "targets": 6
                }
            ]
        });

        $("#classificationId, #clientId").change( function() {
            table.draw();
        });
    });
</script>