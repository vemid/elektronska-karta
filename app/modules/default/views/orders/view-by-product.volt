<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css"/>
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css"/>
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
    <style>
        .page-break {
            page-break-after: always;
        }

        .page-breaker {
            clear: both;
            display: block;
            border: 1px solid transparent;
            page-break-before: always;
        }
    </style>
</head>
<body class="pace-done">
<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h1 class="text-center">{{ t('Detalji Porudžbina') }}</h1>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="pull-right">
                        <a class="btn btn-success" onclick="getAjaxForm('orders/add-order-total-item/{{ product.id }}', event)"
                           title="Ubacivanje totala">
                            <i class="fa fa-tools"></i>{{ t(' Pregrupisanje totala') }}
                        </a>
                        <a href="/orders/view-by-product-print/{{ product.id }}" target="_blank" class="btn btn-success">
                            <i class="fa fa-print"></i>{{ t(' Stampa kolicina') }}
                        </a>
                        <a id="deleteInitial" style="margin-right: 5px" href="#" class="btn btn-warning" onclick="getForm('/order-items/delete-initial-form', '/order-items/delete-initial', event, '')">
                            <i class="fa fa-arrow-up"></i> {{ t('Ponisti porudzbinu') }}
                        </a>
                    </div>
                    <div class="col-xs-6 text-center">
                        <h3 class="text-center">Pregled robe po MP objektimaaa</h3>
                        <table class="table table-striped table-bordered table-hover totalMP" id="report">
                            <thead>
                            <tr>
                                <th></th>
                                {% for productSize in product.productSizes %}
                                    <th>{{ productSize.code.code }}</th>
                                {% endfor %}
                            </tr>
                            </thead>
                            <tbody>
                            {% for shop in shops %}
                                <tr>
                                    <td>{{ shop.name }}</td>
                                    {% for productSize in product.productSizes %}
                                        <td class="text-center" id="initialCreateQty">
                                            {% if dataShops[shop.id][productSize.codeId]['ID'] %}
                                                <a href="#" class="edit"
                                                   data-pk="{{ shop.id }}"
                                                    data-id="{{ dataShops[shop.id][productSize.codeId]['ID']}}">{{ dataShops[shop.id][productSize.codeId]['A'] }}</a>
                                            {% else  %}
                                                <a href="#" class="btn btn-sm btn-xs btn-success create"
                                                    data-entity-id="{{ shop.id}}"
                                                   data-type-id="shop"
                                                    data-product-size-id="{{ productSize.id }}">
                                                    <i class="fa fa-check small"></i>
                                                </a>
                                            {% endif %}
                                        </td>

                                        <!--<td>{{ dataShops[shop.id][productSize.codeId]['A'] ? dataShops[shop.id][productSize.codeId]['A'] : "" }}</td>-->
                                    {% endfor %}
                                </tr>
                            {% endfor %}
                            </tbody>
                            <tfoot>
                            <tr>
                                <th data-footer="true" class="text-center"></th>
                                {% set counter = 1 %}
                                {% for productSize in product.productSizes %}
                                    <th class="text-center" data-footer="true" data-column="{{ counter }}"></th>
                                    {% set counter = counter+1 %}
                                {% endfor %}
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="col-xs-6">
                        <h3 class="text-center">Informacije o proizvodu</h3>
                        <div class="col-xs-12 text-center">
                            <div class="space-15"></div>
                            <span class="text-info bigger-160 bold">
                                            {{ product.getCode() }} --
                                {{ product.getName() }}
                                        </span>
                            <div style="height:400px;">
                                <div id="product-image" style="max-height: 200px">
                                    <input type="image" src="{{ product.getImagePath() }}" width="50%" style="cursor: pointer; outline: none;"/>
                                    <input type="file" id="reUpload" style="display: none;" />
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div>
                                <div class="m-t-md">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Naziv</th>
                                            <th>Vrednost</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Pol</td>
                                            <td>
                                                <span class="bigger-120 bold">{{ hydrator.getGenderName() ? hydrator.getGenderName() : ''}}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Grupa Artikla</td>
                                            <td>
                                                        <span class="bigger-120 bold"><?php echo array_pop(explode(' ', $hydrator->
                                                            getProductGroup()->getName()))?></span></td>
                                        </tr>
                                        <tr>
                                            <td>1st Grupa Artikla</td>
                                            <td><span class="bigger-120 bold"><?php echo $hydrator->getSubProductGroup()->getName(); ?></span></td>
                                        </tr>
                                        <tr>
                                            <td>2nd Grupa Artikla</td>
                                            <td>
                                                <span class="bigger-120 bold"><?php echo substr(strstr($hydrator->getDubSubProductGroup()->getName()," "), 1)?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Boja Artikla</td>
                                            <td>
                                                <span class="bigger-120 bold">{{ hydrator.getColor() ? hydrator.getColor().getName() : '' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Brend</td>
                                            <td>
                                                <span class="bigger-120 bold">{{ hydrator.getBrand() ? hydrator.getBrand().getName() : '' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Uzrast</td>
                                            <td>
                                                <span class="bigger-120 bold">{{ hydrator.getAge() ? hydrator.getAge().getName() : '' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Sezona</td>
                                            <td>
                                                <span class="bigger-120 bold">{{ hydrator.getSeason() ? hydrator.getSeason().getName() : '' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Kolekcija 07</td>
                                            <td>
                                                <span class="bigger-120 bold">{{ hydrator.getOldCollection() ? hydrator.getOldCollection().getName() : '' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Velicine</td>
                                            <td>
                                                <span class="bigger-120 bold">{{ hydrator.getAllProductSizes() }}</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="text-center">
                                <div class="col-xs-2"></div>
                                <div class="col-xs-8">
                                    <table class="table table-striped table-bordered table-hover text-center">
                                        <thead>
                                        <tr>
                                            <th class="text-center">Poruceno</th>
                                            <th class="text-center">Raster</th>
                                            <th class="text-center">Iskrojeno</th>
                                            <th class="text-center">Isporuceno</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ orderTotal }}</td>
                                            <td>{{ orderTotal2 }}</td>
                                            <td>{{ totals }}</td>
                                            <td>{{ totalReceived }}</td>
                                        </tr>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                            <div class="space-30"></div>
                            {%  if productPrices %}
                            <div>
                                <div class="space-30">
                                    <h2 class="text-center">Informacije o ceni</h2>
                                </div>
                                <div class="table-responsive no-padding">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="col-xs-2 text-center">{{ t('') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('Nabavna') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('VP Marza') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('VP') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('MP') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {% for productPrice in productPrices %}
                                            <tr>
                                                <td class="text-center">{{ productPrice['name'] }}</td>
                                                <td class="text-center">{{ productPrice['purchasePrice'] }}</td>
                                                <td class="text-center">{{ number_format((productPrice['wholesalePrice']/productPrice['purchasePrice'])-1,2)*100 }}%</td>
                                                <td class="text-center">{{ productPrice['wholesalePrice'] }}</td>
                                                <td class="text-center">{{ productPrice['retailPrice'] }}</td>
                                            </tr>
                                        {% endfor %}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {% endif %}

                        </div>
                    </div>

                </div>
                <div class="page-breaker"></div>
                <div class="row text-center">
                    <div class="col-xs-12">
                        <h3 class="text-center">Pregled porudzbine VP kupci</h3>
                    </div>

                    <div id="load-into">
                        <div class="col-xs-6 text-center table-responsive" id="loaded-html">
                            <table class="table table-striped table-bordered table-hover total" id="report">
                                <thead>
                                <tr>
                                    <td></td>
                                    {% for productSize in product.productSizes %}
                                        <td>{{ productSize.code.code }}</td>
                                    {% endfor %}
                                </tr>
                                </thead>
                            <tbody>
                            {% for client in clients %}
                                <tr>
                                    <td width="100px">{{ client.name }}</td>
                                    {% for productSize in product.productSizes %}
                                        <td class="text-center" id="initialCreateQty">
                                            {% if dataClientsA[client.id][productSize.codeId]['ID'] %}
                                                <a href="#" class="edit"
                                                   data-pk="{{ client.id }}"
                                                   data-id="{{ dataClientsA[client.id][productSize.codeId]['ID']}}">
                                                    {{ dataClientsA[client.id][productSize.codeId]['QTY'] }}
                                                </a>
                                            {% else  %}
                                                <a href="#" class="btn btn-circle btn-xs btn-success create"
                                                   data-entity-id="{{ client.id}}"
                                                   data-type-id="client"
                                                   data-product-size-id="{{ productSize.id }}">
                                                    <i class="fa fa-check small"></i>
                                                </a>
                                            {% endif %}
                                        </td>


                                        {% continue %}
                                    {% endfor %}
                                </tr>
                                {% endfor %}
                            </tbody>
                                <tfoot>
                                <tr>
                                    <th data-footer="true" class="text-center"></th>
                                    {% set counter = 1 %}
                                    {% for productSize in product.productSizes %}
                                        <th class="text-center" data-footer="true" data-column="{{ counter }}"></th>
                                        {% set counter = counter+1 %}
                                    {% endfor %}
                                </tr>
                                </tfoot>
                        </table>
                    </div>
                    </div>
                    <div id="load-into">
                    <div class="col-xs-6 text-center" id="loaded-html">
                        <table class="table table-striped table-bordered table-hover total" id="report">
                            <thead>
                            <tr>
                                <th></th>
                                {% for productSize in product.productSizes %}
                                    <th class="text-center">{{ productSize.code.code }}</th>
                                {% endfor %}
                            </tr>
                            </thead>
                            <tbody>
                            {% for client in clients %}
                                <tr>
                                    <td>{{ client.name }}</td>
                                    {% for productSize in product.productSizes %}
                                        <td class="text-center" id="initialCreateQty">
                                            {% if dataClientsB[client.id][productSize.codeId]['ID'] %}
                                                <a href="#" class="edit"
                                                   data-pk="{{ client.id }}"
                                                   data-id="{{ dataClientsB[client.id][productSize.codeId]['ID']}}">{{ dataClientsB[client.id][productSize.codeId]['QTY'] }}</a>
                                            {% else  %}
                                                <a href="#" class="btn btn-circle btn-xs btn-success create"
                                                   data-entity-id="{{ client.id}}"
                                                   data-type-id="client"
                                                   data-product-size-id="{{ productSize.id }}">
                                                    <i class="fa fa-check small"></i>
                                                </a>
                                            {% endif %}
                                        </td>
                                    {% endfor %}
                                </tr>
                            {% endfor %}
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th data-footer="true" class="text-center"></th>
                                    {% set counter = 1 %}
                                    {% for productSize in product.productSizes %}
                                        <th class="text-center" data-footer="true" data-column="{{ counter }}"></th>
                                        {% set counter = counter+1 %}
                                    {% endfor %}
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css"/>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>

<script type="text/javascript">
    (function ($) {

        $(document).on("ready", function () {

            $('#initialCreateQty a.create').editable({
                type: 'text',
                url: '/order-items/create-initial',
                pk: 1,
                params: function (params) {
                    params.orderItemId = $(this).attr("data-id");
                    params.productSizeId = $(this).attr("data-product-size-id");
                    params.entityId = $(this).attr("data-entity-id");
                    params.type = $(this).attr("data-type-id");

                    return params;
                },
                title: 'Unesite količinu',
                ajaxOptions: {
                    dataType: 'json'
                },
                success: function (response, newValue) {
                    if (!response) {
                        return "Unknown error!";
                    }

                    if (response.error === false) {
                        renderMessages(response.messages, "success");
                        window.location.reload();

                    } else {
                        $.get(window.location.href, function (responseGet) {
                            var content = $(responseGet).find("#content");
                            var flashSession = $(responseGet).find("#flash-session");
                            $("#content").replaceWith(content);
                            $("#flash-session").replaceWith(flashSession);
                            $.each(response.messages, function (key, message) {
                                showPushMessage(message.message, "danger");
                            });
                        });
                    }
                }
            });

            $('#initialCreateQty a.edit').editable({
                type: 'text',
                url: '/order-items/edit-initial',
                params: function (params) {
                    params.orderItemId = $(this).attr("data-id");
                    return params;
                },
                title: 'Unesi količinu',
                ajaxOptions: {
                    dataType: 'json'
                },
                success: function (response, newValue) {
                    if (!response) {
                        return "Unknown error!";
                    }

                    if (response.error === false) {
                        renderMessages(response.messages, "success");
                    } else {
                        $.get(window.location.href, function (responseGet) {
                            var content = $(responseGet).find("#content");
                            var flashSession = $(responseGet).find("#flash-session");
                            $("#content").replaceWith(content);
                            $("#flash-session").replaceWith(flashSession);
                            $.each(response.messages, function (key, message) {
                                showPushMessage(message.message, "danger");
                            });
                        });
                    }
                }
            });

            $("#save-modal").click(function(){
                $("#hiddenInitialProductId").val("{{ product.id }}");
            });

            $.each($(".totalMP"), function (index, element) {
                console.log(index, element);
                $(element).DataTable({
                    paging:   false,
                    ordering: false,
                    info:     false,
                    fixedHeader: true,
                    searching:false,
                    "pageLength": 40,
                    // dom: '<"html5buttons"B>lTfgitp',
                    "language": {
                        "lengthMenu": t("Prikaz _MENU_  redova po strani"),
                        "zeroRecords": "nije pronađeno",
                        "info": "Prikaz stranice _PAGE_ od _PAGES_",
                        "search": "Traži",
                        "infoEmpty": "Nema redova",
                        "infoFiltered": "(filtrirano od _MAX_ ukupno redova)",
                        "paginate": {
                            "first": "Prva",
                            "last": "Poslednja",
                            "next": ">",
                            "previous": "<"
                        },
                    },
                    'bSort': false,
                    tableTools: {
                        "sSwfPath": "/plugin/copy_csv_xls_pdf.swf"
                    },
                    "footerCallback": function ( row, data, start, end, display ) {
                        var api = this.api(), data;

                        // converting to interger to find total
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };


                        let tds = $("*[data-footer='true']", row);
                        $( api.column(0).footer() ).html('Ukupno');

                        $.each(tds, function($index, $elem){
                            if ($elem.hasAttribute("data-column") && $index != 0) {
                                let columnIndexToSearch = $($elem).attr("data-column");

                                let pageTotal = api
                                    .column(columnIndexToSearch, { page: 'current'} )
                                    .data()
                                    .reduce( function (a, b) {
                                        b = $(b).text();

                                        if (!$.isNumeric(parseInt(b))) {
                                            b = 0;
                                        }

                                        return intVal(a) + intVal(b);
                                    }, 0 );


                                let total = api
                                    .column(columnIndexToSearch )
                                    .data()
                                    .reduce( function (a, b) {
                                        b = $(b).text();

                                        if (!$.isNumeric(parseInt(b))) {
                                            b = 0;
                                        }

                                        return intVal(a) + intVal(b);
                                    }, 0 );

                                $( api.column($index).footer() ).html(
                                    pageTotal +' ( od '+ total +')'
                                );

                            }
                        });
                    },
                });

            });

        });
    })($);
</script>
</body>
</html>
