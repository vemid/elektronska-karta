<style>
    .pageloader {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('{{ siteUrl }}/img/loader.gif') 50% 50% no-repeat rgb(249, 249, 249);
        opacity: .8;
    }
</style>
<div class="row">
    <div class="pageloader"></div>
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-2">
                            <button class="btn btn-success" value="Refresh Page" onClick="window.location.href=window.location.href">Refresh</button>
                        </div>
                        <div class="col-xs-10">
                            <h2>Izvestaj porucenosti kolekcije </h2>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div id="pivot"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let rows = JSON.parse('{{ rows }}');

    let dimensions = [
        {value: 'product', title: 'Proizvod'},
        {value: 'name', title: 'Sifra'},
        {value: 'gender', title: 'Pol'},
        {value: 'seasson', title: 'Sezona'},
        {value: 'size', title: 'Velicina'},
    ];

    let reduce = function(row, memo) {

        memo.qtyA = (memo.qtyA || 0) + parseInt(row.transaction.qtyA);
        memo.qtyB = (memo.qtyB || 0) + parseInt(row.transaction.qtyB);

        return memo
    };

    let calculations = [
        {
            title: 'Kolicina A', value: 'qtyA',
            template: function(val, row) {
                return ' ' + val
            }
        },
        {
            title: 'Kolicina B', value: 'qtyB',
            template: function(val, row) {
                return ' ' + val
            }
        }

    ];
    ReactPivot(document.getElementById("pivot"), {
        rows: rows,
        dimensions: dimensions,
        calculations: calculations,
        reduce: reduce,
        nPaginateRows : 100,
        activeDimensions: ['Sezona','Pol','Velicina']
    })

    $(window).load(function() {
        $(".pageloader").fadeOut("slow");
    });
</script>