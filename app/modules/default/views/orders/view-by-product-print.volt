<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css"/>
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css"/>
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
    <style>
        .page-break {
            page-break-after: always;
        }

        .page-breaker {
            clear: both;
            display: block;
            border: 1px solid transparent;
            page-break-before: always;
        }
    </style>
</head>
<body class="pace-done">
<div>
    <div class="row">
        <div style="font-size: 8px">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-6 text-center">
                        <h3 class="text-center">Pregled robe po MP objektima</h3>
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <td></td>
                                {% for productSize in product.productSizes %}
                                    <td>{{ productSize.code.code }}</td>
                                {% endfor %}
                            </tr>
                            {% for shop in shops %}
                                <tr>
                                    <td>{{ shop.name }}</td>
                                    {% for productSize in product.productSizes %}
                                        <td>{{ dataShops[shop.id][productSize.codeId]['A'] ? dataShops[shop.id][productSize.codeId]['A'] : "" }}</td>
                                    {% endfor %}
                                </tr>
                            {% endfor %}
                        </table>
                    </div>

                    <div class="col-xs-6">
                        <h3 class="text-center">Informacije o proizvodu</h3>
                        <div class="col-xs-12 text-center">
                            <div class="space-15"></div>
                            <span class="text-info bigger-160 bold">
                                            {{ product.getCode() }} --
                                {{ product.getName() }}
                                        </span>
                            <div style="height:300px;">
                                <div id="product-image" style="max-height: 200px">
                                    <input type="image" src="{{ product.getImagePrintPath() }}" width="50%"
                                           style="cursor: pointer; outline: none;"/>
                                    <input type="file" id="reUpload" style="display: none;"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div>
                                <div class="m-t-md">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Naziv</th>
                                            <th>Vrednost</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Pol</td>
                                            <td>
                                                <span class="bigger-120 bold">{{ hydrator.getGenderName() ? hydrator.getGenderName() : '' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Grupa Artikla</td>
                                            <td>
                                                        <span class="bigger-120 bold"><?php echo array_pop(explode(' ', $hydrator->
                                                            getProductGroup()->getName()))?></span></td>
                                        </tr>
                                        <tr>
                                            <td>1st Grupa Artikla</td>
                                            <td><span class="bigger-120 bold"><?php echo $hydrator->
                                                    getSubProductGroup()->getName(); ?></span></td>
                                        </tr>
                                        <tr>
                                            <td>2nd Grupa Artikla</td>
                                            <td>
                                        <span class="bigger-120 bold"><?php echo substr(strstr($hydrator->
                                            getDubSubProductGroup()->getName()," "), 1)?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Boja Artikla</td>
                                            <td>
                                                <span class="bigger-120 bold">{{ hydrator.getColor() ? hydrator.getColor().getName() : '' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Brend</td>
                                            <td>
                                                <span class="bigger-120 bold">{{ hydrator.getBrand() ? hydrator.getBrand().getName() : '' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Uzrast</td>
                                            <td>
                                                <span class="bigger-120 bold">{{ hydrator.getAge() ? hydrator.getAge().getName() : '' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Sezona</td>
                                            <td>
                                                <span class="bigger-120 bold">{{ hydrator.getSeason() ? hydrator.getSeason().getName() : '' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Kolekcija 07</td>
                                            <td>
                                                <span class="bigger-120 bold">{{ hydrator.getOldCollection() ? hydrator.getOldCollection().getName() : '' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Velicine</td>
                                            <td>
                                                <span class="bigger-120 bold">{{ hydrator.getAllProductSizes() }}</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="text-center">
                                <div class="col-xs-2"></div>
                                <div class="col-xs-8">
                                    <table class="table table-striped table-bordered table-hover text-center">
                                        <thead>
                                        <tr>
                                            <th class="text-center">Poruceno</th>
                                            <th class="text-center">Iskrojeno</th>
                                            <th class="text-center">Isporuceno</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ orderTotal }}</td>
                                            <td>{{ totals }}</td>
                                            <td>{{ totalReceived }}</td>
                                        </tr>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                            <div class="space-30"></div>
                            {% if productPrices %}
                                <div>
                                    <div class="space-30">
                                        <h2 class="text-center">Informacije o ceni</h2>
                                    </div>
                                    <div class="table-responsive no-padding">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="col-xs-2 text-center">{{ t('') }}</th>
                                                <th class="col-xs-2 text-center">{{ t('Nabavna') }}</th>
                                                <th class="col-xs-2 text-center">{{ t('VP Marza') }}</th>
                                                <th class="col-xs-2 text-center">{{ t('VP') }}</th>
                                                <th class="col-xs-2 text-center">{{ t('MP') }}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {% for productPrice in productPrices %}
                                                <tr>
                                                    <td class="text-center">{{ productPrice['name'] }}</td>
                                                    <td class="text-center">{{ productPrice['purchasePrice'] }}</td>
                                                    <td class="text-center">{{ number_format((productPrice['wholesalePrice']/productPrice['purchasePrice'])-1,2)*100 }}
                                                        %
                                                    </td>
                                                    <td class="text-center">{{ productPrice['wholesalePrice'] }}</td>
                                                    <td class="text-center">{{ productPrice['retailPrice'] }}</td>
                                                </tr>
                                            {% endfor %}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            {% endif %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-break"></div>
    <div class="row" style="font-size: 8px">
        <div class="col-xs-12">
            <div class="row text-center">
                <div class="col-xs-12">
                    <h3 class="text-center">Pregled porudzbine VP kupci</h3>
                </div>

                <div class="col-xs-6 text-center">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <td></td>
                            {% for productSize in product.productSizes %}
                                <td>{{ productSize.code.code }}</td>
                            {% endfor %}
                        </tr>
                        {% for client in clients %}
                            <tr>
                                <td>{{ client.name }}</td>
                                {% for productSize in product.productSizes %}
                                    <td>{{ dataClientsA[client.id][productSize.codeId] ? dataClientsA[client.id][productSize.codeId] : "" }}</td>
                                {% endfor %}
                            </tr>
                        {% endfor %}
                    </table>
                </div>

                <div class="col-xs-6 text-center">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <td></td>
                            {% for productSize in product.productSizes %}
                                <td>{{ productSize.code.code }}</td>
                            {% endfor %}
                        </tr>
                        {% for client in clients %}
                            <tr>
                                <td>{{ client.name }}</td>
                                {% for productSize in product.productSizes %}
                                    <td>{{ dataClientsB[client.id][productSize.codeId] ? dataClientsB[client.id][productSize.codeId] : "" }}</td>
                                {% endfor %}
                            </tr>
                        {% endfor %}
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css"/>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>