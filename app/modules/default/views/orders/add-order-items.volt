<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="text-center">{{ t('Porudžbine') }}</h3>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            {% if not notFoundCodes|length and not notFoundSizes|length and not notFoundShopCodes|length %}
                            <form action="#" class="dropzone" data-id="{{ order.id }}" id="dropzoneFormOrder" method="post" enctype="multipart/form-data">
                                <input type="hidden" id="orderId" name="orderId" value="{{ order.id }}"/>
                                <div class="fallback">
                                    <input name="file" type="file" />
                                </div>
                            </form>
                            <div class="space-20"></div>
                            <div class="col-xs-12">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-4 text-center">
                                    <a href="#" id="start" class="btn btn-primary btn-rounded btn-block">
                                        {{ t('Snimi') }} <i class="fa fa-image"></i>
                                    </a>
                                </div>
                                <div class="col-xs-4"></div>
                            </div>
                            {% else %}
                                <a href="#" onclick="sendPostAjaxCall('/orders/clear-errors/{{ order.id }}', {})" class="btn btn-warning">
                                    Odbaci fajl <i class="fa fa-remove"></i>
                                </a>
                            {% endif %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {% if notFoundCodes|length or notFoundSizes|length or notFoundShopCodes|length %}
        {% if notFoundCodes|length %}
            <div class="col-xs-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3 class="text-center">{{ t('Proizvod') }}</h3>
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-xs-12">
                                <ul class="todo-list m-t ui-sortable">
                                    {% for code in notFoundCodes %}
                                        <li>
                                            <span class="m-l-xs">{{ code }}</span>
                                        </li>
                                    {% endfor %}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {% endif %}
        {% if notFoundShopCodes|length %}
            <div class="col-xs-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3 class="text-center">{{ t('Radnje') }}</h3>
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-xs-12">
                                <ul class="todo-list m-t ui-sortable">
                                    {% for code in notFoundShopCodes %}
                                        <li>
                                            <span class="m-l-xs">Šifra objekta: {{ code }}</span>
                                        </li>
                                    {% endfor %}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {% endif %}
        {% if notFoundSizes|length %}
            <div class="col-xs-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3 class="text-center">{{ t('Veličine') }}</h3>
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-xs-12">
                                <ul class="todo-list m-t ui-sortable">
                                    {% for code, sizes in notFoundSizes %}
                                        <li>
                                            <span class="m-l-xs">{{ implode(',', sizes) }} ({{ code }})</span>
                                        </li>
                                    {% endfor %}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {% endif %}
    {% elseif hasFileUploaded %}
        <div class="col-xs-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3 class="text-center">{{ t('Veličine') }}</h3>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="#" onclick="sendPostAjaxCall('/order-items/import-shops-order/{{ order.id }}', {})" class="btn btn-primary btn-rounded btn-block">
                                {{ t('Snimi') }} <i class="fa fa-cloud-upload"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {% endif %}
</div>

<script type="text/javascript">
(function ($) {
    $(document).on("ready", function () {

        function updateDiv()
        {
            $( "#item-errors" ).load(window.location.href + " #item-errors" );
        }
    });
})($);
</script>