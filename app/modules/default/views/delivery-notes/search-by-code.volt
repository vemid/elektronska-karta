<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                {% if receiverNotes %}
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" id="data">
                        <thead>
                        <tr>
                            <th>{{ t('Šifra Robe') }}</th>
                            <th>{{ t('Nasziv Robe') }}</th>
                            <th>{{ t('Količina') }}</th>
                            <th>{{ t('Rezervisana kolicina') }}</th>
                            <th class="text-center">{{ t('Akcije') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for receiverNote in receiverNotes %}
                        <tr>
                            <td><?php echo $receiverNote['sif_rob']; ?></td>
                            <td><?php echo $receiverNote['naz_rob'];?></td>
                            <td>{{ receiverNote['kolic'] }}</td>
                            <td>{{ deliveryNoteItemRepository.getStatus(receiverNote['sif_rob']) }}</td>
                            <td class="text-center">
                                <?php $sizes = implode(',',$receiverNote['sif_ent_rob']); ?>
                                <a href="/delivery-note-items/overview/{{ deliveryNote.id }}/<?php echo urlencode($receiverNote['sif_rob']); ?>/{{ sizes }}/{{ receiverNote['kolic'] }}/{{ receiverNote['cen_zal'] }}" class="btn btn-info btn-xs btn-circle">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
                {% endif %}
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready( function () {
        $('#data').DataTable({
            "pageLength": 100,
            "ordering" : false
        });
    })
</script>