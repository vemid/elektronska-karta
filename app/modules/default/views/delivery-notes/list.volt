<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="text-center">{{ t('Otremnice') }}</h3>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj</h3>
                            <a href="#" onclick="getCreateForm('delivery-notes', event)" title="Kreiranje Otrpemnice-a" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables">
                        <thead>
                        <tr>
                            <th class="col-sm-3 m-b-xs">{{ t('Oznaka') }}</th>
                            <th class="col-sm-3 m-b-xs">{{ t('Dokument') }}</th>
                            <th class="col-sm-4 m-b-xs text-center">{{ t('Prijemnica') }}</th>
                            <th class="text-center col-sm-2 m-b-xs text-center">{{ t('Akcije') }}</th>
                        </thead>
                        <tbody>
                        {% for deliveryNote in deliveryNotes %}
                        <tr>
                            <td>ELK/{{ deliveryNote.id }}</td>
                            <td>{{ deliveryNote.documentName }}</td>
                            <td class="text-center">{{ deliveryNote.misDeliveryNote }}</td>
                            <td class="text-center">
                                {% if deliveryNote.type == 'PREPARED' %}
                                    <a class="btn btn-success btn-circle" onclick="getAjaxForm('delivery-notes/finish/{{ deliveryNote.id }}', event)">
                                        <i class="fa fa-check"></i>
                                    </a>
                                {% endif %}
                                <?php $note = urlencode($deliveryNote->getMisDeliveryNote()); ?>
                                <a class="btn btn-info btn-circle" href="/delivery-notes/search-by-code?id={{ deliveryNote.id }}&code={{ note }}">
                                    <i class="fa fa-search"></i>
                                </a>
                                <a class="btn btn-danger btn-circle" href="#" onclick="getDeleteForm('delivery-notes', {{ deliveryNote.id }}, event)">
                                    <i class="fa fa-remove"></i>
                                </a>
                            </td>
                        </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>