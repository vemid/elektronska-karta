<div class="ibox">
    <div class="ibox-title">
        <h3>{{ t('Materijalli') }}</h3>
    </div>
    <div class="ibox-content">
        {{ form('material-factories/create', 'class': 'form-horizontal', 'id' : 'materialFactory', 'enctype' : 'multipart/form-data') }}
        {% set delimiter = form.getElements()|length / 2 + 2 %}
        {% set counter = 0 %}
        <div class="row">
            <div class="col-xs-6">
                {% for element in form.getElements() %}
                {% set counter = counter + 1 %}
                {% if element.getName() == 'materialPicture' or element.getName() == 'invoiceScan' %}
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{{ t(element.getLabel()) }}</label>
                        <div class="col-sm-9">
                            <div class="input-group input-file" name="{{ element.getName() }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-choose"
                                                    type="button">{{ t('Izaberite') }}</button>
                                        </span>
                                <input type="text" class="form-control"
                                       placeholder='{{ t('Izaberite sliku') }}'/>
                            </div>
                        </div>
                    </div>
                {% else %}
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label no-padding-right">
                            {{ element.getLabel() }}
                        </label>
                        <div class="col-sm-9{{ element.getNAme() == 'qty' ? ' input-group spinner' : '' }}">
                            {{ element }}
                            {% if element.getNAme() == 'qty' %}
                                <div class="input-group-btn-vertical">
                                    <button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i></button>
                                    <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                                </div>
                            {% endif %}
                            <div class="help-block m-b-none"></div>
                        </div>
                    </div>
                {% endif %}
                {% if counter > delimiter %}
                {% set counter = 0 %}
            </div>
            <div class="col-xs-6">
                {% endif %}
                {% endfor %}
            </div>
            <div class="col-xs-12">
            <div class="col-xs-2 pull-left">
                <label class="pull-right">Odrzavanje</label>
            </div>
            <div class="col-xs-10" style="margin-bottom: 20px !important;">
                {% for washItem in washItems %}
                    <div class="col-xs-1 text-center" >
                        <a class="product-tooltip text-danger" data-toggle="tooltip"  title="{{ washItem.description }}">
                            <img src="{{ washItem.getImagePath() }}" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="washed[{{washItem.id}}]" value="{{ washItem.id }}" {% if washItem.printSign in washData %}checked{% endif %}>
                        </a>
                    </div>
                {% endfor %}
            </div>
            </div>
        </div>
        <div class="space-25"></div>
        <div class="form-group">
            <div class="col-sm-12">
                <button class="btn btn-primary pull-right btn-success" type="submit">{{ t('Sačuvaj') }}</button>
            </div>
        </div>
        {{ end_form() }}
        <div class="space-25"></div>
        <div class="row"></div>
    </div>
</div>
