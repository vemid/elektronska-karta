<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">

            <div class="col-lg-12">

                <div class="ibox-content">

                    <div class="row">
                        <div class="col-md-5">
                            <div id="product-image">
                                <input type="image" src="{{ materialFactory.getImagePath() }}" width="100%"
                                       style="cursor: pointer; outline: none;"/>
                                <input type="file" id="reUpload" style="display: none;"/>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <h2 class="font-bold m-b-xs">
                                {{ t('CardNumber') }} : <span class="text-info bigger-160 bold">
                                            {{ materialFactory.cardNumber }}
                                        </span>
                            </h2>

                            <div class="space-15"></div>
                            <div class="m-t-md">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Vrednost</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{ t('Season') }}</td>
                                        <td>
                                            <span class="bigger-120 bold">{{ materialFactory.getSeasonClassification() }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Naziv</td>
                                        <td>
                                            <span class="bigger-120 bold">{{ materialFactory.getSubMaterialClassification() ? materialFactory.getSubMaterialClassification().getParentClassification().getName() : '' }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ t('Pod Naziv') }}</td>
                                        <td>
                                            <span class="bigger-120 bold">{{ materialFactory.getSubMaterialClassification() ? materialFactory.getSubMaterialClassification().getName() : '' }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ t('Boja') }}</td>
                                        <td>
                                            <span class="bigger-120 bold">{{ materialFactory.color }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ t('Dobavljac') }}</td>
                                        <td>
                                            <span class="bigger-120 bold">{{ materialFactory.getSupplierName() ? materialFactory.getSupplierName().getName() : '' }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ t('Sifra Materijala') }}</td>
                                        <td>
                                            <span class="bigger-120 bold">{{ materialFactory.fabricCode }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ t('Tezina') }}</td>
                                        <td>
                                            <span class="bigger-120 bold">{{ materialFactory.weight }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ t('Sirina Bale') }}</td>
                                        <td>
                                            <span class="bigger-120 bold">{{ materialFactory.height }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ t('Stanje') }}</td>
                                        <td>
                                            <span class="bigger-120 bold">{{ materialFactoryTotalQty }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ t('Cena') }}</td>
                                        <td>
                                            <span class="bigger-120 bold">{{ materialFactory.costs }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ t('Invoice') }}</td>
                                        <td>
                                            <span class="bigger-120 bold"
                                                  style="padding-right:10px">{{ materialFactory.invoice ? materialFactory.invoice : '' }}</span>
                                            {% if materialFactory.invoiceScan %}
                                                <a href="/material-factories/download/{{ materialFactory.id }}"
                                                   target="_blank" class="btn btn-info">
                                                    <i class="fa fa-print danger"></i>
                                                    {{ t('Stampa fakture') }}
                                                </a>
                                            {% endif %}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Odrzavanje</td>
                                        <td>
                                            <div id="test-label">
                                                {{ careLabel.getCareLabelsImage(materialFactory) }}
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                            <hr>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center">
                                <h3>Pregled Kartice Materijala</h3>
                            </div>
                            <div class="text-center">
                                <div class="space-30"></div>
                                <a href="#"
                                   onclick="getForm('/material-factories-quantities/get-create-form/{{ materialFactory.id }}', '/material-factories-quantities/create/{{ materialFactory.id }}', event,'')"
                                   class="btn btn-success">
                                    <i class="fa fa-plus-square"></i>
                                    {{ t('Ulaz / Izlaz Materijala') }}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="space-30"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">{{ t('Season') }}</th>
                                <th class="text-center">{{ t('Nalog') }}</th>
                                <th class="text-center">{{ t('Datum') }}</th>
                                <th class="text-center">{{ t('Ulaz') }}</th>
                                <th class="text-center">{{ t('Izlaz') }}</th>
                                <th class="text-center">{{ t('Napomena') }}</th>
                                <th class="text-center">{{ t('Akcija') }}</th>

                            </tr>
                            </thead>
                            <tbody>
                            {% for data in materialFactory.getMaterialFactoryQuantities() %}
                                <tr>
                                    <td class="text-center">{{ data.getSeasonClassification() }}</td>
                                    <td class="text-center">{{ data.warrant }}</td>
                                    <td class="text-center">{{ data.dateTime }}</td>
                                    <td class="text-center">{{ data.getType() == 'INCOME' ? data.getAmount() : '' }}</td>
                                    <td class="text-center">{{ data.getType() == 'OUTCOME' ? data.getAmount() : '' }}</td>
                                    <td class="text-center">{{ data.note }}</td>
                                    <td class="text-center">
                                        <a href="#"
                                           onclick="getUpdateForm('material-factories-quantities', {{ data.getId() }}, event)"
                                           class="bigger-140 text-danger">
                                            <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </a>
                                        <a href="#"
                                           onclick="getDeleteForm('material-factories-quantities', {{ data.getId() }}, event)"
                                           class="bigger-140 text-danger">
                                            <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </a></td>
                                </tr>
                            {% endfor %}
                            <tr>
                                <td></td>
                                <td></td>
                                <td class="text-center">Total</td>
                                <td class="text-center">{{ materialFactoryQuantityIncome }}</td>
                                <td class="text-center">{{ materialFactoryQuantityOutcome }}</td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>