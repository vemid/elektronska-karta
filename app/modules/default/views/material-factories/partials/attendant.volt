<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-25"></div>
                <div class="table-responsive">
                    <table id="attendant" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">{{ t('Kartica') }}</th>
                            <th class="text-center">{{ t('Name') }}</th>
                            <th class="text-center">{{ t('Pod Naziv') }}</th>
                            <th class="text-center">{{ t('Boja') }}</th>
                            <th class="text-center">{{ t('Gr.') }}</th>
                            <th class="text-center">{{ t('Sirina') }}</th>
                            <th class="text-center">{{ t('Dobavljac') }}</th>
                            <th class="text-center">{{ t('Mat.') }}</th>
                            <th class="text-center">{{ t('Stanje') }}</th>
                            <th class="text-center">{{ t('Cena') }}</th>
                            <th class="text-center">{{ t('Vrednost') }}</th>
                            <th style="width: 20% !important;" class="text-center">{{ t('Akcija') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th data-footer="true"></th>
                            <th data-footer="true"></th>
                            <th data-footer="true"></th>
                            <th data-footer="true"></th>
                            <th data-footer="true"></th>
                            <th data-footer="true"></th>
                            <th data-footer="true"></th>
                            <th data-footer="true"></th>
                            <th data-footer="true" data-column="8"></th>
                            <th data-footer="true"></th>
                            <th data-footer="true" data-column="10"></th>
                            <th data-column="1"></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $("#attendant").DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 150,
            "headerSort":false,
            "ajax": {
                "url": "/material-factories/get-list-data",
                "data": function (d) {
                    return $.extend({}, d, {
                        "typeMaterial": "02"
                    });
                }
            },
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function (i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                let tds = $("*[data-footer='true']", row);
                $(api.column(0).footer()).html('Ukupno');

                $.each(tds, function ($index, $elem) {
                    if ($elem.hasAttribute("data-column") && $index != 0) {
                        let columnIndexToSearch = $($elem).attr("data-column");

                        let pageTotal = api
                            .column(columnIndexToSearch, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        $(api.column($index).footer()).html(
                            pageTotal.toFixed(1)
                        );

                    }
                });
            },
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return '<a href="/material-factories/edit-form/' + row[11] + '" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Izmena"><i class="fa fa-edit"></i></button></a> ' +
                            '<a href="#" onclick="getDeleteForm(\'material-factories\', ' + row[11] + ', event)" class="bigger-140 text-danger"><button class="btn btn-danger btn-circle" type="button" title="Brisanje"><i class="fa fa-times"></i></button></a> ' +
                            '<a href="/material-factories/overview/' + row[11] + '" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search"></i></button></a> ' +
                            '<a href="#" onclick="getForm(\'/material-factories-quantities/get-create-form/' + row[11] + '\', \'/material-factories-quantities/create/' + row[11] + '\', event)" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Pregled"><i class="fa fa-minus-square"></i></button></a>';
                    },
                    "targets": 11
                },
                {
                    "headerSort":false
                },
            ]
        });
    });
</script>
