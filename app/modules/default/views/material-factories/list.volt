<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <div class="col-xs-9 text-center">
                            <h2>{{ t('Kartice materijala') }}</h2>
                        </div>
                        <div class="col-xs-3 pull-right">
                            <a href="/material-factories/create-form" class="btn btn-success pull-right">
                                <i class="fa fa-plus-square"></i>
                                {{ t('Nova Kartica Materijala') }}
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-20 clear clearfix"></div>
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-table-1">{{ t('Osnovni Materijal') }}</a></li>
                        <li><a data-toggle="tab" href="#tab-table-2">{{ t('Pomocni Materijal') }}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-table-1" class="tab-pane active">
                            <div class="panel-body">
                                {{ partial('material-factories/partials/basic') }}
                            </div>
                        </div>
                        <div id="tab-table-2" class="tab-pane">
                            <div class="panel-body">
                                {{ partial('material-factories/partials/attendant') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
