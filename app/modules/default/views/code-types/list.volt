<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj</h3>
                            <a href="#" onclick="getCreateForm('code-types', event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" >
                        <thead>
                        <tr>
                            <th>{{ t('Code') }}</th>
                            <th>{{ t('Naziv') }}</th>
                            <th>{{ t('Glavni') }}</th>
                            <th>{{ t('Akcije') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for codeType in codeTypes %}
                            <tr>
                                <td>{{ codeType.code }}</td>
                                <td>{{ codeType.name }}</td>
                                <td>{{ codeType.isMain }}</td>
                                <td class="text-center" width="11.5%">
                                    <a href="/code-types/overview/{{ codeType.id }}" class="bigger-140 text-danger">
                                        <button class="btn btn-info btn-circle" type="button" title="Pregled kodova">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getUpdateForm('code-types', {{ codeType.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getDeleteForm('code-types', {{ codeType.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>