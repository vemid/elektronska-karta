<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="text-center">{{ t('Klasifikacije') }}</h3>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <div class="form-group">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-4">
                                    <?php echo $this->tag->select(
                                        [
                                            "classificationType",
                                            $codes,
                                            "using" => [
                                                "id",
                                                "name",
                                            ],
                                            "useEmpty" => true,
                                            "emptyText"  => "-- Izaberite --",
                                            "class" => "form-control"
                                        ]
                                    ); ?>
                                </div>
                                <div class="col-xs-4"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="space-25"></div>
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered table-hover" >
                        <thead>
                        <tr>
                            <th>{{ t('Ime') }}</th>
                            <th>{{ t('Tip Klasifokacije') }}</th>
                            <th>{{ t('Oznaka') }}</th>
                            <th>{{ t('Nad Oznaka') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var cls = $("#classificationType").val();
        var table = $("#example").DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/classifications/list",
                "data":  function ( d ) {
                    return $.extend({}, d, {
                        "classificationType": $('#classificationType').val()
                    });
                }
            }
        });

        $("#classificationType").change( function() {
            table.draw();
        } );
    });
</script>