<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj</h3>
                            <a href="#" onclick="getCustomCreateForm('codes', 'code-type-id', {{ codeType.id }}, event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" >
                        <thead>
                        <tr>
                            <th>{{ t('Ime') }}</th>
                            <th>{{ t('stagod') }}</th>
                            <th>{{ t('Šifra') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for code in codeType.codes %}
                            {% for attribute in code.attributes %}
                                <tr>
                                    <td>{{ attribute }}</td>
                                    <td>{{ code }}</td>
                                    <td>{{ code.code }}</td>
                                    <td class="text-center" width="11.5%">

                                    </td>
                                </tr>
                            {% endfor %}
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>