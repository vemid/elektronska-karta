<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj</h3>
                            <a href="#" onclick="getCreateForm('classification-old-collection-map', event)" class="bigger-200 btn btn-circle btn-lg" title="{{ t('Mapiranje Stare kolekcije') }}">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" >
                        <thead>
                        <tr>
                            <th>{{ t('Stara Kolekcija') }}</th>
                            <th>{{ t('Uzrast') }}</th>
                            <th>{{ t('Sezona') }}</th>
                            <th>{{ t('Kolekcija') }}</th>
                            <th>{{ t('Tip produkta') }}</th>
                            <th class="text-center">{{ t('Akcije') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for oldCollectionMap in oldCollectionMappings %}
                            <tr>
                                <td>{{ oldCollectionMap.getClassificationOldCode().name }}</td>
                                <td>{{ oldCollectionMap.getClassificationAge().name }}</td>
                                <td>{{ oldCollectionMap.getClassificationSeason().name }}</td>
                                <td>{{ oldCollectionMap.getClassificationCollection().name }}</td>
                                <td>{{ oldCollectionMap.getCode().name }}</td>
                                <td class="text-center" width="11.5%">
                                    <a href="#" onclick="getUpdateForm('classification-old-collection-map', {{ oldCollectionMap.id }}, event)" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Izmena"><i class="fa fa-edit"></i></button></a>
                                    <a href="#" onclick="getDeleteForm('classification-old-collection-map', {{ oldCollectionMap.id }}, event)" class="bigger-140 text-danger"><button class="btn btn-danger btn-circle" type="button" title="Brisanje"><i class="fa fa-times"></i></button></a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>