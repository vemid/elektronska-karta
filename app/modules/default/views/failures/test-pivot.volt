{#<script src="https://cdn.webdatarocks.com/latest/webdatarocks.googlecharts.js"></script>#}
{#<script src="https://www.gstatic.com/charts/loader.js"></script>#}
{#<link href="https://cdn.webdatarocks.com/latest/webdatarocks.min.css" rel="stylesheet"/>#}
{#<script src="https://cdn.webdatarocks.com/latest/webdatarocks.toolbar.min.js"></script>#}
{#<script src="https://cdn.webdatarocks.com/latest/webdatarocks.js"></script>#}
<script src=”https://kendo.cdn.telerik.com/2017.1.223/js/jszip.min.js”></script>
<button class="btn btn-success" onclick="excelToValue()">Export</button>
<table>
    <tr>
        <td class="col-lg-12">
            <div id="wdr-component"></div>
        </td>
        <td>
            <div id="googlechart-container" style="width:600px;height:600px;"></div>
        </td>
    </tr>
</table>

<script>
    const data = JSON.parse('{{ data }}');
    var pivot = new WebDataRocks({
        container: "#wdr-component",
        toolbar: true,
        height: 600,
        customizeCell: customizeCellFunction,
        report: {
            "dataSource": {
                "dataSourceType": "json",
                "data": data
            },
            "slice": {
                "rows": [
                    {
                        "uniqueName": "Tip_Felera",
                        "sort": "asc"
                    },
                    {
                        "uniqueName": "Sezona",
                        "sort": "asc"
                    },
                    {
                        "uniqueName": "Proizvod",
                        "sort": "asc"
                    }
                ],
                'reportFilters': [
                    {
                        "uniqueName": "Sezona",
                    }
                ],
                "measures": [
                    {
                        "uniqueName": "Kolicina",
                        "aggregation": "sum"
                    },
                ],
                "expands": {
                    "expandAll": false,
                },
                "drills": {
                    "drillAll": false
                }
            },
            "options": {
                "grid": {
                    "type": "compact"
                },
                "chart": {
                    "type": "bar",
                    "title": "",
                    "showFilter": true,
                    "multipleMeasures": false,
                    "oneLevel": false,
                    "autoRange": false,
                    "reversedAxes": false,
                    "showLegendButton": false,
                    "showAllLabels": false,
                    "showMeasures": true,
                    "showOneMeasureSelection": true,
                    "showWarning": true,
                    "activeMeasure": ""
                },
                "showEmptyData": false
            },
            "formats": [{
                "name": "",
                "thousandsSeparator": ",",
                "decimalSeparator": ".",
                "decimalPlaces": 2,
                "maxSymbols": 20,
                "currencySymbol": "",
                "currencySymbolAlign": "left",
                "nullValue": " ",
                "infinityValue": "Infinity",
                "divideByZeroValue": "Infinity"
            }]
        },
        reportcomplete: function () {
            pivot.off("reportcomplete");
            pivotTableReportComplete = true;
            createGoogleChart();
        }
    });
    var pivotTableReportComplete = false;
    var googleChartsLoaded = false;

    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(onGoogleChartsLoaded);

    function onGoogleChartsLoaded() {
        googleChartsLoaded = true;
        if (pivotTableReportComplete) {
            createGoogleChart();
        }
    }

    function createGoogleChart() {
        if (googleChartsLoaded) {

            pivot.googlecharts.getData({
                    type: "pie"
                },
                drawChart,
                drawChart
            );
        }
    }

    function drawChart(_data) {
        var data = google.visualization.arrayToDataTable(_data.data);

        var options = {
            title: "Prosecan pregled felera",
            legend: {
                position: 'right'
            },
            is3D: true
        };

        var chart = new google.visualization.PieChart(document.getElementById('googlechart-container'));
        chart.draw(data, options);
    }


    function customizeCellFunction(cell, data) {
        if (data.rows) {
            for (var i = 0; i < data.rows.length; i++) {
                if (data.rows[i]["hierarchyCaption"] == "Business Type" && data.rows[i]["caption"] == "Specialty Bike Shop") {
                    cell.attr["hierarchy"] = data.rows[i]["hierarchyCaption"];
                    cell.attr["member"] = data.rows[i]["caption"];
                } else if (data.rows[i]["hierarchyCaption"] == "Business Type" && data.rows[i]["caption"] == "Value Added Reseller") {
                    cell.attr["hierarchy"] = data.rows[i]["hierarchyCaption"];
                    cell.attr["member"] = data.rows[i]["caption"];
                } else if (data.rows[i]["hierarchyCaption"] == "Business Type" && data.rows[i]["caption"] == "Warehouse") {
                    cell.attr["hierarchy"] = data.rows[i]["hierarchyCaption"];
                    cell.attr["member"] = data.rows[i]["caption"];
                }
            }
        }
    }

    function excelToValue() {
        webdatarocks.exportTo("excel", null, (res) => {
            console.log(res);
        });
    }
</script>