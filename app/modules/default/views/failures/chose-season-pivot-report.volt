<div id="choose-order-html" class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="text-center">{{ t('Otvorene Porudžbine') }}</h3>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables">
                                <thead>
                                <tr>
                                    <th class="col-sm-4 m-b-xs">{{ t('Tip') }}</th>
                                    <th class="col-sm-8 m-b-xs">{{ t('Kolekcija') }}</th>
                                    <th class="text-center col-sm-1 m-b-xs">{{ t('Akcije') }}</th>
                                </thead>
                                <tbody>
                                {% for doubleSeason in doubleSeasons %}
                                        <td>
                                            {{ doubleSeason['type'] }}
                                        </td>
                                        <td>{{ doubleSeason['name'] }}</td>
                                        <td class="text-center">
                                            <a class="btn btn-success" href="/failures/pivot-report/{{ doubleSeason['id'] }}">
                                                Pogledaj <i class="fa fa-calculator"></i>
                                            </a>
                                        </td>
                                    </tr>
                                {% endfor %}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
