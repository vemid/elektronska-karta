<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj</h3>
                            <a href="#" onclick="getCreateForm('failures',event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" id="example" >
                        <thead>
                        <tr>
                            <th>{{ t('Proizvod') }}</th>
                            <th>{{ t('Kolekcija') }}</th>
                            <th>{{ t('Objekat') }}</th>
                            <th>{{ t('Kolicina') }}</th>
                            <th>{{ t('Vrsta Felera') }}</th>
                            <th>{{ t('Realizacija') }}</th>
                            <th>{{ t('Prijava') }}</th>
                            <th>{{ t('Akcija') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for failure in failures %}
                            {% if failure.procesType %}
                            <tr>
                             {% else %}
                             <tr style="background-color: lightcoral">
                            {% endif %}
                                <td>{{ failure.getProductSize().getProduct() }}</td>
                                <td>{{ failure.getProductSize().getProductSizeHydrator().getOldCollection().getName() }}</td>
                                <td>{{ failure.getShopCode().getName() }}</td>
                                <td>{{ failure.qty }}</td>
                                <td><a class="product-tooltip text-danger" data-toggle="tooltip"  title="{{ failure.note }}">{{ failure.getFailureCode().getName() }}</a></td>
                                <td>{{ failure.procesType ? t(failure.procesType) : "Nije upisano" }}</td>
                                <td>{{ t(failure.failureOrigin) }}</td>
                                <td class="text-center" width="11.5%">
                                    <a href="#" onclick="getUpdateForm('failures', {{ failure.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getDeleteForm('failures', {{ failure.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready( function () {
        $('#example').DataTable({
            "pageLength": 100
        });
    })
</script>