<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj</h3>
                            <a href="#" title="Dodavanje" onclick="getCreateForm('product-calculation-maps', event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" >
                        <thead>
                        <tr>
                            <th>{{ t('Kolekcija') }}</th>
                            <th>{{ t('Marza') }}</th>
                            <th>{{ t('Kurs Eura') }}</th>
                            <th class="text-center">{{ t('Akcije') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for calculationMapping in calculationMappings %}
                            <tr>
                                <td>{{ calculationMapping.getClassification().name}}</td>
                                <td>{{ calculationMapping.markUp * 100  }} %</td>
                                <td>{{ calculationMapping.eurCourse }}</td>
                                <td class="text-center" width="11.5%">
                                    <a href="#" onclick="getUpdateForm('product-calculation-maps', {{ calculationMapping.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getDeleteForm('product-calculation-maps', {{ calculationMapping.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>