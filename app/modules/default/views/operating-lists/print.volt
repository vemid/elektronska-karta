


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css" />
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css" />
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
</head>
<body class="pace-done">
<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table valign="middle" width="100%" cellpadding="0" cellspacing="0" >
                        <tbody>
                        <tr>
                            <td valign="middle" align="center" style=" padding: 5px;">
                                <p>{{ t('Operaciona Lista') }}</p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="center" style=" padding: 5px;">
                                <p>Proizvod : {{ operatingList.getProduct().getCode() }} --
                                    {{ operatingList.getProduct().getName() }}</p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="center" style=" padding: 5px;">
                                <p>Radni nalozi : {{ operatingList.getWorkOrders() }}</p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="center" style=" padding: 5px;">
                                <p>Planirana Kolicina : {{ totals.getTotalPlanned(operatingList.getProduct()) }}</p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="center" style=" padding: 5px;">
                                <p>Beleska : {{ operatingList.note }} || TG: {{ operatingList.tactGroup }}</p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="center" style=" padding: 5px;">
                                <span class="bigger-120 bold">{{ hydrator.getOldCollection() ? hydrator.getOldCollection().getName() : '' }}</span>
                            </td>
                        </tr>
                        <tr style="padding-top: 50px">
                            <td valign="middle" align="center" style=" padding: 5px;">
                                <input type="image" src="{{ operatingList.getProduct().getImagePrintPath() }}" width="30%" style="cursor: pointer; outline: none;"/>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="page-break"></div>
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" >
                        <thead>
                        <tr>
                            <td class="text-center">
                                <p>{{ t('Naziv') }}</p>
                            </td>
                            <td class="text-center">
                                <p> {{ t('Masina') }} </p>
                            </td>
                            <td class="text-center">
                                <p> {{ t('Vreme 1kom') }} </p>
                            </td>
                            <td class="text-center">
                                <p> {{ t('Kom za 1h') }} </p>
                            </td>
                            <td class="text-center">
                                <p> {{ t('Cena Kom') }} </p>
                            </td>
                            <td class="text-center">
                                <p> {{ t('Opterecenje') }} </p>
                            </td>
                            <td class="text-center">
                                <p> {{ t('Barkod') }} </p>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        {% set totalTime = 0 %}
                        {% for operatingListItem in operatingList.getOperatingListItems() %}
                            <tr>
                                <td>
                                    <p>{{ operatingListItem.getOperatingItemListType().name }}</p>
                                </td>
                                <td>
                                    <p> {{ operatingListItem.getOperatingItemListType().machine ? operatingListItem.getOperatingItemListType().machine : operatingListItem.getOperatingItemListType().getProductionMachine().name }} </p>
                                </td>
                                <td>
                                    <p> {{ operatingListItem.pieceTime }} </p>
                                </td>
                                <td>
                                    <p> {{ operatingListItem.piecesHour }} </p>
                                </td>
                                <td>
                                    <p> {{ operatingListItem.piecePrice }} </p>
                                </td>
                                <td>
                                    <p> {{ number_format(operatingListItem.pieceTime/operatingList.tactGroup*100,0)}} %</p>
                                </td>
                                <td style=" padding: 5px; width: 300px" >
                                    <img src="data:image/png;base64,{{ base64_encode(generatorHtml.getBarcode(operatingListItem.barcode, barcodeType)) }}">
                                </td>
                            </tr>
                            {% set totalTime += operatingListItem.pieceTime %}
                        {% endfor %}
                        <tr>
                            <td></td>
                            <td>Ukupno masinsko vreme:</td>
                            <td>{{ totalTime }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr></tr>
                        <tr>
                            <td></td>
                            <td>Ukupno rucno vreme:</td>
                            <td>{{ totalTime }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css" />
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>