<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-5">
                        <div id="product-image" style="max-height: 500px">
                            <input type="image" src="{{ operatingList.getProduct().getImagePath() }}" width="80%" style="cursor: pointer; outline: none;"/>
                            <input type="file" id="reUpload" style="display: none;" />
                        </div>
                    </div>
                    <div class="col-md-7">

                        <h2 class="font-bold m-b-xs">
                            Sifra Artikla :
                        </h2>
                        <div class="space-15"></div>
                        <span class="text-info bigger-160 bold">
                                            {{ operatingList.getProduct().getCode() }} --
                            {{ operatingList.getProduct().getName() }}
                                        </span>
                        <div class="m-t-md">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Naziv</th>
                                    <th>Vrednost</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Pol</td>
                                    <td>
                                        <span class="bigger-120 bold">{{ hydrator.getGenderName() }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Grupa Artikla</td>
                                    <td>
                                                        <span class="bigger-120 bold"><?php echo array_pop(explode(' ', $hydrator->
                                                            getProductGroup()->getName()))?></span></td>
                                </tr>
                                <tr>
                                    <td>1st Grupa Artikla</td>
                                    <td><span class="bigger-120 bold"><?php echo $hydrator->getSubProductGroup()->getName(); ?></span></td>
                                </tr>
                                <tr>
                                    <td>2nd Grupa Artikla</td>
                                    <td>
                                        <span class="bigger-120 bold"><?php echo substr(strstr($hydrator->getDubSubProductGroup()->getName()," "), 1)?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Boja Artikla</td>
                                    <td>
                                        <span class="bigger-120 bold">{{ hydrator.getColor() ? hydrator.getColor().getName() : '' }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Brend</td>
                                    <td>
                                        <span class="bigger-120 bold">{{ hydrator.getBrand() ? hydrator.getBrand().getName() : '' }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Uzrast</td>
                                    <td>
                                        <span class="bigger-120 bold">{{ hydrator.getAge() ? hydrator.getAge().getName() : '' }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Sezona</td>
                                    <td>
                                        <span class="bigger-120 bold">{{ hydrator.getSeason() ? hydrator.getSeason().getName() : '' }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kolekcija 07</td>
                                    <td>
                                        <span class="bigger-120 bold">{{ hydrator.getOldCollection() ? hydrator.getOldCollection().getName() : '' }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Velicine</td>
                                    <td>
                                        <span class="bigger-120 bold">{{ hydrator.getAllProductSizes() }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bigger-180">Planirano / Isporuceno</td>
                                    <td>
                                        <span class="bigger-180 bold">{{ totals }} / {{ totalReceived }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bigger-180">Radni takt:</td>
                                    <td>
                                        <span class="bigger-180 bold">{{ operatingList.tactGroup }}</span>
                                    </td>
                                </tr>
                                {% if electronCard %}
                                <tr>
                                    <td class="bigger-180">Teh.Skica:</td>
                                    <td>
                                        <button id="myBtn" type="submit" class="btn btn-sm btn-success">
                                            <i class="fa fa-binoculars"></i> Pogledaj skicu
                                        </button>
                                        <div id="myModal" class="modal" >
                                            <!-- Modal content -->
                                            <div class="modal-content">
                                                <div class="col-xs-9" id="electronCard">

                                                </div>
                                                <div class="col-xs3">
                                                    <form id="operatingListNote" method="post"
                                                          action="/operating-lists/add-note/{{ operatingList.id }}"
                                                          class="form-horizontal">
                                                        <label style="margin-top: 20px">
                                                            <textarea rows="10" cols="40" name="note" form="operatingListNote" placeholder="Beleska operacione liste">{{ operatingList.note }}</textarea>
                                                        </label>
                                                        <button type="submit" class="btn btn-sm btn-success">
                                                            <i class="fa fa-save"></i> Sacuvaj belesku
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                                {% endif %}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-8">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="text-center">
                            <h3>1. Dodaj Tip Operacije</h3>
                            <a href="#" onclick="getCreateForm('operating-list-item-types',event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-dashcube"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="text-center">
                            <h3>2. Dodaj operaciju</h3>
                            <a href="#" onclick="getCustomCreateForm('operating-list-items','operatingList','{{ operatingList.id }}',event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="text-center">
                            <h3>3. Dodaj takt</h3>
                            <a href="#" onclick="getForm('/operating-lists/add-tact-group-form', '/operating-lists/save-tact-group/{{ operatingList.id }}', event, '')" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="space-30"></div>
                <div id="operating-lists" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover opListTable" >
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ t('Naziv') }}</th>
                            <th width="1%">{{ t('Masina') }}</th>
                            <th>{{ t('Vreme 1kom') }}</th>
                            <th>{{ t('Kom za 1h') }}</th>
                            <th>{{ t('Cena Kom') }}</th>
                            <th>{{ t('Zavrseno') }}</th>
                            <th>{{ t('Radni takt') }}</th>
                            <th>{{ t('Akcija') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% set total = 0 %}
                        {% for operatingListItem in operationListItems %}
                            <tr {{ finished.getTotalPerItem(operatingListItem) !== totals ? ' style="background: lightcoral !important;"':'' }}>
                                <td>{{ loop.index }}</td>
                                <td>{{ operatingListItem.getOperatingItemListType().name }}</td>
                                <td>{{ operatingListItem.getOperatingItemListType().machine ? operatingListItem.getOperatingItemListType().machine : operatingListItem.getOperatingItemListType().getProductionMachine().name }}</td>
                                <td>{{ operatingListItem.pieceTime }}</td>
                                <td>{{ operatingListItem.piecesHour }}</td>
                                <td>{{ operatingListItem.piecePrice }}</td>
                                <td>{{ finished.getTotalPerItem(operatingListItem) }}</td>
                                <td>{{ operatingList.tactGroup > 0 ? number_format(operatingListItem.pieceTime/operatingList.tactGroup*100,2) : '0'}}</td>
                                <td class="text-center" width="13.5%">
                                    <a href="/operating-list-items/overview/{{ operatingListItem.id }}" class="bigger-140 text-danger">
                                        <button class="btn btn-info btn-circle" type="button" title="Profil">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </a>
                                    {% if operatingList.locked %}
                                        {% if currentUser.isSuperAdmin() %}
                                            <a href="#" onclick="getUpdateForm('operating-list-items', {{ operatingListItem .id }}, event)" class="bigger-140 text-danger">
                                                <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                            </a>
                                            <a href="#" onclick="getDeleteForm('operating-list-items', {{ operatingListItem .id }}, event)" class="bigger-140 text-danger">
                                                <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                            </a>
                                        {% endif %}
                                        {% else %}
                                            <a href="#" onclick="getUpdateForm('operating-list-items', {{ operatingListItem .id }}, event)" class="bigger-140 text-danger">
                                                <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                            </a>
                                            <a href="#" onclick="getDeleteForm('operating-list-items', {{ operatingListItem .id }}, event)" class="bigger-140 text-danger">
                                                <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                            </a>
                                    {% endif %}
                                    <a href="/operating-list-items/print/{{ operatingListItem.id }}"
                                       target="_blank" class="bigger-140 text-danger">
                                        <button class="btn btn-info btn-circle" type="button"
                                                title="Stampa"><i class="fa fa-print"></i></button>
                                    </a>
                                    {% set total += operatingListItem.pieceTime %}
                                </td>
                            </tr>
                        {% endfor %}
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Ukupno vreme:</td>
                            <td>{{ total }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-4">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <h3 class="text-center">Pretraga operacija</h3>
                    <div class="col-xs-12">
                        <div class="space-20"></div>
                        <select data-id="{{ operatingList.id }}" name="productIdCalculations" id="simOperatingList" class="search required form-control" style="display: none;">
                            <option value="" selected="selected">-- Izaberite --</option>
                            {% for key, option in options %}
                                <option value="{{ key }}" {{ similarProductCalculation and similarProductCalculation.id == key ? 'selected' : '' }}>{{ option }}</option>
                            {% endfor %}
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div id="load-into"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function ($) {
        $(document).on("ready", function () {
            $(document).on("change", "#simOperatingList", function (e) {
                var id = $(this).val();

                $( "#load-into" ).load("/operating-lists/copy-items/{{ operatingList.id }}/" + id + " #copy-items", function() {
                } );
                $( "#myModal" ).load( "/electron-cards/overview/{{ electronCard.id }} #tabElectronCard");
            })
        });
    })($);
    $(document).ready(function() {
        $("#myBtn").click(function(){
            $('#myModal').modal('show');
            $( "#electronCard" ).load( "/electron-cards/overview/{{ electronCard.id }} #tabElectronCard");
            $('#operatingListNote').keydown(function(e){
                if ( e.which == 13 ) {
                    e.preventDefault();
                    return false;
                }
            });
        });
    });
</script>
