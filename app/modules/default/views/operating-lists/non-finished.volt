<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="space-30"></div>
                            <div class="form-horizontal">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        {% if form.has('oldCollectionId') %}
                                            {% set oldCollectionId = form.get('oldCollectionId') %}
                                            {{ oldCollectionId }}
                                        {% endif %}
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            {% if form.has('genderClassificationId') %}
                                                {% set genderClassificationId = form.get('genderClassificationId') %}
                                                {{ genderClassificationId }}
                                            {% endif %}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <div class="">
                                            {% if form.has('groupModelId') %}
                                                {% set groupModelId = form.get('groupModelId') %}
                                                {{ groupModelId }}
                                            {% endif %}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space-30"></div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover operatingListTable">
                            <thead>
                            <tr>
                                <th width="1px">{{ t('Proizvod') }}</th>
                                <th class="col-xs-2">{{ t('Proizvod') }}</th>
                                <th class="col-xs-3">{{ t('Naziv') }}</th>
                                <th class="col-xs-3">{{ t('Datum pocetka') }}</th>
                                <th class="col-xs-3">{{ t('Datum poslednje promene') }}</th>
                                <th class="col-xs-3">{{ t('Akcija') }}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        var cls = $("#typeMaterial").val();
        var table = $(".operatingListTable").DataTable( {
            "processing": true,
            "serverSide": true,
            "search" : true,
            "pageLength": 50,
            "ajax": {
                "url": "/operating-lists/get-filter-non-finished",
                "data":  function ( d ) {
                    return $.extend({}, d, {
                        "oldCollectionId": $('#oldCollectionId').val(),
                        "genderClassificationId" : $("#genderClassificationId").val(),
                        "groupModelId" : $("#groupModelId").val()
                    });
                }
            },
            "fnDrawCallback": function( oSettings ) {
                $("a.product-tooltip").tooltip({
                    animated: 'fade',
                    placement: 'top',
                    html: true,
                    container: 'body'
                });
            },
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return '<a class="product-tooltip text-danger" data-toggle="tooltip" title="<img src=\''+ row[5] + '\' width=\'100%\'/>">' + row[1] + '</a>';
                    },
                    "targets": 1
                },
                {
                    "render": function ( data, type, row ) {
                        return '<a href="/operating-lists/overview/' + row[0] + '" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search"></i></button></a> '
                    },
                    "targets": 5
                },
                {
                    "targets": [ 0 ],
                    "visible": false
                },
                {
                    "targets": [ 6 ],
                    "visible": false
                }
            ]
        });

        $(document).on("change", "#oldCollectionId, #genderClassificationId, #groupModelId", function(){
            table.draw();
        });
    });
</script>