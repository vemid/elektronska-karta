<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div id="details" class="col-xs-12">
                        <table class="table table-striped table-bordered dataTable">
                            <thead>
                            <tr>
                                <th class="text-center">{{ t('Datum') }}</th>
                                <th class="text-center">{{ t('Ukupno') }}</th>
                                <th class="text-center">{{ t('Ukupno Radnika') }}</th>
                                <th class="text-center">{{ t('Ostvareno Vreme') }}</th>
                                <th class="text-center">{{ t('Planirano Vreme') }}</th>
                            </thead>
                            <tbody>
                            {% set totalQty = 0 %}
                            {% set totalWorker = 0 %}
                            {% set totalRealized = 0 %}
                            {% set totalPlanned = 0 %}
                            {% for row in data %}
                                {% set totalQty = totalQty + row['total'] %}
                                {% set totalWorker = totalWorker + row['totalWorker'] %}
                                {% set totalRealized = totalRealized + row['realizedTime'] %}
                                {% set totalPlanned = totalPlanned + row['planned'] %}

                                <tr {{ row['sunday'] ? 'style="background: #ea7d7d"' : '' }}>
                                    <td>{{ row['date'] }}</td>
                                    <td>{{ row['total'] }}</td>
                                    <td>{{ row['totalWorker'] }}</td>
                                    <td>{{ row['realizedTime'] }}</td>
                                    <td>{{ row['planned'] }}</td>
                                </tr>
                            {% endfor %}
                            </tbody>
                            <tfoot>
                            <tr>
                                <td class="text-right"><b>Total:</b></td>
                                <td><b>{{ total > 0 ? totalQty : 0 }}</b></td>
                                <td><b>{{ total > 0 ? totalWorker : 0 }}</b></td>
                                <td><b>{{ total > 0 ? totalRealized : 0 }}</b></td>
                                <td><b>{{ total > 0 ? totalPlanned : 0 }}</b></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>