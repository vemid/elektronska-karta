<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="form-horizontal">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Sektor:</label>
                                <div class="col-sm-7">
                                    <select class="search form-control" name="codeId" id="codeId">
                                        <option value="">-- Izaberi --</option>
                                        {% for code in codes %}
                                            <option value="{{ code.id }}">{{ code.name }}</option>
                                        {% endfor %}
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Datum:</label>
                                <div class="col-sm-7">
                                    <input type="text" value="{{ date }}" class="datepicker form-control" name="date" id="date"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="space-25"></div>
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="col-xs-3">{{ t('Proizvod') }}</th>
                            <th class="col-xs-1">{{ t('Ukupno radnika Radnika') }}</th>
                            <th class="col-xs-1">{{ t('% Završenih') }}</th>
                            <th class="col-xs-2">{{ t('Ukupno planirano') }}</th>
                            <th class="col-xs-2">{{ t('Količina izvrešeno') }}</th>
                            <th class="text-center">{{ t('Zarada') }}</th>
                            <th class="text-center">{{ t('Zarada') }}</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var table = $("#example").DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/operating-lists/get-reports-data",
                "data":  function ( d ) {
                    return $.extend({}, d, {
                        "codeId": $('#codeId').val(),
                        "date": $('#date').val()
                    });
                }
            },
            "columns": [
                null,
                null,
                null,
                null,
                null,
                { className: "text-center" },
                { className: "text-center" }
            ],
            // "columnDefs": [
            //     {
            //         "render": function ( data, type, row ) {
            //             return '<a href="/material-samples/edit-form/' + row[4] + '" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Izmena"><i class="fa fa-edit"></i></button></a> ' +
            //                 '<a href="#" onclick="getDeleteForm(\'products\', '+ row[4] + ', event)" class="bigger-140 text-danger"><button class="btn btn-danger btn-circle" type="button" title="Brisanje"><i class="fa fa-times"></i></button></a> ' +
            //                 '<a href="/material-samples/overview/'+ row[4] +'" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search-plus"></i></button></a>';
            //         },
            //         "targets": 4
            //     }
            // ]
        });

        $("#codeId, #date").change( function() {
            table.draw();
        } );
    });
</script>