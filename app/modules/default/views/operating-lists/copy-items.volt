<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div id="copy-items" class="col-xs-12">
                        {% if operatingListCopy %}
                            <div class="row">
                                <div class="text-center">
                                    <div id="product-image" style="max-height: 500px">
                                        <input type="image" src="{{ operatingListCopy.getProduct().getImagePath() }}" width="40%" style="cursor: pointer; outline: none;"/>
                                        <input type="file" id="reUpload" style="display: none;" />
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" >
                                <thead>
                                <tr>
                                    <th>{{ t('Naziv') }}</th>
                                    <th width="14%">{{ t('Masina') }}</th>
                                    <th>{{ t('Vreme 1kom') }}</th>
                                    <th>{{ t('Kom za 1h') }}</th>
                                    <th>{{ t('Cena Kom') }}</th>
                                    <th>{{ t('Akcija') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                {% set total = 0 %}
                                {% for operatingListItem in operatingListCopy.operatingListItems %}
                                    <tr>
                                        <td>{{ operatingListItem.getOperatingItemListType().name }}</td>
                                        <td>{{ operatingListItem.getOperatingItemListType().machine }}</td>
                                        <td>{{ operatingListItem.pieceTime }}</td>
                                        <td>{{ operatingListItem.piecesHour }}</td>
                                        <td>{{ operatingListItem.piecePrice }}</td>
                                        <td>
                                            <a href="#" id="copy-item" onclick="getForm('/operating-lists/copy-item-form/{{ operatingList.id }}/{{ operatingListItem.id }}', '/operating-list-items/create', event, '')" class="btn btn-circle btn-warning">
                                                <i class="fa fa-copy"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    {% set total += operatingListItem.pieceTime %}
                                {% endfor %}
                                </tbody>
                            </table>
                        </div>
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>