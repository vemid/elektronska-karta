<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                        <div class="col-xs-12">
                            <div id="pivot-container"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var pivot = new WebDataRocks({
        container: "#pivot-container",
        toolbar: true,
        report: {
            dataSource: {
                filename: getJSONData()
            }
        }
    });

    function getJSONData() {
        return [{
            "Category": {
                type: "level",
                hierarchy: "Food"
            },
            "Item": {
                type: "level",
                hierarchy: "Food",
                level: "Dish",
                parent: "Category"
            },
            "Serving Size": {
                type: "level",
                hierarchy: "Food",
                level: "Size",
                parent: "Dish"
            },
            "Calories": {
                type: "number"
            },
            "Calories from Fat": {
                type: "number"
            }
        },
            {
                "Category": "Breakfast",
                "Item": "Frittata",
                "Serving Size": "4.8 oz (136 g)",
                "Calories": 300,
                "Calories from Fat": 120
            }
        ];
    }
</script>
