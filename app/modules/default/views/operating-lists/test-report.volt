<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div id="pivot"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let rows = JSON.parse('{{ rows }}');

    let dimensions = [
        {value: 'product', title: 'Proizvod'},
        {value: 'date', title: 'Datum'},
    ];

    let reduce = function(row, memo) {

        memo.total = memo.total < parseFloat(row.transaction.total) ? memo.total : parseFloat(row.transaction.total);
        // memo.total = (memo.total || 0) + parseFloat(row.transaction.total);
        memo.totalWorker = (memo.totalWorker || 0) + parseInt(row.transaction.totalWorker);
        memo.realizedTime = (memo.realizedTime || 0) + parseFloat(row.transaction.realizedTime);
        memo.planedTime = (memo.planedTime || 0) + parseFloat(row.transaction.planedTime);
        memo.outer = (parseFloat(row.transaction.realizedTime))/(parseFloat(row.transaction.planedTime));

        return memo
    };

    let calculations = [
        {
            title: 'Ukupno', value: 'total',
            template: function(val, row) {
                return '$' + val.toFixed(2)
            }
        },
        {
            title: 'Ukupno Radnika', value: 'totalWorker',
            template: function(val, row) {
                return ' ' + val
            }
        },
        {
            title: 'Ostvareno Vreme', value: 'realizedTime',
            template: function(val, row) {
                return ' ' + val
            }
        },
        {
            title: 'Planirano Vreme', value: 'planedTime',
            template: function(val, row) {
                return ' ' + val
            }
        }
        ,
        {
            title: 'Norma', value: 'outer',
            template: function(val, row) {
                return ' ' + val.toFixed(2)*100 + '%'
            }
        }
    ];
    ReactPivot(document.getElementById("pivot"), {
        rows: rows,
        dimensions: dimensions,
        calculations: calculations,
        reduce: reduce,
        activeDimensions: ['Proizvod']
    })
</script>