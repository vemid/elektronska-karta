<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center">
                                <h3>Kreiraj Operacionu listu</h3>
                                <a href="#" onclick="getCreateForm('operating-lists',event)"
                                   class="bigger-200 btn btn-circle btn-lg">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="space-30"></div>
                            <div class="form-horizontal">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        {% if form.has('oldCollectionId') %}
                                            {% set oldCollectionId = form.get('oldCollectionId') %}
                                            {{ oldCollectionId }}
                                        {% endif %}
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            {% if form.has('genderClassificationId') %}
                                                {% set genderClassificationId = form.get('genderClassificationId') %}
                                                {{ genderClassificationId }}
                                            {% endif %}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <div class="">
                                            {% if form.has('groupModelId') %}
                                                {% set groupModelId = form.get('groupModelId') %}
                                                {{ groupModelId }}
                                            {% endif %}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    {% for sector in sectors %}
                        <li class="{{ loop.first ? 'active' : '' }}"><a data-toggle="tab"
                                                                        href="#sector-{{ sector.id }}">{{ sector.name }}</a>
                        </li>
                    {% endfor %}
                </ul>
                <div class="tab-content">
                    {% for sector in sectors %}
                        <div id="sector-{{ sector.id }}" class="tab-pane{{ loop.first ? ' active' : '' }}">
                            <div class="panel-body">

                                <div class="space-30"></div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover operatingListTable-{{ sector.id }}">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th class="col-xs-2">{{ t('Proizvod') }}</th>
                                            <th class="col-xs-2">{{ t('Naziv') }}</th>
                                            <th class="col-xs-4">{{ t('Radni Nalozi') }}</th>
                                            <th class="col-xs-4">{{ t('Akcija') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    {% endfor %}
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        var tables = [];
        {% for sector in sectors %}
        var sectorId = '{{ sector.id }}';
        var table = $(".operatingListTable-" + sectorId).DataTable({
            "processing": true,
            "serverSide": true,
            "search": true,
            "pageLength": 50,
            "ajax": {
                "url": "/operating-lists/get-filter-list/" + sectorId,
                "data": function (d) {
                    return $.extend({}, d, {
                        "oldCollectionId": $('#oldCollectionId').val(),
                        "genderClassificationId": $("#genderClassificationId").val(),
                        "groupModelId": $("#groupModelId").val()
                    });
                }
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (!aData[4]) {
                    $('td', nRow).closest('tr').css('background', 'lightcoral');
                }

                return nRow;
            },
            "fnDrawCallback": function (oSettings) {
                $("a.product-tooltip").tooltip({
                    animated: 'fade',
                    placement: 'top',
                    html: true,
                    container: 'body'
                });
            },
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return '<a class="product-tooltip text-danger" data-toggle="tooltip"  title="<img src=\'' + row[5] + '\' width=\'100%\'/>">' + row[1] + '</a>';
                    },
                    "targets": 1
                },
                {
                    "render": function (data, type, row) {
                        let html = '<a href="/operating-lists/overview/' + row[0] + '" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search"></i></button></a> ';

                        if (row[6]) {
                            if(row[7]== true) {
                                html += '<a href="#" onclick="sendPostAjaxCall(\'/operating-lists/unlock/' + row[0] + '\', {})" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Otkljucavanje"><i class="fa fa-lock"></i></button></a> ';
                            }
                            else html += '<a href="#" onclick="sendPostAjaxCall(\'/operating-lists/lock/' + row[0] + '\', {})" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Zakljucavanje"><i class="fa fa-unlock"></i></button></a> ';

                        }

                        html +=  '<a href="#" onclick="getUpdateForm(\'operating-lists\', ' + row[0] + ', event)" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Izmena"><i class="fa fa-edit"></i></button></a> ' +
                            '<a href="#" onclick="getDeleteForm(\'operating-lists\', ' + row[0] + ', event)" class="bigger-140 text-danger"><button class="btn btn-danger btn-circle" type="button" title="Brisanje"><i class="fa fa-times"></i></button></a> ' +
                            '<a href="/operating-lists/print/' + row[0] + '" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Stampa"><i class="fa fa-print"></i></button></a> ' +
                            '<a href="/operating-lists/print-all-items/' + row[0] + '" class="bigger-140 text-danger"><button class="btn btn-danger btn-circle" type="button" title="Stampa svih listi"><i class="fa fa-print"></i></button></a> ' +
                            '<a href="#" onclick="getForm(\'/operating-lists/copy-form/' + row[0] + '\', \'/operating-lists/copy\', event, \'\')" class="bigger-140 text-danger"><button class="btn btn-warning btn-circle" type="button" title="Izmena"><i class="fa fa-copy"></i></button></a> ';

                        return html;
                    },
                    "targets": 4
                },
                {
                    "targets": [0],
                    "visible": false
                },
                {
                    "targets": [5],
                    "visible": false
                },
                {
                    "targets": 6,
                    "visible": false
                }
            ],
            "columns": [
                {className: null},
                {className: "col-xs-2 text-center"},
                {className: "col-xs-3 text-center"},
                {className: "col-xs-4 text-center"},
                {className: "col-xs-3 text-center"},
                {className: "col-xs-3 text-center"},
            ],
        });
        tables.push(table);
        {% endfor %}

        $(document).on("change", "#oldCollectionId, #genderClassificationId, #groupModelId", function () {
            $.each(tables, function( index, table ) {
                table.draw();
            });
        });
    });
</script>