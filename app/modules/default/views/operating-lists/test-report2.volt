<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h2 class="text-center">Neki izvestaj</h2>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-4">
                        <input name="startDate" id="startDate" type="text" class="form-control datepicker">
                    </div>
                    <div class="col-xs-4">
                        <input type="text" name="endDate" id="endDate" class="form-control datepicker">
                    </div>
                    <div class="col-xs-3">
                        <div class="col-xs-6 no-padding-left">
                            <input type="radio" class="" name="type" id="type" value="byDate"> Po Datumu
                        </div>
                        <div class="col-xs-6 no-padding">
                            <input type="radio" class="" name="type" id="type" value="byProduct"> Po Proizvodu
                        </div>
                    </div>
                    <div class="col-xs-1">
                        <button type="button" id="pivot-search" class="btn btn-success">Trazi</button>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-12">
                        <div id="pivot-wrap-date" class="hidden">
                            <table id="pivot-date" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center">{{ t('Šifra') }}</th>
                                    <th class="text-center">{{ t('Naziv') }}</th>
                                    <th class="text-center">{{ t('Boja') }}</th>
                                    <th class="text-center">{{ t('Cena') }}</th>
                                    <th class="col-sm-1 text-center">{{ t('Naruči') }}</th>
                                </thead>
                            </table>
                        </div>
                        <div id="pivot-wrap-product" class="">
                            <table id="pivot-product" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center">{{ t('Proizvod') }}</th>
                                    <th class="text-center">{{ t('Ukupno') }}</th>
                                    <th class="text-center">{{ t('Ukupno Radnika') }}</th>
                                    <th class="text-center">{{ t('Ostvareno Vreme') }}</th>
                                    <th class="text-center">{{ t('Planirano Vreme') }}</th>
                                    <th class="text-center">{{ t('% Norma') }}</th>
                                    <th class="col-sm-1 text-center">{{ t('Detaljno') }}</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function ($) {
        $(document).on("ready", function () {

            var tableDate = $("#pivot-date").DataTable({
                "processing": true,
                "serverSide": true,
                "searching": false,
                "ajax": {
                    "url": "/operating-lists/get-grouped-by-product-reports-data/{{ order.id }}",
                    "data": function (d) {
                        return $.extend({}, d, {
                            "startDate": $('#startDate').val(),
                            "endDate" : $("#endDate").val(),
                        });
                    }
                },
                "fnDrawCallback": function( oSettings ) {
                    $("a.product-tooltip").tooltip({
                        animated: 'fade',
                        placement: 'top',
                        html: true,
                        container: 'body'
                    });
                },
            });

            var tableProduct = $("#pivot-product").DataTable({
                "processing": true,
                "serverSide": true,
                "searching": false,
                "ajax": {
                    "url": "/operating-lists/get-grouped-by-product-reports-data/{{ order.id }}",
                    "data": function (d) {
                        return $.extend({}, d, {
                            "startDate": $('#startDate').val(),
                            "endDate" : $("#endDate").val(),
                        });
                    }
                },
                "fnDrawCallback": function( oSettings ) {
                    $("a.product-tooltip").tooltip({
                        animated: 'fade',
                        placement: 'top',
                        html: true,
                        container: 'body'
                    });
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td', nRow).closest('tr').addClass(aData[7]);

                    return nRow;
                },
                "columns": [
                    { className: "col-xs-4 text-left" },
                    { className: "col-xs-1 text-center" },
                    { className: "col-xs-1 text-center" },
                    { className: "col-xs-1 text-center" },
                    { className: "col-xs-1 text-center" },
                    { className: "col-xs-1 text-center" },
                    { className: "col-xs-1 text-center" }
                ],
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a class="text-info" id="report-detailed" href="#" data-id="'+ row[6] +'"><i class="fa fa-search"></i></a>';
                        },
                        "targets": 6
                    },
                    {
                        "targets": 7,
                        "visible": false
                    },
                ]
            });

            $(document).on("click", "#pivot-search", function (e) {
                var type = $("#type:checked").val();
                if (type == 'byProduct') {
                    $("#pivot-wrap-product").removeClass('hidden');
                    $("#pivot-wrap-date").addClass('hidden');
                    tableProduct.draw();
                } else if (type == 'byDate') {
                    $("#pivot-wrap-product").addClass('hidden');
                    $("#pivot-wrap-date").removeClass('hidden');
                    tableDate.draw();
                }
            });
        });
    })($);
</script>