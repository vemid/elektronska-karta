{{ partial('partials/call-to-action', {'callToActionHeading': t('Reset password'), 'callToActionDescription': '', 'callToActionImage': 'auth' }) }}

<div class="wrapper">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox float-e-margins">
                <div class="space-30"></div>
                <div class="space-15 clear"></div>
                <div class="ibox-content no-borders">
                    <div class="row">
                        <div class="col-xs-12 col-md-3 col-sm-6 b-r log-right no-padding-left">
                            <div class="space-15"></div>
                            <h4>{{ t('Quick Login') }}</h4>

                            <div class="space-20 clear"></div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <button class="btn social-button facebook" onclick="facebookLogin()">
                                        <span>
                                            <i class="fa fa-facebook modal-icons bigger-120 fa-bold"></i>
                                        </span>
                                        {{ t('Login with Facebook account') }}
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <a class="btn social-button twitter" href="/auth/twitter-login">
                                        <span>
                                            <i class="fa fa-twitter modal-icons bigger-120 fa-bold"></i>
                                        </span>
                                        {{ t('Login with Twitter account') }}
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <button id="signinButton" class="btn social-button google">
                                        <span>
                                            <i class="fa fa-google-plus modal-icons bigger-120 fa-bold"></i>
                                        </span>
                                        {{ t('Login with Google+ account') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 col-sm-6 sign-up-reg-form">
                            <div class="space-15"></div>
                            <h4>{{ t('Enter your email') }}</h4>

                            <div class="space-20 clear"></div>
                            {{ form() }}

                            <div class="form-group">
                                <input type="text" id="username" name="username" placeholder="{{ t('Email') }}"
                                       class="required form-control"/>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success text-uppercase">{{ t('Reset Your Password') }}</button>
                            </div>
                            {{ endForm() }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>