{{ partial('partials/call-to-action', {'callToActionHeading': t('Change password'), 'callToActionDescription': 'In order to set up new password, please fill form down.', 'callToActionImage': 'auth' }) }}

<div class="wrapper">
    <div class="row">
        <div class="col-xs-12">
            <div class="ibox float-e-margins">
                <div class="space-30 clear"></div>
                <div class="ibox-content no-borders">
                    <div class="row">
                        <div class="col-xs-12 col-md-3 col-sm-6 b-r log-right no-padding-left">
                            <div class="space-15"></div>
                            <h4>{{ t('Quick Login') }}</h4>

                            <div class="space-20 clear"></div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <button class="btn social-button facebook" onclick="facebookLogin()">
                                        <span>
                                            <i class="fa fa-facebook modal-icons bigger-120 fa-bold"></i>
                                        </span>
                                        {{ t('Login with Facebook account') }}
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <a class="btn social-button twitter" href="/auth/twitter-login">
                                        <span>
                                            <i class="fa fa-twitter modal-icons bigger-120 fa-bold"></i>
                                        </span>
                                        {{ t('Login with Twitter account') }}
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                    <button id="signinButton" class="btn social-button google">
                                        <span>
                                            <i class="fa fa-google-plus modal-icons bigger-120 fa-bold"></i>
                                        </span>
                                        {{ t('Login with Google+ account') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4 col-sm-6 sign-up-reg-form">
                            <div class="space-15"></div>
                            <h4>{{ t('Change password') }}</h4>

                            <div class="space-20 clear"></div>
                            {{ form() }}

                            <div class="form-group">
                                <input type="password" class="form-control required" id="newPassword" name="newPassword"
                                       placeholder="{{ t('New password') }}"/>

                                <div class="help-block m-b-none"></div>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control required" id="confirmPassword"
                                       name="confirmPassword"
                                       placeholder="{{ t('Confirm password') }}"/>

                                <div class="help-block m-b-none"></div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success text-uppercase">{{ t('Change password') }}</button>
                            </div>
                            {{ endForm() }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>