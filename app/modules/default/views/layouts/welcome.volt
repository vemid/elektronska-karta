<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Article">
<head>
    <meta charset="utf-8"/>
    {% if google.applicationId is defined %}
    <meta name="google-signin-client_id" content="{{ google.applicationId }}"/>
    {% endif %}
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    {{ partial('partials/meta') }}
    {{ getTitle() }}
    {{ assets.outputCss('headCss') }}
    {{ assets.outputCss('customCss') }}d
    {{ assets.outputJs('headJs') }}
</head>
<body class="frontend fixed-sidebar mini-navbar pace-done">
<div id="fb-root"></div>
<div id="wrapper">
    <div id="page-wrapper">
        <div id="content">
            <div id="flash-session">
                {{ flashSession.output() }}
            </div>
            <div class="content">
                {{ content() }}
            </div>

            {{ partial('partials/modal') }}

        </div>
        {{ partial('partials/footer') }}
    </div>
</div>
{{ assets.outputCss('footCss') }}
{{ assets.outputJs('footJs') }}
{{ assets.outputJs('customJs') }}
</body>
</html>