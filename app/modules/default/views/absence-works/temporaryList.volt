<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" id="absence">
                        <thead>
                        <tr>
                            <th>{{ t('Naziv Radnika') }}</th>
                            <th>{{ t('Datum od') }}</th>
                            <th>{{ t('Datum Do') }}</th>
                            <th>{{ t('Tip') }}</th>
                            <th>{{ t('Akcija') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for absence in absences %}
                            <tr>
                                <td>{{ absence.getProductionWorker().getDisplayName() }}</td>
                                <td>{{ absence.startDate }}</td>
                                <td>{{ absence.endDate }}</td>
                                <td>{{ absence.type }}</td>
                                <td class="text-center" width="11.5%">
                                    <a href="#" onclick="getUpdateForm('absence-works', {{ absence.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getDeleteForm('absence-works', {{ absence.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready( function () {
        $('#absence').DataTable({
            "pageLength": 50,
            "ordering" : false
        });
    })
</script>