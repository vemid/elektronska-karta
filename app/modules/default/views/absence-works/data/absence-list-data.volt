
<p>{{ paramDateFrom }}</p>
<p>{{ paramDateTo }}</p>
<p>{{ paramWorker }}</p>

<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" >
                        <thead>
                        <tr>
                            <th>{{ t('Naziv Radnika') }}</th>
                            <th>{{ t('Datum od') }}</th>
                            <th>{{ t('Datum Do') }}</th>
                            <th>{{ t('Tip') }}</th>
                            <th>{{ t('Akcija') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for absence in absences %}
                            <tr {{ absence.onSalary ? 'class="alert-danger"' : '' }}>
                                <td>{{ absence.getProductionWorker().getDisplayName() }}</td>
                                <td>{{ absence.startDate }}</td>
                                <td>{{ absence.endDate }}</td>
                                <td>{{ absence.type }}</td>
                                <td class="text-center" width="11.5%">
                                    <a href="#" onclick="getUpdateForm('absence-works', {{ absence.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getDeleteForm('absence-works', {{ absence.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="/production-workers/print-worker-date/{{ paramDateFrom }}/{{ paramDateTo }}/{{ worker.id }}" target="_blank" style="margin: 0 0 4px 0; padding: 0;">
                            <i class="fa fa-print text-info" style="font-size: 36px"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>