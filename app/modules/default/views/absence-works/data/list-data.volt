
<p>{{ paramDateFrom }}</p>
<p>{{ paramDateTo }}</p>
<p>{{ paramWorker }}</p>

<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" >
                        <thead>
                        <tr>
                            <th>{{ t('Radnik') }}</th>
                            <th>{{ t('Datum') }}</th>
                            <th>{{ t('Vrsta Odsustva') }}</th>
                            <th>{{ t('Operacije') }}</th>
                            <th>{{ t('Rezije') }}</th>
                            <th>{{ t('Zarada') }}</th>
                            <th>{{ t('Norma') }}</th>
                            <th>{{ t('Detaljnije') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% set totalPayment = 0 %}
                        {% set total = 0 %}
                        {% set totalCutting = 0 %}
                        {% set totalOperation = 0 %}
                        {% for dateRange in dateRanges %}
                            {% if (absenceWork.getTypePerDay(dateRange,productionWorker) == null) %}
                            {% set total = number_format((oliw.getTotalPaymentPerWorkerAndDate(dateRange,productionWorker) +
                                cuttingJob.getTotalPaymentPerWorkerAndDateCuttingJobs(dateRange,productionWorker)),0) %}
                            {% else %}
                                {% set total = 0 %}
                            {% endif %}
                            <tr {{ dateRange.format('N') == 7 ? 'class="alert-danger"' : '' }}>
                                <td>{{ productionWorker }}</td>
                                <td>{{ dateRange.getShortSerbianFormat() }}</td>
                                <td>
                                    {{ t(absenceWork.getTypePerDay(dateRange,productionWorker)) }}
                                </td>
                                <td>
                                    {{ number_format(oliw.getTotalPaymentPerWorkerAndDate(dateRange,productionWorker),0) }} RSD
                                </td>
                                <td>
                                    {{  number_format(cuttingJob.getTotalPaymentPerWorkerAndDateCuttingJobs(dateRange,productionWorker),0) }} RSD
                                </td>
                                <td>{{ total }} RSD </td>
                                <td>
                                    {{ number_format(itemCalculator.outTermPerWorkerAndDate(productionWorker,dateRange),2) }} %
                                </td>
                                <td><a href="/production-workers/list-by-worker-data?workerId={{ productionWorker.getId() }}&dateTo={{ dateRange.getUnixFormat() }}&dateFrom={{ dateRange.getUnixFormat() }}" target="_blank" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search"></i></button></a></td>
                            </tr>
                            {% set totalCutting += number_format(cuttingJob.getTotalPaymentPerWorkerAndDateCuttingJobs(dateRange,productionWorker),0)%}
                            {% set totalOperation += number_format(oliw.getTotalPaymentPerWorkerAndDate(dateRange,productionWorker),0)%}
                            {% set totalPayment += cuttingJob.getTotalPaymentPerWorkerAndDateCuttingJobs(dateRange,productionWorker) + oliw.getTotalPaymentPerWorkerAndDate(dateRange,productionWorker) %}
                        {% endfor %}
                        </tbody>
                        <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Ukupno</td>
                            <td>Oper: {{ totalOperation }}</td>
                            <td>Rez: {{ totalCutting }}</td>
                            <td>Zarada: {{ number_format(totalPayment,0) }}</td>
                            <td></td>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="pull-right">
                        <a href="/production-workers/print-worker-date/{{ paramDateFrom }}/{{ paramDateTo }}/{{ worker.id }}" target="_blank" style="margin: 0 0 4px 0; padding: 0;">
                            <i class="fa fa-print text-info" style="font-size: 36px"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>