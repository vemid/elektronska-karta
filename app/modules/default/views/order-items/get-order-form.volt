<style>
  #zoom-placeholder {
    position: fixed;
    bottom: 25px;
    left: 10px;
    z-index: 100;
    width: 250px;
    height: 250px;
  }
</style>
<div class="row" id="shopping-cart">
    <div class="col-xs-12">
        <form class="form-horizontal" action="/order-items/create" method="post">
            {% set totalSizes = product.productSizes|length %}
            {% set blockSize = 86 / (totalSizes + 1) %}
            {% set firstBlockSize = 12 - blockSize  * product.productSizes|length %}
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-6">
                    <a href="{{ product.getImagePath() }}" class="MagicZoom" data-options="zoomPosition: #zoom-placeholder;">
                        <img src="{{ product.getImagePath() }}" width="460px"
                             data-src="{{ product.getImagePath() }}">
</a>
                    </div>
                    <div class="col-xs-6" style="float: right; padding-left: 50px !important;">
                        <p style="font-size: 18px">{{ t("Sifra") }} : <b>{{ product.code }}</b></p>
                        <p style="font-size: 18px">{{ t("Proizvod") }} : <b>{{ product.name }}</b></p>
                        <p style="font-size: 18px">{{ t("Prioritet") }} : <b>{{ product.getPriority().getName() }}</b>
                        </p>
                        {#                        <p style="font-size: 18px">{{ t("Datum ulaza") }} : <b>{{ product.getPriority().getPriorityDate() }}</b></p> #}
                        {# <p style="font-size: 18px">{{ t("Sezona") }} : <b>{{ hydrator.getSeason() ? hydrator.getSeason().getName() : '' }}</b></p> #}
                        <p style="font-size: 18px">{{ t("Kolekcija") }} :
                            <b>{{ hydrator.getOldCollection() ? hydrator.getOldCollection().getName() : '' }}</b></p>
                        <br>
                        <p style="font-size: 18px">{{ t("Cena") }} : <b>{{ wholesalePriceEUR }} &euro;</b></p>
                        <p style="font-size: 18px">{{ t("Poreklo") }} : <b>{{ t(product.origin) }}</b></p>
                        <br>
                        <p style="font-size: 18px">{{ t("Sastav") }} : <b>{{ product.getComposition() }}</b></p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div style="width: {{ blockSize + 2 }}%; margin: 0 5px;" class="pull-left">
                        <div class="row">
                            <div class="form-group col-xs-12  no-padding-right no-margin-right">
                                <div class="col-xs-12">{{ t('Veličine') }}</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="pull-left form-control"
                                     style="background: #03befc;margin: 1px 0 15px 0">{{ t('Raspolozivo') }}</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="pull-left form-control"
                                     style="background: #D7FBCC; margin: 1px 0 15px 0">{{ t('Količina A') }}</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="pull-left form-control"
                                     style="background: #FBD2CC">{{ t('Količina B') }}</div>
                            </div>
                        </div>

                    </div>
                    {% for productSize in product.getProductSizes() %}
                        <div style="width: {{ blockSize }}%; margin: 0 5px;" class="pull-left">
                            {% set code = productSize.code.code %}
                            <div class="form-group">
                                <div class="col-xs-12 text-center"><span class="bigger-120"
                                                                         style="color: #a94442">{{ code }}</span></div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 text-center"><input class="form-control" type="text" value="{{ productSize.tempQty }}"
                                                                          name="qty" readonly="readonly"/></div>
                            </div>
                            {% set orderItem = orderManager.getClientOrderForProductSize(order, productSize, entity, entityType) %}
                            <div class="form-group">
                                <div class="col-xs-12">
                                    {% if orderItem and orderItem.quantityA %}
                                        <input class="form-control pull-left" type="text" id="{{ code }}_a"
                                               value="{{ orderItem.quantityA }}" name="order[{{ productSize.id }}][a]"/>
                                    {% else %}
                                        <input class="form-control pull-left" type="text" id="{{ code }}_a"
                                               name="order[{{ productSize.id }}][a]"/>
                                    {% endif %}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    {% if orderItem and orderItem.quantityB %}
                                        <input class="form-control" type="text" id="{{ code }}_b"
                                               value="{{ orderItem.quantityB }}" name="order[{{ productSize.id }}][b]"/>
                                    {% else %}
                                        <input class="form-control" type="text" id="{{ code }}_b"
                                               name="order[{{ productSize.id }}][b]"/>
                                    {% endif %}
                                </div>
                            </div>
                        </div>
                    {% endfor %}
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    document.querySelectorAll('.thumbnail').forEach(function (elem) {

        elem.onclick = function (e) {

            const src = elem.getAttribute('data-src')
            const html = '<img src="' + src + '">'

            basicLightbox.create(html).show()

        }

    })
</script>