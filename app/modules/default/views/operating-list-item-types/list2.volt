<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="text-center">
                                            <a href="#" title="Dodavanje" onclick="getCreateForm('operating-list-item-types',event)"  class="btn btn-success">
                                                <i class="fa fa-plus-square"></i>
                                                {{ t('Nova Kalkulacija') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                <div class="space-25"></div>
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="col-xs-3">{{ t('Sektor') }}</th>
                            <th class="col-xs-6">{{ t('Naziv Tipa') }}</th>
                            <th class="col-xs-2">{{ t('Masina') }}</th>
                            <th class="col-xs-1 text-center">{{ t('Akcija') }}</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        const table = $("#example").DataTable( {
            "processing": true,
            "serverSide": true,
            "pageLength": 50,
            "paging": true,
            "ajax": {
                "url": "/operating-list-item-types/get-list-data",
            },
            "fnDrawCallback": function (oSettings) {
                $("a.product-tooltip").tooltip({
                    animated: 'fade',
                    placement: 'top',
                    html: true,
                    container: 'body'
                });
            },
            "columns": [
                null,
                { className: "text-center" },
                { className: "text-center" },
                { className: "text-center" },
            ],
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        let html = '<a href="#" onclick="getUpdateForm(\'operating-list-item-types\', ' + row[3] + ', event)" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Izmena"><i class="fa fa-edit"></i></button></a> ' +
                            '<a href="#" onclick="getDeleteForm(\'operating-list-item-types\', ' + row[3] + ', event)" class="bigger-140 text-danger"><button class="btn btn-danger btn-circle" type="button" title="Brisanje"><i class="fa fa-times"></i></button></a> '
                        return html;
                    },
                    "targets": 3
                },
            ],
        });

    });
</script>