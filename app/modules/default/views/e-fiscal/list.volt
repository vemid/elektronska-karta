<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-6"></div>
                        <div class="col-xs-6">
                            <div class="pull-right">
                                <form action="/e-fiscal/upload-file" method="post" enctype="multipart/form-data">
                                    <div title="{{ t('Snimi file') }}" class="dropzone dropZoneSkull">
                                        <div class="fallback">
                                            <input name="warrentFile" type="file" />
                                        </div>
                                    </div>
                                    <div class="space-20"></div>
                                </form>
                                <div class="col-xs-12">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-8 text-center">
                                        <a href="#" id="start" class="btn btn-primary btn-rounded btn-block">
                                            {{ t('Snimi') }} <i class="fa fa-upload"></i>
                                        </a>
                                    </div>
                                    <div class="col-xs-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>