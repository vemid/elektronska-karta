<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css" />
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css" />
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
</head>
<body class="pace-done">
<div id="wrapper" style="margin-top: -20px !important; font-size: 18px !important;">
    <div id="content" class="">
        <div class="row">
            <div class="text-center">
                <h2>Radna lista</h2>
            </div>
            <div class="col-xs-8">
                <div class="ibox float-e-margins">
                    <div class="col-lg-12">
                        <table valign="middle" width="100%" cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr>
                                <td align="center" style="border: 1px solid #333; padding: 5px;">
                                    <img src="data:image/png;base64,{{ base64_encode(generatorHtml.getBarcode(operationListItem.barcode, barcodeType)) }}">
                                </td>
                                <td align="left" style="border: 1px solid #333; padding: 5px;">
                                    <b>Kolicina : {{ total }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="left" style="border: 1px solid #333; padding: 5px;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>Operacija: {{ operationListItem.getOperatingItemListType().name }}</b>
                                    </h7>
                                </td>
                                <td valign="middle" align="left" style="border: 1px solid #333; padding: 5px;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>Cena : {{ operationListItem.piecePrice }} </b>
                                    </h7>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="border: 1px solid #333; padding: 5px;">
                                    <b>Radni nalozi : {{ operationListItem.getoperatingList().getWorkOrders() }}</b>
                                </td>
                                <td align="left" style="border: 1px solid #333; padding: 5px;">
                                    <b>Velicine: {{ hydrator.getAllProductSizes() }}</b>
                                </td>
                            </tr>
                            <tr valign="middle">
                                <td valign="middle" align="left" style="border: 1px solid #333; padding: 5px;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>Model : {{ operationListItem.getOperatingList().getProduct().name }}</b>
                                    </h7>
                                </td>
                                <td valign="middle" align="left" style="border: 1px solid #333; padding: 5px;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>Sifra : {{ operationListItem.getOperatingList().getProduct().code }}</b>
                                    </h7>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="space-30"></div>
                        <table valign="middle" width="100%" cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr>
                                <td valign="middle" align="left" style="border: 1px solid #333; padding: 5px;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>Cena: </b>
                                    </h7>
                                </td>
                                <td valign="middle" align="left" style="border: 1px solid #333; padding: 5px;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>{{ operationListItem.piecePrice }} </b>
                                    </h7>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="border: 1px solid #333; padding: 5px;">
                                    <b>Norma za 1h :</b>
                                </td>
                                <td align="left" style="border: 1px solid #333; padding: 5px;">
                                    <b>{{ operationListItem.piecesHour }}</b>
                                </td>
                            </tr>

                            <tr valign="middle">
                                <td valign="middle" align="left" style="border: 1px solid #333; padding: 5px;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>Vreme za 1kom : </b>
                                    </h7>
                                </td>
                                <td valign="middle" align="left" style="border: 1px solid #333; padding: 5px;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>{{ operationListItem.pieceTime }}</b>
                                    </h7>
                                </td>
                            </tr>
                            <tr>

                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 pull-left">
                <input type="image" src="{{ operationListItem.getOperatingList().getProduct().getImagePrintPath() }}" width="100%" style="cursor: pointer; outline: none;"/>
            </div>

            <div class="col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="col-lg-12">
                        <div id="loaded-html" class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" >
                                <thead>
                                <tr>
                                    <td rowspan="3" class="text-center" style="padding-top: 50px">{{ t('Sifra Radnika') }}</td>
                                    <td rowspan="3" class="text-center" style="padding-top: 50px">{{ t('Datum') }}</td>
                                    <td rowspan="3" class="text-center" style="padding-top: 50px">{{ t('Kolicina') }}</td>
                                    <td rowspan="1" colspan="4" class="text-center">{{ t('Vreme') }}</td>
                                    <td rowspan="1" colspan="3" class="text-center">{{ t('Napomena') }}</td>
                                </tr>
                                <tr>
                                    <td rowspan="1" colspan="2" class="text-center">{{ t('Pocetak') }}</td>
                                    <td rowspan="1" colspan="2" class="text-center">{{ t('Zavrsetak') }}</td>
                                    <td rowspan="3" class="text-center" style="writing-mode: vertical-lr !important; max-width: 20%">{{  t('Odobr.') }}</td>
                                    <td rowspan="3" class="text-center" style="writing-mode: vertical-lr !important; max-width: 20%">{{  t('Preko.') }}</td>
                                    <td rowspan="3" class="text-center" style="writing-mode: vertical-lr !important; max-width: 20%">{{  t('Odsust.') }}</td>
                                </tr>
                                <tr>
                                    <td class="text-center">{{ t('Cas') }}</td>
                                    <td class="text-center">{{ t('Min') }}</td>
                                    <td class="text-center">{{ t('Cas') }}</td>
                                    <td class="text-center">{{ t('Min') }}</td>

                                </tr>
                                </thead>
                                <tbody>
                                {% for i in 2..20 %}
                                    <tr style="height: 25px">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                {% endfor %}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css" />
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>