<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-20"></div>
                <div class="row">
                    <div class="col-xs-10"></div>
                    <div class="col-xs-2 pull-right">
                        <div class="col-xs-2 ">
                            <a href="#" class="btn btn-success pull-right" onclick="getForm('/operating-list-items/find-by-barcode', '/operating-list-items/find-operating-item', event, '')">
                                <i class="fa fa-barcode"></i>
                                <span class="nav-label">{{ t('Unos ucinka') }}</span>
                            </a>
                        </div>
                        <div class="col-xs-2 pull-right">
                            <a href="#" class="btn btn-success pull-right"
                               onclick="getForm('/cutting-jobs/find-by-barcode', '/cutting-jobs/find-operating-item', event, '')">
                                <i class="fa fa-barcode"></i>
                                <span class="nav-label">{{ t('Unos rezijskog posla') }}</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="space-30"></div>
                    <div class="col-xs-9" >
                        <div class="ibox float-e-margins">
                            <div class="col-lg-12" >
                                <table class="table table-striped table-bordered table-hover">
                                    <tbody>
                                    <tr>
                                        <td align="left" style=" padding: 5px;">
                                            <h7>
                                                <b>Sektor : {{ operationListItem.getOperatingList().getCode() }}</b>
                                            </h7>
                                        </td>
                                        <td align="left" style=" padding: 5px;"  >
                                            <b  {{ finished.getTotalPerItem(operationListItem) !== total ? ' style="background: lightcoral !important;"':'' }}>Raspisano : {{ total }} || Zavrseno: {{ finished.getTotalPerItem(operationListItem) }}</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="middle" align="left" style=" padding: 5px;">
                                            <h7 style="margin: 0 2px 0 0;">
                                                <b>Operacija: {{ operationListItem.getOperatingItemListType().name }}</b>
                                            </h7>
                                        </td>
                                        <td valign="middle" align="left" style=" padding: 5px;">
                                            <h7 style="margin: 0 2px 0 0;">
                                                <b>Cena : {{ operationListItem.piecePrice }} </b>
                                            </h7>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style=" padding: 5px;">
                                            <b>Radni nalozi : {{ operationListItem.getoperatingList().getWorkOrders() }}</b>
                                        </td>
                                        <td align="left" style=" padding: 5px;">
                                            <b>Velicine: {{ hydrator.getAllProductSizes() }}</b>
                                        </td>
                                    </tr>
                                    <tr valign="middle">
                                        <td valign="middle" align="left" style=" padding: 5px;">
                                            <h7 style="margin: 0 2px 0 0;">
                                                <b>Model : {{ operationListItem.getOperatingList().getProduct().name }}</b>
                                            </h7>
                                        </td>
                                        <td valign="middle" align="left" style=" padding: 5px;">
                                            <h7 style="margin: 0 2px 0 0;">
                                                <b>Sifra : {{ operationListItem.getOperatingList().getProduct().code }}</b>
                                            </h7>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="space-30"></div>
                                <table valign="middle" width="100%" class="table table-striped table-bordered table-hover">
                                    <tbody>
                                    <tr>
                                        <td valign="middle" align="left" style=" padding: 5px;">
                                            <h7 style="margin: 0 2px 0 0;">
                                                <b>Cena: </b>
                                            </h7>
                                        </td>
                                        <td valign="middle" align="left" style=" padding: 5px;">
                                            <h7 style="margin: 0 2px 0 0;">
                                                <b>{{ operationListItem.piecePrice }} </b>
                                            </h7>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style=" padding: 5px;">
                                            <b>Vreme za 1kom :</b>
                                        </td>
                                        <td align="left" style=" padding: 5px;">
                                            <b>{{ operationListItem.pieceTime }}</b>
                                        </td>
                                    </tr>

                                    <tr valign="middle">
                                        <td valign="middle" align="left" style=" padding: 5px;">
                                            <h7 style="margin: 0 2px 0 0;">
                                                <b>Komada za 1h: </b>
                                            </h7>
                                        </td>
                                        <td valign="middle" align="left" style=" padding: 5px;">
                                            <h7 style="margin: 0 2px 0 0;">
                                                <b>{{ operationListItem.piecesHour }}</b>
                                            </h7>
                                        </td>
                                    </tr>
                                    <tr>

                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3 pull-left">
                        <div style="height: 280px !important;">
                            <input type="image" src="{{ operationListItem.getOperatingList().getProduct().getImagePath() }}" height="100%" style="cursor: pointer; outline: none;"/>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="space-30"></div>
                {% set counter = worksheets|length %}
                <div class="row">
                    <div class="col-md-12">
                        <form id="operationWorker" method="post" action="/operating-list-items/add-worksheet/{{ operationListItem.id }}" class="form-horizontal">
                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <thead>
                                    <tr>
                                        <td rowspan="3" class="text-center" style="padding-top: 50px;; width: 2%">{{ t('#') }}</td>
                                        <td rowspan="3" class="text-center" style="padding-top: 50px; width: 12%">{{ t('Sifra Radnika') }}</td>
                                        <td rowspan="3" class="text-center" style="padding-top: 50px; width: 14%">{{ t('Datum') }}</td>
                                        {#<td rowspan="3" class="text-center" style="padding-top: 50px; width: 12%">{{ t('Velicina') }}</td>#}
                                        <td rowspan="3" class="text-center" style="padding-top: 50px; width: 12%">{{ t('Kolicina') }}</td>
                                        <td rowspan="1" colspan="4" class="text-center col-lg-2">{{ t('Vreme') }}</td>
                                        <td rowspan="1" colspan="2" class="text-center col-lg-1" style="max-width: 50px">{{ t('Napomena') }}</td>
                                        {% if worksheets|length %}
                                            <td rowspan="3" colspan="4" class="text-center col-lg-1"></td>
                                        {% endif %}
                                    </tr>
                                    <tr>
                                        <td rowspan="1" colspan="2" class="text-center">{{ t('Pocetak') }}</td>
                                        <td rowspan="1" colspan="2" class="text-center">{{ t('Zavrsetak') }}</td>
                                        <td rowspan="3" class="text-center" style="writing-mode: vertical-lr !important; max-width: 20%">{{  t('Odbreno') }}</td>
                                        <td rowspan="3" class="text-center" style="writing-mode: vertical-lr !important; max-width: 20%">{{  t('Prekovr.') }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" style="width: 5%">{{ t('Cas') }}</td>
                                        <td class="text-center" style="width: 5%">{{ t('Min') }}</td>
                                        <td class="text-center" style="width: 5%">{{ t('Cas') }}</td>
                                        <td class="text-center" style="width: 5%">{{ t('Min') }}</td>
                                    </tr>
                                    <tr>
                                        {#<td></td>#}
                                    </tr>
                                    </thead>

                                </tr>
                            {% for key, worksheet in worksheets %}
                                {% set times = explode(':',worksheet.startTime) %}
                                {% for time in times %}
                                    {% set hour = time[0] %}
                                    {% set min = time[1] %}
                                {% endfor %}
                                <tr>
                                    <td class="text-center">
                                        <a class="product-tooltip text-danger" data-toggle="tooltip"
                                           title="<p>Ucinak :{{ worksheet.getTimeBetweenWorksheet() ? number_format((worksheet.qty*operationListItem.pieceTime)/worksheet.getTimeBetweenWorksheet()*100,1) : '0'}} %</p>
                                           <p>Vreme: {{ worksheet.getTimeBetweenWorksheet() }}</p>
                                           <p>Radnik: {{ worksheet.getProductionWorker().getDisplayName() }}</p>
                                           <p>Sektor: {{ worksheet.getProductionWorker().getCode().getDisplayName() }}</p>">
                                        <span>{{ key+1 }}</span>
                                        </a>
                                    </td>
                                    <td class="form-group">
                                        <select data-form="true" name="edit[{{ worksheet.id }}][productionWorkerId]" id="productionWorkerId" class="search form-control" disabled>
                                            <option value="0">-- Izaberite --</option>
                                            {% for worker in workers %}
                                                <option {{ worksheet.productionWorkerId == worker.id ? 'selected' : '' }} value="{{ worker.id }}">{{ worker.workerCode }}</option>
                                            {% endfor %}
                                        </select>
                                    </td>
                                    <td class="form-group">
                                        <input data-form="true" type="text" name="edit[{{ worksheet.id }}][date]" value="{{ worksheet.date.getShortSerbianFormat() }}" id="date" class="form-control" disabled/>
                                    </td>
                                    {#<td class="form-group">#}
                                        {#<select data-form="true" name="edit[{{ worksheet.id }}][codeId]" id="codeId" class="search form-control" disabled>#}
                                            {#<option value="0">-- Izaberite --</option>#}
                                            {#{% for code in codes %}#}
                                                {#<option {{ worksheet.codeId == code.id ? 'selected' : '' }} value="{{ code.id }}">{{ code.name }}</option>#}
                                            {#{% endfor %}#}
                                        {#</select>#}
                                    {#</td>#}
                                    <td class="form-group">
                                        <input data-form="true" type="text" name="edit[{{ worksheet.id }}][qty]" value="{{ worksheet.qty }}" id="qty" class="form-control" data-float="true" disabled/>
                                    </td>
                                    <td class="form-group">
                                        <input data-form="true" type="text" name="edit[{{ worksheet.id }}][startTime]" value="{{ worksheet.getWorksheetStartHour() }}" id="startTime" class="form-control" disabled/>
                                    </td>
                                    <td class="form-group">
                                        <input data-form="true" type="text" name="edit[{{ worksheet.id }}][startMin]" value="{{ worksheet.getWorksheetStartMin() }}" id="startMin" class="form-control" disabled/>
                                    </td>
                                    <td class="form-group">
                                        <input data-form="true" type="text" name="edit[{{ worksheet.id }}][endTime]" value="{{ worksheet.getWorksheetEndHour() }}" id="endTime" class="form-control" disabled/>
                                    </td>
                                    <td class="form-group">
                                        <input data-form="true" type="text" name="edit[{{ worksheet.id }}][endMin]" value="{{ worksheet.getWorksheetEndMin() }}" id="endMin" class="form-control" disabled/>
                                    </td>
                                    <td class="form-group text-center">
                                        <input data-from="true" type="checkbox" name="edit[][]" value="" id="approved" {{ worksheet.approved ? 'checked' : '' }} class="form-control checkbox-inline i-checks" disabled>
                                    </td>
                                    <td class="form-group text-center">
                                        <input data-from="true" type="checkbox" name="edit[][]" value="" id="overtime" {{ worksheet.overtime ? 'checked' : '' }} class="form-control checkbox-inline i-checks" disabled>
                                    </td>
                                    <td>
                                        <a href="#" onclick="getUpdateForm('operating-list-item-worksheets', {{ worksheet.id }}, event)" class="bigger-140 text-danger">
                                            <button class="btneport-work-time btn-success btn-circle" type="button" title="Izmena">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </a>
                                        <a href="#" onclick="getDeleteForm('operating-list-item-worksheets', {{ worksheet.id }}, event)" class="bigger-140 text-danger">
                                            <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            {% endfor %}
                            {% for i in counter..24 %}
                                <tr>
                                    <td class="text-center"><span>{{ i + 1 }}</span></td>
                                    <td class="form-group">
                                        <select data-form="true" name="create[{{ i }}][productionWorkerId]" id="productionWorkerId" class="search form-control">
                                            <option value="0">-- Izaberite --</option>
                                            {% for worker in workers %}
                                                <option value="{{ worker.id }}">{{ worker.workerCode }}</option>
                                            {% endfor %}
                                        </select>
                                    </td>
                                    <td class="form-group">
                                        <input data-form="true" type="text" name="create[{{ i }}][date]" id="date" class="form-control datepicker"/>
                                    </td>
                                    {#<td class="form-group">#}
                                        {#<select data-form="uetr" name="create[{{ i }}][codeId]" id="codeId" class="search form-control">#}
                                            {#<option value="0">-- Izaberite --</option>#}
                                            {#{% for code in codes %}#}
                                            {#<option value="{{ code.id }}">{{ code.name }}</option>#}
                                            {#{% endfor %}#}
                                        {#</select>#}
                                    {#</td>#}
                                    <td class="form-group">
                                        <input data-form="true" type="text" name="create[{{ i }}][qty]" id="qty" value="" data-float="true" class="form-control"/>
                                    </td>
                                    <td class="form-group">
                                        <input type="text" name="create[{{ i }}][startHour]" id="startHour" value="" class="form-control"/>
                                    </td>
                                    <td class="form-group">
                                        <input type="text" name="create[{{ i }}][startMin]" id="startMin" value="" class="form-control"/>
                                    </td>
                                    <td class="form-group">
                                        <input type="text" name="create[{{ i }}][endHour]" id="endHour" value="" class="form-control"/>
                                    </td>
                                    <td class="form-group">
                                        <input type="text" name="create[{{ i }}][endMin]" id="endMin" value="" class="form-control"/>
                                    </td>
                                    <td class="form-group text-center" valign="baseline">
                                        <input type="checkbox" name="create[{{ i }}][approved]" id="approved" value="1" class="form-control checkbox-inline i-checks"/>
                                    </td>
                                    <td class="form-group text-center" valign="baseline">
                                        <input type="checkbox" name="create[{{ i }}][overtime]" id="overtime" value="1" class="form-control checkbox-inline i-checks"/>
                                    </td>
                                    {% if worksheets|length %}
                                        <td></td>
                                    {% endif %}
                                </tr>
                            {% endfor %}
                            </table>
                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-save"></i> Sačuvaj
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>