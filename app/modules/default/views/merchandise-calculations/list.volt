<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj</h3>
                            <a href="#" onclick="getCreateForm('merchandise-calculations', event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" >
                        <thead>
                        <tr>
                            <th>{{ t('Dokument') }}</th>
                            <th>{{ t('Mis Dokument') }}</th>
                            <th>{{ t('Type') }}</th>
                            <th>{{ t('Akcija') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for merchandiseCalculation in merchandiseCalculations %}
                            <tr>
                                <td>{{ merchandiseCalculation.documentName }}</td>
                                <td>{{ merchandiseCalculation.misCalculation }}</td>
                                <td>{{ merchandiseCalculation.type }}</td>
                                <td class="text-center" width="11.5%">
                                    {% if merchandiseCalculation.type == 'PREPARED' %}
                                        <a class="btn btn-success btn-circle" onclick="getAjaxForm('merchandise-calculations/finish/{{ merchandiseCalculation.id }}', event)">
                                            <i class="fa fa-check"></i>
                                        </a>
                                    {% endif %}
                                    <a href="/merchandise-calculations/overview/{{ merchandiseCalculation.id }}" class="bigger-140 text-danger">
                                        <button class="btn btn-info btn-circle" type="button" title="Pregled">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </a>
                                    <a href="{{ merchandiseCalculation.getFilePath() }}" class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Download">
                                            <i class="fa fa-download"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getDeleteForm('merchandise-calculations', {{ merchandiseCalculation.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>