<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="text-center">
                        <h2>Kalkulacija : {{ merchandiseCalculation.documentName }}</h2>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" id="report">
                        <thead>
                        <tr>
                            <th>{{ t('Sifra') }}</th>
                            <th>{{ t('Naziv') }}</th>
                            <th>{{ t('Kurs') }}</th>
                            <th class="bg-green">{{ t('RSD Nabavna') }}</th>
                            <th class="bg-green">{{ t('RSD VP') }}</th>
                            <th class="bg-green">{{ t('RSD MP') }}</th>
                            <th class="bg-light">{{ t('EUR Nabavna') }}</th>
                            <th class="bg-light">{{ t('EUR VP') }}</th>
                            <th class="bg-light">{{ t('EUR MP') }}</th>
                            <th class="bg-red">{{ t('CG Nabavna') }}</th>
                            <th class="bg-red">{{ t('CG VP') }}</th>
                            <th class="bg-red">{{ t('CG MP') }}</th>
                            <th class="bg-info">{{ t('BIH Nabavna') }}</th>
                            <th class="bg-info">{{ t('BIH VP') }}</th>
                            <th class="bg-info">{{ t('BIH MP') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for data in datas %}
                            <tr>
                                <td>{{ data['code'] }}</td>
                                <td>{{ data['name'] }}</td>
                                <td>{{ data['course'] }}</td>
                                <td class="bg-green">{{ data['purchasePriceRSD'] }}</td>
                                <td class="bg-green">{{ data['wholesalePriceRSD'] }}</td>
                                <td class="bg-green">{{ data['retailPriceRSD'] }}</td>
                                <td class="bg-light">{{ data['purchasePriceEUR'] }}</td>
                                <td class="bg-light">{{ data['wholesalePriceEUR'] }}</td>
                                <td class="bg-light">{{ data['retailPriceEUR'] }}</td>
                                <td class="bg-red">{{ data['purchasePriceCG'] }}</td>
                                <td class="bg-red">{{ data['wholesalePriceCG'] }}</td>
                                <td class="bg-red">{{ data['retailPriceCG'] }}</td>
                                <td class="bg-info">{{ data['purchasePriceBIH'] }}</td>
                                <td class="bg-info">{{ data['wholesalePriceBIH'] }}</td>
                                <td class="bg-info">{{ data['retailPriceBIH'] }}</td>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready( function () {
        $('#report').DataTable({
            "pageLength": 50,
            fixedHeader: true,
            "ordering" : true
        });
    })
</script>