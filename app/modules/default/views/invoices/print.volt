<style>
    h1 {
        color: navy;
        font-family: times;
        font-size: 18pt;
        text-decoration: underline;
    }

    p.first {
        color: #003300;
        font-family: helvetica;
        font-size: 12pt;
    }

    p.first span {
        color: #006600;
        font-style: italic;
    }

    p#second {
        color: rgb(00, 63, 127);
        font-family: times;
        font-size: 12pt;
        text-align: justify;
    }

    p#second > span {
        background-color: #FFFFAA;
    }

    table {
        border-collapse: separate;
        width: 100%;
    }

    table th {
        padding: 8px 5px;
        text-align: right;
        border-bottom: 4px solid #487774;
        background: #E6E7E7;

    }

    table td {
        text-align: right;
        border-right: 1px solid #CCCCCF;
    }



    .currency {
        border-bottom: 4px solid #487774;
        padding: 0 20px;
    }

    .currency span {
        font-size: 11px;
        font-style: italic;
        color: #8b8b8b;
        display: inline-block;
        min-width: 20px;
    }


</style>
<img src="<?php echo APP_PATH; ?>public/img/memo.png" alt="">
<div>
    <table style="text-align: center !important;">
        <tr>
            <td style="border: 2px solid #487774; !important;text-align: center !important;">
                <br/>KIDS BEBA DOO
                <br/><span>Ignjata Joba 37</span>
                <br/><span>Belgrade Serbia</span>
                <br/><span>PIB: 101743849</span>
            </td>
            <td style="border: none !important"></td>
            <td style="border: 2px solid #487774; !important;text-align: center !important;">
                <br/>{{ invoice.getClient().invoiceName }}
                <br/><span>{{ invoice.getClient().address }}</span>
                <br/><span>{{ invoice.getClient().phone }}</span>
                <br/><span>VAT ID: {{ invoice.getClient().pib }}</span>
            </td>
        </tr>
    </table>
</div>
<div style="text-align: center;font-size: 18px">
    <span>Invoice : <b>{{ invoice.invoice }}</b> , Belgrade {{ invoice.date.getShortSerbianFormat() }}</span>
</div>

<div style="clear:both">
    <section id="items">

        <table cellpadding="0" cellspacing="0">

            <tr>
                <th width="5%">#</th> <!-- Dummy cell for the row number and row commands -->
                <th width="20%">SKU</th>
                <th width="30%">Name</th>
                <th width="5%">Size</th>
                <th width="20%">Model</th>
                <th width="5%">Qty</th>
                <th width="5%">Price</th>
                <th width="10%">Ammount</th>
            </tr>
            {% set total = 0 %}
            {% set totalQty = 0 %}
            {% set rbr = 1 %}
            {% for row in data %}
                <tr data-iterate="item" style="font-size: 12px !important;">
                    {% set ammount = row["qty"] * row["price"] %}
                    <td width="5%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ rbr }}</td> <!-- Dummy cell for the row number and row commands -->
                    <td width="20%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ row["code"] }}</td>
                    <td width="30%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ row["name"] }}</td>
                    <td width="5%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ row["size"] }}</td>
                    <td width="17%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ row["model"] }}</td>
                    <td width="5%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ row["qty"] }}</td>
                    <td width="8%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ row["price"] }} &euro;</td>
                    <td width="10%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ ammount }} &euro;</td>
                </tr>
                {% set rbr = rbr+1 %}
                {#{% set total = total + number_format(ammount,2) %}#}
                {% set totalQty = totalQty + row["qty"] %}
            {% endfor %}


        </table>

    </section>

    <div class="currency">
        <span></span> <span></span>
    </div>
    <div style="margin-bottom: 5px !important;">
        <table cellpadding="0" cellspacing="0" style="background: none !important">

            <tr style="background: none !important;border-bottom: 2px solid white; !important;">
                <td width="70%" style="text-align:left !important; padding-top: 5px !important;">
                    The goods are produced in Serbia in „KIDS BEBA“ D.O.O. Belgrade.
                </td>
                <td width="15%"
                    style="background-color: #8BA09E !important;border-bottom: 2px solid white; !important;">Total Qty
                </td>
                <td width="15%"
                    style="background-color: #8BA09E !important;border-bottom: 2px solid white; !important;">{{ totalQty }}</td>
            </tr>

            <tr style="background: none !important;border-bottom: 2px solid white; !important;">
                <td width="70%" style="text-align:left !important">
                    FREE OF CARGE PDV ACCORDING ITEM 24.1.2
                </td>
                <td width="15%" style="background-color: #8BA09E !important">Total Value</td>
                <td width="15%" style="background-color: #8BA09E !important">{{ invoice.amount }} &euro;</td>
            </tr>

        </table>
    </div>
    <br pagebreak="true" />
    <div>
        {% if invoice.statement  %}
        <table style="border: none !important">
            <tr>
                <td width="72%" style="text-align: left !important; border-right: none !important;">{{invoice.statement}}
                </td>
                <td width="28%" style="border-bottom: 2px solid black; !important; border-right: none !important;"></td>
            </tr>
            <tr>
                <td width="72%" style="border-right: none !important;"></td>
                <td width="28%" style="border-right: none !important; text-align: left !important;">Direktor: Ninoslav Perisic</td>
            </tr>
            <tr>
                <td width="72%" style="text-align: left !important; border-right: none !important;"></td>
                <td width="28%" style="border-right: none !important; text-align: left !important;">Belgrade, {{ invoice.date.getShortSerbianFormat() }}</td>
            </tr>
        </table>
        {% endif %}
    </div>
    <div style="font-size: 12px !important;">
        <table>
            <tr>
                <td style="border-right: none !important;text-align: left !important;">
                    <br/>PARITET: {{ invoice.custom }}
                    <br/><span>NR.COLETS: {{invoice.coletsQty}}</span>
                    <br/><span>NW: {{invoice.netoWeight}}KGR</span>
                    <br/><span>BW: {{invoice.brutoWeight}} KGR</span>
                    <br/><span> </span>
                    <br/><span> </span>
                </td>
                <td style="border-right: none !important"></td>
                <td style="border-right: none !important;text-align: left !important;">
                    <br/>BANK INSTRUCTION:
                    <br/><span>NAME OF BANK: CREDIT AGRICOLE SRBIJA,AD ,Novi Sad</span>
                    <br/><span>SWIFT CODE: MEBARS22</span>
                    <br/><span>IBAN RS35 3300 4705 0043 1150 61</span>
                    <br/><span>KIDS BEBA DOO</span>
                    <br/><span>IGNJATA JOBA 37</span>
                </td>
            </tr>
        </table>
    </div>
</div>
</div>