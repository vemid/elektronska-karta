<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css"/>
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css"/>
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
{#    <style>{{ file_get_contents(css1)}}</style>#}
{#    <style>{{ file_get_contents(css2)}}</style>#}
    <style>
        .page-break {
            page-break-after: always;
        }

        .page-breaker {
            clear: both;
            display: block;
            border: 1px solid transparent;
            page-break-before: always;
        }
    </style>
</head>
<body class="pace-done">
<img src="<?php echo APP_PATH; ?>public/img/memo.png" alt="">
<div class="row">

    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-1"></div>
                    <div class="col-xs-4">
                        <div style="border-style: double;" class="text-center">
                            <p>KIDS BEBA DOO BEOGRAD</p>
                            <p>Ignajta Joba 37</p>
                            <p>Belgrad, Serbia</p>
                            <p>PIB: 101743849</p>
                        </div>
                    </div>
                    <div class="col-xs-2"></div>
                    <div class="col-xs-4">
                        <div style="border-style: solid;" class="text-center">
                            <p>{{ invoice.getClient().name }}</p>
                            <p>{{ invoice.getClient().address }}</p>
                            <p>Okresný súd Trenčín, oddiel: Sro, vložka č. 31590/R</p>
                            <p>VAT ID: {{ invoice.getClient().pib }}</p>
                        </div>
                    </div>
                    <div class="col-xs-1"></div>
                </div>
                <div class="row">
                    <div class="space-20"></div>
                    <div class="col-xs-1"></div>
                    <div class="col-xs-9" style="font-size: 18px">
                        <p>Invoice Number : {{ invoice.invoice }} , Belgrade {{ invoice.date }}</p>
                    </div>
                    <div class="col-xs-1"></div>
                    <div class="space-20"></div>
                    <div class="space-20"></div>
                </div>
                <div class="space-20"></div>
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table class="table table-bordered" style="font-size: 14px">
                            <thead>
                            <tr style="font-size: 12px !important;" class="text-center">
                                <th style="width: 10px !important">{{ t('Rbr') }}</th>
                                <th style="width: 30px">{{ t('Code') }}</th>
                                <th style="width: 100px">{{ t('Name') }}</th>
                                <th style="width: 30px">{{ t('Size') }}</th>
                                <th style="width: 30px">{{ t('Model') }}</th>
                                <th style="width: 20px">{{ t('Pcs') }}</th>
                                <th style="width: 20px">{{ t('Price') }}</th>
                                <th style="width: 20px">{{ t('Ammount') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% set total = 0 %}
                            {% set totalQty = 0 %}
                            {% set rbr = 1 %}
                            {% for row in data %}
                                <tr>
                                    {% set ammount = row["qty"] * row["price"] %}
                                    <td style="padding: 2px !important">{{ rbr }}</td>
                                    <td>{{ row["code"] }}</td>
                                    <td style="padding: 2px !important">{{ row["name"] }}</td>
                                    <td style="padding: 2px !important">{{ row["size"] }}</td>
                                    <td style="padding: 2px !important">{{ row["model"] }}</td>
                                    <td style="padding: 2px !important">{{ row["qty"] }}</td>
                                    <td style="padding: 2px !important">{{ row["price"] }} &euro;</td>
                                    <td style="padding: 2px !important">{{ ammount }} &euro;</td>
                                </tr>
                                {% set rbr = rbr+1 %}
                                {% set total = total + number_format(ammount,2) %}
                                {% set totalQty = totalQty + row["qty"] %}
                            {% endfor %}
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Total :</td>
                                <td>{{ totalQty }}</td>
                                <td></td>
                                <td>{{ total }} &euro;</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="space-20"></div>
                    <div style="margin: 30px">
                        <p>The goods are produced in Serbia in „KIDS BEBA“ D.O.O. Belgrade.</p>
                        <p>FREE OF CARGE PDV ACCORDING ITEM 24.1.2</p>
                        <div class="space-20"></div>
                        <div class="col-xs-8">
                            <p>The exporter of the products covered by this document declears that, except where otherwise clearly indicated,these products are of Serbian preferential origin.</p>
                        </div>
                        <div class="col-xs-4 text-center">
                            <br>
                            <br>
                            <br>
                            <p>_________________________</p>
                            <p>Ninoslav Perisic</p>
                        </div>
                    </div>
                </div>
                <div class="space-20"></div>
                <div style="width: 50%">
                    <span>Packet information:</span><br>
                    <p>PARITET: FCA BEOGRAD</p>
                    <p>NR.COLETS: 5</p>
                    <p>NW: 62.50KGR</p>
                    <p>BW: 67.50 KGR</p>
                    <p></p>
                </div>
                <div style="width: 50%">
                    <span>BANK INSTRUCTION</span><br>
                    <p>NAME OF BANK: CREDIT AGRICOLE SRBIJA,AD ,Novi Sad</p>
                    <p>SWIFT CODE: MEBARS22</p>
                    <p>IBAN RS35 3300 4705 0043 1150 61</p>
                    <p>KIDS BEBA DOO</p>
                    <p>IGNJATA JOBA 37, BEOGRAD</p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css"/>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>