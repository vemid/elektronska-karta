<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" >
                        <thead>
                        <tr>
                            <th>{{ t('Code') }}</th>
                            <th>{{ t('Client') }}</th>
                            <th>{{ t('Invoice') }}</th>
                            <th>{{ t('Date') }}</th>
                            <th>{{ t('Amount') }}</th>
                            <th>{{ t('Action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for invoice in invoices %}
                            {% set klasa = '' %}
                            {% if invoice.getStatus() == 0 %}
                            {% set klasa = 'danger' %}
                            {% endif %}

                            <tr class="{{ klasa }}">
                                <td>{{ invoice.client.code }}</td>
                                <td>{{ invoice.client.invoiceName }}</td>
                                <td>{{ invoice.invoice }}</td>
                                <td>{{ invoice.date }}</td>
                                <td>{{ invoice.amount }}</td>
                                <td class="text-center" style="width:15% !important">
{#                                    <a href="#" onclick="getUpdateForm('clients', {{ invoice.id }}, event)" class="bigger-140 text-danger">#}
{#                                        <button class="btn btn-success btn-circle" type="button" title="Izmena">#}
{#                                            <i class="fa fa-search"></i>#}
{#                                        </button>#}
{#                                    </a>#}
                                    <a href="/invoices/download/{{ invoice.id }}" class="bigger-140 text-danger">
                                        <button class="btn btn-info btn-circle" type="button" title="Download Product Information">
                                            <i class="far fa-file-excel"></i>
                                        </button>
                                    </a>
                                    <a href="/invoices/download-images/{{ invoice.id }}" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Download Images">
                                            <i class="far fa-file-archive"></i>
                                        </button>
                                    </a>
                                    <a href="/invoices/print/{{ invoice.id }}" class="bigger-140 text-success">
                                        <button class="btn btn-white btn-circle" type="button" title="Print invoice">
                                            <i class="fa fa-print"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>