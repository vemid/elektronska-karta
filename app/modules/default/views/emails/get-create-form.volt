<form id="email-template-save" action="/emails/add-notification-template" method="post">
<div class="row">
    <div class="col-xs-8">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="text-center">{{ t('Podešavanje emailova') }}</h3>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                    <input type="hidden" name="body" id="body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="froala-editor" class="clearfix"></div>
                        </div>
                    </div>
                    <div class="space-30"></div>
                    <div class="row">
                        <div class="form-horizontal">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label  class="col-xs-3 control-label text-left flash-notification no-padding-right no-margin-right" for="">Subject: <span>*</span></label>
                                    <div class="col-xs-9">
                                        <input type="text" id="subject" name="subject" class="form-inline form-control required">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label  class="col-xs-3 control-label text-left flash-notification no-padding-right no-margin-right" for="">Pošiljalac: <span>*</span></label>
                                    <div class="col-xs-9">
                                        <input type="email" id="fromAddress" name="fromAddress" class="form-inline form-control required">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-horizontal">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label flash-notification no-padding-right no-margin-right" for="">Kontroler: <span>*</span></label>
                                    <div class="col-sm-9">
                                        <select data-form="true" name="controllerName" id="controller" class="search form-control form-inline required">
                                            <option value="0">-- Izaberite --</option>
                                            {% for controller in controllers %}
                                                <option  value="{{ controller }}">{{ controller }}</option>
                                            {% endfor %}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label  class="col-sm-3 control-label flash-notification no-padding-right no-margin-right" for="">Akcija: <span>*</span></label>
                                    <div class="col-sm-9">
                                        <select data-form="true" name="actionName" id="action" class="search form-control form-inline required">
                                            <option value="0">-- Izaberite --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-horizontal">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label  class="col-xs-3 control-label text-left flash-notification no-padding-right no-margin-right" for="">Korisnici: <span>*</span></label>
                                    <div class="col-xs-9">
                                        <select name="users[]" id="users" multiple class="search form-inline form-control required">
                                            {% for user in users %}
                                                <option value="{{ user.id }}">{{ user.getDisplayName() }}</option>
                                            {% endfor %}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label  class="col-xs-3 control-label text-left flash-notification no-padding-right no-margin-right" for="">Odlozi: </label>
                                    <div class="col-xs-9">
                                        <select name="prolongOption" id="prolongOption" class="search form-inline form-control">
                                            <option value="">{{ t('-- Izaberi --') }}</option>
                                            <option value="ONE_HOUR">{{ t('1 Hour') }}</option>
                                            <option value="TWO_HOURS">{{ t('2 Hours') }}</option>
                                            <option value="SIX_HOURS">{{ t('6 Hours') }}</option>
                                            <option value="TWELVE_HOURS">{{ t('12 Hours') }}</option>
                                            <option value="TWENTY_FOUR_HOURS">{{ t('24 Hours') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space-30"></div>
                    <div class="space-30"></div>
                    <div class="row">
                        <div class="form-horizontal">
                            <div class="col-xs-12">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-lg" style="width:100%">
                                        <i class="fa fa-save"></i> {{ t('Save') }}
                                    </button>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <div class="col-xs-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="text-center">{{ t('Podaci iz modela') }}</h3>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12 form-horizontal">
                        <select name="models" id="models" class="search form-inline form-control">
                            <option value="0">{{ t('-- Izaberite --') }}</option>
                            {% for key, model in models %}
                                <option value="{{ key }}">{{ model }}</option>
                            {% endfor %}
                        </select>
                        <div class="space-15"></div>
                        <input type="hidden" id="entityObjectName" name="entityObjectName">
                        <div class="jumbotron table-bordered no-padding pull-left width-100" style="background: #fff; width:100%; min-height: 130px; padding: 10px!important;">
                            <ul class="tag-list pull-left no-padding no-margins">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
            </div>
        </div>
    </div>
</div>
</form>

<script async defer src="https://buttons.github.io/buttons.js"></script>