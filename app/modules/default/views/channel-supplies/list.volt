<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj</h3>
                            <a href="#" onclick="getCreateForm('channel-supplies', event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" >
                        <thead>
                        <tr>
                            <th>{{ t('Kanal nabdevanja') }}</th>
                            <th>{{ t('Tip Snabdevanja') }}</th>
                            <th>{{ t('Email') }}</th>
                            <th>{{ t('Akcije') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for channelSupply in channelSupplies %}
                            <tr>
                                <td>{{ channelSupply.name }}</td>
                                <td>{{ channelSupply.type }}</td>
                                <td>{{ channelSupply.email }}</td>
                                <td class="text-center" width="11.5%">
                                    <a href="/channel-supply-entities/list/{{ channelSupply.id }}"  class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Pregled">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getUpdateForm('channel-supplies', {{ channelSupply.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getDeleteForm('channel-supplies', {{ channelSupply.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>