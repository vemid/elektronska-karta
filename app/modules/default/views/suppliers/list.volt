<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj</h3>
                            <a href="#" onclick="getCreateForm('suppliers', event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" >
                        <thead>
                        <tr>
                            <th>{{ t('Dobavljač') }}</th>
                            <th>{{ t('Šifra dobavljača') }}</th>
                            <th>{{ t('Kontakt osoba') }}</th>
                            <th>{{ t('Telefon') }}</th>
                            <th>{{ t('Email') }}</th>
                            <th>{{ t('Adresa') }}</th>
                            <th>{{ t('Akcije') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for supplier in suppliers %}
                            <tr>
                                <td>{{ supplier }}</td>
                                <td>{{ supplier.code }}</td>
                                <td>{{ supplier.contactPerson }}</td>
                                <td>{{ supplier.phoneNumber }}</td>
                                <td>{{ supplier.email }}</td>
                                <td>{{ supplier.address }}</td>
                                <td class="text-center" width="11.5%">
                                    <a href="#" onclick="getUpdateForm('suppliers', {{ supplier.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getDeleteForm('suppliers', {{ supplier.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>