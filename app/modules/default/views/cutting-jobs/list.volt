<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj tip rezije</h3>
                            <a href="#" onclick="getCreateForm('cutting-jobs',event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" >
                        <thead>
                        <tr>
                            <th>{{ t('Naziv') }}</th>
                            <th>{{ t('Tip') }}</th>
                            <th>{{ t('Barcode') }}</th>
                            <th>{{ t('Akcija') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for cuttingJob in cuttingJobs %}
                            <tr>
                                <td>{{ cuttingJob.name }}</td>
                                <td>{{ cuttingJob.type }}</td>
                                <td>{{ cuttingJob.barcode }}</td>
                                <td class="text-center" width="11.5%">
                                    <a href="/cutting-jobs/overview/{{ cuttingJob.id }}" class="bigger-140 text-danger">
                                        <button class="btn btn-info btn-circle" type="button" title="Profil">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getUpdateForm('cutting-jobs', {{ cuttingJob .id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                    <a href="/cutting-jobs/print/{{ cuttingJob.id }}"
                                       target="_blank" class="bigger-140 text-danger">
                                        <button class="btn btn-info btn-circle" type="button"
                                                title="Stampa"><i class="fa fa-print"></i></button>
                                    </a>
                                    <a href="#" onclick="getDeleteForm('cutting-jobs', {{ cuttingJob .id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>