<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="form-horizontal">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Sektor:</label>
                                <div class="col-sm-7">
                                    <select class="search form-control" name="codeId" id="codeId">
                                        <option value="">-- Izaberi --</option>
                                        {% for code in sectors %}
                                            <option value="{{ code.id }}">{{ code.name }}</option>
                                        {% endfor %}
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Datum od:</label>
                                <div class="col-sm-7">
                                    <input type="text" value="{{ date }}" class="datepicker form-control" name="dateFrom" id="dateFrom"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Datum do:</label>
                                <div class="col-sm-7">
                                    <input type="text" value="{{ date }}" class="datepicker form-control" name="dateTo" id="dateTo"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="space-25"></div>
                <div class="table-responsive dataTable">
                    <table id="example" class="table table-striped table-bordered table-hover dataTable">
                        <thead>
                        <tr>
                            <th class="col-xs-4">{{ t('Radnik') }}</th>
                            <th class="col-xs-2">{{ t('Datum') }}</th>
                            <th class="col-xs-2">{{ t('Količina') }}</th>
                            <th class="text-center col-xs-3">{{ t('Zarada') }}</th>
                            <th class="text-center col-xs-1">{{ t('Akcija') }}</th>
                        </thead>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var table = $("#example").DataTable( {
            "processing": true,
            "serverSide": true,
            "pageLength": 50,
            "ajax": {
                "url": "/cutting-jobs/get-report-by-sector-worker-data",
                "data":  function ( d ) {
                    return $.extend({}, d, {
                        "codeId": $('#codeId').val(),
                        "dateTo": $('#dateTo').val(),
                        "dateFrom": $('#dateFrom').val()
                    });
                }
            },
            "columns": [
                null,
                null,
                null,
                { className: "text-center" },
                { className: "col-xs-1" },
            ],
            "columnDefs": [
                {
                    "render": function ( data, type, row ) {
                        return '<a href="/production-workers/list-by-worker-data?workerId='+ row[4] +'&dateTo='+ row[1] +'&dateFrom='+ row[1] +'#tabCuttingJobs" target="_blank" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search"></i></button></a> ' ;
                    },
                    "width" : "10px",
                    "targets": 4
                },
            ]
        });

        $("#codeId, #dateTo, #dateFrom").change( function() {
            table.draw();
        } );
    });


</script>