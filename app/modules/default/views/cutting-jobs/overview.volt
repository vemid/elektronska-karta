<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-20"></div>
                <div class="row">
                    <div class="col-xs-10">
                        <h2 class="text-center">Rezija : {{ cuttingJob.name }}</h2></div>
                    <div class="col-xs-2 pull-right">
                        <div class="col-xs-2 ">
                            <a href="#" class="btn btn-success pull-right" onclick="getForm('/operating-list-items/find-by-barcode', '/operating-list-items/find-operating-item', event, '')">
                                <i class="fa fa-barcode"></i>
                                <span class="nav-label">{{ t('Unos ucinka') }}</span>
                            </a>
                        </div>
                        <div class="col-xs-2 pull-right">
                            <a href="#" class="btn btn-success pull-right"
                               onclick="getForm('/cutting-jobs/find-by-barcode', '/cutting-jobs/find-operating-item', event, '')">
                                <i class="fa fa-barcode"></i>
                                <span class="nav-label">{{ t('Unos rezijskog posla') }}</span>
                            </a>
                        </div>
                    </div>
                </div>
                <form id="operationWorker" method="post"
                      action="/cutting-jobs/add-cutting-job-worksheet/{{ cuttingJob.id }}" class="form-horizontal">
                    <div class="space-30"></div>
                    <div class="space-30"></div>
                    {% set counter = cuttingWorksheets|length %}
                    <div class="row">
                        <div class="col-md-12">
                            {#<form id="operationWorker" method="post" action="/operating-list-items/add-cuttingWorksheet/{{ operationListItem.id }}" class="form-horizontal">#}
                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <thead>
                                    <tr>
                                        <td rowspan="3" class="text-center"
                                            style="padding-top: 50px;; width: 2%">{{ t('#') }}</td>
                                        <td rowspan="3" class="text-center"
                                            style="padding-top: 50px; width: 12%">{{ t('Sifra Managera') }}</td>
                                        <td rowspan="3" class="text-center"
                                            style="padding-top: 50px; width: 12%">{{ t('Sifra Radnika') }}</td>
                                        <td rowspan="3" class="text-center"
                                            style="padding-top: 50px; width: 14%">{{ t('Datum') }}</td>
                                        {#<td rowspan="3" class="text-center" style="padding-top: 50px; width: 12%">{{ t('Velicina') }}</td>#}
                                        <td rowspan="3" class="text-center"
                                            style="padding-top: 50px; width: 12%">{{ t('Kolicina') }}</td>
                                        <td rowspan="1" colspan="4" class="text-center col-lg-2">{{ t('Vreme') }}</td>
                                        {#{% if cuttingWorksheets|length %}#}
                                            {#<td rowspan="3" colspan="4" class="text-center col-lg-1"></td>#}
                                        {#{% endif %}#}
                                    </tr>
                                    <tr>
                                        <td rowspan="1" colspan="2" class="text-center">{{ t('Pocetak') }}</td>
                                        <td rowspan="1" colspan="2" class="text-center">{{ t('Zavrsetak') }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" style="width: 5%">{{ t('Cas') }}</td>
                                        <td class="text-center" style="width: 5%">{{ t('Min') }}</td>
                                        <td class="text-center" style="width: 5%">{{ t('Cas') }}</td>
                                        <td class="text-center" style="width: 5%">{{ t('Min') }}</td>
                                    </tr>
                                    {#<tr>#}
                                        {#<td></td>#}
                                    {#</tr>#}
                                    </thead>

                                </tr>
                                {#{% for key, cuttingWorksheet in cuttingWorksheets %}#}
                                    {#{% set times = explode(':',cuttingWorksheet.startTime) %}#}
                                    {#{% for time in times %}#}
                                        {#{% set hour = time[0] %}#}
                                        {#{% set min = time[1] %}#}
                                    {#{% endfor %}#}
                                    {#<tr>#}
                                        {#<td class="text-center"><span>{{ key + 1 }}</span></td>#}
                                        {#<td class="form-group">#}
                                            {#<select data-form="true" name="edit[{{ cuttingWorksheet.id }}][productionWorkerId]"#}
                                                    {#id="productionWorkerId" class="search form-control" disabled>#}
                                                {#<option value="0">-- Izaberite --</option>#}
                                                {#{% for worker in workers %}#}
                                                    {#<option {{ cuttingWorksheet.productionWorkerId == worker.id ? 'selected' : '' }}#}
                                                            {#value="{{ worker.id }}">{{ worker.workerCode }}</option>#}
                                                {#{% endfor %}#}
                                            {#</select>#}
                                        {#</td>#}
                                        {#<td class="form-group">#}
                                            {#<input data-form="true" type="text" name="edit[{{ cuttingWorksheet.id }}][date]"#}
                                                   {#value="{{ cuttingWorksheet.date.getShortSerbianFormat() }}" id="date"#}
                                                   {#class="form-control" disabled/>#}
                                        {#</td>#}
                                        {#<td class="form-group">#}
                                            {#<input data-form="true" type="text" name="edit[{{ cuttingWorksheet.id }}][qty]"#}
                                                   {#value="{{ cuttingWorksheet.qty }}" id="qty" class="form-control"#}
                                                   {#data-float="true" disabled/>#}
                                        {#</td>#}
                                        {#<td class="form-group">#}
                                            {#<input data-form="true" type="text"#}
                                                   {#name="edit[{{ cuttingWorksheet.id }}][startTime]"#}
                                                   {#value="{{ cuttingWorksheet.getWorksheetStartHour() }}" id="startTime"#}
                                                   {#class="form-control" disabled/>#}
                                        {#</td>#}
                                        {#<td class="form-group">#}
                                            {#<input data-form="true" type="text"#}
                                                   {#name="edit[{{ cuttingWorksheet.id }}][startMin]"#}
                                                   {#value="{{ cuttingWorksheet.getWorksheetStartMin() }}" id="startMin"#}
                                                   {#class="form-control" disabled/>#}
                                        {#</td>#}
                                        {#<td class="form-group">#}
                                            {#<input data-form="true" type="text" name="edit[{{ cuttingWorksheet.id }}][endTime]"#}
                                                   {#value="{{ cuttingWorksheet.getWorksheetEndHour() }}" id="endTime"#}
                                                   {#class="form-control" disabled/>#}
                                        {#</td>#}
                                        {#<td class="form-group">#}
                                            {#<input data-form="true" type="text" name="edit[{{ cuttingWorksheet.id }}][endMin]"#}
                                                   {#value="{{ cuttingWorksheet.getTimeBetweenWorksheet() }}" id="endMin"#}
                                                   {#class="form-control" disabled/>#}
                                        {#</td>#}
                                        {#<td>#}
                                            {#<a href="#"#}
                                               {#onclick="getUpdateForm('operating-list-item-cuttingWorksheets', {{ cuttingWorksheet.id }}, event)"#}
                                               {#class="bigger-140 text-danger">#}
                                                {#<button class="btn btn-success btn-circle" type="button" title="Izmena">#}
                                                    {#<i class="fa fa-edit"></i>#}
                                                {#</button>#}
                                            {#</a>#}
                                            {#<a href="#"#}
                                               {#onclick="getDeleteForm('job', {{ cuttingWorksheet.id }}, event)"#}
                                               {#class="bigger-140 text-danger">#}
                                                {#<button class="btn btn-danger btn-circle" type="button"#}
                                                        {#title="Brisanje">#}
                                                    {#<i class="fa fa-times"></i>#}
                                                {#</button>#}
                                            {#</a>#}
                                        {#</td>#}
                                    {#</tr>#}
                                {#{% endfor %}#}
                                {% for i in counter..9 %}
                                    <tr>
                                        <td class="text-center"><span>{{ i + 1 }}</span></td>
                                        <td class="form-group">
                                            <select data-form="true" name="create[{{ i }}][managerId]"
                                                    id="managerId" class="search form-control">
                                                <option value="0">-- Izaberite --</option>
                                                {% for worker in workers %}
                                                    <option value="{{ worker.id }}">{{ worker.workerCode }}</option>
                                                {% endfor %}
                                            </select>
                                        </td>
                                        <td class="form-group">
                                            <select data-form="true" name="create[{{ i }}][productionWorkerId]"
                                                    id="productionWorkerId" class="search form-control">
                                                <option value="0">-- Izaberite --</option>
                                                {% for worker in workers %}
                                                    <option value="{{ worker.id }}">{{ worker.workerCode }}</option>
                                                {% endfor %}
                                            </select>
                                        </td>
                                        <td class="form-group">
                                            <input data-form="true" type="text" name="create[{{ i }}][date]" id="date"
                                                   class="form-control datepicker"/>
                                        </td>
                                        <td class="form-group">
                                            <input data-form="true" type="text" name="create[{{ i }}][qty]" id="qty"
                                                   value="" data-float="true" class="form-control"/>
                                        </td>
                                        <td class="form-group">
                                            <input type="text" name="create[{{ i }}][startHour]" id="startHour" value=""
                                                   class="form-control"/>
                                        </td>
                                        <td class="form-group">
                                            <input type="text" name="create[{{ i }}][startMin]" id="startMin" value=""
                                                   class="form-control"/>
                                        </td>
                                        <td class="form-group">
                                            <input type="text" name="create[{{ i }}][endHour]" id="endHour" value=""
                                                   class="form-control"/>
                                        </td>
                                        <td class="form-group">
                                            <input type="text" name="create[{{ i }}][endMin]" id="endMin" value=""
                                                   class="form-control"/>
                                        </td>
                                    </tr>
                                {% endfor %}
                            </table>
                            <div class="row">
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-save"></i> Sačuvaj
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>