<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div id="loaded-html">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ t('Naziv') }}</th>
                                <th>{{ t('Vreme') }}</th>
                                <th>{{ t('Vrednost RSD') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for data in datas %}
                                <tr>
                                    <td><a href="/operating-list-items/overview/" target="_blank"> {{ data['name'] }}</a></td>
                                    <td>{{ number_format(data['total'],1) }}</td>
                                    <td>{{ number_format(data['money'],0) }}</td>
                                </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>