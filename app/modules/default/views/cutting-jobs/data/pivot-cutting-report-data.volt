<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div id="loaded-html">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="pivot"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let rows = JSON.parse('{{ rows }}');

    let dimensions = [
        {value: 'year', title: 'Godina'},
        {value: 'month', title: 'Mesec'},
        {value: 'sector', title: 'Sektor'},
        {value: 'worker', title: 'Radnik'},
        {value: 'manager', title: 'Menadzer'},
        {value: 'cuttingJob', title: 'Rezija'},
    ];

    let reduce = function(row, memo) {

        //memo.total = memo.total < parseFloat(row.transaction.total) ? memo.total : parseFloat(row.transaction.total);
        // memo.total = (memo.total || 0) + parseFloat(row.transaction.total);
        memo.quantity = (memo.quantity || 0) + parseInt(row.transaction.quantity);
        memo.totalValue = (memo.totalValue || 0) + parseFloat(row.transaction.totalValue);
        //memo.planedTime = (memo.planedTime || 0) + parseFloat(row.transaction.planedTime);
        //memo.outer = (parseFloat(row.transaction.realizedTime))/(parseFloat(row.transaction.planedTime));

        return memo
    };

    let calculations = [
        {
            title: 'Kolicina', value: 'quantity',
            template: function(val, row) {
                return  val.toFixed(0)
            }
        },
        {
            title: 'Vrednost', value: 'totalValue',
            template: function(val, row) {
                return val.toFixed(1) + ' rsd'
            }
        },

    ];
    ReactPivot(document.getElementById("pivot"), {
        rows: rows,
        dimensions: dimensions,
        calculations: calculations,
        reduce: reduce,
        activeDimensions: ['Mesec'],
        tableClassName : 'table table-striped table-bordered table-hover',
        nPaginateRows : 200
    })
</script>