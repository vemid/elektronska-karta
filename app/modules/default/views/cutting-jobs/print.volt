<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css" />
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css" />
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
</head>
<body class="pace-done">
<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-20"></div>
                <div class="row">
                    <div class="col-xs-10">
                        <h2 class="text-center">Rezija : {{ cuttingJob.name }}</h2>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="col-lg-12">
                        <table valign="middle" width="100%" cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr>
                                <td align="center" style="border: 1px solid #333; padding: 5px;">
                                    <img src="data:image/png;base64,{{ base64_encode(generatorHtml.getBarcode(cuttingJob.barcode, barcodeType)) }}">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="space-30"></div>
                        <table valign="middle" width="100%" cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr>
                                <td valign="middle" align="left" style="border: 1px solid #333; padding: 5px;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>Cena: </b>
                                    </h7>
                                </td>
                                <td valign="middle" align="left" style="border: 1px solid #333; padding: 5px;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>{{ cuttingJob.piecePrice }} </b>
                                    </h7>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="border: 1px solid #333; padding: 5px;">
                                    <b>Norma za 1h :</b>
                                </td>
                                <td align="left" style="border: 1px solid #333; padding: 5px;">
                                    <b>{{ cuttingJob.piecesHour }}</b>
                                </td>
                            </tr>

                            <tr valign="middle">
                                <td valign="middle" align="left" style="border: 1px solid #333; padding: 5px;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>Vreme za 1kom : </b>
                                    </h7>
                                </td>
                                <td valign="middle" align="left" style="border: 1px solid #333; padding: 5px;">
                                    <h7 style="margin: 0 2px 0 0;">
                                        <b>{{ cuttingJob.pieceTime }}</b>
                                    </h7>
                                </td>
                            </tr>
                            <tr>

                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="ibox float-e-margins">
                        <div class="col-lg-12">
                            <div id="loaded-html" class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" >
                                    <thead>
                                    <tr>
                                        <td rowspan="3" class="text-center" style="padding-top: 50px">{{ t('Sifra Poslovodje') }}</td>
                                        <td rowspan="3" class="text-center" style="padding-top: 50px">{{ t('Sifra Radnika') }}</td>
                                        <td rowspan="3" class="text-center" style="padding-top: 50px">{{ t('Datum') }}</td>
                                        <td rowspan="3" class="text-center" style="padding-top: 50px">{{ t('Kolicina') }}</td>
                                        <td rowspan="1" colspan="4" class="text-center">{{ t('Vreme') }}</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" colspan="2" class="text-center">{{ t('Pocetak') }}</td>
                                        <td rowspan="1" colspan="2" class="text-center">{{ t('Zavrsetak') }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">{{ t('Cas') }}</td>
                                        <td class="text-center">{{ t('Min') }}</td>
                                        <td class="text-center">{{ t('Cas') }}</td>
                                        <td class="text-center">{{ t('Min') }}</td>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    {% for i in 2..20 %}
                                        <tr style="height: 25px">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    {% endfor %}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css" />
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>