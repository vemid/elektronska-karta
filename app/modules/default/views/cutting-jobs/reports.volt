<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="form-horizontal">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Sektor:</label>
                                <div class="col-sm-7">
                                    <select class="search form-control" name="codeId" id="codeId">
                                        <option value="">-- Izaberi --</option>
                                        {% for code in codes %}
                                            <option value="{{ code.id }}">{{ code.name }}</option>
                                        {% endfor %}
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Radnik:</label>
                                <div class="col-sm-7">
                                    <select class="search form-control" name="workerId" id="workerId">
                                        <option value="">-- Izaberi --</option>
                                        {% for worker in workers %}
                                            <option value="{{ worker.id }}">{{ worker.displayName }}</option>
                                        {% endfor %}
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Datum od:</label>
                                <div class="col-sm-7">
                                    <input type="text" value="{{ date }}" class="datepicker form-control" name="dateFrom" id="dateFrom"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Datum do:</label>
                                <div class="col-sm-7">
                                    <input type="text" value="{{ date }}" class="datepicker form-control" name="dateTo" id="dateTo"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="space-25"></div>
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="col-xs-1">{{ t('Radnik') }}</th>
                            <th class="col-xs-1">{{ t('Datum') }}</th>
                            <th class="col-xs-2">{{ t('Rezija') }}</th>
                            <th class="col-xs-2">{{ t('Količina') }}</th>
                            <th class="text-center">{{ t('Zarada') }}</th>
                            <th class="col-xs-1">{{ t('Akcija') }}</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var table = $("#example").DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/cutting-jobs/get-reports-data",
                "data":  function ( d ) {
                    return $.extend({}, d, {
                        "codeId": $('#codeId').val(),
                        "workerId": $('#workerId').val(),
                        "dateTo": $('#dateTo').val(),
                        "dateFrom": $('#dateFrom').val()
                    });
                }
            },
            "columns": [
                null,
                null,
                null,
                null,
                { className: "text-center" },
                { className: "text-center" }
            ],
            "columnDefs": [
                {
                    "render": function ( data, type, row ) {
                        return '<a class="text-success" href="#" onclick="getUpdateForm(\'cutting-job-worksheets\', \''+ row[5] +'\', event)" ><i class="fa fa-edit"></i></a>'+
                        '<a href="#" onclick="getDeleteForm(\'cutting-job-worksheets\', \''+ row[5] +'\', event)" class="bigger-140 text-danger"><i class="fa fa-times"></i></a> ' ;
                    },
                    "targets": 5
                },
            ]
        });

        $("#codeId, #dateTo, #dateFrom").change( function() {
            table.draw();
        } );
    });
</script>