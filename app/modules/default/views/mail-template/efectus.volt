<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <link rel="stylesheet" href="https://elektronska-karta.bebakids.com/assets/head.css" type="text/css"/>
    <link rel="stylesheet" href="https://elektronska-karta.bebakids.com/public/assets/custom.css" type="text/css"/>
    <link rel="stylesheet" href="https://elektronska-karta.bebakids.com/public/assets/foot.css" type="text/css"/>
    <style>
        .feed-element {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            color: inherit;
            box-sizing: border-box;
            padding-bottom: 15px;
            overflow: hidden;
            border-bottom: 1px solid #e7eaec;
            margin-top: 0;
        }

        .alert {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            box-sizing: border-box;
            border: 1px solid #ebccd1;
            border-radius: 4px;
            color: #a94442;
            background-color: #f2dede !important;
            padding: 10px !important;
            margin-top: 0 !important;
            margin-bottom: 5px;
            overflow: hidden;
            border-bottom: 1px solid #e7eaec;
        }

        .col-lg-6 {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            box-sizing: border-box;
            width: 50%;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
            float: left;
        }

        .col-lg-4 {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            box-sizing: border-box;
            width: 33.33333333%;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
            float: left;
        }

        .col-lg-3 {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            box-sizing: border-box;
            width: 25%;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
            float: left;
        }

        .col-lg-2 {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            box-sizing: border-box;
            width: 16.66666667%;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
            float: left;
        }

        .col-lg-1 {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            box-sizing: border-box;
            text-align: center;
            width: 8.33333333%;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
            float: left;
        }

        .col-lg-2 {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            box-sizing: border-box;
            text-align: center;
            width: 16.66666667%;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
            float: left;
        }

        .col-lg-5 {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            box-sizing: border-box;
            width: 41.66666667%;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
            float: left;
        }

        .feed-activity-list {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            color: inherit;
            box-sizing: border-box;
        }

        .col-xs-12 {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            color: inherit;
            box-sizing: border-box;
            float: left;
            width: 100%;
            position: relative;
            min-height: 1px;
            padding-right: 15px;
            margin-top: 20px;
        }

        .btn {
            display: inline-block;
            padding: 6px 12px ;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }
        .btn-success {
            background-color: #00b048;
            border-color: #00b048;
            color: white;
        }

    </style>

</head>


<body style="margin: 30px 15px 30px 30px!important;">
<div id="wrapper">
    <div id="page-wrapper">
        <div id="content" class="wrapper wrapper-content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="space-25"></div>
                            <div class="row">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-4">
                                    <a href="{{ link }}" class="btn btn-xl btn-success">Pregled</a>
                                </div>
                                <div class="col-xs-4"></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 m-t-md">
                                    <div class="feed-activity-list">
                                        <div class="feed-element">
                                            <div class="col-lg-5 bigger-120 p-xs"><b>Proizvod</b></div>
                                            <div class="col-lg-2 text-center bigger-120"><b>Veličina</b></div>
                                            <div class="col-lg-3 text-center bigger-120"><b>Za</b></div>
                                            <div class="col-lg-2 text-center bigger-120"><b>Količina</b></div>
                                        </div>

                                        {% for efectus in data %}
                                            <div class="feed-element alert alert-danger p-xs">
                                                <div class="col-lg-5 bigger-120 p-xs" style="float: left">{{ efectus.productSize.product }}</div>
                                                <div class="col-lg-2 text-center bigger-120">{{ efectus.productSize.code }}</div>
                                                <div class="col-lg-3 text-center bigger-120">{{ efectus.getCodeTo() }}</div>
                                                <div class="col-lg-2 text-center bigger-120">{{ efectus.qty }}</div>
                                            </div>
                                        {% endfor %}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>