<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <link rel="stylesheet" href="https://elektronska-karta.bebakids.com/assets/head.css" type="text/css"/>
    <link rel="stylesheet" href="https://elektronska-karta.bebakids.com/public/assets/custom.css" type="text/css"/>
    <link rel="stylesheet" href="https://elektronska-karta.bebakids.com/public/assets/foot.css" type="text/css"/>
    <style>
        .feed-element {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            color: inherit;
            box-sizing: border-box;
            padding-bottom: 15px;
            overflow: hidden;
            border-bottom: 1px solid #e7eaec;
            margin-top: 0;
        }

        .alert {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            box-sizing: border-box;
            border: 1px solid #ebccd1;
            border-radius: 4px;
            color: #a94442;
            background-color: #f2dede !important;
            padding: 10px !important;
            margin-top: 0 !important;
            margin-bottom: 5px;
            overflow: hidden;
            border-bottom: 1px solid #e7eaec;
        }

        .col-lg-6 {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            box-sizing: border-box;
            width: 50%;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
            float: left;
        }

        .col-lg-1 {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            box-sizing: border-box;
            text-align: center;
            width: 8.33333333%;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
            float: left;
        }

        .col-lg-2 {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            box-sizing: border-box;
            text-align: center;
            width: 16.66666667%;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
            float: left;
        }

        .feed-activity-list {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            color: inherit;
            box-sizing: border-box;
        }

        .col-xs-12 {
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: "PT Sans", sans-serif;
            font-size: 14px;
            line-height: 17px;
            color: inherit;
            box-sizing: border-box;
            float: left;
            width: 100%;
            position: relative;
            min-height: 1px;
            padding-right: 15px;
            margin-top: 20px;
        }

        .fa, .fas {
            font-family: 'Font Awesome 5 Free';
            font-weight: 900;
        }

        .fa-times:before {
            content: "\f00d";
        }

    </style>

</head>


<body style="margin: 30px 15px 30px 30px!important;">
<div id="wrapper">
    <div id="page-wrapper">
        <div id="content" class="wrapper wrapper-content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="space-25"></div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p><b>Poruka: </b>{{ note }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 m-t-md">
                                    <div class="feed-activity-list">
                                        <div class="feed-element">
                                            <div class="col-lg-6 col-xs-4 bigger-120 p-xs"><b>Proizvod</b></div>
                                            <div class="col-lg-2 col-xs-2 text-center bigger-120"><b>Veličina</b></div>
                                            <div class="col-lg-2 col-xs-2 text-center bigger-120"><b>Poslato</b></div>
                                            <div class="col-lg-2 col-xs-2 text-center bigger-120"><b>Očitano</b></div>
                                        </div>

                                        {% for item in items %}
                                            <div class="feed-element alert alert-danger p-xs">
                                                <div class="col-lg-6 col-xs-4">{{ item.getProductSize().getProduct().name }}</div>
                                                <div class="col-lg-2 col-xs-2 text-center">{{ item.getProductSize().getCode().name }}</div>
                                                <div class="col-lg-2 col-xs-2 text-center">{{ item.quantity }}</div>
                                                <div class="col-lg-2 col-xs-2 text-center">{{ item.deliveredQuantity ? item.deliveredQuantity : 0 }}</div>
                                            </div>
                                        {% endfor %}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>