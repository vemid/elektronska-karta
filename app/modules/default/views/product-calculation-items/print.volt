<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css" />
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css" />
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
    <style>
        .page-break {
            page-break-after: always;
        }

        .page-breaker {
            clear: both;
            display: block;
            border: 1px solid transparent;
            page-break-before: always;
        }
    </style>
</head>
<body class="pace-done">
<img src="<?php echo APP_PATH; ?>public/img/memo.png" alt="">
<div class="row">

    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">

                    <div class="col-xs-4">
                        <div id="product-image">
                            <input type="image" src="{{ productCalculation.getProduct().getImagePrintPath() }}" width="100%" style="cursor: pointer; outline: none;"/>
                            <input type="file" id="reUpload" style="display: none;" />
                        </div>
                    </div>
                    <div class="col-xs-8">
                        <div class="pull-left col-xs-12">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Sifra proizvda :</td>
                                    <td>
                                        <span class="bigger-120 bold">{{ productCalculation.getProduct().code }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Naziv :</td>
                                    <td>
                                        <span class="bigger-120 bold">{{  productCalculation.getProduct().name }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kolekcija :</td>
                                    <td>
                                        <span class="bigger-120 bold">{{  oldCollection }}</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered" style="font-size: 14px" >
                                    <thead>
                                    <tr>
                                        <th rowspan="2" class="col-xs-2 text-center">{{ t('') }}</th>
                                        <th rowspan="1" colspan="2" class="col-xs-2 text-center">{{ t('Nabavna') }}</th>
                                        <th rowspan="1" colspan="2" class="col-xs-2 text-center">{{ t('Veloprodajna') }}</th>
                                        <th rowspan="1" colspan="2" class="col-xs-2 text-center">{{ t('Maloprodajna') }}</th>
                                    </tr>
                                    <tr>
                                        <td class="text-center">{{ t('Cena') }}</td>
                                        <td class="text-center">{{ t('Marza') }}</td>
                                        <td class="text-center">{{ t('Cena') }}</td>
                                        <td class="text-center">{{ t('Marza') }}</td>
                                        <td class="text-center">{{ t('Cena') }}</td>
                                        <td class="text-center">{{ t('Marza') }}</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {% for productPrice in productPrices %}
                                        <tr>
                                            <td class="text-center">{{ productPrice['name'] }}</td>
                                            <td class="text-center">{{ productPrice['purchasePrice'] }}</td>
                                            <td class="text-center">{{ markUp*100 }}%</td>
                                            <td class="text-center">{{ productPrice['wholesalePrice'] }}</td>
                                            <td class="text-center">{{ number_format((productPrice['wholesalePrice']/productPrice['purchasePrice'])-1,2)*100 }}%</td>
                                            <td class="text-center">{{ productPrice['retailPrice'] }}</td>
                                            <td class="text-center">{{ number_format((productPrice['retailPrice']/productPrice['wholesalePrice'])-1,2)*100 }}%</td>
                                        </tr>
                                    {% endfor %}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-bordered" style="font-size: 14px">
                                <thead>
                                <tr style="font-size: 12px !important;">
                                    <th style="width: 10px !important">{{ t('Rbr') }}</th>
                                    <th style="width: 30px">{{ t('Kartica') }}</th>
                                    <th style="width: 100px">{{ t('Naziv') }}</th>
                                    <th style="width: 30px">{{ t('Cena') }}</th>
                                    <th style="width: 30px">{{ t('Utrosak') }}</th>
                                    <th style="width: 20px">{{ t('Ukpno') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                {% set total = 0 %}
                                {% for i,item in productCalculation.getProductCalculationItems() %}
                                    <tr class="text-center">
                                        <td class="padding-2">{{ i+1 }}.</td>
                                        <td class="padding-2">{{ item.getMaterialFactory().cardNumber }}</td>
                                        <td class="padding-2">{{ item.name }}</td>
                                        <td class="padding-2">{{ item.getMaterialFactory() ? item.getMaterialFactory().costs : (item.consumption > 0 ? number_format(item.total/item.consumption,2) : 0) }}</td>
                                        <td class="padding-2">{{ item.consumption }}</td>
                                        <td class="padding-2">{{ number_format(item.total,2) }}</td>
                                    </tr>
                                    {% set i = i+1 %}
                                    {% set total = total + number_format(item.total,2) %}
                                {% endfor %}
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Ukupno :</td>
                                        <td>{{ total }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css" />
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>