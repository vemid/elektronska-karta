<div class="row">
    <div class="col-xs-7">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">

                    <div class="col-xs-5">
                        <div id="product-image">
                            <img id="myImg" src="{{ productCalculation.getProduct().getImagePath() }}" alt="Snow"
                                 style="width:100%;max-width:300px">

                            <!-- The Modal -->
                            <div id="myModal" class="modal">
                                <span class="close">&times;</span>
                                <img class="modal-content" id="img01">
                                <div id="caption"></div>
                            </div>
                            {#<input type="image" src="{{ productCalculation.getProduct().getImagePath() }}" width="100%"#}
                            {#style="cursor: pointer; outline: none;"/>#}
                            <div>
                                {% if productCalculation.status == 'FINAL' and productCalculation.completed == 0 and productPrices|length > 0 %}
                                    <a class="btn btn-success"
                                       onclick="getAjaxForm('product-calculation-items/finish/{{ productCalculation.getId() }}', event)">
                                        <i class="fa fa-check"></i> Konacna cena
                                    </a>
                                {% endif %}
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-7">
                        <div class="pull-left col-xs-12 no-padding">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Sifra proizvda :</td>
                                    <td>
                                        <span class="bigger-120 bold">{{ productCalculation.getProduct().code }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Naziv :</td>
                                    <td>
                                        <span class="bigger-120 bold">{{ productCalculation.getProduct().name }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kolekcija :</td>
                                    <td>
                                        <span class="bigger-120 bold">{{ oldCollection }}</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="space-30">
                                <h2 class="text-center">Informacije o ceni</h2>
                            </div>
                            <div class="table-responsive no-padding">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="col-xs-2 text-center">{{ t('') }}</th>
                                        <th class="col-xs-2 text-center">{{ t('Nabavna') }}</th>
                                        <th class="col-xs-2 text-center">{{ t('VP Marza') }}</th>
                                        <th class="col-xs-2 text-center">{{ t('VP') }}</th>
                                        <th class="col-xs-2 text-center">{{ t('MP') }}</th>
                                        <th class="col-xs-2 text-center">{{ t('Edit') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {% for productPrice in productPrices %}
                                        <tr>
                                            <td class="text-center">{{ productPrice['name'] }}</td>
                                            <td class="text-center">{{ productPrice['purchasePrice'] }}</td>
                                            <td class="text-center">{{ number_format((productPrice['wholesalePrice']/productPrice['purchasePrice'])-1,2)*100 }}
                                                %
                                            </td>
                                            <td class="text-center">{{ productPrice['wholesalePrice'] }}</td>
                                            <td class="text-center">{{ productPrice['retailPrice'] }}</td>
                                            <td class="text-center">
                                                <a href="#"
                                                   onclick="getUpdateForm('product-prices', {{ productPrice['id'] }}, event)">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    {% endfor %}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>

                <div class="table-responsive">
                    <form id="formCalculationItems">
                        <input type="hidden" name="productCalculationId" value="{{ productCalculation.id }}"/>
                        <div class="col-xs-12">
                            <div class="col-xs-1">
                                <label class="pull-left" style="padding-top: 10px;">Poreklo:</label>
                            </div>
                            <div class="col-xs-11">
                                <div class="col-xs-4" style="margin-left:20px !important;">
                                <select data-form="true" name="origin" id="origin" class="search form-control">
{#                                    <option value="0">{{ productCalculation.getProduct().getOrigin().name ? productCalculation.getProduct().getOrigin().name : "--- Izaberite ---" }}</option>#}
                                    <option value="0">--- Izaberite ---</option>
                                    {% for originOption in originOptions %}
                                        <option  value="{{ originOption.id }}" {{ productCalculation.getProduct().getOrigin().id == originOption.id ? 'selected':''}}>{{ originOption.name }}</option>
                                    {% endfor %}
                                </select>
                                </div>
{#                                <select data-form="true" name="origin" id="origin" class="search form-control">#}
{#                                    <option value="0">-- Izaberite --</option>#}
{#                                    <option value="TURKISH">Tursko</option>#}
{#                                    <option value="EUROP">Evropsko</option>#}
{#                                    <option value="SERBIAN">Srpsko</option>#}
{#                                    <option value="NOTHING">Bez Porekla</option>#}
{#                                </select>#}
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-1">
                                <label class="pull-left" style="padding-top: 10px;">Sastav:</label>
                            </div>
                            <div class="col-xs-11">
                                <span class="pull-left col-xs-12">
                                <input type="text" placeholder="Sastav"
                                       style="margin-left:20px !important" class="form-control"
                                       value="{{ composition }}" name="composition" id="composition"/>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-1">
                                <label class="pull-left" style="padding-top: 10px;">Kolicina:</label>
                            </div>
                            <div class="col-xs-11">
                                <span class="pull-left col-xs-2">
                                <input type="text" placeholder="Kolicina"
                                       style="margin-left:20px !important" class="form-control"
                                       value="{{ qty }}" name="qty" id="qty"/>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="col-xs-4">
                                <label class="pull-left" style="padding-right: 10px;padding-top: 10px;">{{ collection == "Basic" ? "Preporucena MP" : "VP Eur"}}
                                </label>
                            </div>
                            <div class="col-xs-8">
                            <span class="pull-left col-xs-8">
                            <input type="text" placeholder={{ collection == "Basic" ? "MP" : "Eur"}} style="width:60%!important" class="form-control"
                                   value="{{ eurWholesaprice }}" name="eurWholesalePrice" id="eurWholesalePrice"/>
                            </span>
                            </div>
                        </div>
                        <div class="col-xs-5">
                            <label class="pull-left" style="padding-right: 10px;padding-top: 10px;">Total
                                Nabavna: </label>
                            <span>
                            <input type="text" placeholder="total" style="width:20%!important"
                                   value="{{ total }} eur"/>
                            </span>
                        </div>
                        <table class="table table-striped table-bordered table-hover calculationItems">
                            <thead>
                            <tr>
                                <th class="col-xs-4">{{ t('Kupon') }}</th>
                                <th class="col-xs-2">{{ t('Cena') }}</th>
                                <th class="col-xs-2">{{ t('Consupmtion') }}</th>
                                <th class="col-xs-2">{{ t('Ukpno') }}</th>
                                <th class="text-center col-xs-2">{{ t('Akcije') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% if productCalculationItems|length == 0 %}
                                <tr>
                                    <td class="text-center">
                                        <input type="hidden" class="form" name="create[1][materialFactoryId]"
                                               id="materialFactoryId"/>
                                        <input type="hidden" class="form" name="create[1][name]" value="Krojenje"
                                               id="name"/>
                                        <input style="width:100%!important" type="text" class="required form-control"
                                               id="materialFactoryIdSearch" value="Krojenje"
                                               name="materialFactoryIdSearch"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0.07"
                                               class="form-control" id="costs" name="create[1][costs]"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="consumption" name="create[1][consumption]"/>
                                    </td>
                                    <td>
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="total" readonly name="create[1][total]"/>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" id="deleteRow" class="btn btn-circle btn-danger">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <input type="hidden" class="form" name="create[2][materialFactoryId]"
                                               id="materialFactoryId"/>
                                        <input type="hidden" class="form" name="create[2][name]" value="Sivenje"
                                               id="name"/>
                                        <input style="width:100%!important" type="text" class="required form-control"
                                               id="materialFactoryIdSearch" value="Sivenje"
                                               name="materialFactoryIdSearch"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0.07"
                                               class="form-control" id="costs" name="create[2][costs]"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="consumption" name="create[2][consumption]"/>
                                    </td>
                                    <td>
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="total" readonly name="create[2][total]"/>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" id="deleteRow" class="btn btn-circle btn-danger">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <input type="hidden" class="form" name="create[3][materialFactoryId]"
                                               id="materialFactoryId"/>
                                        <input type="hidden" class="form" name="create[3][name]" value="Pegla"
                                               id="name"/>
                                        <input style="width:100%!important" type="text" class="required form-control"
                                               id="materialFactoryIdSearch" value="Pegla"
                                               name="materialFactoryIdSearch"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0.07"
                                               class="form-control" id="costs" name="create[3][costs]"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="consumption" name="create[3][consumption]"/>
                                    </td>
                                    <td>
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="total" readonly name="create[3][total]"/>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" id="deleteRow" class="btn btn-circle btn-danger">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <input type="hidden" class="form" name="create[4][materialFactoryId]"
                                               id="materialFactoryId"/>
                                        <input type="hidden" class="form" name="create[4][name]" value="Kontrola"
                                               id="name"/>
                                        <input style="width:100%!important" type="text" class="required form-control"
                                               id="materialFactoryIdSearch" value="Kontrola"
                                               name="materialFactoryIdSearch"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0.07"
                                               class="form-control" id="costs" name="create[4][costs]"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="consumption" name="create[4][consumption]"/>
                                    </td>
                                    <td>
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="total" readonly name="create[4][total]"/>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" id="deleteRow" class="btn btn-circle btn-danger">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <input type="hidden" class="form" name="create[5][materialFactoryId]"
                                               id="materialFactoryId"/>
                                        <input type="hidden" class="form" name="create[5][name]" value="Pakovanje"
                                               id="name"/>
                                        <input style="width:100%!important" type="text" class="required form-control"
                                               id="materialFactoryIdSearch" value="Pakovanje"
                                               name="materialFactoryIdSearch"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0.07"
                                               class="form-control" id="costs" name="create[5][costs]"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="consumption" name="create[5][consumption]"/>
                                    </td>
                                    <td>
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="total" readonly name="create[5][total]"/>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" id="deleteRow" class="btn btn-circle btn-danger">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <input type="hidden" class="form" name="create[6][materialFactoryId]"
                                               id="materialFactoryId"/>
                                        <input type="hidden" class="form" name="create[6][name]" value="Dorada"
                                               id="name"/>
                                        <input style="width:100%!important" type="text" class="required form-control"
                                               id="materialFactoryIdSearch" value="Dorada"
                                               name="materialFactoryIdSearch"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0.9" class="form-control"
                                               id="costs" name="create[6][costs]"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="consumption" name="create[6][consumption]"/>
                                    </td>
                                    <td>
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="total" readonly name="create[6][total]"/>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" id="deleteRow" class="btn btn-circle btn-danger">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <input type="hidden" class="form" name="create[7][materialFactoryId]"
                                               id="materialFactoryId"/>
                                        <input type="hidden" class="form" name="create[7][name]" value="Ostalo"
                                               id="name"/>
                                        <input style="width:100%!important" type="text" class="required form-control"
                                               id="materialFactoryIdSearch" value="Ostalo"
                                               name="materialFactoryIdSearch"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0.15"
                                               class="form-control" id="costs" name="create[7][costs]"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="consumption" name="create[7][consumption]"/>
                                    </td>
                                    <td>
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="total" readonly name="create[7][total]"/>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" id="deleteRow" class="btn btn-circle btn-danger">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <input type="hidden" class="form" name="create[8][materialFactoryId]"
                                               id="materialFactoryId"/>
                                        <input type="hidden" class="form" name="create[8][name]" value="Stampa"
                                               id="name"/>
                                        <input style="width:100%!important" type="text" class="required form-control"
                                               id="materialFactoryIdSearch" value="Stampa"
                                               name="materialFactoryIdSearch"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0.1"
                                               class="form-control" id="costs" name="create[8][costs]"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="consumption" name="create[8][consumption]"/>
                                    </td>
                                    <td>
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="total" readonly name="create[8][total]"/>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" id="deleteRow" class="btn btn-circle btn-danger">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        <input type="hidden" class="form" name="create[9][materialFactoryId]"
                                               id="materialFactoryId"/>
                                        <input type="hidden" class="form" name="create[9][name]" value="Vez" id="name"/>
                                        <input style="width:100%!important" type="text" class="required form-control"
                                               id="materialFactoryIdSearch" value="Vez" name="materialFactoryIdSearch"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0.04"
                                               class="form-control" id="costs" name="create[9][costs]"/>
                                    </td>
                                    <td class="text-center">
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="consumption" name="create[9][consumption]"/>
                                    </td>
                                    <td>
                                        <input style="width:100%!important" type="text" value="0" class="form-control"
                                               id="total" readonly name="create[9][total]"/>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" id="deleteRow" class="btn btn-circle btn-danger">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            {% endif %}
                            {% for productCalculationItem in productCalculationItems %}
                                <tr>
                                    <td class="col-xs-4">
                                        <input type="hidden"
                                               name="edit[{{ productCalculationItem.id }}][materialFactoryId]"
                                               id="materialFactoryId"
                                               value="{{ productCalculationItem.getMaterialFactory().id }}"/>
                                        <input type="hidden" name="edit[{{ productCalculationItem.id }}][name]"
                                               id="name" value="{{ productCalculationItem.name }}"/>
                                        <input type="text" style="width:100%!important" class="form-control"
                                               name="materialFactoryIdSearch" id="materialFactoryIdSearch"
                                               value="{{ productCalculationItem.getMaterialFactory() ? productCalculationItem.getMaterialFactory().getDisplayName() : productCalculationItem.name }}"/>
                                    </td>
                                    <td class="col-xs-2">
                                        <input type="text" style="width:100%!important"
                                               class="form-control"{{ productCalculationItem.getMaterialFactory() ? ' readonly' : '' }}
                                               value="{{ number_format(productCalculationItem.getCosts(),3) }}"
                                               name="edit[{{ productCalculationItem.id }}][costs]"/>

                                    </td>
                                    <td class="col-xs-2">
                                        <input type="text" style="width:100%!important" class="form-control"
                                               value="{{ productCalculationItem.consumption }}"
                                               name="edit[{{ productCalculationItem.id }}][consumption]"
                                               id="consumption"/>
                                    </td>
                                    <td class="col-xs-2">
                                        <input type="text" readonly style="width:100%!important" class="form-control"
                                               value="{{ number_format(productCalculationItem.total,3) }}"
                                               name="edit[{{ productCalculationItem.id }}][total]"/>
                                    </td>
                                    <td class="text-center col-xs-2" width="11.5%">
                                        <a href="#"
                                           onclick="getDeleteForm('product-calculation-items', {{ productCalculationItem.id }}, event)"
                                           class="bigger-140 text-danger">
                                            <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </form>
                </div>
                <div class="row">
                    <div class="space-30"></div>
                    <div class="col-xs-12">
                        <div class="col-xs-4 pull-left">
                            <a href="#" title="Dodavanje" data-id="{{ productCalculation.id }}"
                               id="addProductCalculationItem" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus text-info"><span style="font-size: 15px; padding-left: 10px">Dodaj novi red</span></i>
                            </a>
                        </div>
                        <div class="col-xs-4 text-center">
                            <a href="#" class="btn btn-info" id="saveProductCalculationItems">
                                Snimi <i class="fa fa-save"></i>
                            </a>
                        </div>
                        <div class="col-xs-4 text-right">

                            <a href="/product-calculation-items/print/{{ productCalculation.id }}" title="Stampa"
                               target="_blank">
                                <i class="fa fa-print text-info" style="font-size: 36px"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-5">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <h3 class="text-center">Pretraga slicnog artikla</h3>
                    <div class="col-xs-">
                        <select data-id="{{ productCalculation.id }}" name="productIdCalculations"
                                id="simProductIdCalculations" class="search required form-control"
                                style="display: none;">
                            <option value="" selected="selected">-- Izaberite --</option>
                            {% for key, option in options %}
                                <option value="{{ key }}" {{ similarProductCalculation and similarProductCalculation.id == key ? 'selected' : '' }}>{{ option }}</option>
                            {% endfor %}
                        </select>
                    </div>
                    <div id="productCalculationContainer">
                        {% if similarProductCalculation %}
                        <div class="space-30"></div>
                        <div class="col-xs-5">
                            <div id="product-image">
                                <input type="image" src="{{ similarProductCalculation.getProduct().getImagePath() }}"
                                       width="100%" style="cursor: pointer; outline: none;"/>
                                <input type="file" id="reUpload" style="display: none;"/>
                            </div>
                        </div>

                        <div class="col-xs-7 no-padding">
                            <div class="pull-left col-xs-12 no-padding">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td>Sifra proizvda :</td>
                                        <td>
                                            <span class="bigger-120 bold">{{ similarProductCalculation.getProduct().code }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Naziv :</td>
                                        <td>
                                            <span class="bigger-120 bold">{{ similarProductCalculation.getProduct().name }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kolekcija :</td>
                                        <td>
                                            <span class="bigger-120 bold">{{ similarOldCollection }}</span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-12">

                                <h2 class="text-center">Informacije o ceni</h2>

                                <div class="table-responsive no-padding">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="col-xs-2 text-center">{{ t('') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('Nabavna') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('Marza') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('VP') }}</th>
                                            <th class="col-xs-2 text-center">{{ t('MP') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {% for productPlanedPrice in productPlanedPrices %}
                                            <tr>
                                                <td class="text-center">{{ productPlanedPrice['name'] }}</td>
                                                <td class="text-center">{{ productPlanedPrice['purchasePrice'] }}</td>
                                                <td class="text-center">{{ number_format((productPlanedPrice['wholesalePrice']/productPlanedPrice['purchasePrice'])-1,2)*100 }}
                                                    %
                                                </td>
                                                <td class="text-center">{{ productPlanedPrice['wholesalePrice'] }}</td>
                                                <td class="text-center">{{ productPlanedPrice['retailPrice'] }}</td>
                                            </tr>
                                        {% endfor %}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space-30"></div>
                    <div class="table-responsive pull-left" style="width: 100%!important;">
                        <div class="col-xs-12 text-center">
                            <h3>
                                {{ similarProductCalculation.getProduct() }} - ( {{ similarProductCalculation.status }}
                                )
                            </h3>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="col-xs-4">{{ t('Kupon') }}</th>
                                <th class="col-xs-2">{{ t('Cena') }}</th>
                                <th class="col-xs-2">{{ t('Consupmtion') }}</th>
                                <th class="col-xs-2">{{ t('Ukpno') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for similarProductCalculationItem in similarProductCalculation.getProductCalculationItems() %}
                                <tr>
                                    <td>{{ similarProductCalculationItem.getMaterialFactory() ? similarProductCalculationItem.getMaterialFactory().getDisplayName() : similarProductCalculationItem.name }}</td>
                                    <td>{{ number_format(similarProductCalculationItem.getCosts(),3) }}</td>
                                    <td>{{ similarProductCalculationItem.consumption }}</td>
                                    <td>{{ similarProductCalculationItem.total }}</td>
                                </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    (function ($) {
        $(document).on("ready", function () {
            initChosen();

            $(".calculationItems").DataTable({
                // dom: '<"html5buttons"B>lTfgitp',
                "language": {
                    "lengthMenu": t("Prikaz _MENU_  redova po strani"),
                    "zeroRecords": "nije pronađeno",
                    "info": "Prikaz stranice _PAGE_ od _PAGES_",
                    "search": "Traži",
                    "infoEmpty": "Nema redova",
                    "infoFiltered": "(filtrirano od _MAX_ ukupno redova)",
                    "paginate": {
                        "first": "Prva",
                        "last": "Poslednja",
                        "next": ">",
                        "previous": "<"
                    },
                },
                'bSort': false,
                'searching': false,
                "paging": false,
                tableTools: {
                    "sSwfPath": "/plugin/copy_csv_xls_pdf.swf"
                },
                buttons: []
            });
        })
    })($);
    $('#eurWholesalePrice').attr('required', 'required');

</script>