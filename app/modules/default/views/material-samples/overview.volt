<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">

            <div class="col-lg-12">

                <div class="ibox product-detail">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-md-5">
                                <img src="{{ materialSample.getImagePath() }}" width="600px">

                            </div>
                            <div class="col-md-7">

                                <h2 class="font-bold m-b-xs">
                                    Sifra Materijala :
                                </h2>
                                <div class="space-15"></div>
                                <span class="text-info bigger-160 bold">
                                    {{ materialSample.getId() }}  --
                                    {{ materialSample.getId() }}

                                </span>
                                <div class="m-t-md">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Naziv Klasifikacije</th>
                                            <th>Vrednost</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Vrsta Robe</td>
                                            <td><span class="bigger-120 bold"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Pol</td>
                                            <td><span class="bigger-120 bold"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Grupa Artikla</td>
                                            <td><span class="bigger-120 bold"></span></td>
                                        </tr>
                                        <tr>
                                            <td>1st Grupa Artikla</td>
                                            <td><span class="bigger-120 bold"></span></td>
                                        </tr>
                                        <tr>
                                            <td>2nd Grupa Artikla</td>
                                            <td><span class="bigger-120 bold"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Boja Artikla</td>
                                            <td><span class="bigger-120 bold"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Brend</td>
                                            <td><span class="bigger-120 bold"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Uzrast</td>
                                            <td><span class="bigger-120 bold"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Sezona</td>
                                            <td><span class="bigger-120 bold"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Kolekcija 07</td>
                                            <td><span class="bigger-120 bold"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Velicine</td>
                                            <td><span class="bigger-120 bold"></span></td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <hr>

                                <h4>Atributi proizvoda</h4>

                                <div class="small text-muted">
                                    It is a long established fact that a reader will be distracted by the readable
                                    content of a page when looking at its layout. The point of using Lorem Ipsum is

                                    <br>
                                    <br>
                                    There are many variations of passages of Lorem Ipsum available, but the majority
                                    have suffered alteration in some form, by injected humour, or randomised words
                                    which don't look even slightly believable.
                                </div>
                                <dl class="small m-t-md">
                                    <dt>Description lists</dt>
                                    <dd>A description list is perfect for defining terms.</dd>
                                    <dt>Euismod</dt>
                                    <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
                                    <dd>Donec id elit non mi porta gravida at eget metus.</dd>
                                    <dt>Malesuada porta</dt>
                                    <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
                                </dl>
                                <hr>
                                <div>
                                    <div class="btn-group">
                                        {% if materialSample.getStatus() == 'ZAHTEVANO' %}
                                            <label class="pull-left">Proizvod je zahtevan i potrebno je da potrdite upit dobavljacu </label>
                                            <button class="btn btn-primary"><i class="fa fa-rocket"></i> Upitano </button>
                                        {% elseif materialSample.getStatus() == 'UPITANO' %}
                                            <button class="btn btn-success"><i class="fa fa-check-square"></i> Kompletiran </button>
                                        {% elseif materialSample.getStatus() == 'OTKAZAN' %}
                                            <label class="btn btn-danger"><i class="fa fa-window-close"></i> Prozvod je otkazan </label>
                                        {% elseif materialSample.getStatus() == 'KOMPLETIRAN' %}
                                            <button class="btn btn-success"><i class="fa fa-check-square"></i> Kompletiran </button>
                                        {% endif %}
                                        <a href="/products/edit-form/{{ materialSample.getId() }} ">
                                            <button class="btn btn-white pull-right"><i class="fas fa-pencil-alt"></i>
                                                Edituj
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>