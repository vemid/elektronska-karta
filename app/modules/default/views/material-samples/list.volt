<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="text-center">{{ t('Klasifikacije') }}</h3>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <div class="form-group">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-4">
                                    <?php echo $this->tag->select(
                                    [
                                    "typeMaterial",
                                    $options,
                                    "useEmpty" => true,
                                    "emptyText"  => "-- Izaberite --",
                                    "class" => "form-control"
                                    ]
                                    ); ?>

                                </div>
                                <div class="col-xs-4"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <a href="/material-samples/create-form" class="btn btn-success pull-right">
                            <i class="fa fa-plus-square"></i>
                            {{ t('Novi Kupon') }}
                        </a>
                        </h1>
                    </div>
                </div>
                <div class="space-25"></div>
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>{{ t('Kupon') }}</th>
                            <th>{{ t('Dobavljač') }}</th>
                            <th>{{ t('Pod klasifikacija') }}</th>
                            <th class="text-center">{{ t('Status') }}</th>
                            <th class="col-xs-2 text-center">{{ t('Tip Klasifikacije') }}</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var table = $("#example").DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/material-samples/get-list-data",
                "data":  function ( d ) {
                    return $.extend({}, d, {
                        "typeMaterial": $('#typeMaterial').val()
                    });
                }
            },
            "columns": [
                null,
                null,
                null,
                { className: "text-center" },
                { className: "text-center" }
            ],
            "columnDefs": [
                {
                    "render": function ( data, type, row ) {
                       return '<a href="/material-samples/edit-form/' + row[4] + '" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Izmena"><i class="fa fa-edit"></i></button></a> ' +
                            '<a href="#" onclick="getDeleteForm(\'products\', '+ row[4] + ', event)" class="bigger-140 text-danger"><button class="btn btn-danger btn-circle" type="button" title="Brisanje"><i class="fa fa-times"></i></button></a> ' +
                            '<a href="/material-samples/overview/'+ row[4] +'" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search-plus"></i></button></a>';
                    },
                    "targets": 4
                }
            ]
        });

        $("#typeMaterial").change( function() {
            table.draw();
        } );
    });
</script>