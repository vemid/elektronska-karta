<div class="ibox">
    <div class="ibox-title">
        <h5>{{ t('Kreiranje kupona') }}</h5>
    </div>
    <div class="ibox-content">
        {{ form('material-samples/create', 'class': 'form-horizontal', 'enctype' : 'multipart/form-data') }}
        {% set delimiter = form.getElements()|length / 2 +2 %}
        {% set counter = 0 %}
        <div class="row">
            <div class="col-xs-6">
                {% for element in form.getElements() %}
                {% set counter = counter + 1 %}
                {% if element.getName() == 'materialPicture' %}
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{{ t('Slika') }}</label>
                        <div class="col-sm-9">
                            <div class="input-group input-file" name="productImage">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-choose"
                                                    type="button">{{ t('Izaberite') }}</button>
                                        </span>
                                <input type="text" class="form-control"
                                       placeholder='{{ t('Izaberite sliku') }}'/>
                            </div>
                        </div>
                    </div>
                {% else %}
                    <div class="form-group">
                        <label for="" class="col-sm-3 control-label no-padding-right">
                            {{ element.getLabel() }}
                        </label>
                        <div class="col-sm-9{{ element.getNAme() == 'qty' ? ' input-group spinner' : '' }}">
                            {{ element }}
                            {% if element.getNAme() == 'qty' %}
                                <div class="input-group-btn-vertical">
                                    <button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i></button>
                                    <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                                </div>
                            {% endif %}
                            <div class="help-block m-b-none"></div>
                        </div>
                    </div>
                {% endif %}
                {% if (counter + 1) > delimiter %}
                {% set counter = 0 %}
            </div>
            <div class="col-xs-6">
                {% endif %}
                {% endfor %}
            </div>
        </div>
        <div class="space-25"></div>
        <div class="form-group">
            <div class="col-sm-12">
                <button class="btn btn-primary pull-right btn-success" type="submit">{{ t('Sačuvaj') }}</button>
            </div>
        </div>
        {{ end_form() }}
        <div class="space-25"></div>
        <div class="row"></div>
    </div>
</div>
