<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>{{ t('Not found') }}</h5>
            </div>
            <div class="ibox-content">
                <p>
                    {{ t('The content you are looking for could not be found.') }}
                </p>
            </div>
        </div>
    </div>
</div>
