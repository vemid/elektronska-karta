<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>{{ t('Internal server error') }}</h5>
            </div>
            <div class="ibox-content">
                {% if error is defined and error %}
                    {% if error.getMessage() %}
                        <pre>{{ error.getMessage() }}</pre>
                    {% else %}
                        <pre>{{ error.message() }}</pre>
                    {% endif %}
                {% endif %}
            </div>
        </div>
    </div>
</div>
