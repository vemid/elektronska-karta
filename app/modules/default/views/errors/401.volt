<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>{{ t('Not authorized') }}</h5>
            </div>
            <div class="ibox-content">
                <p>
                    {{ t('You do not have sufficient permission to access this page.') }}
                </p>
            </div>
        </div>
    </div>
</div>
