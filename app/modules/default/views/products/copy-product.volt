<div class="row">
    <div class="col-lg-12">
        <div class="ibox">

            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-4">
                        <h2 class="pull-left">
                            {{ t('Kreiranje proizvoda') }}
                        </h2>
                    </div>
                    <div class="col-xs-4">
                        <input type="text" value="{{ code }}" id="finalCode" readonly="true" name="finalCode" class="form-control"
                               placeholder="Šifra Robe"/>
                    </div>
                    <div class="col-xs-4">
                    </div>
                </div>

                <div class="space-25"></div>

                <form id="product-form" action="/products/create" method="post" class="wizard-big form-horizontal"
                      enctype="multipart/form-data">
                    <h1>Roba</h1>
                    <fieldset>
                        <div id="step-select-one">
                            <h2>Vrsta Robe</h2>
                            <div class="space-15"></div>
                            <div class="row">
                                <div class="col-sm-12">
                                    {% if form.has('productTypeCodeId') %}
                                        <div class="form-group">
                                            {% set productTypeCodeId = form.get('productTypeCodeId') %}
                                            <label class="col-sm-2 control-label">{{ productTypeCodeId.getLabel() }}</label>
                                            <div class="col-sm-5">
                                                {{ productTypeCodeId }}
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-4">
                                                <div class="help-block m-b-none"></div>
                                            </div>
                                        </div>
                                    {% endif %}

                                    {% if form.has('season') %}
                                        <div class="form-group">
                                            {% set season = form.get('season') %}
                                            <label class="col-sm-2 control-label">{{ season.getLabel() }}</label>
                                            <div class="col-sm-5">
                                                {{ season }}
                                            </div>
                                            <div class="col-sm-1">
                                                <a href="#" id="customClassification"
                                                   class="btn btn-danger btn-circle btn-outline">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="help-block m-b-none"></div>
                                            </div>
                                        </div>
                                    {% endif %}

                                    {% if form.has('productProgram') %}
                                        <div class="form-group">
                                            {% set productProgram = form.get('productProgram') %}
                                            <label class="col-sm-2 control-label">{{ productProgram.getLabel() }}</label>
                                            <div class="col-sm-5">
                                                {{ productProgram }}
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-4">
                                                <div class="help-block m-b-none"></div>
                                            </div>
                                        </div>
                                    {% endif %}

                                    {% if form.has('genderClassification') %}
                                        <div class="form-group">
                                            {% set genderClassification = form.get('genderClassification') %}
                                            <label class="col-sm-2 control-label">{{ genderClassification.getLabel() }}</label>
                                            <div class="col-sm-5">
                                                {{ genderClassification }}
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-4">
                                                <div class="help-block m-b-none"></div>
                                            </div>
                                        </div>
                                    {% endif %}

                                    {% if form.has('productGroup') %}
                                        <div class="form-group">
                                            {% set productGroup = form.get('productGroup') %}
                                            <label class="col-sm-2 control-label">{{ productGroup.getLabel() }}</label>
                                            <div class="col-sm-5">
                                                {{ productGroup }}
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-4">
                                                <div class="help-block m-b-none"></div>
                                            </div>
                                        </div>
                                    {% endif %}

                                    {% if form.has('subProductGroup') %}
                                        <div class="form-group">
                                            {% set subProductGroup = form.get('subProductGroup') %}
                                            <label class="col-sm-2 control-label">{{ subProductGroup.getLabel() }}</label>
                                            <div class="col-sm-5">
                                                {{ subProductGroup }}
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-4">
                                                <div class="help-block m-b-none"></div>
                                            </div>
                                        </div>
                                    {% endif %}

                                    {% if form.has('subSubProductGroup') %}
                                        <div class="form-group">
                                            {% set subSubProductGroup = form.get('subSubProductGroup') %}
                                            <label class="col-sm-2 control-label">{{ subSubProductGroup.getLabel() }}</label>
                                            <div class="col-sm-5">
                                                {{ subSubProductGroup }}
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-4">
                                                <div class="help-block m-b-none"></div>
                                            </div>
                                        </div>
                                    {% endif %}

                                    {% if form.has('color') %}
                                        <div class="form-group">
                                            {% set color = form.get('color') %}
                                            <label class="col-sm-2 control-label">{{ color.getLabel() }}</label>
                                            <div class="col-sm-5">
                                                {{ color }}
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-4">
                                                <div class="help-block m-b-none"></div>
                                            </div>
                                        </div>
                                    {% endif %}

                                    {% if form.has('name') %}
                                        <div class="form-group">
                                            {% set name = form.get('name') %}
                                            <label class="col-sm-2 control-label">{{ name.getLabel() }}</label>
                                            <div class="col-sm-5">
                                                {{ name }}
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-4">
                                                <div class="help-block m-b-none"></div>
                                            </div>
                                        </div>
                                    {% endif %}

                                </div>
                            </div>
                        </div>

                    </fieldset>
                    <h1>{{ t('Klasifikacije') }}</h1>
                    <fieldset>
                        <h2>{{ t('Vrsta Klasifikacija') }}</h2>
                        <div class="row">
                            <div class="col-sm-12">
                                {% if form.has('produceType') %}
                                    <div class="form-group">
                                        {% set produceType = form.get('produceType') %}
                                        <label class="col-sm-2 control-label">{{ produceType.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ produceType }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}
                                {% if form.has('productTypeCode') %}
                                    <div class="form-group">
                                        {% set productTypeCode = form.get('productTypeCode') %}
                                        <label class="col-sm-2 control-label">{{ productTypeCode.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ productTypeCode }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('brand') %}
                                    <div class="form-group">
                                        {% set brand = form.get('brand') %}
                                        <label class="col-sm-2 control-label">{{ brand.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ brand }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('age') %}
                                    <div class="form-group">
                                        {% set age = form.get('age') %}
                                        <label class="col-sm-2 control-label">{{ age.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ age }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('collection') %}
                                    <div class="form-group">
                                        {% set collection = form.get('collection') %}
                                        <label class="col-sm-2 control-label">{{ collection.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ collection }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('oldCollection') %}
                                    <div class="form-group">
                                        {% set oldCollection = form.get('oldCollection') %}
                                        <label class="col-sm-2 control-label">{{ oldCollection.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ oldCollection }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('priorityId') %}
                                    <div class="form-group">
                                        {% set priority = form.get('priorityId') %}
                                        <label class="col-sm-2 control-label">{{ priority.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ priority }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('doubleSeason') %}
                                    <div class="form-group">
                                        {% set doubleSeason = form.get('doubleSeason') %}
                                        <label class="col-sm-2 control-label">{{ doubleSeason.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ doubleSeason }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}
                            </div>
                        </div>
                    </fieldset>

                    <h1>{{ t('Atributi') }}</h1>
                    <fieldset>
                        <h2>{{ t('Vrste Atributa') }}</h2>
                        <div class="row">
                            <div class="col-sm-12">
                                {% if form.has('manufacturer') %}
                                    <div class="form-group">
                                        {% set manufacturer = form.get('manufacturer') %}
                                        <label class="col-sm-2 control-label">{{ manufacturer.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ manufacturer }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('seasonCollection') %}
                                    <div class="form-group">
                                        {% set seasonCollection = form.get('seasonCollection') %}
                                        <label class="col-sm-2 control-label">{{ seasonCollection.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ seasonCollection }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('modelType') %}
                                    <div class="form-group">
                                        {% set modelType = form.get('modelType') %}
                                        <label class="col-sm-2 control-label">{{ modelType.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ modelType }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('dezenType') %}
                                    <div class="form-group">
                                        {% set dezenType = form.get('dezenType') %}
                                        <label class="col-sm-2 control-label">{{ dezenType.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ dezenType }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('printType') %}
                                    <div class="form-group">
                                        {% set printType = form.get('printType') %}
                                        <label class="col-sm-2 control-label">{{ printType.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ printType }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('applicationAttribute') %}
                                    <div class="form-group">
                                        {% set applicationAttribute = form.get('applicationAttribute') %}
                                        <label class="col-sm-2 control-label">{{ applicationAttribute.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ applicationAttribute }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('materialType') %}
                                    <div class="form-group">
                                        {% set materialType = form.get('materialType') %}
                                        <label class="col-sm-2 control-label">{{ materialType.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ materialType }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('modelProductType') %}
                                    <div class="form-group">
                                        {% set modelProductType = form.get('modelProductType') %}
                                        <label class="col-sm-2 control-label">{{ modelProductType.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ modelProductType }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('model') %}
                                    <div class="form-group">
                                        {% set model = form.get('model') %}
                                        <label class="col-sm-2 control-label">{{ model.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ model }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                {% if form.has('modelInsane') %}
                                    <div class="form-group">
                                        {% set modelInsane = form.get('modelInsane') %}
                                        <label class="col-sm-2 control-label">{{ modelInsane.getLabel() }}</label>
                                        <div class="col-sm-5">
                                            {{ modelInsane }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ t('Slika') }}</label>
                                    <div class="col-sm-5">
                                        <div class="input-group input-file" name="productImage">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-choose"
                                                    type="button">{{ t('Izaberite') }}</button>
                                        </span>
                                            <input type="text" class="form-control"
                                                   placeholder='{{ t('Izaberite sliku') }}'/>
                                        </div>
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <div class="col-sm-4">
                                        <img width="200px" src="{{ product.getImagePath() }}" alt=>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <h1>{{  t('Veličine i Vezni proizvodi') }}</h1>
                    <fieldset>
                        <h2>{{ t('Vezni proizvod') }}</h2>
                        {% set parentProduct = form.get('parentId') %}
                        <div class="space-15"></div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">{{ parentProduct.getLabel() }}</label>
                                    <div class="col-sm-5">
                                        {{ parentProduct }}
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <div class="col-sm-4">
                                        <div class="help-block m-b-none"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="step-select-one">
                            <h2>{{ t('Veličine') }}</h2>
                            <div class="space-15"></div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">{{ t('Veličine') }}</label>
                                        <div class="col-sm-5">
                                            <div id="">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        {% set counter = 0 %}
                                                        {% for code in sizes %}
                                                        {% if counter % 8 == 0 %}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        {% set counter = 0 %}
                                                        {% endif %}
                                                        <div class="checkbox-inline i-checks"><label> <input type="checkbox" data-barcode="{{ array_key_exists(code.code, productBarCodes) ? productBarCodes[code.code] : '' }}" {{ in_array(code.code, productSizes) ? 'checked' : '' }} value="{{ code.code }}" name="sizes[]"> <i></i> {{ code.code }} </label></div>
                                                        {% set counter = counter + 1 %}
                                                        {% endfor %}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {% if form.has('supplierCode') %}
                                        <div class="form-group">
                                            {% set supplierCode = form.get('supplierCode') %}
                                            <label class="col-sm-2 control-label">{{ supplierCode.getLabel() }}</label>
                                            <div class="col-sm-5">
                                                {{ supplierCode }}
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-4">
                                                <div class="help-block m-b-none"></div>
                                            </div>
                                        </div>
                                    {% endif %}
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <h1>{{ t('Barkodovi') }}</h1>
                    <fieldset>
                        <h2>{{ t('Barkodovi') }}</h2>
                        <div class="row">
                            <div class="col-xs-12">

                            </div>
                        </div>
                    </fieldset>

                    <h1>{{ t('Sirovinski i odrzavanje') }}</h1>
                    <fieldset>
                        <h2>{{ t('Sirovinski') }}</h2>
                        <div class="space-15"></div>
                        <div class="row">
                            <div class="col-sm-12">
                                {% if form.has('composition') %}
                                    <div class="form-group">
                                        {% set composition = form.get('composition') %}
                                        <label class="col-sm-1 control-label">{{ composition.getLabel() }}</label>
                                        <div class="col-sm-7">
                                            {{ composition }}
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <div class="help-block m-b-none"></div>
                                        </div>
                                    </div>
                                {% endif %}
                                <div class="form-group">
                                    <label class="col-sm-1 control-label">{{ t('Veličine') }}</label>
                                    <div class="col-sm-11" style="margin-bottom: 20px !important;">
                                        {% for washItem in washItems %}
                                            <div class="col-sm-1 text-center checkbox-inline i-checks" >
                                                <a class="product-tooltip text-danger" data-toggle="tooltip"  title="{{ washItem.description }}">
                                                    <label>
                                                        <img src="{{ washItem.getImagePath() }}" alt="" width="35px"><input style="margin-left:5px !important" type="checkbox" name="washed[{{washItem.id}}]" value="{{ washItem.id }}">
                                                    </label>
                                                </a>
                                            </div>
                                        {% endfor %}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function ($) {
        $(document).on("ready", function () {

            $(document).on("change", "#oldCollection", function () {
                var $val = $(this).val();
                var $priority = $("#priorityId");

                $.ajax({
                    url: "/classifications/find-priorities/",
                    type: "post",
                    data: {code: $val},
                    success: function (data) {
                        if (!$.isEmptyObject(data)) {
                            $priority
                                .find('option')
                                .remove()
                                .end()
                                .append($("<option/>", {
                                    value: 0,
                                    text: '-- Izaberite --'
                                }));
                            ;

                            $.each(data, function (index, value) {
                                var option = new Option(value, index);
                                $priority.append($(option));
                            });

                            $priority.trigger("liszt:updated").trigger("chosen:updated");
                        }
                    }
                });
            });
        });
    })($);
</script>