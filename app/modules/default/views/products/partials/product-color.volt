<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css"/>
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css"/>
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
    <style>
        .page-break {
            page-break-after: always;
        }

        .page-breaker {
            clear: both;
            display: block;
            border: 1px solid transparent;
            page-break-before: always;
        }
    </style>
</head>
<body class="pace-done">
<div>
    {% set numberOfProducts = 0 %}
    {% for key,seasons in data %}
        {% for rows in seasons %}
            {% for row in rows %}
                {% if numberOfProducts === 0 %}
                    <h2 class="text-center"><?php echo $key.'</br>';?></h2>
                {% endif %}
                {% set numberOfProducts = numberOfProducts + 1 %}
                <div class="row">
                    <div class="col-xs-12">
                        <div class="ibox float-e-margins">
                            <div class="col-xs-6">
{#                                <img src="{{ row['image'] }}" style="max-height: 400px" alt="">#}
                                <span>{{ row['image'] }}</span>
                            </div>
                            <div class="col-xs-6" style="font-size: 10px">
                                <span class="bold" style="color: red">{{ row['priority'] }}</span><br>
                                <span class="bold">{{ row['product'] }}</span><br>
                                <span>{{ t('Kolekcija') }}: {{ row['oldCollection'] }}&nbsp;</span><br>
                                <span>{{ t('Klasifikacija') }}: {{ row['classification'] }}</span><br>
                                <span>Velicine :</span>
                                <span class="bold">{{ row['sizes'] }}</span><br>
                                <span class="bold">Materijaliwerwerwr: </span><br>
                                {% for electronCardFactory in row['electronCardFactories'] %}
                                    <span style="font-size: 10px !important; padding-top: -8px !important;"> - {{ electronCardFactory }}</span>
                                    <br>
                                {% endfor %}
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                {% if numberOfProducts % 2 === 0 %}
                    <div class="page-break"></div>
                    {% set numberOfProducts = 0 %}
                {% endif %}
            {% endfor %}
            <div class="page-break"></div>
        {% endfor %}
        <div class="page-break"></div>
    {% endfor %}
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css"/>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>