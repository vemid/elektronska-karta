<div class="panel-body">
    <div class="row">
        <div class="col-xs-7">
            <div class="text-center">
                <h2>{{ t('Klasifikacije i atributi') }}</h2>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
                    <thead>
                    <tr>
                        <th>{{ t('Tip') }}</th>
                        <th>{{ t('Vrsta') }}</th>
                        <th>{{ t('Kriterijum') }}</th>
                        <th>{{ t('Oznaka') }}</th>
                        <th>{{ t('Naziv') }}</th>
                        <th>{{ t('Sinhr.') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {% for syncData in syncDatas %}
                        <tr>
                            <td>{{ syncData['type'] }}</td>
                            <td>{{ syncData['name'] }}</td>
                            <td>{{ syncData['kriterijum'] }}</td>
                            <td>{{ syncData['oznaka'] }}</td>
                            <td>{{ syncData['naziv'] }}</td>
                            <td>
                                {% if syncData['is_synced'] == 1 %}
                                    <i class="fa fa-check-square text-success"></i>
                                {% else %}
                                    <i class="fa fa-square text-danger"></i>
                                {% endif %}
                            </td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-xs-5">
            <div class="text-center">
                <h2>{{ t('Barkodovi') }}</h2>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" >
                    <thead>
                    <tr>
                        <th>{{ t('Sifra') }}</th>
                        <th>{{ t('Velicina') }}</th>
                        <th>{{ t('Barcode') }}</th>
                        <th>{{ t('Sinhr.') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {% for productSize in product.getProductSizes() %}
                        <tr>
                            <td>{{ product.code }}</td>
                            <td>{{ productSize.getCode().getName() }}</td>
                            <td>{{ productSize.barcode }}</td>
                            <td class="text-center">
                                {% if productSize.isSynced == true %}
                                    <i class="fa fa-check-square text-success"></i>
                                {% else %}
                                    <i class="fa fa-square text-danger"></i>
                                {% endif %}
                            </td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {#{% for ob in obj %}#}
        {#<p>{{ ob['post']['id'] }}</p>#}
    {#{% endfor %}#}
</div>