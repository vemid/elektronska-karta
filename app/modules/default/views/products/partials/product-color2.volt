<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css"/>
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css"/>
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
    <style>
        .page-break {
            page-break-after: always;
        }

        .page-breaker {
            clear: both;
            display: block;
            border: 1px solid transparent;
            page-break-before: always;
        }
    </style>
</head>
<body class="pace-done">
<div class="row">
    {% set i = 0 %}
    {% for row in data %}
        {% set i=i+1 %}
        <div style="width: 305px; height: 434px; float: right">
            <div class="ibox float-e-margins">
                <div class="col-lg-6">
                    <table valign="middle" width="100%" cellpadding="0" cellspacing="0">
                        <tbody>
                        <tr>
                            <td colspan="2" style=" font-size: 10px">
                                <span class="bold">{{ row['productName'] }} - {{ row['productCode'] }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="font-size: 10px">
                                {{ t('Kolekcija') }}: {{ row['oldCollection'] }}&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div style="font-size: 10px;">
                                    <span>Velicine : </span>
                                    <span class="bold">{{ row['sizes'] }}</span>
                                </div>
                            </td>
                        </tr>
                        <tr valign="middle" style="padding-top: 10px">
                            <td colspan="4" style="max-height: 250px;">
                                <img src="{{ row['image'] }}" style="max-height: 250px" alt="">
                            </td>
                        </tr>
                        <tr>
                            <td  colspan="2" >
                                {% for electronCardFactory in row['electronCardFactories'] %}
                                    <span style="font-size: 10px !important; padding-top: -8px !important;"> - {{ electronCardFactory }}</span><br>
                                {% endfor %}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {% if i % 4 == 0 %}
            <div class="page-break"></div>
        {% endif %}
    {% endfor %}
</div>
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css"/>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>