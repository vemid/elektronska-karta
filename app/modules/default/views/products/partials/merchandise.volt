<div class="table-responsive">
    <table id="products" class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th class="col-sm-2 text-center">{{ t('Sifra') }}</th>
            <th class="col-sm-2 text-center">{{ t('Ime') }}</th>
            <th class="col-sm-2 text-center">{{ t('Uzrast') }}</th>
            <th class="col-sm-3 text-center">{{ t('Kolekcija') }}</th>
            <th class="col-sm-1 text-center">{{ t('Boja') }}</th>
            <th class="col-sm-2 text-center">{{ t('Akcija') }}</th>
        </tr>
        </thead>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var table = $("#products").DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/products/get-list-data/68",
                "data":  function ( d ) {
                    return $.extend({}, d, {
                        "productCodeId": {{ productCodeIdMerchandise.id }}
                    });
                }
            },
            "columnDefs": [
                {
                    "render": function ( data, type, row ) {
                        return '<a href="/products/edit-form/' + row[5] + '" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Izmena"><i class="fa fa-edit"></i></button></a> ' +
                            '<a href="/products/copy-product/' + row[5] + '" class="bigger-140 text-danger"><button class="btn btn-warning btn-circle" type="button" title="Kopiranje"><i class="fa fa-copy"></i></button></a> ' +
                            '<a href="#" onclick="getDeleteForm(\'products\', ' + row[5] + ', event)" class="bigger-140 text-danger"><button class="btn btn-danger btn-circle" type="button" title="Brisanje"><i class="fa fa-times"></i></button></a> ' +
                            '<a href="/products/overview/' + row[5] + '" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search"></i></button></a>';
                    },
                    "targets": 5
                }
            ]
        });
    });
</script>