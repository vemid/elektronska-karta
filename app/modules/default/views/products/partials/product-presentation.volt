<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css"/>
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css"/>
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
    <style>
        .page-break {
            page-break-after: always;
        }

        .page-breaker {
            clear: both;
            display: block;
            border: 1px solid transparent;
            page-break-before: always;
        }

        .demo {
            border: 1px solid #C0C0C0;
            border-collapse: collapse;
            padding-top: 0px;
            text-align: center;
        }

        .demo th {
            border: 1px solid #C0C0C0;
            padding: 0px;
            background: #0e9aef !important;
            text-align: center;
        }

        .demo td {
            border: 1px solid #C0C0C0;
            text-align: center;
        }


    </style>
</head>
<body class="pace-done">
<div>
    {% set numberOfProducts = 0 %}
    {% for seasonKey,seasons in data %}
        {% for genderKey,genders in seasons %}
            {% for key,rows in genders %}
                {% for row in rows %}
                    {% if numberOfProducts === 0 %}
                        <h2 class="text-center"><?php echo $seasonKey.' '.$genderKey.' '.$key.'</br>';?></h2>
                    {% endif %}
                    {% set numberOfProducts = numberOfProducts + 1 %}
                    <div class="col-xs-4 text-center" style="font-size: 10px; float: left; height: 72mm">
                        <div class="col-xs-12 text-center">
                            <img src="{{ row['image'] }}" style="max-height: 40mm" alt="">
                        </div>
                        <span>{{ row['priority'] }}</span><br>
                        <span>{{ row['product'] }}</span><br>
                        <span>Velicine :</span>
                        <span>{{ row['sizes'] }}</span><br>
                    </div>
                    {% if numberOfProducts % 6 === 0 %}
                        <hr>
                        <div class="page-break"></div>
                        {% set numberOfProducts = 0 %}
                    {% endif %}
                {% endfor %}
                <div class="page-break"></div>
                {% set numberOfProducts = 0 %}
            {% endfor %}
            <div class="page-break"></div>
            {% set numberOfProducts = 0 %}
        {% endfor %}
        <div class="page-break"></div>
        {% set numberOfProducts = 0 %}
    {% endfor %}
</div>
<div>
    {% set numberOfProducts = 0 %}
    {% for seassonKey,seassons in dataSizes %}
        {% for genderKey,genders in seassons %}
            {% for row in genders %}
                {% set numberOfProducts = numberOfProducts + 1 %}
                <div class="col-xs-3" style="font-size: 9px; float:left;">
                    <div class="col-xs-12 text-center">
                        <img src="{{ row['image'] }}" style="max-height: 20mm !important;" alt="">
                    </div>
                    <p style="margin-bottom: -10px !important;">Sifra : {{ row['product'].code }}</p>
                    <p style="margin: -8px 0px !important;">{{ row['product'].getShortName() }}</p>
                    <p style="margin: -8px 0px -1px 0px !important;">Cena : {{ row['product'].getPrice() }}
                        &euro; {{ row['product'].getOrigin() }}</p>
                    {#<p style="margin-top:10px !important; line-height: 7px !important;">{{ row['product'].getComposition() }}</p>#}
                    <table class="demo" style="font-size: 8px !important;">
                        <thead>
                        <tr>
                            <th>Velicina</th>
                            {% for productSize in row['product'].getProductSizes() %}
                                <th>{{ productSize.code.code }}</th>
                            {% endfor %}
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Kolicina</td>
                            {% for productSize in row['product'].getProductSizes() %}
                                <td></td>
                            {% endfor %}
                        </tr>
                        <tr>
                            <td style="border-left: 1px solid transparent; border-bottom: 1px solid transparent"></td>
                            {% for productSize in row['product'].getProductSizes() %}
                                <td style="background: #c4def1 !important; padding: 7px"></td>
                            {% endfor %}
                        </tr>
                        </tbody>
                    </table>
                </div>
                {% if numberOfProducts % 12 === 0 %}
                    <hr style="border: 1px solid transparent">
                    <div class="page-break"></div>
                    {% set numberOfProducts = 0 %}
                {% endif %}
            {% endfor %}
            <hr style="border: 1px solid transparent">
            <div class="page-break"></div>
            {% set numberOfProducts = 0 %}
        {% endfor %}
        <hr style="border: 1px solid transparent">
        <div class="page-break"></div>
        {% set numberOfProducts = 0 %}
    {% endfor %}
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css"/>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>