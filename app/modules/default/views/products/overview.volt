<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="row">
                <div class="col-lg-12">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tabProduct">{{ t('Product Details') }}</a>
                            </li>
                            {% if not currentUser.isClient() %}
                                <li class="">
                                    <a data-toggle="tab" href="#tabElectronCard">{{ t('Pregled Tehnickih skica') }}</a>
                                </li>
                            {% endif %}
                            {% if electronCard.active and electronCard.finished %}
                                <li class="pull-right">
                                    <a href="/electron-cards/print/{{ electronCard.id }}" target="_blank"
                                       style="margin: 0 0 4px 0; padding: 0;">
                                        <i class="fa fa-print text-info" style="font-size: 36px"></i>
                                    </a>
                                </li>
                            {% endif %}
                            {% if not currentUser.isClient() %}
                                <li class="">
                                    <a data-toggle="tab" href="#syncDetails">{{ t('Sync details') }}</a>
                                </li>
                            {% endif %}
                        </ul>
                        <div class="tab-content">
                            <div id="tabProduct" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div id="product-image">
                                                <input type="image" src="{{ product.getImagePath() }}" width="100%"
                                                       style="cursor: pointer; outline: none;"/>
                                                <input type="file" id="reUpload" style="display: none;"/>
                                            </div>
                                        </div>
                                        <div class="col-md-7">

                                            <h2 class="font-bold m-b-xs">
                                                Sifra Artikla :
                                            </h2>
                                            <div class="space-15"></div>
                                            <span class="text-info bigger-160 bold">
                                            {{ product.getCode() }} --
                                                {{ product.getName() }}
                                        </span>
                                            <span class="text-right pull-right minus-15">
                                            <input type="checkbox" name="status" id="status"
                                                   data-id="{{ product.id }}"
                                                   data-status="{{ product.status|lower }}" {{ product.status|lower == 'kompletiran' ? 'checked' : '' }}/>
                                        </span>
                                            <div class="m-t-md">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>Naziv Klasifikacije</th>
                                                        <th>Vrednost</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>Vrsta Robe</td>
                                                        <td>
                                                            <span class="bigger-120 bold">{{ hydrator.getProductProgram() ? hydrator.getProductProgram().getName() : '' }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pol</td>
                                                        <td>
                                                            <span class="bigger-120 bold">{{ hydrator.getGenderName() }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Grupa Artikla</td>
                                                        <td>
                                                        <span class="bigger-120 bold"><?php echo array_pop(explode(' ', $hydrator->
                                                            getProductGroup()->getName()))?></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1st Grupa Artikla</td>
                                                        <td><span class="bigger-120 bold"><?php echo $hydrator->
                                                                getSubProductGroup()->getName(); ?></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2nd Grupa Artikla</td>
                                                        <td>
                                                            <span class="bigger-120 bold"><?php echo substr(strstr($hydrator->
                                                                getDubSubProductGroup()->getName()," "), 1)?></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Boja Artikla</td>
                                                        <td>
                                                            <span class="bigger-120 bold">{{ hydrator.getColor() ? hydrator.getColor().getName() : '' }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Brend</td>
                                                        <td>
                                                            <span class="bigger-120 bold">{{ hydrator.getBrand() ? hydrator.getBrand().getName() : '' }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Uzrast</td>
                                                        <td>
                                                            <span class="bigger-120 bold">{{ hydrator.getAge() ? hydrator.getAge().getName() : '' }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sezona</td>
                                                        <td>
                                                            <span class="bigger-120 bold">{{ hydrator.getSeason() ? hydrator.getSeason().getName() : '' }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kolekcija 07</td>
                                                        <td>
                                                            <span class="bigger-120 bold">{{ hydrator.getOldCollection() ? hydrator.getOldCollection().getName() : '' }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Velicine</td>
                                                        <td>
                                                            <span class="bigger-120 bold">{{ hydrator.getAllProductSizes() }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sastav</td>
                                                        <td>
                                                            <span class="bigger-120 bold">{{ product.composition ? product.composition : '' }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ulaz</td>
                                                        <td>
                                                            <span class="bigger-120 bold">{{ product.priority ? product.priority : '' }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr></tr>
                                                    <td>Za porucivanje</td>
                                                    <td>

                                            <input type="checkbox" name="productOrderable" id="productOrderable"
                                                   data-id="{{ product.id }}"
                                                   data-status="{{ product.isOrderable }}" {{ product.isOrderable  ? 'checked' : '' }}/>

                                                    </td>
                                                    </tbody>
                                                </table>

                                            </div>
                                            <hr>

                                            <h4>Atributi proizvoda</h4>
                                            <div class="m-t-md">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>Naziv Atributa</th>
                                                        <th>Vrednost</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {% for attribute in attributes %}
                                                        <tr>
                                                            <td>{{ attribute['name'] }}</td>
                                                            <td>
                                                                <span class="bigger-120 bold">{{ attribute['naziv'] }}</span>
                                                            </td>
                                                        </tr>
                                                    {% endfor %}

                                                    </tbody>
                                                </table>

                                            </div>

                                            <hr>

                                            <div>
                                                <div class="btn-group">
                                                    {% if product.getStatus() == 'U_PROCESU' %}
                                                        <button class="btn btn-primary"><i class="fa fa-rocket"></i>
                                                            Posalji u Mis
                                                        </button>
                                                    {% elseif product.getStatus() == 'OTKAZAN' %}
                                                        <label class="btn btn-danger"><i class="fa fa-window-close"></i>
                                                            Prozvod je otkazan </label>
                                                    {% elseif product.getStatus() == 'KOMPLETIRAN' %}
                                                        <button class="btn btn-success"><i
                                                                    class="fa fa-check-square"></i> Kompletiran
                                                        </button>
                                                    {% endif %}
                                                    <a href="/products/edit-form/{{ product.getId() }} ">
                                                        <button class="btn btn-white pull-right"><i
                                                                    class="fas fa-pencil-alt"></i>
                                                            Edituj
                                                        </button>
                                                    </a>
                                                    <a href="/orders/view-by-product/{{ product.getId() }} ">
                                                        <button class="btn btn-white pull-right"><i
                                                                    class="fa fa-search"></i>
                                                            Pogledaj raster
                                                        </button>
                                                    </a>
                                                    <a href="/products/print-label-overview/{{ product.getId() }} ">
                                                        <button class="btn btn-white pull-right"><i
                                                                    class="fa fa-print"></i>
                                                            Etikete
                                                        </button>
                                                    </a>

                                                    {% if product.getElectronCards(['active = :active:', 'bind' : ['active' : 1]]).count() == 0 %}
                                                        <a href="#"
                                                           onclick="getCustomCreateForm('electron-cards', 'productId', {{ product.id }}, event)">
                                                            <button class="btn btn-warning pull-right"><i
                                                                        class="fa fa-window-close-o"></i>
                                                                Elektronska Karta
                                                            </button>
                                                        </a>
                                                    {% endif %}
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    {% if pricelistDatas %}
                                    <div>
                                        <div class="col-xs-3"></div>
                                        <div class="col-xs-6 text-center">
                                            <div class="space-30">
                                                <h2 class="text-center">Informacije o ceni</h2>
                                            </div>

                                            <div class="table-responsive no-padding">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th class="col-xs-2 text-center">{{ t('Cenovnik') }}</th>
                                                        <th class="col-xs-2 text-center">{{ t('Nabavna') }}</th>
                                                        <th class="col-xs-2 text-center">{{ t('VP Marza') }}</th>
                                                        <th class="col-xs-2 text-center">{{ t('VP') }}</th>
                                                        <th class="col-xs-2 text-center">{{ t('MP') }}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {% for key,dataPricelist in pricelistDatas %}
                                                        {% if dataPricelist['NABAVNA'] %}
                                                        <tr>
                                                            <td class="text-center"><?php echo $key.'</br>';?></td>
                                                            <td class="text-center">{{ dataPricelist['NABAVNA'] }}</td>
                                                            <td class="text-center">{{ number_format((dataPricelist['VP']/dataPricelist['NABAVNA'])-1,2)*100 }}
                                                                %
                                                            </td>
                                                            <td class="text-center">{{ dataPricelist['VP'] }}</td>
                                                            <td class="text-center">{{ dataPricelist['MP'] }}</td>
                                                        </tr>
                                                        {% endif %}
                                                    {% endfor %}
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                    {% endif %}

                                    <div class="row animated fadeInRight">
                                        <div class="col-lg-12">
                                            <div class="ibox float-e-margins">
                                                <div class="" id="ibox-content">

                                                    <div id="vertical-timeline"
                                                         class="vertical-container center-orientation light-timeline">
                                                        {% for date, times in timeline %}
                                                            {% for time, data in times %}
                                                                {% set user =  data['entity'] %}
                                                                {% set icon = 'cube' %}
                                                                {% if data['entity'] == 'ElectronCard' %}
                                                                    {% set icon = 'image' %}
                                                                {% endif %}
                                                                {% if data['action'] == 'nonActive' %}
                                                                    {% set icon = icon ~ ' text-danger' %}
                                                                {% else %}
                                                                    {% set icon = icon ~ ' text-success' %}
                                                                {% endif %}
                                                                <div class="vertical-timeline-block">
                                                                    <div class="vertical-timeline-icon navy-bg">
                                                                        <i class="fa fa-{{ icon }}"></i>
                                                                    </div>

                                                                    <div class="vertical-timeline-content">
                                                                        <h2>{{ data['text'] }}</h2>
                                                                        <p>Korisnik <b>{{ data['user'] }}</b>
                                                                        </p>
                                                                        <span class="vertical-date">{{ date }}<br>
                                                                    <small>{{ time }}</small>
                                                                </span>
                                                                    </div>
                                                                </div>
                                                            {% endfor %}
                                                        {% endfor %}


                                                        <div class="vertical-timeline-block">
                                                            <div class="vertical-timeline-icon navy-bg">
                                                                <i class="fa fa-comments"></i>
                                                            </div>

                                                            <div class="vertical-timeline-content">
                                                                <h2>Chat with Monica and Sandra</h2>
                                                                <p>Web sites still in their infancy. Various versions
                                                                    have evolved over the years, sometimes by accident,
                                                                    sometimes on purpose (injected humour and the
                                                                    like). </p>
                                                                <span class="vertical-date">Yesterday <br><small>Dec 23</small></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="ibox-footer">
                                    <span class="pull-right">
                                        Full stock - <i class="fa fa-clock-o"></i> 14.04.2016 10:04 pm
                                    </span>
                                    The generated Lorem Ipsum is therefore always free
                                </div>
                            </div>
                            {% if not currentUser.isClient() %}
                                <div id="tabElectronCard" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dataTables">
                                                <thead>
                                                <tr>
                                                    <th>{{ t('Sifra') }}</th>
                                                    <th>{{ t('Naziv') }}</th>
                                                    <th>{{ t('Sezona') }}</th>
                                                    <th>{{ t('Verzija') }}</th>
                                                    <th>{{ t('Akcija') }}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {% for electronCard in electronCards %}
                                                    {% set klasa = '' %}
                                                    {% if(not electronCard.getActive()) %}
                                                        {% set klasa = 'cardActive' %}
                                                    {% endif %}
                                                    <tr class="{{ klasa }}">
                                                        <td>{{ electronCard.product.code }}</td>
                                                        <td>{{ electronCard.active }}</td>
                                                        <td>{{ hydrator.getSeason() ? hydrator.getSeason().getName() : '' }}</td>
                                                        <td>{{ electronCard.version }}</td>
                                                        <td class="text-center" width="11.5%">
                                                            <a href="/electron-cards/overview/{{ electronCard.id }}"
                                                               target="_blank" class="bigger-140 text-danger">
                                                                <button class="btn btn-info btn-circle" type="button"
                                                                        title="Pregled"><i class="fa fa-search"></i>
                                                                </button>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                {% endfor %}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            {% endif %}
                            <div id="syncDetails" class="tab-pane">
                                {{ partial('products/partials/sync') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    (function ($) {
        $(document).on("ready", function () {
            $(document).on("click", "input[type='image']", function () {
                $("input[id='reUpload']").click();
            });

            $(document).on("change", "#reUpload", function () {
                readURL(this);
                $('<button type="submit" id="file-uploader" class="btn btn-success">Upload</button>').insertAfter($(this));
            });

            $(document).on("click", "#file-uploader", function (e) {
                e.preventDefault();

                var currentUrl = window.location.href;
                var formData = new FormData();
                formData.append('productId', '{{ product.id }}');
                formData.append('image', $("#reUpload")[0].files[0]);

                $.ajax({
                    url: "/products/re-upload-image",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $.get(currentUrl, function (response) {
                            var content = $(response).find("#content");
                            var navbar = $(response).find(".navbar-top-links");
                            $("#content").replaceWith(content);
                            $(".navbar-top-links").replaceWith(navbar);
                            renderMessages(data.messages, "success");
                            setChosen();
                            initDataTable();
                            initProductStatus();
                            initProductOrderable();
                            iniQuestionnaire();
                            initTablsOnLoad();
                            initChecks();
                            initChosen();
                        });
                    }
                });
            })
        });
    })($);
</script>