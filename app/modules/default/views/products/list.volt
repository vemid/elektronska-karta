<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="text-center">{{ t('Roba') }}</h3>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div class="space-30"></div>
                <div class="row">
                    <div class="col-xs-12 text-center" style="padding-top: 10px">
                        <a style="margin-right: 5px" href="#" class="btn btn-success" onclick="getForm('/products/export-products-presentation-form', '/products/export-products-presentation', event, '')">
                            <i class="fa fa-arrow-up"></i> {{ t('Export prezentacije') }}
                        </a>
                        <a style="margin-right: 5px" href="#" class="btn btn-danger" onclick="getForm('/products/export-products-color-form', '/products/export-products-color', event, '')">
                            <i class="fa fa-arrow-up"></i> {{ t('Export izbojenja') }}
                        </a>
                            <a style="margin-right: 5px" href="#" class="btn btn-danger" onclick="getForm('/products/export-products-form', '/products/export-products', event, '')">
                                <i class="fa fa-arrow-up"></i> {{ t('Export sifara') }}
                            </a>
                            <a style="margin-right: 5px" href="#" class="btn btn-warning" onclick="getForm('/products/send-collection-to-mis-form', '/products/send-collection-to-mis', event, '')">
                                <i class="fa fa-arrow-up"></i> {{ t('Prenos u MIS') }}
                            </a>

                            <a href="/products/create-form" class="btn btn-success">
                                <i class="fa fa-plus-square"></i>
                                {{ t('Novi Proizvod') }}
                            </a>
                    </div>
                </div>
                <div class="space-20 clear clearfix"></div>
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-table-1">{{ t('Proizvod') }}</a></li>
                        <li><a data-toggle="tab" href="#tab-table-2">{{ t('Trgovačka roba') }}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-table-1" class="tab-pane active">
                            <div class="panel-body">
                                {{ partial('products/partials/products') }}
                            </div>
                        </div>
                        <div id="tab-table-2" class="tab-pane">
                            <div class="panel-body">
                                {{ partial('products/partials/merchandise') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

