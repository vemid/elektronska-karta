<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div class="row">
                    <div>{% if farbenCard.paperLabel or product.composition %}
                            <a href="/products/print-label/{{ product.getId() }} ">
                                <button class="btn btn-green pull-right" style="margin-left: 10px"><i
                                            class="fa fa-print"></i>
                                    Posalji na stampu
                                </button>
                            </a>
                            <a href="/products/disable-print-label/{{ product.getId() }} ">
                                <button class="btn btn-red pull-right"><i
                                            class="fa fa-print"></i>
                                    Ponisti stampu
                                </button>
                            </a>
                        {% else %}
                            <button class="btn btn-red pull-right"><i
                                        class="fa fa-bell"></i>
                                Nema svih podataka
                            </button>
                        {% endif %}
                    </div>
                    <div class="col-xs-12">
                        <h1 class="text-center">Pregled etiketa za stampu</h1>
                        {% for key,productSize in productSizes %}
                            {% if key == 3 or key == 6 %}
                                <p>.</p>
                            {% endif %}
                            <div class="col-xs-4">

                                <div class="col-xs-12">
                                    <table class="col-lg-12">
                                        <tr class="border-bottom"><td collspan="3"></td></tr>
                                        <tr class="border-top border-left-right">
                                            <td width="70%">
                                                <img src="/img/logo_bebakids.jpg" height="30px" alt="">
                                            </td>
                                            <td width="10%"></td>
                                            <td width="20%"><b
                                                        style="font-size: 18px !important;">{{ productSize.getCode() }}</b></td>
                                        </tr>
                                        <tr class="border-top border-left-right">
                                            <td colspan="3">
                                                <p>
                                                    <span>Proizvođač: KIDS BEBA DOO, Srbija-Beograd,Ignjata Joba 37</span>
                                                    <br>
                                                    <span>Uvoznik za Crnu Goru: DALTON DOO, Igalo, Bratstva Jedinstva 25</span>
                                                    <br>
                                                    <span style="font-size:11px !important">Uvoznik za Crnu Goru:KIDS BEBA MONTENEGRO DOO,Budva,Mediteranska bb</span><br>
                                                    <span>Uvoznik za BIH:KIDS BEBA BH, DOO,Banja Luka,Veljka Mladjenovica bb</span><br>
                                                    <span>Kvalitet kontrolisao: Jugoinspekt Beograd</span><br>
                                                    <span>Skupljanje po dužini i širini: +/- 2%</span><br>
                                                    <span>Sirovinski sastav :
                                                {% if farbenCard.paperLabel %}
                                                    {{ farbenCard.paperLabel }}
                                                    {% elseif product.composition %}
                                                        {{ product.composition }}
                                                {% else %}
                                                    <span style="color: red">Nije unet sirovinski</span>
                                                {% endif %}
                                            </span>
                                                </p>
                                                <p class="text-center">
                                                    <span class="pull-left"></span>
                                                    <span>
                                                    {% for washItem in washItems %}
                                                            {% if washItem.printSign in washData %}
                                                                <img src="{{washItem.getImagePath()}}" alt="" width="20px">
                                                            {% endif %}
                                                    {% endfor %}
{#                                            {% if 'a' in washData %}#}
{#                                                <img src="/img/textile/30.png" alt="" width="20px">#}
{#                                            {% endif %}#}
{#                                                        {% if 'b' in washData %}#}
{#                                                            <img src="/img/textile/40.png" alt="" width="20px">#}
{#                                                        {% endif %}#}
{#                                                        {% if '$' in washData %}#}
{#                                                            <img src="/img/textile/no-iron.png" alt="" width="20px">#}
{#                                                        {% endif %}#}
{#                                                        {% if '!' in washData %}#}
{#                                                            <img src="/img/textile/iron.png" alt="" width="20px">#}
{#                                                        {% endif %}#}
{#                                                        {% if 's' in washData %}#}
{#                                                            <img src="/img/textile/not-wash.png" alt="" width="20px">#}
{#                                                        {% endif %}#}
{#                                                        {% if 't' in washData %}#}
{#                                                            <img src="/img/textile/hand-wash.png" alt="" width="20px">#}
{#                                                        {% endif %}#}
{#                                                        {% if 'A' in washData %}#}
{#                                                            <img src="/img/textile/p.png" alt="" width="20px">#}
{#                                                        {% endif %}#}
{#                                                        {% if 'x' in washData %}#}
{#                                                            <img src="/img/textile/bleach.png" alt="" width="20px">#}
{#                                                        {% endif %}#}
{#                                                        {% if '+' in washData %}#}
{#                                                            <img src="/img/textile/dryer.png" alt="" width="20px">#}
{#                                                        {% endif %}#}
{#                                                        {% if '9' in washData %}#}
{#                                                            <img src="/img/textile/line.png" alt="" width="20px">#}
{#                                                        {% endif %}#}
{#                                                        {% if '//' in washData %}#}
{#                                                            <img src="/img/textile/dry-line.png" alt="" width="20px">#}
{#                                                        {% endif %}#}
{#                                                        {% if '7' in washData %}#}
{#                                                            <img src="/img/textile/dry-clean.png" alt="" width="20px">#}
{#                                                        {% endif %}#}
                                            </span>
                                                </p>
                                                <p class="text-center" style="font-size: 18px !important;">
                                                    {% for productSize2 in productSizes2 %}
                                                        {% if productSize2.getCode() == productSize.getCode() %}
                                                            <span style="background-color: black; color: white">
                                                        <b style="margin-left:4px !important">{{ productSize2.getCode() }}</b>
                                                    </span>
                                                        {% else %}
                                                            <span>
                                                        <b>{{ productSize2.getCode() }}</b>
                                                    </span>
                                                        {% endif %}
                                                    {% endfor %}
                                                </p>
                                            </td>
                                        </tr>
                                        <tr class="border-left-right">
                                            <td colspan="3">
                                                <p style="font-size: 14px !important; padding-left: 10px">
                                                    Artikal : <br>
                                                    <b style="font-size: 18px !important;">{{ product.name }}</b>
                                                </p>
                                                <p style="font-size: 14px !important; padding-left: 10px">
                                                    Sifra : <span class="pull-right" style="padding-right:10px">Visina : 110cm</span><br>
                                                    <b style="font-size: 20px !important;">{{ product.code }}</b>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr class="border-left-right text-center">
                                            <td colspan="3">
                                                <img src="data:image/png;base64,{{ base64_encode(generatorHtml.getBarcode(productSize.barcode, barcodeType)) }}">
                                            </td>
                                        </tr>
                                        <tr class="border-left-right text-center" style="margin:20px !important">
                                            <td colspan="3">
                                                <br>
                                                <span class="text-center"
                                                      style="font-size: 18px !important;margin: 20px !important"><b>www.bebakids.com</b></span>
                                                <br>
                                            </td>
                                        </tr>

                                        <tr class="border-left-right border-bottom text-center">
                                        </tr>
                                    </table>
                                    <div class="space-30"></div>
                                    <div class="space-30"></div>
                                    <table border="1px" class="col-lg-12" style="padding: 10px!important">
                                        <tr>
                                            <td class="col-lg-4" colspan="2">Sifra : <b
                                                        style="font-size: 18px !important;">{{ product.code }}</b></td>
                                            <td class="col-lg-4">Velicina : <b
                                                        style="font-size: 18px !important;">{{ productSize.getCode() }}</b>
                                            </td>
                                        </tr>
                                        <tr class="text-center">
                                            <td class="col-lg-4" style="margin:10px !important"><b
                                                        style="font-size: 18px !important;">SRB</b></td>
                                            <td class="col-lg-4"><b style="font-size: 18px !important;">MNE</b></td>
                                            <td class="col-lg-4"><b style="font-size: 18px !important;">BIH</b></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td class="col-lg-4"><b style="font-size: 18px !important;">{{pricelistData['DINARSKI']['MP']}}</b> rsd
                                            </td>
                                            <td class="col-lg-4"><b style="font-size: 18px !important;">{{pricelistData['CG']['MP']}}</b> eur</td>
                                            <td class="col-lg-4"><b style="font-size: 18px !important;">{{pricelistData['BIH']['MP']}}</b> km</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                        {% endfor %}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>