<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3 class="text-center">{{ t('Izvestaj') }}</h3>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div class="space-25"></div>
                <div class="row">
                    <div class="form-horizontal">
                        <div class="col-lg-6 col-xs-12">
                            <div class="form-group">
                                {% set season = form.get('seasonReport') %}
                                <div class="col-xs-5"><b class="bigger-120">1.</b> {{ t(season.getLabel()) }}
                                </div>
                                <div class="col-xs-7">
                                    {{ season }}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xs-12">
                            <div class="form-group">
                                {% set priorities = form.get('priorities') %}
                                <div class="col-xs-5"><b
                                            class="bigger-120">2.</b> {{ t(priorities.getLabel()) }}
                                </div>
                                <div class="col-xs-7">
                                    {{ priorities }}
                                </div>
                                <div class="col-xs-5"></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="space-25"></div>
                <div class="priority-catalog"></div>
                <div class="table-responsive">
                    <table id="productionReport" class="dataTables table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">{{ 'Proizvod' }}</th>
                            <th class="text-center col-xs-2">{{ t('Šifra proizvoda') }}</th>
                            <th class="text-center col-xs-2">{{ t('Planirano/Iskrojeno') }}</th>
                            <th class="text-center col-xs-2">{{ t('Sasiveno') }}</th>
                            <th class="col-sm-2 text-center">{{ t('Pegla/Dorada') }}</th>
                            <th class="col-sm-2 text-center">{{ t('Kontr./Pakov.') }}</th>
                            <th class="col-sm-2 text-center">{{ t('Otpremljeno') }}</th>
                            <th class="col-sm-1 text-center">{{ 'Akcija' }}</th>
                            <th></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function ($) {

        var table = $("#productionReport").DataTable({
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "pageLength": 50,
            "ajax": {
                "url": "/production-report/get-list-products-data",
                "data": function (d) {
                    return $.extend({}, d, {
                        "season": $('#seasonReport').val(),
                        "priorities": $("#priorities").val(),
                    });
                }
            },
            "fnDrawCallback": function (oSettings) {
                $("a.product-tooltip").tooltip({
                    animated: 'fade',
                    placement: 'top',
                    html: true,
                    container: 'body'
                });
            },
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return '<a class="product-tooltip text-danger" data-toggle="tooltip" title="<img src=\'' + row[9] + '\' width=\'100%\'/>">' + row[0] + '</a>';
                    },
                    "targets": 0
                },
                {
                    "render": function (data, type, row) {
                        return '<a href="/production-report/print/' + row[8] + '" class="bigger-140 text-info"><i class="fa fa-print"></i></a>' +
                            ' <a href="/production-report/overview/' + row[8] + '" class="bigger-140 text-success"><i class="fa fa-search"></i></a>';
                    },
                    "targets": 7
                },
                {
                    "targets": 8,
                    "visible": false
                }
            ],
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (aData[3] == aData[6] && aData[6]>0) {
                    $('td', nRow).closest('tr').css('background', 'lightgreen');
                }
                else if (aData[6] > aData[3]) {
                    $('td', nRow).closest('tr').css('background', 'lightcoral');
                }

                return nRow;
            },
            "columns": [
                null,
                null,
                null,
                null,
                {className: "text-center"},
                {className: "text-center"}
            ],
        });

        $(document).on("change", "#priorities", function(){
            table.draw();
        });

        $(document).on("change", "#seasonReport", function(){
            let classification = $(this).val();
            let priorities = $("#priorities");
            priorities.empty().trigger("liszt:updated").trigger("chosen:updated");
            table.draw();

            $.ajax({
                url: "/production-report/get-priorities-by-classification/",
                type: "post",
                data: {classification: classification},
                success: function (data) {
                    if (!$.isEmptyObject(data)) {
                        priorities
                            .find('option')
                            .remove()
                            .end()
                            .append($("<option/>", {
                                value: 0,
                                text: '-- Izaberite --'
                            }));
                        ;

                        $.each(data, function (index, value) {
                            let option = new Option(value, index);
                            priorities.append($(option));
                        });

                        priorities.trigger("liszt:updated").trigger("chosen:updated");
                    }
                }
            });
        });
    })(jQuery);
</script>
