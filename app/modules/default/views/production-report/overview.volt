<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="col-xs-12">
                    <a class="btn btn-success btn-circle" onclick="getAjaxForm('production-report/validate-quantities/{{ product.id }}', event)">
                        <i class="fa fa-archive"></i>
                    </a>
                    <a href="/orders/view-by-product/{{ product.id }}"  class="bigger-140 text-danger">
                        <button class="btn btn-success btn-circle" type="button" title="Pregled rastera">
                            <i class="fa fa-search"></i>
                        </button>
                    </a>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-8">
                        <div>
                            <div class="m-t-md">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Naziv</th>
                                        <th>Vrednost</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td style="padding: 2px 3px!important;">Proizvod</td>
                                        <td style="padding: 2px 3px!important;">
                                            <span class="bold">{{ product }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px 3px!important;">Sezona</td>
                                        <td style="padding: 2px 3px!important;">
                                            <span class="bold">{{ hydrator.getSeason() ? hydrator.getSeason().getName() : '' }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px 3px!important;">Prioritet / Ulaz</td>
                                        <td style="padding: 2px 3px!important;">
                                            <span class="bold">{{ product.getPriority() }}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 2px 3px!important;">Velicine</td>
                                        <td style="padding: 2px 3px!important;">
                                            <span class="bold">{{ hydrator.getAllProductSizes() }}</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 pull-left">
                        <div style="height: 280px !important;">
                            <input type="image" src="{{ product.getImagePath() }}" height="100%"
                                   style="cursor: pointer; outline: none;"/>
                        </div>
                    </div>

                </div>
                <div class="col-xs-12">
                    <div class="col-xs-12 text-center">
                        <h2>Pregled spakovanosti</h2>
                    </div>
                    <table class="table table-striped table-bordered table-hover p-xs"
                           style="font-size: 12px !important;">
                        <thead>
                        <tr>
                            <th rowspan="2"></th>
                            {% for productSize in product.productSizes %}
                                <th rowspan="1" colspan="2" class="text-center">{{ productSize.code.code }}</th>
                            {% endfor %}
                        </tr>
                        <tr>
                            {% for productSize in product.productSizes %}
                                <th class="text-center" style="padding: 1px 1px!important;">{{ t('Spa.') }}</th>
                                <th class="text-center" style="padding: 1px 1px!important;">{{ t('Por.') }}</th>
                            {% endfor %}
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>MP</td>
                            {% for productSize in product.productSizes %}
                                <td class="text-center">{{ productSizeRepository.getTotalMPPacked(productSize) }}</td>
                                <td class="text-center">{{ productSizeRepository.getTotalMPOrder(productSize) }}</td>
                            {% endfor %}
                        </tr>
                        <tr>
                            <td>VP</td>
                            {% for productSize in product.productSizes %}
                                <td class="text-center">{{ productSizeRepository.getTotalVPPacked(productSize) }}</td>
                                <td class="text-center">{{ productSizeRepository.getTotalVPOrder(productSize) }}</td>
                            {% endfor %}
                        </tr>
                        <tr>
                            <td>Total</td>
                            {% for productSize in product.productSizes %}
                                <td class="text-center">{{ productSizeRepository.getTotalMPPacked(productSize)+productSizeRepository.getTotalVPPacked(productSize) }}</td>
                                <td class="text-center">{{ productSizeRepository.getTotalMPOrder(productSize)+productSizeRepository.getTotalVPOrder(productSize) }}</td>
                            {% endfor %}
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="space-25"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-12 text-center">
                            <h2>Pregled spakovanosti MP</h2>
                        </div>
                        <table class="table table-striped table-bordered table-hover totalMP" id="report"
                               style="font-size: 12px !important;">
                            <thead>
                            <tr>
                                <th rowspan="2"></th>
                                {% for productSize in product.productSizes %}
                                    <th rowspan="1" colspan="2" class="text-center">{{ productSize.code.code }}</th>
                                {% endfor %}
                            </tr>
                            <tr>
                                {% for productSize in product.productSizes %}
                                    <th class="text-center" style="padding: 1px 1px!important;">{{ t('Spa.') }}</th>
                                    <th class="text-center" style="padding: 1px 1px!important;">{{ t('Por.') }}</th>
                                {% endfor %}
                            </tr>
                            </thead>
                            {% for dataShop in dataShops['MP'] %}
                                <tr>
                                    <td style="padding: 1px 1px!important;">{{ dataShop['name'] }}</td>
                                    <!-- Treba promeniti logiku kako se prosledjuje entityTypeId-->
                                    {% for productSize in product.productSizes %}
                                        {% set spakovano = data['5'][dataShop['entityId']][productSize.id] is defined ? data['5'][dataShop['entityId']][productSize.id] : '0' %}
                                        {% set nalozi = dataWarehouseCode['5'][dataShop['entityId']][productSize.id] %}
                                        {% set poruceno = dataOrderedItems['5'][dataShop['entityId']][productSize.id] is defined ? dataOrderedItems['5'][dataShop['entityId']][productSize.id] : '0' %}
                                        {% if number_format(spakovano) <> number_format(poruceno) and spakovano<>0 %}
                                            {% set class = '#f0afbb' %}
                                        {% endif %}
                                        {% if number_format(spakovano) == number_format(poruceno) %}
                                            {% set class ='#aff0c6' %}
                                        {% endif %}
                                        <td class="text-center"
                                            style="padding: 1px 1px!important; background: {{ class }}"><a
                                                    class="product-tooltip" data-toggle="tooltip"
                                                    title="Roba spakovana u nalozima : {{ nalozi }}">{{ spakovano }}</a>
                                        </td>
                                        <td class="text-center" style="padding: 1px 1px!important;">{{ poruceno }}</td>
                                        {% set class = '' %}
                                    {% endfor %}
                                </tr>
                            {% endfor %}
                        </table>
                    </div>
                    <div class="space-25"></div>
                    <div class="col-xs-12">
                        <div class="col-xs-12 text-center">
                            <h2>Pregled spakovanosti VP</h2>
                        </div>
                        <table class="table table-striped table-bordered table-hover p-xs"
                               style="font-size: 12px !important;">
                            <tr>
                                <td rowspan="2"></td>
                                {% for productSize in product.productSizes %}
                                    <td rowspan="1" colspan="2" class="text-center">{{ productSize.code.code }}</td>
                                {% endfor %}
                            </tr>
                            <tr>
                                {% for productSize in product.productSizes %}
                                    <td class="text-center" style="padding: 1px 1px!important;">{{ t('Spa.') }}</td>
                                    <td class="text-center" style="padding: 1px 1px!important;">{{ t('Por.') }}</td>
                                {% endfor %}
                            </tr>
                            {% for dataShop in dataShops['VP'] %}
                                <tr>
                                    <td style="padding: 1px 1px!important;">{{ dataShop['name'] }}</td>
                                    <!-- Treba promeniti logiku kako se prosledjuje entityTypeId-->
                                    {% for productSize in product.productSizes %}
                                        {% set spakovano = data['9'][dataShop['entityId']][productSize.id] is defined ? data['9'][dataShop['entityId']][productSize.id] : '0' %}
                                        {% set nalozi = dataWarehouseCode['9'][dataShop['entityId']][productSize.id] %}
                                        {% set poruceno = dataOrderedItems['9'][dataShop['entityId']][productSize.id] is defined ? dataOrderedItems['9'][dataShop['entityId']][productSize.id] : '0' %}
                                        {% if number_format(spakovano) <> number_format(poruceno) and spakovano<>0 %}
                                            {% set class = '#f0afbb' %}
                                        {% endif %}
                                        {% if number_format(spakovano) == number_format(poruceno) %}
                                            {% set class ='#aff0c6' %}
                                        {% endif %}
                                        <td class="text-center"
                                            style="padding: 1px 1px!important; background: {{ class }}"><a
                                                    class="product-tooltip" data-toggle="tooltip"
                                                    title="Roba spakovana u nalozima : {{ nalozi }}">{{ spakovano }}</a>
                                        </td>
                                        <td class="text-center" style="padding: 1px 1px!important;">{{ poruceno }}</td>
                                        {% set class = '' %}
                                    {% endfor %}
                                </tr>
                            {% endfor %}
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>