<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css"/>
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css"/>
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
    <style>
        .page-break {
            page-break-after: always;
        }

        .page-breaker {
            clear: both;
            display: block;
            border: 1px solid transparent;
            page-break-before: always;
        }
    </style>
</head>
<body class="pace-done">
<div>
    <div class="row">
        <div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-6 text-center">
                        <h3 class="text-center">Pregled robe po MP objektima</h3>
                        <table class="table table-striped table-bordered table-hover p-xs"  style="font-size: 12px !important;">
                            <tr>
                                <td></td>
                                {% for productSize in product.productSizes %}
                                    <td>{{ productSize.code.code }}</td>
                                {% endfor %}
                            </tr>
                            {% for channelSupplyShop in channelSupplyShops.channelSupplyEntities %}
                                {% if shopsToExclude[channelSupplyShop.entityId] is not defined %}
                                    <tr>
                                        <td style="padding: 1px 1px!important;">{{ channelSupplyShop }}</td>
                                        {% for productSize in product.productSizes %}
                                            <td style="padding: 1px 1px!important;">{{ dataShops[channelSupplyShop.entityId][productSize.codeId] is defined ? dataShops[channelSupplyShop.entityId][productSize.codeId] : '0' }}</td>
                                        {% endfor %}
                                    </tr>
                                {% endif %}
                            {% endfor %}
                            <tr>
                                <td><b>Ukupno</b></td>
                                {% for productSize in product.productSizes %}
                                    <td>{{ dataShops['total'][productSize.codeId] is defined ? dataShops['total'][productSize.codeId] : '0' }}</td>
                                {% endfor %}
                            </tr>
                        </table>
                    </div>

                    <div class="col-xs-6">
                        <h3 class="text-center">Informacije o proizvodu</h3>
                        <div class="col-xs-12 text-center">
                            <div class="space-15"></div>
                            <span class="text-info bigger-160 bold">
                                            {{ product.getCode() }} --
                                            {{ product.getName() }}
                                        </span>
                            <div style="height:300px;">
                                <div id="product-image" style="max-height: 200px">
                                    <input type="image" src="{{ product.getImagePrintPath() }}" width="50%"
                                           style="cursor: pointer; outline: none;"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div>
                                <div class="m-t-md">
                                    <table class="table" style="font-size: 10px !important;">
                                        <thead>
                                        <tr>
                                            <th>Naziv</th>
                                            <th>Vrednost</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td style="padding: 2px 3px!important;">Pol</td>
                                            <td style="padding: 2px 3px!important;">
                                                <span class="bold">{{ hydrator.getGenderName() ? hydrator.getGenderName() : '' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 2px 3px!important;">Grupa Artikla</td>
                                            <td style="padding: 2px 3px!important;">
                                                <?php $nameP = explode(' ', $hydrator->getProductGroup()->getName()); ?>
                                                <span class="bold"><?php echo array_pop($nameP); ?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 2px 3px!important;">Uzrast</td>
                                            <td style="padding: 2px 3px!important;">
                                                <span class="bold">{{ hydrator.getAge() ? hydrator.getAge().getName() : '' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 2px 3px!important;">Sezona</td>
                                            <td style="padding: 2px 3px!important;">
                                                <span class="bold">{{ hydrator.getSeason() ? hydrator.getSeason().getName() : '' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 2px 3px!important;">Prioritet / Ulaz</td>
                                            <td style="padding: 2px 3px!important;">
                                                <span class="bold">{{ product.getPriority() }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 2px 3px!important;">Poreklo</td>
                                            <td style="padding: 2px 3px!important;">
                                                <span class="bold">{{ product.getOrigin() }}</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <div class="col-xs-12">
                                <h3 class="text-center">Pregled robe za Magacin</h3>
                                <table class="table table-striped table-bordered table-hover text-center">
                                    <thead>
                                    <tr>
                                        <td></td>
                                        {% for productSize in product.productSizes %}
                                            <td style="padding: 2px 3px!important;">{{ productSize.code.code }}</td>
                                        {% endfor %}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {% for channelSupplyWh in dataWarehouses %}
                                        <tr>
                                            <td style="padding: 2px 3px!important;">{{ channelSupplyWh }}</td>
                                            {% for productSize in product.productSizes %}
                                                <td style="padding: 2px 3px!important;">{{ dataShops[channelSupplyWh.entityId][productSize.codeId] is defined ? dataShops[channelSupplyWh.entityId][productSize.codeId] : '0' }}</td>
                                            {% endfor %}
                                        </tr>
                                    {% endfor %}
                                    </tbody>

                                </table>
                                <h3 class="text-center">Pregled Ukupne robe</h3>
                                <table class="table table-striped table-bordered table-hover text-center">
                                    <thead>
                                    <tr>
                                        {% for productSize in product.productSizes %}
                                            <td style="padding: 2px 3px!important;">{{ productSize.code.code }}</td>
                                        {% endfor %}
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            {% for productSize in product.productSizes %}
                                                <td style="padding: 2px 3px!important;">{{ dataSizes[productSize.code.code] }}</td>
                                            {% endfor %}
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-break"></div>
    <div class="row">
        <div class="col-xs-12">
            <div class="row text-center">
                <div class="col-xs-12">
                    <h3 class="text-center">Pregled porudzbine VP kupci - DOMACI</h3>
                </div>

                <div class="col-xs-6 text-center">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <td></td>
                            {% for productSize in product.productSizes %}
                                <td style="padding: 2px 3px!important;">{{ productSize.code.code }}</td>
                            {% endfor %}
                        </tr>
                        {% for channelSupplDomesticClient in channelSupplyDomesticClients.channelSupplyEntities %}
                            {% if vpToExcludeA[channelSupplDomesticClient.entityId] is not defined %}
                                <tr>
                                    <td style="padding: 2px 3px!important;">{{ channelSupplDomesticClient }}</td>
                                    {% for productSize in product.productSizes %}
                                        <td style="padding: 2px 3px!important;">{{ dataClientA[channelSupplDomesticClient.entityId][productSize.codeId] is defined ? dataClientA[channelSupplDomesticClient.entityId][productSize.codeId] : '0' }}</td>
                                    {% endfor %}
                                </tr>
                            {% endif %}
                        {% endfor %}
                    </table>
                    <div class="space-15"></div>

                </div>
                <div class="col-xs-6">
                    <table class="table table-striped table-bordered table-hover text-center">
                        <thead>
                        <tr>
                            <td></td>
                            {% for productSize in product.productSizes %}
                                <td style="padding: 2px 3px!important;">{{ productSize.code.code }}</td>
                            {% endfor %}
                        </tr>
                        </thead>
                        <tbody>
                        {% for channelSupplDomesticClient in channelSupplyDomesticClients.channelSupplyEntities %}
                            {% if vpToExcludeB[channelSupplDomesticClient.entityId] is not defined %}
                                <tr>
                                    <td style="padding: 2px 3px!important;">{{ channelSupplDomesticClient }}</td>
                                    {% for productSize in product.productSizes %}
                                        <td style="padding: 2px 3px!important;">{{ dataClientB[channelSupplDomesticClient.entityId][productSize.codeId] is defined ? dataClientB[channelSupplDomesticClient.entityId][productSize.codeId] : '0' }}</td>
                                    {% endfor %}
                                </tr>
                            {% endif %}
                        {% endfor %}
                    </table>
                </div>
                <div class="space-15"></div>
                <div class="col-xs-12">
                    <h3 class="text-center">Pregled porudzbine VP kupci - INO</h3>
                </div>
                <div class="col-xs-6 text-center">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <td></td>
                            {% for productSize in product.productSizes %}
                                <td style="padding: 2px 3px!important;">{{ productSize.code.code }}</td>
                            {% endfor %}
                        </tr>
                        {% for channelSupplyForeignClient in channelSupplyForeignClients.channelSupplyEntities %}
                            {% if vpToExcludeA[channelSupplyForeignClient.entityId] is not defined %}
                                <tr>
                                    <td style="padding: 2px 3px!important;">{{ channelSupplyForeignClient }}</td>
                                    {% for productSize in product.productSizes %}
                                        <td style="padding: 2px 3px!important;">{{ dataClientA[channelSupplyForeignClient.entityId][productSize.codeId] is defined ? dataClientA[channelSupplyForeignClient.entityId][productSize.codeId] : '0' }}</td>
                                    {% endfor %}
                                </tr>
                            {% endif %}
                        {% endfor %}
                        <tr>
                            <td style="padding: 2px 3px!important;"><b>Ukupno</b></td>
                            {% for productSize in product.productSizes %}
                                <td style="padding: 2px 3px!important;">{{ dataClientA['total'][productSize.codeId] is defined ? dataClientA['total'][productSize.codeId] : '0' }}</td>
                            {% endfor %}
                        </tr>
                    </table>
                </div>
                <div class="col-xs-6 text-center">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <td></td>
                            {% for productSize in product.productSizes %}
                                <td style="padding: 2px 3px!important;">{{ productSize.code.code }}</td>
                            {% endfor %}
                        </tr>
                        {% for channelSupplyForeignClient in channelSupplyForeignClients.channelSupplyEntities %}
                            {% if vpToExcludeB[channelSupplyForeignClient.entityId] is not defined %}
                                <tr>
                                    <td style="padding: 2px 3px!important;">{{ channelSupplyForeignClient }}</td>
                                    {% for productSize in product.productSizes %}
                                        <td style="padding: 2px 3px!important;">{{ dataClientB[channelSupplyForeignClient.entityId][productSize.codeId] is defined ? dataClientB[channelSupplyForeignClient.entityId][productSize.codeId] : '0' }}</td>
                                    {% endfor %}
                                </tr>
                            {% endif %}
                        {% endfor %}
                        <tr>
                            <td style="padding: 2px 3px!important;"><b>Ukupno</b></td>
                            {% for productSize in product.productSizes %}
                                <td style="padding: 2px 3px!important;">{{ dataClientB['total'][productSize.codeId] is defined ? dataClientB['total'][productSize.codeId] : '0' }}</td>
                            {% endfor %}
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css"/>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>