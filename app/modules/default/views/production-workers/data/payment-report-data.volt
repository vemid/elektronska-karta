
<p>{{ paramDateFrom }}</p>
<p>{{ paramDateTo }}</p>
<p>{{ paramSector }}</p>

<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" >
                        <thead>
                        <tr>
                            <th>{{ t('Datum') }}</th>
                            <th>{{ t('Broj radnika') }}</th>
                            <th>{{ t('Ostvareno vreme') }}</th>
                            <th>{{ t('Vrednost') }}</th>
                            <th>{{ t('Ucinak') }}</th>
                            <td>{{ t('Detaljnije') }}</td>
                        </tr>
                        </thead>
                        <tbody>
                        {% set total = 0 %}
                        {% set totalWorkers = 0 %}
                        {% set totalWorkTime = 0 %}
                        {% for sectorAllOperation in sectorAllOperations %}
                            {% set date = dateTime.modify(sectorAllOperation['date']).getShortSerbianFormat() %}
                            <tr>
                                <td>{{ date }}</td>
                                <td>{{ sectorAllOperation['count'] }}</td>
                                <td>{{ sectorAllOperation['workTime'] }}</td>
                                <td>{{ number_format(sectorAllOperation['money'],0) }} RSD</td>
                                <td>{{ number_format((sectorAllOperation['seconds']/(sectorAllOperation['count']*27000))*100,2) }} %</td>
                                <td><a href="/production-workers/report-work-time?sectorId={{ paramSector }}&date={{ dateTime.modify(sectorAllOperation['date']).getUnixFormat()}}" target="_blank" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search"></i></button></a></td>
                            </tr>
                            {% set total += sectorAllOperation['money'] %}
                            {% set totalWorker += sectorAllOperation['count'] %}
                            {% set totalWorkTime += sectorAllOperation['seconds'] %}
                        {% endfor %}
                        <tr>
                            <td class="text-center">Ukupno :</td>
                            <td>{{ totalWorker }}</td>
                            <td></td>
                            <td>{{ number_format(total,2) }} RSD</td>
                            <td>{{ number_format(totalWorkTime/(totalWorker*27000),2)*100 }} %</td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="/production-workers/print-sectors/{{ paramDateFrom }}/{{ paramDateTo }}/{{ paramSector }}" target="_blank" style="margin: 0 0 4px 0; padding: 0;">
                            <i class="fa fa-print text-info" style="font-size: 36px"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>