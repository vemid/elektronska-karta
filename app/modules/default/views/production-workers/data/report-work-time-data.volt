<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" >
                        <thead>
                        <tr>
                            <th>{{ t('Datum') }}</th>
                            <th>{{ t('Naziv') }}</th>
                            <th>{{ t('Ostvareno vreme') }}</th>
                            <th>dgdfgdfgdf</th>
                            <th>{{ t('Vrednost') }}</th>
                            <th>{{ t('Norma') }}</th>
                            <th>{{ t('Status') }}</th>
                            <th>{{ t('Detaljnije') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for data in datas %}
                            <tr {{ data[5] == 0 ? ' style="background: lightcoral !important;"':'' }}>
                                <td>{{ data[0].getShortSerbianFormat() }}</td>
                                <td>{{ data[1] }}</td>
                                <td>{{ data[2] }}</td>
                                <td>{{ data[3] }}</td>
                                <td>{{ number_format(data[4],2) }} RSD</td>
                                <td {{ data[6] > 150 ? ' style="background: lightblue !important;"':'' }}>{{ data[6] }} %</td>
                                <td>{{ t(data[7]) }}</td>
                                <td><a href="/production-workers/list-by-worker-data?workerId={{ data[8] }}&dateTo={{ data[0].getUnixFormat() }}&dateFrom={{ data[0].getUnixFormat() }}" target="_blank" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search"></i></button></a></td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>