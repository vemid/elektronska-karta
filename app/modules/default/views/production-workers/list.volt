<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj</h3>
                            <a href="#" onclick="getCreateForm('production-workers',event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables" >
                        <thead>
                        <tr>
                            <th>{{ t('Klijent') }}</th>
                            <th>{{ t('Sektor') }}</th>
                            <th>{{ t('Sifra radnika') }}</th>
                            <th>{{ t('Sifra za prijavu') }}</th>
                            <th>{{ t('MIS Sifra') }}</th>
                            <th>{{ t('Akcija') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for worker in workers %}
                            <tr>
                                <td>{{ worker.getDisplayName() }}</td>
                                <td>{{ worker.getSectorCode() }}</td>
                                <td>{{ worker.workerCode }}</td>
                                <td>{{ worker.checkinCode }}</td>
                                <td>{{ worker.misCode }}</td>
                                <td class="text-center" width="11.5%">
                                    <a href="/production-workers/overview/{{ worker.id }}"  class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Pregled">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getUpdateForm('production-workers', {{ worker.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-success btn-circle" type="button" title="Izmena">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                    <a href="#" onclick="getDeleteForm('production-workers', {{ worker.id }}, event)" class="bigger-140 text-danger">
                                        <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>