<div class="row">{{ sectors }}
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="space-30"></div>
                        <div class="form-horizontal">
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <select name="sectorId" id="sectorId" class="search form-control">
                                        <option value="0">-- Izaberite --</option>
                                        {% for sector in codes %}
                                            <option  value="{{ sector.id }}" {{ paramSectorId == sector.id ? 'selected' : '' }}>{{ sector.name }}</option>
                                        {% endfor %}
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input data-form="true" type="date" name="date" value="{{ paramDate }}" id="date" class="datepicker"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <div class="">
                                        <a href="#" id="search-workers" class="btn btn-success" style="margin-left: 20px">Pretraga</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="space-20"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <div class="initial-state" {{ paramDate and paramSectorId ? '' : 'style="display: none"'}}>
                                <table id="worker-table" class="table table-striped table-bordered table-hover" >
                                    <thead>
                                    <tr>
                                        <th>{{ t('Datum') }}</th>
                                        <th>{{ t('Naziv') }}</th>
                                        <th>{{ t('Ostvareno vreme') }}</th>
                                        <th>{{ t('Planirano vreme') }}</th>
                                        <th>{{ t('Vrednost') }}</th>
                                        <th>{{ t('Norma') }}</th>
                                        <th>{{ t('Status') }}</th>
                                        <th>{{ t('Prijava') }}</th>
                                        <th>{{ t('Odjava') }}</th>
                                        <th>{{ t('Detaljnije') }}</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><script type="text/javascript">
    (function ($) {
        $(document).on("ready", function () {

            var tableDate = $("#worker-table").DataTable({
                "processing": true,
                "serverSide": true,
                "searching": false,
                "paging": false,
                "ajax": {
                    "url": "/production-workers/report-work-time-data",
                    "data": function (d) {
                        return $.extend({}, d, {
                            "sectorId": $('#sectorId').val(),
                            "date" : $("#date").val(),
                        });
                    }
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    if (!aData[14] && aData[15]) {
                        $('td', nRow).closest('tr').css('background', 'orange');
                    }
                    else if (!aData[14] && !aData[15]) {
                        $('td', nRow).closest('tr').css('background', 'red');
                    }
                    else if (!aData[12] && !aData[13] && aData[14]) {
                        $('td', nRow).closest('tr').css('background', 'lightblue');
                    }
                    else if (!aData[13]) {
                        $('td', nRow).closest('tr').css('background', 'lightcoral');
                    }



                    return nRow;
                },
                "fnDrawCallback": function( oSettings, ac, tr ) {
                    console.log(oSettings, ac, tr);
                    $(".worker-tooltip").tooltip({
                        animated: 'fade',
                        placement: 'top',
                        html: true,
                        container: 'body'
                    });
                },
                "columnDefs": [
                    {
                        targets: 1,
                        render: function(data, type, row) {
                            if(row[15]) {
                                return '<div class="worker-tooltip" title="' + row[10] + '">' + row[1] + '</div>';
                            }

                            return data;
                        }

                    },
                    {
                        "render": function ( data, type, row ) {
                            return {{ t(row[6]) }};
                        },
                        "targets:": 6
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="/production-workers/list-by-worker-data?workerId=' + row[9] + '&dateTo=' + row[0] + '&dateFrom=' + row[0] + '" target="_blank" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search"></i></button></a>\n' +
                                '<a id="add-comment-worker" onclick="getForm(\'/comments/get-create-form/' + row[9] + '/WORKING_TIME/'+ row[11] +'\', \'/comments/create\', event)" href="#" target="_blank"  data-worker-id="' + row[9] + '" class="btn btn-circle btn-white"><i class="fa fa-envelope text-info"></i></a>';
                        },
                        "targets": 9
                    }
                ]
            });

            $(document).on("click", "#search-workers", function (e) {
                if (!$(".initial-state").is(":visible")) {
                    $(".initial-state").show();
                }

                tableDate.draw();
            });

            $(document).on("click", "#save-modal-o-worker", function (e) {
                e.preventDefault();
                var form = $(this).parents("form");
                var button = $(this);

                if (form.length == 0) {
                    var modal = $(this).parents("#modal");
                    form = $(modal).find("form");
                }

                var isValid = getFormValidation(form);
                var formData = new FormData(form.get(0));
                var files = form.find("input[type='file']");

                $(files).each(function (key, element) {
                    if ("" != $(element).val()) {
                        var name = $(element).attr("name");
                        if (formData.has(name)) {
                            formData.delete(name);
                        }

                        formData.append(name, $(element)[0].files[0]);
                    }
                });

                if (isValid) {
                    $(button).addClass("disabled");
                    var currentUrl = window.location.href;
                    $.ajax({
                        url: form.attr("action"),
                        type: "post",
                        data: formData,
                        dataType: "json",
                        enctype: "multipart/form-data",
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if (!data.error) {
                                $("#modal").hide();
                                $("body").removeClass("modal-open");
                                tableDate.draw();
                                renderMessages(data.messages, "success");
                            } else {
                                $.get(currentUrl, function (response) {
                                    var content = $(response).find("#conteoperationWorkernt");
                                    var flashSession = $(response).find("#flash-session");
                                    $("#content").replaceWith(content);
                                    $("#flash-session").replaceWith(flashSession);
                                    $.each(data.messages, function (key, message) {
                                        showPushMessage(message.message, "danger");
                                    });

                                    grepSessionMessages();
                                    initChecks();
                                    initChosen();
                                    initDataTable();
                                    initDropUploadOrders();
                                    initDropZone();
                                    reUploadImage();
                                    initOrderListTable();
                                    $(".reUploadImage").trigger("change");
                                });
                            }
                        },
                        error: function (request, error) {
                            $.get(currentUrl, function (response) {
                                var content = $(response).find("#content");
                                var flashSession = $(response).find("#flash-session");
                                $("#content").replaceWith(content);
                                $("#flash-session").replaceWith(flashSession);
                                showPushMessage(t("An error was detected."), "error");
                                grepSessionMessages();
                                initProductStatus();
                                initChecks();
                                initChosen();
                                initDataTable();
                                initDropUploadOrders();
                                initDropZone();
                                reUploadImage();
                            });
                        }
                    });
                }
            });

        });
    })($);
</script>
