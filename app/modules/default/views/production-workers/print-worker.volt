<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css" />
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css" />
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
</head>
<body class="pace-done">
<div class="row">
    <div>
        <div class="col-xs-12">
            <img src="{{ siteUrl }}/img/logo_bebakids.jpg" height="50px" alt="">
        </div>
        <div class="col-xs-12">
            <span style="margin-left: 20px">
                <h1 style="text-align: center" class="bold">Radna lista</h1>
            </span>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" >
                        <tr>
                            <td class="text-center">{{ t('Radnik') }}</td>
                            <td class="text-center">{{ t('Datum Od') }}</td>
                            <td class="text-center">{{ t('Datum Do') }}</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td class="text-center">{{ worker }}</td>
                            <td class="text-center">{{ paramDateFrom }}</td>
                            <td class="text-center">{{ paramDateTo }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" >
                        <thead>
                        <tr>
                            <td>{{ t('Datum') }}</td>
                            <td>{{ t('Naziv') }}</td>
                            <td>{{ t('Cena po kom') }}</td>
                            <td>{{ t('Kolicina') }}</td>
                            <td>{{ t('Vrednost') }}</td>
                        </tr>
                        </thead>
                        <tbody>
                        {% set total = 0 %}
                        {% set timeTotal = 0 %}
                        {% set qtyTotal = 0 %}
                        {% for workerAllOperation in workerAllOperations %}
                            <tr>
                                <td>{{ workerAllOperation.date.getShortSerbianFormat() }}</td>
                                <td>{{ workerAllOperation.getOperatingListItem().getOperatingItemListType().getName() }}</td>
                                <td>{{ workerAllOperation.getOperatingListItem().getPiecePrice() }}</td>
                                <td>{{ workerAllOperation.getQty() }}</td>
                                <td>{{ workerAllOperation.getOperatingListItem().getPiecePrice() * workerAllOperation.getQty() }}</td>
                            </tr>
                            {% set total += (workerAllOperation.getOperatingListItem().getPiecePrice() * workerAllOperation.getQty()) %}
                            {% set qtyTotal += workerAllOperation.getQty() %}
                            {% set timeTotal += workerAllOperation.getTimeBetweenWorksheet() %}
                        {% endfor %}
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="text-center">Ukupno :</td>
                            <td>{{ qtyTotal }}</td>
                            <td>{{ timeTotal }}</td>
                            <td>{{ total }}</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css" />
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>