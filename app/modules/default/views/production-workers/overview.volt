<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-20 clear clearfix"></div>
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-table-1">{{ t('Podaci o radniku') }}</a></li>
                        <li><a data-toggle="tab" href="#tab-table-2">{{ t('Podaci o bolovanju') }}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-table-1" class="tab-pane active">
                            <div class="panel-body">
                                {{ partial('production-workers/partials/salary') }}
                            </div>
                        </div>
                        <div id="tab-table-2" class="tab-pane">
                            <div class="panel-body">
                                {{ partial('production-workers/partials/absences') }}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    (function ($) {

        $(document).on("ready", function () {

            $.each($(".totalSalary"), function (index, element) {
                console.log(index, element);
                $(element).DataTable({
                    paging:   false,
                    ordering: false,
                    info:     false,
                    fixedHeader: true,
                    searching:false,
                    "pageLength": 600,
                    "footerCallback": function ( row, data, start, end, display ) {
                        var api = this.api(), data;

                        // converting to interger to find total
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };


                        let tds = $("*[data-footer='true']", row);
                        $( api.column(0).footer() ).html('Ukupno');

                        $.each(tds, function($index, $elem){
                            if ($elem.hasAttribute("data-column") && $index != 0) {
                                let columnIndexToSearch = $($elem).attr("data-column");

                                let pageTotal = api
                                    .column(columnIndexToSearch, { page: 'current'} )
                                    .data()
                                    .reduce( function (a, b) {
                                        b = $(b).text();

                                        if (!$.isNumeric(parseInt(b))) {
                                            b = 0;
                                        }

                                        return intVal(a) + intVal(b);
                                    }, 0 );


                                let total = api
                                    .column(columnIndexToSearch )
                                    .data()
                                    .reduce( function (a, b) {
                                        b = $(b).text();

                                        if (!$.isNumeric(parseInt(b))) {
                                            b = 0;
                                        }

                                        return intVal(a) + intVal(b);
                                    }, 0 );

                                $( api.column($index).footer() ).html(
                                    pageTotal +' ( od '+ total +')'
                                );

                            }
                        });
                    },
                });

            });

        });
    })($);

    var chartDataPazar = {{ chartData }};
    var dataA = [];
    var cola = 0;
    $.each(chartDataPazar.data, function(index, value) {
        var object = {};
        cola +=100;
        console.log(index, $.makeArray( value ));
        object.label = index;
        object.backgroundColor = 'rgb(54, 162,'+cola+' )';
        object.borderColor = 'rgb(54, 162,'+cola+' )';
        object.data = $.map(value, function(value, index) {
            return [value];
        });
        object.fill = false;

        dataA.push(object);
    });
    var config = {
        type: 'line',
        data: {
            labels: chartDataPazar.labels,
            datasets: dataA
        },
        options: {
            responsive: true,
            title:{
                display:true,
                text:'Godisnje plate'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        }
    };

    function random_rgba() {
        var o = Math.round, r = Math.random, s = 255;
        return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(1) + ')';
    }

    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myLine = new Chart(ctx, config);

    };
</script>