<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-25"></div>
                <div class="table-responsive">
                    <table id="absence" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>{{ t('Naziv Radnika') }}</th>
                            <th>{{ t('Datum od') }}</th>
                            <th>{{ t('Datum Do') }}</th>
                            <th>{{ t('Tip') }}</th>
                            <th>{{ t('Na plati') }}</th>
                            <th>{{ t('Akcija') }}</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th data-footer="true"></th>
                            <th data-footer="true"></th>
                            <th data-footer="true"></th>
                            <th data-footer="true"></th>
                            <th data-footer="true"></th>
                            <th data-footer="true"></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $("#absence").DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 50,
            "ajax": {
                "url": "/production-workers/get-worker-absence-data",
                "data": function (d) {
                    return $.extend({}, d, {
                        "workerId": {{ workerId }}
                    });
                }
            },
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return '<a href="#" onclick="getUpdateForm(\'absence-works\', ' + row[5] + ', event)" class="bigger-140 text-danger"><button class="btn btn-success btn-circle" type="button" title="Izmena"><i class="fa fa-edit"></i></button></a> ' +
                            '<a href="#" onclick="getDeleteForm(\'absence-works\', ' + row[5] + ', event)" class="bigger-140 text-danger"><button class="btn btn-danger btn-circle" type="button" title="Brisanje"><i class="fa fa-times"></i></button></a> ';
                    },
                    "targets": 5
                },
            ]
        });
    });
</script>
