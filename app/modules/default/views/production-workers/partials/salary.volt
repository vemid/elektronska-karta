<div class="row">
    <div class="col-xs-6">
        <div class="col-xs-12">
            <div>
                <div class="m-t-md">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Podataka</th>
                            <th>Vrednost</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Ime i Prezime</td>
                            <td>
                                <span class="bigger-120 bold">{{ productionWorker.getDisplayName() }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>Sektor</td>
                            <td>
                                <span class="bigger-120 bold">{{ productionWorker.getSectorCode().getName() }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>Pol</td>
                            <td>
                                <span class="bigger-120 bold">{{ productionWorker.gender }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>Sifra prijavljivanja</td>
                            <td>
                                <span class="bigger-120 bold">{{ productionWorker.checkinCode }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>Sifra radnika</td>
                            <td>
                                <span class="bigger-120 bold">{{ productionWorker.workerCode }}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>Sifra za popust</td>
                            <td>
                                <span class="bigger-120 bold">{{ productionWorker.misCode }}</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 text-center">
        <img src="{{ siteUrl }}/uploads/avatar/avatar.png">
    </div>
</div>
<div class="row">
    <div class="col-xs-5">
        <table class="table table-striped table-bordered table-hover totalSalary" id="report">
            <thead>
            <tr>
                <th>Pregled zarada</th>
                {% for year in years %}
                    <th>{{ year }}</th>
                {% endfor %}
            </tr>
            </thead>
            <tbody>
            {% for month in months %}
                <tr>
                    <td>{{ month }}</td>
                    {% for yearData in years %}
                        <td class="text-center">
                            {{ dataSalary[month][yearData] }}
                        </td>
                    {% endfor %}
                </tr>
            {% endfor %}
            </tbody>
            <tfoot>
            <tr>
                <th data-footer="true" class="text-center"></th>
                {% set counter = 1 %}
                {% for yea in years %}
                    <th class="text-center" data-footer="true" data-column="{{ counter }}"></th>
                    {% set counter = counter+1 %}
                {% endfor %}
            </tr>
            </tfoot>
        </table>
    </div>
    <div class="col-xs-7">
        <div style="width:100%;">
            <canvas id="canvas"></canvas>
        </div>
    </div>
</div>