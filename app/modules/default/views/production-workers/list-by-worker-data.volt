<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <h2>Pregled za radnika : {{ worker }} , datum od : {{ paramDateFrom.getShortSerbianFormat() }} ,  datum do {{ paramDateTo.getShortSerbianFormat() }}</h2>
                        </div>
                        <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#tabOperations">{{ t('Operacije') }}</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#tabCuttingJobs">{{ t('Rezijski poslovi') }}</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="tabOperations" class="tab-pane active">
                                    <div class="space-30"></div>
                                    <div id="loaded-html" class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" >
                                            <thead>
                                            <tr>
                                                <th rowspan="1" colspan="9" class="text-center">Pregled operacija</th>
                                            </tr>
                                            <tr>
                                                <th>{{ t('Datum') }}</th>
                                                <th>{{ t('Naziv') }}</th>
                                                <th>{{ t('Cena po min') }}</th>
                                                <th>{{ t('Vreme za 1kom') }}</th>
                                                <th>{{ t('Kolicina') }}</th>
                                                <th>{{ t('Ostvareno Vreme') }}</th>
                                                <th>{{ t('Planirano Vreme') }}</th>
                                                <th>{{ t('Vrednost') }}</th>
                                                <th>{{ t('Norma') }}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {% set total = 0 %}
                                            {% set timeTotal = 0 %}
                                            {% set qtyTotal = 0 %}
                                            {% set totalMadeTime = 0 %}
                                            {% set unique = [] %}
                                            {% for workerAllOperation in workerAllOperations %}
                                                {% set date = dateTime.modify(workerAllOperation.date) %}
                                                {% set outTerm = itemCalculator.outTermPerWorksheetItem(workerAllOperation) %}
                                                {% set short = date.short %}
                                                <?php $unique[$short] = null; ?>
                                                <tr>
                                                    <td>{{ workerAllOperation.date.getShortSerbianFormat() }}</td>
                                                    <td><a href="/operating-list-items/overview/{{ workerAllOperation.getOperatingListItem().getId() }}"> {{ workerAllOperation.getOperatingListItem().getOperatingItemListType().getName() }}</a></td>
                                                    <td>{{ workerAllOperation.getOperatingListItem().getPiecePrice() }}</td>
                                                    <td>{{ workerAllOperation.getOperatingListItem().getPieceTime() }}</td>
                                                    <td>{{ workerAllOperation.getQty() }}</td>
                                                    <td>{{ workerAllOperation.getOperatingListItem().getPieceTime()*workerAllOperation.getQty() }}</td>
                                                    <td>{{ workerAllOperation.getTimeBetweenWorksheet() }}</td>
                                                    <td>{{ number_format(workerAllOperation.getOperatingListItem().getPiecePrice() * workerAllOperation.getQty() *workerAllOperation.getOperatingListItem().getPieceTime(),1) }}</td>
                                                    <td>{{ number_format(outTerm,1) }} %</td>
                                                </tr>
                                                {% set total += (workerAllOperation.getOperatingListItem().getPiecePrice() * workerAllOperation.getQty()*workerAllOperation.getOperatingListItem().getPieceTime()) %}
                                                {% set qtyTotal += workerAllOperation.getQty() %}
                                                {% set totalMadeTime += workerAllOperation.getOperatingListItem().getPieceTime()*workerAllOperation.getQty() %}
                                                {% set timeTotal += workerAllOperation.getTimeBetweenWorksheet() %}
                                            {% endfor %}
                                            <tr>
                                                <td>{{ unique|length }}</td>
                                                <td></td>
                                                <td></td>
                                                <td class="text-center">Ukupno :</td>
                                                <td>{{ qtyTotal }}</td>
                                                <td>{{ totalMadeTime }}</td>
                                                <td>{{ timeTotal }}</td>
                                                <td>{{ total }}</td>
                                                {% if timeTotal>0 %}
                                                    <td>{{ number_format((totalMadeTime/timeTotal)*100,1) }} % / <span style="color: red">{{ number_format((totalMadeTime/(450*(unique|length)))*100,1) }}</span> </td>
                                                {% else %}
                                                    <td>NN</td>
                                                {% endif %}
                                                <!--  number_format((totalMadeTime/(450*(unique|length)) -->
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="tabCuttingJobs" class="tab-pane">
                                    <div class="space-30"></div>
                                    <div id="loaded-html" class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" >
                                            <thead>
                                            <tr>
                                                <th rowspan="1" colspan="6" class="text-center">Pregled rezijskih poslova</th>
                                            </tr>
                                            <tr>
                                                <th>{{ t('Datum') }}</th>
                                                <th>{{ t('Naziv') }}</th>
                                                <th>{{ t('Cena po kom') }}</th>
                                                <th>{{ t('Vreme') }}</th>
                                                <th>{{ t('Vrednost') }}</th>
                                                <th>{{ t('Norma') }}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {% set total = 0 %}
                                            {% set timeTotal = 0 %}
                                            {% set qtyTotal = 0 %}
                                            {% for workerAllCuttingJob in workerAllCuttingJobs %}
                                                <tr>
                                                    <td>{{ workerAllCuttingJob.date.getShortSerbianFormat() }}</td>
                                                    <td>{{ workerAllCuttingJob.getCuttingJob().name }}</td>
                                                    <td>{{ workerAllCuttingJob.getCuttingJob().getPiecePrice() }}</td>
                                                    <td>{{ workerAllCuttingJob.getQty() }}</td>
                                                    <td>{{ workerAllCuttingJob.getCuttingJob().getPiecePrice() * workerAllCuttingJob.getQty() }}</td>
                                                    <td>100 %</td>
                                                </tr>
                                                {% set total += (workerAllCuttingJob.getCuttingJob().getPiecePrice() * workerAllCuttingJob.getQty()) %}
                                                {% set qtyTotal += workerAllCuttingJob.getQty() %}
                                                {% set timeTotal += workerAllCuttingJob.getTimeBetweenWorksheet() %}
                                            {% endfor %}
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="text-center">Ukupno :</td>
                                                    <td>{{ qtyTotal }}</td>
                                                    <td>{{ total }}</td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <div class="pull-right">
                                            <a href="/production-workers/print-worker/{{ paramDateFrom }}/{{ paramDateTo }}/{{ paramWorker }}" target="_blank" style="margin: 0 0 4px 0; padding: 0;">
                                                <i class="fa fa-print text-info" style="font-size: 36px"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>