<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/head.css" />
    <link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/custom.css" />
    <script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/head.js"></script>
</head>
<body class="pace-done">
<div class="row">
    <div>
        <div class="col-xs-12">
            <img src="{{ siteUrl }}/img/logo_bebakids.jpg" height="50px" alt="">
        </div>
        <div class="col-xs-12">
            <span style="margin-left: 20px">
                <h1 style="text-align: center" class="bold">Radna lista</h1>
            </span>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" >
                        <tr>
                            <td class="text-center">{{ t('Sector') }}</td>
                            <td class="text-center">{{ t('Datum Od') }}</td>
                            <td class="text-center">{{ t('Datum Do') }}</td>
                        </tr>
                        <tbody>
                        <tr>
                            <td class="text-center">{{ sector.name }}</td>
                            <td class="text-center">{{ paramDateFrom }}</td>
                            <td class="text-center">{{ paramDateTo }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="space-30"></div>
                <div id="loaded-html" class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" >
                        <thead>
                        <tr>
                            <th>{{ t('Datum') }}</th>
                            <th>{{ t('Kolicina') }}</th>
                            <th>{{ t('Ucinak') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% set total = 0 %}
                        {% for sectorAllOperation in sectorAllOperations %}
                            {#{% set date = dateTime.modify(sectorAllOperation['date']) %}#}
                            <tr>
                                <td>{{ sectorAllOperation['date'] }}</td>
                                <td>{{ sectorAllOperation['total'] }}</td>
                                <td></td>
                            </tr>
                            {% set total += sectorAllOperation['total'] %}
                        {% endfor %}
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-center">Ukupno :</td>
                            <td>{{ total }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="file://<?php echo APP_PATH; ?>/public/assets/foot.css" />
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/foot.js"></script>
<script type="text/javascript" src="file://<?php echo APP_PATH; ?>/public/assets/custom.js"></script>
</body>
</html>