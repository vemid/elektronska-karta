<div id="load-into">
    <div id="loaded-html">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover deliveryNotes" id="report">
                <tfoot>
                <tr>
                    <th data-footer="true"></th>
                    <th data-footer="true" data-column="1"></th>
                    <th data-footer="true" data-column="2"></th>
                    <th data-footer="true" data-column="3"></th>
                    <th data-footer="true" data-column="4"></th>
                    <th data-column="1"></th>
                    <th data-column="1"></th>
                </tr>
                </tfoot>
                <thead>
                <tr>
                    <th class="center">Partner</th>
                    <th class="text-center">Poruceno A</th>
                    <th class="text-center">Poruceno B</th>
                    <th class="text-center">Isporuceno A</th>
                    <th class="text-center">Isporuceno B</th>
                    <th class="text-center">Uneto A</th>
                    <th class="text-center">Uneto B</th>
                </tr>
                </thead>
                <tbody>
                {% for orderItem in orderItems %}
                    {% if orderItem.getEntityTypeId() == typeClient or in_array(orderItem.getEntityId(),shops) %}

                        {% set deliveredA = orderItem.getDeliveryNoteItem() ? orderItem.getDeliveryNoteItem().quantityA : 0 %}
                        {% set deliveredB = orderItem.getDeliveryNoteItem() ? orderItem.getDeliveryNoteItem().quantityB : 0 %}
                        {% set deliveredMisA = deliveryNotes[code][orderItem.client.code] ? deliveryNotes[code][orderItem.client.code] : 0 %}
                        {% set deliveredMisB = 0 %}
                        <tr>

                            <td>{{ orderItem.client.getDisplayName() }}</td>
                            <td class="text-center">{{ orderItem.quantityA }}</td>
                            <td>{{ orderItem.quantityB }}</td>
                            <td class="text-center">{{ deliveredMisA }}</td>
                            <td class="text-center">{{ deliveredMisB }}</td>
                            <td class="text-center" id="deliveryNoteCreate">
                                {% if deliveredA %}
                                    <a href="#" class="edit"
                                       data-pk="{{ orderItem.getDeliveryNoteItem().id }}"
                                       data-type-id="A" data-id="{{ orderItem.id }}"
                                       data-delivered-a="{{ deliveredMisA }}"
                                       data-delivered-b="{{ deliveredMisB }}">
                                        {{ deliveredA }}
                                    </a>
                                {% elseif calculator.getMaximumToDeliverA(orderItem, deliveredMisA, deliveredMisB) %}
                                    <a href="#" class="btn btn-circle btn-xs btn-success create"
                                       data-type-id="A" data-id="{{ orderItem.id }}"
                                       data-delivered-a="{{ deliveredMisA }}"
                                       data-delivered-b="{{ deliveredMisB }}">
                                        <i class="fa fa-check"></i>
                                    </a>
                                {% endif %}
                            </td>
                            <td class="text-center" id="deliveryNoteCreate">
                                {% if deliveredB %}
                                    <a href="#" class="edit"
                                       data-pk="{{ orderItem.getDeliveryNoteItem().id }}"
                                       data-type-id="B" data-id="{{ orderItem.id }}"
                                       data-delivered-a="{{ deliveredMisA }}"
                                       data-delivered-b="{{ deliveredMisB }}">
                                        {{ deliveredB }}
                                    </a>
                                {% elseif calculator.getMaximumToDeliverB(orderItem, deliveredMisA) %}
                                    <a href="#" class="btn btn-circle btn-xs btn-success create"
                                       data-type-id="B" data-id="{{ orderItem.id }}"
                                       data-delivered-a="{{ deliveredMisA }}"
                                       data-delivered-b="{{ deliveredMisB }}">
                                        <i class="fa fa-check"></i>
                                    </a>
                                {% endif %}
                            </td>
                        </tr>
                    {% endif %}
                {% endfor %}
                </tbody>
            </table>
        </div>
    </div>
</div>

