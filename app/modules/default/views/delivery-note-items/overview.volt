<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="space-15"></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <h3>Kolicina sa prijemnice : <b><strong>{{ qty }}</strong></b></h3>
                    </div>
                </div>
            </div>
            <div class="space-30"></div>
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    {% for index, productSize in productSizes %}
                        <li{{ loop.first ? ' class="active"' : '' }}><a data-toggle="tab" href="#tab-table-{{ index }}">{{ productSize.code.code }}</a></li>
                    {% endfor %}
                </ul>
                <div class="tab-content">
                    {% for index, productSize in productSizes %}
                        <div id="tab-table-{{ index }}" class="tab-pane{{ loop.first ? ' active' : '' }}">
                            <div class="panel-body">
                                {{ partial('delivery-note-items/partials/table', [
                                    'orderItems' : productSize.orderItems,
                                    'shops' : shops,
                                    'calculator' : calculator,
                                    'deliveryNotes' : deliveryNotes,
                                    'code' : productSize.code.code
                                ]) }}
                            </div>
                        </div>
                    {% endfor %}
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function ($) {
        $(document).on("ready", function () {

            $('#deliveryNoteCreate a.create').editable({
                type: 'text',
                url: '/delivery-note-items/create',
                pk: 1,
                params: function (params) {
                    params.orderItemId = $(this).attr("data-id");
                    params.deliveryNoteId = '{{ deliveryNote.id }}';
                    params.purchasePrice = '{{ price }}'

                    if ($(this).attr("data-type-id") === "A") {
                        params.quantityA = params.value;
                    } else if ($(this).attr("data-type-id") === "B") {
                        params.quantityB = params.value;
                    }
                    params.deliveredA = $(this).attr("data-delivered-a");
                    params.deliveredB = $(this).attr("data-delivered-a");

                    return params;
                },
                title: 'Unesite količinu',
                ajaxOptions: {
                    dataType: 'json'
                },
                success: function (response, newValue) {
                    if (!response) {
                        return "Unknown error!";
                    }

                    if (response.error === false) {
                        renderMessages(response.messages, "success");
                        window.location.reload();

                    } else {
                        $.get(window.location.href, function (responseGet) {
                            var content = $(responseGet).find("#content");
                            var flashSession = $(responseGet).find("#flash-session");
                            $("#content").replaceWith(content);
                            $("#flash-session").replaceWith(flashSession);
                            $.each(response.messages, function (key, message) {
                                showPushMessage(message.message, "danger");
                            });
                        });
                    }
                }
            });

            $('#deliveryNoteCreate a.edit').editable({
                type: 'text',
                url: '/delivery-note-items/update',
                params: function (params) {
                    params.orderItemId = $(this).attr("data-id");
                    params.deliveryNoteId = '{{ deliveryNote.id }}';
                    params.purchasePrice = '{{ price }}';
                    if ($(this).attr("data-type-id") === "A") {
                        params.quantityA = params.value;
                    } else if ($(this).attr("data-type-id") === "B") {
                        params.quantityB = params.value;
                    }
                    params.deliveredA = $(this).attr("data-delivered-a");
                    params.deliveredB = $(this).attr("data-delivered-a");

                    return params;
                },
                title: 'Unesi količinu',
                ajaxOptions: {
                    dataType: 'json'
                },
                success: function (response, newValue) {
                    if (!response) {
                        return "Unknown error!";
                    }

                    if (response.error === false) {
                        renderMessages(response.messages, "success");
                    } else {
                        $.get(window.location.href, function (responseGet) {
                            var content = $(responseGet).find("#content");
                            var flashSession = $(responseGet).find("#flash-session");
                            $("#content").replaceWith(content);
                            $("#flash-session").replaceWith(flashSession);
                            $.each(response.messages, function (key, message) {
                                showPushMessage(message.message, "danger");
                            });
                        });
                    }
                }
            });

            $.each($(".deliveryNotes"), function (index, element) {
                $(element).DataTable({
                    fixedHeader: true,
                    "pageLength": 20,
                    // dom: '<"html5buttons"B>lTfgitp',
                    "language": {
                        "lengthMenu": t("Prikaz _MENU_  redova po strani"),
                        "zeroRecords": "nije pronađeno",
                        "info": "Prikaz stranice _PAGE_ od _PAGES_",
                        "search": "Traži",
                        "infoEmpty": "Nema redova",
                        "infoFiltered": "(filtrirano od _MAX_ ukupno redova)",
                        "paginate": {
                            "first": "Prva",
                            "last": "Poslednja",
                            "next": ">",
                            "previous": "<"
                        },
                    },
                    'bSort': false,
                    tableTools: {
                        "sSwfPath": "/plugin/copy_csv_xls_pdf.swf"
                    },
                    "footerCallback": function ( row, data, start, end, display ) {
                        var api = this.api(), data;

                        // converting to interger to find total
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        let tds = $("*[data-footer='true']", row);
                        $( api.column(0).footer() ).html('Ukupno');

                        $.each(tds, function($index, $elem){
                            if ($elem.hasAttribute("data-column") && $index != 0) {
                                let columnIndexToSearch = $($elem).attr("data-column");

                                let pageTotal = api
                                    .column(columnIndexToSearch, { page: 'current'} )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0 );

                                let total = api
                                    .column(columnIndexToSearch )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0 );

                                $( api.column($index).footer() ).html(
                                    pageTotal +' ( od '+ total +')'
                                );

                            }
                        });
                    },
                });

            });

        });
    })($);
</script>