<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-3 col-xs-12">
                        <div class="text-center">
                            <h3>Grupisi naloge</h3>
                            <a href="#" onclick="getForm('/warehouse-order-groups/get-grouped-form', '/warehouse-order-groups/grouped-warehouse-order-group', event, '')" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-archive"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj groupni nalog</h3>
                            <a href="#" onclick="getCreateForm('warehouse-order-groups', event)" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12">
                        <div class="text-center">
                            <h3>Automatski grupni nalog</h3>
                            <a href="/warehouse-order-groups/auto-group" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12">
                        <div class="text-center">
                            <h3>Automatsko pozivanje kurira</h3>
                            <a class="bigger-200 btn btn-circle btn-lg"
                               onclick="getAjaxForm('warehouse-order-groups/auto-call-courier', event)">
                                <i class="fa fa-envelope"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="space-25"></div>
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="col-xs-2">{{ t('Za') }}</th>
                            <th class="col-xs-1">{{ t('Tip') }}</th>
                            <th class="col-xs-2">{{ t('Spakovano') }}</th>
                            <th class="col-xs-2 text-center">{{ t('Status') }}</th>
                            <th class="col-xs-2 text-center">{{ t('MIS code') }}</th>
                            <th class="col-xs-2 text-center">{{ t('Akcija') }}</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        const isSuperAdmin = '{{ currentUser.isSuperAdmin() }}';

        const table = $("#example").DataTable( {
            "processing": true,
            "serverSide": true,
            "pageLength": 25,
            "paging": true,
            "ajax": {
                "url": "/warehouse-order-groups/get-list-warehouse-order-groups",
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (aData[6]) {
                    $('td', nRow).closest('tr').css('background', 'lightcoral');
                }
                else if (aData[7]) {
                    $('td', nRow).closest('tr').css('background', 'lightgreen');
                }

                return nRow;
            },
            "columns": [
                null,
                { className: "text-center" },
                { className: "text-center" },
                { className: "text-center" },
                { className: "text-center" },
                { className: "text-center" },
            ],
            "columnDefs": [
                {
                    "render": function ( data, type, row ) {
                        let html = '<a href="/warehouse-order-groups/add-items/'+ row[5] +'" class="bigger-140 text-danger"><button class="btn btn-info btn-circle" type="button" title="Pregled"><i class="fa fa-search-plus"></i></button></a>'+
                            '<a href="/warehouse-order-groups/print/'+ row[5] +'" class="bigger-140 text-danger"><button class="btn btn-danger btn-circle" type="button" title="Stampa"><i class="fa fa-print"></i></button></a>';
                        return html;
                    },
                    "targets": 5
                },
                {
                    "render": function (data, type, row) {
                        if (row[3] === 'SENT') {
                            return 'Poslato';
                        }

                        let checked = row[3] === 'LOCKED';
                        let attributes = '';
                        if (checked) {
                            attributes = ' checked ';
                            if (!isSuperAdmin) {
                                attributes = ' checked disabled ';
                            }
                        }

                        return '<span>Otkljucan </span><input type="checkbox" data-id="' + row[5] + '" name="status[' + row[5] + ']" value="" id="overtime"' + attributes + 'class="form-control js-switch-lock"><span> Zakljucan</span>';
                    },
                    "targets": 2
                }
            ],
            "fnDrawCallback": function() {
                $(".js-switch-lock").each(function (key, elem) {
                    if (!elem.hasAttribute("data-switchery")) {
                        let check = elem;
                        let switchery = new Switchery(elem, {color: "#1AB394"});
                        let warehouseGroupId = $(elem).attr("data-id");

                        elem.onchange = function () {
                            $(check).attr('checked', check.checked);
                            $(check).val(check.checked ? 1 : 0);
                            let data = new FormData();
                            data.append('status', check.checked);
                            sendPostAjaxCall("/warehouse-order-groups/set-status/" + warehouseGroupId, data);
                        }

                        if (elem.hasAttribute("disabled")) {
                            switchery.disable();
                        }
                    }
                });
            }
        });

    });
</script>