<style>
    h1 {
        color: navy;
        font-family: times;
        font-size: 18pt;
        text-decoration: underline;
    }

    p.first {
        color: #003300;
        font-family: helvetica;
        font-size: 12pt;
    }

    p.first span {
        color: #006600;
        font-style: italic;
    }

    p#second {
        color: rgb(00, 63, 127);
        font-family: times;
        font-size: 12pt;
        text-align: justify;
    }

    p#second > span {
        background-color: #FFFFAA;
    }

    table {
        border-collapse: separate;
        width: 100%;
    }

    table th {
        padding: 8px 5px;
        text-align: right;
        border-bottom: 4px solid #487774;
        background: #E6E7E7;

    }

    table td {
        text-align: right;
        border-right: 1px solid #CCCCCF;
    }


    .currency {
        border-bottom: 4px solid #487774;
        padding: 0 20px;
    }

    .currency span {
        font-size: 11px;
        font-style: italic;
        color: #8b8b8b;
        display: inline-block;
        min-width: 20px;
    }


</style>

<img src="<?php echo APP_PATH; ?>public/img/memo.png" alt="">
<div>
    <table style="text-align: center !important;">
        <tr>
            <td style="border: 2px solid #487774; !important;text-align: center !important;">
                <br/>KIDS BEBA DOO
                <br/><span>Ignjata Joba 37</span>
                <br/><span>Belgrade Serbia</span>
                <br/><span>PIB: 101743849</span>
{#                <br/><span><tcpdf method="write1DBarcode" params="{{ params }}"/></span>#}
            </td>
            <td style="border: none !important"></td>
{#            <td style="border: 2px solid #487774; !important;text-align: center !important;">#}
{#                <br/>{{ warehouseOrder.getChannelSupplyEntity().getDisplayName() }}#}
{#                <br/><span>{{ warehouseOrder.getChannelSupplyEntity().getPostalAddress()}}</span>#}
{#                <br/><span>{{ warehouseOrder.getChannelSupplyEntity().getEmail()}}</span>#}
{#                <br/><span>#}
{#                        <tcpdf method="write1DBarcode" params="{{ params }}"/>#}
{#                    </span>#}
{#            </td>#}
        </tr>
    </table>
</div>
<div style="text-align: center;font-size: 18px">
    <span>Spisak nalog za otpremu robe : <b>{{ warehouseOrderGroup.warehouseOrderGroupCode }}</b></span>
</div>
<div style="clear:both">
    <section id="items">
        <table cellpadding="0" cellspacing="0">

            <tr>
                <th width="10%">#</th> <!-- Dummy cell for the row number and row commands -->
                <th width="20%">Nalog</th>
                <th width="20%">Objekat</th>
                <th width="15%">Datum</th>
                <th width="55%">Note</th>
            </tr>
            {% set rbr = 1 %}
            {% for row in data %}
                <tr data-iterate="item" style="font-size: 12px !important;">
                    {# {% set ammount = row["qty"] * row["price"] %} #}
                    <td width="10%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ rbr }}</td>
                    <td width="20%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ row['warehouseOrderCode'] }}</td>
                    <td width="20%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ row['entity'] }}</td>
                    <td width="15%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ row['date'] }}</td>
                    <td width="55%" style="border-bottom: 1px solid #CCCCCF; !important;">{{ row['note'] }}</td>
                </tr>
                {% set rbr = rbr+1 %}
            {% endfor %}


        </table>

    </section>

    <div class="currency">
        <span></span> <span></span>
    </div>
    <div style="margin-bottom: 5px !important;">
        <table cellpadding="0" cellspacing="0" style="background: none !important">

            <tr style="background: none !important;border-bottom: 2px solid white; !important;">
                <td width="70%" style="text-align:left !important; padding-top: 5px !important;">
                </td>
                <td width="15%"
                    style="background-color: #8BA09E !important;border-bottom: 2px solid white; !important;">Ukupna kolicina
                </td>
                <td width="15%"
                    style="background-color: #8BA09E !important;border-bottom: 2px solid white; !important;">0</td>
            </tr>
        </table>
    </div>
</div>
</div>