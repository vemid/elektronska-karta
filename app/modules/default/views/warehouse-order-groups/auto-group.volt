<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Grupni nalog</h3>
                        </div>
                    </div>
                </div>
                <div class="space-25"></div>
                <div class="row">
                    <div class="col-xs-12 m-t-md">
                        <textarea name="auto-barcodes" id="auto-barcodes" rows="20"
                                  class="form-control tag-editor"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 m-t-md">
                        <button type="button" id="auto-group" class="btn btn-success">{{ t('Grupiši') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    (function ($) {
        window.onload = function () {
            let element = document.getElementById("barcodes");
            if (element) {
                element.focus();
            }
        }

        $(document).on("click", "body:not(input, textarea)", function (event) {
            let $barcode = $('#barcodes');
            if ($barcode.length > 0) {
                $barcode.focus();
            }
        });

        $(document).on("click", "#auto-group", function () {
            let url = "/warehouse-order-groups/make-groups";

            let request = $.ajax({
                url: url,
                type: "post",
                data: {groupedCodes: $("#auto-barcodes").val()}
            });

            request.done(function (response, textStatus, jqXHR) {
                let content = $(response).find("#content");
                let navbar = $(response).find(".navbar-top-links");
                $("#content").replaceWith(content);
                $(".navbar-top-links").replaceWith(navbar);
                setChosen();
                initChecks();

                $(".search-choice").each(function (key, value) {
                    let $1 = $(value);
                    let index = $("a", $1).attr("data-option-array-index");
                    let select = $1.parents(".chosen-container-multi").prev("select");
                    let option = $("option:eq(" + index + ")", select);
                    $("span", $1).addClass(option.attr("class"));
                })
            });
        });

        $(document).on("click", "#create-group-warehouse-order", function () {
            let parents = $(this).parents("#group-section");
            let warehouseOrderIds = parents.find("select");
            let channelSupplyEntityId = parents.find("#channelSupplyId");
            let courier = parents.find("#courier")[0];

            let form = new FormData();
            form.append('channelSupplyEntityId', channelSupplyEntityId.val());
            form.append('warehouseOrderIds', warehouseOrderIds.val());
            form.append('courier', courier.checked);

            let request = $.ajax({
                url: "/warehouse-order-groups/save-auto-group",
                type: "post",
                data: form,
                dataType: "json",
                processData: false,
                contentType: false,
                success: function (data) {
                    renderMessages(data.messages, data.error ? 'danger' : 'success');

                    if (!data.error) {
                        parents.remove();
                    }

                    if (!$("#group-section").length) {
                        window.location = "/warehouse-order-groups/list";
                    }
                }
            });

        })
    })(jQuery);
</script>