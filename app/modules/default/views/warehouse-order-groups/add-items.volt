<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Grupni nalog</h3>
                        </div>
                    </div>
                </div>
                <div class="space-25"></div>
                <div class="row">
                    <div class="col-xs-12 m-t-md">
                        <div class="feed-activity-list">
                            {% if warehouseOrderGroup.getWarehouseOrderGroupItems()|length == 0 %}
                                <table class="table warehouse-order-item-table">
                                    <tbody>
                                    <tr>
                                        <td class="text-center">
                                            <div class="col-xs-12" id="barcode-info-container">
                                                <p>{{ t('Stavke su prazne') }}</p>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="bar-container">
                                                    <div class="l1"></div>
                                                    <div class="l2"></div>
                                                    <div class="l3"></div>
                                                    <div class="l4"></div>
                                                    <div class="l5"></div>
                                                    <div class="l6"></div>
                                                    <div class="l7"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            {% else %}
                                <div class="feed-element">
                                    <div class="col-lg-4 bigger-120"><b>Nalog</b></div>
                                    <div class="col-lg-6 bigger-120"><b>Sadržaj</b></div>
                                    <div class="col-lg-2 col-xs-2 text-center bigger-120"><b>Akcija</b></div>
                                </div>

                                {% for warehouseOrderGroupItem in warehouseOrderGroup.getWarehouseOrderGroupItems() %}
                                    <div class="feed-element  no-padding" id="{{ warehouseOrderGroupItem.getWarehouseOrder().warehouseOrderCode|lower }}">
                                        <div class="col-lg-4">{{ warehouseOrderGroupItem.getWarehouseOrder().getWarehouseOrderCode() }}</div>
                                        <div class="col-lg-6">{{ warehouseOrderGroupItem.getWarehouseOrder().getProductListFromWarehouseOrder() }}</div>
                                        <div class="col-lg-2 col-xs-2 text-center">
                                            {% if warehouseOrderGroup.isEditable() %}
                                                <a data-id="{{ warehouseOrderGroupItem.id }}" data-code="{{ warehouseOrderGroupItem.getWarehouseOrder().getWarehouseOrderCode() }}" id="remove-saved-warehouse-order-group-item" class="btn btn-danger btn-circle"><i class="fa fa-times"></i></a>
                                            {% endif %}
                                        </div>
                                    </div>
                                {% endfor %}
                                {% if warehouseOrderGroup.isEditable() %}
                                    <div class="feed-element">
                                        <div class="col-lg-12 m-t-md">
                                            <a onclick="return false;" id="save-warehouse-order-group-items" class="btn btn-label btn-success">Snimi stavke</a>
                                        </div>
                                    </div>
                                {% endif %}
                            {% endif %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{% if warehouseOrderGroup.isEditable() %}
    <form action="" id="barcode-form">
        <input type="hidden" id="warehouse-order-group-id" name="warehouse-order-group-id" value="{{ warehouseOrderGroup.id }}">
        <input type="text" id="barcode-warehouse-group" name="barcode-warehouse-group">
        <button type="submit" id="barcode-submit">tet</button>
    </form>
{% endif %}

<script type="text/javascript">
    (function ($) {
        $(document).on("click", "body:not(input, textarea)", function(event){
            if (event.target.tagName.toLowerCase() === 'textarea' || event.target.tagName.toLowerCase() === 'select') {
                return false;
            }

            let $barcode = $('#barcode-warehouse-group');
            if ($barcode.length > 0 ) {
                $barcode.focus();
            }
        });

        window.onload = function() {
            let element = document.getElementById("barcode-warehouse-group");
            if (element) {
                element.focus();
            }
        }

        let warehouseOrderGroupId = $("#warehouse-order-group-id").val();
        const storageIndex = warehouseOrderGroupId + 'G';

        let isEditable = {{ warehouseOrderGroup.isEditable() }};
        let alreadyStored = {{ warehouseOrderGroup.getWarehouseOrderGroupItems()|length }};
        let serverStoredBarcodes = JSON.parse('{{ serverStoredBarcodes }}');
        let barcodeStorage = window.localStorage;
        let warehouseOrderGroupStorage = barcodeStorage.getItem(storageIndex);
        let storedBarcodes = JSON.parse(warehouseOrderGroupStorage);
        if (!alreadyStored) {
            if (warehouseOrderGroupStorage && storedBarcodes && Object.keys(storedBarcodes).length > 0) {
                $(".warehouse-order-item-table").remove();
                let header =
                    '<div class="feed-element">' +
                    '<div class="col-lg-4 bigger-120"><b>Nalog</b></div>' +
                    '<div class="col-lg-6 bigger-120"><b>Sadržaj</b></div>' +
                    '<div class="col-lg-2 text-center bigger-120"><b>Akcija</b></div>' +
                    '</div>';

                $(".feed-activity-list").append(header);

                for (let barcode in storedBarcodes) {
                    let counter = storedBarcodes[barcode];
                    $.get("/warehouse-order-groups/get-warehouse-order/" + barcode, function(response) {
                        let row =
                            '<div class="feed-element no-padding" id="'+ barcode.toLowerCase() + '">' +
                            '<div class="col-lg-4 col-xs-5">' + response.warehouseOrder + '</div>' +
                            '<div class="col-lg-6">' + response.items + '</div>' +
                            '<div class="col-lg-2 col-xs-2 text-center">' +
                            '<a id="remove-warehouse-order-group-item" class="btn btn-danger btn-circle" onclick="return false;" data-id="'+ barcode +'"><i class="fa fa-times"></i></a>' +
                            '</div>' +
                            '</div>';

                        $(".feed-activity-list > div:eq(0)").after(row);
                    });
                }

                if (isEditable) {
                    let footer =
                        '<div class="feed-element">' +
                        '<div class="col-lg-12 m-t-md">' +
                        '<a onclick="return false;" id="save-warehouse-order-group-items" class="btn btn-label btn-success">Snimi stavke</a>' +
                        '</div>' +
                        '</div>';

                    $(".feed-activity-list").append(footer);
                }
            }
        } else {
            if (storedBarcodes) {
                for (let barcode in storedBarcodes) {
                    let $1 = $("#" + barcode.toLowerCase());

                    if ($1.length === 0) {
                        let counter = storedBarcodes[barcode];
                        $.get("/warehouse-order-groups/get-warehouse-order/" + barcode, function(response) {
                            let row =
                                '<div class="feed-element no-padding" id="'+ barcode.toLowerCase() + '">' +
                                '<div class="col-lg-4">' + response.warehouseOrder + '</div>' +
                                '<div class="col-lg-6">' + response.items + '</div>' +
                                '<div class="col-lg-2 col-xs-2 text-center">' +
                                '<a id="remove-warehouse-order-group-item" class="btn btn-danger btn-circle" onclick="return false;" data-id="'+ barcode +'"><i class="fa fa-times"></i></a>' +
                                '</div>' +
                                '</div>';

                            $(".feed-activity-list > div:eq(0)").after(row);
                        });
                    } else {
                        $1.find("#row-counter").empty().html(storedBarcodes[barcode]);
                    }
                }
            } else {
                storedBarcodes = {};
                for (let key in serverStoredBarcodes) {
                    if (serverStoredBarcodes.hasOwnProperty(key)) {
                        storedBarcodes[key] = serverStoredBarcodes[key];
                        barcodeStorage.setItem(storageIndex, JSON.stringify(storedBarcodes));
                    }
                }
            }
        }

        $(document).on("focusout", "#barcode-warehouse-group", function () {
            let barcode = $(this).val().trim();
            if (!barcode || (barcode.length !== 13 && barcode.length !== 14)) {
                return false;
            }

            $(this).focus().val("");

            let barcodeStorage = window.localStorage;
            let warehouseOrderGroupStorage = barcodeStorage.getItem(storageIndex);
            let storedBarcodes = JSON.parse(warehouseOrderGroupStorage);

            if (storedBarcodes === null) {
                storedBarcodes = {};

                $(".warehouse-order-item-table").remove();
                let header = '<div class="feed-element">' +
                    '<div class="col-lg-4 bigger-120"><b>Nalog</b></div>' +
                    '<div class="col-lg-6 bigger-120"><b>Sadržaj</b></div>' +
                    '<div class="col-lg-2 text-center bigger-120"><b>Akcija</b></div>';

                $(".feed-activity-list").append(header);

                let footer = '<div class="feed-element">' +
                    '<div class="col-lg-12 m-t-md">' +
                    '<a onclick="return false;" id="save-warehouse-order-group-items" class="btn btn-label btn-success">Snimi stavke</a>' +
                    '</div>' +
                    '</div>';
                $(".feed-activity-list").append(footer);
            }

            let initialBarcode = false;
            if(typeof storedBarcodes[barcode] === 'undefined') {
                initialBarcode = true;
            }

            if (initialBarcode) {
                $.get("/warehouse-order-groups/get-warehouse-order/" + barcode, function(response) {
                    if (response.error) {
                        $(this).focus().val("");
                        renderMessages(response.messages, "danger");
                    } else {
                        storedBarcodes[barcode] = response.key;
                        barcodeStorage.setItem(storageIndex, JSON.stringify(storedBarcodes));

                        let row = '<div class="feed-element no-padding" id="'+ barcode + '">' +
                            '<div class="col-lg-4 col-xs-5">' + response.warehouseOrder + '</div>' +
                            '<div class="col-lg-6 col-xs-2">' + response.items + '</div>' +
                            '<div class="col-lg-2 col-xs-2 text-center"><a href="#" id="remove-warehouse-order-group-item" data-id="'+ barcode +'" class="btn btn-danger btn-circle"><i class="fa fa-times"></i></a></div>';

                        $(".feed-element:eq(0)").after(row);
                    }

                    $(this).focus().val("");
                });
            } else {
                $(this).focus().val("");
            }

            $(this).focus().val("");
        });

        $("#barcode-form").submit(function(e){
            e.preventDefault();
            $("#barcode-warehouse-group").focus().val("");
        });

        $(document).on("click", "#remove-warehouse-order-group-item", function(){
            let barcodeStorage = window.localStorage;
            let warehouseOrderStorage = barcodeStorage.getItem(storageIndex);
            let storedBarcodes = JSON.parse(warehouseOrderStorage);
            let barcode = $(this).attr("data-id");
            console.log(storedBarcodes, barcode);

            if (storedBarcodes === null) {
                return false;
            }

            if(typeof storedBarcodes[barcode] === 'undefined') {
                return false;
            }

            delete storedBarcodes[barcode];
            $(this).parents(".feed-element").remove();
            barcodeStorage.setItem(storageIndex, JSON.stringify(storedBarcodes));
        });


        $(document).on("click", "#save-warehouse-order-group-items", function(e){
            e.preventDefault();

            let warehouseOrderGroupId = $("#warehouse-order-group-id").val();
            let barcodeStorage = window.localStorage;
            let warehouseOrderStorage = barcodeStorage.getItem(storageIndex);
            let storedBarcodes = JSON.parse(warehouseOrderStorage);
            if (storedBarcodes === null) {
                alert("Ne postoje stavke za unos!");
                return false;
            }

            let postData = new FormData();
            let index = 0;
            let CheckVal = '';
            let validation = [];
            let validationB = [];
            $.each(storedBarcodes, function(key, value) {
                postData.append('codes['+ index +']', key);
                index++;
                if (CheckVal !== '' && CheckVal !== value && !validationB.includes(value)) {
                    validation.push(value);
                } else {
                    validationB.push(value);
                }

                CheckVal = value;
            });

            if (validation.length) {
                validation.forEach((x, i) => {
                    let barcode = Object.keys(storedBarcodes).find(key => storedBarcodes[key] === x);
                    if (barcode) {
                        let $1 = $("#" + barcode.toLowerCase());
                        if ($1.length) {
                            $1.addClass('alert alert-danger');
                        }
                    }
                });

                alert("Neki dzak nije dobro grupisan");
                return  false;
            }

            $.ajax({
                url: "/warehouse-order-groups/store-items/" + warehouseOrderGroupId,
                type: "post",
                data: postData,
                dataType: "json",
                enctype: "multipart/form-data",
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.error) {
                        renderMessages(data.messages, "danger");
                    } else if (typeof data.url !== "undefined") {
                        barcodeStorage.removeItem(storageIndex);
                        window.location = data.url;
                        $("#modal").hide();
                        $("body").removeClass("modal-open");
                        initTablsOnLoad();
                        initSwitch();
                    }
                }
            });
        });

        $(document).on("click", "#remove-saved-warehouse-order-group-item", function (){
            let id = $(this).attr("data-id");
            let barcode = $(this).attr("data-code");

            let url = "/warehouse-order-group-items/delete/" + id;
            swal({
                title: t("Da li ste sigurni?!"),
                text: "",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: t("Odustani"),
                confirmButtonColor: "#da180f",
                confirmButtonText: t("Da, Obriši!"),
                closeOnConfirm: true
            }, function () {
                let barcodeStorage = window.localStorage;
                let warehouseOrderStorage = barcodeStorage.getItem(storageIndex);
                let storedBarcodes = JSON.parse(warehouseOrderStorage);

                if (storedBarcodes === null) {
                    return false;
                }

                if(typeof storedBarcodes[barcode] === 'undefined') {
                    return false;
                }

                delete storedBarcodes[barcode];
                barcodeStorage.setItem(storageIndex, JSON.stringify(storedBarcodes));
                sendPostAjaxCall(url, {})
            });
        });

    })(jQuery);
</script>