<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">

                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Dodaj groupni nalog</h3>
                            <a href="#" class="bigger-200 btn btn-circle btn-lg">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-horizontal">
                        <div class="col-lg-6 col-xs-12">
                            <div class="form-group">
                                {% set channelSupply = form.get('channelSupply') %}
                                <div class="col-xs-5"><b class="bigger-120">1.</b> {{ t(channelSupply.getLabel()) }}
                                </div>
                                <div class="col-xs-7">
                                    {{ channelSupply }}
                                </div>
                            </div>
                            <div class="form-group">
                                {% set channelSupplyEntities = form.get('channelSupplyEntities') %}
                                <div class="col-xs-5"><b
                                            class="bigger-120">2.</b> {{ t(channelSupplyEntities.getLabel()) }}
                                </div>
                                <div class="col-xs-7">
                                    {{ channelSupplyEntities }}
                                </div>
                                <div class="col-xs-5"></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="space-25"></div>
                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="col-xs-2">{{ t('Za') }}</th>
                            <th class="col-xs-2">{{ t('Spakovano') }}</th>
                            <th class="col-xs-2 text-center">{{ t('Planirani izlazak') }}</th>
                            <th class="col-xs-2 text-center">{{ t('Order') }}</th>
                            <th class="col-xs-2 text-center">{{ t('MIS code') }}</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        const table = $("#example").DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/warehouse-order-groups/get-list-data",
                "data":  function ( d ) {
                    return $.extend({}, d, {
                        "channelSupplyId": $('#channelSupplyId').val()
                    });
                }
            },
            "columns": [
                null,
                { className: "text-center" },
                { className: "text-center" },
                { className: "text-center" },
            ],
            "columnDefs": [
                {
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }
            ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            },
            order: [[ 1, 'asc' ]],

            "fnDrawCallback": function() {
            }
        });

    });
</script>