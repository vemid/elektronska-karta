<div class="row">
    <div class="col-xs-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center">
                            <h3>Grupni nalog</h3>
                        </div>
                    </div>
                </div>
                <div class="space-25"></div>
                {% if usedWarehouseOrders|length and groupedWarehouseOrders|length === 0 %}
                    <div class="alert alert-danger">{{ 'Ne postoje validni nalozi za grupisanje ' }}</div>
                {% endif %}
                {% if usedWarehouseOrders|length %}
                    <p>Već spakovani džakovi:
                        {% for usedWarehouseOrder in usedWarehouseOrders %}
                            {{ usedWarehouseOrder.warehouseOrderCode }}{{ loop.last ? '' : ', ' }}
                        {% endfor %}
                    </p>
                {% endif %}
                {% for key, entityType in entityNames %}
                    {% for type, entityName in entityType %}
                        {% set ordersByEntity = groupedWarehouseOrders[key][type] %}
                        {% if entityName.entityTypeId == 5 %}
                        <div class="row form-horizontal" id="group-section">
                            <div class="col-xs-12">
                                <h4>{{ entityName }}{{ type === 'B' ? ' B' : '' }}</h4>
                            </div>
                            <div class="col-xs-8">
                                <input type="hidden" name="channelSupplyId" id="channelSupplyId" value="{{ key }}">
                                <select name="warehouse-order[]" id="warehouse-order" multiple class="form-control chosen-select">
                                    {% set canCreateGroupOrder = true %}
                                    {% for warehouseOrder in ordersByEntity %}
                                        {% if not warehouseOrder.pushedToMis %}
                                            {% set canCreateGroupOrder = false %}
                                        {% endif %}
                                    <option{{ warehouseOrder.pushedToMis ? '' : ' class="text-danger"'}} value="{{ warehouseOrder.id }}" selected>{{ warehouseOrder.warehouseOrderCode }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <div class="checkbox-inline">
                                    <label for="courier">Slanje kuriskom: </label>
                                    <input class="i-checks" type="checkbox" id="courier" name="courier">
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <button type="button" id="create-group-warehouse-order"{{ canCreateGroupOrder ? '' : ' disabled' }} class="btn btn-success">{{ t('Kreiraj grupni nalog') }}</button>
                            </div>
                        </div>
                        {% endif %}
                    {% endfor %}
                {% endfor %}
                {% for key, entityType in entityNames %}
                    {% for type, entityName in entityType %}
                        {% set ordersByEntity = groupedWarehouseOrders[key][type] %}
                        {% if entityName.entityTypeId == 9 %}
                            <div class="row form-horizontal" id="group-section">
                                <div class="col-xs-12">
                                    <h4>{{ entityName }}</h4>
                                </div>
                                <div class="col-xs-8">
                                    <input type="hidden" name="channelSupplyId" id="channelSupplyId" value="{{ key }}">
                                    <select name="warehouse-order[]" id="warehouse-order" multiple class="form-control chosen-select">
                                        {% set canCreateGroupOrder = true %}
                                        {% for warehouseOrder in ordersByEntity %}
                                            {% if not warehouseOrder.pushedToMis %}
                                                {% set canCreateGroupOrder = false %}
                                            {% endif %}
                                            <option{{ warehouseOrder.pushedToMis ? '' : ' class="text-danger"' }} value="{{ warehouseOrder.id }}" selected>{{ warehouseOrder.warehouseOrderCode }}</option>
                                        {% endfor %}
                                    </select>
                                </div>
                                <div class="col-xs-2">
                                    <div class="checkbox-inline">
                                        <label for="courier">Slanje kuriskom: </label>
                                        <input class="i-checks" type="checkbox" id="courier" name="courier">
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <button type="button" id="create-group-warehouse-order"{{ canCreateGroupOrder ? '' : ' disabled' }} class="btn btn-success">{{ t('Kreiraj grupni nalog') }}</button>
                                </div>
                            </div>
                        {% endif %}
                    {% endfor %}
                {% endfor %}
            </div>
        </div>
    </div>
</div>