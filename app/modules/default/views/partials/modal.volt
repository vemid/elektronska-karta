<div class="modal inmodal" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog text-center">
        <div class="modal-content animated fadeIn no-padding-left text-left">
            <div class="modal-header">
                <button type="button" class="close destroy-modal" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">{{ t('Close') }}</span>
                </button>
                <h4 class="modal-title"></h4>

                <div class="flash-notification"></div>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default destroy-modal" data-dismiss="modal" id="close-modal">{{ t('Close') }}</button>
                <button type="button" class="btn btn-success" id="save-modal">{{ t('Save changes') }}</button>
            </div>
        </div>
    </div>
</div>