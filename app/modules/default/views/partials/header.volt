<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" style="margin-bottom: 0" role="navigation">
        <div class="navbar-header col-xs-6 no-padding">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>

            <form class="navbar-form-custom" action="" role="search">
                <div class="form-group">
                    <input id="top-search border" class="form-control bordered" type="text" name="top-search" placeholder="{{ t('Pretraga') }}">
                </div>
            </form>
        </div>
        <ul class="nav navbar-top-links navbar-right">

            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="true">
                    <i class="fa fa-bell"></i>  <span class="label label-warning">{{ totalActiveNotifications }}</span>
                </a>
                <ul class="dropdown-menu dropdown-alerts">
                    {% for message in activeNotifications %}
                        {% set cls = 'cls' %}
                        {% if message.isOverDue() %}
                            {% set cls = 'alert alert-danger' %}
                        {% elseif message.isGonnaBeOverDue() %}
                            {% set cls = 'alert alert-warning' %}
                        {% endif %}
                    <li>
                        <a href="#" class="{{ cls }}">
                            <div>
                                <i class="fa fa-envelope fa-fw"></i> <span class="">{{ message.message }}</span>
                                <span class="pull-right {{ cls == 'cls' ? 'text-muted ' : '' }}small">{{ message.dueDate.short }}</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    {% endfor %}
                    <li>
                        <div class="text-center link-block">
                            <a href="/user-notifications/list" class="block">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
            <li>
                <a href="/auth/logout">
                    <i class="fa fa-sign-out"></i>
                    Log out
                </a>
            </li>
        </ul>
    </nav>
</div>