<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul id="side-menu" class="nav metismenu">
            <li class="nav-header">
                {% if currentUser %}
                <div class="dropdown profile-element text-center">
                    <span>
{#                        <img class="img-circle" width="60px" src="{{ currentUser.getAvatarUrl() }}" alt="image">#}
                    </span>
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                        <span class="clear">
                            <span class="text-muted text-xs block">
                                {{ currentUser.displayName }}
                            </span>
                        </span>
                    </a>
                </div>
                {% endif %}
            </li>
            <li class="{{ request.getURI() == '/' ? 'active' : '' }}">
                <a href="/">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">{{ t('Početna') }}</span>
                </a>
            </li>
            <?php if ($this->aclHelper->isAllowed('default/orders', '*')): ?>
            <li class="{{ dispatcher.getControllerName() == 'orders' ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">{{ t('Porudzbina') }}</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse" style="">
                    {% if activeOrder %}
                    <li class="{{ dispatcher.getActionName() == 'list-products' ? 'active' : '' }}">
                        <a href="/orders/list-products/{{ activeOrder.id }}">
                            <i class="fa fa-shopping-basket"></i>
                            <span class="nav-label">{{ t('Poručivanje Robe') }}</span>
                        </a>
                    </li>
                    {% else %}
                        <li class="{{ dispatcher.getActionName() == 'list-products' ? 'active' : '' }}">
                            <a href="/orders/choose-order" id="choose-order">
                                <i class="fa fa-shopping-basket"></i>
                                <span class="nav-label">{{ t('Poručivanje Robe') }}</span>
                            </a>
                        </li>
                    {% endif %}
                    {% if aclHelper.isAllowed('default/orders', 'list') %}
                    <li class="{{ dispatcher.getActionName() == 'list' or dispatcher.getActionName() == 'overview' or dispatcher.getActionName() == 'listDetails' ? 'active' : '' }}">
                        <a href="/orders/list">
                            <i class="fa fa-shopping-basket"></i>
                            <span class="nav-label">{{ t('Lista Porudzbina') }}</span>
                        </a>
                    </li>
                    {% endif %}
                    {% if aclHelper.isAllowed('default/receiver-notes', 'search-by-code') %}
                        <li class="{{ dispatcher.getActionName() == 'search-by-code' ? 'active' : '' }}">
                            <a href="/delivery-notes/list">
                                <i class="fa fa-shopping-basket"></i>
                                <span class="nav-label">{{ t('Prenos porudzbine') }}</span>
                            </a>
                        </li>
                    {% endif %}
                </ul>
            </li>
            <?php endif ?>
            <?php if ($this->aclHelper->isAllowed('default/orders', '*')): ?>
            <li class="{{ dispatcher.getControllerName() == 'invoices' ? 'active' : '' }}">
                <a href="#">
                    <i class="fas fa-file-invoice-dollar"></i>
                    <span class="nav-label">{{ t('Invoices') }}</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse" style="">
                        <li class="{{ dispatcher.getActionName() == 'list' ? 'active' : '' }}">
                            <a href="/invoices/list">
                                <i class="fas fa-file-invoice-dollar"></i>
                                <span class="nav-label">{{ t('Invoice List') }}</span>
                            </a>
                        </li>
                </ul>
            </li>
            <?php endif ?>
            {% if aclHelper.isAllowed('default/suppliers', '*') %}
            <li class="{{ dispatcher.getControllerName() == 'suppliers' ? 'active' : '' }}">
                <a href="#">
                    <i class="fas fa-cubes"></i>
                    <span class="nav-label">{{ t('Sifarnik') }}</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse" style="">
                    <li>
                        <a href="/suppliers/list">
                            <i class="fa fa-truck"></i>
                            <span class="nav-label">{{ t('Dobavljaci') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="/clients/list">
                            <i class="fa fa-user-circle"></i>
                            <span class="nav-label">{{ t('Klijenti') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="/washing-care-label/list">
                            <i class="fa fa-warning"></i>
                            <span class="nav-label">{{ t('Oznake odrzavanja') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            {% endif %}
            {% if aclHelper.isAllowed('default/suppliers', '*') %}
                <li class="{{ (dispatcher.getControllerName() == 'merchandise-calculations' or dispatcher.getControllerName() == 'product-calculations' or dispatcher.getControllerName() == 'product-calculation-maps') ? 'active' : '' }}">
                    <a href="#">
                        <i class="fas fa-cubes"></i>
                        <span class="nav-label">{{ t('Kalkulacije') }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li>
                            <a href="/merchandise-calculations/list">
                                <i class="fa fa-truck"></i>
                                <span class="nav-label">{{ t('Kalk. Trgovacke') }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="/product-calculations/list">
                                <i class="fa fa-user-circle"></i>
                                <span class="nav-label">{{ t('Kalk. Proizvodnje') }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="/product-calculation-maps/list">
                                <i class="fa fa-random"></i>
                                <span class="nav-label">{{ t('Mapiranje') }}</span>
                            </a>
                        </li>
                    </ul>
                </li>
            {% endif %}
            {% if aclHelper.isAllowed('default/products', '*') %}
            <li class="{{ (dispatcher.getControllerName() == 'products' or dispatcher.getControllerName() == 'classification-sizes' or dispatcher.getControllerName() == 'classification-old-collection-map' or dispatcher.getControllerName() == 'electron-cards') ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">Roba</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse" style="">
                    <li class="{{ (dispatcher.getControllerName() == 'products' and dispatcher.getActionName() == 'list') ? 'active' : '' }}">
                        <a href="/products/list">
                            <i class="fa fa-users"></i>
                            <span class="nav-label">{{ t('Pregled Robe') }}</span>
                        </a>
                    </li>
                    <li>
                        <ul class="nav nav-second-level collapse in" style="">
                            <li class="{{ (dispatcher.getControllerName() == 'classification-sizes'  or dispatcher.getControllerName() == 'classification-old-collection-map') ? 'active' : '' }}">
                                <a href="#">
                                    <i class="fa fa-random"></i>
                                    <span class="nav-label">{{ t('Mapiranja') }}</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-third-level collapse" style="">
                                    <li class="{{ (dispatcher.getControllerName() == 'classification-sizes' and dispatcher.getActionName() == 'list') ? 'active' : '' }}">
                                        <a href="/classification-sizes/list">
                                            <i class="fa fa-users"></i>
                                            <span class="nav-label">{{ t('Velicine') }}</span>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="nav nav-third-level collapse" style="">
                                    <li class="{{ (dispatcher.getControllerName() == 'classification-old-collection-map' and dispatcher.getActionName() == 'list') ? 'active' : '' }}">
                                        <a href="/classification-old-collection-map/list">
                                            <i class="fa fa-users"></i>
                                            <span class="nav-label">{{ t('Kolekcije') }}</span>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="nav nav-third-level collapse" style="">
                                    <li class="{{ (dispatcher.getControllerName() == 'priorities' and dispatcher.getActionName() == 'list') ? 'active' : '' }}">
                                        <a href="/priorities/list">
                                            <i class="fa fa-users"></i>
                                            <span class="nav-label">{{ t('Prioriteti') }}</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="{{ (dispatcher.getControllerName() == 'electron-cards' and dispatcher.getActionName() == 'listNoneMaterialFactory') ? 'active' : '' }}">
                        <a href="/electron-cards/list-none-material-factory">
                            <i class="fa fa-address-card"></i>
                            <span class="nav-label">{{ t('Pregled El. Karti') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            {% endif %}
            {% if aclHelper.isAllowed('default/material-samples', '*') %}
            <li class="{{ dispatcher.getControllerName() == 'material-samples' ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">Materijali</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse" style="">
                    <li>
                        <a href="/material-samples/list">
                            <i class="fa fa-users"></i>
                            <span class="nav-label">{{ t('Pregled Kupona') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="/material-factories/list">
                            <i class="fa fa-users"></i>
                            <span class="nav-label">{{ t('Kartice Materijala') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="/farben-cards/list">
                            <i class="fa-solid fa-abacus"></i>
                            <span class="nav-label">{{ t('Farben Karte') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
           {% endif %}
            {% if aclHelper.isAllowed('default/product-calculations', '*') or aclHelper.isAllowed('default/product-calculation-maps', '*')
                or aclHelper.isAllowed('default/operating-lists', '*') %}
                <li class="{{ dispatcher.getControllerName() == 'production-workers'
                or dispatcher.getControllerName() == 'operating-lists' or dispatcher.getControllerName() == 'cutting-jobs' or dispatcher.getControllerName() == 'operating-list-item-types'  ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-industry"></i>
                        <span class="nav-label">{{ t('Proizvodnja') }}</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li>
                            <ul class="nav nav-second-level collapse in" style="">
                                <li class="{{ (dispatcher.getControllerName() == 'operating-list-item-type'  or dispatcher.getControllerName() == 'production_machines') ? 'active' : '' }}">
                                    <a href="#">
                                        <i class="fa fa-users"></i>
                                        <span class="nav-label">{{ t('Radnici') }}</span>
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'production-workers' and dispatcher.getActionName() == 'list') ? 'active' : '' }}">
                                            <a href="/production-workers/list">
                                                <i class="fa fa-users"></i>
                                                <span class="nav-label">{{ t('Lista radnika') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'absence-works' and dispatcher.getActionName() == 'list') ? 'active' : '' }}">
                                            <a href="/absence-works/list">
                                                <i class="fa fa-calendar"></i>
                                                <span class="nav-label">{{ t('Pregled Odsustva') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li>
                                            <a href="#" onclick="getCreateForm('absence-works',event)">
                                                <i class="fa fa-dashcube"></i>
                                                <span class="nav-label">{{ t('Odsustvo') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'absence-works' and dispatcher.getActionName() == 'temporaryList') ? 'active' : '' }}">
                                            <a href="/absence-works/absence-list">
                                                <i class="fa fa-calendar"></i>
                                                <span class="nav-label">{{ t('Pregled svih unosa') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="{{ (dispatcher.getControllerName() == 'cutting-jobs') ? 'active' : '' }}">
                            <ul class="nav nav-second-level collapse in" style="">
                                <li class="{{ dispatcher.getControllerName() == 'cutting-jobs'  ? 'active' : '' }}">
                                    <a href="#">
                                        <i class="fa fa-cut"></i>
                                        <span class="nav-label">{{ t('Rezijski poslovi') }}</span>
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'cutting-jobs' and dispatcher.getActionName() == 'list') ? 'active' : '' }}">
                                            <a href="/cutting-jobs/list">
                                                <i class="fa fa-cut"></i>
                                                <span class="nav-label">{{ t('Spisak rezija') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'cutting-jobs' and dispatcher.getActionName() == 'reports') ? 'active' : '' }}">
                                            <a href="/cutting-jobs/reports">
                                                <i class="fa fa-product-hunt"></i>
                                                <span class="nav-label">{{ t('Pregled rezija') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'cutting-jobs' and dispatcher.getActionName() == 'pivot-cutting-report-data') ? 'active' : '' }}">
                                            <a href="/cutting-jobs/pivot-cutting-report-data">
                                                <i class="fa fa-percent"></i>
                                                <span class="nav-label">{{ t('Pivot Izvestaj') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'cutting-jobs' and dispatcher.getActionName() == 'report-by-sector-worker') ? 'active' : '' }}">
                                            <a href="/cutting-jobs/report-by-sector-worker">
                                                <i class="fa fa-calculator"></i>
                                                <span class="nav-label">{{ t('Pregled rezija po radnicima') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'cutting-jobs' and dispatcher.getActionName() == 'report-by-sector') ? 'active' : '' }}">
                                            <a href="/cutting-jobs/report-by-sector">
                                                <i class="fa fa-calculator"></i>
                                                <span class="nav-label">{{ t('Pregled rezija sumarno') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <ul class="nav nav-second-level collapse in" style="">
                                <li class="{{ (dispatcher.getControllerName() == 'failures') ? 'active' : '' }}">
                                    <a href="#">
                                        <i class="fa fa-ambulance"></i>
                                        <span class="nav-label">{{ t('Feleri') }}</span>
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'failures' and dispatcher.getActionName() == 'list') ? 'active' : '' }}">
                                            <a href="/failures/list">
                                                <i class="fa fa-ambulance"></i>
                                                <span class="nav-label">{{ t('Pregled Felera') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'failures' and dispatcher.getActionName() == 'pivot-report') ? 'active' : '' }}">
                                            <a href="/failures/test-pivot" id="">
                                                <i class="fa fa-ambulance"></i>
                                                <span class="nav-label">{{ t('Pivot Felera') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <ul class="nav nav-second-level collapse in" style="">
                                <li class="{{ (dispatcher.getControllerName() == 'operating-lists'  or dispatcher.getControllerName() == 'reports') ? 'active' : '' }}">
                                    <a href="#">
                                        <i class="fa fa-users"></i>
                                        <span class="nav-label">{{ t('Operacione liste') }}</span>
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'operating-lists' and dispatcher.getActionName() == 'list') ? 'active' : '' }}">
                                            <a href="/operating-lists/list">
                                                <i class="fa fa-database"></i>
                                                <span class="nav-label">{{ t('Lista Operacionih listi') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'reports' and dispatcher.getActionName() == 'out-term-by-sector') ? 'active' : '' }}">
                                            <a href="/reports/out-term-by-sector">
                                                <i class="fa fa-calculator"></i>
                                                <span class="nav-label">{{ t('Pregled Ucinka po operacijama') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'reports' and dispatcher.getActionName() == 'out-term-by-sector-worker') ? 'active' : '' }}">
                                            <a href="/reports/out-term-by-sector-worker">
                                                <i class="fa fa-calendar"></i>
                                                <span class="nav-label">{{ t('Pregled Ucinka po oper. i radnicima') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" onclick="getForm('/operating-list-items/find-by-barcode', '/operating-list-items/find-operating-item', event, '')">
                                <i class="fa fa-barcode"></i>
                                <span class="nav-label">{{ t('Unos ucinka') }}</span>
                            </a>
                        </li>
                        <li>
                            <ul class="nav nav-second-level collapse in" style="">
                                <li class="{{ (dispatcher.getControllerName() == 'operating-list-item-type'  or dispatcher.getControllerName() == 'production_machines') ? 'active' : '' }}">
                                    <a href="#">
                                        <i class="fa fa-sitemap"></i>
                                        <span class="nav-label">{{ t('Tipovi') }}</span>
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'operating-list-item-type' and dispatcher.getActionName() == 'list') ? 'active' : '' }}">
                                            <a href="/operating-list-item-types/list">
                                                <i class="fa fa-dashcube"></i>
                                                <span class="nav-label">{{ t('Tipovi Operacija') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'production_machines' and dispatcher.getActionName() == 'list') ? 'active' : '' }}">
                                            <a href="/production-machines/list">
                                                <i class="fa fa-product-hunt"></i>
                                                <span class="nav-label">{{ t('Tipovi Masina') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="{{ (dispatcher.getControllerName() == 'production_workers' and dispatcher.getControllerName() == 'reports') ? 'active' : '' }}">
                            <ul class="nav nav-second-level collapse in" style="">
                                <li class="{{ (dispatcher.getControllerName() == 'production_workers') ? 'active' : '' }}">
                                    <a href="#">
                                        <i class="fa fa-calculator"></i>
                                        <span class="nav-label">{{ t('Izvestaji') }}</span>
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'reports' and dispatcher.getActionName() == 'payment-report-by-worker') ? 'active' : '' }}">
                                            <a href="/reports/payment-report-by-worker">
                                                <i class="fa fa-user-secret"></i>
                                                <span class="nav-label">{{ t('Preged ucinka po danima') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'production_workers' and dispatcher.getActionName() == 'list-by-worker') ? 'active' : '' }}">
                                            <a href="/production-workers/list-by-worker">
                                                <i class="fa fa-calendar"></i>
                                                <span class="nav-label">{{ t('Preged ucinka Radnici') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'reports' and dispatcher.getActionName() == 'performance-review') ? 'active' : '' }}">
                                            <a href="/reports/performance-review">
                                                <i class="fa fa-calendar"></i>
                                                <span class="nav-label">{{ t('Preged ucinka sektori') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'production_workers' and dispatcher.getActionName() == 'report-work-time') ? 'active' : '' }}">
                                            <a href="/production-workers/report-work-time">
                                                <i class="fa fa-calendar"></i>
                                                <span class="nav-label">{{ t('Preged radnog vremena') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'operating-lists' and dispatcher.getActionName() == 'non-finished') ? 'active' : '' }}">
                                            <a href="/operating-lists/non-finished">
                                                <i class="fa fa-calendar"></i>
                                                <span class="nav-label">{{ t('Preged nedovrsene proizvodnje') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="nav nav-third-level collapse" style="">
                                        <li class="{{ (dispatcher.getControllerName() == 'reports' and dispatcher.getActionName() == 'payment-report-by-sector') ? 'active' : '' }}">
                                            <a href="/reports/payment-report-by-sector">
                                                <i class="fa fa-calendar"></i>
                                                <span class="nav-label">{{ t('Isplata po sektorima') }}</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            {% endif %}
            {% if aclHelper.isAllowed('default/dealerships', '*') %}
                <li class="{{ dispatcher.getControllerName() == 'dealerships' ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-home"></i>
                        <span class="nav-label">Shops</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse" style="">
                        <li class="{{ dispatcher.getControllerName() == 'dealerships' and dispatcher.getActionName() == 'listByUser' ? 'active' : '' }}">
                            <a href="/dealerships/list-by-user">
                                <i class="fa fa-money"></i>
                                <span class="nav-label">{{ t('Pregled naloga') }}</span>
                            </a>
                        </li>
                        {% if not currentUser.isShop() %}
                            <li class="{{ dispatcher.getControllerName() == 'dealerships' and dispatcher.getActionName() == 'listAll' ? 'active' : '' }}">
                                <a href="/dealerships/list-all">
                                    <i class="fa fa-users"></i>
                                    <span class="nav-label">{{ t('Knjizenje naloga') }}</span>
                                </a>
                            </li>
                            <li class="{{ dispatcher.getControllerName() == 'dealerships' and dispatcher.getActionName() == 'pivotReportData' ? 'active' : '' }}">
                                <a href="/dealerships/pivot-report-data">
                                    <i class="fa fa-table"></i>
                                    <span class="nav-label">{{ t('Pregled svih naloga') }}</span>
                                </a>
                            </li>
                            <li class="{{ dispatcher.getControllerName() == 'dealerships' and dispatcher.getActionName() == 'list' ? 'active' : '' }}">
                                <a href="/dealerships/list">
                                    <i class="fa fa-users"></i>
                                    <span class="nav-label">{{ t('Podesavanje fransize') }}</span>
                                </a>
                            </li>
                        {% endif %}
                    </ul>
                </li>
            {% endif %}
            {% if not currentUser.isClient() %}
                <li class="{{ dispatcher.getControllerName() == 'warehouse-orders' or  dispatcher.getControllerName() == 'production-report' ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-warehouse"></i>
                        <span class="nav-label">{{ t('Magacin') }}</span>
                        <span class="fa arrow"></span>
                    </a>

                    <ul class="nav nav-second-level collapse" style="">
                        {% if not currentUser.isShop() %}
                        <li class="{{ dispatcher.getControllerName() == 'production-report' ? 'active' : '' }}">
                            <a href="/production-report/report">
                                <i class="fa fa-list"></i>
                                <span class="nav-label">{{ t('Izvestaj Proizvodnje') }}</span>
                            </a>
                        </li>
                        <li>
                            <ul class="nav nav-second-level collapse in" style="">
                                <li class="{{ dispatcher.getControllerName() == 'warehouse-orders' ? 'active' : '' }}">
                                    <a href="#">
                                        <i class="fa fa-users"></i>
                                        <span class="nav-label">{{ t('Nalozi') }}</span>
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul class="nav nav-third-level collapse" style="">
                                        {% for channelSupply in channelSuppliesMenu %}
                                            <li class="{{ dispatcher.getControllerName() == 'warehouse-orders' and paramId == channelSupply.id ? 'active' : '' }}">
                                                <a href="/warehouse-orders/list/{{ channelSupply.id }}">
                                                    <i class=""></i>
                                                    <span class="nav-label">{{ channelSupply }}</span>
                                                </a>
                                            </li>
                                        {% endfor %}
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="{{ dispatcher.getControllerName() == 'warehouse-order-groups' ? 'active' : '' }}">
                            <a href="/warehouse-order-groups/list">
                                <i class="fa fa-barcode"></i>
                                <span class="nav-label">{{ t('Groupni nalozi') }}</span>
                            </a>
                        </li>
                        {% endif %}
                        <li>
                            <a href="#" id="scan-warehouse-order-barcode" onclick="getForm('/warehouse-orders/find-by-barcode', '/warehouse-orders/find-by-barcode', event, '')">
                                <i class="fa fa-barcode"></i>
                                <span class="nav-label">{{ t('Prijem naloga') }}</span>
                            </a>
                        </li>
                        {% if not currentUser.isShop() %}
                        <li {{ dispatcher.getControllerName() == 'channel-supplies' ? 'active' : '' }}">
                            <a href="/channel-supplies/list">
                                <i class="fa fa-cogs"></i>
                                <span class="nav-label">{{ t('Podesavanja') }}</span>
                            </a>
                        </li>
                        {% endif %}
                    </ul>
                </li>
            {% endif %}
            {% if aclHelper.isAllowed('default/users', '*') %}
            <li class="{{ request.getURI() == '/users/list' ? 'active' : '' }}">
            <a href="/users/list">
                <i class="fa fa-users"></i>
                <span class="nav-label">{{ t('User Management') }}</span>
            </a>
            </li>
            {% endif %}
        </ul>
    </div>
</nav>
