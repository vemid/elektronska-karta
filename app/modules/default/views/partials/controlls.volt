{% if formUrl is defined and object is defined %}
    <div class="pull-right">
        <button type="button" class="btn btn-default btn-xs text-uppercase"
                {% if object.isEditable() %}
                    onclick="getUpdateForm('{{ formUrl }}', {{ object.id }}, event)"
                {% else %}
                    disabled="disabled"
                {% endif %}
                title="{{ t('Edit') }}">
            {{ t('Edit') }}
        </button>
        <button type="button" class="btn btn-danger btn-xs text-uppercase"
                {% if object.isDeletable() %}
                    onclick="getDeleteForm('{{ formUrl }}', {{ object.id }}, event)"
                {% else %}
                    disabled="disabled"
                {% endif %}
                title="{{ t('Delete') }}">
            <i class="fa fa-close"></i>
        </button>
    </div>
{% endif %}