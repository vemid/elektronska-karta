{% if searchForm is defined and searchForm %}
    <div class="ibox">
        <div class="ibox-title">
            <h5>{{ t('Search') }}</h5>

            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <form method="get" class="form-horizontal">
                <div class="row">
                    <div class="col-sm-12">
                        {% for element in searchForm %}
                            <div class="form-group">
                                <label for="{{ element.getName() }}" class="col-sm-5 control-label">
                                    {{ element.getLabel() }}:
                                </label>

                                <div class="col-sm-7">
                                    {{ element }}
                                </div>
                            </div>
                        {% endfor %}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        {{ submit_button(t('Search'), 'class':'btn btn-sm btn-success pull-right m-xs') }}
                        {{ link_to(resetLink, t('Reset'), 'class':'btn btn-sm btn-danger pull-right m-xs') }}
                    </div>
                </div>
            </form>
        </div>
    </div>
{% endif %}