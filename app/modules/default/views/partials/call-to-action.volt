<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="call-to-action call-to-action-{{ callToActionImage is defined ? callToActionImage : '' }}">
            <div class="wrapper">

                <div class="row">
                    <div class="col-xs-12">
                        <div class="account-call">
                            <div class="space-30 clear"></div>
                            <h1 class="call-to-action-header">
                                {{ callToActionHeading is defined ? callToActionHeading : getTitle() }}
                            </h1>

                            {% if callToActionDescription is defined %}
                                <h4 class="action">{{ callToActionDescription }}</h4>
                            {% endif %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>