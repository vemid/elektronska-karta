<?php

use \Vemid\Form\Form;
use \Vemid\Entity\Repository\ClassificationRepository;
use \Phalcon\Forms\Element\Select;


/**
 * Class DateForm
 */
class DateForm extends Form
{
    public function initialize()
    {

        $entityManager = $this->getDI()->getEntityManager();

        $select = new Select('date', \Phalcon\Forms\Element\Date::class);
        $select->setLabel('Kolekcija');
        $select->setName($select->getName() . '[]');
        $select->setDefault([]);

        $this->add($select);

    }

}