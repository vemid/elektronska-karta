<?php

class PriorityEditForm extends \Vemid\Form\Form
{
    public function initialize()
    {
        $entityManager = $this->getDI()->getEntityManager();

        /** @var Priority $priority */
        $priority = $this->getEntity();

        foreach ($priority->getForm()->getElements() as $element) {
            $this->add($element);
        }

        /** @var CodeType $codeType */
        $codeType = $entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        $oldCollectionCode = null;
        if ($codeType) {
            /** @var Code $oldCollectionCode */
            $oldCollectionCode = $entityManager->findOne(Code::class, [
                'code = :code: AND codeTypeId = :codeTypeId:',
                'bind' => [
                    'code' => '07',
                    'codeTypeId' => $codeType->getId()
                ]
            ]);
        }

        $priorityAble = [];
        $data = [];
        if ($oldCollectionCode) {
            /** @var Classification[] $oldCollectionClassifications */
            $oldCollectionClassifications = $entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeTypeId' => $oldCollectionCode->getId()
                ]
            ]);

            /** @var Classification $classification */
            foreach ($oldCollectionClassifications as $classification) {
                $priorityClassifications = $classification->getPriorityClassifications([
                    PriorityClassification::PROPERTY_PRIORITY_ID . ' != :priorityId:',
                    'bind' => [
                        'priorityId' => $priority->getId()
                    ]
                ]);

//                if ($priorityClassifications->count()) {
//                    continue;
//                }

                $data[$classification->getCode()] = $classification->getName();
            }
        }

        foreach ($priority->getPriorityClassifications() as $priorityClassification) {
            $priorityAble[] = $priorityClassification->getClassification()->getCode();
        }

        $select = new \Phalcon\Forms\Element\Select('oldCollection', $data);
        $select->setLabel('Klasifikacije');
        $select->setAttribute('class', 'search');
        $select->setAttribute('multiple', 'true');
        $select->setName($select->getName() . '[]');
        $select->setDefault($priorityAble);

        $this->add($select, Order::PROPERTY_NAME);
    }
}