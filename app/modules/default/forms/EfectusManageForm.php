<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Select;
use \Vemid\Entity\Type;

/**
 * Class ChannelSupplyEntityEditForm
 */
class EfectusManageForm extends Form
{
    public function initialize()
    {
        /** @var \Vemid\Date\DateTime $date */
        $date = $this->getUserOptions()['date'];
        $entityManager = $this->getDI()->getEntityManager();

        /** @var Efectus[] $efectus */
        $efectus = $entityManager->find(Efectus::class, [
            Efectus::PROPERTY_DATE . ' = :date:',
            'bind' => [
                'date' => $date->format('Y-m-d')
            ]
        ]);

        $shopIn = [];
        $shopOut = [];
        foreach ($efectus as $record) {
            $shopIn[$record->getToCodeId()] = (string)$record->getCodeTo();
            $shopOut[$record->getFromCodeId()] = (string)$record->getCodeFrom();
        }

        $query = $this->modelsManager->createBuilder()
            ->addFrom(Product::class, 'p')
            ->innerJoin(ProductSize::class, 'p.id = ps.productId', 'ps')
            ->innerJoin(Efectus::class, 'ps.id = e.productSizeId', 'e')
            ->where('e.date = :date:', [
                'date' => $date->format('Y-m-d')
            ]);

        /** @var Product[] $products */
        $products = $query->getQuery()->execute();
        $productOptions = [];
        $priorityOptions = [];
        foreach ($products as $product) {
            $productOptions[$product->getId()] = (string)$product;

            if ($priority = $product->getPriority()) {
                $priorityOptions[$priority->getId()] = $priority->getName();
            }
        }

        $seasonOptions = [];

        /** @var CodeType $codeTypeType */
        $codeTypeType = $entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        /** @var CodeType $codeTypeCategory */
        $codeTypeCategory = $entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        $codeProductCategory = $entityManager->findOne(Code::class, [
            'codeTypeId = :codeTypeId: AND code = :code:',
            'bind' => [
                'codeTypeId' => $codeTypeCategory->getId(),
                'code' => '3'
            ]
        ]);

        if ($codeTypeType) {
            /** @var \Code $seasonCode */
            $seasonCode = $entityManager->findOne(\Code::class, [
                Code::PROPERTY_CODE_TYPE_ID .'= :classificationTypeId: AND ' .
                Code::PROPERTY_CODE . '= :code: ',
                'bind' => [
                    'code' => '05',
                    'classificationTypeId' => $codeTypeType->getId()
                ]
            ]);

            if ($seasonCode) {
                $query = $this->modelsManager->createBuilder()
                    ->addFrom(Classification::class, 'c')
                    ->innerJoin(ProductClassification::class, 'c.id = pc.entityId', 'pc')
                    ->innerJoin(Product::class, 'pc.productId = p.id', 'p')
                    ->innerJoin(ProductSize::class, 'p.id = ps.productId', 'ps')
                    ->innerJoin(Efectus::class, 'ps.id = e.productSizeId', 'e')
                    ->where('pc.entityTypeId = :entityTypeId:', [
                        'entityTypeId' => Type::CLASSIFICATION
                    ])
                    ->andWhere('c.' . \Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :classificationTypeCodeId:', [
                        'classificationTypeCodeId' => $seasonCode->getId()
                    ])
                    ->andWhere('e.date = :date:', [
                        'date' => $date->format('Y-m-d')
                    ]);

                /** @var Classification[] $collections */
                $collections = $query->getQuery()->execute();

                foreach ($collections as $classification) {
                    $seasonOptions[$classification->getCode()] = $classification->getName();
                }
            }
        }

        /** @var \Code $collectionCode */
        $collectionCode = $this->entityManager->findOne(\Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '12',
                'codeTypeId' => $codeTypeType->getId()
            ]
        ]);

        $query = $this->modelsManager->createBuilder()
            ->addFrom(Classification::class, 'c')
            ->innerJoin(ProductClassification::class, 'c.id = pc.entityId', 'pc')
            ->innerJoin(Product::class, 'pc.productId = p.id', 'p')
            ->innerJoin(ProductSize::class, 'p.id = ps.productId', 'ps')
            ->innerJoin(Efectus::class, 'ps.id = e.productSizeId', 'e')
            ->where('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLASSIFICATION
            ])
            ->andWhere('c.' . \Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :classificationTypeCodeId:', [
                'classificationTypeCodeId' => $collectionCode->getId()
            ])
            ->andWhere('c.' . \Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :classificationCategoryCodeId:', [
                'classificationCategoryCodeId' => $codeProductCategory->getId()
            ])
            ->andWhere('e.date = :date:', [
                'date' => $date->format('Y-m-d')
            ]);

        /** @var Classification[] $collections */
        $collections = $query->getQuery()->execute();
        $collectionOptions = [];
        foreach ($collections as $collection) {
            $collectionOptions[$collection->getId()] = $collection->getName();
        }

        $shopInSelect = new Select('codeId', $shopIn);
        $shopInSelect->setLabel('U Radnju');
        $shopInSelect->setAttribute('class', 'search');
        $shopInSelect->setAttribute('multiple', 'true');
        $shopInSelect->setDefault([]);

        $shopOutSelect = new Select('codeOut', $shopOut);
        $shopOutSelect->setLabel('Iz Radnje');
        $shopOutSelect->setAttribute('class', 'search');
        $shopOutSelect->setAttribute('multiple', 'true');
        $shopOutSelect->setDefault([]);


        $productSelect = new Select('productId', $productOptions);
        $productSelect->setLabel('Proizvod');
        $productSelect->setAttribute('class', 'search productId');
        $productSelect->setAttribute('multiple', 'true');
        $productSelect->setDefault([]);

        $seasonSelect = new Select('season', $seasonOptions);
        $seasonSelect->setLabel('Sezona');
        $seasonSelect->setAttribute('class', 'search productId');
        $seasonSelect->setAttribute('multiple', 'true');
        $seasonSelect->setDefault([]);

        $collectionSelect = new Select('collection', $collectionOptions);
        $collectionSelect->setLabel('Kolekcija');
        $collectionSelect->setAttribute('class', 'search productId');
        $collectionSelect->setAttribute('multiple', 'true');
        $collectionSelect->setDefault([]);

        $prioritySelect = new Select('priority', $priorityOptions);
        $prioritySelect->setLabel('Prioritet');
        $prioritySelect->setAttribute('class', 'search productId');
        $prioritySelect->setAttribute('multiple', 'true');
        $prioritySelect->setDefault([]);

        $this->add($shopOutSelect);
        $this->add($shopInSelect);
        $this->add($productSelect);
        $this->add($seasonSelect);
        $this->add($collectionSelect);
        $this->add($prioritySelect);
    }
}
