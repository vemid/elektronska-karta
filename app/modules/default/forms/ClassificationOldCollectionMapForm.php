<?php

use \Vemid\Form\Form;

class ClassificationOldCollectionMapForm extends Form
{
    public function initialize()
    {
        $entityManager = $this->getDI()->getEntityManager();

        $form = $this->getEntity()->getForm();

        foreach ($form->getElements() as $element) {
            $this->add($element);
        }

        /** @var CodeType $codeType */
        $codeType = $entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        if ($codeType) {
            /** @var Code $oldCollection */
            $oldCollection = $entityManager->findOne(Code::class, [
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
                Code::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'codeTypeId' => $codeType->getId(),
                    'code' => '07'
                ]
            ]);

            $oldCollectionOptions = [];
            foreach ($oldCollection->getClassifications(['OrderItem' => 'name', 'distinct' => 'name']) as $classification) {
                if (in_array($classification->getName(), $oldCollectionOptions, true)) {
                    continue;
                }

                $oldCollectionOptions[$classification->getId()] = $classification->getName();
            }

            /** @var Code $ageCollection */
            $ageCollection = $entityManager->findOne(Code::class, [
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
                Code::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'codeTypeId' => $codeType->getId(),
                    'code' => '02'
                ]
            ]);

            $ageCollectionOptions = [];
            foreach ($ageCollection->getClassifications(['distinct' => 'name', 'OrderItem' => 'name']) as $classification) {
                if (in_array($classification->getName(), $ageCollectionOptions, true)) {
                    continue;
                }

                $ageCollectionOptions[$classification->getId()] = $classification->getName();
            }

            /** @var Code $seasonCollection */
            $seasonCollection = $entityManager->findOne(Code::class, [
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
                Code::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'codeTypeId' => $codeType->getId(),
                    'code' => '05'
                ]
            ]);

            $seasonCollectionOptions = [];
            foreach ($seasonCollection->getClassifications(['distinct' => 'name', 'OrderItem' => 'name']) as $classification) {
                if (in_array($classification->getName(), $seasonCollectionOptions, true)) {
                    continue;
                }

                $seasonCollectionOptions[$classification->getId()] = $classification->getClassificationCategory()->getName().'-'.$classification->getName();
            }

            /** @var Code $collection */
            $collection = $entityManager->findOne(Code::class, [
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
                Code::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'codeTypeId' => $codeType->getId(),
                    'code' => '12'
                ]
            ]);

            $collectionOptions = [];
            foreach ($collection->getClassifications(['distinct' => 'name', 'OrderItem' => 'name']) as $classification) {
                if (in_array($classification->getName(), $collectionOptions, true)) {
                    continue;
                }

                $collectionOptions[$classification->getId()] = $classification->getClassificationCategory()->getName().'-'.$classification->getName();
            }

            /** @var Code $doubleSeasonCode */
            $doubleSeasonCode = $entityManager->findOne(Code::class, [
                'code = :code: AND codeTypeId = :codeTypeId:',
                'bind' => [
                    'code' => '13',
                    'codeTypeId' => $codeType->getId()
                ]
            ]);

            $doubleSeasonOptions = [];
            foreach ($doubleSeasonCode->getClassifications(['distinct' => 'name', 'OrderItem' => 'name']) as $classification) {
                if (in_array($classification->getName(), $doubleSeasonOptions, true)) {
                    continue;
                }

                $doubleSeasonOptions[$classification->getId()] = $classification->getClassificationCategory()->getName().'-'.$classification->getName();
            }

            $this->get(ClassificationOldCollectionMap::PROPERTY_CLASSIFICATION_OLD_COLLECTION_ID)->setOptions($oldCollectionOptions);
            $this->get(ClassificationOldCollectionMap::PROPERTY_CLASSIFICATION_SEASON_ID)->setOptions($seasonCollectionOptions);
            $this->get(ClassificationOldCollectionMap::PROPERTY_CLASSIFICATION_COLLECTION_ID)->setOptions($collectionOptions);
            $this->get(ClassificationOldCollectionMap::PROPERTY_CLASSIFICATION_AGE_ID)->setOptions($ageCollectionOptions);
            $this->get(ClassificationOldCollectionMap::PROPERTY_CLASSIFICATION_DOUBLE_SEASON_ID)->setOptions($doubleSeasonOptions);
        }

        /** @var CodeType $attributeCodeType */
        $attributeCodeType = $entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::ATTRIBUTES
            ]
        ]);

        if ($attributeCodeType) {

            /** @var Code $manufacturerCode */
            $manufacturerCode = $entityManager->findOne(Code::class, [
                Code::PROPERTY_CODE . ' = :code: AND ' .
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                'bind' => [
                    'code' => '13',
                    'codeTypeId' => $attributeCodeType->getId()
                ]
            ]);

            if ($manufacturerCode) {
                /** @var Attribute[] $manufacturers */
                $manufacturers = $entityManager->find(Attribute::class, [
                    Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                    'bind' => [
                        'attributeCodeId' => $manufacturerCode->getId()
                    ]
                ]);

                $options = [];
                foreach ($manufacturers as $manufacturerEntity) {
                    $options[$manufacturerEntity->getId()] = $manufacturerEntity->getName();
                }

                $this->get(ClassificationOldCollectionMap::PROPERTY_ATTRIBUTE_PRODUCER_ID)->setOptions($options);
            }

            /** @var Code $modelProductType */
            $modelProductTypeCode = $entityManager->findOne(Code::class, [
                Code::PROPERTY_CODE . ' = :code: AND ' .
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                'bind' => [
                    'code' => '24',
                    'codeTypeId' => $attributeCodeType->getId()
                ]
            ]);

            if ($modelProductTypeCode) {
                /** @var Attribute[] $modelProductTypes */
                $modelProductTypes = $entityManager->find(Attribute::class, [
                    Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                    'bind' => [
                        'attributeCodeId' => $modelProductTypeCode->getId()
                    ]
                ]);

                $options = [];
                foreach ($modelProductTypes as $modelProductTypeEntity) {
                    $options[$modelProductTypeEntity->getId()] = $modelProductTypeEntity->getName();
                }

                $this->get(ClassificationOldCollectionMap::PROPERTY_ATTRIBUTE_PRODUCT_TYPE_ID)->setOptions($options);
            }
        }

        /** @var CodeType $codeProductType */
        $codeProductType = $entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCT_TYPE
            ]
        ]);

        if ($codeProductType) {
            /** @var Code[] $productTypeCodes */
            $productTypeCodes = $entityManager->find(Code::class, [
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeTypeId' => $codeProductType->getId()
                ]
            ]);

            $codeOptions = [];
            foreach ($productTypeCodes as $productTypeCode) {
                if (in_array($productTypeCode->getName(), $codeOptions, true)) {
                    continue;
                }

                $codeOptions[$productTypeCode->getId()] = $productTypeCode->getName();
            }

            $this->get(ClassificationOldCollectionMap::PROPERTY_CODE_ID)->setOptions($codeOptions);
        }
    }
}