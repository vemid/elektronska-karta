<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Select;
use \Phalcon\Forms\Element\Text;
use \Vemid\Validation\Validator\PresenceOf;

/**
 * Class ProductForm
 */
class ProductForm extends Form
{
    public function initialize()
    {
        /** @var CodeType $codeType */
        $codeType = $this->getDI()->getEntityManager()->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        $entityManager = $this->getDI()->getEntityManager();
        
        $translate = $this->getDI()->getTranslator();

        $productTypeData = [0 => '-- Izaberite --'];
        if ($codeType) {
            foreach ($codeType->getCodes() as $code) {
                $productTypeData[$code->getCode()] = $code->getName();
            }
        }

        $productType = new Select('productTypeCodeId', $productTypeData);
        $productType->setAttribute('class', 'form-control required search');
        $productType->setAttribute('data-position', 2);
        $productType->setAttribute('data-generator', 'false');
        $productType->setLabel($translate->t('Vrsta Proizvoda'));
        $productType->addValidator(new PresenceOf([
            'message' => $translate->t('Obavezno polje'),
        ]));

        $seasonField = new Select('season', []);
        $seasonField->setAttribute('class', 'form-control required search');
        $seasonField->setAttribute('data-position', 1);
        $seasonField->setAttribute('data-generator', 'true');
        $seasonField->setLabel($translate->t('Sezona'));

        $productProgram = new Select('productProgram', []);
        $productProgram->setAttribute('class', 'form-control required search');
        $productProgram->setAttribute('data-position', 3);
        $productProgram->setAttribute('data-generator', 'false');
        $productProgram->setLabel($translate->t('Program Robe'));

        $genderClassification = new Select('genderClassification', []);
        $genderClassification->setAttribute('class', 'form-control required search');
        $genderClassification->setAttribute('data-position', 4);
        $genderClassification->setAttribute('data-generator', 'false');
        $genderClassification->setLabel($translate->t('Klasifikacija po polu'));

        $productGroup = new Select('productGroup', []);
        $productGroup->setAttribute('class', 'form-control required search');
        $productGroup->setAttribute('data-position', 5);
        $productGroup->setAttribute('data-generator', 'false');
        $productGroup->setLabel($translate->t('Robna grupa'));

        $subProductGroup = new Select('subProductGroup', []);
        $subProductGroup->setAttribute('class', 'form-control required search');
        $subProductGroup->setAttribute('data-position', 6);
        $subProductGroup->setAttribute('data-generator', 'false');
        $subProductGroup->setLabel($translate->t('Robna Pod Grupa'));

        $subSubProductGroup = new Select('subSubProductGroup', []);
        $subSubProductGroup->setAttribute('class', 'form-control required search');
        $subSubProductGroup->setAttribute('data-position', 7);
        $subSubProductGroup->setAttribute('data-generator', 'false');
        $subSubProductGroup->setLabel($translate->t('Robna Pod Pod Grupa'));
        $subSubProductGroup->addValidator(new PresenceOf([
            'message' => $translate->t('Obavezno polje'),
        ]));

        $color = new Select('color', []);
        $color->setAttribute('class', 'form-control required search');
        $color->setAttribute('data-position', 8);
        $color->setAttribute('data-generator', 'true');
        $color->setLabel($translate->t('Boja'));

        $priority = new Select('priorityId', []);
        $priority->setAttribute('class', 'form-control search');
        $priority->setLabel($translate->t('Prioritet'));

        $name = new Text('name');
        $name->setAttribute('class', 'form-control required');
        $name->setLabel($translate->t('Naziv Artikla'));

        $produceTypeElement = new Select('produceType', [
            0 => '-- Izaberite --', 'PLETENINA' => 'Pletenina', 'STATIKA' => 'Statika'
        ]);

        $produceTypeElement->setLabel($translate->t('Produce Type'));
        $produceTypeElement->setAttribute('class', 'form-control search');

        $brand = new Select('brand', []);
        $brand->setAttribute('class', 'form-control search');
        $brand->setLabel($translate->t('Brend'));

        $age = new Select('age', []);
        $age->setAttribute('class', 'form-control search');
        $age->setLabel($translate->t('Uzrast'));

        /** @var CodeType $codeTypeProductType */
        $codeTypeProductType = $entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCT_TYPE
            ]
        ]);

        $productTypeCodeOptions = [];
        if ($codeTypeProductType) {
            /** @var Code[] $codes */
            $codes = $entityManager->find(Code::class, [
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeTypeId' => $codeTypeProductType->getId()
                ]
            ]);

            foreach ($codes as $codeC) {
                $productTypeCodeOptions[$codeC->getCode()] = $codeC->getName();
            }
        }

        $productTypeCode = new Select('productTypeCode', $productTypeCodeOptions);
        $productTypeCode->setAttribute('class', 'form-control search');
        $productTypeCode->setLabel($translate->t('Product Type'));

        $collection = new Select('collection', []);
        $collection->setAttribute('class', 'form-control required search');
        $collection->setLabel($translate->t('Kolekcija'));

        $oldCollection = new Select('oldCollection', []);
        $oldCollection->setAttribute('class', 'form-control required search');
        $oldCollection->setLabel($translate->t('Stara kolekcija'));

        $doubleSeason = new Select('doubleSeason', []);
        $doubleSeason->setAttribute('class', 'form-control required search');
        $doubleSeason->setLabel($translate->t('Dupla Sezona'));

        $manufacturer = new Select('manufacturer');
        $manufacturer->setAttribute('class', 'form-control required search');
        $manufacturer->setLabel($translate->t('Proizvodjač'));

        /** @var CodeType $attributeCodeType */
        $attributeCodeType = $entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::ATTRIBUTES
            ]
        ]);

        /** @var Code $manufacturerCode */
        $manufacturerCode = $entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '13',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($manufacturerCode) {
            /** @var Attribute[] $manufacturers */
            $manufacturers = $entityManager->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $manufacturerCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($manufacturers as $manufacturerEntity) {
                $options[$manufacturerEntity->getId()] = $manufacturerEntity->getName();
            }

            $manufacturer->setOptions($options);
        }

        $seasonCollection = new Select('seasonCollection');
        $seasonCollection->setAttribute('class', 'form-control required search');
        $seasonCollection->setLabel($translate->t('Karakteristika sezone'));

        /** @var Code $seasonCollectionCode */
        $seasonCollectionCode = $entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '03',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($seasonCollectionCode) {
            /** @var Attribute[] $seasonCollections */
            $seasonCollections = $entityManager->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $seasonCollectionCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($seasonCollections as $seasonCollectionOption) {
                $options[$seasonCollectionOption->getId()] = $seasonCollectionOption->getName();
            }

            $seasonCollection->setOptions($options);
        }

        $modelType = new Select('modelType');
        $modelType->setAttribute('class', 'form-control required search');
        $modelType->setLabel($translate->t('Tip Modela'));

        /** @var Code $modelTypeCode */
        $modelTypeCode = $entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '04',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($modelTypeCode) {
            /** @var Attribute[] $modelTypes */
            $modelTypes = $entityManager->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $modelTypeCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($modelTypes as $modelTypeEntity) {
                $options[$modelTypeEntity->getId()] = $modelTypeEntity->getName();
            }

            $modelType->setOptions($options);
        }


        $designType = new Select('dezenType');
        $designType->setAttribute('class', 'form-control required search');
        $designType->setLabel($translate->t('Tip Dezena'));

        /** @var Code $designTypeCode */
        $designTypeCode = $entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '05',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($designTypeCode) {
            /** @var Attribute[] $dezenTypes */
            $designTypes = $entityManager->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $designTypeCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($designTypes as $designTypeEntity) {
                $options[$designTypeEntity->getId()] = $designTypeEntity->getName();
            }

            $designType->setOptions($options);
        }

        $printType = new Select('printType');
        $printType->setAttribute('class', 'form-control required search');
        $printType->setLabel($translate->t('Tip Stampe'));

        /** @var Code $printTypeCode */
        $printTypeCode = $entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '08',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($printTypeCode) {
            /** @var Attribute[] $printTypes */
            $printTypes = $entityManager->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $printTypeCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($printTypes as $printTypeEntity) {
                $options[$printTypeEntity->getId()] = $printTypeEntity->getName();
            }

            $printType->setOptions($options);
        }

        $applicationAttribute = new Select('applicationAttribute');
        $applicationAttribute->setAttribute('class', 'form-control required search');
        $applicationAttribute->setLabel($translate->t('Aplikacija'));

        /** @var Code $applicationAttributeCode */
        $applicationAttributeCode = $entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '09',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($applicationAttributeCode) {
            /** @var Attribute[] $applicationAttributes */
            $applicationAttributes = $entityManager->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $applicationAttributeCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($applicationAttributes as $applicationAttributeEntity) {
                $options[$applicationAttributeEntity->getId()] = $applicationAttributeEntity->getName();
            }

            $applicationAttribute->setOptions($options);
        }

        $materialType = new Select('materialType');
        $materialType->setAttribute('class', 'form-control required search');
        $materialType->setLabel($translate->t('Vrsta Materijala'));

        /** @var Code $materialTypeCode */
        $materialTypeCode = $entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '20',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($materialTypeCode) {
            /** @var Attribute[] $materialTypes */
            $materialTypes = $entityManager->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $materialTypeCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($materialTypes as $materialTypeEntity) {
                $options[$materialTypeEntity->getId()] = $materialTypeEntity->getName();
            }

            $materialType->setOptions($options);
        }

        $modelProductType = new Select('modelProductType');
        $modelProductType->setAttribute('class', 'form-control search');
        $modelProductType->setLabel($translate->t('Vrsta Robe'));

        /** @var Code $modelProductType */
        $modelProductTypeCode = $entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '24',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($modelProductTypeCode) {
            /** @var Attribute[] $modelProductTypes */
            $modelProductTypes = $entityManager->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $modelProductTypeCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($modelProductTypes as $modelProductTypeEntity) {
                $options[$modelProductTypeEntity->getId()] = $modelProductTypeEntity->getName();
            }

            $modelProductType->setOptions($options);
        }

        $model = new Select('model');
        $model->setAttribute('class', 'form-control search');
        $model->setLabel($translate->t('Model'));

        /** @var Code $modelCode */
        $modelCode = $entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '25',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($modelCode) {
            /** @var Attribute[] $modelProductTypes */
            $models = $entityManager->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $modelCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($models as $modelsEntity) {
                $options[$modelsEntity->getId()] = $modelsEntity->getName();
            }

            $model->setOptions($options);
        }

        $modelInsane = new Select('modelInsane');
        $modelInsane->setAttribute('class', 'form-control search');
        $modelInsane->setLabel($translate->t('Insane'));

        /** @var Code $modelInsaneCode */
        $modelInsaneCode = $entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '27',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($modelInsaneCode) {
            /** @var Attribute[] $modelInsanes */
            $modelInsanes = $entityManager->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $modelInsaneCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($modelInsanes as $modelInsaneEntity) {
                $options[$modelInsaneEntity->getId()] = $modelInsaneEntity->getName();
            }

            $modelInsane->setOptions($options);
        }

        $supplierCode = new Text('supplierCode');
        $supplierCode->setAttribute('class', 'form-control');
        $supplierCode->setLabel($translate->t('Sifra Dobavlajca Artikla'));

        $composition = new \Phalcon\Forms\Element\TextArea('composition');
        $composition->setAttribute('class', 'form-control');
        $composition->setLabel($translate->t('Sirovinski sastav'));

        $parentProduct = new Select('parentId', []);
        $parentProduct->setAttribute('class', 'search form-control');
        $parentProduct->setLabel($translate->t('Vezani proizvod'));


        $this->add($seasonField);
        $this->add($productType);
        $this->add($productProgram);
        $this->add($genderClassification);
        $this->add($productGroup);
        $this->add($subProductGroup);
        $this->add($subSubProductGroup);
        $this->add($color);
        $this->add($name);
        $this->add($produceTypeElement);
        $this->add($brand);
        $this->add($productTypeCode);
        $this->add($age);
        $this->add($collection);
        $this->add($oldCollection);
        $this->add($doubleSeason);
        $this->add($manufacturer);
        $this->add($seasonCollection);
        $this->add($modelType);
        $this->add($designType);
        $this->add($printType);
        $this->add($applicationAttribute);
        $this->add($materialType);
        $this->add($modelProductType);
        $this->add($model);
        $this->add($modelInsane);
        $this->add($supplierCode);
        $this->add($priority);
        $this->add($parentProduct);
        $this->add($composition);
    }
}
