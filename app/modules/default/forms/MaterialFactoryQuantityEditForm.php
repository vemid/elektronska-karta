<?php

use \Phalcon\Forms\Element\Check;

/**
 * Class MaterialFactoryQuantityEditForm
 */
class MaterialFactoryQuantityEditForm extends \Vemid\Form\Form
{
    public function initialize()
    {
        $form = $this->getEntity()->getForm();
        /** @var MaterialFactoryQuantity $materialFactoryQuantity */
        $materialFactoryQuantity = $this->getUserOption('materialFactoryQuantity');

        foreach ($form->getElements() as $element) {
            $this->add($element);
        }

        /** @var Code $codeTypeCategory */
        $codeTypeCategory = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        /** @var CodeType $codeTypeType */
        $codeTypeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        /** @var Code $codetype */
        $codetype = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => 13,
                'codeTypeId' => $codeTypeType->getId(),
            ]
        ]);

        /** @var Code $codeCategory */
        $codeCategory = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => 1,
                'codeTypeId' => $codeTypeCategory->getId(),
            ]
        ]);

        $options = [];
        if ($codetype && $codeCategory) {
            /** @var Classification[] $classifications */
            $classifications = $this->entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId: AND ' .
                Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategory:',
                'bind' => [
                    'codeTypeId' => $codetype->getId(),
                    'codeCategory' => $codeCategory->getId(),
                ],
                'order' => 'name ASC'
            ]);

            foreach ($classifications as $classification) {
                $options[$classification->getId()] = $classification->getName();
            }
        }

        $inField = new Check('typeIn');
        $inField->setLabel('Ulaz');
        $inField->setAttribute('class', 'factory-status');

        $outField = new Check('typeOut');
        $outField->setLabel('Izlaz');
        $outField->setAttribute('class', 'factory-status');

        $this->get(MaterialFactoryQuantity::PROPERTY_MATERIAL_FACTORY_ID)->setDefault($materialFactoryQuantity->getMaterialFactory());
        $this->get(MaterialFactoryQuantity::PROPERTY_AMOUNT)->setDefault($materialFactoryQuantity->getAmount());
        $this->get(MaterialFactoryQuantity::PROPERTY_SEASON_CLASSIFICATION_ID)->setDefault($materialFactoryQuantity->getSeasonClassificationId());
        $this->get(MaterialFactoryQuantity::PROPERTY_DATETIME)->setDefault($materialFactoryQuantity->getDatetime());
        $this->get(MaterialFactoryQuantity::PROPERTY_WARRANT)->setDefault($materialFactoryQuantity->getWarrant());
        $this->get(MaterialFactoryQuantity::PROPERTY_NOTE)->setDefault($materialFactoryQuantity->getNote());
        if($materialFactoryQuantity->getType() == MaterialFactoryQuantity::INCOME) {
            $inField->setDefault(true);
        }
        else $outField->setDefault(true);
        $this->get(MaterialFactoryQuantity::PROPERTY_SEASON_CLASSIFICATION_ID)->setOptions($options);
        $this->add($inField);
        $this->add($outField);
    }

}