<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Select;
use \Vemid\Entity\Type;

class OrderFilterForm extends Form
{
    public function initialize()
    {
        $translator = $this->getDI()->getTranslator();
        $entityManager = $this->getDI()->getEntityManager();
        /** @var Order $order */
        $order = $this->getEntity();

        $genderCodes = [
            'Ženska' => ['1BŽ' => '1BŽ', '1OŽ' => '1OŽ', '1IŽ' => '1IŽ'],
            'Muška' => ['1BM' => '1BM', '1IM' => '1IM', '1OM' => '1OM'],
            'Unisex' => ['1OU' => '1OU', '1BU' => '1BU', '1IU' => '1IU']
        ];

        $query = $this->modelsManager->createBuilder();
        $query->addFrom(Code::class, 'c')
            ->leftJoin(Product::class, 'p.productCodeTypeId = c.id', 'p')
            ->where('p.isOrderable = :orderable:', [
                'orderable' => true
            ])
            ->groupBy('c.id');

        /** @var Code[] $codes */
        $codes = $query->getQuery()->execute();
        $productTypeOptions = [];
        foreach ($codes as $code) {
            $productTypeOptions[$code->getCode()] = $code->getName();
        }

        /** @var \CodeType $codeType */
        $codeType = $entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        $programOptions = [];
        $classificationCategoryCodeIds = [];
        $programOptionsIds = [];
        if ($codeType && count($productTypeOptions)) {
            /** @var Code[] $productTypeCodes */
            $productTypeCodes = $entityManager->find(Code::class, [
                'code IN ({codes:array}) AND codeTypeId = :codeTypeId:',
                'bind' => [
                    'codes' => array_keys($productTypeOptions),
                    'codeTypeId' => $codeType->getId()
                ]
            ]);

            foreach ($productTypeCodes as $productTypeCode) {
                $classificationCategoryCodeIds[] = $productTypeCode->getId();
            }

            $query = $this->modelsManager->createBuilder();
            $query->addFrom(Classification::class, 'c')
                ->leftJoin(Code::class, 'co.id = c.classificationTypeCodeId', 'co')
                ->leftJoin(ProductClassification::class, 'c.id = pc.entityId', 'pc')
                ->leftJoin(Product::class, 'p.id = pc.productId', 'p')
                ->where('p.isOrderable = :orderable:', [
                    'orderable' => true
                ])
                ->andWhere('pc.entityTypeId = :entityTypeId:', [
                    'entityTypeId' => Type::CLASSIFICATION
                ])
                ->andWhere('c.classificationCategoryCodeId IN ({classificationCategoryCodeIds:array})', [
                    'classificationCategoryCodeIds' => $classificationCategoryCodeIds
                ])
                ->andWhere('co.code = :code:', [
                    'code' => '00'
                ])
                ->andWhere('c.parentClassificationId IS NULL')
                ->groupBy('c.id');

            /** @var Classification[] $classifications */
            $classifications = $query->getQuery()->execute();

            foreach ($classifications as $classification) {
                $programOptions[$classification->getCode()] = $classification->getName();
                $programOptionsIds[] = $classification->getId();
            }
        }

        $genderOptions = [0 => '-- Izaberite --'];
        $genderParentOptions = [];
        if (count($programOptionsIds)) {
            $genderClassifications = $this->getParentClassifications($programOptionsIds);

            foreach ($genderClassifications as $classification) {

                $genderParentOptions[] = $classification->getId();
                $genderCode = '';
                foreach ($genderCodes as $key => $codes) {
                    if (in_array($classification->getCode(), $codes, true)) {
                        $genderCode = $key;
                        break;
                    }
                }

                $genderOptions[$genderCode] = $genderCode;
            }
        }

        $collectionOptions = [0 => '-- Izaberite --'];
        $productSizeOptions = ['' => '-- Izaberite --'];
        $groupModeOptions = [0 => '-- Izaberite --'];

        $query = $this->modelsManager->createBuilder();
        $query->addFrom(Classification::class, 'c')
            ->leftJoin(Code::class, 'co.id = c.classificationTypeCodeId', 'co')
            ->leftJoin(CodeType::class, 'ct.id = co.codeTypeId', 'ct')
            ->leftJoin(ProductClassification::class, 'c.id = pc.entityId', 'pc')
            ->leftJoin(Product::class, 'p.id = pc.productId', 'p')
            ->where('p.isOrderable = :orderable:', [
                'orderable' => true
            ])
            ->andWhere('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLASSIFICATION
            ])
            ->andWhere('co.code = :coOde:', [
                'coOde' => '07'
            ])
            ->andWhere('ct.code = :code:', [
                'code' => CodeType::CLASSIFICATION_TYPE
            ])
            ->groupBy('c.id');

        /** @var Classification[] $collectionClassifications */
        $collectionClassifications = $query->getQuery()->execute();

        foreach ($collectionClassifications as $classification) {
            if (strpos($classification->getName(), '-') !== false) {
                $name = explode('-', $classification->getName());
                $name = $name[1];
            } else {
                $name = explode(' ', $classification->getName());
                array_shift($name);
                $name = implode(' ', $name);
            }

            $collectionOptions[$classification->getCode()] = $name;
        }

        if (count($genderParentOptions)) {
            $groupClassifications = $this->getParentClassifications($genderParentOptions);
            foreach ($groupClassifications as $classification) {
                $nameChunk = explode(' ', $classification->getName());
                $groupModeOptions[$classification->getCode()] = array_pop($nameChunk);
            }
        }

        if (count($classificationCategoryCodeIds)) {

            $query = $this->modelsManager->createBuilder();
            $query->addFrom(Classification::class, 'c')
                ->leftJoin(Code::class, 'co.id = c.classificationTypeCodeId', 'co')
                ->leftJoin(ProductClassification::class, 'c.id = pc.entityId', 'pc')
                ->leftJoin(Product::class, 'p.id = pc.productId', 'p')
                ->where('p.isOrderable = :orderable:', [
                    'orderable' => true
                ])
                ->andWhere('pc.entityTypeId = :entityTypeId:', [
                    'entityTypeId' => Type::CLASSIFICATION
                ])
                ->andWhere('co.code = :code:', [
                    'code' => '02'
                ])
                ->andWhere('c.classificationCategoryCodeId IN ({productTypes:array})', [
                    'productTypes' => $classificationCategoryCodeIds
                ])
                ->groupBy('c.id');

            /** @var Classification[] $ageClassifications */
            $ageClassifications = $query->getQuery()->execute();

            foreach ($ageClassifications as $classification) {
                $productSizeOptions[$classification->getCode()] = $classification->getName();
            }
        }

        $productTypeOptions = [0 => '-- Izaberite --'] + $productTypeOptions;
        $programOptions = [0 => '-- Izaberite --'] + $programOptions;

        $productType = new Select('productTypeOrder', $productTypeOptions);
        $productType->setAttribute('class', 'form-control search');
        $productType->setLabel($translator->t('Vrsta Proizvoda'));

        if (count($productTypeOptions) === 2) {
            $productType->setAttribute('disabled', true);
            $productType->setDefault(max(array_keys($productTypeOptions)));
        }

        $program = new Select('productProgramOrder', $programOptions);
        $program->setAttribute('class', 'form-control search');
        $program->setLabel($translator->t('Program Robe'));

        if (count($programOptions) === 2) {
            $program->setAttribute('disabled', true);
            $program->setDefault(max(array_keys($programOptions)));
        }

        $gender = new Select('genderClassificationOrder', $genderOptions);
        $gender->setAttribute('class', 'form-control search');
        $gender->setLabel($translator->t('Pol'));

        if (count($genderOptions) === 2 && count($programOptions) === 2) {
            $gender->setAttribute('disabled', true);
            $gender->setDefault(max(array_keys($genderOptions)));
        }

        $collection = new Select('collection', $collectionOptions);
        $collection->setAttribute('class', 'form-control search');
        $collection->setLabel($translator->t('Kolekcija'));

        if (count($collectionOptions) === 2) {
            $collection->setAttribute('disabled', true);
            $collection->setDefault(array_keys($collectionOptions)[1]);
        }

        $sizes = new Select('sizes', $productSizeOptions);
        $sizes->setAttribute('class', 'form-control search');
        $sizes->setLabel($translator->t('Veličina'));

        $group = new Select('groupModel', $groupModeOptions);
        $group->setAttribute('class', 'form-control search');
        $group->setLabel($translator->t('Grupa model'));

        $priorityData = [0 => '-- Izaberite --'];
        foreach ($order->getOrderClassifications() as $orderClassification) {
            if (!$classification = $orderClassification->getClassification()) {
                continue;
            }

            foreach ($classification->getPriorityClassifications() as $priorityClassification) {
                if (!$priority = $priorityClassification->getPriority()) {
                    continue;
                }

                $priorityData[$priority->getPriorityNumber()] = $priority;
            }
        }

        $priority = new Select('priorityId', $priorityData);
        $priority->setAttribute('class', 'form-control search');
        $priority->setLabel($translator->t('Prioritet'));

        $this->add($productType);
        $this->add($program);
        $this->add($gender);
        $this->add($collection);
        $this->add($priority);
        $this->add($sizes);
        $this->add($group);
    }

    /**
     * @param array $parentClassificationIds
     * @return Classification[]
     */
    private function getParentClassifications(array $parentClassificationIds)
    {
        $query = $this->modelsManager->createBuilder();
        $query->addFrom(Classification::class, 'c')
            ->leftJoin(ProductClassification::class, 'c.id = pc.entityId', 'pc')
            ->leftJoin(Product::class, 'p.id = pc.productId', 'p')
            ->where('p.isOrderable = :orderable:', [
                'orderable' => true
            ])
            ->andWhere('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLASSIFICATION
            ])
            ->andWhere('c.parentClassificationId IN ({parentClassifications:array})', [
                'parentClassifications' => $parentClassificationIds
            ])
            ->groupBy('c.id');

        return $query->getQuery()->execute();
    }
}
