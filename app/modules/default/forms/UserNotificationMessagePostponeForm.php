<?php

use \Phalcon\Forms\Element\Select;
use \Vemid\Date\DateTime;

class UserNotificationMessagePostponeForm extends Vemid\Form\Form
{
    public function initialize()
    {
        $translator = $this->getDI()->getTranslator();
        /** @var UserNotificationMessage $userNotificationMessage */
        $userNotificationMessage = $this->getUserOption('userNotificationMessage');
        $options = ['' => $translator->t('-- Izaberite --')];

        $today = new DateTime();
        $dueDate = $userNotificationMessage->getDueDate();
        if (!$dueDate || $today->diff($dueDate)->days > 2) {
            $options[UserNotificationMessage::STATUS_POSTPONED_ONE_DAY] = $translator->t('Odloži za jedan dan');
        }

        if (!$dueDate || $today->diff($dueDate)->days > 3) {
            $options[UserNotificationMessage::STATUS_POSTPONED_TWO_DAYS] = $translator->t('Odloži za dva dana');
        }

        if (!$dueDate || $today->diff($dueDate)->days > 4) {
            $options[UserNotificationMessage::STATUS_POSTPONED_THREE_DAYS] = $translator->t('Odloži za tri dana');
        }

        if (!$dueDate || $today->diff($dueDate)->days > 6) {
            $options[UserNotificationMessage::STATUS_POSTPONED_FIVE_DAYS] = $translator->t('Odloži za pet dana');
        }

        if (!$dueDate || $today->diff($dueDate)->days > 11) {
            $options[UserNotificationMessage::STATUS_POSTPONED_TEN_DAYS] = $translator->t('Odloži za deset dana');
        }

        $status = new Select('status', $options);
        $status->setLabel('Status');
        $status->setAttribute('class', $status->getAttribute('class') . ' search');

        $this->add($status);
    }
}