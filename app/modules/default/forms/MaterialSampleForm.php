<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Select;

/**
 * Class MaterialSampleForm
 */
class MaterialSampleForm extends Form
{
    public function initialize()
    {
        $entityManager = $this->getDI()->getEntityManager();
        $translator = $this->getDI()->getTranslator();
        $form = $this->getEntity()->getForm();
        foreach ($form->getElements() as $element) {
            $this->add($element);
        }

        if ($this->has(MaterialSample::PROPERTY_MATERIAL_TYPE_CODE_ID)) {
            $options = [0 => $translator->t('-- Izaberite --')];

            /** @var CodeType $codeType */
            $codeType = $entityManager->findOne(\CodeType::class, [
                CodeType::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'code' => \CodeType::CLASSIFICATION_CATEGORY
                ]
            ]);

            /** @var CodeType $codeTypeClassification */
            $codeTypeClassification = $entityManager->findOne(CodeType::class, [
                'code = :code:',
                'bind' => [
                    'code' => CodeType::CLASSIFICATION_TYPE
                ]
            ]);

            /** @var Code $doubleSeasonCode */
            $doubleSeasonCode = $entityManager->findOne(Code::class, [
                'code = :code: AND codeTypeId = :codeTypeId:',
                'bind' => [
                    'code' => '13',
                    'codeTypeId' => $codeTypeClassification->getId()
                ]
            ]);

            /** @var \Code $code */
            $code = $entityManager->findOne(\Code::class, [
                Code::PROPERTY_CODE . ' = :code: AND ' .
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                'bind' => [
                    'code' => '1',
                    'codeTypeId' => $codeType->getId()
                ]
            ]);

            $classifications = $code->getClassificationCategories([
                Classification::PROPERTY_PARENT_CLASSIFICATION_ID . ' IS NULL AND '.
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' != :codeId:',
                'bind' => [
                    'codeId' => $doubleSeasonCode->getId()
                ]
            ]);

            if ($code) {
                foreach ($classifications as $classification) {
                    $options[$classification->getCode()] = $classification->getName();
                }
            }

            $this->get(MaterialSample::PROPERTY_MATERIAL_TYPE_CODE_ID)->setOptions($options);
        }
        
        if ($this->has(MaterialSample::PROPERTY_SUPPLIER_CODE_ID)) {
            $suppliers = $entityManager->find(Supplier::class);

            $options = [0 => $translator->t(' - Izaberite - ')];
            foreach ($suppliers as $supplier) {
                $options[$supplier->getEntityId()] = $supplier->getName();
            }

            $this->get(MaterialSample::PROPERTY_SUPPLIER_CODE_ID)->setOptions($options);
        }

        $this->get(MaterialSample::PROPERTY_NOTE)->setAttribute(
            'class',
            $this->get(MaterialSample::PROPERTY_NOTE)->getAttribute('class') . ' sample-note'
        );

        foreach ($this->getElements() as $element) {
            $element->setAttribute('class', $element->getAttribute('class') . ' form-control' . ($element->getName() === 'deliveryTime' ? ' datepicker' : ''));
        }
    }
}