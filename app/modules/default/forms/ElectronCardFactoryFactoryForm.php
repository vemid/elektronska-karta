<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Select;
use \Phalcon\Forms\Element\Hidden;

/**
 * Class ElectronCardFactoryFactoryForm
 */
class ElectronCardFactoryFactoryForm extends Form
{

    public function initialize()
    {
        $translate = $this->getDI()->getTranslator();
        $modelsManager = $this->getDI()->getModelsManager();

        /** @var ElectronCard $electronCard */
        $electronCard = $this->getUserOption('electronCard');
        /** @var ElectronCardFactory|null $electronCardFactory */
        $electronCardFactory = $this->getUserOption('electronCardFactory');
        $type = $this->getUserOption('type');

        $electronCardFactoryType = $type === ElectronCardFactory::BASIC ? '01' : '02';

        $query = $modelsManager->createBuilder()
            ->addFrom(MaterialFactory::class, 'mf')
            ->leftJoin(Classification::class, 'mf.subMaterialCodeId = c1.id', 'c1')
            ->leftJoin(Classification::class, 'c1.parentClassificationId = c2.id', 'c2')
            ->leftJoin(Classification::class, 'c2.parentClassificationId = c3.id', 'c3')
            ->where('c3.code = :code: or c2.code = :code2:' , [
                'code' => $electronCardFactoryType,
                'code2' => $electronCardFactoryType
            ])
            ;

        /** @var MaterialFactory[] $materialFactories */
        $materialFactories = $query->getQuery()->execute();

        $options = [];
        foreach ($materialFactories as $materialFactory) {
            $options[$materialFactory->getId()] = $materialFactory->getDisplayName();
        }

        $electronCardElement = new Hidden(ElectronCardFactory::PROPERTY_ELECTRON_CARD_ID);
        $electronCardElement->setDefault($electronCard->getId());

        $typeElement = new Hidden(ElectronCardFactory::PROPERTY_TYPE);
        $typeElement->setDefault($type);

        $materialFactorySelect = new Select('materialFactoryId');
        $materialFactorySelect->setLabel($translate->t('Material Factory'));
        $materialFactorySelect->setOptions($options);
        $materialFactorySelect->setAttribute('class', 'search required');

        if ($electronCardFactory) {
            $materialFactorySelect->setDefault($electronCardFactory->getMaterialFactoryId());
        }

        $this->add($electronCardElement);
        $this->add($typeElement);
        $this->add($materialFactorySelect);
    }
}
