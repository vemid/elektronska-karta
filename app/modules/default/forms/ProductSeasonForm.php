<?php

use \Vemid\Form\Form;
use \Vemid\Entity\Repository\ClassificationRepository;
use \Phalcon\Forms\Element\Select;


/**
 * Class ProductSeasonForm
 */
class ProductSeasonForm extends Form
{
    public function initialize()
    {

        $entityManager = $this->getDI()->getEntityManager();

        /** @var CodeType $codeType */
        $codeType = $entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        $classifications = null;
        if ($codeType) {
            /** @var ClassificationRepository $classificationRepository */
            $classificationRepository = $entityManager->getRepository(Classification::class);
            $classifications = $classificationRepository->getSeasonIncludedInProducts( $codeType);
        }

        $options = [];
        foreach ($classifications as $classification) {
            $options[$classification->getCode()] = $classification->getName().' ( '. $classification->getCode().' )';
        }

        $users = [];
        foreach ($this->entityManager->find(User::class) as $user) {
            if($user->getEmail()) {
                $users[$user->getId()] = $user->getEmail();
            }
        }

        $select = new Select('season', $options);
        $select->setLabel('Sezona');
        $select->setAttribute('class', 'search');
        $select->setAttribute('multiple', 'true');
        $select->setName($select->getName() . '[]');
        $select->setDefault([]);

        $this->add($select);

        $users = new Select('users', $users);
        $users->setLabel('Korisnici');
        $users->setAttribute('class', 'search');
        $users->setAttribute('multiple', 'true');
        $users->setName($users->getName() . '[]');
        $users->setDefault([]);

        $this->add($users);

    }

}