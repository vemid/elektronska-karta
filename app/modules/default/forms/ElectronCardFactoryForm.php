<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Text;
use \Vemid\Validation\Validator\PresenceOf;
use \Phalcon\Forms\Element\Hidden;
use \Phalcon\Forms\Element\Select;

class ElectronCardFactoryForm extends Form
{
    public function initialize()
    {
        $translate = $this->getDI()->getTranslator();
        $entityManager = $this->getDI()->getEntityManager();

        /** @var ElectronCard $electronCard */
        $electronCard = $this->getUserOption('electronCard');
        /** @var ElectronCardFactory|null $electronCardFactory */
        $electronCardFactory = $this->getUserOption('electronCardFactory');
        $type = $this->getUserOption('type');

        $electronCardElement = new Hidden(ElectronCardFactory::PROPERTY_ELECTRON_CARD_ID);
        $electronCardElement->setDefault($electronCard->getId());

        $typeElement = new Hidden(ElectronCardFactory::PROPERTY_TYPE);
        $typeElement->setDefault($type);

        $options = [];
        $subMaterialCodeOptions = [];

        /** @var \CodeType $codeType */
        $codeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        /** @var \CodeType $codeTypeType */
        $codeTypeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        /** @var \Code $codeCategory */
        $codeCategory = $entityManager->findOne(\Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '1',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        /** @var \Code $code */
        $code = $entityManager->findOne(\Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '00',
                'codeTypeId' => $codeTypeType->getId()
            ]
        ]);

        $classifications = $codeCategory->getClassificationCategories([
            Classification::PROPERTY_PARENT_CLASSIFICATION_ID . ' IS NULL AND ' .
            Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeId:',
            'bind' => [
                'codeId' => $code->getId(),
            ]
        ]);

        foreach ($classifications as $classification) {
            $options[$classification->getCode()] = $classification->getName();
        }

        if ($electronCardFactory) {
            $subMaterialCodeClassification = $electronCardFactory->getClassificationCode();
            $materialCodeClassification = $subMaterialCodeClassification ? $subMaterialCodeClassification->getParentClassification() : null;
            $materialTypeCodeClassification = $materialCodeClassification ? $materialCodeClassification->getParentClassification() : null;

            $materialCodeOptions = [];
            if ($materialTypeCodeClassification) {
                foreach ($materialTypeCodeClassification->getChildClassifications() as $childClassification) {
                    $materialCodeOptions[$childClassification->getCode()] = $childClassification->getName();
                }
            }

            $subMaterialCodeOptions = [];
            if ($materialCodeClassification) {
                foreach ($materialCodeClassification->getChildClassifications() as $childClassification) {
                    $subMaterialCodeOptions[$childClassification->getCode()] = $childClassification->getName();
                }
            }
        }

        $name = new Text('name');
        $name->setAttribute('class', 'form-control required complete');
        $name->setLabel($translate->t('Naziv'));
        $name->addValidator(new PresenceOf([
            'message' => $translate->t('Obavezno polje'),
        ]));

        if ($electronCardFactory) {
            $name->setDefault($electronCardFactory->getName());
        }

        /** @var CodeType $codeTypeClassification */
        $codeTypeClassification = $entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        /** @var Code $codeProgram */
        $codeProgram = $entityManager->findOne(Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '00',
                'codeTypeId' => $codeTypeClassification->getId()
            ]
        ]);

        /** @var Classification $classificationSubsidiaryProgram */
        $classificationSubsidiaryProgram = $entityManager->findOne(Classification::class, [
            'code = :code: AND classificationTypeCodeId = :classificationTypeCodeId:',
            'bind' => [
                'code' => '02',
                'classificationTypeCodeId' => $codeProgram->getId()
            ]
        ]);

        /** @var Classification $classificationBasicProgram */
        $classificationBasicProgram = $entityManager->findOne(Classification::class, [
            'code = :code: AND classificationTypeCodeId = :classificationTypeCodeId:',
            'bind' => [
                'code' => '01',
                'classificationTypeCodeId' => $codeProgram->getId()
            ]
        ]);

        $materialTypeIdSubsidiaryOptions = [];
        $materialTypeIdDefault = '';
        foreach ($classificationSubsidiaryProgram->getChildClassifications() as $childClassification) {
            $materialTypeIdSubsidiaryOptions[$childClassification->getCode()] = $childClassification->getName();

            if (strtolower($childClassification->getName()) === 'konac') {
                $materialTypeIdDefault = $childClassification->getCode();
            }
        }

        $materialTypeIdBasicOptions = [];
        foreach ($classificationBasicProgram->getChildClassifications() as $childClassification) {
            $materialTypeIdBasicOptions[$childClassification->getCode()] = $childClassification->getName();
        }

        $classificationStitchProgramOptions = [];
        if ($materialTypeIdDefault) {
            /** @var Classification $classificationStitchProgram */
            $classificationStitchProgram = $entityManager->findOne(Classification::class, [
                'code = :code: AND classificationTypeCodeId = :classificationTypeCodeId:',
                'bind' => [
                    'code' => $materialTypeIdDefault,
                    'classificationTypeCodeId' => $codeProgram->getId()
                ]
            ]);

            if ($classificationStitchProgram) {
                foreach ($classificationStitchProgram->getChildClassifications() as $childClassificationStitch) {
                    $classificationStitchProgramOptions[$childClassificationStitch->getCode()] = $childClassificationStitch->getName();
                }
            }
        }

        if ($type === ElectronCardFactory::BASIC) {
            $materialCodeOptions = $materialTypeIdBasicOptions;
        } else {
            $materialCodeOptions = $materialTypeIdSubsidiaryOptions;
        }

        if ($type === ElectronCardFactory::STRING) {
            $subMaterialCodeOptions = $classificationStitchProgramOptions;
        }

        $materialTypeCodeIdElement = new Select('materialTypeCodeId', $options);
        $materialTypeCodeIdElement->setLabel('Material type code');
        $materialTypeCodeIdElement->setAttribute('class', 'search');
        $materialTypeCodeIdElement->setDefault($type === ElectronCardFactory::BASIC ? '01' : '02');

        $materialCodeIdElement = new Select('materialCodeId', (count($materialCodeOptions) ? $materialCodeOptions : $subMaterialCodeOptions));
        $materialCodeIdElement->setLabel('Material code');
        $materialCodeIdElement->setAttribute('class', 'search');
        if ($type === ElectronCardFactory::STRING) {
            $materialCodeIdElement->setDefault($materialTypeIdDefault);
        }

        $subMaterialCode = new Select('subMaterialCodeId', $subMaterialCodeOptions);
        $subMaterialCode->setLabel('Sub material code');
        $subMaterialCode->setAttribute('class', 'search');

        if ($electronCardFactory && $electronCardFactory->getClassificationCode()) {
            $subMaterial = $electronCardFactory->getClassificationCode();
            $parentMaterialClassification = $subMaterial->getParentClassification();
            $parentParentMaterialClassification = $parentMaterialClassification->getParentClassification();

            $subMaterialCode->setDefault($subMaterial ? $subMaterial->getCode() : '');
            $materialCodeIdElement->setDefault($parentParentMaterialClassification ? $parentMaterialClassification->getCode() : ($subMaterial ? $subMaterial->getCode() : ''));
            $materialTypeCodeIdElement->setDefault($parentParentMaterialClassification ? $parentParentMaterialClassification->getCode() : ($parentMaterialClassification ? $parentMaterialClassification->getCode() : ''));
        }

        $this->add($electronCardElement);
        $this->add($typeElement);
        $this->add($materialTypeCodeIdElement);
        $this->add($materialCodeIdElement);
        $this->add($subMaterialCode);
        $this->add($name);

        if (!$electronCardFactory || !$materialSketch = $electronCardFactory->getMaterialSketch()) {
            $materialSketch = new MaterialSketch();
        }

        $form = $materialSketch->getForm(['classificationCodeId']);
        foreach ($form->getElements() as $element) {
            if ($element->getName() === MaterialSketch::PROPERTY_SUPPLIER_NAME) {
                $element->setAttributes([
                    'data-search' => 'true',
                    'data-table' => Supplier::class,
                    'data-column' => 'name',
                    'data-hidden-field' => 'supplierId',
                    'class' => 'required'
                ]);
            }

            $this->add($element);
        }
    }
}
