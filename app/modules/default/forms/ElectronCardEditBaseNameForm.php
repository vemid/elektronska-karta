<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\TextArea;
use \Vemid\Validation\Validator\PresenceOf;

class ElectronCardEditBaseNameForm extends Form
{
    public function initialize()
    {
        /** @var ElectronCard $electronCard */
        $electronCard = $this->getEntity();
        $form = $electronCard->getForm();

        $this->add($form->get(ElectronCard::PROPERTY_BASE_PRODUCT_ID));
        $this->add($form->get(ElectronCard::PROPERTY_BASE_NAME));
    }
}