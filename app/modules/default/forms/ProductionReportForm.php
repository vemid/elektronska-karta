<?php


class ProductionReportForm extends \Vemid\Form\Form
{
    public function initialize()
    {
        $entityManager = $this->getDI()->getEntityManager();

        /** @var CodeType $codeType */
        $codeType = $entityManager->findOne(\CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        /** @var Code $materialTypeCode */
        $materialTypeCode = $entityManager->findOne(Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '3',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        /** @var CodeType $codeTypeClassification */
        $codeTypeClassification = $entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        /** @var Code $doubleSeasonCode */
        $doubleSeasonCode = $entityManager->findOne(Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '13',
                'codeTypeId' => $codeTypeClassification->getId()
            ]
        ]);


        $currentYear = date('Y');
        $nextYear = $currentYear + 1;
        $previousYear = $currentYear - 1;

        $doubleSeasonClassifications = $entityManager->find(Classification::class, [
            Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
            Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId: AND (name LIKE :like1: OR name LIKE :like2: OR name LIKE :like3:)',
            'bind' => [
                'codeCategoryId' => $materialTypeCode->getId(),
                'codeTypeId' => $doubleSeasonCode->getId(),
                'like1' => "%$currentYear%",
                'like2' => "%$previousYear%",
                'like3' => "%$nextYear%"
            ]
        ]);

        $seasonOptions = [];
        foreach ($doubleSeasonClassifications as $classification) {
            $seasonOptions[$classification->getId()] = $classification->getName();
        }

        $seasonElement = new \Phalcon\Forms\Element\Select('seasonReport');
        krsort($seasonOptions);
        $options = [0 => '-- Izaberite --'] + $seasonOptions;

        $seasonElement->setOptions($options);
        $seasonElement->setLabel('Sezona');
        $seasonElement->setAttribute('class', 'form-control search');

        $prioritiesElement = new \Phalcon\Forms\Element\Select('priorities');
        $prioritiesElement->setLabel('Prioriteti');
        $prioritiesElement->setOptions([0 => '-- Izaberite --']);
        $prioritiesElement->setAttribute('class', 'form-control search');

        $this->add($seasonElement);
        $this->add($prioritiesElement);
    }
}
