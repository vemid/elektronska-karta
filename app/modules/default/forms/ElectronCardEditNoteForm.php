<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\TextArea;
use \Vemid\Validation\Validator\PresenceOf;

class ElectronCardEditNoteForm extends Form
{
    public function initialize()
    {
        $translate = $this->getDI()->getTranslator();


        $note = new TextArea(ElectronCard::PROPERTY_NOTE);
        $note->setAttribute('class', 'form-control required complete');
        $note->setLabel($translate->t('Naziv Artikla'));
        $note->addValidator(new PresenceOf([
            'message' => $translate->t('Obavezno polje'),
        ]));

        /** @var ElectronCard $electronCard */
        $electronCard = $this->getEntity();
        $note->setDefault($electronCard->getNote());

        $this->add($note);
    }
}