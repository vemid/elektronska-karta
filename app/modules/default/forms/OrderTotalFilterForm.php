<?php

use \Vemid\Form\Form;
use \Vemid\Entity\Type;
use \Phalcon\Forms\Element\Select;

/**
 * Class ElectronCardFilterForm
 */
class OrderTotalFilterForm extends Form
{
    public function initialize()
    {
        $entityManager = $this->getDI()->getEntityManager();
        $modelsManager = $this->getDI()->getModelsManager();

        /** @var Order $order */
        $order = $this->getUserOption('order');

        /** @var \CodeType $codeType */
        $codeType = $entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        /** @var \CodeType $codeTypeCategory */
        $codeTypeCategory = $entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        $codeIds = [];
        if ($codeTypeCategory) {
            /** @var Code[] $codes */
            $codes = $entityManager->find(Code::class, [
                'codeTypeId = :codeTypeId:',
                'bind' => [
                    'codeTypeId' => $codeTypeCategory->getId()
                ]
            ]);

            foreach ($codes as $code) {
                $codeIds[] = $code->getId();
            }
        }

        if ($codeType) {
            /** @var Code $oldCollectionCode */
            $oldCollectionCode = $entityManager->findOne(Code::class, [
                'code = :code: AND codeTypeId = :codeTypeId:',
                'bind' => [
                    'code' => '07',
                    'codeTypeId' => $codeType->getId()
                ]
            ]);

            if ($oldCollectionCode) {
                /** @var ElectronCard[] $electronCards */
                $query = $modelsManager->createBuilder()
                    ->addFrom(Classification::class, 'c')
                    ->leftJoin(ProductClassification::class, 'c.id = pc.entityId', 'pc')
                    ->leftJoin(Product::class, 'p.id = pc.productId', 'p')
                    ->leftJoin(ProductSize::class, 'ps.productId = p.id', 'ps')
                    ->leftJoin(OrderTotal::class, 'ot.productSizeId = ps.id', 'ot')
                    ->where('ot.orderId = :orderId:', [
                        'orderId' => $order->getId()
                    ])
                    ->andWhere('pc.entityTypeId = :entityTypeId:', [
                        'entityTypeId' => Type::CLASSIFICATION
                    ])
                    ->andWhere('c.classificationTypeCodeId = :codeTypeId:', [
                        'codeTypeId' => $oldCollectionCode->getId()
                    ])
                    ->groupBy('c.id')
                    ->orderBy('c.name ASC');

                /** @var Classification[] $oldCollectionClassifications */
                $oldCollectionClassifications = $query->getQuery()->execute();

                $oldCollectionElement = new Select('oldCollectionId', $oldCollectionClassifications, [
                    'using' => ['code', 'name'],
                    'useEmpty' => true,
                    'emptyText' => '-- Stara Kolekcija --'
                ]);

                $oldCollectionElement->setAttribute('class', 'form-control search');

                $this->add($oldCollectionElement);
            }

            /** @var Code $codeProgram */
            $codeProgram = $entityManager->findOne(Code::class, [
                'code = :code: AND codeTypeId = :codeTypeId:',
                'bind' => [
                    'code' => '00',
                    'codeTypeId' => $codeType->getId()
                ]
            ]);

            if ($codeProgram) {

                /** @var ElectronCard[] $electronCards */
                $query = $modelsManager->createBuilder()
                    ->addFrom(Classification::class, 'c')
                    ->leftJoin(ProductClassification::class, 'c.id = pc.entityId', 'pc')
                    ->leftJoin(Product::class, 'p.id = pc.productId', 'p')
                    ->leftJoin(ProductSize::class, 'ps.productId = p.id', 'ps')
                    ->leftJoin(OrderTotal::class, 'ot.productSizeId = ps.id', 'ot')
                    ->leftJoin(Classification::class, 'cp.id = c.parentClassificationId', 'cp')
                    ->where('ot.orderId = :orderId:', [
                        'orderId' => $order->getId()
                    ])
                    ->andWhere('pc.entityTypeId = :entityTypeId:', [
                        'entityTypeId' => Type::CLASSIFICATION
                    ])
                    ->andWhere('c.classificationCategoryCodeId IN ({codeIds:array})', [
                        'codeIds' => $codeIds
                    ])
                    ->andWhere('c.classificationTypeCodeId = :classificationTypeCodeId:', [
                        'classificationTypeCodeId' => $codeProgram->getId()
                    ])
                    ->andWhere('cp.parentClassificationId IS NULL')
                    ->andWhere('c.parentClassificationId IS NOT NULL')
                    ->orderBy('c.name ASC')
                    ->groupBy('c.name');

                /** @var Classification[] $genderClassifications */
                $genderClassifications = $query->getQuery()->execute();

                $genderElement = new Select('genderClassificationId', $genderClassifications, [
                    'using' => ['id', 'name'],
                    'useEmpty' => true,
                    'emptyText' => '-- Pol --'
                ]);

                $genderElement->setAttribute('class', 'form-control search');

                /** @var ElectronCard[] $electronCards */
                $query = $modelsManager->createBuilder()
                    ->addFrom(Classification::class, 'c')
                    ->leftJoin(ProductClassification::class, 'c.id = pc.entityId', 'pc')
                    ->leftJoin(Product::class, 'p.id = pc.productId', 'p')
                    ->leftJoin(ProductSize::class, 'ps.productId = p.id', 'ps')
                    ->leftJoin(OrderTotal::class, 'ot.productSizeId = ps.id', 'ot')
                    ->leftJoin(Classification::class, 'cp.id = c.parentClassificationId', 'cp')
                    ->leftJoin(Classification::class, 'cpp.id = cp.parentClassificationId', 'cpp')
                    ->where('ot.orderId = :orderId:', [
                        'orderId' => $order->getId()
                    ])
                    ->andWhere('pc.entityTypeId = :entityTypeId:', [
                        'entityTypeId' => Type::CLASSIFICATION
                    ])
                    ->andWhere('c.classificationCategoryCodeId IN ({codeIds:array})', [
                        'codeIds' => $codeIds
                    ])
                    ->andWhere('c.classificationTypeCodeId = :classificationTypeCodeId:', [
                        'classificationTypeCodeId' => $codeProgram->getId()
                    ])
                    ->andWhere('cpp.parentClassificationId IS NULL')
                    ->andWhere('cp.parentClassificationId IS NOT NULL')
                    ->andWhere('c.parentClassificationId IS NOT NULL')
                    ->orderBy('c.name ASC')
                    ->groupBy('c.name');

                /** @var Classification[] $groupModels */
                $groupModels = $query->getQuery()->execute();

                $groupModelElement = new Select('groupModelId', $groupModels, [
                    'using' => ['id', 'name'],
                    'useEmpty' => true,
                    'emptyText' => '-- Model --'
                ]);

                $groupModelElement->setAttribute('class', 'form-control search');

                $this->add($genderElement);
                $this->add($groupModelElement);
            }
        }
    }
}
