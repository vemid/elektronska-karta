<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Select;
use \Phalcon\Forms\Element\Text;
use \Vemid\Validation\Validator\PresenceOf;

/**
 * Class ProductCopyForm
 */
class ProductCopyForm extends Form
{
    public function initialize()
    {
        /** @var Product $product */
        if (!$product = $this->getEntity()) {
            $product = $this->getUserOption('product');
        }

        $entityManager = $this->getDI()->getEntityManager();

        $productHydrated = new \Vemid\Service\Product\Hydrator($product, $entityManager);

        $productTypeCode = $product->getProductCodeType();
        $productProgramClassification = $productHydrated->getProductProgram();
        $genderClassification = $productHydrated->getGender();
        $subProductGroupClassification = $productHydrated->getSubProductGroup();
        $productGroupClassification = $productHydrated->getProductGroup();
        $subSubProductGroupClassification = $productHydrated->getDubSubProductGroup();
        $colorClassification = $productHydrated->getColor();

        /** @var CodeType $codeTypeType */
        $codeTypeType = $entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        $seasonOptions = [0 => '-- Izaberite --'];

        if ($codeTypeType && $productTypeCode) {
            /** @var \Code $seasonCode */
            $seasonCode = $entityManager->findOne(\Code::class, [
                Code::PROPERTY_CODE_TYPE_ID .'= :classificationTypeId: AND ' .
                Code::PROPERTY_CODE . '= :code: ',
                'bind' => [
                    'code' => '05',
                    'classificationTypeId' => $codeTypeType->getId()
                ]
            ]);

            if ($seasonCode) {
                $classifications = $entityManager->find(Classification::class, [
                    Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
                    Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId: AND ' .
                    Classification::PROPERTY_PARENT_CLASSIFICATION_ID . ' IS NULL',
                    'bind' => [
                        'codeCategoryId' => $productTypeCode->getId(),
                        'codeTypeId' => $seasonCode->getId()
                    ]
                ]);

                foreach ($classifications as $classification) {
                    $seasonOptions[$classification->getCode()] = $classification->getName();
                }
            }
        }

        /** @var CodeType $codeType */
        $codeType = $entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);
        
        $translate = $this->getDI()->getTranslator();

        $productTypeData = [0 => '-- Izaberite --'];
        if ($codeType) {
            foreach ($codeType->getCodes() as $code) {
                $productTypeData[$code->getCode()] = $code->getName();
            }
        }

        $productType = new Select('productTypeCodeId', $productTypeData);
        $productType->setAttribute('class', 'form-control required search');
        $productType->setAttribute('data-position', 2);
        $productType->setAttribute('readonly', true);
        $productType->setAttribute('data-generator', 'false');
        $productType->setLabel($translate->t('Vrsta Proizvoda'));
        $productType->setDefault($productTypeCode->getCode());
        $productType->addValidator(new PresenceOf([
            'message' => $translate->t('Obavezno polje'),
        ]));

        $seasonField = new Select('season', $seasonOptions);
        $seasonField->setAttribute('class', 'form-control required search');
        $seasonField->setAttribute('data-position', 1);
        $seasonField->setAttribute('data-generator', 'true');
        $seasonField->setDefault($productHydrated->getSeason() ? $productHydrated->getSeason()->getCode() : 0);
        $seasonField->setLabel($translate->t('Sezona'));

        $productProgramOptions = $this->listProductProgramByProductType($productTypeCode->getCode());
        $productProgram = new Select('productProgram', $productProgramOptions);
        $productProgram->setAttribute('class', 'form-control required search');
        $productProgram->setAttribute('data-position', 3);
        $productProgram->setAttribute('data-generator', 'false');
        $productProgram->setDefault($productProgramClassification ? $productProgramClassification->getCode() : 0);
        $productProgram->setLabel($translate->t('Program Robe'));

        $genderOptions = $this->getChildrenClassificationOptions($productProgramClassification);
        $genderFieldClassification = new Select('genderClassification', $genderOptions);
        $genderFieldClassification->setAttribute('class', 'form-control required search');
        $genderFieldClassification->setAttribute('data-position', 4);
        $genderFieldClassification->setAttribute('data-generator', 'false');
        $genderFieldClassification->setDefault($genderClassification ? $genderClassification->getCode() : 0);
        $genderFieldClassification->setLabel($translate->t('Klasifikacija po polu'));

        $productGroupOptions = $this->getChildrenClassificationOptions($genderClassification);
        $productGroup = new Select('productGroup', $productGroupOptions);
        $productGroup->setAttribute('class', 'form-control required search');
        $productGroup->setAttribute('data-position', 5);
        $productGroup->setAttribute('data-generator', 'false');
        $productGroup->setDefault($productGroupClassification ? $productGroupClassification->getCode() : 0);
        $productGroup->setLabel($translate->t('Robna grupa'));

        $subProductGroupOptions = $this->getChildrenClassificationOptions($productGroupClassification);
        $subProductGroup = new Select('subProductGroup', $subProductGroupOptions);
        $subProductGroup->setAttribute('class', 'form-control required search');
        $subProductGroup->setAttribute('data-position', 6);
        $subProductGroup->setAttribute('data-generator', 'false');
        $subProductGroup->setDefault($subProductGroupClassification ? $subProductGroupClassification->getCode() : 0);
        $subProductGroup->setLabel($translate->t('Robna Pod Grupa'));

        $subSubProductGroupOptions = $this->getChildrenClassificationOptions($subProductGroupClassification);
        $subSubProductGroup = new Select('subSubProductGroup', $subSubProductGroupOptions);
        $subSubProductGroup->setAttribute('class', 'form-control required search');
        $subSubProductGroup->setAttribute('data-position', 7);
        $subSubProductGroup->setAttribute('data-generator', 'false');
        $subSubProductGroup->setDefault($subSubProductGroupClassification ? $subSubProductGroupClassification->getCode() : 0);
        $subSubProductGroup->setLabel($translate->t('Robna Pod Pod Grupa'));
        $subSubProductGroup->addValidator(new PresenceOf([
            'message' => $translate->t('Obavezno polje'),
        ]));

        if ($colorClassification->getCode() === $subSubProductGroupClassification->getCode()) {
            $colorClassification = clone $subSubProductGroupClassification;
            $subSubProductGroupClassification = clone $subProductGroupClassification;
        }

        $colorOptions = $this->getChildrenClassificationOptions($subSubProductGroupClassification);
        $color = new Select('color', $colorOptions);
        $color->setAttribute('class', 'form-control required search');
        $color->setAttribute('data-position', 8);
        $color->setAttribute('data-generator', 'true');
        $color->setDefault($colorClassification ? $colorClassification->getCode() : 0);
        $color->setLabel($translate->t('Boja'));

        $name = new Text('name');
        $name->setAttribute('class', 'form-control required');
        $name->setLabel($translate->t('Naziv Artikla'));
        $name->setDefault($product->getName());

        $produceTypeElement = new Select('produceType', [
            0 => '-- Izaberite --', 'PLETENINA' => 'Pletenina', 'STATIKA' => 'Statika'
        ]);

        $produceTypeElement->setLabel($translate->t('Produce Type'));
        $produceTypeElement->setAttribute('class', 'form-control search');
        $produceTypeElement->setDefault($product->getProduceType() ?: 0);

        $productTypeCodeElement = new Select('productTypeCode', []);
        $productTypeCodeElement->setAttribute('class', 'form-control search');
        $productTypeCodeElement->setLabel($translate->t('Product Type'));

        /** @var CodeType $codeTypeProductType */
        $codeTypeProductType = $entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCT_TYPE
            ]
        ]);

        $productTypeCodeOptions = [0 => '-- Izaberite --'];
        if ($codeTypeProductType) {
            /** @var Code[] $codes */
            $codes = $entityManager->find(Code::class, [
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeTypeId' => $codeTypeProductType->getId()
                ]
            ]);

            foreach ($codes as $codeC) {
                $productTypeCodeOptions[$codeC->getCode()] = $codeC->getName();
            }
        }

        $productTypeCodeElement->setOptions($productTypeCodeOptions);
        $productTypeCodeElement->setDefault($productHydrated->getProductCode() ? $productHydrated->getProductCode()->getCode() : 0);
        $otherClassifications = $this->getOtherClassifications($productTypeCode);

        $brandOptions = [0 => '-- Izaberite --'] + $otherClassifications['brand'];
        $brand = new Select('brand', $brandOptions);
        $brand->setAttribute('class', 'form-control search');
        $brand->setLabel($translate->t('Brend'));
        $brand->setDefault($productHydrated->getBrand() ? $productHydrated->getBrand()->getCode() : 0);

        $ageOptions = [0 => '-- Izaberite --'] + $otherClassifications['age'];
        $age = new Select('age', $ageOptions);
        $age->setAttribute('class', 'form-control search');
        $age->setLabel($translate->t('Uzrast'));
        $age->setDefault($productHydrated->getAge() ? $productHydrated->getAge()->getCode() : 0);

        $collectionOptions = [0 => '-- Izaberite --'] + $otherClassifications['collection'];
        $collection = new Select('collection', $collectionOptions);
        $collection->setAttribute('class', 'form-control search');
        $collection->setLabel($translate->t('Kolekcija'));
        $collection->setDefault($productHydrated->getCollection() ? $productHydrated->getCollection()->getCode() : 0);

        $oldCollectionOptions = [0 => '-- Izaberite --'] + $otherClassifications['oldCollection'];
        $oldCollection = new Select('oldCollection', $oldCollectionOptions);
        $oldCollection->setAttribute('class', 'form-control search');
        $oldCollection->setLabel($translate->t('Stara kolekcija'));
        $oldCollection->setDefault($productHydrated->getOldCollection() ? $productHydrated->getOldCollection()->getCode() : 0);

        $doubleSeasonOptions = [0 => '-- Izaberite --'] + $otherClassifications['doubleSeason'];
        $doubleSeason = new Select('doubleSeason', $doubleSeasonOptions);
        $doubleSeason->setAttribute('class', 'form-control search');
        $doubleSeason->setLabel($translate->t('Dupla Sezona'));
        $doubleSeason->setDefault($productHydrated->getDoubleSeason() ? $productHydrated->getDoubleSeason()->getCode() : 0);

        $manufacturer = new Select('manufacturer');
        $manufacturer->setAttribute('class', 'form-control search');
        $manufacturer->setLabel($translate->t('Proizvodjač'));

        /** @var CodeType $attributeCodeType */
        $attributeCodeType = $this->getDI()->getEntityManager()->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::ATTRIBUTES
            ]
        ]);

        $manufacturerCode = null;
        if ($attributeCodeType) {
            /** @var Code $manufacturerCode */
            $manufacturerCode = $this->getDI()->getEntityManager()->findOne(Code::class, [
                Code::PROPERTY_CODE . ' = :code: AND ' .
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                'bind' => [
                    'code' => '13',
                    'codeTypeId' => $attributeCodeType->getId()
                ]
            ]);
        }

        if ($manufacturerCode) {
            /** @var Attribute[] $manufacturers */
            $manufacturers = $this->getDI()->getEntityManager()->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $manufacturerCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($manufacturers as $manufacturerEntity) {
                $options[$manufacturerEntity->getId()] = $manufacturerEntity->getName();
            }

            $manufacturer->setOptions($options);
            $manufacturer->setDefault($productHydrated->getManufacturer() ? $productHydrated->getManufacturer()->getId() : 0);
        }

        $seasonCollection = new Select('seasonCollection');
        $seasonCollection->setAttribute('class', 'form-control required search');
        $seasonCollection->setLabel($translate->t('Karakteristika sezone'));

        /** @var Code $seasonCollectionCode */
        $seasonCollectionCode = $this->getDI()->getEntityManager()->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '03',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($seasonCollectionCode) {
            /** @var Attribute[] $seasonCollections */
            $seasonCollections = $this->getDI()->getEntityManager()->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $seasonCollectionCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($seasonCollections as $seasonCollectionOption) {
                $options[$seasonCollectionOption->getId()] = $seasonCollectionOption->getName();
            }

            $seasonCollection->setOptions($options);
            $seasonCollection->setDefault($productHydrated->getSeasonCollection() ? $productHydrated->getSeasonCollection()->getId() : 0);
        }

        $designType = new Select('dezenType');
        $designType->setAttribute('class', 'form-control required search');
        $designType->setLabel($translate->t('Tip Dezena'));

        /** @var Code $designTypeCode */
        $designTypeCode = $this->getDI()->getEntityManager()->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '05',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($designTypeCode) {
            /** @var Attribute[] $dezenTypes */
            $designTypes = $this->getDI()->getEntityManager()->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $designTypeCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($designTypes as $designTypeEntity) {
                $options[$designTypeEntity->getId()] = $designTypeEntity->getName();
            }

            $designType->setOptions($options);
            $designType->setDefault($productHydrated->getDesignType() ? $productHydrated->getDesignType()->getId() : 0);
        }

        $printType = new Select('printType');
        $printType->setAttribute('class', 'form-control required search');
        $printType->setLabel($translate->t('Tip Stampe'));

        /** @var Code $printTypeCode */
        $printTypeCode = $this->getDI()->getEntityManager()->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '08',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($printTypeCode) {
            /** @var Attribute[] $printTypes */
            $printTypes = $this->getDI()->getEntityManager()->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $printTypeCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($printTypes as $printTypeEntity) {
                $options[$printTypeEntity->getId()] = $printTypeEntity->getName();
            }

            $printType->setOptions($options);
            $printType->setDefault($productHydrated->getPrintType() ? $productHydrated->getPrintType()->getId() : 0);
        }

        $applicationAttribute = new Select('applicationAttribute');
        $applicationAttribute->setAttribute('class', 'form-control required search');
        $applicationAttribute->setLabel($translate->t('Aplikacija'));

        /** @var Code $applicationAttributeCode */
        $applicationAttributeCode = $this->getDI()->getEntityManager()->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '09',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($applicationAttributeCode) {
            /** @var Attribute[] $applicationAttributes */
            $applicationAttributes = $this->getDI()->getEntityManager()->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $applicationAttributeCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($applicationAttributes as $applicationAttributeEntity) {
                $options[$applicationAttributeEntity->getId()] = $applicationAttributeEntity->getName();
            }

            $applicationAttribute->setOptions($options);
            $applicationAttribute->setDefault($productHydrated->getApplication() ? $productHydrated->getApplication()->getId() : 0);
        }

        $modelType = new Select('modelType');
        $modelType->setAttribute('class', 'form-control required search');
        $modelType->setLabel($translate->t('Tip Modela'));

        /** @var Code $modelTypeCode */
        $modelTypeCode = $this->getDI()->getEntityManager()->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '04',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($modelTypeCode) {
            /** @var Attribute[] $modelTypes */
            $modelTypes = $this->getDI()->getEntityManager()->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $modelTypeCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($modelTypes as $modelTypeEntity) {
                $options[$modelTypeEntity->getId()] = $modelTypeEntity->getName();
            }

            $modelType->setOptions($options);
            $modelType->setDefault($productHydrated->getModelType() ? $productHydrated->getModelType()->getId() : 0);
        }

        $materialType = new Select('materialType');
        $materialType->setAttribute('class', 'form-control required search');
        $materialType->setLabel($translate->t('Vrsta Materijala'));

        /** @var Code $materialTypeCode */
        $materialTypeCode = $this->getDI()->getEntityManager()->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '20',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($materialTypeCode) {
            /** @var Attribute[] $materialTypes */
            $materialTypes = $this->getDI()->getEntityManager()->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $materialTypeCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($materialTypes as $materialTypeEntity) {
                $options[$materialTypeEntity->getId()] = $materialTypeEntity->getName();
            }

            $materialType->setOptions($options);
            $materialType->setDefault($productHydrated->getMaterialType() ? $productHydrated->getMaterialType()->getId() : 0);
        }


        $modelProductType = new Select('modelProductType');
        $modelProductType->setAttribute('class', 'form-control search');
        $modelProductType->setLabel($translate->t('Vrsta Robe'));

        /** @var Code $modelProductTypeCode */
        $modelProductTypeCode = $entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '24',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($modelProductTypeCode) {
            /** @var Attribute[] $modelProductTypes */
            $modelProductTypes = $entityManager->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $modelProductTypeCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($modelProductTypes as $modelProductTypeEntity) {
                $options[$modelProductTypeEntity->getId()] = $modelProductTypeEntity->getName();
            }

            $modelProductType->setOptions($options);
            $modelProductType->setDefault($productHydrated->getProductType() ? $productHydrated->getProductType()->getId() : 0);
        }

        $model = new Select('model');
        $model->setAttribute('class', 'form-control search');
        $model->setLabel($translate->t('Model'));

        /** @var Code $modelCode */
        $modelCode = $entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '25',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($modelCode) {
            /** @var Attribute[] $modelProductTypes */
            $models = $entityManager->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $modelCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($models as $modelsEntity) {
                $options[$modelsEntity->getId()] = $modelsEntity->getName();
            }

            $model->setOptions($options);
            $model->setDefault($productHydrated->getAttributeModel() ? $productHydrated->getAttributeModel()->getId() : 0);

        }

        $modelInsane = new Select('modelInsane');
        $modelInsane->setAttribute('class', 'form-control search');
        $modelInsane->setLabel($translate->t('Insane'));

        /** @var Code $modelInsaneCode */
        $modelInsaneCode = $entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '27',
                'codeTypeId' => $attributeCodeType->getId()
            ]
        ]);

        if ($modelInsaneCode) {
            /** @var Attribute[] $modelInsanes */
            $modelInsanes = $entityManager->find(Attribute::class, [
                Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :attributeCodeId:',
                'bind' => [
                    'attributeCodeId' => $modelInsaneCode->getId()
                ]
            ]);

            $options = [0 => '-- Izaberite --'];
            foreach ($modelInsanes as $modelInsaneEntity) {
                $options[$modelInsaneEntity->getId()] = $modelInsaneEntity->getName();
            }

            $modelInsane->setOptions($options);
            $modelInsane->setDefault($productHydrated->getAttributeInsane() ? $productHydrated->getAttributeInsane()->getId() : 0);

        }

        $priorityOptions = [];
        if ($oldCollectionClassification = $productHydrated->getOldCollection()) {
            foreach ($oldCollectionClassification->getPriorityClassifications() as $priorityClassification) {
                if (!$priority = $priorityClassification->getPriority()) {
                    continue;
                }

                $priorityOptions[$priority->getId()] = $priority;
            }
        }

        $supplierCode = new Text('supplierCode');
        $supplierCode->setAttribute('class', 'form-control');
        $supplierCode->setLabel($translate->t('Sifra Dobavlajca Artikla'));

        $priority = new Select('priorityId', []);
        $priority->setAttribute('class', 'form-control search');
        $priority->setLabel($translate->t('Prioritet'));
        $priority->setOptions($priorityOptions);
        $priority->setDefault($product->getPriorityId());

        /** @var \Vemid\Entity\Repository\ProductRepository $productRepository */
        $productRepository = $entityManager->getRepository(Product::class);
        $code = mb_substr($product->getCode(), 0, -2);

        $data = ['0' => $translate->t('-- Izaberite --')];
        $products = $productRepository->findPossiblePrentProducts($code);
        foreach ($products as $possiblePrentProduct) {
            $data[$possiblePrentProduct->getId()] = $possiblePrentProduct->getCode();
        }

        $parentProduct = new Select('parentId', $data);
        $parentProduct->setAttribute('class', 'search form-control');
        $parentProduct->setLabel($translate->t('Vezani proizvod'));
        $parentProduct->setDefault('0');

        $composition = new \Phalcon\Forms\Element\TextArea('composition');
        $composition->setAttribute('class', 'form-control');
        $composition->setLabel($translate->t('Sirovinski sastav'));

        $this->add($seasonField);
        $this->add($productType);
        $this->add($productProgram);
        $this->add($genderFieldClassification);
        $this->add($productGroup);
        $this->add($subProductGroup);
        $this->add($subSubProductGroup);
        $this->add($color);
        $this->add($name);
        $this->add($produceTypeElement);
        $this->add($productTypeCodeElement);
        $this->add($brand);
        $this->add($age);
        $this->add($collection);
        $this->add($oldCollection);
        $this->add($doubleSeason);
        $this->add($manufacturer);
        $this->add($seasonCollection);
        $this->add($applicationAttribute);
        $this->add($printType);
        $this->add($designType);
        $this->add($modelType);
        $this->add($materialType);
        $this->add($modelProductType);
        $this->add($model);
        $this->add($modelInsane);
        $this->add($supplierCode);
        $this->add($priority);
        $this->add($parentProduct);
        $this->add($composition);
    }

    /**
     * @param $code
     * @return array
     */
    private function listProductProgramByProductType($code)
    {
        $entityManager = $this->getDI()->getEntityManager();

        /** @var \CodeType $codeType */
        $codeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        /** @var \CodeType $codeTypeType */
        $codeTypeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        $data = [0 => '-- Izaberite --'];
        if ($codeType)  {
            /** @var Code $productTypeCode */
            $productTypeCode = $entityManager->findOne(Code::class, [
                'code = :code: AND codeTypeId = :codeTypeId:',
                'bind' => [
                    'code' => $code,
                    'codeTypeId' => $codeType->getId()
                ]
            ]);

            $code = null;
            if ($codeTypeType) {
                /** @var Code $code */
                $code = $entityManager->findOne(Code::class, [
                    'code = :code: AND codeTypeId = :codeTypeId:',
                    'bind' => [
                        'code' => '00',
                        'codeTypeId' => $codeTypeType->getId()
                    ]
                ]);
            }


            if ($productTypeCode && $code) {
                $classifications = $entityManager->find(Classification::class, [
                    Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
                    Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId: AND ' .
                    Classification::PROPERTY_PARENT_CLASSIFICATION_ID . ' IS NULL',
                    'bind' => [
                        'codeCategoryId' => $productTypeCode->getId(),
                        'codeTypeId' => $code->getId()
                    ]
                ]);

                foreach ($classifications as $classification) {
                    $data[$classification->getCode()] = $classification->getName();
                }
            }
        }

        return $data;
    }

    /**
     * @param Classification $classification
     * @return array
     */
    private function getChildrenClassificationOptions(Classification $classification)
    {
        $entityManager = $this->getDI()->getEntityManager();
        
        /** @var \Classification[] $classifications */
        $classifications = $entityManager->find(Classification::class, [
            \Classification::PROPERTY_PARENT_CLASSIFICATION_ID . ' = :parentClassificationId:',
            'bind' => [
                'parentClassificationId' => $classification->getId()
            ]
        ]);

        $data = [0 => '-- Izaberite --'];
        foreach ($classifications as $classificationOption) {
            $data[$classificationOption->getCode()] = $classificationOption->getName();
        }

        return $data;
    }
    
    private function getOtherClassifications(Code $productTypeCode)
    {
        $entityManager = $this->getDI()->getEntityManager();
        
        $data = [
            'brand' =>  [],
            'age' =>  [],
            'material' =>  [],
            'collection' =>  [],
            'oldCollection' =>  [],
            'doubleSeason' =>  [],
        ];

        /** @var \CodeType $codeType */
        $codeType = $entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' =>\CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        if (!$codeType) {
            return $data;
        }

        /** @var Code $brandCode */
        $brandCode = $entityManager->findOne(Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '01',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if ($brandCode) {
            $brandClassifications = $entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeCategoryId' => $productTypeCode->getId(),
                    'codeTypeId' => $brandCode->getId()
                ]
            ]);

            foreach ($brandClassifications as $classification) {
                $data['brand'][$classification->getCode()] = $classification->getName();
            }
        }

        /** @var Code $ageCode */
        $ageCode = $entityManager->findOne(Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '02',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if ($ageCode) {
            $ageClassifications = $entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeCategoryId' => $productTypeCode->getId(),
                    'codeTypeId' => $ageCode->getId()
                ]
            ]);

            foreach ($ageClassifications as $classification) {
                $data['age'][$classification->getCode()] = $classification->getName();
            }
        }

        /** @var Code $materialCode */
        $materialCode = $entityManager->findOne(Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '03',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if ($materialCode) {
            $materialClassifications = $entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeCategoryId' => $productTypeCode->getId(),
                    'codeTypeId' => $materialCode->getId()
                ]
            ]);

            foreach ($materialClassifications as $classification) {
                $data['material'][$classification->getCode()] = $classification->getName();
            }
        }

        /** @var Code $collectionCode */
        $collectionCode = $entityManager->findOne(Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '12',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if ($collectionCode) {
            $collectionClassifications = $entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeCategoryId' => $productTypeCode->getId(),
                    'codeTypeId' => $collectionCode->getId()
                ]
            ]);

            foreach ($collectionClassifications as $classification) {
                $data['collection'][$classification->getCode()] = $classification->getName();
            }
        }

        /** @var Code $oldCollectionCode */
        $oldCollectionCode = $entityManager->findOne(Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '07',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if ($oldCollectionCode) {
            $oldCollectionClassifications = $entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeCategoryId' => $productTypeCode->getId(),
                    'codeTypeId' => $oldCollectionCode->getId()
                ],
                'OrderItem' => 'name ASC'
            ]);

            foreach ($oldCollectionClassifications as $classification) {
                $data['oldCollection'][$classification->getCode()] = $classification->getName();
            }
        }

        /** @var Code $doubleSeasonCode */
        $doubleSeasonCode = $entityManager->findOne(Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '13',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if ($doubleSeasonCode) {
            $doubleSeasonClassifications = $entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeCategoryId' => $productTypeCode->getId(),
                    'codeTypeId' => $doubleSeasonCode->getId()
                ]
            ]);

            foreach ($doubleSeasonClassifications as $classification) {
                $data['doubleSeason'][$classification->getCode()] = $classification->getName();
            }
        }
        
        return $data;
    }
}