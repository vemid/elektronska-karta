<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Select;
use \Phalcon\Forms\Element\Text;

/**
 * Class MaterialFactoryForm
 */
class MaterialFactoryForm extends Form
{
    public function initialize()
    {
        $entityManager = $this->getDI()->getEntityManager();
        $translator = $this->getDI()->getTranslator();
        $form = $this->getEntity()->getForm();
        /** @var MaterialSample|null $defaultMaterialSample */
        $defaultMaterialSample = $this->getUserOption('materialSample');

        foreach ($form->getElements() as $element) {
            $this->add($element);
        }

        $options = [0 => $translator->t('-- Izaberite --')];
        $materialSamples = $entityManager->find(MaterialSample::class);
        foreach ($materialSamples as $materialSample) {
            $options[$materialSample->getEntityId()] = $materialSample->getDisplayName();
        }

        $this->get(MaterialFactory::PROPERTY_MATERIAL_SAMPLE_ID)->setOptions($options);
        if ($defaultMaterialSample) {
            $this->get(MaterialFactory::PROPERTY_MATERIAL_SAMPLE_ID)->setDefault($defaultMaterialSample->getId());
            $this->get(MaterialFactory::PROPERTY_MATERIAL_SAMPLE_ID)->setAttribute('disabled', true);
            $this->get(MaterialFactory::PROPERTY_MATERIAL_SAMPLE_ID)->setName('disabledMaterialSampleId');

            $materialSampleHidden = new \Phalcon\Forms\Element\Hidden(MaterialFactory::PROPERTY_MATERIAL_SAMPLE_ID);
            $materialSampleHidden->setDefault(1);
            $materialSampleHidden->setDefault($defaultMaterialSample->getId());
            $materialSampleHidden->setName('hiddenSampleId');
            $materialSampleHidden->setAttribute('id', 'hiddenSampleId');
            $this->add($materialSampleHidden, MaterialFactory::PROPERTY_SUPPLIER_CODE_ID);
        }

        $qty = new Text('qty');
        $qty->setLabel($translator->t('Kolicina'));

        $this->add($qty, MaterialFactory::PROPERTY_INVOICE_SCAN);

        $options = [0 => $translator->t('-- Izaberite --')];

        /** @var CodeType $codeType */
        $codeType = $entityManager->findOne(\CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        /** @var \Code $code */
        $code = $entityManager->findOne(\Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '1',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        /** @var CodeType $codeTypeClassification */
        $codeTypeClassification = $entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        /** @var Code $doubleSeasonCode */
        $doubleSeasonCode = $entityManager->findOne(Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '13',
                'codeTypeId' => $codeTypeClassification->getId()
            ]
        ]);

        $classifications = $code->getClassificationCategories([
            Classification::PROPERTY_PARENT_CLASSIFICATION_ID . ' IS NULL AND '.
            Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' != :codeId:',
            'bind' => [
                'codeId' => $doubleSeasonCode->getId()
            ]
        ]);

        foreach ($classifications as $classification) {
            $options[$classification->getCode()] = $classification->getName();
        }

        $materialTypeCodeIdElement = new Select('materialTypeCodeId', $options);
        $materialTypeCodeIdElement->setLabel('Material type code');
        $materialTypeCodeIdElement->setAttribute('class', $materialTypeCodeIdElement->getAttribute('class') . ' search');

        $materialCodeIdElement = new Select('materialCodeId', []);
        $materialCodeIdElement->setLabel('Material code');
        $materialCodeIdElement->setAttribute('class', $materialCodeIdElement->getAttribute('class') . ' search');

        $this->add($materialCodeIdElement, MaterialFactory::PROPERTY_SUPPLIER_CODE_ID);
        $this->add($materialTypeCodeIdElement, MaterialFactory::PROPERTY_SUPPLIER_CODE_ID);

        /** @var Code $materialTypeCode */
        $materialTypeCode = $entityManager->findOne(Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '1',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $doubleSeasonClassifications = $entityManager->find(Classification::class, [
            Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
            Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeCategoryId' => $materialTypeCode->getId(),
                'codeTypeId' => $doubleSeasonCode->getId()
            ]
        ]);

        $seasonOptions = [0 => '-- Izaberite --'];
        foreach ($doubleSeasonClassifications as $classification) {
            $seasonOptions[$classification->getId()] = $classification->getName();
        }
        $this->get(MaterialFactory::PROPERTY_SEASON_CLASSIFICATION_ID)->setOptions($seasonOptions);

        if ($this->has(MaterialFactory::PROPERTY_SUPPLIER_CODE_ID)) {
            $suppliers = $entityManager->find(Supplier::class,[
                'order' => 'name'
            ]);

            $options = [0 => $translator->t(' - Izaberite - ')];
            foreach ($suppliers as $supplier) {
                $options[$supplier->getEntityId()] = $supplier->getName();
            }

            $this->get(MaterialFactory::PROPERTY_SUPPLIER_CODE_ID)->setOptions($options);
        }

        $this->get(MaterialFactory::PROPERTY_NOTE)->setAttribute(
            'class',
            $this->get(MaterialFactory::PROPERTY_NOTE)->getAttribute('class') . ' sample-note'
        );

        foreach ($this->getElements() as $element) {
            $element->setAttribute('class', $element->getAttribute('class') . ' form-control' . ($element->getName() === 'deliveryTime' ? ' datepicker' : ''));
        }
    }
}