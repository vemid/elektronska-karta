<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Select;
use \Vemid\Entity\Type;
use \Phalcon\Forms\Element\Hidden;

/**
 * Class WarehouseOrderForm
 */
class WarehouseOrderForm extends Form
{
    public function initialize()
    {
        /** @var ChannelSupply $channelSupply */
        $channelSupply = $this->getUserOptions()['channelSupply'];
        $form = $this->getEntity()->getForm();
        $entityManager = $this->getDI()->getEntityManager();

        $selectElement = new Select('entity');
        $selectElement->setAttribute('class', 'search');

        /** @var ChannelSupplyEntity[] $channelSupplyEntities */
        $channelSupplyEntities = $entityManager->find(ChannelSupplyEntity::class, [
            ChannelSupplyEntity::PROPERTY_CHANNEL_SUPPLY_ID . ' = :channelSupplyId: AND ' .
            ChannelSupplyEntity::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:',
            'bind' => [
                'channelSupplyId' => $channelSupply->getEntityId(),
                'entityTypeId' => $channelSupply->getType() === ChannelSupply::MP ? Type::CODE : Type::CLIENT
            ]
        ]);

        $options = [];
        $ids = [];


        foreach ($channelSupplyEntities as $channelSupplyEntity) {
            $ids[] = $channelSupplyEntity->getEntityId();
        }

        $exitIds = [];
        $exitOptions = [];
        $exitChannelSupplyEntityTypes = $entityManager->find(ChannelSupply::class, [
            ChannelSupply::PROPERTY_TYPE . ' = :type1: ',
            'bind' => [
                'type1' => ChannelSupply::MP
            ],
        ]);

        foreach ($exitChannelSupplyEntityTypes as $exitChannelSupplyEntityType) {
            $exitIds[] = $exitChannelSupplyEntityType->getEntityId();
        }

        $exitChannelSupplyEntities = $entityManager->find(ChannelSupplyEntity::class, [
            ChannelSupplyEntity::PROPERTY_CHANNEL_SUPPLY_ID . ' IN ({ids:array})',
            'bind' => [
                'ids' => $exitIds
            ],
            'order' => 'id DESC'
        ]);

        if($exitChannelSupplyEntities){
            /** @var ChannelSupplyEntity $exitChannelSupplyEntity */
            foreach ($exitChannelSupplyEntities as $exitChannelSupplyEntity) {
                $exitOptions[$exitChannelSupplyEntity->getId()] = $exitChannelSupplyEntity->getDisplayName();
            }
        }

        if ($channelSupply->getType() === ChannelSupply::MP) {
            if (count($ids)) {
                $shops = $entityManager->find(Code::class, [
                    Code::PROPERTY_ID . ' IN ({ids:array})',
                    'bind' => [
                        'ids' => $ids
                    ]
                ]);

                foreach ($shops as $shop) {
                    $options[$shop->getId()] = $shop->getDisplayName();
                }
            }
        }

        if ($channelSupply->getType() === ChannelSupply::VP) {
            $clients = $entityManager->find(Client::class, [
                Client::PROPERTY_ID . ' IN ({ids:array})',
                'bind' => [
                    'ids' => $ids
                ]
            ]);

            foreach ($clients as $client) {
                $options[$client->getId()] = $client->getDisplayName();
            }
        }

        $priorityOptions = [];
        $priorities = $this->entityManager->find(Priority::class,[
            'priorityDate >= :date:',
            'bind' => [
                'date' => '2021-12-17'
            ]
        ]);

        /** @var Priority $priority */
        foreach ($priorities as $priority) {
//            $priorityOptions[$priority->getId()] = "Ulaz ".$priority->getPriorityNumber() . '-' . $priority->getPriorityDate()->getShortSerbianFormat() ."-".$priority->getName();
            $priorityOptions[$priority->getId()] = $priority->getPriorityDate()->getShortSerbianFormat() . " - Ulaz" .$priority->getPriorityNumber();
        }

        asort($priorityOptions);

        $hidden = new Hidden('channelSupplyId');
        $hidden->setDefault($channelSupply->getId());

        $selectElement->setOptions($options);
        $selectElement->setLabel('Za');

        $exitElement = new Select('exitEntity');
        $exitElement->setAttribute('class', 'form-control required search');

        $exitElement->setDefault('86');
        $exitElement->setOptions($exitOptions);
        $exitElement->setLabel('Iz');

        $priorityElement = new Select('priorityId');
        $priorityElement->setAttribute('class', 'form-control required search');

        $priorityElement->setDefault('--Izaberite--');
        $priorityElement->setOptions($priorityOptions);
        $priorityElement->setLabel('Prioritet');

        $this->add($hidden);
        $this->add($selectElement);
        $this->add($exitElement);
        $this->add($priorityElement);

        foreach ($form->getElements() as $element) {
            $this->add($element);
        }

        if ($channelSupply->getType() === ChannelSupply::MP) {
            $this->remove(WarehouseOrder::PROPERTY_TYPE);
        }
    }
}
