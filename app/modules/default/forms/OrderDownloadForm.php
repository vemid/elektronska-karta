<?php

use \Vemid\Form\Form;
use \Vemid\Entity\Repository\ClassificationRepository;
use \Phalcon\Forms\Element\Select;

/**
 * Class OrderDownloadForm
 */
class OrderDownloadForm extends Form
{
    public function initialize()
    {
        $entityManager = $this->getDI()->getEntityManager();

        /** @var User $user */
        $user = $this->getUserOption('user');

        /** @var Order $order */
        $order = $this->getUserOption('order');

        /** @var Client $client */
        $client = $entityManager->findOne(Client::class, [
            Client::PROPERTY_USER_ID . ' = :userId:',
            'bind' => [
                'userId' => $user->getId()
            ]
        ]);

        /** @var CodeType $codeType */
        $codeType = $entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        $classifications = null;
        if ($codeType) {
            /** @var ClassificationRepository $classificationRepository */
            $classificationRepository = $entityManager->getRepository(Classification::class);
            $classifications = $classificationRepository->getClassificationsIncludedInOrders($order, $codeType, !$user->isSuperAdmin() ? $client : null);
        }

        $options = [];
        foreach ($classifications as $classification) {
            $options[$classification->getCode()] = $classification->getName();
        }

        $select = new Select('oldCollections', $options);
        $select->setLabel('Kolekcija');
        $select->setAttribute('multiple', true);
        $select->setAttribute('style', 'height:200px');
        $select->setName($select->getName() . '[]');

        $this->add($select);
    }
}