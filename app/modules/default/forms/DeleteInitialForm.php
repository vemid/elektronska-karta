<?php

use \Vemid\Form\Form;
use \Vemid\Entity\Repository\ClassificationRepository;
use \Phalcon\Forms\Element\Select;

class DeleteInitialForm extends Form
{

    public function initialize()
    {

        $options = ['','1' => 'MP','2'=>'VP'];
        $select = new Select('type', $options);
        $select->setLabel('Kome ponistavate porudzbinu');
        //$select->setName($select->getName() . '[]');
        $select->setDefault([]);

        $this->add(
            new \Phalcon\Forms\Element\Hidden('hiddenInitialProductId')
        );

        $this->add($select);

    }
}