<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Select;
use \Vemid\Entity\Type;

/**
 * Class ChannelSupplyEntityForm
 */
class ChannelSupplyEntityForm extends Form
{
    public function initialize()
    {
        /** @var ChannelSupply $channelSupply */
        $channelSupply = $this->getUserOptions()['channelSupply'];

        /** @var \Vemid\CourierService\Handler $courierHandler */
        $courierHandler = $this->getUserOptions()['courierHandler'];

        $form = $this->getEntity()->getForm();

        foreach ($form->getElements() as $element) {
            $this->add($element);
        }

        $selectElement = new Select('entity');
        $selectElement->setAttribute('class', 'search');
        $options = [];

        if ($channelSupply->getType() === ChannelSupply::MP) {
            $codeTypeShop = $this->entityManager->findOne(CodeType::class, [
                CodeType::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'code' => CodeType::SHOPS
                ]
            ]);

            $shops = $this->entityManager->find(\Code::class, [
                'codeTypeId = :codeTypeId:',
                'bind' => [
                    'codeTypeId' => $codeTypeShop->getId()
                ],
                'order' => 'code'
            ]);

            foreach ($shops as $shop) {
                $channelSupplyShop = $this->entityManager->findOne(ChannelSupplyEntity::class, [
                    ChannelSupplyEntity::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId: AND ' .
                    ChannelSupplyEntity::PROPERTY_ENTITY_ID . ' = :entityId:',
                    'bind' => [
                        'entityTypeId' => Type::CODE,
                        'entityId' => $shop->getEntityId(),
                    ]
                ]);

                if ($channelSupplyShop) {
                    continue;
                }

                $options[$shop->getId()] = $shop->getName();
            }

            $selectElement->setLabel('MP');
        }

        if ($channelSupply->getType() === ChannelSupply::VP) {
            foreach ($this->entityManager->find(\Client::class) as $client) {
                $channelSupplyClient = $this->entityManager->findOne(ChannelSupplyEntity::class, [
                    ChannelSupplyEntity::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId: AND ' .
                    ChannelSupplyEntity::PROPERTY_ENTITY_ID . ' = :entityId:',
                    'bind' => [
                        'entityTypeId' => Type::CLIENT,
                        'entityId' => $client->getEntityId(),
                    ]
                ]);

                if ($channelSupplyClient) {
                    continue;
                }

                $options[$client->getId()] = $client->getName();
            }

            $selectElement->setLabel('VP');
        }

        $selectElement->setOptions($options);

        $townOptions = [];
        foreach ($courierHandler->getTowns() as $town) {
            $townOptions[$town['Id']] = $town['Name'];
        }

        $streetOptions = [];
        foreach ($courierHandler->getStreets() as $town) {
            $streetOptions[$town['Id']] = $town['Name'];
        }

        $this->get(ChannelSupplyEntity::PROPERTY_CHANNEL_SUPPLY_ID)->setDefault($channelSupply->getId());
        $this->get(ChannelSupplyEntity::PROPERTY_POSTAL_SERVICE_TOWN_ID)->setOptions($townOptions);
        $this->get(ChannelSupplyEntity::PROPERTY_STREET_ID)->setOptions($streetOptions);
        $this->remove(ChannelSupplyEntity::PROPERTY_ENTITY_ID);
        $this->remove(ChannelSupplyEntity::PROPERTY_ENTITY_TYPE_ID);
        $this->add($selectElement, 'channelSupplyId');
    }
}
