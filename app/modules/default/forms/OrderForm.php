<?php

class OrderForm extends \Vemid\Form\Form
{
    public function initialize()
    {
        $entityManager = $this->getDI()->getEntityManager();

        $form = $this->getEntity()->getForm();
        foreach ($form->getElements() as $element) {
            $this->add($element);
        }

        /** @var CodeType $codeType */
        $codeType = $entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        $oldCollectionCode = null;
        if ($codeType) {
            /** @var Code $oldCollectionCode */
            $oldCollectionCode = $entityManager->findOne(Code::class, [
                'code = :code: AND codeTypeId = :codeTypeId:',
                'bind' => [
                    'code' => '07',
                    'codeTypeId' => $codeType->getId()
                ]
            ]);
        }



        $data = [];
        if ($oldCollectionCode) {
            /** @var Classification[] $oldCollectionClassifications */
            $oldCollectionClassifications = $entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeTypeId' => $oldCollectionCode->getId()
                ]
            ]);

            foreach ($oldCollectionClassifications as $classification) {
//                if ($classification->getOrderClassifications()->count()) {
//                    continue;
//                }

                $data[$classification->getId()] = $classification->getName();
            }
        }

        /** @var CodeType $attributeCode */
        $attributeCode = $this->entityManager->findOne(CodeType::class,[
            CodeType::PROPERTY_CODE . ' = :type: ',
        'bind' => [
            'type' => CodeType::ATTRIBUTES
            ]
        ]);

        /** @var Code $attributeSeason */
        $attributeSeason = $this->entityManager->findOne(Code::class,[
            Code::PROPERTY_CODE_TYPE_ID . ' = :type: AND '. Code::PROPERTY_NAME . ' = :name: ' ,
            'bind' => [
                'type'=> $attributeCode->getId(),
                'name'=> 'Sezona'
            ]
        ]);

        /** @var Code $attributeYear */
        $attributeYear = $this->entityManager->findOne(Code::class,[
            Code::PROPERTY_CODE_TYPE_ID . ' = :type: AND '. Code::PROPERTY_NAME . ' = :name: ' ,
            'bind' => [
                'type'=> $attributeCode->getId(),
                'name'=> 'Godina'
            ]
        ]);

        $attributes = $this->entityManager->find(Attribute::class,[
            Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :codeId: ',
            'bind' => [
                'codeId' => $attributeSeason->getId()
            ]
        ]);

        $attributeYears = $this->entityManager->find(Attribute::class,[
            Attribute::PROPERTY_ATTRIBUTE_CODE_ID . ' = :codeId: ',
            'bind' => [
                'codeId' => $attributeYear->getId()
            ],
            'order' => 'id desc'
        ]);

        $attributeData = [];
        $attributeDataYears = [];
        if($attributes) {
            /** @var Attribute $attribute */
            foreach ($attributes as $attribute) {
                $attributeData[$attribute->getId()] = $attribute->getName();
            }
        }

        if($attributeYears) {
            /** @var Attribute $attribute */
            foreach ($attributeYears as $attribute) {
                $attributeDataYears[$attribute->getId()] = $attribute->getName();
            }
        }

        $select = new \Phalcon\Forms\Element\Select('oldCollection', $data);
        $select->setLabel('Klasifikacije');
        $select->setAttribute('class', 'search');
        $select->setAttribute('multiple', 'true');
        $select->setName($select->getName() . '[]');
        $select->setDefault([]);

        $this->add($select, Order::PROPERTY_NAME);

        $selectSeason = new \Phalcon\Forms\Element\Select('attributeSeason', $attributeData);
        $selectSeason->setLabel('Sezona');
        $selectSeason->setAttribute('class', 'form-control search');
        $selectSeason->setName($selectSeason->getName() . '[]');
        $selectSeason->setDefault([]);

        $this->add($selectSeason, Order::PROPERTY_NAME);

        $selectYear = new \Phalcon\Forms\Element\Select('attributeYear',$attributeDataYears);
        $selectYear->setLabel('Godina');
        $selectYear->setAttribute('class', 'form-control search');
        $selectYear->setName($selectYear->getName() . '[]');
        $selectYear->setDefault([]);

        $this->add($selectYear, Order::PROPERTY_NAME);
    }
}