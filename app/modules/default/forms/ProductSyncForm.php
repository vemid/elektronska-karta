<?php

use \Vemid\Form\Form;
use \Vemid\Entity\Repository\ClassificationRepository;
use \Phalcon\Forms\Element\Select;


/**
 * Class ProductSyncForm
 */
class ProductSyncForm extends Form
{
    public function initialize()
    {

        $entityManager = $this->getDI()->getEntityManager();

        /** @var CodeType $codeType */
        $codeType = $entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        $classifications = null;
        if ($codeType) {
            /** @var ClassificationRepository $classificationRepository */
            $classificationRepository = $entityManager->getRepository(Classification::class);
            $classifications = $classificationRepository->getClassificationsIncludedInProduceProducts( $codeType);
        }

        $options = [];
        foreach ($classifications as $classification) {
            $options[$classification->getCode()] = $classification->getName();
        }

        $select = new Select('oldCollections', $options);
        $select->setLabel('Kolekcija');
        $select->setAttribute('class', 'search');
        $select->setAttribute('multiple', 'true');
        $select->setName($select->getName() . '[]');
        $select->setDefault([]);

        $this->add($select);

    }

}