<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Select;

/**
 * Class MaterialSampleEditForm
 */
class MaterialSampleEditForm extends Form
{
    public function initialize()
    {
        $entityManager = $this->getDI()->getEntityManager();
        $translator = $this->getDI()->getTranslator();

        /** @var MaterialSample $materialSample */
        $materialSample = $this->getUserOption('materialSample');
        $materialTypeClassification = $materialSample->getMaterialTypeClassification();

        foreach ($materialSample->getForm()->getElements() as $element) {
            $class = get_class($element);
            /** @var \Phalcon\Forms\Element $newElement */
            $newElement = new $class($element->getName());
            $newElement->setAttributes($element->getAttributes());
            $newElement->setDefault($element->getValue());
            $newElement->setLabel($element->getLabel());
            $this->add($newElement);
        }

        $options = [0 => $translator->t('-- Izaberite --')];

        /** @var CodeType $codeType */
        $codeType = $entityManager->findOne(\CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        /** @var \Code $code */
        $code = $entityManager->findOne(\Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '1',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        /** @var CodeType $codeTypeClassification */
        $codeTypeClassification = $entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        /** @var Code $doubleSeasonCode */
        $doubleSeasonCode = $entityManager->findOne(Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '13',
                'codeTypeId' => $codeTypeClassification->getId()
            ]
        ]);

        $classifications = $code->getClassificationCategories([
            Classification::PROPERTY_PARENT_CLASSIFICATION_ID . ' IS NULL AND '.
            Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' != :codeId:',
            'bind' => [
                'codeId' => $doubleSeasonCode->getId()
            ]
        ]);

        foreach ($classifications as $classification) {
            $options[$classification->getCode()] = $classification->getName();
        }

        $this->get(MaterialSample::PROPERTY_MATERIAL_TYPE_CODE_ID)->setOptions($options);
        $this->get(MaterialSample::PROPERTY_MATERIAL_TYPE_CODE_ID)->setDefault($materialTypeClassification->getCode());

        $suppliers = $entityManager->find(Supplier::class);

        $supplierOptions = [0 => $translator->t('-- Izaberite --')];
        foreach ($suppliers as $supplier) {
            $supplierOptions[$supplier->getEntityId()] = $supplier->getName();
        }

        $this->get(MaterialSample::PROPERTY_SUPPLIER_CODE_ID)->setOptions($supplierOptions);

        $materialCodeOptions = [0 => $translator->t('-- Izaberite --')];
        /** @var Classification[] $materialCodeClassifications */
        $materialCodeClassifications = $entityManager->find(Classification::class, [
            Classification::PROPERTY_PARENT_CLASSIFICATION_ID . ' = :parentId:',
            'bind' => [
                'parentId' => $materialTypeClassification->getId()
            ]
        ]);

        foreach ($materialCodeClassifications as $materialCodeClassification) {
            $materialCodeOptions[(string)$materialCodeClassification->getCode()] = $materialCodeClassification->getName();
        }

        $this->get(MaterialSample::PROPERTY_MATERIAL_CODE_ID)->setOptions($materialCodeOptions);
        $this->get(MaterialSample::PROPERTY_MATERIAL_CODE_ID)->setDefault($materialSample->getMaterialClassification()->getCode());


        /** @var Classification[] $subMaterialCodeClassifications */
        $subMaterialCodeClassifications = $entityManager->find(Classification::class, [
            Classification::PROPERTY_PARENT_CLASSIFICATION_ID . ' = :parentId:',
            'bind' => [
                'parentId' => $materialSample->getMaterialCodeId()
            ]
        ]);

        $subOptions = [0 => $translator->t('-- Izaberite --')];
        foreach ($subMaterialCodeClassifications as $subMaterialCodeClassification) {
            $subOptions["{$subMaterialCodeClassification->getCode()}"] = $subMaterialCodeClassification->getName();
        }

        $this->get(MaterialSample::PROPERTY_SUB_MATERIAL_CODE_ID)->setOptions($subOptions);
        $this->get(MaterialSample::PROPERTY_SUB_MATERIAL_CODE_ID)->setDefault("{$materialSample->getSubMaterialClassification()->getCode()}");

        $this->get(MaterialSample::PROPERTY_NOTE)->setAttribute(
            'class',
            $this->get(MaterialSample::PROPERTY_NOTE)->getAttribute('class') . ' sample-note'
        );

        $subMaterialCode = new Select('subMaterialCodeId', []);
        $subMaterialCode->setLabel($translator->t('Pod Grupa Materijala'));
        $subMaterialCode->setAttribute('class', 'search');

        $this->add($subMaterialCode, 'materialCodeId');

        foreach ($this->getElements() as $element) {
            $element->setAttribute('class', $element->getAttribute('class') . ' form-control' . ($element->getName() === 'deliveryTime' ? ' datepicker' : ''));
        }
    }
}