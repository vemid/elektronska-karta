<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Select;
use \Phalcon\Forms\Element\Text;
use \Vemid\Validation\Validator\PresenceOf;

/**
 * Class OperatingListCopyForm
 */

class OperatingListCopyForm extends Form
{

    public function initialize()
    {
        /** @var OperatingList $operatingList */
        $operatingList = $this->getEntity();

        $entityManager = $this->getDI()->getEntityManager();

        $oldProduct = $operatingList->getProduct();

        $name = new Text('name');
        $name->setAttribute('class', 'form-control required');
        $name->setLabel('Prethodni artikal Artikla');
    }
}