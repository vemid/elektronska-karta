<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Select;
use \Vemid\Entity\Type;
use \Phalcon\Forms\Element\Hidden;

/**
 * Class WarehouseOrderForm
 */
class WarehouseOrderGroupForm extends Form
{
    public function initialize()
    {
        $form = $this->getEntity()->getForm();
        $entityManager = $this->getDI()->getEntityManager();

        $selectElement = new Select('channelSupplyEntityId');
        $selectElement->setAttribute('class', 'search');

        /** @var ChannelSupplyEntity[] $channelSupplyEntities */
        $channelSupplyEntities = $entityManager->find(ChannelSupplyEntity::class);

        $options = [];
        foreach ($channelSupplyEntities as $channelSupplyEntity) {
            $options[$channelSupplyEntity->getId()] = (string)$channelSupplyEntity->getEntity();
        }

        $selectElement->setOptions($options);
        $selectElement->setLabel('Za');

        $this->add($selectElement);

        foreach ($form->getElements() as $element) {
            $this->add($element);
        }
    }
}
