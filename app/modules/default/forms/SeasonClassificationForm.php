<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Text;
use \Vemid\Validation\Validator\PresenceOf;
use \Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\Alnum;
use Phalcon\Validation\Validator\StringLength;

class SeasonClassificationForm extends Form
{
    public function initialize()
    {
        $translate = $this->getDI()->getTranslator();

        $name = new Text('name');
        $name->setAttribute('class', 'form-control required');
        $name->setLabel($translate->t('Naziv Artikla'));
        $name->addValidator(new PresenceOf([
            'message' => $translate->t('Obavezno polje'),
        ]));

        $presenceOf = new PresenceOf([
            'message' => $translate->t('Obavezno polje'),
        ]);

        $unique = new Uniqueness([
            'model'   => new Classification(),
            'message' => $translate->t(':field mora biti jedinstvena'),
        ]);

        $alNum = new Alnum([
            'message' => $translate->t(':field mora sadržatis samo slova i brojeve'),
        ]);
        
        $stringLength = new StringLength([
            'max'            => 4,
            'min'            => 4,
            'messageMaximum' => $translate->t(':field mora sadržati 4 karaktera'),
            'messageMinimum' => $translate->t(':field mora sadržati 4 karaktera'),
        ]);

        $code = new Text('code');
        $code->setAttribute('class', 'form-control required');
        $code->setLabel($translate->t('Šifra'));
        $code->addValidators([
            $presenceOf,
            $unique,
            $alNum,
            $stringLength
        ]);

        $this->add($name);
        $this->add($code);
    }
}