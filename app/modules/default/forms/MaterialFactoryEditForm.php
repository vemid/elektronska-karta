<?php

use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Select;

/**
 * Class MaterialFactoryEditForm
 */
class MaterialFactoryEditForm extends Form
{
    public function initialize()
    {
        $entityManager = $this->getDI()->getEntityManager();
        $translator = $this->getDI()->getTranslator();
        /** @var MaterialFactory $materialFactory */
        $materialFactory = $this->getUserOption('materialFactory');
        $form = $materialFactory->getForm();

        foreach ($form->getElements() as $element) {
            $this->add($element);
        }

        $options = [0 => $translator->t('-- Izaberite --')];
        /** @var CodeType $codeType */
        $codeType = $entityManager->findOne(\CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        $code = null;
        if ($codeType) {
            /** @var \Code $code */
            $code = $entityManager->findOne(\Code::class, [
                Code::PROPERTY_CODE . ' = :code: AND ' .
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                'bind' => [
                    'code' => '1',
                    'codeTypeId' => $codeType->getId()
                ]
            ]);
        }

        $codeTypeClassification = null;
        if ($code) {
            /** @var CodeType $codeTypeClassification */
            $codeTypeClassification = $entityManager->findOne(CodeType::class, [
                'code = :code:',
                'bind' => [
                    'code' => CodeType::CLASSIFICATION_TYPE
                ]
            ]);
        }

        $doubleSeasonCode = null;
        if ($codeTypeClassification) {
            /** @var Code $doubleSeasonCode */
            $doubleSeasonCode = $entityManager->findOne(Code::class, [
                'code = :code: AND codeTypeId = :codeTypeId:',
                'bind' => [
                    'code' => '13',
                    'codeTypeId' => $codeTypeClassification->getId()
                ],
                'order' => 'id DESC'
            ]);
        }

        $classifications = [];
        if ($doubleSeasonCode) {
            $classifications = $code->getClassificationCategories([
                Classification::PROPERTY_PARENT_CLASSIFICATION_ID . ' IS NULL AND '.
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' != :codeId:',
                'bind' => [
                    'codeId' => $doubleSeasonCode->getId()
                ],
                'order' => 'id DESC'

            ]);
        }

        foreach ($classifications as $classification) {
            $options[$classification->getCode()] = $classification->getName();
        }

        $subMaterialCodeClassification = $materialFactory->getSubMaterialClassification();
        $materialCodeClassification = $subMaterialCodeClassification ? $subMaterialCodeClassification->getParentClassification() : null;
        $materialTypeCodeClassification = $materialCodeClassification ? $materialCodeClassification->getParentClassification() : null;

        $materialCodeOptions = [];
        if ($materialTypeCodeClassification) {
            foreach ($materialTypeCodeClassification->getChildClassifications() as $childClassification) {
                $materialCodeOptions[$childClassification->getCode()] = $childClassification->getName();
            }
        }

        $subMaterialCodeOptions = [];
        if ($materialCodeClassification) {
            foreach ($materialCodeClassification->getChildClassifications() as $childClassification) {
                $subMaterialCodeOptions[$childClassification->getCode()] = $childClassification->getName();
            }
        }

        $materialTypeCodeIdElement = new Select('materialTypeCodeId', $options);
        $materialTypeCodeIdElement->setLabel('Material type code');
        $materialTypeCodeIdElement->setAttribute('class', $materialTypeCodeIdElement->getAttribute('class') . ' search');
        $materialTypeCodeIdElement->setAttribute('disabled', true);
        $materialTypeCodeIdElement->setDefault($materialTypeCodeClassification ? $materialTypeCodeClassification->getCode() : ($materialCodeClassification ? $materialCodeClassification->getCode() : 0));

        $materialCodeIdElement = new Select('materialCodeId', []);
        $materialCodeIdElement->setLabel('Material code');
        $materialCodeIdElement->setAttribute('class', $materialCodeIdElement->getAttribute('class') . ' search');
        $materialCodeIdElement->setOptions(count($materialCodeOptions) ? $materialCodeOptions : $subMaterialCodeOptions);
        $materialCodeIdElement->setDefault(count($materialCodeOptions) ? $materialCodeClassification->getCode() : ($subMaterialCodeClassification ? $subMaterialCodeClassification->getCode() : 0));
        $materialCodeIdElement->setAttribute('disabled', true);


        $this->get(MaterialFactory::PROPERTY_SUB_MATERIAL_CODE_ID)->setOptions($subMaterialCodeOptions);
        $this->get(MaterialFactory::PROPERTY_SUB_MATERIAL_CODE_ID)->setDefault($subMaterialCodeClassification ? $subMaterialCodeClassification->getCode() : 0);
//        $this->get(MaterialFactory::PROPERTY_SUPPLIER_CODE_ID)->setAttribute('disabled', true);

        if ($materialFactory->getMaterialSample()) {
            $this->get(MaterialFactory::PROPERTY_MATERIAL_SAMPLE_ID)->setAttribute('disabled', true);
        }

        $this->add($materialCodeIdElement, MaterialFactory::PROPERTY_SUPPLIER_CODE_ID);
        $this->add($materialTypeCodeIdElement, MaterialFactory::PROPERTY_SUPPLIER_CODE_ID);

        /** @var Code $materialTypeCode */
        $materialTypeCode = $entityManager->findOne(Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '1',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $doubleSeasonClassifications = $entityManager->find(Classification::class, [
            Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
            Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeCategoryId' => $materialTypeCode->getId(),
                'codeTypeId' => $doubleSeasonCode->getId()
            ],
            'order' => 'name DESC,id DESC'
        ]);

        $seasonOptions = [0 => '-- Izaberite --'];
        foreach ($doubleSeasonClassifications as $classification) {
            $seasonOptions[$classification->getId()] = $classification->getName();
        }
        $this->get(MaterialFactory::PROPERTY_SEASON_CLASSIFICATION_ID)->setOptions($seasonOptions);

        if ($this->has(MaterialFactory::PROPERTY_SUPPLIER_CODE_ID)) {
            $suppliers = $entityManager->find(Supplier::class);

            $options = [0 => $translator->t(' - Izaberite - ')];
            foreach ($suppliers as $supplier) {
                $options[$supplier->getEntityId()] = $supplier->getName();
            }

            $this->get(MaterialFactory::PROPERTY_SUPPLIER_CODE_ID)->setOptions($options);
        }

        $this->get(MaterialFactory::PROPERTY_NOTE)->setAttribute(
            'class',
            $this->get(MaterialFactory::PROPERTY_NOTE)->getAttribute('class') . ' sample-note'
        );

        foreach ($this->getElements() as $element) {
            $element->setAttribute('class', $element->getAttribute('class') . ' form-control' . ($element->getName() === 'deliveryTime' ? ' datepicker' : ''));
        }
    }
}