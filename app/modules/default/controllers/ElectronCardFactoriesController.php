<?php

use Vemid\Controller\CrudController;

/**
 * Class ElectronCardFactoriesController
 *
 * @package Default\Controllers
 */
class ElectronCardFactoriesController extends CrudController
{
    public function getDistinctGramsAction()
    {
        $this->view->disable();
        $json = [];
        $term = $this->request->getQuery('term');

        /** @var ElectronCardFactory[] $electronCardFactories */
        $electronCardFactories = $this->entityManager->find(ElectronCardFactory::class, [
            ElectronCardFactory::PROPERTY_GRAMS . ' LIKE :term:',
            'bind' => [
                'term' => "%$term%"
            ],
        ]);

        foreach ($electronCardFactories as $electronCardFactory) {
            $id = (int)$electronCardFactory->getGrams();
            if (array_key_exists($id, $json)) {
                continue;
            }

            $json[$id]['id'] = $electronCardFactory->getGrams();
            $json[$id]['value'] = $electronCardFactory->getGrams() . ' gr';
            $json[$id]['label'] = $electronCardFactory->getGrams() . ' gr';
        }

        $this->response->setContent(json_encode($json));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return ElectronCardFactory::class;
    }
}
