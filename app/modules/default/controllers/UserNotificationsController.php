<?php

use Vemid\Controller\CrudController;
use Vemid\Entity\EntityInterface;
use \Vemid\Entity\Repository\NotificationTypeRepository;
use Vemid\Messenger\Message;
use \Vemid\Form\Renderer\Json as FormRenderer;
use \Vemid\Date\DateTime;

/**
 * Class UserRoleAssignmentsController
 *
 * @package Default\Controllers
 */
class UserNotificationsController extends CrudController
{

    public function getCreateFormAction()
    {
        $data = parent::getCreateFormAction();
        $params = $this->dispatcher->getParams();

        $userNotifications = [];
        /** @var User $user */
        if ($user = $this->entityManager->findOne(User::class, $params[1])) {
            $subscribedUserNotifications = $user->getUserNotifications([
                UserNotification::PROPERTY_SUBSCRIBED . ' = :subscribed:',
                'bind' => [
                    'subscribed' => true
                ]
            ]);

            foreach ($subscribedUserNotifications as $userNotification) {
                if (!$userNotification->getNotificationType()) {
                    continue;
                }

                $userNotifications[] = (string)$userNotification->getNotificationType()->getId();
            }
        }

        if (isset($data[UserNotification::PROPERTY_NOTIFICATION_TYPE_ID])) {
            $data[\UserNotification::PROPERTY_NOTIFICATION_TYPE_ID]['attributes']['multiple'] = true;
            $data[\UserNotification::PROPERTY_NOTIFICATION_TYPE_ID]['name'] .= '[]';
            $data[\UserNotification::PROPERTY_NOTIFICATION_TYPE_ID]['default'] = $userNotifications;
        }

        return $data;
    }

    public function createAction()
    {
        $params = $this->dispatcher->getParams();
        /** @var User $user */
        if ($user = $this->entityManager->findOne(User::class, $params[1])) {
            /** @var array $notificationTypes */
            $notificationTypes = $this->request->getPost(UserNotification::PROPERTY_NOTIFICATION_TYPE_ID);
            $userNotificationManager = $this->getDI()->getUserNotification();

            foreach ($user->getUserNotifications() as $userNotification) {
                if (!in_array($userNotification->getId(), $notificationTypes, false)) {
                    $userNotificationManager->unsubscribe($userNotification);
                }
            }

            foreach ($notificationTypes as $notificationTypeId) {
                /** @var NotificationType $notificationType */
                if (!$notificationType = $this->entityManager->findOne(NotificationType::class, $notificationTypeId)) {
                    continue;
                }

                /** @var UserNotification $unSubscribedUserNotification */
                $unSubscribedUserNotification = $notificationType->getUserNotifications([
                    UserNotification::PROPERTY_USER_ID . ' = :userId: AND ' .
                    UserNotification::PROPERTY_SUBSCRIBED . ' = :subscribed:',
                    'bind' => [
                        'userId' => $user->getId(),
                        'subscribed' => false
                    ]
                ])->getLast();

                if ($unSubscribedUserNotification) {
                    $userNotificationManager->subscribe($unSubscribedUserNotification);
                    continue;
                }

                /** @var UserNotification $userNotification */
                $userNotification = $notificationType->getUserNotifications([
                    UserNotification::PROPERTY_USER_ID . ' = :userId: AND ' .
                    UserNotification::PROPERTY_SUBSCRIBED . ' = :subscribed:',
                    'bind' => [
                        'userId' => $user->getId(),
                        'subscribed' => true
                    ]
                ])->getLast();

                if ($userNotification) {
                    continue;
                }

                $userNotification = new UserNotification();
                $userNotification->setUser($user);
                $userNotification->setNotificationType($notificationType);
                $userNotification->setSubscribed(true);

                if (!$this->entityManager->save($userNotification)) {
                    $this->addFlashMessagesFromEntity($userNotification);
                }
            }

            $this->addFlashMessage(
                $this->_translate('Data was saved successfully.'),
                null,
                Message::SUCCESS
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function deleteAction($id)
    {
    }

    public function unsubscribeAction($id)
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var NotificationType $notificationType */
        if (!$notificationType = $this->entityManager->findOne(NotificationType::class, $id)) {
            return $this->returnNotFound();
        }

        $userNotificationService = $this->getDI()->getUserNotification();
        $userNotifications = $notificationType->getUserNotifications([
            UserNotification::PROPERTY_USER_ID . ' = :userId:',
            'bind' => [
                'userId' => $this->currentUser->getId()
            ]
        ]);

        foreach ($userNotifications as $userNotification) {
            $userNotificationService->unsubscribe($userNotification);
        }

        $this->addFlashMessage(
            $this->_translate('Data was saved successfully.'),
            null,
            Message::SUCCESS
        );
    }


    public function deactivateMessageAction($id)
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var UserNotificationMessage $userNotificationMessage */
        if (!$userNotificationMessage = $this->entityManager->findOne(UserNotificationMessage::class, $id)) {
            return $this->returnNotFound();
        }

        $userNotificationMessage->setStatus(UserNotificationMessage::STATUS_DEACTIVATED);
        $userNotificationMessage->setDeactivatedDate(new DateTime());

        if (!$this->entityManager->save($userNotificationMessage)) {
            $this->addFlashMessagesFromEntity($userNotificationMessage);
        }

        $this->addFlashMessage(
            $this->_translate('Data was saved successfully.'),
            null,
            Message::SUCCESS
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityName()
    {
        return \UserNotification::class;
    }

    /**
     * {@inheritdoc}err
     */
    public function getForm(EntityInterface $entity = null, array $exclude = ['userId', 'subscribed'])
    {
        return parent::getForm($entity, $exclude);
    }

    public function listAction()
    {
        /** @var NotificationTypeRepository $notificationTypeRepository */
        $notificationTypeRepository = $this->entityManager->getRepository(NotificationType::class);
        $notificationTypes = $notificationTypeRepository->getNotificationTypesByUser($this->currentUser);

        $notifications = [];
        foreach ($notificationTypes as $notificationType) {
            /** @var UserNotification $userNotification */
            $userNotification = $notificationType->getUserNotifications([
                UserNotification::PROPERTY_USER_ID . ' = :userId: AND ' .
                UserNotification::PROPERTY_SUBSCRIBED . ' = :subscribed:',
                'bind' => [
                    'userId' => $this->currentUser->getId(),
                    'subscribed' => true
                ]
            ])->getLast();

            if ($userNotification) {
                $userNotificationMessages = $userNotification->getUserNotificationMessages([
                    UserNotificationMessage::PROPERTY_STATUS . ' != :status:',
                    'bind' => [
                        'status' => UserNotificationMessage::STATUS_DEACTIVATED
                    ]
                ]);

                foreach ($userNotificationMessages as $userNotificationMessage) {

                    $statuses = 'Odložene';
                    if ($userNotificationMessage->isOverDue()) {
                        $statuses = 'Zakasnele';
                    } elseif ($userNotificationMessage->isGonnaBeOverDue()) {
                        $statuses = 'Ističu';
                    } elseif ($userNotificationMessage->getStatus() === UserNotificationMessage::STATUS_ACTIVE) {
                        $statuses = 'Aktivne';
                    } elseif ($userNotificationMessage->isFinished()) {
                        $statuses = 'Završene';
                    }
                    $notifications[$notificationType->getName()][$statuses][$userNotificationMessage->getId()] = $userNotificationMessage;
                    $notifications[$notificationType->getName()]['notification'] = $notificationType;
                }
            }
        }

        $this->view->setVar('notificationTypes', $notifications);
    }

    public function getPostponeFormAction($id)
    {
        /** @var UserNotificationMessage $userNotificationMessage */
        if (!$userNotificationMessage = $this->entityManager->findOne(UserNotificationMessage::class, $id)) {
            return $this->returnNotFound();
        }

        $today = new DateTime();
        $dueDate = $userNotificationMessage->getDueDate();

        if ($dueDate && ($dueDate <= $today || $today->diff($dueDate)->days <= 2)) {
            $this->addFlashMessage(
                $this->_translate('Nije dozovoljeno odlaganje notifikacije zbog roka koji je pri isteku'),
                null,
                Message::DANGER
            );

            return;
        }

        $form = new UserNotificationMessagePostponeForm(null, ['userNotificationMessage' => $userNotificationMessage]);

        return (new FormRenderer())->render($form);
    }

    public function postponeAction($id)
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var UserNotificationMessage $userNotificationMessage */
        if (!$userNotificationMessage = $this->entityManager->findOne(UserNotificationMessage::class, $id)) {
            return $this->returnNotFound();
        }

        $status = $this->request->getPost('status');

        if ($status) {
            $userNotificationMessage->setStatus($status);
            $userNotificationMessage->setPostponedDate(new DateTime());
            $this->saveEntity($userNotificationMessage);

            return;
        }

        $this->addFlashMessage(
            $this->_translate('Izaberite period za koji želite da odložite poruku!'),
            null,
            Message::DANGER
        );

        return;
    }
}
