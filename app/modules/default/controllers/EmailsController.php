<?php

use Vemid\Controller\BaseController;
use \Vemid\Entity\Type;

/**
 * Class EmailsController
 *
 * @package Default\Controllers
 */
class EmailsController extends BaseController
{
    public function getCreateFormAction()
    {
        $this->tag::appendTitle($this->translator->t("Dodavanje email notifikacija"));

        $modelFiles = scandir(APP_PATH . 'app/models', null);
        $models = [];
        $controllers = [];

        foreach ($modelFiles as $key => $value) {
            $modelName = preg_replace('/[^[:alpha:]]/', null, str_replace('.php', '', $value));

            if ($modelName && $modelName !== Phinxlog::class && $modelName !== AuditLog::class) {
                $models[Type::getEntityType($modelName)] = $modelName;
            }
        }

        $controllerFiles = scandir(APP_PATH . 'app/modules/default/controllers', null);
        foreach ($controllerFiles as $key => $value) {
            $controllerName = preg_replace('/[^[:alpha:]]/', null, str_replace('Controller.php', '', $value));
            $controllers[$controllerName] = $controllerName;
        }

        $this->view->pick('emails/get-create-form');
        $this->view->setVar('models', $models);
        $this->view->setVar('controllers', $controllers);
        $this->view->setVar('users', $this->entityManager->find(User::class));
    }

    public function addNotificationTemplateAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $emailNotificationTemplate = new EmailNotificationTemplate();
        $postData = $this->request->getPost();
        $postData[EmailNotificationTemplate::PROPERTY_ENTITY_OBJECT_NAME] = Type::getObjectName($postData[EmailNotificationTemplate::PROPERTY_ENTITY_OBJECT_NAME]);

        $form = $emailNotificationTemplate->getForm();
        if (!$form->isValid($postData)) {
            $this->addFlashMessagesFromForm($form);
            $this->response->redirect('emails/get-create-form');
            return;
        }

        if (!$this->entityManager->save($emailNotificationTemplate)) {
            $this->addFlashMessagesFromEntity($emailNotificationTemplate);
            $this->response->redirect('emails/get-create-form');
            return;
        }

        if (!empty($postData['users'])) {
            foreach ($postData['users'] as $userId) {
                $emailTemplateReceiver = new EmailTemplateReceiver();
                $emailTemplateReceiver->setTemplateObjectId($emailNotificationTemplate->getEntityId());
                $emailTemplateReceiver->setTemplateObjectTypeId($emailNotificationTemplate->getEntityTypeId());
                $emailTemplateReceiver->setUserId($userId);

                if (!$this->entityManager->save($emailTemplateReceiver)) {
                    $this->addFlashMessagesFromEntity($emailTemplateReceiver);
                    return;
                }
            }
        }

        $this->addFlashMessage(
            $this->translator->t('Email template added'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
        $this->response->redirect('emails/list');
    }

    public function readPropertiesAction($entityTypeId)
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $properties = [
            'currentUser.firstName' => 'currentUser.firstName',
            'currentUser.lastName' => 'currentUser.lastName'
        ];

        $childProperties = [];

        if ($entity = Type::getObjectName($entityTypeId)) {
            $annotationClass = $this->getDI()->getAnnotations();
            $reflector = $annotationClass->get($entity);
            $notAllowedProperties = [];

            if ($annotations = $reflector->getClassAnnotations()) {
                foreach ($annotations as $annotation) {
                    if ($annotation->getName() !== 'BelongsTo') {
                        continue;
                    }

                    $notAllowedProperties[] = $annotation->getArgument(0);
                    $childModel = $annotation->getArgument(1);
                    $reflectorChild = $annotationClass->get($childModel);

                    if ($childModelAnnotations = $reflectorChild->getClassAnnotations()) {
                        $childModelNotAllowedProperties = [];

                        foreach ($childModelAnnotations as $childModelAnnotation) {
                            if ($childModelAnnotation->getName() !== 'BelongsTo') {
                                continue;
                            }

                            $childModelNotAllowedProperties[] = $childModelAnnotation->getArgument(0);
                        }

                        foreach ($annotationClass->getProperties($childModel) as $childProperty => $childReflectionClass) {
                            if ($childProperty === 'id' || in_array($childProperty, $childModelNotAllowedProperties, true)) {
                                continue;
                            }

                            $str = lcfirst($entity) . '.' . lcfirst($childModel) . '.' . $childProperty;
                            $childProperties[$str] = $str;
                        }
                    }
                }
            }

            foreach ($annotationClass->getProperties($entity) as $modelProperty => $refClass) {
//                if ($modelProperty === 'id' || in_array($modelProperty, $notAllowedProperties, false)) {
//                    continue;
//                }

                $str = lcfirst($entity) . '.' . lcfirst($modelProperty);
                $properties[$str] = $str;
            }
        }

        $this->view->disable();
        $this->response->setContent(json_encode($properties + $childProperties));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function readActionsAction($controllerName)
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $reflector = new ReflectionClass($controllerName . 'Controller');
        $data = $reflector->getMethods(ReflectionMethod::IS_PUBLIC);

        $actions = [];
        $notAllowedMethods = ['getForm', 'getEntity', 'getEntityName', 'getCreateFormAction', 'getUpdateFormAction'];
        foreach ($data as $classReflection) {
            if (false === strpos($classReflection->getName(), 'Action') || in_array($classReflection->getName(), $notAllowedMethods, false)) {
                continue;
            }

            $name = str_replace('Action', '', $classReflection->getName());
            $actions[$name] = $name;
        }

        $this->view->disable();
        $this->response->setContent(json_encode($actions));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function listAction()
    {
        $this->tag::appendTitle($this->translator->t("Pregled email notifikacija "));
        $this->view->pick('emails/list');
        $this->view->setVar('emails', $this->entityManager->find(EmailNotificationTemplate::class));
    }

    /**
     * {@inheritdoc}
     */
    public function deleteAction($id)
    {
        /** @var EmailNotificationTemplate $emailNotificationTemplate */
        if (!$emailNotificationTemplate = $this->entityManager->findOne(EmailNotificationTemplate::class, $id)) {
            return $this->returnNotFound();
        }

        if (!$emailNotificationTemplate->isDeletable()) {
            $this->addFlashMessage(
                $this->_translate('It is not allowed to delete this record.'),
                null,
                \Vemid\Messenger\Message::WARNING
            );
        } else {
            /** @var EmailTemplateReceiver $emailTemplateReceivers */
            $emailTemplateReceivers = $this->entityManager->find(EmailTemplateReceiver::class,[
                EmailTemplateReceiver::PROPERTY_TEMPLATE_OBJECT_ID . '= :emailTemplateObjectId: AND ' .
                EmailTemplateReceiver::PROPERTY_TEMPLATE_OBJECT_TYPE_ID . ' = :emailTemplateObjectTypeId:',
                'bind' => [
                    'emailTemplateObjectId' => $emailNotificationTemplate->getId(),
                    'emailTemplateObjectTypeId' => Type::EMAIL_NOTIFICATION_TEMPLATE
                ]
            ]);

            foreach ($emailTemplateReceivers as $emailTemplateReceiver){
                $this->entityManager->delete($emailTemplateReceiver);
            }
            $this->entityManager->delete($emailNotificationTemplate);

            $this->addFlashMessage(
                $this->_translate('Email Teplate obirsan!'),
                null,
                \Vemid\Messenger\Message::SUCCESS
            );
        }
    }

    public function overviewAction($id)
    {
        return parent::overviewAction($id);
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return EmailNotificationTemplate::class;
    }
}
