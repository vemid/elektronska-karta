<?php

use Vemid\Controller\CrudController;

class ProductPricesController extends CrudController
{

    /**
     * @return string
     */
    public function getEntityName()
    {
        return ProductPrice::class;
    }
}