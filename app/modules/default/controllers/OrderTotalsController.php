<?php

use Vemid\Controller\CrudController;
use \Vemid\Messenger\Message;

/**
 * Class OrderTotalsController
 *
 * @package Default\Controllers
 */
class OrderTotalsController extends CrudController
{

    public function updateTotalAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $this->view->disable();

        $postData = $this->request->getPost();

        /** @var $orderTotal OrderTotal */
        if (!$orderTotal = $this->entityManager->findOne(OrderTotal::class, $postData['column'])) {
            return $this->returnNotFound();
        }

        $orderTotal->setFinalQty($postData['value']);

        if (!$this->entityManager->save($orderTotal)) {
            $this->addFlashMessagesFromEntity($orderTotal);
        } else {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('Sačuvana količina!'),
                null,
                Message::SUCCESS
            );
        }
    }

    public function getListDataAction($id)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');
        $oldCollectionId = $this->request->getQuery('oldCollectionId', 'string', '');
        $genderClassificationId = $this->request->getQuery('genderClassificationId', 'int', 0);
        $groupModelId = $this->request->getQuery('groupModelId', 'int', 0);


        $genders = [];
        /** @var Classification $classification */
        if ($genderClassificationId && $classification = $this->entityManager->findOne(Classification::class, $genderClassificationId)) {
            /** @var Classification[] $genderClassifications */
            $genderClassifications = $this->entityManager->find(Classification::class, [
                'name = :name:',
                'bind' => [
                    'name' => $classification->getName()
                ]
            ]);

            foreach ($genderClassifications as $genderClassification) {
                $genders[] = $genderClassification->getId();
            }
        }

        $groupModels = [];
        /** @var Classification $classification */
        if ($groupModelId && $classification = $this->entityManager->findOne(Classification::class, $groupModelId)) {

            /** @var ElectronCard[] $electronCards */
            $query = $this->modelsManager->createBuilder()
                ->addFrom(Classification::class, 'c')
                ->leftJoin(Classification::class, 'cp.id = c.parentClassificationId', 'cp')
                ->where('c.name = :name:', [
                    'name' => $classification->getName()
                ])
                ->andWhere('c.name != cp.name');

            /** @var Classification[] $groupModelClassifications */
            $groupModelClassifications = $query->getQuery()->execute();

            foreach ($groupModelClassifications as $groupModelClassification) {
                $groupModels[] = $groupModelClassification->getId();
            }
        }

        $orderTotalsQuery = $this->modelsManager->createBuilder()
            ->addFrom(OrderTotal::class, 'ot')
            ->leftJoin(ProductSize::class, 'ps.id = ot.productSizeId', 'ps');

        if ($this->currentUser->isClient()) {
            $orderTotalsQuery->leftJoin(OrderItem::class, 'oi.productSizeId = ot.productSizeId', 'oi');
            $orderTotalsQuery->leftJoin(Client::class, 'c.id = oi.entityId', 'c');
        }

        $orderTotalsQuery->leftJoin(ProductClassification::class, 'pc.productId = ps.productId', 'pc')
            ->leftJoin(Product::class, 'p.id = ps.productId', 'p')
            ->where('ot.orderId = :orderId:', [
                'orderId' => $order->getId()
            ]);

        if ($this->currentUser->isClient()) {
            $orderTotalsQuery->andWhere('oi.entityTypeId = :entityTypeId:', [
                'entityTypeId' => \Vemid\Entity\Type::CLIENT
            ])
                ->andWhere('c.userId = :userId:', [
                    'userId' => $this->currentUser->getId()
                ]);
        }

        $orderTotalsQuery->groupBy('ot.id')
            ->orderBy('pc.id');


        $queryCodes = [];
        $filter = 0;
        if ($oldCollectionId) {
            /** @var Classification $oldCollection */
            $oldCollection = $this->entityManager->findOne(Classification::class, [
                'code = :code:',
                'bind' => [
                    'code' => $oldCollectionId
                ]
            ]);

            if ($oldCollection) {
                $filter++;
                $queryCodes[] = $oldCollection->getId();
            }
        }

        if (count($genders)) {
            $filter++;
            $queryCodes = array_merge($queryCodes, $genders);
        }

        if (count($groupModels)) {
            $filter++;
            $queryCodes = array_merge($queryCodes, $groupModels);
        }

        if (count($queryCodes)) {
            $orderTotalsQuery->andWhere('pc.entityId IN ({codeQuery:array})', [
                'codeQuery' => $queryCodes
            ]);

            $orderTotalsQuery->having('COUNT(pc.entityId) >= ' . $filter);
        }

        $cloned = clone $orderTotalsQuery;
        $allOderTotals = $cloned->getQuery()->execute();

        $orderTotalsQuery->limit($limit, $offset);

        /** @var OrderTotal[] $orderTotals */
        $orderTotals = $orderTotalsQuery->getQuery()->execute();

        $ids = [];
        foreach ($orderTotals as $orderTotal) {
            $ids[] = $orderTotal->getId();
        }

        $orderTotalsGrouped = [];
        if (count($ids)) {
            $query = $this->modelsManager->createBuilder()
                ->columns([
                    'SUM(ot.finalQty) as finalQty',
                    'p.code'
                ])
                ->addFrom(OrderTotal::class, 'ot')
                ->leftJoin(ProductSize::class, 'ps.id = ot.productSizeId', 'ps');

            if ($this->currentUser->isClient()) {
                $query->leftJoin(OrderItem::class, 'oi.productSizeId = ot.productSizeId', 'oi')
                    ->leftJoin(Client::class, 'c.id = oi.entityId', 'c');
            }

            $query->leftJoin(Product::class, 'p.id = ps.productId', 'p')
                ->where('ot.orderId = :orderId:', [
                    'orderId' => $order->getId()
                ])
                ->andWhere('ot.id IN ({ids:array})', [
                    'ids' => $ids
                ]);

            if ($this->currentUser->isClient()) {
                $orderTotalsQuery->andWhere('oi.entityTypeId = :entityTypeId:', [
                        'entityTypeId' => \Vemid\Entity\Type::CLIENT
                    ])
                    ->andWhere('c.userId = :userId:', [
                        'userId' => $this->currentUser->getId()
                    ]);
            }

            $query->groupBy('p.id')
                ->orderBy('p.code ASC, ps.codeId ASC');
            $orderTotalsGrouped = $query->getQuery()->execute()->toArray();
        }

        $data = [];
        foreach ($orderTotals as $orderTotal) {
            if (!$productSize = $orderTotal->getProductSize()) {
                continue;
            }

            $code = $productSize->getProduct()->getCode();
            $result = array_column(array_filter($orderTotalsGrouped, function ($item) use ($code) {
                if (stripos($item['code'], $code) !== false) {
                    return true;
                }

                return false;
            }), 'finalQty');

            $data[] = [
                $orderTotal->getId(),
                $productSize->getProduct()->getName(),
                $productSize->getProduct()->getCode(),
                $productSize->getCode()->getCode(),
                $orderTotal->getQuantity(),
                $orderTotal->getFinalQty(),
                $code . '_' . array_shift($result)
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $allOderTotals->count(),
            'recordsFiltered' => $allOderTotals->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getTotalProductAction() {

        $this->view->disable();
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $productId = $this->request->getPost('productId');

        /** @var Product $product */
        $product = $this->entityManager->findOne(Product::class,$productId);

        $query = $this->modelsManager->createBuilder()
            ->columns([
                'SUM(oi.finalQty) as komada',
            ])
            ->addFrom(\OrderTotal::class,'oi')
            ->leftJoin(\ProductSize::class, 'ps.id = oi.productSizeId', 'ps')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->where('p.id = :productId:', [
                'productId' => $product->getId()
            ]);

        /** @var OrderTotal|null $orderTotal */
        $orderTotal = $query->getQuery()->execute()->getLast();

        return $orderTotal ? $orderTotal->toArray() : [];
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return OrderTotal::class;
    }
}
