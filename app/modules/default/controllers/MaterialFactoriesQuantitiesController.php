<?php


use  \Vemid\Form\Renderer\Json as Renderer;
use \Vemid\Controller\BaseController;
use \Vemid\Entity\EntityInterface;

/***
 * Class MaterialFactoriesQuantitiesController
 */

class MaterialFactoriesQuantitiesController extends BaseController
{
    public function getCreateFormAction($id)
    {
        $materialFactory = $this->entityManager->findOne(MaterialFactory::class, $id);
        if (!$materialFactory) {
            return $this->returnNotFound();
        }

        $form = new MaterialFactoryQuantityCreateForm(new MaterialFactoryQuantity(), ['materialFactory' => $materialFactory]);

        return (new Renderer())->render($form);
    }

    public function createAction($id)
    {
        $materialFactory = $this->entityManager->findOne(MaterialFactory::class, $id);
        if (!$materialFactory) {
            return $this->returnNotFound();
        }

        $postData = $this->request->getPost();

        $type =  null;
        if (!empty($postData['typeIn'])) {
            $type =  MaterialFactoryQuantity::INCOME;
        } else if (!empty($postData['typeOut'])) {
            $type =  MaterialFactoryQuantity::OUTCOME;
        }

        $postData['type'] = $type;

        $materialFactoryQuantity = new MaterialFactoryQuantity();
        $materialFactoryQuantity->setType($type);
        $form = $this->getForm($materialFactoryQuantity);

        if ($form->isValid($postData)) {
            $this->saveEntity($materialFactoryQuantity);
        }
    }

    /**
     * @return string
     */
    public function getEntityName(): string
    {
        return MaterialFactoryQuantity::class;
    }

    public function getForm(EntityInterface $entity = null, array $exclude = [])
    {
        if ($entity === null) {
            $entityName = '\\' . ltrim($this->getEntityName(), '\\');
            $entity = new $entityName;
        }

        return $entity->getForm($exclude);
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdateFormAction($id)
    {
        /** @var MaterialFactoryQuantity $entity */
        if (!$entity = $this->entityManager->findOne(MaterialFactoryQuantity::class, $id)) {
            return $this->returnNotFound();
        }

        $renderer = new Renderer();
        $form = new MaterialFactoryQuantityEditForm(new MaterialFactoryQuantity(),['materialFactoryQuantity' => $entity]);

        return (new Renderer())->render($form);
    }

    /**
     * {@inheritdoc}
     */
    public function updateAction($id)
    {
        if ($this->request->isPost()) {
            if (!$entity = $this->entityManager->findOne(MaterialFactoryQuantity::class, $id)) {
                return $this->returnNotFound();
            }

            $form = $this->getForm($entity);
            if ($form->isValid($this->request->getPost())) {
                $form->synchronizeEntityData();
                $this->saveEntity($entity);
            } else {
                $this->addFlashMessagesFromForm($form);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function deleteAction($id)
    {
        if (!$entity = $this->entityManager->findOne(MaterialFactoryQuantity::class, $id)) {
            return $this->returnNotFound();
        }

        if (!$entity->isDeletable()) {
            $this->addFlashMessage(
                $this->_translate('It is not allowed to delete this record.'),
                null,
                MessengerMessage::WARNING
            );
        } else {
            $this->deleteEntity($entity);
        }
    }

}