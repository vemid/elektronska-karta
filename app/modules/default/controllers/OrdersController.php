<?php

use Phalcon\Mvc\Model\Resultset\Simple;
use Vemid\Controller\CrudController;
use \Vemid\Service\Product\Hydrator;
use \Vemid\Entity\Type;
use \Vemid\Entity\Repository\ProductSizeRepository;
use \Vemid\Entity\Repository\ProductRepository;
use \Vemid\Entity\Repository\ClientOrderItemRepository;
use \Vemid\Entity\Repository\OrderItemRepository;
use \Vemid\Entity\Repository\ClassificationRepository;
use \Vemid\Entity\Repository\ProductClassificationRepository;
use \Vemid\Form\Renderer\Json as Renderer;
use \Vemid\Date\DateTime;
use Vemid\Messenger\Message as MessengerMessage;
use \Vemid\Services\Assets\DefaultAssets;
use Vemid\Task\Orders\AddOrderTotals;
use \Vemid\Entity\Repository\ProductionOrderRepository;


/**
 * Class OrdersController
 *
 * @package Default\Controllers
 */
class OrdersController extends CrudController
{
    public function listAction()
    {
        $orders = $this->entityManager->find(Order::class, [
            'order' => 'id DESC'
        ]);

        if( $this->currentUser->isClient()) {
            $user = $this->currentUser;
            /** @var Client $client */
            $client = $this->entityManager->findOne(Client::class, [
                Client::PROPERTY_USER_ID . ' = :userId:',
                'bind' => [
                    'userId' => $this->currentUser->getId()
                ]
            ]);

            $orderId=[];

            $ordersIds= $this->modelsManager->createBuilder()
                    ->columns('orderId')
                    ->addFrom(OrderItem::class, 'oi')
                    ->where(OrderItem::PROPERTY_ENTITY_TYPE_ID .'= :type: ',[
                        'type' => Vemid\Entity\Type::CLIENT
                    ])
                    ->andWhere(OrderItem::PROPERTY_ENTITY_ID . '=:clientId:', [
                    'clientId' => $client->getId()
                    ])
                    ->groupBy('oi.orderId')
            ->getQuery()->execute();

            $activeOrders = $this->entityManager->find(Order::class,[
                Order::PROPERTY_ACTIVE . ' = :active:',
                'bind' => [
                    'active' => true
                ]
            ]);

            foreach ($activeOrders as $activeOrder) {
                $orderId[] = $activeOrder->getId();
            }

            foreach ($ordersIds->toArray() as $id) {
                $orderId[] = $id['orderId'];
            }


            $orders = $this->entityManager->find(Order::class, [
                Order::PROPERTY_ID . ' IN ({orders:array})',
                'bind' => [
                    'orders' => $orderId
                ],
                'order' => 'id DESC'
            ]);
        }


        $this->view->setVar('orders', $orders);
        $this->view->setVar('superAdmin', $this->currentUser->isSuperAdmin());
        $this->view->setVar('date', new DateTime());
    }

    public function listClientAction()
    {

        $orders = $this->entityManager->find(Order::class, [
            'order' => 'id DESC'
        ]);

        $this->view->setVar('orders', $orders);
        $this->view->setVar('superAdmin', $this->currentUser->isSuperAdmin());
        $this->view->setVar('date', new DateTime());
    }

    public function overviewAction($id)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        /** @var Client $client */
        $client = $this->entityManager->findOne(Client::class, [
            Client::PROPERTY_USER_ID . ' = :userId:',
            'bind' => [
                'userId' => $this->currentUser->getId()
            ]
        ]);

        $classifications = null;
        if ($codeType) {
            /** @var ClassificationRepository $classificationRepository */
            $classificationRepository = $this->entityManager->getRepository(Classification::class);
            $classifications = $classificationRepository->getClassificationsIncludedInOrders($order, $codeType, !$this->currentUser->isSuperAdmin() ? $client : null);
        }

        /** @var ClientOrderItemRepository $orderItemRepository */
        $orderItemRepository = $this->entityManager->getRepository(ClientOrderItem::class);

        /** @var Client[] $clients */
        $clients = $this->entityManager->find(Client::class);
        $options = [];
        foreach ($clients as $client) {
            if (!$orderItemRepository->getOrderItems($order, $client)->count()) {
                continue;
            }

            $options[$client->getCode()] = $client->getName();
        }

        $this->view->setVar('form', new OrderTotalFilterForm(null, ['order' => $order]));
        $this->view->setVar('classifications', $classifications);
        $this->view->setVar('options', $options);
        $this->view->setVar('client', $client);
        $this->view->setVar('order', $order);
        $this->view->setVar('superAdmin', $this->currentUser->isSuperAdmin());
    }

    public function getCreateFormAction()
    {
        $form = new OrderForm(new Order());

        return (new Renderer())->render($form);
    }

    public function createAction()
    {
        if ($this->request->isPost()) {
            $form = $this->getForm();
            $postData = $this->request->getPost();
            $postData[Order::PROPERTY_USER_ID] = $this->currentUser->getId();
            $postData[Order::PROPERTY_ACTIVE] = 1;
            $startDate = new DateTime($postData[Order::PROPERTY_START_DATE]);
            $endDate = new DateTime($postData[Order::PROPERTY_END_DATE]);

            if ($endDate < $startDate) {
                $this->addFlashMessage(
                    $this->getDI()->getTranslator()->t(sprintf('Početni datum mora biti veći od krajnjeg!')),
                    Order::PROPERTY_START_DATE,
                    MessengerMessage::DANGER
                );
            }

            /** @var array $oldCollectionIds */
            $oldCollectionIds = $postData['oldCollection'];

            if ($form->isValid($postData)) {
                $form->synchronizeEntityData();
                /** @var Order $order */
                $order = $form->getEntity();
                $order->setSeasonAttributeId($postData['attributeSeason'][0]);
                $order->setYearAttributeId($postData['attributeYear'][0]);
                if (!$this->entityManager->save($order)) {
                    $this->addFlashMessagesFromEntity($order);
                    return;
                }

                foreach ($oldCollectionIds as $oldCollectionId) {
                    $orderClassification = new OrderClassification();
                    $orderClassification->setOrder($order);
                    $orderClassification->setClassificationId($oldCollectionId);

                    if (!$this->entityManager->save($orderClassification)) {
                        $this->addFlashMessagesFromEntity($orderClassification);
                        return;
                    }
                }

                $this->addFlashMessage(
                    $this->getDI()->getTranslator()->t(sprintf('Order je kreiran!')),
                    null,
                    MessengerMessage::SUCCESS
                );
            } else {
                $this->addFlashMessagesFromForm($form);
            }
        }
    }

    public function getUpdateFormAction($id)
    {
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        $form = new OrderEditForm($order);

        return (new Renderer())->render($form);
    }

    public function updateAction($id)
    {
        if ($this->request->isPost()) {

            /** @var $order Order */
            if (!$order = $this->getEntity($id)) {
                return $this->returnNotFound();
            }

            $postData = $this->request->getPost();
            /** @var array $oldCollectionIds */
            $oldCollectionIds = $postData['oldCollection'];

            $form = $this->getForm($order);
            if ($form->isValid($postData)) {
                $form->synchronizeEntityData();
                $order->setSeasonAttributeId($postData['attributeSeason'][0]);
                $order->setYearAttributeId($postData['attributeYear'][0]);
                if (!$this->entityManager->save($order)) {
                    $this->addFlashMessagesFromEntity($order);
                    return;
                }

                $order->getOrderClassifications()->delete();

                foreach ($oldCollectionIds as $oldCollectionId) {
                    /** @var Classification $classification */
                    $classification = $this->entityManager->findOne(Classification::class, [
                        Classification::PROPERTY_CODE . ' = :code:',
                        'bind' => [
                            'code' => $oldCollectionId
                        ]
                    ]);

                    if (!$classification) {
                        continue;
                    }

                    $orderClassification = new OrderClassification();
                    $orderClassification->setOrder($order);
                    $orderClassification->setClassification($classification);

                    if (!$this->entityManager->save($orderClassification)) {
                        $this->addFlashMessagesFromEntity($orderClassification);
                        return;
                    }
                }

                $this->addFlashMessage(
                    $this->getDI()->getTranslator()->t(sprintf('Order je izmenjen!')),
                    null,
                    MessengerMessage::SUCCESS
                );

            } else {
                $this->addFlashMessagesFromForm($form);
            }
        }
    }

    public function listDetailsAction($id, $classificationCode, $clientCode)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        $classification = $this->entityManager->findOne(Classification::class, [
            Classification::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $classificationCode
            ]
        ]);

        $client = $this->entityManager->findOne(Client::class, [
            Client::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $clientCode
            ]
        ]);

        if (!$client) {

            /** @var CodeType $codeType */
            $codeType = $this->entityManager->findOne(CodeType::class, [
                'code = :code:',
                'bind' => [
                    'code' => CodeType::SHOPS
                ]
            ]);

            if ($codeType) {
                $client = $this->entityManager->findOne(Code::class, [
                    Code::PROPERTY_CODE . ' = :code: AND ' .
                    Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                    'bind' => [
                        'code' => $clientCode,
                        'codeTypeId' => $codeType->getId()
                    ]
                ]);
            }

            if (!$classification || !$client) {
                return $this->returnNotFound();
            }
        }

        $this->view->setVar('client', $client);
        $this->view->setVar('classification', $classification);
        $this->view->setVar('order', $order);
        $this->view->pick('orders/list-details');
    }

    public function listProductsAction($id)
    {
        $this->tag::appendTitle($this->translator->t("Poručivanje Robe"));
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            $this->returnNotFound();
        }


        $orderFilterForm = new OrderFilterForm($order);

        $client = $this->entityManager->findOne(Client::class, [
            Client::PROPERTY_USER_ID . ' = :userId:',
            'bind' => [
                'userId' => $this->currentUser->getId()
            ]
        ]);

        $queryString = $this->request->getQuery();
        $selected = [];
        foreach ($queryString as $category) {
            if (!is_array($category)) {
                continue;
            }

            $selected = array_merge($category, $selected);
        }

        /** @var \Vemid\Entity\Repository\ProductTagRepository $productTagRepository */
        $productTagRepository = $this->entityManager->getRepository(ProductTag::class);

        $productTags = $productTagRepository->getOrderTags($order,$selected);
        if (count($productTags) === 0 && count($selected) > 0) {
            $newSelected = $selected;
            $uri = parse_url($this->request->getURI());
            $array = explode('=', $uri['query']);
            $lastQueryString = end($array);
            if (($key = array_search($lastQueryString, $newSelected)) !== false) {
                unset($newSelected[$key]);
            }

            $productTags = $productTagRepository->getOrderTags($order,$newSelected);
        }

        $tags = [];
        foreach ($productTags as $productTag) {
            $tags[$productTag->getCategory()][$productTag->getTag()] = $productTag->getTag();
        }

        $this->view->setVar('client', $client);
        $this->view->setVar('form', $orderFilterForm);
        $this->view->setVar('order', $order);
        $this->view->setVar('tags', $tags);
        $this->view->setVar('selected', $selected);

        $this->view->pick('orders/list-products');
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityName()
    {
        return \Order::class;
    }

    public function getListDataAction($id)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $classificationCode = $this->request->getQuery('classificationCode', 'string', 0);
        $clientCode = $this->request->getQuery('clientCode', 'string', 0);

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        /** @var OrderItemRepository $ordersRepository */
        $ordersRepository = $this->entityManager->getRepository(OrderItem::class);
        $allOrders = $ordersRepository->getOrdersByClientGroupedByCollection($order, $codeType, $classificationCode, $clientCode);

        $orders = $ordersRepository->getOrdersByClientGroupedByCollection($order, $codeType, $classificationCode, $clientCode, $limit, $offset);
        $data = [];
        foreach ($orders as $orderData) {
            $data[] = [
                $orderData['Collection'],
                $orderData['Client'],
                $orderData['totalQtyA'],
                $orderData['totalQtyB'],
                number_format($orderData['totalA'],1),
                number_format($orderData['totalB'],1),
                $orderData['totalQtyA']+$orderData['totalQtyB'],
                number_format($orderData['totalA']+$orderData['totalB'],1),
                sprintf('/orders/list-details/%s/%s/%s', $order->getId(), $orderData['CollectionCode'], $orderData['ClientCode'])
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $allOrders->count(),
            'recordsFiltered' => $allOrders->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getListDetailsDataAction($id)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $classificationCode = $this->request->getQuery('classificationCode', 'string', 0);
        $clientCode = $this->request->getQuery('clientCode', 'string', 0);

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        /** @var ProductSizeRepository $productSizeRepository */
        $productSizeRepository = $this->entityManager->getRepository(ProductSize::class);
        $allRecords = $productSizeRepository->getGroupedOrderedProductSizeByClientCodeAndCollectionCode($order, $codeType, $clientCode, $classificationCode);
        $orders = $productSizeRepository->getGroupedOrderedProductSizeByClientCodeAndCollectionCode($order, $codeType, $clientCode, $classificationCode, $limit, $offset);

        $data = [];
        foreach ($orders as $order) {
            $data[] = [
                $order['ProductCode'],
                $order['name'],
                '',
                $order['Collection'],
                $order['totalSizesA'],
                $order['totalSizesB'],
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => count($allRecords),
            'recordsFiltered' => count($allRecords),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getListProductsDataAction($id)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        $this->view->disable();

        /** @var Client $client */
        $client = $this->entityManager->findOne(Client::class, [
            Client::PROPERTY_USER_ID . ' = :userId:',
            'bind' => [
                'userId' => $this->currentUser->getId()
            ]
        ]);

        $data = $this->request->get('data');
        parse_str($data, $filters);

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');

        $selected = [];
        foreach ($filters as $category) {
            if (!is_array($category)) {
                continue;
            }

            $selected = array_merge($category, $selected);
        }

        /** @var \Vemid\Entity\Repository\ProductTagRepository $productTagRepository */
        $productTagRepository = $this->entityManager->getRepository(ProductTag::class);
        $allProducts = $productTagRepository->getProductByTags($order,$selected, $search);
        $products = $productTagRepository->getProductByTags($order,$selected, $search, $limit, $offset);

        /** @var ClientOrderItemRepository $orderRepository */
        $orderRepository = $this->entityManager->getRepository(\ClientOrderItem::class);

        $data = [];
        foreach ($products as $product) {
            /** @var \Pricelist $priceListEUR */
            $priceListEUR = $this->entityManager->findOne(\Pricelist::class, [
                'name = :name:',
                'bind' => [
                    'name' => 'DEVIZNI'
                ]
            ]);
            $productPrices = $this->getDI()->getProductPrices();
            $wholesalePriceEUR = $productPrices->getWholesalePrice($priceListEUR, $product) ? $productPrices->getWholesalePrice($priceListEUR, $product) : 0;


            $orderItems = $orderRepository->getOrdersByProductAndClient($order, $product, $client);
            $hydrator = new Hydrator($product, $this->entityManager);

            $collectionName = '';
            if ($color = $hydrator->getColor()) {
                $preg_split = strlen(preg_split('/(\s[A-Z]{2,})/', $color->getName(), -1, PREG_SPLIT_NO_EMPTY)[0]);
                $collectionName = trim(substr($color->getName(), $preg_split, strlen($color->getName())));
            }

            $data[] = [
                $product->getCode(),
                $product->getName(),
                $this->_translate($collectionName),
                $wholesalePriceEUR,
                $product->getId(),
                $client && $orderItems->count() ? 1 : 0,
                $product->getImagePath()
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $allProducts->count(),
            'recordsFiltered' => $allProducts->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getProductProgramAction()
    {
        $this->view->disable();

        /** @var Code $codeProgramCategory */
        $codeProgramCategory = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('code')
            ]
        ]);

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        /** @var Code $codeProgramType */
        $codeProgramType = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '00',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $options = [];
        if ($codeProgramCategory && $codeProgramType) {
            $query = $this->modelsManager->createBuilder();
            $query->addFrom(ProductClassification::class, 'pc')
                ->leftJoin(Product::class, 'p.id = pc.productId', 'p')
                ->leftJoin(Classification::class, 'c.id = pc.entityId', 'c')
                ->where('p.isOrderable = :isOrderable: AND pc.entityTypeId = :entityTypeId:', [
                    'isOrderable' => true,
                    'entityTypeId' => Type::CLASSIFICATION
                ])
                ->andWhere('c.classificationCategoryCodeId = :classificationCategoryCodeId:', [
                    'classificationCategoryCodeId' => $codeProgramCategory->getId()
                ])
                ->andWhere('c.classificationTypeCodeId = :classificationTypeCodeId:', [
                    'classificationTypeCodeId' => $codeProgramType->getId()
                ])
                ->andWhere('c.parentClassificationId IS NULL');

            /** @var ProductClassification[] $productClassifications */
            $productClassifications = $query->getQuery()->execute();

            foreach ($productClassifications as $productClassification) {
                if (!$classification = $productClassification->getProductClassification()) {
                    continue;
                }

                if (!$code = $classification->getClassificationType()) {
                    continue;
                }

                $options[$classification->getCode()] = $classification->getName();
            }
        }

        $this->response->setContent(json_encode($options));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getGenderAction()
    {
        $this->view->disable();

        $genderCodes = [
            'Ženska' => ['1BŽ' => '1BŽ', '1OŽ' => '1OŽ', '1IŽ' => '1IŽ', '9BŽ' => '9BŽ', '9AŽ' => '9AŽ', '9OŽ' => '9OŽ'],
            'Muška' => ['1BM' => '1BM', '1IM' => '1IM', '1OM' => '1OM', '9BM' => '9BM', '9AM' => '9AM', '9OM' => '9OM'],
            'Unisex' => ['1OU' => '1OU', '1BU' => '1BU', '1IU' => '1IU', '9BU' => '9BU', '9AU' => '9AU', '9OU' => '9OU'],
            'Default' => ['9OX' => '9OX', '9AX' => '9AX', '9BX' => '9BX', '1BX' => '1BX', '1OX' => '1OX', '1AX' => '1AX']
        ];

        /** @var Classification $classificationParent */
        $classificationParent = $this->entityManager->findOne(Classification::class, [
            Classification::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('code')
            ]
        ]);

        $options = [];
        if ($classificationParent) {
            /** @var ClassificationRepository $classificationRepository */
            $classificationRepository = $this->entityManager->getRepository(Classification::class);

            $classifications = $classificationRepository->getParentClassificationsIncludedInOrders($classificationParent);

            foreach ($classifications as $classification) {
                $classificationCode = $classification->getCode();
                $genderCode = '';

                foreach ($genderCodes as $key => $codes) {
                    if (in_array($classificationCode, $codes, true)) {
                        $genderCode = $key;
                        break;
                    }
                }

                $options[$genderCode] = $genderCode;
            }
        }

        $this->response->setContent(json_encode($options));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getCollectionAction($id)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        $this->view->disable();
        $additionFilters = [];

        $genderCodes = [
            'Ženska' => ['1BŽ' => '1BŽ', '1OŽ' => '1OŽ', '1IŽ' => '1IŽ', '9BŽ' => '9BŽ', '9AŽ' => '9AŽ', '9OŽ' => '9OŽ'],
            'Muška' => ['1BM' => '1BM', '1IM' => '1IM', '1OM' => '1OM', '9BM' => '9BM', '9AM' => '9AM', '9OM' => '9OM'],
            'Unisex' => ['1OU' => '1OU', '1BU' => '1BU', '1IU' => '1IU', '9BU' => '9BU', '9AU' => '9AU', '9OU' => '9OU'],
            'Default' => ['9OX' => '9OX', '9AX' => '9AX', '9BX' => '9BX', '1BX' => '1BX', '1OX' => '1OX', '1AX' => '1AX']
        ];

        $gender = $this->request->getPost('gender');

        $classifications = [];
        if (isset($genderCodes[$gender])) {
            $additionFilters += array_values($genderCodes[$gender]);
        }

        /** @var Classification $programClassification */
        $programClassification = $this->entityManager->findOne(Classification::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('program')
            ]
        ]);

        if ($programClassification) {
            $additionFilters[] = $programClassification->getCode();
        }

        /** @var ProductRepository $productRepository */
        $productRepository = $this->entityManager->getRepository(Product::class);

        $allProducts = $productRepository->getProductEligibleForOrdering($order, $additionFilters, 1, null, $this->request->getPost('productTypeOrder'));

        $productIds = [];
        foreach ($allProducts as $product) {
            $productIds[] = $product->getId();
        }

        $options = [];
        if (!empty($productIds)) {

            $query = $this->modelsManager->createBuilder()
                ->addFrom(Classification::class, 'c')
                ->leftJoin(\OrderClassification::class, 'c.id = oc.classificationId', 'oc')
                ->leftJoin(\ProductClassification::class, 'oc.classificationId = pc.entityId', 'pc')
                ->leftJoin(\Product::class, 'p.id = pc.productId', 'p')
                ->where('p.isOrderable = :orderable:', [
                    'orderable' => true
                ])
                ->andWhere('pc.entityTypeId = :entityTypeId:', [
                    'entityTypeId' => Type::CLASSIFICATION
                ])
                ->andWhere('pc.productId IN ({ids:array})', [
                    'ids' => $productIds
                ])
                ->groupBy('c.id');

            /** @var Classification[] $classifications */
            $classifications = $query->getQuery()->execute();

            foreach ($classifications as $classification) {
                if (strpos($classification->getName(), '-') !== false) {
                    $name = explode('-', $classification->getName());
                    $name = $name[1];
                } else {
                    $name = explode(' ', $classification->getName());
                    array_shift($name);
                    $name = implode(' ', $name);
                }

                $options[$classification->getCode()] = $name;
            }
        }

        $this->response->setContent(json_encode($options));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getSizesAction()
    {
        $this->view->disable();

        $genderCodes = [
            'Ženska' => ['1BŽ' => '1BŽ', '1OŽ' => '1OŽ', '1IŽ' => '1IŽ', '9BŽ' => '9BŽ', '9AŽ' => '9AŽ', '9OŽ' => '9OŽ'],
            'Muška' => ['1BM' => '1BM', '1IM' => '1IM', '1OM' => '1OM', '9BM' => '9BM', '9AM' => '9AM', '9OM' => '9OM'],
            'Unisex' => ['1OU' => '1OU', '1BU' => '1BU', '1IU' => '1IU', '9BU' => '9BU', '9AU' => '9AU', '9OU' => '9OU'],
            'Default' => ['9OX' => '9OX', '9AX' => '9AX', '9BX' => '9BX', '1BX' => '1BX', '1OX' => '1OX', '1AX' => '1AX']
        ];

        $classifications = [];
        /** @var Classification $oldCollectionClassification */
        $oldCollectionClassification = $this->entityManager->findOne(Classification::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('code')
            ]
        ]);

        if ($oldCollectionClassification) {
            $classifications[] = $oldCollectionClassification->getId();
        }

        $gender = $this->request->getPost('gender');

        if (isset($genderCodes[$gender])) {
            /** @var Classification $programClassification */
            $genderClassifications = $this->entityManager->find(Classification::class, [
                CodeType::PROPERTY_CODE . ' IN ({genders:array})',
                'bind' => [
                    'genders' => array_values($genderCodes[$gender])
                ]
            ]);

            foreach ($genderClassifications as $classification) {
                $classifications[] = $classification->getId();
            }
        }

        /** @var Classification $programClassification */
        $programClassification = $this->entityManager->findOne(Classification::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('program')
            ]
        ]);

        if ($programClassification) {
            $classifications[] = $programClassification->getId();
        }

        /** @var ProductClassificationRepository $productClassificationRepository */
        $productClassificationRepository = $this->entityManager->getRepository(ProductClassification::class);

        /** @var Product[] $products */
        $products = $productClassificationRepository->getProductByEligibleClassifications($classifications);

        $productIds = [];
        foreach ($products as $product) {
            $productIds[] = $product['productIds'];
        }

        $options = [];

        /** @var \CodeType $codeType */
        $codeType = $this->entityManager->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        if ($codeType) {
            /** @var Code $productTypeCode */
            $productTypeCode = $this->entityManager->findOne(Code::class, [
                'code = :code: AND codeTypeId = :codeTypeId:',
                'bind' => [
                    'code' => $this->request->getPost('productTypeOrder'),
                    'codeTypeId' => $codeType->getId()
                ]
            ]);

            if (!empty($productIds) && $productTypeCode) {
                /** @var ClassificationRepository $classificationRepository */
                $classificationRepository = $this->entityManager->getRepository(Classification::class);

                $ageClassifications = $classificationRepository->getAgeCollectionsIncludedInOrders($productTypeCode, $productIds);

                foreach ($ageClassifications as $ageClassification) {
                    $options[$ageClassification->getCode()] = $ageClassification->getName();
                }
            }
        }

        $this->response->setContent(json_encode($options));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getGroupModelAction()
    {
        $this->view->disable();

        $genderCodes = [
            'Ženska' => ['1BŽ' => '1BŽ', '1OŽ' => '1OŽ', '1IŽ' => '1IŽ', '9BŽ' => '9BŽ', '9AŽ' => '9AŽ', '9OŽ' => '9OŽ'],
            'Muška' => ['1BM' => '1BM', '1IM' => '1IM', '1OM' => '1OM', '9BM' => '9BM', '9AM' => '9AM', '9OM' => '9OM'],
            'Unisex' => ['1OU' => '1OU', '1BU' => '1BU', '1IU' => '1IU', '9BU' => '9BU', '9AU' => '9AU', '9OU' => '9OU'],
            'Default' => ['9OX' => '9OX', '9AX' => '9AX', '9BX' => '9BX', '1BX' => '1BX', '1OX' => '1OX', '1AX' => '1AX']
        ];

        $classifications = [];
        /** @var Classification $oldCollectionClassification */
        $oldCollectionClassification = $this->entityManager->findOne(Classification::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('collection')
            ]
        ]);

        if ($oldCollectionClassification) {
            $classifications[] = $oldCollectionClassification->getId();
        }

        $gender = $this->request->getPost('gender');
        $genderOptions = [];
        if (isset($genderCodes[$gender])) {
            /** @var Classification $programClassification */
            $genderClassifications = $this->entityManager->find(Classification::class, [
                CodeType::PROPERTY_CODE . ' IN ({genders:array})',
                'bind' => [
                    'genders' => array_values($genderCodes[$gender])
                ]
            ]);

            foreach ($genderClassifications as $classification) {
                $genderOptions[] = $classification->getId();
            }
        }

        /** @var Classification $programClassification */
        $programClassification = $this->entityManager->findOne(Classification::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('program')
            ]
        ]);

        if ($programClassification) {
            $classifications[] = $programClassification->getId();
        }

        /** @var ProductClassificationRepository $productClassificationRepository */
        $productClassificationRepository = $this->entityManager->getRepository(ProductClassification::class);

        $products = $productClassificationRepository->getProductByEligibleClassifications($classifications + $genderOptions);
        $productIds = [];
        $options = [];
        foreach ($products as $product) {
            $productIds[] = $product['productIds'];
        }

        if (!empty($productIds)) {
            /** @var ClassificationRepository $classificationRepository */
            $classificationRepository = $this->entityManager->getRepository(Classification::class);

            /** @var Classification[] $groupClassifications */
            $groupClassifications = $classificationRepository->getProductSubGroupClassificationsIncludedInOrder($genderOptions, $productIds);

            foreach ($groupClassifications as $classification) {
                $nameChunk = explode(' ', $classification->getName());
                $options[$classification->getCode()] = array_pop($nameChunk);
            }
        }

        $this->response->setContent(json_encode($options));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getPrioritiesAction()
    {
        $this->view->disable();
        $options  = [];
        /** @var Classification $collection */
        $collection = $this->entityManager->findOne(Classification::class, [
            'code = :code:',
            'bind' => [
                'code' => $this->request->getPost('collection')
            ]
        ]);

        if ($collection) {
            foreach ($collection->getPriorityClassifications() as $priorityClassification) {
                if (!$priority = $priorityClassification->getPriority()) {
                    continue;
                }

                $options[$priority->getId()] = $priority;
            }
        }

        $this->response->setContent(json_encode($options));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function downloadFormAction($id)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        $form = new OrderDownloadForm(null, ['user' => $this->currentUser, 'order' => $order]);

        return (new Renderer())->render($form);
    }

    public function downloadShopsFormAction($id)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        $form = new OrderDownloadForm(null, ['user' => $this->currentUser, 'order' => $order]);

        return (new Renderer())->render($form);
    }

    public function prepareDownloadAction($id)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        return ['url' => '/orders/download?orderId=' . $order->getId() . '&' . http_build_query($this->request->getPost())];
    }

    public function prepareDownloadShopsAction($id)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        return ['url' => '/orders/download-shops-orders?orderId=' . $order->getId() . '&' . http_build_query($this->request->getPost())];
    }

    public function downloadAction()
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $this->request->getQuery('orderId'))) {
            return $this->returnNotFound();
        }

        /** @var array $oldCollections */
        $oldCollections = $this->request->getQuery('oldCollections');

        $tables = [];
        foreach ($oldCollections as $code) {
            /** @var Classification $classification */
            $classification = $this->entityManager->findOne(Classification::class, [
                'code = :code:',
                'bind' => [
                    'code' => $code
                ]
            ]);

            if (!$classification) {
                continue;
            }

            $exporter = new \Vemid\Exporter\Order\Exporter(
                $this->modelsManager->createBuilder(), $order, $classification
            );

            $tables[] = $exporter->export();
        }

        $renderer = new \Vemid\Table\Renderer\ExcelRenderer($tables, 'Narudžbenice');
        $renderer->render();
    }

    public function downloadShopsOrdersAction()
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $this->request->getQuery('orderId'))) {
            return $this->returnNotFound();
        }

        $printer = new \Vemid\Services\Excel\OrderPrinter($this->entityManager, $this->modelsManager, $order);
        $printer->printFile($this->request->get('oldCollections'));

        $this->response->redirect('orders/overview/' . $order->getId());

        $this->flashSession->success(sprintf('Štampanje u toku. Uskoro će fajl biti poslat mailom!'));
    }

    public function statusAction()
    {
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        $postData = $this->request->getPost();

        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $postData['orderId'])) {
            return $this->returnNotFound();
        }

        $order->setActive($postData['status'] === 'enable' ? 1 : 0);
        if (!$this->entityManager->save($order)) {
            $this->addFlashMessagesFromEntity($order);

            return;
        }

        if($postData['status'] === 'enable') {
            foreach($order->getOrderClassifications() as $classification) {
                $productOrderable = new \Vemid\Task\Classifications\SetProductsIsOrderable();
                $productOrderable->classification = $classification->getClassification();
                $productOrderable->runInBackground();
            }
        }
        else {
            foreach($order->getOrderClassifications() as $classification) {
                $productOrderable = new \Vemid\Task\Classifications\UnSetProductsIsOrderable();
                $productOrderable->classification = $classification->getClassification();
                $productOrderable->runInBackground();
            }
        }

        $this->dispatcher->setParam('url', '/orders/list');

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t(sprintf('Order je setovan kao %s', $order->getActive() ? 'aktivan' : 'neaktivan')),
            null,
            MessengerMessage::SUCCESS
        );
    }

    public function addOrderItemsAction($id)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        $this->assets->collection(DefaultAssets::ASSETS_FOOT_JS)
            ->addJs(APP_PATH . 'assets/js/drop-zone.js');

        $this->assets->collection(DefaultAssets::ASSETS_HEAD_CSS)
            ->addCss(APP_PATH . 'assets/css/drop-zone.css');

        $notFoundCodes = [];
        $notFoundSizes = [];
        $notFoundShopCodes = [];
        $hasFileUploaded = false;

        if ($this->session->has('orderItems__' . $order->getId())) {
            $file = $this->session->get('orderItems__' . $order->getId())['file'];
            $hasFileUploaded = true;

            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
            $objPHPExcel = $objReader->load($file);
            $objPHPExcel->setActiveSheetIndex();
            $sheetData = $objPHPExcel->getActiveSheet()->toArray();
            $data = [];

            $counter = 0;
            $indexModel = 0;
            $indexShop = 1;
            $indexQty = 2;
            $indexSize = 3;
            foreach ($sheetData as $row) {
                $array_filter = array_filter($row);
                if (count($array_filter) !== 4) {
                    continue;
                }

                if ($counter === 0) {
                    $arrayHeader = array_map('strtolower', $array_filter);
                    $arrayHeader = array_map('trim', $arrayHeader);
                    $flippedData = array_flip($arrayHeader);
                    $indexModel = $flippedData['model'];
                    $indexShop = $flippedData['radnja'];
                    $indexQty = $flippedData['kolicina'];
                    $indexSize = $flippedData['velicina'];
                    $counter++;
                } else {
                    $filteredData = array_filter($array_filter);
                    $data[] = [
                        'mp' => $filteredData[$indexShop],
                        'code' => $filteredData[$indexModel],
                        'size' => $filteredData[$indexSize],
                        'qtyA' => $filteredData[$indexQty],
                    ];
                }
            }

            $codes = array_values(array_unique(array_column($data, 'code')));
            $mps = array_values(array_unique(array_column($data, 'mp')));

            $query = $this->modelsManager->createBuilder()
                ->columns(['code'])
                ->from(Product::class)
                ->where('code IN ({codes:array})', [
                    'codes' => $codes
                ]);

            $productCodes = $query->getQuery()->execute()->toArray();
            $foundCodes =  array_column($productCodes, 'code');
            $notFoundCodes = array_diff($codes, $foundCodes);

            $query = $this->modelsManager->createBuilder()
                ->columns(['c.code'])
                ->addFrom(Code::class, 'c')
                ->leftJoin(CodeType::class, 'ct.id = c.codeTypeId', 'ct')
                ->where('c.code IN ({codes:array})', [
                    'codes' => $mps
                ])
                ->andWhere('ct.code = :code:', [
                    'code' => CodeType::SHOPS
                ]);

            $shopCodes = $query->getQuery()->execute()->toArray();
            $foundShopCodes =  array_column($shopCodes, 'code');
            $notFoundShopCodes = array_diff($mps, $foundShopCodes);

            foreach ($foundCodes as $code) {
                $filtered = array_filter($data, function ($var) use ($code) {
                    return ($var['code'] === $code);
                });

                $sizes = array_values(array_unique(array_column($filtered, 'size')));

                $query = $this->modelsManager->createBuilder()
                    ->columns(['c.code'])
                    ->addFrom(Product::class, 'p')
                    ->leftJoin(ProductSize::class, 'ps.productId = p.id', 'ps')
                    ->leftJoin(Code::class, 'c.id = ps.codeId', 'c')
                    ->where('p.code = :codeP:', [
                        'codeP' => $code
                    ])
                ->andWhere('c.code IN ({sizeArr:array})', [
                    'sizeArr' => $sizes
                ]);

                $codeSizes = $query->getQuery()->execute()->toArray();
                $foundCodeSizes =  array_column($codeSizes, 'code');
                $diffSizes = array_diff($sizes, $foundCodeSizes);
                if (count($diffSizes)) {
                    $notFoundSizes[$code] = $diffSizes;
                }
            }
        }

        $this->view->setVar('order', $order);
        $this->view->setVar('notFoundCodes', $notFoundCodes);
        $this->view->setVar('notFoundSizes', $notFoundSizes);
        $this->view->setVar('notFoundShopCodes', $notFoundShopCodes);
        $this->view->setVar('hasFileUploaded', $hasFileUploaded);

        $this->view->pick('orders/add-order-items');
    }

    public function clearErrorsAction($id)
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        if ($this->session->has('orderItems__' . $order->getId())) {
            $this->session->remove('orderItems__' . $order->getId());

            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('File obrisan!'),
                null,
                \Vemid\Messenger\Message::SUCCESS
            );

            return;
        }

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t('Nesto je pošlo po zlu!'),
            null,
            \Vemid\Messenger\Message::DANGER
        );
    }

    public function chooseOrderAction()
    {
        $orders = $this->entityManager->find(\Order::class, [
            'active = :active:',
            'bind' => [
                'active' => true
            ],
            'order' => 'startDate DESC'
        ]);

        $this->view->setVar('orders', $orders);
        $this->view->pick('orders/choose-order');
    }

    public function viewByProductAction($productId)
    {
        $this->tag::appendTitle($this->translator->t("Detalji porudzbina"));
        /** @var Product $product */
        if (!$product = $this->entityManager->findOne(Product::class, $productId)) {
            return $this->returnNotFound();
        }

        $hydrator = new Hydrator($product, $this->entityManager);

        /** @var ClientOrderItemRepository $productTotals */
        $productTotals = $this->entityManager->getRepository(\ClientOrderItem::class);
        $orderTotal = $productTotals->getOrderPerProduct($product);

        /** @var OrderItemRepository $orderRepo*/
        $orderRepo = $this->entityManager->getRepository(\OrderItem::class);
        $orderTotal2 = $orderRepo->getOrderPerProduct($product);

        /** @var ProductionOrderRepository $totals */
        $totals = $this->entityManager->getRepository(\ProductionOrder::class);
        $totalPlanned = $totals->getTotalPlanned($product);
        $totalReceived = $totals->getTotalReceived($product);

        $dataShops = [];
        foreach ($product->getProductSizes() as $productSize) {
            $orderItems = $productSize->getOrderItems([
                'entityTypeId = :entityTypeId:',
                'bind' => [
                    'entityTypeId' => Type::CODE
                ]
            ]);

            foreach ($orderItems as $orderItem) {
                $dataShops[$orderItem->getEntityId()][$productSize->getCodeId()]['A'] += $orderItem->getQuantityA();
                $dataShops[$orderItem->getEntityId()][$productSize->getCodeId()]['B'] += $orderItem->getQuantityA();
                $dataShops[$orderItem->getEntityId()][$productSize->getCodeId()]['ID'] += $orderItem->getId();
            }
        }

        $dataClientsA = [];
        $dataClientsB = [];
        foreach ($product->getProductSizes() as $productSize) {
            $orderItems = $productSize->getOrderItems([
                'entityTypeId = :entityTypeId:',
                'bind' => [
                    'entityTypeId' => Type::CLIENT
                ]
            ]);

            foreach ($orderItems as $orderItem) {
                $dataClientsA[$orderItem->getEntityId()][$productSize->getCodeId()]['QTY'] += $orderItem->getQuantityA();
                $dataClientsA[$orderItem->getEntityId()][$productSize->getCodeId()]['ID'] += $orderItem->getId();
                $dataClientsB[$orderItem->getEntityId()][$productSize->getCodeId()]['QTY'] += $orderItem->getQuantityB();
                $dataClientsB[$orderItem->getEntityId()][$productSize->getCodeId()]['ID'] += $orderItem->getId();
            }
        }

        /** @var CodeType $codeTypeShop */
        $codeTypeShop = $this->entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::SHOPS
            ]
        ]);

        $shops = $this->entityManager->find(Code::class, [
            'codeTypeId = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeTypeShop->getId()
            ]
        ]);

        /** @var ProductCalculation $productCalculation */
        $productCalculation = $this->entityManager->findOne(\ProductCalculation::class, [
            'status = :status: AND productId = :productId:',
            'bind' => [
                'productId' => $product->getId(),
                'status' => ProductCalculation::STATUS_FINAL
            ]
        ]);

        if ($productCalculation) {
            $price = new Vemid\Service\Product\Price($this->entityManager, $productCalculation, null);

            $this->view->setVar('productPrices', $price->getPrices($product));
        }

        $this->view->setVar('product', $product);
        $this->view->setVar('orderTotal', $orderTotal);
        $this->view->setVar('orderTotal2', $orderTotal2);
        $this->view->setVar('totals', $totalPlanned);
        $this->view->setVar('totalReceived', $totalReceived);
        $this->view->setVar('hydrator', $hydrator);
        $this->view->setVar('dataShops', $dataShops);
        $this->view->setVar('dataClientsA', $dataClientsA);
        $this->view->setVar('dataClientsB', $dataClientsB);
        $this->view->setVar('shops', $shops);
        $this->view->setVar('clients', $this->entityManager->find(Client::class));

        $this->view->pick('orders/view-by-product');
    }

    public function viewByProductPrintAction()
    {
        $this->view->disable();

        $params = $this->dispatcher->getParams();

        $paramProduct = $params[0];

        /** @var Product $product */
        if (!$product = $this->entityManager->findOne(Product::class, $paramProduct)) {
            return $this->returnNotFound();
        }

        $hydrator = new Hydrator($product, $this->entityManager);

        /** @var OrderItemRepository $productTotals */
        $productTotals = $this->entityManager->getRepository(\OrderItem::class);
        $orderTotal = $productTotals->getOrderPerProduct($product);

        /** @var ProductionOrderRepository $totals */
        $totals = $this->entityManager->getRepository(\ProductionOrder::class);
        $totalPlanned = $totals->getTotalPlanned($product);
        $totalReceived = $totals->getTotalReceived($product);

        $dataShops = [];
        foreach ($product->getProductSizes() as $productSize) {
            $orderItems = $productSize->getOrderItems([
                'entityTypeId = :entityTypeId:',
                'bind' => [
                    'entityTypeId' => Type::CODE
                ]
            ]);

            foreach ($orderItems as $orderItem) {
                $dataShops[$orderItem->getEntityId()][$productSize->getCodeId()]['A'] += $orderItem->getQuantityA();
                $dataShops[$orderItem->getEntityId()][$productSize->getCodeId()]['B'] += $orderItem->getQuantityA();
            }
        }

        $dataClientsA = [];
        $dataClientsB = [];
        foreach ($product->getProductSizes() as $productSize) {
            $orderItems = $productSize->getOrderItems([
                'entityTypeId = :entityTypeId:',
                'bind' => [
                    'entityTypeId' => Type::CLIENT
                ]
            ]);

            foreach ($orderItems as $orderItem) {
                $dataClientsA[$orderItem->getEntityId()][$productSize->getCodeId()] += $orderItem->getQuantityA();
                $dataClientsB[$orderItem->getEntityId()][$productSize->getCodeId()] += $orderItem->getQuantityB();
            }
        }

        /** @var CodeType $codeTypeShop */
        $codeTypeShop = $this->entityManager->findOne(CodeType::class, [
            'code = :code:',
            'bind' => [
                'code' => CodeType::SHOPS
            ]
        ]);

        $shops = $this->entityManager->find(Code::class, [
            'codeTypeId = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeTypeShop->getId()
            ]
        ]);

        /** @var ProductCalculation $productCalculation */
        $productCalculation = $this->entityManager->findOne(\ProductCalculation::class, [
            'status = :status: AND productId = :productId:',
            'bind' => [
                'productId' => $product->getId(),
                'status' => ProductCalculation::STATUS_FINAL
            ]
        ]);

        if($productCalculation) {
            $price = new Vemid\Service\Product\Price($this->entityManager, $productCalculation, null);
        }

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();

        $html = $this->getDI()->getSimpleView()->render('orders/view-by-product-print', [
            'product'=>$product,
            'orderTotal' => $orderTotal,
            'totals' => $totalPlanned,
            'totalReceived' => $totalReceived,
            'hydrator' => $hydrator,
            'dataShops' => $dataShops,
            'dataClientsA' => $dataClientsA,
            'dataClientsB' => $dataClientsB,
            'shops' => $shops,
            'clients' => $this->entityManager->find(Client::class),
            'siteUrl' => ltrim($config->application->baseUri, '/'),
            'productPrices' => $productCalculation ? $price->getPrices($product) : "",

        ]);

        $content = $this->getDI()->getPdfRenderer()->render($html);
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $product);

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;
    }

    public function choseCollectionPivotReportAction($id)
    {
        /** @var OrderClassification $orders */
        $orders = $this->entityManager->find(\OrderClassification::class, [
            'orderId = :orderId:',
            'bind' => [
                'orderId' => $id
            ]
        ]);

        $oldCollections = [];
        /** @var OrderClassification $classification */
        foreach ($orders as $classification) {
            $oldCollections[] = [
                'id' => $classification->getClassificationId(),
                'code' => $classification->getClassification()->getCode(),
                'name' => $classification->getClassification()->getName()
            ];
        }

        $this->view->setVar('oldCollections', $oldCollections);
        $this->view->setVar('orderId', $id);
        $this->view->pick('orders/chose-collection-pivot-report');
    }

    public function pivotReportAction($id,$collection)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        /** @var Classification $classification */
        if (!$classification = $this->entityManager->findOne(Classification::class, $collection)) {
            return $this->returnNotFound();
        }

        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_HEAD_JS)
            ->addJs(APP_PATH . 'assets/js/react-pivot-standalone-3.0.0.min.js');

        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_HEAD_CSS)
            ->addCss(APP_PATH . 'assets/css/react-pivot.css');

        /** @var Client $client */
        $client = $this->entityManager->findOne(Client::class, [
            Client::PROPERTY_USER_ID . ' = :userId:',
            'bind' => [
                'userId' => $this->currentUser->getId()
            ]
        ]);

        $clientId = 0;
        if($client) {
            $clientId = $client->getId();
        }

        /** @var ClientOrderItemRepository $orderItemRepository */
        $orderItemRepository = $this->entityManager->getRepository(ClientOrderItem::class);
        $reportData = $orderItemRepository->getPivotReportData($classification,$order,$clientId,$this->currentUser);

        $data = [];
        foreach ($reportData as $row) {

            /** @var Product $product */
            $product = $this->entityManager->findOne(Product::class, [
                Product::PROPERTY_ID . ' = :productId:',
                'bind' => [
                    'productId' =>  $row['product']
                ]
            ]);

            $hydrator = new Hydrator($product, $this->entityManager);
            $array = explode(' ', $hydrator->getProductGroup()->getName());

            $data[] = [
                'product' => $product->getName(),
                'model' => array_pop($array),
                'gender' => $hydrator->getGenderName(),
                'seasson' => $hydrator->getOldCollection()->getName(),
                'name' => $row['productCode'],
                'size' => $row['size'],
                'client' => $row['client'],
                'transaction' => [
                    'qtyA' => $row['quantityA'],
                    'qtyB' => $row['quantityB'],
                    'priceValueA' => $row['priceValueA'],
                    'priceValueB' => $row['priceValueB'],
                    'totalQty' => ($row['quantityA']+$row['quantityB']),
                    'totalValue' => ($row['priceValueA']+$row['priceValueB']),
                ]
            ];
        }

        $config = $this->getDI()->getConfig();

        $this->view->setVar('rows', json_encode($data));
        $this->view->setVar('siteUrl', ltrim($config->application->baseUri, '/'));
        $this->view->setVar('collection', $classification);
        $this->view->pick('orders/pivot-report');
    }

    public function pivotRetailOrderReportAction($id)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_HEAD_JS)
            ->addJs(APP_PATH . 'assets/js/react-pivot-standalone-3.0.0.min.js');

        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_HEAD_CSS)
            ->addCss(APP_PATH . 'assets/css/react-pivot.css');

        /** @var ProductSizeRepository $productSizeRepository */
        $productSizeRepository = $this->entityManager->getRepository(ProductSize::class);
        $reportData = $productSizeRepository->getPivotRetailReportData($order);

        $data = [];
        foreach ($reportData as $row) {
            $data[] = [
                'product' => $row['naziv'],
                'gender' =>  $row['pol'],
                'seasson' => $row['kolekcija'],
                'name' => $row['sifra'],
                'size' => $row['velicina'],
                'transaction' => [
                    'qtyA' => $row['kolicinaA'],
                    'qtyB' => $row['kolicinaB'],
                ]
            ];
        }

        $config = $this->getDI()->getConfig();

        $this->view->setVar('rows', json_encode($data));
        $this->view->setVar('siteUrl', ltrim($config->application->baseUri, '/'));
        $this->view->pick('orders/pivot-retail-order-report');

    }

    public function pivotReportDataAction($id)
    {
        $this->tag::appendTitle($this->translator->t("Pregled izvestaja porudzbine"));
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        $user = $this->currentUser;

        /** @var ClientOrderItemRepository $clientOrderItemRepository */
        $clientOrderItemRepository = $this->entityManager->getRepository(ClientOrderItem::class);
        $reportData = $clientOrderItemRepository->getReportDataAction($order,$user);

        $data = [];
        foreach ($reportData as $row) {
            $data[] = [
                'client' =>$row['client'],
                'product_name' =>$row['product_name'] ? $row['product_name'] : "",
                'model' =>$this->_translate($row['model']),
                'season' => $this->_translate($row['season']),
                'gender' => $this->_translate($row['gender']),
                'quantityA' =>$row['quantityA'],
                'quantityB' =>$row['quantityB'],
                'quantity' =>$row['quantityA']+$row['quantityB'],
                'price' => $row['price'],
                'age' => $this->_translate($row['age']),
                'total' => ($row['quantityA']+$row['quantityB'])*$row['price']
            ];
        }

        $out = array_values($data);
        $this->view->setVar('data', json_encode($out,JSON_UNESCAPED_UNICODE));
        $this->view->pick('orders/pivot-report-data');


    }
    public function downloadOrderDataAction($id)
    {
        /** @var Order $invoice */
        if (!$invoice = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        $user = $this->currentUser;

        $tables = [];

        $googleTranslate = new Vemid\Service\GoogleTranslator\GoogleTranslateClient();
        $translated = 'Export podataka u toku. Uskoro će fajl biti poslat mailom!';

        $this->addFlashMessage(
            $this->translator->t($translated),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );

        $exporter = new \Vemid\Exporter\Invoice\Exporter($this->modelsManager->createBuilder(), $invoice,$user,  $this->entityManager,$this->translator);

        $tables[] = $exporter->export();


        $renderer = new \Vemid\Table\Renderer\ExcelRenderer($tables, 'Order data');
        $renderer->render();


    }

    public function addOrderTotalAction($id) {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        $orderTotalTask = new AddOrderTotals();
        $orderTotalTask->order = $order;
        $orderTotalTask->runInBackground();

        $this->addFlashMessage(
            $this->translator->t("Uspesno setovan kolicine"),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function addOrderTotalItemAction($id) {

        /** @var Product $product */
        if (!$product = $this->entityManager->findOne(Product::class, $id)) {
            return $this->returnNotFound();
        }

        $orderTotalTask = new \Vemid\Task\Orders\AddOrderTotalItems();
        $orderTotalTask->product = $product;
        $orderTotalTask->run();

        $this->addFlashMessage(
            $this->translator->t("Uspesno setovan kolicine"),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }
}
