<?php

use \Vemid\Controller\CrudController;
use Vemid\Form\Renderer\Json as Renderer;
use \Vemid\Entity\Type;
use \Vemid\Messenger\Message;
use \Vemid\Service\MisWsdl\WarehouseOrder\WarehouseOrderCancellation;
use \Vemid\Printer\Manager;

/**
 * Class WarehouseOrdersController
 */
class WarehouseOrdersController extends CrudController
{
    public function listAction($id)
    {
        if (!$channelSupply = $this->entityManager->findOne(ChannelSupply::class, $id)) {
            $this->returnNotFound();
        }

        $this->tag::appendTitle($this->translator->t("Pregled naloga"));
        $this->view->setVar('channelSupplyId', $id);
    }

    /**
     * {@inheritdoc}
     */
    public function getCreateFormAction()
    {
        if (!isset($this->router->getParams()[1])) {
            $this->returnNotFound();
        }

        if (!$channelSupply = $this->entityManager->findOne(ChannelSupply::class, $this->router->getParams()[1])) {
            $this->returnNotFound();
        }

        return (new Renderer())->render(
            new WarehouseOrderForm(
                new WarehouseOrder(),
                ['channelSupply' => $channelSupply]
            )
        );
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var ChannelSupply $channelSupply */
        if (!$channelSupply = $this->entityManager->findOne(ChannelSupply::class, $this->request->getPost('channelSupplyId'))) {
            return $this->returnNotFound();
        }

        /** @var ChannelSupplyEntity $channelSupplyEntity */
        $channelSupplyEntity = $this->entityManager->findOne(ChannelSupplyEntity::class, [
            ChannelSupplyEntity::PROPERTY_CHANNEL_SUPPLY_ID . ' = :channelSupplyId: AND ' .
            ChannelSupplyEntity::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId: AND ' .
            ChannelSupplyEntity::PROPERTY_ENTITY_ID . ' = :entityId:',
            'bind' => [
                'channelSupplyId' => $channelSupply->getId(),
                'entityId' => $this->request->getPost('entity'),
                'entityTypeId' => $channelSupply->getType() === ChannelSupply::MP ? Type::CODE : Type::CLIENT
            ]
        ]);

        $channelSupplyExitEntity = $this->entityManager->findOne(ChannelSupplyEntity::class,$this->request->getPost('exitEntity'));

        if (!$channelSupplyEntity) {
            $this->addFlashMessage(
                $this->translator->t('Nije pronadjen krajnji korisnik'),
                null,
                Message::DANGER
            );

            return;
        }

        if ($channelSupply->getType() === ChannelSupply::MP) {
            $_POST[WarehouseOrder::PROPERTY_TYPE] = 'A';
        }

        $form = $this->getForm();
        if (!$form->isValid($this->request->getPost())) {
            $this->addFlashMessagesFromForm($form);
            return;
        }

        $form->synchronizeEntityData();
        /** @var WarehouseOrder $entity */
        $entity = $form->getEntity();
        $entity->setPosted(false);
        $entity->setChannelSupplyEntity($channelSupplyEntity);
        $entity->setStatus(WarehouseOrder::EDITABLE);
        $entity->setExitChannelSupplyEntity($channelSupplyExitEntity);

        if ($this->saveEntity($entity)) {
            $this->dispatcher->setParam('url', '/warehouse-orders/add-items/' . $entity->getId());
        }
    }

    public function addItemsAction($id)
    {
        /** @var WarehouseOrder $warehouseOrder */
        if (!$warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, $id)) {
            return $this->returnNotFound();
        }

        $barCodes = [];
        if ($warehouseOrder->isEditable()) {
            foreach ($warehouseOrder->getWarehouseOrderItems() as $orderItem) {
                $barCodes[strtolower($orderItem->getProductSize()->getBarcode())] = $orderItem->getQuantity();
            }
        }

        $this->view->setVar('warehouseOrder', $warehouseOrder);
        $this->view->setVar('serverStoredBarcodes', json_encode($barCodes));
        $this->view->pick('warehouse-orders/add-items');
    }

    public function storeItemsAction(int $warehouseOrderId)
    {
        /** @var WarehouseOrder $warehouseOrder */
        if (!$warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, $warehouseOrderId)) {
            return $this->returnNotFound();
        }

        $codes = $this->request->getPost('codes');
        $reasons = $this->request->getPost('reason');

        foreach ($codes as $barcode => $value) {
            /** @var ProductSize $productSize */
            $productSize = $this->entityManager->findOne(ProductSize::class, [
                ProductSize::PROPERTY_BARCODE . ' = :barcode:',
                'bind' => [
                    'barcode' => strtoupper($barcode)
                ]
            ]);

            if (!$productSize) {
                $this->addFlashMessage(
                    'Nepostoji produkt sa trazenim barcodom',
                    null,
                    \Vemid\Messenger\Message::DANGER
                );

                return;
            }

            $warehouseOrderItem = $this->entityManager->findOne(WarehouseOrderItem::class, [
                WarehouseOrderItem::PROPERTY_WAREHOUSE_ORDER_ID . ' = :warehouseOrderId: AND ' .
                WarehouseOrderItem::PROPERTY_PRODUCT_SIZE_ID . ' = :productSizeId:',
                'bind' => [
                    'warehouseOrderId' => $warehouseOrder->getId(),
                    'productSizeId' => $productSize->getId()
                ]
            ]);

            if (!$warehouseOrderItem) {
                $warehouseOrderItem = new WarehouseOrderItem();
                $warehouseOrderItem->setWarehouseOrder($warehouseOrder);
                $warehouseOrderItem->setProductSize($productSize);
            }

            $warehouseOrderItem->setQuantity($value);
            if (isset($reasons[strtoupper($barcode)])) {
                $warehouseOrderItem->setNote($reasons[strtoupper($barcode)]);
            }

            $this->entityManager->save($warehouseOrderItem);
        }

//        $warehouseOrderMisPusher = $this->getDI()->getMisDocumentPusher();
//        $warehouseOrder->setPushedToMis($warehouseOrderMisPusher($warehouseOrder));
//        $warehouseOrder->setStatus(WarehouseOrder::LOCKED);
//        $warehouseOrder->save($warehouseOrder);

        return ['error' => false, 'url' => '/warehouse-orders/list/' . $warehouseOrder->getChannelSupplyEntity()->getChannelSupplyId()];
    }

    public function findByBarcodeAction()
    {
        $this->view->disable();

        if (!$this->request->isPost()) {
            $form = new Vemid\Form\Form();
            $text = new \Phalcon\Forms\Element\Text('barcode');
            $text->setAttributes([
                'placeholder' => 'Barcode',
                'data-autofocus' => 'true'
            ]);

            $form->add($text);

            return (new Renderer())->render($form);
        }

        if (!$barcode = $this->request->getPost('barcode')) {
            $this->addFlashMessage(
                'Nepostoji nalog sa trazenim barcodom',
                null,
                \Vemid\Messenger\Message::DANGER
            );

            return;
        }

        /** @var WarehouseOrder $warehouseOrder */
        $warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, [
            WarehouseOrder::PROPERTY_WAREHOUSE_ORDER_CODE . ' = :warehouseOrderCode:',
            'bind' => [
                'warehouseOrderCode' => strtoupper($barcode)
            ]
        ]);

        if (!$warehouseOrder) {
            $this->addFlashMessage(
                'Nepostoji nalog sa trazenim barcodom',
                null,
                \Vemid\Messenger\Message::DANGER
            );

            return;
        }

        return ['error' => false, 'url' => '/warehouse-orders/delivered-items/' . $warehouseOrder->getId()];
    }

    public function deliveredItemsAction(int $warehouseOrderId)
    {
        /** @var WarehouseOrder $warehouseOrder */
        $warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, $warehouseOrderId);

        if (!$warehouseOrder) {
            $this->addFlashMessage(
                'Nepostoji nalog sa trazenim barcodom',
                null,
                \Vemid\Messenger\Message::DANGER
            );

            return;
        }

        $this->view->setVar('warehouseOrder', $warehouseOrder);
        $this->view->pick('warehouse-orders/delivered-items');
    }

    public function checkItemsAction(int $warehouseOrderId)
    {
        /** @var WarehouseOrder $warehouseOrder */
        if (!$warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, $warehouseOrderId)) {
            return $this->returnNotFound();
        }

        $channelSupplyEntity = $warehouseOrder->getChannelSupplyEntity();
        $entityId = $channelSupplyEntity->getEntityId();
        $entityTypeId = $channelSupplyEntity->getEntityTypeId();
        $type = $warehouseOrder->getType();
        $post = $this->request->getPost();
        $post = array_change_key_case($post, CASE_LOWER);

        $builder = $this->modelsManager->createBuilder();
        $query = $builder
            ->addFrom(OrderItem::class, 'oi')
            ->leftJoin(ProductSize::class, 'oi.productSizeId = ps.id', 'ps')
            ->where(OrderItem::PROPERTY_ENTITY_ID . ' = :entityId:', [
                'entityId' => $entityId
            ])
            ->andWhere(OrderItem::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:', [
                'entityTypeId' => $entityTypeId
            ])
            ->andWhere(ProductSize::PROPERTY_BARCODE . ' IN ({ids:array})', [
                'ids' => array_keys($post)
            ]);

        /** @var OrderItem[] $orderItems */
        $orderItems = $query->getQuery()->execute();

        $collection = [];
        foreach ($orderItems as $orderItem) {
            $productSize = $orderItem->getProductSize();

            $warehouseOrderItems = $this->modelsManager->createBuilder()
                ->addFrom(WarehouseOrderItem::class, 'who')
                ->innerJoin(WarehouseOrder::class, 'who.warehouseOrderId = wo.id', 'wo')
                ->innerJoin(ChannelSupplyEntity::class, 'wo.channelSupplyEntityId = cse.id', 'cse')
                ->where('wo.id != :woId:', [
                    'woId' => $warehouseOrder->getId()
                ])
                ->andWhere('cse.entityTypeId = :entityTypeId:', [
                    'entityTypeId' => $orderItem->getEntityTypeId()
                ])
                ->andWhere('who.productSizeId = :productSizeId:',[
                    'productSizeId' =>$productSize->getId()
                ])
                ->andWhere('cse.entityId = :entityId:', [
                    'entityId' => $orderItem->getEntityId()
                ])->getQuery()->execute()->toArray();

            $alreadyPacked = array_sum(array_column($warehouseOrderItems, 'quantity'));

            $skip = true;
            if (isset($post[strtolower($productSize->getBarcode())])) {
                $skip = false;
            }

            if (isset($post[$productSize->getBarcode()])) {
                $skip = false;
            }

            if ($skip) {
                continue;
            }

            $qty = 0;
            if ($orderItem) {
                $qty = $orderItem->getQuantityA();
                if ($type === 'B') {
                    $qty = $orderItem->getQuantityB();
                }
            }

            if ($qty === $post[strtolower($productSize->getBarcode())]) {
                continue;
            }

            $collection[$orderItem->getProductSizeId()]['id'] = $productSize->getBarcode();
            $collection[$orderItem->getProductSizeId()]['item'] = $post[strtolower($productSize->getBarcode())];
            $collection[$orderItem->getProductSizeId()]['name'] = $productSize->getProduct()->getName();
            $collection[$orderItem->getProductSizeId()]['size'] = $productSize->getCode()->getName();
            $collection[$orderItem->getProductSizeId()]['ordered'] = $qty - (int)$post[strtolower($productSize->getBarcode())] - $alreadyPacked;
            $collection[$orderItem->getProductSizeId()]['alreadyPacked'] = $alreadyPacked;
            $collection[$orderItem->getProductSizeId()]['qty'] = $qty;
        }

        $this->view->setVar('warehouseOrder', $warehouseOrder);
        $this->view->setVar('items', $collection);
        $this->view->pick('warehouse-orders/check-items');
    }

    public function getListDataAction()
    {
        $this->view->disable();
        /** @var ChannelSupply $channelSupply */
        $channelSupply = $this->entityManager->findOne(ChannelSupply::class, $this->request->getQuery('channelSupplyId'));
        if (!$channelSupply) {
            $this->returnNotFound();
        }

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');

        $query = $this->modelsManager->createBuilder();
        $query->addFrom(WarehouseOrder::class, 'wo')
            ->innerJoin(ChannelSupplyEntity::class, 'wo.channelSupplyEntityId = cse.id', 'cse');

        if ($channelSupply->getType() === ChannelSupply::MP) {
            $query->innerJoin(Code::class, 'co.id = cse.entityId', 'co');
        } else {
            $query->innerJoin(Client::class, 'co.id = cse.entityId', 'co');
        }

        $query->where('cse.channelSupplyId = :channelSupplyId:', [
            'channelSupplyId' => $channelSupply->getId()
        ]);

        if (is_array($search) && $search['value']) {
            $query->andWhere('wo.warehouseOrderCode LIKE :search: OR co.name LIKE :search:', [
                'search' => '%' . $search['value'] . '%'
            ]);
        }
        if($this->request->getQuery('courier')) {
            $query->andWhere('wo.posted = :posted: AND wo.useCourierService = :forCourier:',[
                'posted' => false,
                'forCourier' => true
            ]);
        }
        $query->orderBy('wo.id desc');

        $queryForCount = clone $query;

        /** @var WarehouseOrder[] $warehouseOrders */
        $allWarehouseOrders = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);

        /** @var WarehouseOrder[] $warehouseOrders */
        $warehouseOrders = $query->getQuery()->execute();

        $data = [];
        foreach ($warehouseOrders as $warehouseOrder) {
            /** @var WarehouseOrderGroupItem $item */
            $item = $this->entityManager->findOne(WarehouseOrderGroupItem::class,[
                WarehouseOrderGroupItem::PROPERTY_WAREHOUSE_ORDER_ID .'= :id: ',
                'bind' =>[
                    'id' => $warehouseOrder->getId()
                ],
            ]);
            $pushed ="0";
            if($item) {
                if($item->getWarehouseOrderGroup()) {
                    $pushed = "1";
                }
            }
            $data[] = [
                (string)$warehouseOrder->getChannelSupplyEntity(),
                (string)"Ulaz ".$warehouseOrder->getPriority()->getPriorityNumber() ." - ".$warehouseOrder->getPriority()->getPrioritySerbianDate(),
                $warehouseOrder->getWarehouseOrderCode(),
                $warehouseOrder->getStatus(),
                $warehouseOrder->getUseCourierService(),
                $warehouseOrder->getPostalBarcode(),
                $warehouseOrder->getId(),
                $pushed,
                $warehouseOrder->getLocked(),
                $warehouseOrder->getPushedToMis()
            ];
        }

        $count = $allWarehouseOrders->count();
        $responseData = [
            'draw' => $page,
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function storeDeliveredItemsAction($warehouseOrderId)
    {
        /** @var WarehouseOrder $warehouseOrder */
        $warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, $warehouseOrderId);

        if (!$warehouseOrder) {
            $this->addFlashMessage(
                'Nepostoji nalog!',
                null,
                \Vemid\Messenger\Message::DANGER
            );

            return;
        }

        $codes = $this->request->getPost('code');
        $note = $this->request->getPost('note');
        $items = [];

        foreach ($warehouseOrder->getWarehouseOrderItems() as $warehouseOrderItem) {
            if (!isset($codes[strtolower($warehouseOrderItem->getProductSize()->getBarcode())])) {
                $items[] = $warehouseOrderItem;
                continue;
            }

            $deliveredQty = $codes[strtolower($warehouseOrderItem->getProductSize()->getBarcode())];
            $warehouseOrderItem->setDeliveredQuantity($deliveredQty);
            $this->entityManager->save($warehouseOrderItem);

            if ((int)$deliveredQty !== (int)$warehouseOrderItem->getQuantity()) {
                $items[] = $warehouseOrderItem;
            }
        }

        $mailManager = $this->getDI()->getMailManager();
        $message = $mailManager->createMessageFromView("mail-template/warehouse-order-items-confirmation.volt", [
            'items' => $items,
            'note' => $note
        ]);
        $message->setSubject('Izvetaštaj zaprimnjene robe');
        $message->setFrom('darkovesic3@bebakids.com', $this->currentUser->getDisplayName());
        $message->setTo('darkovesic3@gmail.com');
        $message->send();

        return [
            'error' => false,
            'url' => '/'
        ];
    }

    public function setStatusAction($warehouseOrderId)
    {
        $this->view->disable();
        /** @var WarehouseOrder $warehouseOrder */
        $warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, $warehouseOrderId);

        if (!$warehouseOrder) {
            $this->addFlashMessage(
                'Nepostoji nalog!',
                null,
                \Vemid\Messenger\Message::DANGER
            );

            return;
        }

        $responseStatus ="";
        $status = $this->request->getPost('status') !== 'false' ? WarehouseOrder::LOCKED : WarehouseOrder::EDITABLE;
//        if ($status === WarehouseOrder::LOCKED) {
//            $warehouseOrderMisPusher = $this->getDI()->getMisDocumentPusher();
//            $responseStatus = $warehouseOrderMisPusher($warehouseOrder);
//            $warehouseOrder->setPushedToMis($responseStatus);
//        }

        if ($status === WarehouseOrder::EDITABLE) {
            $warehouseOrderCancellation = new WarehouseOrderCancellation($warehouseOrder);
            $response = $warehouseOrderCancellation->run();
            $responseStatus = $response['success'];
            $warehouseOrder->setPushedToMis(!$response['success']);

        }

            $warehouseOrder->setStatus($status);
            $warehouseOrder->setLocked(false);
            if (!$this->saveEntity($warehouseOrder)) {
                $this->addFlashMessagesFromEntity($warehouseOrder);
                return;
            }

//        else {
//            $this->addFlashMessage(
//                'Nemoze se konektovati na servis!',
//                null,
//                \Vemid\Messenger\Message::DANGER
//            );
//
//            $connection = ssh2_connect('192.168.100.210', 22);
//            ssh2_auth_password($connection, 'root', 'admin710412');
//
//            $stream = ssh2_exec($connection, 'service tomcat restart');
//        }

       
    }

    public function setUseCourierServiceAction($warehouseOrderId)
    {
        if (1 == 2) {
            $this->view->disable();
            /** @var WarehouseOrder $warehouseOrder */
            $warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, $warehouseOrderId);

            if (!$warehouseOrder) {
                $this->addFlashMessage(
                    'Nepostoji nalog!',
                    null,
                    \Vemid\Messenger\Message::DANGER
                );

                return;
            }

            $useCourierService = $this->request->getPost('useCourierService') !== 'false';

            $warehouseOrder->setUseCourierService($useCourierService);
            if (!$this->saveEntity($warehouseOrder)) {
                $this->addFlashMessagesFromEntity($warehouseOrder);
                return;
            }
        }
    }

    public function printWarehouseOrderAction($warehouseOrderId)
    {
        /** @var WarehouseOrder $warehouseOrder */
        $warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, $warehouseOrderId);
        $warehouseOrderItems = $warehouseOrder->getWarehouseOrderItems([
            'order' => 'productSizeId ASC'
        ]);
        $data = [];
        /** @var WarehouseOrderItem $item */
        foreach ($warehouseOrderItems as $item) {
            $data[] = [
                'sku' => $item->getProductSize()->getProduct()->getCode(),
                'name' => $item->getProductSize()->getProduct()->getName(),
                'size' => $item->getProductSize()->getCode(),
                'qty' => $item->getQuantity()
            ];
        }

        $tcpdf = new \TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $params = $tcpdf->serializeTCPDFtagParameters([
            $warehouseOrder->getWarehouseOrderCode(),
            'C128A',
            '',
            '',
            55,
            15,
            0.4,
            [
                'position' => 'S',
                'border' => false,
                'padding' => 0,
                'fgcolor' => [0, 0, 0],
                'bgcolor' => [255, 255, 255],
                'text' => true, 'font' => 'helvetica', 'fontsize' => 8, 'stretchtext' => 4
            ],
            'N'
        ]);

        $courierParams = $tcpdf->serializeTCPDFtagParameters([
            $warehouseOrder->getPostalBarcode(),
            'C128A',
            '',
            '',
            70,
            15,
            0.4,
            [
                'position' => 'S',
                'border' => false,
                'padding' => 0,
                'fgcolor' => [0, 0, 0],
                'bgcolor' => [255, 255, 255],
                'text' => true, 'font' => 'helvetica', 'fontsize' => 8, 'stretchtext' => 4
            ],
            'N'
        ]);

        $html = $this->getDI()->getSimpleView()->render('warehouse-orders/print-warehouse-order', [
            'warehouseOrder' => $warehouseOrder,
            'data' => $data,
            'params' => $params,
            'courierParams' => $courierParams,
        ]);

        $fileName = sprintf('/tmp/%s.pdf', $warehouseOrder->getWarehouseOrderCode());

        $tcpdf->SetCreator(PDF_CREATOR);
        $tcpdf->SetAuthor('Bebakids');
        $tcpdf->SetTitle('STAMPA NALOGA');
        $tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $tcpdf->setPrintHeader(false);
        $tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $tcpdf->SetMargins(10, 0, 10, true);
        $tcpdf->AddPage();
        $tcpdf->writeHTML($html, true, false, false, false, 'left');
        $tcpdf->Output($fileName, 'F');

        $config = $this->getDI()->getConfig();
        $printerFabricConfig = $config->printers->fabric;

        $manager = new Manager(
            $printerFabricConfig->ipp,
            $printerFabricConfig->username,
            $printerFabricConfig->password
        );
        $manager($fileName);

        $this->addFlashMessage(
            $this->translator->t('Dokument poslat na štampanje'),
            null,
            Message::SUCCESS
        );

        $this->dispatcher->setParam('url', '/warehouse-orders/list/');
    }

    public function addShipmentAction($warehouseOrderId)
    {
        $this->view->disable();
        /** @var WarehouseOrder $warehouseOrder */
        $warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, $warehouseOrderId);

        if (!$warehouseOrder) {
            $this->addFlashMessage(
                'Nepostoji nalog!',
                null,
                \Vemid\Messenger\Message::DANGER
            );

            return;
        }

        $status = $this->request->getPost('courier');
        if ($warehouseOrder->getStatus() === WarehouseOrder::LOCKED && $warehouseOrder->getPushedToMis() === true) {
            $postalManager = $this->getDI()->getPostalManager();
            $postalManager($warehouseOrder);
        }

        $this->addFlashMessage(
            'Poslat poziv kuriru!',
            null,
            \Vemid\Messenger\Message::SUCCESS
        );

        return;

    }

    public function addWeightFormAction($warehouseOrderId = null)
    {
        if (!$this->request->isAjax()) {
            return $this->returnNotFound();
        }

        $warehouseOrder = null;
        if ($warehouseOrderId) {
            /** @var WarehouseOrder $warehouseOrder */
            $warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, $warehouseOrderId);
        }

        $neto = new \Phalcon\Forms\Element\Text('neto');
        $neto->setAttribute('placeHolder', 'Neto');
        if($warehouseOrder->getNeto()) {
            $neto->setDefault($warehouseOrder->getNeto());
        }
        $bruto = new \Phalcon\Forms\Element\Text('bruto');
        $bruto->setAttribute('placeHolder', 'Bruto');
        if($warehouseOrder->getBruto()) {
            $bruto->setDefault($warehouseOrder->getBruto());
        }

        $form = new \Vemid\Form\Form();
        $form->add($neto);
        $form->add($bruto);
        $form->add(new \Phalcon\Forms\Element\Text('testFakeInout'));

        return (new \Vemid\Form\Renderer\Json())->render($form);
    }

    public function addWeightAction($id)
    {
        if ($this->request->isPost()) {
            $post = $this->request->getPost();

            /** @var WarehouseOrder $warehouseOrder */
            if (!$warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, $id)) {
                return $this->returnNotFound();
            }

            $warehouseOrder->setBruto($post['bruto']);
            $warehouseOrder->setNeto($post['neto']);
            if (!$this->entityManager->save($warehouseOrder)) {
                $this->addFlashMessagesFromEntity($warehouseOrder);

                return;
            }

            $this->addFlashMessage(
                'Uspesno',
                null,
                \Vemid\Messenger\Message::SUCCESS
            );
        }
    }

    public function getEntityName()
    {
        return WarehouseOrder::class;
    }
}