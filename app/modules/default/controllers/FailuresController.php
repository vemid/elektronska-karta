<?php

use Vemid\Controller\CrudController;
use \Vemid\Service\Product\Hydrator;
use Vemid\Entity\Repository\ProductSizeRepository;
use \Vemid\Messenger\Message;
use Vemid\Entity\Type;

/**
 * Class FailuresController
 *
 * @package Default\Controllers
 */
class FailuresController extends CrudController
{
    public function listAction()
    {
        $this->tag::appendTitle($this->translator->t("Lista Felera"));

        $failures = $this->entityManager->find(Failure::class, [
            'order' => 'id DESC'
        ]);

        $this->view->setVar('failures', $failures);
    }

    public function getCreateFormAction()
    {
        $data = parent::getCreateFormAction();

        /** @var CodeType $codeType */
        $codeTypeShop = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SHOPS
            ]
        ]);

        /** @var CodeType $codeTypeLocation */
        $codeTypeFailure = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::FAILURE
            ]
        ]);

        if (!$codeTypeShop && !$codeTypeFailure) {
            return $this->returnNotFound();
        }

        /** @var Code[] $shops */
        $shops = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeTypeShop->getId()
            ],
            'order' => 'code ASC'

        ]);

        $shopsData = [];
        foreach ($shops as $shop) {
            $shopsData[$shop->getId()] = $shop->getName();
        }

        /** @var Code[] $failures */
        $failures = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeTypeFailure->getId()
            ]
        ]);
        $failuresData = [];
        foreach ($failures as $failure) {
            $failuresData[$failure->getId()] = $failure->getName();
        }

        $data[Failure::PROPERTY_SHOP_CODE_ID]['options'] = $shopsData;
        $data[Failure::PROPERTY_FAILURE_CODE_ID]['options'] = $failuresData;

        return $data;
    }

    public function getUpdateFormAction($id)
    {
        $data = parent::getUpdateFormAction($id);
        /** @var CodeType $codeType */
        $codeTypeShop = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SHOPS
            ]
        ]);

        /** @var CodeType $codeTypeLocation */
        $codeTypeFailure = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::FAILURE
            ]
        ]);

        if (!$codeTypeShop && !$codeTypeFailure) {
            return $this->returnNotFound();
        }

        /** @var Code[] $shops */
        $shops = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeTypeShop->getId()
            ],
            'order' => 'code ASC'

        ]);

        $shopsData = [];
        foreach ($shops as $shop) {
            $shopsData[$shop->getId()] = $shop->getName();
        }

        /** @var Code[] $failures */
        $failures = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeTypeFailure->getId()
            ]
        ]);
        $failuresData = [];
        foreach ($failures as $failure) {
            $failuresData[$failure->getId()] = $failure->getName();
        }

        $data[Failure::PROPERTY_SHOP_CODE_ID]['options'] = $shopsData;
        $data[Failure::PROPERTY_FAILURE_CODE_ID]['options'] = $failuresData;
        $data[Failure::PROPERTY_DOCUMENT]['attributes'] = ['disabled' => true];
        $data[Failure::PROPERTY_PRODUCT_SIZE_ID]['attributes'] = ['disabled' => true];
        $data[Failure::PROPERTY_SHOP_CODE_ID]['attributes'] = ['disabled' => true];
        $data[Failure::PROPERTY_DATE]['attributes'] = ['disabled' => true];

        return $data;
    }

    public function pivotReportAction($classificationId)
    {
        $this->tag::appendTitle($this->translator->t("Lista Felera"));

        $classificationCode = Type::CLASSIFICATION;

        $query = $this->modelsManager->createBuilder()
            ->from(\Failure::class)
            ->leftJoin(\ProductSize::class,"ps.id = Failure.productSizeId",'ps')
            ->leftJoin(\ProductClassification::class, "pc.productId = ps.productId and pc.entityTypeId = {$classificationCode}", 'pc')
            ->where('pc.entityId = :bindParam:',
                [
                    'bindParam' => $classificationId
                ])
            ;


        /** @var \Failure[] $failureData */
        $failureData = $query->getQuery()->execute();

        /** @var Failure $failures */
        $failures = $this->entityManager->find(Failure::class, [
            'order' => 'id DESC'
        ]);


        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_HEAD_JS)
            ->addJs(APP_PATH . 'assets/js/react-pivot-standalone-3.0.0.min.js');

        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_HEAD_CSS)
            ->addCss(APP_PATH . 'assets/css/react-pivot.css');


        $data = [];
        /** @var Failure $row */
        foreach ($failureData as $row) {

//            /** @var Product $product */
//            $product = $this->entityManager->findOne(Product::class, [
//                Product::PROPERTY_ID . ' = :productId:',
//                'bind' => [
//                    'productId' =>  $row['product']
//                ]
//            ]);
//
//            $hydrator = new Hydrator($product, $this->entityManager);

            $data[] = [
                'product' => $row->getProductSize()->getProduct()->getBigName(),
                'seasson' => $row->getProductSize()->getProductSizeHydrator()->getOldCollection()->getName(),
                'failureType' =>  $row->getFailureCode()->getName(),
                'shop' => $row->getShopCode()->getName(),
                'size' => $row->getProductSize()->getCode()->getName(),
                'realization' => $row->getProcesType(),
                'origin' => $row->getFailureOrigin(),
                'transaction' => [
                    'qty' => $row->getQty(),
                ]
            ];
        }

        $config = $this->getDI()->getConfig();

        $this->view->setVar('rows', json_encode($data));
        $this->view->setVar('siteUrl', ltrim($config->application->baseUri, '/'));
        $this->view->pick('failures/pivot-report');

    }

    public function pivotReportTestAction()
    {
        $this->tag::appendTitle($this->translator->t("Lista Felera"));

        $classificationCode = Type::CLASSIFICATION;

        $test = 48488;

        $query = $this->modelsManager->createBuilder()
            ->from(\Failure::class)
            ->leftJoin(\ProductSize::class,"ps.id = Failure.productSizeId",'ps')
            ->leftJoin(\ProductClassification::class, "pc.productId = ps.productId and pc.entityTypeId = {$classificationCode}", 'pc')
            ->where('pc.entityId = :bindParam:',
                [
                    'bindParam' => $test
                ])
        ;


        /** @var \Failure[] $failureData */
        $failureData = $query->getQuery()->execute();



        $data = [];
        /** @var Failure $row */
        foreach ($failureData as $row) {
            $data[] = [
                'product' => $row->getProductSize()->getProduct()->getBigName(),
                'seasson' => $row->getProductSize()->getProductSizeHydrator()->getOldCollection()->getName(),
                'failureType' =>  $row->getFailureCode()->getName(),
                'shop' => $row->getShopCode()->getName(),
                'size' => $row->getProductSize()->getCode()->getName(),
                'realization' => $row->getProcesType(),
                'origin' => $row->getFailureOrigin(),
                'transaction' => [
                    'qty' => $row->getQty(),
                ]
            ];
        }

        $json = json_encode($data);

        $tempFolder = rtrim(sys_get_temp_dir(), '/') . '/pivot-report';
        if (!is_dir($tempFolder) && !mkdir($tempFolder, 0700)) {
            throw new \LogicException('Dir can not be created!');
        }
        $dir = $tempFolder;

        header('Content-Type: application/json');
        $file = fopen($tempFolder . '/report.json','w');


        echo json_encode($data);



    }

    public function testPivotAction()
    {
        $this->tag::appendTitle($this->translator->t("Lista Felera"));

        $classificationCode = Type::CLASSIFICATION;

        $test = 48488;

        $query = $this->modelsManager->createBuilder()
            ->from(\Failure::class)
        ;


        /** @var \Failure[] $failureData */
        $failureData = $query->getQuery()->execute();



        $data = [];
        /** @var Failure $row */
        foreach ($failureData as $row) {
            $data[] = [
                'Proizvod' => $row->getProductSize()->getProduct()->getBigName(),
                'Sezona' => $row->getProductSize()->getProductSizeHydrator()->getOldCollection()->getName(),
                'Tip_Felera' =>  $row->getFailureCode()->getName(),
                'Objekat' => $row->getShopCode()->getName(),
                'Velicina' => $row->getProductSize()->getCode()->getName(),
                'Realizacija' => $row->getProcesType(),
                'Poreklo' => $row->getFailureOrigin(),
                'Kolicina' => $row->getQty(),
            ];
        }

//        $path = APP_PATH . 'var/uploads/files/';
//
//        $fp = fopen($path.'report.csv', 'w+');
//        $header=null;
//        foreach ($data as $fields) {
//            if(!$header) {
//
//                fputcsv($fp,array_keys($fields));
//                fputcsv($fp, $fields);   // do the first row of data too
//                $header = true;
//            }
//            else {
//
//                fputcsv($fp, $fields);
//            }
//
//        }
//
//        fclose($fp);

        $out = array_values($data);


        print_r($out);

        $this->view->setVar('data', json_encode($out));
        $this->view->pick('failures/test-pivot');
    }

    public function choseSeasonPivotReportAction()
    {
        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class,[
            CodeType::PROPERTY_CODE . ' = :type:',
            'bind' => [
                'type' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        /** @var $code */
        $code = $this->entityManager->findOne(\Code::class,[
            Code::PROPERTY_CODE_TYPE_ID . ' = :typeCode: AND ' .
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => 13,
                'typeCode' => $codeType->getId()
            ]
        ]);

        /** @var Classification $classificationDoubleSeasons */
        $classificationDoubleSeasons = $this->entityManager->find(Classification::class,[
            Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . '= :typeCode: ',
            'bind' => [
                'typeCode' => $code->getEntityId()
            ]
        ]);



        $doubleSeasons = [];
        /** @var Classification $classificationDoubleSeason */
        foreach ($classificationDoubleSeasons as $classificationDoubleSeason) {
            $doubleSeasons[] = [
                'id' => $classificationDoubleSeason->getId(),
                'type' => $classificationDoubleSeason->getClassificationCategory()->getName(),
                'code' => $classificationDoubleSeason->getCode(),
                'name' => $classificationDoubleSeason->getName(),
            ];
        }

        //echo "fsdfsd";

        $this->view->setVar('doubleSeasons', $doubleSeasons);
        $this->view->pick('failures/chose-season-pivot-report');
    }

    public function getEntityName()
    {
        return \Failure::class;
    }
}