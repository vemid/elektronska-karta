<?php

use Vemid\Controller\CrudController;
use Vemid\Service\Product\Price;
use \Vemid\Service\Product\Hydrator;
use Vemid\Form\Renderer\Json as Renderer;
use \Vemid\Messenger\Message;

/**
 * Class ProductCalculationsController
 *
 * @package Frontemd\Controllers
 */

class ProductCalculationsController extends CrudController
{
    public function listAction()
    {
//        $this->view->setVar('productCalculations', $this->entityManager->find(ProductCalculation::class,[
//            'order' => 'id DESC'
//        ]));
//        $this->view->pick('product-calculations/list');
    }

    public function getCreateFormAction()
    {
        $renderer = new Renderer();
        $form = $this->getForm();
        $text = new \Phalcon\Forms\Element\Text('productIdCalculations');
        $text->setLabel('Product');
        $text->setAttribute('class', 'required form-control');

        $form->add($text, 'status', true);

        return $renderer->render($form);
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $productId = $this->request->getPost('productId');

        /** @var Product $product */
        if (!$product = $this->entityManager->findOne(Product::class, $productId)) {
            return $this->returnNotFound();
        }

        $query = $this->modelsManager->createBuilder()
            ->addFrom(CalculationMapping::class, 'cm')
            ->leftJoin(ProductClassification::class, 'cm.classificationId = pc.entityId', 'pc')
            ->leftJoin(Product::class, 'pc.productId = p.id', 'p')
            ->where('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => \Vemid\Entity\Type::CLASSIFICATION
            ])
            ->andWhere('cm.id IS NOT NULL')
            ->andWhere('p.id = :id:', [
                'id' => $product->getId()
            ]);
        $calculationMappings = $query->getQuery()->execute();

        if ($calculationMappings->count() > 1) {
            throw new LogicException($this->_translate('More than one calculation mappings!'));
        }

        $productRepository = new \Vemid\Entity\Repository\ProductRepository($this->modelsManager);
        $productTotal = $productRepository->getOrderTotal($product);
        $qty = $productTotal['komada'] ? $productTotal['komada'] : 0;


        /** @var CalculationMapping|null $calculationMapping */
        $calculationMapping = $calculationMappings->getFirst();

        if (!$calculationMapping) {
            throw new LogicException($this->_translate('Calculation mapping not found!'));
        }

        $check = $this->entityManager->findOne(ProductCalculation::class,[
            ProductCalculation::PROPERTY_PRODUCT_ID.'= :productId: AND ' .
            ProductCalculation::PROPERTY_STATUS.'= :status:',
            'bind' => [
               'productId' => $product->getId(),
               'status' => $this->request->getPost('status'),
            ]]);

        if(!$check) {

            $productCalculation = new ProductCalculation();
            $productCalculation->setProduct($product);
            $productCalculation->setCalculationMapping($calculationMapping);
            $productCalculation->setStatus($this->request->getPost('status'));
            $productCalculation->setQty($qty);
            $productCalculation->setComposition($this->request->getPost('composition'));
            $productCalculation->setCreated((new \Vemid\Date\DateTime())->getUnixFormat());
            if (!$this->entityManager->save($productCalculation)) {
                $this->addFlashMessagesFromEntity($productCalculation);
                return;
            }

            $this->dispatcher->setParam('url', '/product-calculation-items/list/' . $productCalculation->getId());

            $this->addFlashMessage(
                $this->_translate('Uspešno kreirana kalkulacija'),
                null,
                \Vemid\Messenger\Message::SUCCESS
            );
        }
        else {
            $this->addFlashMessage("Ne moze se snimiti jer vec postoji kalkulacija sa statusom ".$this->request->getPost('status')."!",null,Message::DANGER);
        }
    }

    public function addItemsAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        parse_str($this->request->getRawBody(), $postData);

        /** @var ProductCalculation $productCalculation */
        if (!$productCalculation = $this->entityManager->findOne(ProductCalculation::class, $postData['productCalculationId'])) {
            return $this->returnNotFound();
        }

        /** @var Product $product */
        $product = $productCalculation->getProduct();
        $productRepository = new \Vemid\Entity\Repository\ProductRepository($this->modelsManager);
        $productTotal = $productRepository->getOrderTotal($product);
        $qty = $postData['qty'];

        if (!$postData['eurWholesalePrice']) {
            $this->addFlashMessage(
                $this->_translate('EUR cena ne sme biti nula'),
                null,
                \Vemid\Messenger\Message::DANGER
            );
            return;
        }

        if (!empty($postData['create'])) {
            $productCalculation->setComposition($postData['composition']);
            $productCalculation->getProduct()->setOriginCodeId($postData['origin'] == '0' ? null : $postData['origin']);
            $productCalculation->setQty($qty);
            $this->entityManager->save($productCalculation);
            foreach ($postData['create'] as $rows) {
                $productCalculationItem = new ProductCalculationItem();
                $productCalculationItem->setProductCalculation($productCalculation);
                foreach ($rows as $property => $value) {
                    $productCalculationItem->setProperty($property, str_replace(',','.',$value));
                }

                if (!$this->entityManager->save($productCalculationItem)) {
                    $this->addFlashMessagesFromEntity($productCalculationItem);
                }
            }
        }

        if (!empty($postData['edit'])) {
            $productCalculation->setCompleted(false);
            $productCalculation->setIsSynced(false);
            $productCalculation->setComposition($postData['composition']);
            $productCalculation->getProduct()->setOriginCodeId($postData['origin'] == '0' ? null : $postData['origin']);
            $productCalculation->setQty($qty);
            $this->entityManager->save($productCalculation);

            foreach ($postData['edit'] as $id => $properties) {
                if (!$productCalculationItem = $this->entityManager->findOne(ProductCalculationItem::class, $id)) {
                    continue;
                }

                foreach ($properties as $property => $value) {
                    $productCalculationItem->setProperty($property, str_replace(',','.',$value));
                }

                if (!$this->entityManager->save($productCalculationItem)) {
                    $this->addFlashMessagesFromEntity($productCalculationItem);
                }
            }
        }

        $product->setComposition($postData['composition']);
        $this->entityManager->save($product);


        /** @var \ProductCalculation $productCalculation */
        $addPrices = new Price($this->entityManager, $productCalculation, $postData['eurWholesalePrice']);
        $addPrices->addPrices();

        $this->addFlashMessage(
            'Uspešno sačuvane cene!',
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function getSimilarProductAction($id)
    {
        /** @var \ProductCalculation $similarProductCalculation */
        if (!$similarProductCalculation = $this->entityManager->findOne(ProductCalculation::class, $id)) {
            return $this->returnNotFound();
        }

        $hydrator = new Hydrator($similarProductCalculation->getProduct(), $this->entityManager);
        $price = new Price($this->entityManager, $similarProductCalculation, null);

        $this->view->setVar('similarProductCalculationItems',$similarProductCalculation->getProductCalculationItems());
        $this->view->setVar('similarOldCollection', $hydrator->getOldCollection());
        $this->view->setVar('productPrices', $price->getPrices($similarProductCalculation->getProduct()));
        $this->view->setVar('similarProductCalculation',$similarProductCalculation);
        $this->view->pick('product-calculations/similar-product');

    }

    public function findProductAction()
    {
        $this->view->disable();
        $json = [];
        $term = $this->request->getQuery('term');

        $query = $this->modelsManager->createBuilder()
            ->addFrom(Product::class, 'p')
            ->leftJoin(ProductClassification::class, 'p.id = pc.productId', 'pc')
            ->leftJoin(CalculationMapping::class, 'pc.entityId = cm.classificationId', 'cm')
            ->Where('p.code LIKE :code: OR p.name LIKE :code:', [
                'code' => "%$term%"
            ]);

        /** @var Product[] $products */
        $products = $query->getQuery()->execute();

         foreach ($products as $product) {
             $id = $product->getId();
             $json[$id]['id'] = $id;
             $json[$id]['value'] = $product->getName();
             $json[$id]['label'] = $product->getName() . ' (' . $product->getCode() . ')';
         }

        $this->response->setContent(json_encode($json));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getListDataAction() {
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');

        $query = $this->modelsManager->createBuilder();

        $query->addFrom(ProductCalculation::class, 'pc')
            ->innerJoin(Product::class, 'pc.productId = p.id', 'p');

        if (is_array($search) && $search['value']) {
            $query->andWhere('p.code LIKE :search: OR p.name LIKE :search:', [
                'search' => '%' . $search['value'] . '%'
            ]);
        }

        $query->orderBy('pc.id desc');

        $queryForCount = clone $query;

        /** @var ProductCalculation[] $allProductCalculations */
        $allProductCalculations = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);

        /** @var ProductCalculation[] $productCalculations */
        $productCalculations = $query->getQuery()->execute();

        $data = [];
        foreach ($productCalculations as $productCalculation) {

            $data[] = [
                (string)$productCalculation->getProduct()->getName(),
                (string)$productCalculation->getProduct()->getCode(),
                $productCalculation->getCalculationMapping()->getMarkUp() *100 ." %",
                $productCalculation->getCalculationMapping()->getEurCourse(),
                $productCalculation->getStatus(),
                (string)$productCalculation->getProduct()->getImagePath(),
                $productCalculation->getId(),
            ];
        }

        $count = $allProductCalculations->count();
        $responseData = [
            'draw' => $page,
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();

    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return ProductCalculation::class;
    }
}