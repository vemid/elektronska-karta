<?php

use Vemid\Controller\CrudController;
use \Phalcon\Validation\Validator\File;
use \Vemid\Form\Renderer\Json as Renderer;
use \Vemid\Date\DateTime;
use \Vemid\Entity\Type;
use  \Vemid\Services\Image\ImageManager;

/**
 * Class ElectronCardsController
 *
 * @package Default\Controllers
 */
class ElectronCardsController extends CrudController
{
    public function listAction()
    {
        $this->view->setVar('electronCards', $this->entityManager->find(ElectronCard::class));
    }

    public function getCreateFormAction()
    {
        $data = parent::getCreateFormAction();
        $params = $this->dispatcher->getParams();

        /** @var Product $product */
        if ($product = $this->entityManager->findOne(Product::class, $params[1])) {
            if (isset($data[ElectronCard::PROPERTY_PRODUCT_ID])) {
                $data[ElectronCard::PROPERTY_PRODUCT_ID]['default'] = $params[1];
            }
        }

        return $data;
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $form = $this->getForm();
        $postData = $this->request->getPost();
        /** @var ElectronCard $electronCard */
        $electronCard = $form->getEntity();

        $validation = new Phalcon\Validation();
//        $validation->add(ElectronCard::PROPERTY_IMAGE, new File([
//            'allowedTypes' => [
//                'image/jpeg',
////                'image/jpg',
////                'image/JPG',
//                'image/png',
//            ],
//        ]));
//
        $messages = [];
        if (count($messages)) {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('File mora biti slika!'),
                ElectronCard::PROPERTY_IMAGE,
                \Vemid\Messenger\Message::SUCCESS
            );

            return;
        }

        /** @var Phalcon\Http\Request\File $file */
        $file = array_pop($this->request->getUploadedFiles());

        if ($file !== null) {
            $uploadPath = $electronCard->getUploadPath();

            $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();
            if ($file->moveTo($filePath)) {
                if (is_file($uploadedFile = $uploadPath . $electronCard->getImage())) {
                    @unlink($uploadedFile);
                }

                $postData[ElectronCard::PROPERTY_IMAGE] = basename($filePath);
            }
        }

        /** @var ElectronCard[] $electronCards */
        $electronCards = $this->entityManager->find(ElectronCard::class, [
            ElectronCard::PROPERTY_PRODUCT_ID . ' = :productId:',
            'bind' => [
                'productId' => $postData[ElectronCard::PROPERTY_PRODUCT_ID]
            ]
        ]);

        foreach ($electronCards as $electronCardPrev) {
            $electronCardPrev->setActive(0);
            $electronCardPrev->setFinished(0);
            $electronCardPrev->setFinished(null);
            $this->entityManager->save($electronCardPrev);
        }

        $postData[ElectronCard::PROPERTY_ACTIVE] = 1;
        $postData[ElectronCard::PROPERTY_VERSION] = $electronCards->count() + 1;
        $postData[ElectronCard::PROPERTY_FINISHED] = 0;

        if (!$form->isValid($postData)) {
            $this->addFlashMessagesFromForm($form);
            return;
        }

        $form->synchronizeEntityData();
        $this->saveEntity($form->getEntity());
        $this->dispatcher->setParam('url', '/electron-cards/overview/' . $form->getEntity()->getEntityId());

        return ['error' => false, 'messages' => []];
    }

    public function overviewAction($id)
    {
        /** @var ElectronCard $electronCard */
        if (!$electronCard = $this->entityManager->findOne(ElectronCard::class, $id)) {
            $this->returnNotFound();
        }
        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_FOOT_JS)
            ->addJs(APP_PATH . 'assets/js/drop-zone.js');

        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_HEAD_CSS)
            ->addCss(APP_PATH . 'assets/css/drop-zone.css');


        $product = $electronCard->getProduct();
        $hydrator = new \Vemid\Service\Product\Hydrator($product, $this->entityManager);

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var Code $code */
        $code = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'codeTypeId' => $codeType->getId(),
                'code' => 2
            ]
        ]);

        $userSectors = [];
        foreach ($this->currentUser->getUserSectors() as $userSector) {
            $userSectors[] = $userSector->getCodeId();
        }

        $designer = $electronCard->getEntityLogOnCreate() ? $electronCard->getEntityLogOnCreate()->getUser() : '';
        $designerTime = $electronCard->getEntityLogOnCreate() ? $electronCard->getEntityLogOnCreate()->getTimestamp() : '';

        $this->view->setVar('userSectors', $userSectors);
        $this->view->setVar('electronCard', $electronCard);
        $this->view->setVar('hydrator', $hydrator);
        $this->view->setVar('display', $designer ? sprintf('%s - %s', $designer, $designerTime ? $designerTime->getShortSerbianFormat() : '') : '');
        $this->view->setVar('molder', $code);
    }

    public function addFactoryFormAction($id, $type, $relationType = null, $factoryId = null)
    {
        /** @var ElectronCard $electronCard */
        $electronCard = $this->entityManager->findOne(ElectronCard::class, $id);
        $electronCardFactory = $this->entityManager->findOne(ElectronCardFactory::class, $factoryId);

        if (!$electronCard || !defined("ElectronCardFactory::{$type}")) {
            return $this->returnNotFound();
        }

        if ($relationType === 'factory') {
            $form = new ElectronCardFactoryFactoryForm(null, ['electronCard' => $electronCard, 'type' => $type, 'electronCardFactory' => $electronCardFactory]);
        } elseif ($relationType === 'sketch') {
            $form = new ElectronCardFactoryForm(null, ['electronCard' => $electronCard, 'type' => $type, 'electronCardFactory' => $electronCardFactory]);
        } else {
            $this->addFlashMessage(
                'Unknown form type',
                null,
                \Vemid\Messenger\Message::DANGER
            );

            return;
        }

        return (new Renderer())->render($form);
    }

    public function addFactoryAction($id, $type, $relationType = null, $factoryId = null)
    {
        /** @var ElectronCard $electronCard */
        $electronCard = $this->entityManager->findOne(ElectronCard::class, $id);

        /** @var ElectronCardFactory $existingElectronCardFactory */
        $existingElectronCardFactory = $this->entityManager->findOne(ElectronCardFactory::class, $factoryId);

        if (!$electronCard || !defined("ElectronCardFactory::{$type}") || !$this->request->isPost()) {
            return $this->returnNotFound();
        }

        if (!$relationType && $existingElectronCardFactory) {
            $relationType = $existingElectronCardFactory->getMaterialSketch() ? 'sketch' : 'factory';
        }

        $postData = $this->request->getPost();

        if ($relationType === 'factory') {
            $form = new ElectronCardFactoryFactoryForm(new ElectronCardFactory(), ['electronCard' => $electronCard, 'type' => $type, 'electronCardFactory' => $existingElectronCardFactory]);

            $materialFactory = $this->entityManager->findOne(MaterialFactory::class, $postData[ElectronCardFactory::PROPERTY_MATERIAL_FACTORY_ID]);
            if ($materialFactory) {
                $postData[ElectronCardFactory::PROPERTY_NAME] = $materialFactory->getDisplayName();
            }
        }

        if ($relationType === 'sketch') {
            $form = new ElectronCardFactoryForm(new ElectronCardFactory(), ['electronCard' => $electronCard, 'type' => $type, 'electronCardFactory' => $existingElectronCardFactory]);
        }

        if ($relationType === null) {
            $this->addFlashMessage(
                'Unknown form type',
                null,
                \Vemid\Messenger\Message::DANGER
            );

            return;
        }

        /** @var Classification $classification */
        $classification = $this->entityManager->findOne(Classification::class, [
            Classification::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $postData['subMaterialCodeId']
            ]
        ]);

        $postData[ElectronCardFactory::PROPERTY_CLASSIFICATION_CODE_ID] = $classification ? $classification->getId() : null;

        if (!$form->isValid($postData)) {
            $this->addFlashMessagesFromForm($form);
            return;
        }

        $form->synchronizeEntityData();
        /** @var ElectronCardFactory $electronCardFactory */
        $electronCardFactory = $form->getEntity();
        if (!$this->entityManager->save($electronCardFactory)) {
            $this->addFlashMessagesFromEntity($electronCardFactory);
            return;
        }

        if ($existingElectronCardFactory) {
            $this->entityManager->delete($existingElectronCardFactory);
        }

        if ($relationType === 'sketch') {
            $materialSketch = new MaterialSketch();
            $materialSketchForm = $materialSketch->getForm();

            if (!$materialSketchForm->isValid($postData)) {
                $this->addFlashMessagesFromForm($materialSketchForm);
                return;
            }

            $materialSketchForm->synchronizeEntityData();
            if (!$this->entityManager->save($materialSketch)) {
                $this->addFlashMessagesFromEntity($materialSketch);
                return;
            }

            $electronCardFactory->setMaterialSketch($materialSketch);
            if (!$this->entityManager->save($electronCardFactory)) {
                $this->addFlashMessagesFromEntity($electronCardFactory);
                return;
            }

            if ($existingElectronCardFactory && $existingMaterialSketch = $existingElectronCardFactory->getMaterialSketch()) {
                $this->entityManager->delete($existingMaterialSketch);
            }
        }

        if ($relationType === 'factory') {

        }

        $this->addFlashMessage(
            '-----------------------',
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function editNoteFormAction($id)
    {
        /** @var ElectronCard $electronCard */
        $electronCard = $this->entityManager->findOne(ElectronCard::class, $id);

        if (!$electronCard) {
            return $this->returnNotFound();
        }

        $form = new ElectronCardEditNoteForm($electronCard);

        return (new Renderer())->render($form);
    }

    public function editNoteAction($id)
    {
        /** @var ElectronCard $electronCard */
        $electronCard = $this->entityManager->findOne(ElectronCard::class, $id);

        if (!$electronCard) {
            return $this->returnNotFound();
        }

        $form = new ElectronCardEditNoteForm($electronCard);

        if ($form->isValid($this->request->getPost())) {
            $form->synchronizeEntityData();
            $this->saveEntity($form->getEntity());
        } else {
            $this->addFlashMessagesFromForm($form);
        }
    }

    public function printAction($id)
    {
        $this->view->disable();

        /** @var ElectronCard $electronCard */
        $electronCard = $this->entityManager->findOne(ElectronCard::class, $id);

        if (!$electronCard) {
            return $this->returnNotFound();
        }

        if (!$product = $electronCard->getProduct()) {
            return $this->returnNotFound();
        }

        $hydrator = new \Vemid\Service\Product\Hydrator($product, $this->entityManager);

        /** @var object $config */
        $config = $this->getDI()->getConfig();

        $html = $this->getDI()->getSimpleView()->render('electron-cards/print', [
            'electronCard' => $electronCard,
            'hydrator' => $hydrator,
            'date' => new Vemid\Date\DateTime(),
            'siteUrl' => ltrim($config->application->baseUri, '/')
        ]);

        $renderer = $this->getDI()->getPdfRenderer();
        $renderer->setPageSize('210mm', '297mm');


        $content1 = $renderer->render($html, 'landscape');


        $html = $this->getDI()->getSimpleView()->render('electron-cards/print-second-page', [
            'electronCard' => $electronCard,
            'siteUrl' => ltrim($config->application->baseUri, '/')
        ]);

        $content2 = $renderer->render($html, 'landscape');
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ']', ','], '-', $electronCard->getProduct()->getName());

        $pdfOne = sprintf(
            '%s/%s1.pdf',
            rtrim(sys_get_temp_dir(), '/'),
            'tmp'
        );

        $pdfTwo = sprintf(
            '%s/%s2.pdf',
            rtrim(sys_get_temp_dir(), '/'),
            'tmp'
        );

        file_put_contents($pdfOne, $content1);
        file_put_contents($pdfTwo, $content2);

        $pdfs = [$pdfOne /*, $pdfTwo */];

        $cmd = 'gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=- ' . implode(' ', $pdfs);
        $result = shell_exec($cmd);

        unlink($pdfOne);
        unlink($pdfTwo);

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');
        header('Content-Disposition: attachment; filename="' . $name . '.pdf"');

        print $result;
        exit;
    }

    public function editBaseNameFormAction($id)
    {
        /** @var ElectronCard $electronCard */
        $electronCard = $this->entityManager->findOne(ElectronCard::class, $id);

        if (!$electronCard) {
            return $this->returnNotFound();
        }

        $form = new ElectronCardEditBaseNameForm($electronCard);

        return (new Renderer())->render($form);
    }

    public function editBaseNameAction($id)
    {
        /** @var ElectronCard $electronCard */
        $electronCard = $this->entityManager->findOne(ElectronCard::class, $id);

        if (!$electronCard) {
            return $this->returnNotFound();
        }

        $form = new ElectronCardEditBaseNameForm($electronCard);

        $post[ElectronCard::PROPERTY_BASE_PRODUCT_ID] = $post[ElectronCard::PROPERTY_BASE_PRODUCT_ID] ?? null;

        if ($form->isValid($this->request->getPost())) {
            $this->saveEntity($electronCard);
        } else {
            $this->addFlashMessagesFromForm($form);
        }
    }

    public function uploadElectronCardImagesAndDescriptionAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var ElectronCard $electronCard */
        $electronCard = $this->entityManager->findOne(ElectronCard::class, $this->request->getPost('electronCardId'));

        if (!$electronCard) {
            return $this->returnNotFound();
        }

        /** @var Phalcon\Http\Request\File $file */
        $file = array_pop($this->request->getUploadedFiles());
        $postData = $this->request->getPost();

        if ($file !== null) {
            $uploadPath = $electronCard->getUploadPath();

            $imageManager = new ImageManager($file, $uploadPath);
            $imagePath = $imageManager->uploadImage($file->getKey() === 'image' ? 1400 : 960);
            $imageManager->unlinkPreviousImage($electronCard->getProperty($file->getKey()));

            $electronCard->setProperty($file->getKey(), basename($imagePath));
            foreach ($postData as $property => $value) {
                $electronCard->setProperty($property, $value);
            }

            if (!$this->entityManager->save($electronCard)) {
                $this->addFlashMessagesFromEntity($electronCard);

                return;
            }

            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('Uspešno snimljena slika!'),
                null,
                \Vemid\Messenger\Message::SUCCESS
            );

            return;
        }
    }

    public function finishAction($id)
    {
        /** @var ElectronCard $electronCard */
        $electronCard = $this->entityManager->findOne(ElectronCard::class, $id);

        if (!$electronCard) {
            $this->returnNotFound();
        }

        if ($electronCard->getFinished()) {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('Već je karta završena!'),
                null,
                \Vemid\Messenger\Message::WARNING
            );

            return;
        }

        if (!$electronCard->getActive()) {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('Nemoguće je odraditi akciju pošto je karta stornirana!'),
                null,
                \Vemid\Messenger\Message::DANGER
            );

            return;
        }

        $electronCard->setFinished(true);
        $electronCard->setCardClosedTime(new DateTime());

        if (!$this->entityManager->save($electronCard)) {
            $this->addFlashMessagesFromEntity($electronCard);

            return;
        }

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t('Uspešno je zatvorena karta!'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function cancelAction($id)
    {
        /** @var ElectronCard $electronCard */
        $electronCard = $this->entityManager->findOne(ElectronCard::class, $id);

        if (!$electronCard) {
            return $this->returnNotFound();
        }

        if (!$electronCard->getActive()) {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('Nemoguće je odraditi akciju pošto je karta već stornirana!'),
                null,
                \Vemid\Messenger\Message::DANGER
            );

            return;
        }

        $electronCard->setActive(false);
        $electronCard->setFinished(false);
        $electronCard->setCardClosedTime(new DateTime());

        if (!$this->entityManager->save($electronCard)) {
            $this->addFlashMessagesFromEntity($electronCard);

            return;
        }

        $oldData = $electronCard->toArray();
        unset($oldData['id']);
        unset($oldData['version']);
        unset($oldData['cardClosedTime']);
        unset($oldData['created']);
        $oldData['version'] = ($electronCard->getVersion())+1;
        $oldData['active'] = true;
        $oldData['finished'] = false;
        $oldData['created'] = (new DateTime())->getUnixFormat();
        $newRecord = new ElectronCard();

        if (!$newRecord->save($oldData)) {
            $this->addFlashMessagesFromEntity($newRecord);

            return;
        }

        $oldElectronFactories = $electronCard->getElectronCardFactories()->toArray();
        foreach ($oldElectronFactories as $oldElectronFactory) {
            $newElectronCardFactory = new ElectronCardFactory();
            unset($oldElectronFactory['id']);
            unset($oldElectronFactory['electronCardId']);
            unset($oldElectronFactory['created']);
            $oldElectronFactory['electronCardId'] = $newRecord->getId();
            $oldElectronFactory['created'] = (new DateTime())->getUnixFormat();
            $newElectronCardFactory->save($oldElectronFactory);

        }

//        print_r($newRecord->getId());
//        $oldData[]
      $this->dispatcher->setParam('url', '/electron-cards/overview/' . $newRecord->getId());

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t('Uspešno je zatvorena karta!'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function activateAction($id)
    {
        /** @var ElectronCard $electronCard */
        $electronCard = $this->entityManager->findOne(ElectronCard::class, $id);

        if (!$electronCard) {
            return $this->returnNotFound();
        }

        if ($electronCard->getFinished()) {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('Hm, ovo je problem. Karta je već završena, stoga nemoguce je biti ovde!'),
                null,
                \Vemid\Messenger\Message::DANGER
            );

            return;
        }

        if ($electronCard->getActive()) {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('Nemoguće je odraditi akciju pošto je karta već aktivna!'),
                null,
                \Vemid\Messenger\Message::WARNING
            );

            return;
        }

        $electronCard->setActive(true);

        if (!$this->entityManager->save($electronCard)) {
            $this->addFlashMessagesFromEntity($electronCard);

            return;
        }

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t('Uspešno je zatvorena karta!'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function getForm(\Vemid\Entity\EntityInterface $entity = null, array $exclude = [])
    {
        $exclude = array_merge($exclude, [ElectronCard::PROPERTY_ADDITIONAL_SKETCH_DESCRIPTION, ElectronCard::PROPERTY_ADDITIONAL_SKETCH_IMAGE, ElectronCard::PROPERTY_ADDITIONAL_SKETCH_SECOND_DESCRIPTION, ElectronCard::PROPERTY_ADDITIONAL_SKETCH_SECOND_IMAGE]);

        return parent::getForm($entity, $exclude); // TODO: Change the autogenerated stub
    }

    public function listNoneMaterialFactoryAction()
    {
        $this->view->setVar('form', new ElectronCardFilterForm());
        $this->view->pick('electron-cards/list-none-material-factory');
    }

    public function getNoneMaterialFactoryDataAction()
    {
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');
        $oldCollectionId = $this->request->getQuery('oldCollectionId', 'string', '');
        $genderClassificationId = $this->request->getQuery('genderClassificationId', 'int', 0);
        $groupModelId = $this->request->getQuery('groupModelId', 'int', 0);

        $genders = [];
        /** @var Classification $classification */
        if ($genderClassificationId && $classification = $this->entityManager->findOne(Classification::class, $genderClassificationId)) {
            /** @var Classification[] $genderClassifications */
            $genderClassifications = $this->entityManager->find(Classification::class, [
                'name = :name:',
                'bind' => [
                    'name' => $classification->getName()
                ]
            ]);

            foreach ($genderClassifications as $genderClassification) {
                $genders[] = $genderClassification->getId();
            }
        }

        $groupModels = [];
        /** @var Classification $classification */
        if ($groupModelId && $classification = $this->entityManager->findOne(Classification::class, $groupModelId)) {

            /** @var ElectronCard[] $electronCards */
            $query = $this->modelsManager->createBuilder()
                ->addFrom(Classification::class, 'c')
                ->leftJoin(Classification::class, 'cp.id = c.parentClassificationId', 'cp')
                ->where('c.name = :name:', [
                    'name' => $classification->getName()
                ])
                ->andWhere('c.name != cp.name');

            /** @var Classification[] $groupModelClassifications */
            $groupModelClassifications = $query->getQuery()->execute();

            foreach ($groupModelClassifications as $groupModelClassification) {
                $groupModels[] = $groupModelClassification->getId();
            }
        }

        /** @var ElectronCard[] $electronCards */
        $query = $this->modelsManager->createBuilder()
            ->addFrom(ElectronCard::class, 'el')
            ->leftJoin(Product::class, 'el.productId = p.id', 'p')
            ->leftJoin(ProductClassification::class, 'p.id = pc.productId', 'pc')
            ->leftJoin(Classification::class, 'c.id = pc.entityId', 'c')
            ->where('el.active = :active:', [
                'active' => true
            ])
            ->andWhere('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => Type::CLASSIFICATION
            ]);

        if (is_array($search) && $search['value']) {
            $query->andWhere('p.code LIKE :search: OR p.name LIKE :search:', [
                'search' => '%' . trim($search['value']) . '%'
            ]);
        }

        $queryCodes = [];
        $filter = 0;
        if ($oldCollectionId) {
            /** @var Classification $oldCollection */
            $oldCollection = $this->entityManager->findOne(Classification::class, [
                'code = :code:',
                'bind' => [
                    'code' => $oldCollectionId
                ]
            ]);

            if ($oldCollection) {
                $filter++;
                $queryCodes[] = $oldCollection->getId();
            }
        }

        if (count($genders)) {
            $filter++;
            $queryCodes = array_merge($queryCodes, $genders);
        }

        if (count($groupModels)) {
            $filter++;
            $queryCodes = array_merge($queryCodes, $groupModels);
        }

        if (count($queryCodes)) {
            $query->andWhere('pc.entityId IN ({codeQuery:array})', [
                'codeQuery' => $queryCodes
            ]);

            $query->having('COUNT(pc.entityId) >= ' . $filter);
        }

        $query->groupBy('el.id');
        $query->orderBy('el.id DESC');

        $queryForCount = clone $query;
        $allElectronCards = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);
        $electronCards = $query->getQuery()->execute();

        $data = [];
        foreach ($electronCards as $electronCard) {
            $piecesBasic = '';
            $basicElectronCardFactories = $electronCard->getElectronCardFactories([
                'type = :type:',
                'bind' => [
                    'type' => ElectronCardFactory::BASIC
                ]
            ]);

            if ($basicElectronCardFactories->count()) {
                foreach ($basicElectronCardFactories as $basicElectronCardFactory) {
                    $relationType = $basicElectronCardFactory->getMaterialSketch() ? 'sketch' : 'factory';
                    $editBasic = '';

                    if ($relationType === 'sketch') {
                        $editBasic .= sprintf(
                            '<a href="#" class="pull-right text-success" onclick="getForm(\'/electron-cards/add-factory-form/%d/BASIC/factory/%d\', \'/electron-cards/add-factory/%d/BASIC/factory/%d\', event)"><i class="fa fa-edit"></i></a>',
                            $electronCard->getId(),
                            $basicElectronCardFactory->getId(),
                            $electronCard->getId(),
                            $basicElectronCardFactory->getId()
                        );
                    }

                    $editBasic .= sprintf(
                        '<a href="#" class="pull-right m-r-xs" onclick="getForm(\'/electron-cards/add-factory-form/%d/BASIC/%s/%d\', \'/electron-cards/add-factory/%d/BASIC/%s/%d\', event)"><i class="fa fa-pencil"></i></a>',
                        $electronCard->getId(),
                        $relationType,
                        $basicElectronCardFactory->getId(),
                        $electronCard->getId(),
                        $relationType,
                        $basicElectronCardFactory->getId()
                    );

                    $piecesBasic .= sprintf(
                        '<div class="bd-ud alert-%s">- %s%s</div>',
                        $basicElectronCardFactory->getMaterialFactoryId() ? 'success' : 'danger',
                        $basicElectronCardFactory->getName(),
                        $basicElectronCardFactory->getMaterialFactory() ? '' : $editBasic
                    );
                }
            }

            $piecesBasic .= sprintf(
                '<a href="#" class="pull-right" onclick="getForm(\'/electron-cards/add-factory-form/%d/BASIC/factory\', \'/electron-cards/add-factory/%d/BASIC/factory\', event)"><i class="fa fa-pencil"></i></a>',
                $electronCard->getId(),
                $electronCard->getId()
            );

            $stringElectronCardFactories = $electronCard->getElectronCardFactories([
                'type = :type:',
                'bind' => [
                    'type' => ElectronCardFactory::STRING
                ]
            ]);

            $piecesString = '';
            if ($stringElectronCardFactories->count()) {
                foreach ($stringElectronCardFactories as $stringElectronCardFactory) {
                    $relationType = $stringElectronCardFactory->getMaterialSketch() ? 'sketch' : 'factory';
                    $editString = '';

                    if ($relationType === 'sketch') {
                        $editString .= sprintf(
                            '<a href="#" class="pull-right text-success" onclick="getForm(\'/electron-cards/add-factory-form/%d/STRING/factory/%d\', \'/electron-cards/add-factory/%d/STRING/factory/%d\', event)"><i class="fa fa-edit"></i></a>&nbsp;',
                            $electronCard->getId(),
                            $stringElectronCardFactory->getId(),
                            $electronCard->getId(),
                            $stringElectronCardFactory->getId()
                        );
                    }

                    $editString .= sprintf(
                        '&nbsp;<a href="#" class="pull-right m-r-xs" onclick="getForm(\'/electron-cards/add-factory-form/%d/STRING/%s/%d\', \'/electron-cards/add-factory/%d/STRING/%s/%d\', event)"><i class="fa fa-pencil"></i></a>&nbsp;',
                        $electronCard->getId(),
                        $relationType,
                        $stringElectronCardFactory->getId(),
                        $electronCard->getId(),
                        $relationType,
                        $stringElectronCardFactory->getId()
                    );

                    $piecesString .= sprintf(
                        '<div class="bd-ud alert-%s">- %s%s</div>',
                        $stringElectronCardFactory->getMaterialFactoryId() ? 'success' : 'danger',
                        $stringElectronCardFactory->getName(),
                        $stringElectronCardFactory->getMaterialFactoryId() ? '' : $editString
                    );
                }
            }

            $piecesString .= sprintf(
                '<a href="#" class="pull-right" onclick="getForm(\'/electron-cards/add-factory-form/%d/STRING/factory\', \'/electron-cards/add-factory/%d/STRING/factory\', event)"><i class="fa fa-pencil"></i></a></div>',
                $electronCard->getId(),
                $electronCard->getId()
            );

            $stitchElectronCardFactories = $electronCard->getElectronCardFactories([
                'type = :type:',
                'bind' => [
                    'type' => ElectronCardFactory::STITCH
                ]
            ]);

            $piecesStitch = '';
            if ($stitchElectronCardFactories->count()) {
                foreach ($stitchElectronCardFactories as $stitchElectronCardFactory) {
                    $relationType = $stitchElectronCardFactory->getMaterialSketch() ? 'sketch' : 'factory';
                    $editStitch = '';

                    if ($relationType === 'sketch') {
                        $editStitch .= sprintf(
                            '<a href="#" class="pull-right text-success" onclick="getForm(\'/electron-cards/add-factory-form/%d/STITCH/factory/%d\', \'/electron-cards/add-factory/%d/STITCH/factory/%d\', event)"><i class="fa fa-edit"></i></a>',
                            $electronCard->getId(),
                            $stitchElectronCardFactory->getId(),
                            $electronCard->getId(),
                            $stitchElectronCardFactory->getId()
                        );
                    }

                    $editStitch .= sprintf(
                        '<a href="#" class="pull-right m-r-xs" onclick="getForm(\'/electron-cards/add-factory-form/%d/STITCH/%s/%d\', \'/electron-cards/add-factory/%d/STITCH/%s/%d\', event)"><i class="fa fa-pencil"></i></a>',
                        $electronCard->getId(),
                        $relationType,
                        $stitchElectronCardFactory->getId(),
                        $electronCard->getId(),
                        $relationType,
                        $stitchElectronCardFactory->getId()
                    );

                    $piecesStitch .= sprintf(
                        '<div class="bd-ud alert-%s">- %s%s</div>',
                        $stitchElectronCardFactory->getMaterialFactoryId() ? 'success' : 'danger',
                        $stitchElectronCardFactory->getName(),
                        $stitchElectronCardFactory->getMaterialFactoryId() ? '' : $editStitch
                    );
                }
            }

            $piecesStitch .= sprintf(
                '<a href="#" class="pull-right" onclick="getForm(\'/electron-cards/add-factory-form/%d/STITCH/factory\', \'/electron-cards/add-factory/%d/STITCH/factory\', event)"><i class="fa fa-pencil"></i></a>',
                $electronCard->getId(),
                $electronCard->getId()
            );


            $attendantElectronCardFactories = $electronCard->getElectronCardFactories([
                'type = :type:',
                'bind' => [
                    'type' => ElectronCardFactory::ATTENDANT
                ]
            ]);

            $piecesAttendant = '';
            if ($attendantElectronCardFactories->count()) {
                foreach ($attendantElectronCardFactories as $attendantElectronCardFactory) {
                    $relationType = $attendantElectronCardFactory->getMaterialSketch() ? 'sketch' : 'factory';
                    $editAttendant = '';

                    if ($relationType === 'sketch') {
                        $editAttendant .= sprintf(
                            '<a href="#" class="pull-right text-success" onclick="getForm(\'/electron-cards/add-factory-form/%d/ATTENDANT/factory/%d\', \'/electron-cards/add-factory/%d/ATTENDANT/factory/%d\', event)"><i class="fa fa-edit"></i></a>',
                            $electronCard->getId(),
                            $attendantElectronCardFactory->getId(),
                            $electronCard->getId(),
                            $attendantElectronCardFactory->getId()
                        );
                    }

                    $editAttendant .= sprintf(
                        '<a href="#" class="pull-right m-r-xs" onclick="getForm(\'/electron-cards/add-factory-form/%d/ATTENDANT/%s/%d\', \'/electron-cards/add-factory/%d/ATTENDANT/%s/%d\', event)"><i class="fa fa-pencil"></i></a>',
                        $electronCard->getId(),
                        $relationType,
                        $attendantElectronCardFactory->getId(),
                        $electronCard->getId(),
                        $relationType,
                        $attendantElectronCardFactory->getId()
                    );

                    $piecesAttendant .= sprintf(
                        '<div class="bd-ud alert-%s">- %s%s</div>',
                        $attendantElectronCardFactory->getMaterialFactoryId() ? 'success' : 'danger',
                        $attendantElectronCardFactory->getName(),
                        $attendantElectronCardFactory->getMaterialFactoryId() ? '' : $editAttendant
                    );
                }
            }

            $piecesAttendant .= sprintf(
                '<a href="#" class="pull-right" onclick="getForm(\'/electron-cards/add-factory-form/%d/ATTENDANT/factory\', \'/electron-cards/add-factory/%d/ATTENDANT/factory\', event)"><i class="fa fa-pencil"></i></a>',
                $electronCard->getId(),
                $electronCard->getId()
            );

            $data[] = [
                $electronCard->getId(),
                sprintf('%s - %s',$electronCard->getProduct()->getCode(), $electronCard->getProduct()->getName()),
                $piecesBasic,
                $piecesString,
                $piecesStitch,
                $piecesAttendant
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $allElectronCards->count(),
            'recordsFiltered' => $allElectronCards->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getNameOrMaterialFactoriesAction()
    {
        $this->view->disable();
        $json = [];
        $term = $this->request->getQuery('term');

        $query = $this->modelsManager->createBuilder();
        $query->addFrom(MaterialFactory::class, 'mf')
            ->leftJoin(MaterialSample::class, 'ms.id = mf.materialSampleId', 'ms')
            ->where('mf.cardNumber LIKE :term: OR mf.misCode LIKE :term: OR ms.name LIKE :term:', [
                'term' => "%$term%"
            ]);

        /** @var MaterialFactory[] $materialFactories */
        $materialFactories = $query->getQuery()->execute();

        foreach ($materialFactories as $materialFactory) {
            $id = $materialFactory->getId();
            $json[$id]['id'] = $id;
            $json[$id]['value'] = $materialFactory->getMaterialSample() ? $materialFactory->getMaterialSample()->getName() : $materialFactory->getCardNumber();
            $json[$id]['label'] = $materialFactory->getMaterialSample() ? $materialFactory->getMaterialSample()->getName() . ' (' . $materialFactory->getDisplayName() . ')' : $materialFactory->getDisplayName();
            $json[$id]['type'] = 'material';
        }

        /** @var ElectronCardFactory[] $electronCardFactories */
        $electronCardFactories = $this->entityManager->find(ElectronCardFactory::class, [
            ElectronCardFactory::PROPERTY_NAME . ' LIKE :name:',
            'bind' => [
                'name' => "%$term%"
            ]
        ]);

        foreach ($electronCardFactories as $electronCardFactory) {
            $id = $electronCardFactory->getId();
            $json[$id . '_name']['id'] = $id;
            $json[$id . '_name']['value'] = $electronCardFactory->getName();
            $json[$id . '_name']['label'] = $electronCardFactory->getName();
            $json[$id . '_name']['type'] = 'electronCard';
        }

        $this->response->setContent(json_encode($json));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return ElectronCard::class;
    }
}
