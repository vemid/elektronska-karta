<?php

use Vemid\Controller\CrudController;
use Picqer\Barcode\BarcodeGeneratorHTML;
use \Vemid\Service\Product\Hydrator;
use \Vemid\Entity\Repository\ProductionOrderRepository;
use \Phalcon\Forms\Element\Text;
use Vemid\Form\Renderer\Json;
use \Picqer\Barcode\BarcodeGeneratorPNG;
use \Vemid\Date\DateTime;
use \Vemid\Messenger\Message;
use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Hidden;

/**
 * Class OperatingListItemsController
 */
class OperatingListItemsController extends CrudController
{
    public function overviewAction($id)
    {
        $this->tag::appendTitle($this->translator->t('Operacija Artikla'));

        /** @var OperatingListItem $operationListItem */
        if (!$operationListItem = $this->entityManager->findOne(OperatingListItem::class, $id)) {
            return $this->returnNotFound();
        }

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SIZES
            ]
        ]);

        $codes = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $query = $this->modelsManager->createBuilder()
            ->addFrom(Code::class, 'c')
            ->leftJoin(ProductSize::class, 'c.id = ps.codeId', 'ps')
            ->leftJoin(ProductionOrder::class, 'ps.id = po.productSizeId', 'po')
            ->where('ps.productId = :productId:', [
                'productId' => $operationListItem->getOperatingList()->getProductId()
            ])
            ->orderBy('c.id DESC');

        $sizeCodes = $query->getQuery()->execute();
        $worksheets = $this->entityManager->find(OperatingListItemWorksheet::class, [
            OperatingListItemWorksheet::PROPERTY_OPERATING_LIST_ITEM_ID . ' = :operationListItem:',
            'bind' => [
                'operationListItem' => $operationListItem->getEntityId()
            ]
        ]);

        $product = $operationListItem->getOperatingList()->getProduct();
        $hydrator = new Hydrator($product, $this->entityManager);

        /** @var ProductionOrderRepository $totals */
        $totals = $this->entityManager->getRepository(\ProductionOrder::class);
        $total = $totals->getTotalPlanned($product);
        /** @var \Vemid\Entity\Repository\OperatingListItemWorksheetRepository $finished */
        $finished = $this->entityManager->getRepository(\OperatingListItemWorksheet::class);

        $this->view->setVar('finished', $finished);
        $this->view->setVar('operationListItem', $operationListItem);
        $this->view->setVar('hydrator', $hydrator);
        $this->view->setVar('total', $total);
        $this->view->setVar('worksheets', $worksheets);
        $this->view->setVar('codes', $sizeCodes->count() ? $sizeCodes : $codes);
        $this->view->setVar('workers', $this->entityManager->find(ProductionWorker::class));
    }

    public function getCreateFormAction()
    {
        $params = $this->dispatcher->getParams();
        /** @var OperatingList $operatingList */
        if (empty($params[1]) || !$operatingList = $this->entityManager->findOne(\OperatingList::class, $params[1])) {
            return $this->returnNotFound();
        }

        $product = new Text('productFilter');
        $product->setLabel('Proizvod');

        $productHidden = new Hidden('productId');

        /** @var OperatingListItem $operatingListItems */
        $operatingListItems = $this->entityManager->find(\OperatingListItem::class,[
            \OperatingListItem::PROPERTY_OPERATING_LIST_ID .' = :operatingListId: ',
            'bind' => [
                'operatingListId' => $operatingList->getId(),
            ]
        ]);

        $excludedType = [];
        /** @var OperatingListItem $operatingListItem */
        foreach ( $operatingListItems as $operatingListItem) {
            $excludedType[] =
                (string)$operatingListItem->getOperatingListItemTypeId()
            ;

        }

        $form = $this->getForm();
        $form->get(\OperatingListItem::PROPERTY_OPERATING_LIST_ITEM_TYPE_ID)->setAttribute('disabled', $excludedType);
        $form->get(\OperatingListItem::PROPERTY_OPERATING_LIST_ID)->setDefault($operatingList->getEntityId());
        $form->add($product, \OperatingListItem::PROPERTY_OPERATING_LIST_ITEM_TYPE_ID, true);
        $form->add($productHidden);

        return (new Json())->render($form);
    }

    public function createAction()
    {
        if ($this->request->isPost()) {
            $form = $this->getForm();
            if ($form->isValid($this->request->getPost())) {
                $form->synchronizeEntityData();

                /** @var OperatingListItem $operatingListItems */
                $operatingListItems = $this->entityManager->findOne(\OperatingListItem::class,[
                    \OperatingListItem::PROPERTY_OPERATING_LIST_ID .' = :operatingListId: AND '.
                    \OperatingListItem::PROPERTY_OPERATING_LIST_ITEM_TYPE_ID .' = :operatingListItemTypeId:',
                    'bind' => [
                        'operatingListId' => $form->getData()['operatingListId'],
                        'operatingListItemTypeId' => $form->getData()['operatingListItemTypeId']
                    ]
                ]);
                if($operatingListItems) {
                    $this->addFlashMessage("Ne moze se snimiti jer vec postoji takva operacija !",null,Message::DANGER);
                }
                else {
                    if (!$this->saveEntity($form->getEntity())) {

                        $this->addFlashMessagesFromForm($form);
                    }
                }
            }
        }
    }

    public function printAction($id)
    {
        $this->view->disable();

        /** @var OperatingListItem $operationListItem */
        if (!$operationListItem = $this->entityManager->findOne(OperatingListItem::class, $id)) {
            return $this->returnNotFound();
        }

        $product = $operationListItem->getOperatingList()->getProduct();
        $hydrator = new Hydrator($product, $this->entityManager);

        /** @var ProductionOrderRepository $totals */
        $totals = $this->entityManager->getRepository(\ProductionOrder::class);
        $total = $totals->getTotalPlanned($product);

        $generatorHtml = new BarcodeGeneratorPNG();

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();

        $html = $this->getDI()->getSimpleView()->render('operating-list-items/print', [
            'operationListItem' => $operationListItem,
            'hydrator' => $hydrator,
            'total' => $total,
            'generatorHtml' => $generatorHtml,
            'barcodeType' => BarcodeGeneratorHTML::TYPE_CODE_128,
            'siteUrl' => ltrim($config->application->baseUri, '/')
        ]);

        $content = $this->getDI()->getPdfRenderer()->render($html);
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $product->getCode() . '-' . $product->getName());

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;
    }

    public function addWorksheetAction($id)
    {
        /** @var OperatingListItem $operationListItem */
        if (!$operationListItem = $this->entityManager->findOne(OperatingListItem::class, $id)) {
            return $this->returnNotFound();
        }

        $postData = $this->request->getPost();
        foreach ($postData['create'] as $data) {
            if (empty(array_filter($data))) {
                continue;
            }

            /** @var ProductionWorker $productionWorker */
            if (!$productionWorker = $this->entityManager->findOne(ProductionWorker::class, $data['productionWorkerId'])) {
                continue;
            }

            /** @var Code $code |null */
            $code = $this->entityManager->findOne(Code::class, $data['codeId']);

            if (empty($data['startHour'])) {
                $data['startHour'] = '00';
            }

            if (empty($data['startMin'])) {
                $data['startMin'] = '00';
            }

            if (empty($data['endHour'])) {
                $data['endHour'] = '00';
            }

            if (empty($data['endMin'])) {
                $data['endMin'] = '00';
            }

            $startTime = new \Vemid\Date\Time($data['startHour'] . ':' . $data['startMin']);
            $endTime = new \Vemid\Date\Time($data['endHour'] . ':' . $data['endMin']);


            $workSheet = new OperatingListItemWorksheet();
            $workSheet->setOperatingListItem($operationListItem);
            $workSheet->setProductionWorker($productionWorker);
            $workSheet->setDate(new DateTime($data['date']));
            $workSheet->setQty($data['qty']);
            $workSheet->setStartTime($startTime);
            $workSheet->setEndTime($endTime);
            $workSheet->setApproved((isset($data['approved']) && $data['approved']) ? 1 : 0);
            $workSheet->setOvertime((isset($data['overtime']) && $data['overtime']) ? 1 : 0);
            $workSheet->setCreated(new DateTime());

            if ($code) {
                $workSheet->setCode($code);
            }

            if (!$this->entityManager->save($workSheet)) {
                $this->flashSession->error($workSheet->getMessages()[0]->getMessage());

                return $this->response->redirect('operating-list-items/overview/' . $id);
            }
        }

        $this->flashSession->success('Uspešno ste dodali listu!');

        return $this->response->redirect('operating-list-items/overview/' . $id);
    }

    public function findByBarcodeAction()
    {
        if (!$this->request->isAjax()) {
            return $this->returnNotFound();
        }

        $barcode = new Text('findBarcode');
        $barcode->setAttribute('placeHolder', 'Barkod operacije');

        $form = new Form();
        $form->add($barcode);
        $form->add(new \Phalcon\Forms\Element\Text('testFakeInout'));

        return (new Json())->render($form);
    }

    public function findOperatingItemAction()
    {
        $this->view->disable();

        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var OperatingListItem $operatingListItem */
        $operatingListItem = $this->entityManager->findOne(OperatingListItem::class, [
            OperatingListItem::PROPERTY_BARCODE . ' = :barcode:',
            'bind' => [
                'barcode' => $this->request->getPost('findBarcode')
            ]
        ]);

        if (!$operatingListItem) {
            $this->addFlashMessage(
                'Ne postoji takav barcode!',
                null,
                Message::DANGER
            );

            return;
        }

        $this->dispatcher->setParam('url', '/operating-list-items/overview/' . $operatingListItem->getEntityId());
    }

    public function findAllTypesAction()
    {
        $this->view->disable();
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $post = $this->request->getPost('productId');

        $query = $this->modelsManager->createBuilder()
            ->addFrom(OperatingListItem::class, 'oli');

        if ($post) {
            $query->leftJoin(OperatingList::class, 'oi.id = oli.operatingListId', 'oi')
                ->where('oi.productId = :productId:', [
                    'productId' => $post
                ]);
        }

        /** @var OperatingListItem[] $operatingListItems */
        $operatingListItems = $query->getQuery()->execute();

        $types = ['elements' => []];
        foreach ($operatingListItems as $operatingListItem) {
            $types['elements'][$operatingListItem->getOperatingListItemTypeId()] = $operatingListItem->getOperatingItemListType()->getName();
        }

        return $types;
    }

    public function getBaseProductsAction()
    {
        $this->view->disable();
        $json = [];
        $term = $this->request->getQuery('term');

        $query = $this->modelsManager->createBuilder()
            ->addFrom(OperatingList::class, 'oi')
            ->leftJoin(Product::class, 'p.id = oi.productId', 'p')
            ->where('p.name LIKE :name: OR p.code LIKE :name:', [
                'name' => "%$term%"
            ]);

        /** @var OperatingList[] $operatingLists */
        $operatingLists = $query->getQuery()->execute();

        foreach ($operatingLists as $operatingList) {
            $product = $operatingList->getProduct();

            $id = $product->getId();
            $json[$id]['id'] = $id;
            $json[$id]['value'] = $product->getName();
            $json[$id]['label'] = $product->getName() . ' (' . $product->getCode() . ')';
        }

        $this->response->setContent(json_encode($json));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function listPricesAction()
    {
        $this->view->disable();
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $productId = $this->request->getPost('productId');

        $query = $this->modelsManager->createBuilder()
            ->addFrom(OperatingListItem::class, 'oli')
            ->leftJoin(OperatingList::class, 'oi.id = oli.operatingListId', 'oi')
            ->where('oli.operatingListItemTypeId = :operatingListItemTypeId:', [
                'operatingListItemTypeId' => $this->request->getPost('operatingListItemTypeId')
            ])
            ->andWhere('oi.active = :active:', [
                'active' => true
            ]);

        if ($productId) {
            $query->andWhere('oi.productId = :productId:', [
                'productId' => $productId
            ]);
        }

        $query->orderBy('oli.created');

        /** @var OperatingListItem|null $operatingListItem */
        $operatingListItem = $query->getQuery()->execute()->getLast();

        return $operatingListItem ? $operatingListItem->toArray() : [];
    }



    /**
     * @return string
     */
    public function getEntityName()
    {
        return OperatingListItem::class;
    }
}
