<?php

use Vemid\Controller\CrudController;
use Vemid\Form\Renderer\Json as Renderer;

/**
 * Class ClientsController
 *
 * @package Default\Controllers
 */
class QuestionsController extends CrudController
{
    public function getCreateFormAction()
    {
        $params = $this->dispatcher->getParams();
        /** @var Questionnaire $questionnaire */
        $questionnaire = $this->entityManager->findOne(Questionnaire::class, $params[1]);

        if (!$questionnaire) {
            $this->returnNotFound();
        }

        $renderer = new Renderer();
        $form = $this->getForm(null, [
            Question::PROPERTY_ANSWERED_BY_USER_ID,
            Question::PROPERTY_ANSWERED_NOTE,
            Question::PROPERTY_ANSWERS,
        ]);

        if ($form->has(Question::PROPERTY_POSSIBLE_ANSWERS)) {
            $form->get(Question::PROPERTY_POSSIBLE_ANSWERS)->setAttribute('data-multiple', true);
        }

        if ($form->has(Question::PROPERTY_QUESTIONNAIRE_ID)) {
            $form->get(Question::PROPERTY_QUESTIONNAIRE_ID)->setDefault($questionnaire->getId());
        }

        return $renderer->render($form);
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        $postData = $this->request->getPost();
        $postData[Question::PROPERTY_POSSIBLE_ANSWERS] = json_encode($postData[Question::PROPERTY_POSSIBLE_ANSWERS]);
        $form = $this->getForm();

        $postData[Question::PROPERTY_ADDITIONAL_NOTE]  = !empty($postData[Question::PROPERTY_ADDITIONAL_NOTE]) ? $postData[Question::PROPERTY_ADDITIONAL_NOTE] : 0;

        if ($form->isValid($postData)) {
            if (!$this->saveEntity($form->getEntity())) {
                $this->addFlashMessagesFromEntity($form->getEntity());
            }
        } else {
            $this->addFlashMessagesFromForm($form);
        }
    }

    public function answerAction()
    {
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        $postData = $this->request->getPost();
        if (empty($postData['questionId']) || empty($postData['answer'])) {
            $this->returnNotFound();
        }

        $questions = $postData['questionId'];
        $answers = $postData['answer'];
        $additionalNote = $postData['additionalNote'] ?? null;

        foreach ($questions as $questionId) {
            /** @var Question $question */
            if (!$question = $this->entityManager->findOne(Question::class, $questionId)) {
                continue;
            }

            if (!isset($answers[$questionId])) {
                $value = null;
            } else {
                $value = $answers[$questionId];
            }

            if ($value && is_array($value)) {
                $value = json_encode($value);
            }

            $question->setAnswers($value);
            $question->setAnsweredByUserId($this->currentUser->getId());

            if ($additionalNote && isset($additionalNote[$questionId])) {
                $question->setAnsweredNote($additionalNote[$questionId]);
            }

            if (!$this->entityManager->save($question)) {
                $this->addFlashMessagesFromEntity($question);
                return;
            }
        }

        $this->addFlashMessage(
            $this->_translate('Data was saved successfully.'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return Question::class;
    }
}
