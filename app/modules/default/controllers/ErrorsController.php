<?php

use Vemid\Controller\BaseController;
use Vemid\Messenger\Message as MessengerMessage;

/**
 * Class ErrorsController
 *
 * @package Frontend\Controllers
 */
class ErrorsController extends BaseController
{

    public function show404Action()
    {
        $this->response->resetHeaders()
            ->setStatusCode(404)
            ->setHeader(404, 'Not found');

        $this->view->setTemplateAfter('welcome');
        $this->view->pick('errors/404');

        $this->_setTitle($this->getDI()->getTranslator()->t('Not found'));
    }

    public function show500Action()
    {
        $this->response->resetHeaders()
            ->setStatusCode(500)
            ->setHeader(500, 'Internal server error');

        $this->view->setTemplateAfter('welcome');
        $this->view->pick('errors/500');

        $this->_setTitle($this->getDI()->getTranslator()->t('Internal server error'));
    }

    public function show401Action()
    {
        if ($this->request->isAjax()) {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('You are not allowed to access this resource.'),
                null,
                MessengerMessage::DANGER
            );
        } else {
            $this->response->resetHeaders()
                ->setStatusCode(401)
                ->setHeader(401, 'Not authorized request');

            $this->view->pick('errors/401');

            $this->_setTitle($this->getDI()->getTranslator()->t('Not authorized request'));
        }
    }

}
