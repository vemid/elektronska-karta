<?php

use Vemid\Controller\CrudController;
use Vemid\Date\DateRange;
use \Vemid\Date\DateTime;
use Vemid\Service\OperationList\ItemCalculator;
use \Vemid\Service\Product\Hydrator;

/**
 * Class AbsenceWorksController
 *
 * @package Default\Controllers
 */

class AbsenceWorksController extends CrudController
{

    public function temporaryListAction()
    {
        $this->view->pick('absence-works/temporaryList');
        $this->view->setVar('absences', $this->entityManager->find(AbsenceWork::class, [
            AbsenceWork::PROPERTY_START_DATE . ' >= :startDate:',
            'bind' => [
                'startDate' => '2020-01-01'
            ],
            'order' => 'startDate ASC, productionWorkerId ASC'
        ]));
    }

    public function listAction()
    {
        $this->view->pick('absence-works/reports/list');
        $this->view->setVar('workers', $this->entityManager->find(ProductionWorker::class));
    }

    public function createAction(){

        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        $form = $this->getForm();
        $post = $this->request->getPost();
        $starDate = new DateTime($post['startDate']);
        $endDate = (new DateTime($post['endDate']))->modify('+1 day');
        /** @var ProductionWorker $productionWorker */
        $productionWorker = $this->entityManager->findOne(ProductionWorker::class,$post['productionWorkerId']);
        if (!$productionWorker)
        throw new DomainException('Ne postoji radnik');

        $dateRanges = (new DateRange($starDate,$endDate))->getAllDates();

        foreach ($dateRanges as $dateRange) {
            $absenceWork = new AbsenceWork();
            $absenceWork->setProductionWorker($productionWorker);
            $absenceWork->setStartDate($dateRange);
            $absenceWork->setEndDate($dateRange);
            $absenceWork->setTime($post['time']);
            $absenceWork->setType($post['type']);
            $absenceWork->setCreated(new DateTime());
            $absenceWork->setOnSalary(isset($post['onSalary']) ? 0 : 1);

            if ($this->saveEntity($absenceWork)) {
            } else {
                $this->addFlashMessagesFromEntity($absenceWork);
            }

        }
    }

    public function updateAction($id)
    {
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        $form = $this->getForm();
        $post = $this->request->getPost();
        $starDate = new DateTime($post['startDate']);
        $endDate = (new DateTime($post['endDate']))->modify('+1 day');
        /** @var ProductionWorker $productionWorker */
        $productionWorker = $this->entityManager->findOne(ProductionWorker::class,$post['productionWorkerId']);
        if (!$productionWorker)
            throw new DomainException('Ne postoji radnik');

        $dateRanges = (new DateRange($starDate,$endDate))->getAllDates();

        /** @var AbsenceWork $absenceWork */
        $absenceWork = $this->entityManager->findOne(AbsenceWork::class,$id);
        if (!$absenceWork)
            throw new DomainException('Ne postoji takav record');

        foreach ($dateRanges as $dateRange) {
            $absenceWork->setProductionWorker($productionWorker);
            $absenceWork->setStartDate($dateRange);
            $absenceWork->setEndDate($dateRange);
            $absenceWork->setTime($post['time']);
            $absenceWork->setType($post['type']);
            $absenceWork->setCreated(new DateTime());
            $absenceWork->setOnSalary(isset($post['onSalary']) ? 1 : 0);

            if ($this->saveEntity($absenceWork)) {
            } else {
                $this->addFlashMessagesFromEntity($absenceWork);
            }

        }
    }

    public function listDataAction()
    {
        $paramDateFrom = $this->request->getQuery('dateFrom');
        $paramDateTo = $this->request->getQuery('dateTo');
        $paramWorker = $this->request->getQuery('workerId');

        $dateTime = new DateTime();
        $productionWorker = $this->entityManager->findOne(ProductionWorker::class,$paramWorker);

        if (!$productionWorker) {
            $this->returnNotFound();
        }

        $dateRanges = new \Vemid\Date\DateRange(new DateTime($paramDateFrom),new DateTime($paramDateTo));

        /** @var \Vemid\Entity\Repository\AbsenceWorkRepository $absenceWork */
        $absenceWork = $this->entityManager->getRepository(AbsenceWork::class);

        /** @var \Vemid\Entity\Repository\CuttingJobWorksheetRepository $cuttingJob */
        $cuttingJob = $this->entityManager->getRepository(CuttingJobWorksheet::class);

        /** @var \Vemid\Entity\Repository\OperatingListItemWorksheetRepository $oliw */
        $oliw = $this->entityManager->getRepository(OperatingListItemWorksheet::class);

        $itemCalculator = new ItemCalculator($this->entityManager);

        $this->view->pick('absence-works/data/list-data');
        $this->view->setVar('absenceWork', $absenceWork);
        $this->view->setVar('cuttingJob', $cuttingJob);
        $this->view->setVar('oliw', $oliw);
        $this->view->setVar('productionWorker', $productionWorker);
        $this->view->setVar('itemCalculator', $itemCalculator);
        $this->view->setVar('dateTime', $dateTime);
        $this->view->setVar('dateRanges', $dateRanges->getAllDates());
    }


    public function testMarkoAction()
    {
        $this->view->disable();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()
            ->setCreator("Temporaris")
            ->setLastModifiedBy("Temporaris")
            ->setTitle("Template Relevé des heures intérimaires")
            ->setSubject("Template excel")
            ->setDescription("Template excel permettant la création d'un ou plusieurs relevés d'heures")
            ->setKeywords("Template excel");
        $objPHPExcel->setActiveSheetIndex(0);
        //$objPHPExcel->getActiveSheet()->SetCellValue('B1', "12");

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);

        $styleData = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );

        $styleHeader = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '1C9EC9')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            )
        );


        $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($styleHeader);


        $rows = array(
            array(
                'Slika',
                'Sifra',
                'Naziv',
                'Boja',
                'Prioritet',
                'Model',
                'Pod-Model',
                'Sezona',
                'Kolekcija',
                'Pol',
                'Velicine'
            )
        );
        $objPHPExcel->getActiveSheet()->fromArray($rows,null, 'A1');

        /** @var Product $product */
        $products = $this->entityManager->find(Product::class,[
            'code like "3191%" or code like "4191%" or code like "6191%"'
        ]);

        $rowPosition =2;
        foreach ($products as $product) {
            $hydrator = new \Vemid\Service\Product\Hydrator($product, $this->entityManager);
            //$objPHPExcel->getActiveSheet()->getRowDimension($rowPosition)->setRowHeight(120);
            $objPHPExcel->getActiveSheet()->getStyle("A".$rowPosition.":K".$rowPosition)->applyFromArray($styleData);

            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowPosition, $product->getCode());
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowPosition, $product->getName());
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowPosition, $hydrator->getColor()->getShortColorName());
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowPosition, $product->getPriority());
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowPosition, array_pop(explode(' ', $hydrator->getProductGroup()->getName())));
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowPosition, $hydrator->getSubProductGroup()->getName());
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowPosition, ($hydrator->getSeason() ? $hydrator->getSeason()->getName() : ''));
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowPosition, ($hydrator->getOldCollection() ? $hydrator->getOldCollection()->getName() : ''));
            $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowPosition, ($hydrator->getGenderName() ? $hydrator->getGenderName() : ''));
            $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowPosition, ($hydrator->getAllProductSizes() ? $hydrator->getAllProductSizes() : ''));

//            $objDrawing = new PHPExcel_Worksheet_Drawing();
//            $objDrawing->setName($product->getCode());
//            $objDrawing->setDescription('test_img');
//            $objDrawing->setPath($product->getUploadPath() . $product->getImage());
//            $objDrawing->setCoordinates('A'.$rowPosition);
////setOffsetX works properly
//            $objDrawing->setOffsetX(5);
//            $objDrawing->setOffsetY(5);
////set width, height
//            //$objDrawing->setWidth(100);
//            $objDrawing->setHeight(150);
//            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

            $rowPosition++;
        }

//        $tempFolder = rtrim(sys_get_temp_dir(), '/') . '/order-products';
//        $fileName = $tempFolder . '/' . 'Fajl';
//
//        if (file_exists($fileName)) {
//            unlink($fileName);
//        }
//
//        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
//        $objWriter->save($fileName);
//
//        $mailManager = $this->getDI()->getMailManager();
//        $message = $mailManager->createMessageFromView('reports/order-mail-template');
//
//        $message->setSubject("Slanje Excela ");
//        $message->setTo('admin@bebakids.com');
//        $message->setAttachment($fileName);
//        $message->send();

        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="excel.xls"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');


    }

    public function workerAbsenceListAction()
    {
        /** @var ProductionWorker $productionWorkers */
        $absenceWorkers = $this->entityManager->find(ProductionWorker::class);

        $this->view->pick('absence-works/reports/worker-absence-list');
        $this->view->setVar('absenceWorkers', $absenceWorkers);
    }

    public function workerAbsenceListDataAction()
    {

        $paramDateFrom = new DateTime($this->request->get('dateFrom'));
        $paramDateTo = (new DateTime($this->request->get('dateTo')));
        $paramWorker = $this->request->get('workerId');

        /** @var \ProductionWorker $productionWorker */
        $productionWorker = $this->entityManager->findOne(ProductionWorker::class, $paramWorker);

        if (!$productionWorker) {
            return $this->returnNotFound();
        }

        $absences = $this->entityManager->find(AbsenceWork::class, [
            AbsenceWork::PROPERTY_PRODUCTION_WORKER_ID . ' = :workerId: AND ' .
            AbsenceWork::PROPERTY_START_DATE . ' >= :startDate: AND ' . AbsenceWork::PROPERTY_END_DATE . ' <=:endDate:',
            'bind' => [
                'workerId' => $productionWorker->getId(),
                'startDate' => $paramDateFrom->getUnixFormat(),
                'endDate' => $paramDateTo->getUnixFormat(),
            ],
            'order' => 'startDate ASC, productionWorkerId ASC'
        ]);



        //print_r($absences);

        $this->view->pick('absence-works/data/worker-absence-list-data');
        $this->view->setVar('absences', $absences);

        //$this->dispatcher->setParam('url', 'absence-works/worker-absence-list-data?workerId='.$productionWorker->getId().'&dateFrom='.$paramDateFrom.'&dateTo='.$paramDateTo);

    }

    public function absenceListDataActionTEST()
    {
        //TODO proveriti da li da se razvija ovakava funkcionalnost
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');

        $query = $this->modelsManager->createBuilder();
        $query->addFrom(AbsenceWork::class, 'aw')
            ->leftJoin(ProductionWorker::class, 'pw.id = aw.productionWorkerId', 'pw');

        if (is_array($search) && $search['value']) {
            $query->andWhere('pw.fabricCode = :search: OR ms.cardNumber = :search: OR subParentClass2.name LIKE :search2: OR ms.color = :search: OR subParentClass.name LIKE :search2: OR subClass.name LIKE :search2:', [
                'search' => $search['value'],
                'search2' => '%' . $search['value'] . '%'
            ]);
        }
        $query->orderBy('id desc');
    }

    public function absenceListAction()
    {
        $this->view->pick('absence-works/reports/absence-list');
        $this->view->setVar('workers', $this->entityManager->find(ProductionWorker::class));
    }

    public function absenceListDataAction()
    {
        $paramDateFrom = $this->request->getQuery('dateFrom');
        $paramDateTo = $this->request->getQuery('dateTo');
        $paramWorker = $this->request->getQuery('workerId');

        $productionWorker = $this->entityManager->findOne(ProductionWorker::class,$paramWorker);

        if (!$productionWorker) {
            $this->returnNotFound();
        }

        $this->view->pick('absence-works/data/absence-list-data');
        $absencesData = $this->entityManager->find(AbsenceWork::class, [
            AbsenceWork::PROPERTY_START_DATE . ' >= :startDate: AND '.
            AbsenceWork::PROPERTY_END_DATE . ' <= :endDate: AND '.
            AbsenceWork::PROPERTY_PRODUCTION_WORKER_ID .' = :productionWorkerId:',
            'bind' => [
                'startDate' => $paramDateFrom,
                'endDate' => $paramDateTo,
                'productionWorkerId' => $productionWorker->getEntityId()
            ],
            'order' => 'startDate ASC, productionWorkerId ASC'
        ]);
        $this->view->setVar('absences', $absencesData);
    }


    /**
     * @return string
     */
    public function getEntityName()
    {
        return AbsenceWork::class;
    }
}