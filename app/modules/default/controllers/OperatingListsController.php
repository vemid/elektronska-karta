<?php

use Vemid\Controller\CrudController;
use \Vemid\Service\Product\Hydrator;
use \Vemid\Entity\Repository\ProductionOrderRepository;
use Picqer\Barcode\BarcodeGeneratorHTML;
use \Picqer\Barcode\BarcodeGeneratorPNG;
use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Text;
use Vemid\Form\Renderer\Json;
use \Vemid\Date\DateTime;
use \Vemid\Entity\Repository\OperatingListRepository;
use \Vemid\Entity\Repository\OperatingListItemRepository;
use \Vemid\Entity\Repository\OperatingListItemWorksheetRepository;

/**
 * Class OperatingListsController
 */
class OperatingListsController extends CrudController
{
    public function listAction()
    {
        $operationList = $this->entityManager->find(OperatingList::class, [
            'order' => 'id DESC'
        ]);

        /** @var OperatingList[] $operatingLists */
        $query = $this->modelsManager->createBuilder()
            ->addFrom(Code::class, 'c')
            ->leftJoin(OperatingList::class, 'ol.codeId = c.id', 'ol')
            ->where('ol.id IS NOT NULL')
            ->groupBy('c.id');

        $sectors = $query->getQuery()->execute();

        $this->view->setVar('form', new OperatingListForm());
        $this->view->setVar('operatingLists', $operationList);
        $this->view->setVar('sectors', $sectors);
    }

    public function nonFinishedAction()
    {
        $operationLists = $this->entityManager->find(OperatingList::class, [
            'finished = :finished:',
            'bind' => [
                'finished' => false
            ],
            'order' => 'id DESC'
        ]);

        /** @var ProductionOrderRepository $totals */
        $totals = $this->entityManager->getRepository(\ProductionOrder::class);

        $this->view->setVar('operatingLists', $operationLists);
        $this->view->setVar('form', new OperatingListForm());
        $this->view->setVar('totals', $totals);
        $this->view->pick('operating-lists/non-finished');
    }

    public function overviewAction($id)
    {
        $this->tag::appendTitle($this->translator->t("Operacije Artikla"));
        /** @var OperatingList $operationList */
        if (!$operationList = $this->entityManager->findOne(OperatingList::class, $id)) {
            return $this->returnNotFound();
        }

        $operationListItems = $this->entityManager->find(OperatingListItem::class, [
            'operatingListId = :operatinListId:',
            'bind' => [
                'operatinListId' => $id
            ],
            'order' => 'id DESC'
        ]);

        /** @var ProductionOrderRepository $totals */
        $totals = $this->entityManager->getRepository(\ProductionOrder::class);
        $totalPlanned = $totals->getTotalPlanned($operationList->getProduct());
        $totalReceived = $totals->getTotalReceived($operationList->getProduct());
        $hydrator = new Hydrator($operationList->getProduct(), $this->entityManager);

        /** @var OperatingList[] $operationLists */
        $operationLists = $this->entityManager->find(OperatingList::class, [
            'productId != :productId:',
            'bind' => [
                'productId' => $operationList->getProductId(),
            ]
        ]);

        $options = [];
        foreach ($operationLists as $operationListSelect) {
            if (!$product = $operationListSelect->getProduct()) {
                continue;
            }

            $options[$product->getId()] = $product;
        }

        if($operationList->getProduct()->getElectronCards()->count() > 0) {
            /** @var ElectronCard $electron */
            $electron = ($operationList->getProduct())->getElectronCards(['active = :kolona:', 'bind' => ['kolona' => true]]);
            $this->view->setVar('electronCard', $electron[0]);
        }
        /** @var \Vemid\Entity\Repository\OperatingListItemWorksheetRepository $finished */
        $finished = $this->entityManager->getRepository(\OperatingListItemWorksheet::class);

        $this->view->setVar('finished', $finished);
        $this->view->setVar('operatingList', $operationList);
        $this->view->setVar('operationListItems', $operationListItems);
        $this->view->setVar('totals', $totalPlanned);
        $this->view->setVar('totalReceived', $totalReceived);
        $this->view->setVar('hydrator', $hydrator);
        $this->view->setVar('generatorHtml', new BarcodeGeneratorHTML());
        $this->view->setVar('barcodeType', BarcodeGeneratorHTML::TYPE_CODE_128);
        $this->view->setVar('options', $options);
    }

    public function getUpdateFormAction($id)
    {
        $data = parent::getUpdateFormAction($id);
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        if (!$codeType) {
            return $this->returnNotFound();
        }

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $sectorsData = [];
        foreach ($sectors as $sector) {
            $sectorsData[$sector->getId()] = $sector->getName();
        }

        $data[OperatingList::PROPERTY_CODE_ID]['options'] = $sectorsData;

        return $data;
    }

    public function printAction($id)
    {
        $this->view->disable();

        /** @var OperatingList $operationList */
        if (!$operationList = $this->entityManager->findOne(OperatingList::class, $id)) {
            return $this->returnNotFound();
        }
        /** @var ProductionOrderRepository $totals */
        $totals = $this->entityManager->getRepository(\ProductionOrder::class);

        $generatorHtml = new \Picqer\Barcode\BarcodeGeneratorPNG();
        $hydrator = new Hydrator($operationList->getProduct(), $this->entityManager);

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();

        $html = $this->getDI()->getSimpleView()->render('operating-lists/print', [
            'operatingList' => $operationList,
            'generatorHtml' => $generatorHtml,
            'hydrator' => $hydrator,
            'totals' => $totals,
            'barcodeType' => BarcodeGeneratorHTML::TYPE_CODE_128,
            'siteUrl' => ltrim($config->application->baseUri, '/')
        ]);

        $content = $this->getDI()->getPdfRenderer()->render($html, 'landscape');
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', 'test');

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;
    }

    public function printAllItemsAction($id)
    {
        $this->view->disable();
        /** @var OperatingList $operationList */
        if (!$operationList = $this->entityManager->findOne(OperatingList::class, $id)) {
            return $this->returnNotFound();
        }

        $product = $operationList->getProduct();
        $hydrator = new Hydrator($product, $this->entityManager);

        /** @var ProductionOrderRepository $totals */
        $totals = $this->entityManager->getRepository(\ProductionOrder::class);
        $total = $totals->getTotalPlanned($product);

        $generatorHtml = new BarcodeGeneratorPNG();

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();

        $html = $this->getDI()->getSimpleView()->render('operating-lists/print-all-items', [
            'operatingList' => $operationList,
            'hydrator' => $hydrator,
            'product' => $product,
            'totals' => $totals,
            'generatorHtml' => $generatorHtml,
            'barcodeType' => BarcodeGeneratorHTML::TYPE_CODE_128,
            'siteUrl' => ltrim($config->application->baseUri, '/')
        ]);

        $content = $this->getDI()->getPdfRenderer()->render($html);
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $product->getCode() . '-' . $product->getName());

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;

//        $this->view->pick('operating-lists/print-all-items');
//        $this->view->setVar('operatingList', $operationList);
//        $this->view->setVar('totals', $totals);
//        $this->view->setVar('hydrator', $hydrator);
//        $this->view->setVar('product', $product);
//        $this->view->setVar('generatorHtml', $generatorHtml);
//        $this->view->setVar('barcodeType', BarcodeGeneratorHTML::TYPE_CODE_128);
    }


    public function getCreateFormAction()
    {
        $form = $this->getForm();
        $form->get(OperatingList::PROPERTY_PRODUCT_ID)->setName('operationListProductId');
        $form->add(
            new \Phalcon\Forms\Element\Hidden('hiddenProductId')
        );

        $renderer = new \Vemid\Form\Renderer\Json();

        $data = $renderer->render($form);

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        if (!$codeType) {
            return $this->returnNotFound();
        }

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $sectorsData = [];
        foreach ($sectors as $sector) {
            $sectorsData[$sector->getId()] = $sector->getName();
        }

        $data[OperatingList::PROPERTY_CODE_ID]['options'] = $sectorsData;

        return $data;
    }

    public function createAction()
    {
        if ($this->request->isPost()) {
            $form = $this->getForm();
            $post = $this->request->getPost();
            $post['productId'] = $post['hiddenProductId'];
            if ($form->isValid($post)) {
                $entity = $form->getEntity();
                $form->synchronizeEntityData();
                if ($this->saveEntity($entity)) {
                    $this->dispatcher->setParam('url', '/operating-lists/overview/' . $entity->getEntityId());
                } else {
                    $this->addFlashMessagesFromEntity($entity);
                }
            } else {
                $this->addFlashMessagesFromForm($form);
            }
        }
    }

    public function getProductsAction()
    {
        $this->view->disable();
        $json = [];
        $term = $this->request->getQuery('term');

        /** @var Product[] $products */
        $productCollections = $this->entityManager->find(Product::class, [
            Product::PROPERTY_CODE . ' LIKE :term: OR ' . Product::PROPERTY_NAME . ' LIKE :term:',
            'bind' => [
                'term' => "%$term%"
            ]
        ]);

        $products = [];
        foreach ($productCollections as $productCollection) {
            $products[$productCollection->getId()] = $productCollection;
        }

        /** @var ProductionOrder[] $productionOrder */
        $productionOrder = $this->entityManager->find(ProductionOrder::class, [
            ProductionOrder::PROPERTY_WORK_ORDER . ' LIKE :term:',
            'bind' => [
                'term' => "%$term%"
            ]
        ]);

        foreach ($productionOrder as $productionOrder) {
            if (!$product = $productionOrder->getProductSize()->getProduct()) {
                continue;
            }

            $products[$product->getId()] = $product;
        }

        foreach ($products as $product) {
            $id = $product->getId();
            $json[$id]['id'] = $id;
            $json[$id]['value'] = $product->getName() . ' (' . $product->getCode() . ')';;
            $json[$id]['label'] = $product->getName() . ' (' . $product->getCode() . ')';
        }

        $this->response->setContent(json_encode($json));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function filterListAction()
    {
        $this->view->setVar('form', new OperatingListFilterForm());
        $this->view->pick('operating-lists/filter-list');
    }

    public function copyFormAction($id)
    {
        /** @var OperatingList $operationList */
        if (!$operationList = $this->entityManager->findOne(OperatingList::class, $id)) {
            return $this->returnNotFound();
        }

        $oldOrderList = new \Phalcon\Forms\Element\Hidden('operatingListId');
        $oldOrderList->setDefault($operationList->getId());

        $form = $this->getForm();
        $form->add($oldOrderList);
        $form->remove(OperatingList::PROPERTY_CODE_ID);

        $form->get(OperatingList::PROPERTY_PRODUCT_ID)->setName('operationListProductId');
        $form->add(
            new \Phalcon\Forms\Element\Hidden('hiddenProductId')
        );

        $renderer = new \Vemid\Form\Renderer\Json();

        return $renderer->render($form);
    }

    public function copyAction()
    {
        $this->view->disable();

        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $postData = $this->request->getPost();

        /** @var OperatingList $operationList */
        if (!$operationList = $this->entityManager->findOne(OperatingList::class, $postData['operatingListId'])) {
            return $this->returnNotFound();
        }

        /** @var Product $product */
        if (!$product = $this->entityManager->findOne(Product::class, $postData['hiddenProductId'])) {
            return $this->returnNotFound();
        }

        /** @var OperatingList $operationListCheck */
        $operationListCheck = $this->entityManager->findOne(OperatingList::class, [
            OperatingList::PROPERTY_PRODUCT_ID . ' = :productId: AND ' .
            OperatingList::PROPERTY_CODE_ID . ' = :codeId:',
            'bind' => [
                'productId' => $product->getId(),
                'codeId' => $operationList->getCodeId()
            ]
        ]);

        if ($operationListCheck) {
            $this->addFlashMessage(
                'Vec postoji operaciona lista!!',
                null,
                \Vemid\Messenger\Message::DANGER
            );

            return;
        }

        $data = $operationList->toArray();
        unset($data[OperatingList::PROPERTY_ID]);
        $data[OperatingList::PROPERTY_PRODUCT_ID] = $product->getId();
        $data[OperatingList::PROPERTY_CREATED] = new \Vemid\Date\DateTime();
        $data[OperatingList::PROPERTY_FINISHED] = false;

        $newOperatingList = new OperatingList();
        if (!$newOperatingList->create($data)) {
            $this->addFlashMessagesFromEntity($newOperatingList);
            return;
        }

        foreach ($operationList->getOperatingListItems() as $operatingListItem) {
            $newOperatingListItem = new OperatingListItem();
            $newOperatingListItem->setOperatingList($newOperatingList);
            $newOperatingListItem->setOperatingListItemType($operatingListItem->getOperatingItemListType());
            $newOperatingListItem->setPiecePrice($operatingListItem->getPiecePrice());
            $newOperatingListItem->setPiecesHour($operatingListItem->getPiecesHour());
            $newOperatingListItem->setPieceTime($operatingListItem->getPieceTime());
            $newOperatingListItem->setFinished(false);
            if (!$this->entityManager->save($newOperatingListItem)) {
                $this->addFlashMessagesFromEntity($newOperatingListItem);
                return;
            }
        }

        $this->addFlashMessage(
            'Uspešno kopirana operaciona lista!',
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function getFilterListAction($sectorId)
    {
        /** @var $code Code */
        if (!$code = $this->entityManager->findOne(Code::class, $sectorId)) {
            return $this->returnNotFound();
        }

        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');
        $oldCollectionId = $this->request->getQuery('oldCollectionId', 'string', '');
        $genderClassificationId = $this->request->getQuery('genderClassificationId', 'int', 0);
        $groupModelId = $this->request->getQuery('groupModelId', 'int', 0);

        $genders = [];
        /** @var Classification $classification */
        if ($genderClassificationId && $classification = $this->entityManager->findOne(Classification::class, $genderClassificationId)) {
            /** @var Classification[] $genderClassifications */
            $genderClassifications = $this->entityManager->find(Classification::class, [
                'name = :name:',
                'bind' => [
                    'name' => $classification->getName()
                ]
            ]);

            foreach ($genderClassifications as $genderClassification) {
                $genders[] = $genderClassification->getId();
            }
        }

        $groupModels = [];
        /** @var Classification $classification */
        if ($groupModelId && $classification = $this->entityManager->findOne(Classification::class, $groupModelId)) {

            /** @var ElectronCard[] $electronCards */
            $query = $this->modelsManager->createBuilder()
                ->addFrom(Classification::class, 'c')
                ->leftJoin(Classification::class, 'cp.id = c.parentClassificationId', 'cp')
                ->where('c.name = :name:', [
                    'name' => $classification->getName()
                ])
                ->andWhere('c.name != cp.name');

            /** @var Classification[] $groupModelClassifications */
            $groupModelClassifications = $query->getQuery()->execute();

            foreach ($groupModelClassifications as $groupModelClassification) {
                $groupModels[] = $groupModelClassification->getId();
            }
        }

        /** @var OperatingList[] $operatingLists */
        $query = $this->modelsManager->createBuilder()
            ->addFrom(OperatingList::class, 'ol')
            ->leftJoin(Product::class, 'ol.productId = p.id', 'p')
            ->leftJoin(ProductClassification::class, 'p.id = pc.productId', 'pc')
            ->leftJoin(Classification::class, 'c.id = pc.entityId', 'c')
            ->where('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => \Vemid\Entity\Type::CLASSIFICATION
            ])
            ->andWhere('ol.codeId = :codeId:', [
                'codeId' => $code->getId()
            ]);

        if (is_array($search) && $search['value']) {
            $query->andWhere('p.code LIKE :search: OR p.name LIKE :search:', [
                'search' => '%' . trim($search['value']) . '%'
            ]);
        }

        $queryCodes = [];
        $filter = 0;
        if ($oldCollectionId) {
            /** @var Classification $oldCollection */
            $oldCollection = $this->entityManager->findOne(Classification::class, [
                'code = :code:',
                'bind' => [
                    'code' => $oldCollectionId
                ]
            ]);

            if ($oldCollection) {
                $filter++;
                $queryCodes[] = $oldCollection->getId();
            }
        }

        if (count($genders)) {
            $filter++;
            $queryCodes = array_merge($queryCodes, $genders);
        }

        if (count($groupModels)) {
            $filter++;
            $queryCodes = array_merge($queryCodes, $groupModels);
        }

        if (count($queryCodes)) {
            $query->andWhere('pc.entityId IN ({codeQuery:array})', [
                'codeQuery' => $queryCodes
            ]);

            $query->having('COUNT(pc.entityId) >= ' . $filter);
        }

        $query->groupBy('ol.id');
        $query->orderBy('ol.id DESC');

        $queryForCount = clone $query;
        $allOperatingLists = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);
        $operatingLists = $query->getQuery()->execute();

        $data = [];
        foreach ($operatingLists as $operatingList) {
            $data[] = [
                $operatingList->getId(),
                $operatingList->getProduct()->getCode(),
                $operatingList->getProduct()->getName(),
                $operatingList->getWorkOrders(),
                $operatingList->getFinished(),
                $operatingList->getProduct()->getImagePath(),
                (int)$this->currentUser->isSuperAdmin(),
                $operatingList->getLocked(),
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $allOperatingLists->count(),
            'recordsFiltered' => $allOperatingLists->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getFilterNonFinishedAction()
    {
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');
        $oldCollectionId = $this->request->getQuery('oldCollectionId', 'string', '');
        $genderClassificationId = $this->request->getQuery('genderClassificationId', 'int', 0);
        $groupModelId = $this->request->getQuery('groupModelId', 'int', 0);

        $genders = [];
        /** @var Classification $classification */
        if ($genderClassificationId && $classification = $this->entityManager->findOne(Classification::class, $genderClassificationId)) {
            /** @var Classification[] $genderClassifications */
            $genderClassifications = $this->entityManager->find(Classification::class, [
                'name = :name:',
                'bind' => [
                    'name' => $classification->getName()
                ]
            ]);

            foreach ($genderClassifications as $genderClassification) {
                $genders[] = $genderClassification->getId();
            }
        }

        $groupModels = [];
        /** @var Classification $classification */
        if ($groupModelId && $classification = $this->entityManager->findOne(Classification::class, $groupModelId)) {

            /** @var ElectronCard[] $electronCards */
            $query = $this->modelsManager->createBuilder()
                ->addFrom(Classification::class, 'c')
                ->leftJoin(Classification::class, 'cp.id = c.parentClassificationId', 'cp')
                ->where('c.name = :name:', [
                    'name' => $classification->getName()
                ])
                ->andWhere('c.name != cp.name');

            /** @var Classification[] $groupModelClassifications */
            $groupModelClassifications = $query->getQuery()->execute();

            foreach ($groupModelClassifications as $groupModelClassification) {
                $groupModels[] = $groupModelClassification->getId();
            }
        }

        /** @var OperatingListItemRepository $operatingListItemRepository */
        $operatingListItemRepository = $this->entityManager->getRepository(\OperatingListItem::class);

        /** @var OperatingList[] $operatingLists */
        $query = $this->modelsManager->createBuilder()
            ->columns([
                'ol.id',
                'SUM(COALESCE(oliw.qty, 0)) as total'
            ])
            ->addFrom(OperatingList::class, 'ol')
            ->leftJoin(OperatingListItem::class, 'oli.operatingListId = ol.id', 'oli')
            ->leftJoin(OperatingListItemWorksheet::class, 'oliw.operatingListItemId = oli.id', 'oliw')
            ->leftJoin(Product::class, 'ol.productId = p.id', 'p')
            ->leftJoin(ProductClassification::class, 'p.id = pc.productId', 'pc')
            ->leftJoin(Classification::class, 'c.id = pc.entityId', 'c')
            ->where('pc.entityTypeId = :entityTypeId:', [
                'entityTypeId' => \Vemid\Entity\Type::CLASSIFICATION
            ])
            ->andWhere('ol.finished = 0');

        if (is_array($search) && $search['value']) {
            $query->andWhere('p.code LIKE :search: OR p.name LIKE :search:', [
                'search' => '%' . trim($search['value']) . '%'
            ]);
        }

        $queryCodes = [];
        $filter = 0;
        $conditionHaving = '';
        if ($oldCollectionId) {
            /** @var Classification $oldCollection */
            $oldCollection = $this->entityManager->findOne(Classification::class, [
                'code = :code:',
                'bind' => [
                    'code' => $oldCollectionId
                ]
            ]);

            if ($oldCollection) {
                $filter++;
                $queryCodes[] = $oldCollection->getId();
            }
        }

        if (count($genders)) {
            $filter++;
            $queryCodes = array_merge($queryCodes, $genders);
        }

        if (count($groupModels)) {
            $filter++;
            $queryCodes = array_merge($queryCodes, $groupModels);
        }

        if (count($queryCodes)) {
            $query->andWhere('pc.entityId IN ({codeQuery:array})', [
                'codeQuery' => $queryCodes
            ]);

            $conditionHaving .= ' AND COUNT(pc.entityId) >= ' . $filter;
        }

        $query->having('total > 0' . $conditionHaving);
        $query->groupBy('ol.id');

        $queryForCount = clone $query;
        $allOperatingLists = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);
        $results = $query->getQuery()->execute();

        $data = [];
        foreach ($results as $row) {
            /** @var OperatingList $operatingList */
            if (!$operatingList = $this->entityManager->findOne(OperatingList::class, $row['id'])) {
                continue;
            }

            $data[] = [
                $operatingList->getId(),
                $operatingList->getProduct()->getCode(),
                $operatingList->getProduct()->getName(),
                $operatingListItemRepository->getStartProductionDate($operatingList),
                $operatingListItemRepository->getEndProductionDate($operatingList),
                '',
                $operatingList->getProduct()->getImagePath()
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $allOperatingLists->count(),
            'recordsFiltered' => $allOperatingLists->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function addTactGroupFormAction()
    {
        if (!$this->request->isAjax()) {
            return $this->returnNotFound();
        }

        $tactGroupValue = new Text('tactGroupValue');
        $tactGroupValue->setAttribute('placeHolder', 'Takt grupe ');

        $form = new Form();
        $form->add($tactGroupValue);
        $form->add(new \Phalcon\Forms\Element\Text('testFakeInout'));

        return (new Json())->render($form);
    }

    public function copyItemsAction($id, $productId)
    {
        /** @var OperatingList $operatingList */
        if (!$operatingList = $this->entityManager->findOne(OperatingList::class, $id)) {
            return $this->returnNotFound();
        }

        /** @var Product $product */
        if (!$product = $this->entityManager->findOne(Product::class, $productId)) {
            return $this->returnNotFound();
        }

        /** @var OperatingList $operatingListCopy */
        $operatingListCopy = $this->entityManager->findOne(OperatingList::class, [
            'productId = :productId:',
            'bind' => [
                'productId' => $product->getId()
            ]
        ]);

        $this->view->setVar('operatingListCopy', $operatingListCopy);
        $this->view->setVar('operatingList', $operatingList);
        $this->view->pick('operating-lists/copy-items');
    }

    public function copyItemFormAction($id, $itemId)
    {
        /** @var OperatingList $operationList */
        if (!$operationList = $this->entityManager->findOne(OperatingList::class, $id)) {
            return $this->returnNotFound();
        }

        /** @var OperatingListItem $operatingListItem */
        if (!$operatingListItem = $this->entityManager->findOne(OperatingListItem::class, $itemId)) {
            return $this->returnNotFound();
        }

        $data = $operatingListItem->toArray();
        unset($data[OperatingListItem::PROPERTY_ID], $data[OperatingListItem::PROPERTY_CREATED]);
        $data[OperatingListItem::PROPERTY_OPERATING_LIST_ID] = $operationList->getId();
        $data[OperatingListItem::PROPERTY_CREATED] = new \Vemid\Date\DateTime();

        $newOperationListItem = new OperatingListItem($data);

        return (new Json())->render($newOperationListItem->getForm());
    }

    public function saveTactGroupAction($id)
    {
        if ($this->request->isPost()) {
            $post = $this->request->getPost();

            /** @var OperatingList $operatingList */
            if (!$operatingList = $this->entityManager->findOne(OperatingList::class, $id)) {
                return $this->returnNotFound();
            }

            $operatingList->setTactGroup($post['tactGroupValue']);
            if (!$this->entityManager->save($operatingList)) {
                $this->addFlashMessagesFromEntity($operatingList);

                return;
            }

            $this->addFlashMessage(
                'Uspesno',
                null,
                \Vemid\Messenger\Message::SUCCESS
            );
        }
    }

    public function reportsAction()
    {
        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        if (!$codeType) {
            return $this->returnNotFound();
        }

        $codes = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $this->view->setVar('codes', $codes);
        $this->view->setVar('date', new \Vemid\Date\DateTime());
    }

    public function sectorMonthlyReportAction()
    {
        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        if (!$codeType) {
            return $this->returnNotFound();
        }

        $codes = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        /** @var OperatingListItemRepository $operatingListItemRepository */
        $operatingListItemRepository = $this->entityManager->getRepository(OperatingListItem::class);

        $this->view->pick('operating-lists/sector-monthly-report');

        $this->view->setVar('codes', $codes);
        $this->view->setVar('dateFrom', new \Vemid\Date\DateTime());
        $this->view->setVar('dateTo', new \Vemid\Date\DateTime());
        $this->view->setVar('niz', $operatingListItemRepository->getTotalProducedProduct());
    }

    public function getReportsDataAction()
    {

        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');

        $codeId = $this->request->getQuery('codeId', 'int', 0);
        $dateTo = new DateTime($this->request->getQuery('dateTo', 'string', ''));
        $dateTo->setDateType(DateTime::TYPE_DATE);

        $query = $this->modelsManager->createBuilder()
            ->columns([
                'p.code',
                'p.name',
                'p.id as productId',
                'ol.id as operatingListId',
                'COUNT(DISTINCT oliw.productionWorkerId) as totalWorkers',
                'SUM(oliw.qty) as totalQty',
            ])
            ->addFrom(OperatingList::class, 'ol')
            ->leftJoin(Product::class, 'ol.productId = p.id', 'p')
            ->leftJoin(OperatingListItem::class, 'oli.operatingListId = ol.id', 'oli')
            ->leftJoin(OperatingListItemWorksheet::class, 'oliw.operatingListItemId = oli.id', 'oliw')
            ->where('oliw.date = :date:', [
                'date' => $dateTo->getUnixFormat()
            ]);

        if ($codeId) {
            $query->andWhere('ol.codeId = :codeId:', [
                'codeId' => $codeId
            ]);
        }

        $query->groupBy('ol.productId');

        $queryForCount = clone $query;
        $totalResults = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);

        $result = $query->getQuery()->execute();

        /** @var OperatingListRepository $operatingListRepository */
        $operatingListRepository = $this->entityManager->getRepository(OperatingList::class);
        /** @var ProductionOrderRepository $productOrderRepository */
        $productOrderRepository = $this->entityManager->getRepository(ProductionOrder::class);

        $totalWorkers = 0;
        $sumTotalNorm = $operatingListRepository->getTotalProfitTillDay($date);
        $data = [];
        foreach ($result as $row) {
            /** @var OperatingList $operatingList */
            if (!$operatingList = $this->entityManager->findOne(OperatingList::class, $row['operatingListId'])) {
                continue;
            }

            /** @var Product $product */
            if (!$product = $this->entityManager->findOne(Product::class, $row['productId'])) {
                continue;
            }
            $totalWorkers += $row['totalWorkers'];
            $totalItems = $operatingList->getOperatingListItems()->count();
            $totalPlaned = $productOrderRepository->getTotalPlanned($product);
            $totalQty = $operatingListRepository->getTotalQtyTillDay($operatingList, $date);
            $sumProfit = $operatingListRepository->getSumQtyGroupeByItemOnDay($operatingList, $date);
            $totalProfit = $operatingListRepository->getProfitTillDay($operatingList, $date);

            $f = ($totalQty / ($totalPlaned * $totalItems)) * 100;
            $totalNorm = ($sumProfit / ($row['totalWorkers'] * 450)) * 100;
            $data[] = [
                $row['code'] . ' - ' . $row['name'],
                $row['totalWorkers'],
                number_format($f, 2),
                $totalPlaned,
                $row['totalQty'],
                number_format($totalNorm, 2) . ' %',
                number_format($totalProfit, 2),
            ];
        }

        $grandTotalNorm = round($sumTotalNorm / (($totalWorkers * 450) * 100), 2);

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $totalResults->count(),
            'recordsFiltered' => $totalResults->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getSectorMonthlyReportDataAction()
    {

        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');

        $codeId = $this->request->getQuery('codeId', 'int', 0);
        $date = new DateTime($this->request->getQuery('dateTo', 'string', ''));
        $date->setDateType(DateTime::TYPE_DATE);

        $query = $this->modelsManager->createBuilder()
            ->columns([
                'p.code',
                'p.name',
                'p.id as productId',
                'ol.id as operatingListId',
                'COUNT(DISTINCT oliw.productionWorkerId) as totalWorkers',
                'SUM(oliw.qty) as totalQty',
            ])
            ->addFrom(OperatingList::class, 'ol')
            ->leftJoin(Product::class, 'ol.productId = p.id', 'p')
            ->leftJoin(OperatingListItem::class, 'oli.operatingListId = ol.id', 'oli')
            ->leftJoin(OperatingListItemWorksheet::class, 'oliw.operatingListItemId = oli.id', 'oliw')
            ->where('oliw.date = :date:', [
                'date' => $date->getUnixFormat()
            ]);

        if ($codeId) {
            $query->andWhere('ol.codeId = :codeId:', [
                'codeId' => $codeId
            ]);
        }

        $query->groupBy('ol.productId');

        $queryForCount = clone $query;
        $totalResults = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);

        $result = $query->getQuery()->execute();

        /** @var OperatingListRepository $operatingListRepository */
        $operatingListRepository = $this->entityManager->getRepository(OperatingList::class);
        /** @var ProductionOrderRepository $productOrderRepository */
        $productOrderRepository = $this->entityManager->getRepository(ProductionOrder::class);

        $totalWorkers = 0;
        $sumTotalNorm = $operatingListRepository->getTotalProfitTillDay($date);
        $data = [];
        foreach ($result as $row) {
            /** @var OperatingList $operatingList */
            if (!$operatingList = $this->entityManager->findOne(OperatingList::class, $row['operatingListId'])) {
                continue;
            }

            /** @var Product $product */
            if (!$product = $this->entityManager->findOne(Product::class, $row['productId'])) {
                continue;
            }
            $totalWorkers += $row['totalWorkers'];
            $totalItems = $operatingList->getOperatingListItems()->count();
            $totalPlaned = $productOrderRepository->getTotalPlanned($product);
            $totalQty = $operatingListRepository->getTotalQtyTillDay($operatingList, $date);
            $sumProfit = $operatingListRepository->getSumQtyGroupeByItemOnDay($operatingList, $date);
            $totalProfit = $operatingListRepository->getProfitTillDay($operatingList, $date);

            $f = ($totalQty / ($totalPlaned * $totalItems)) * 100;
            $totalNorm = ($sumProfit / ($row['totalWorkers'] * 450)) * 100;
            $data[] = [
                $row['code'] . ' - ' . $row['name'],
                $row['totalWorkers'],
                number_format($f, 2),
                $totalPlaned,
                $row['totalQty'],
                number_format($totalNorm, 2) . ' %',
                number_format($totalProfit, 2),
            ];
        }

        $grandTotalNorm = round($sumTotalNorm / (($totalWorkers * 450) * 100), 2);

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $totalResults->count(),
            'recordsFiltered' => $totalResults->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function testReportAction()
    {
        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_HEAD_JS)
            ->addJs(APP_PATH . 'assets/js/react-pivot-standalone-3.0.0.min.js');

        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_HEAD_CSS)
            ->addCss(APP_PATH . 'assets/css/react-pivot.css');

        /** @var OperatingListItemRepository $operatingListItemRepository */
        $operatingListItemRepository = $this->entityManager->getRepository(OperatingListItem::class);
        $date = new DateTime();
        $date->setDateType(DateTime::TYPE_DATE);

        $totals = $operatingListItemRepository->getTotalQtyTillDay($date);
        $planedTime = 450;
        $data = [];
        $totalsPerItem = [];
        foreach ($totals as $row) {
            $totalsPerItem[$row['code']][] = (int)$row['total'];

            $data[] = [
                'product' => $row['code'],
                'name' => $row['code'],
                'date' => (new DateTime($row['date']))->getShortSerbianFormat(),
                'transaction' => [
                    'total' => $row['total'],
                    'totalWorker' => (int)$row['totalWorker'],
                    'realizedTime' => $row['realizedTime'],
                    'planedTime' => $planedTime * (int)$row['totalWorker']
                ]
            ];
        }

        $this->view->setVar('rows', json_encode($data));
        $this->view->pick('operating-lists/test-report');
    }

    public function testReport2Action()
    {
        $this->view->pick('operating-lists/test-report2');
    }

    public function getGroupedByProductReportsDataAction()
    {
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');

        $startDate = new DateTime($this->request->getQuery('startDate'));
        $endDate = new DateTime($this->request->getQuery('endDate'));
        $dateRange = new \Vemid\Date\DateRange($startDate, $endDate);

        $query = $this->modelsManager->createBuilder()
            ->columns([
                'ol.id as operatingListId',
                'COALESCE(SUM(oliw.qty), 0) as total',
                'IF(oliw.id IS NULL, 0, SUM(oliw.qty)) as totalS',
                'CONCAT(p.code, "-",  p.name) as code',
                'olit.name',
                'oliw.date',
                'COALESCE(SUM(oliw.qty * oli.pieceTime), 0) as realizedTime'
            ])
            ->addFrom(OperatingList::class, 'ol')
            ->leftJoin(\OperatingListItem::class, 'oli.operatingListId = ol.id', 'oli')
            ->leftJoin(\OperatingListItemType::class, 'oli.operatingListItemTypeId = olit.id', 'olit')
            ->leftJoin(\Product::class, 'ol.productId = p.id', 'p')
            ->leftJoin(\OperatingListItemWorksheet::class, 'oliw.operatingListItemId = oli.id', 'oliw')
            ->where('oliw.date BETWEEN :startDate: AND :endDate: OR oliw.id IS NULL', [
                'startDate' => $startDate->getUnixFormat(),
                'endDate' => $endDate->getUnixFormat(),
            ])
            ->groupBy('ol.id');

        $queryForCount = clone $query;
        $totalResults = $queryForCount->getQuery()->execute();

        $results = $query->limit($limit, $offset)->getQuery()->execute();

        /** @var OperatingListItemRepository $operatingListItemRepository */
        $operatingListItemRepository = $this->entityManager->getRepository(OperatingListItem::class);

        /** @var OperatingListItemWorksheetRepository $operatingListItemWorksheetRepository */
        $operatingListItemWorksheetRepository = $this->entityManager->getRepository(OperatingListItemWorksheet::class);
        /** @var ProductionOrderRepository $totals */
        $totals = $this->entityManager->getRepository(\ProductionOrder::class);

        $data = [];
        foreach ($results as $row) {
            /** @var OperatingList $operatingList */
            if (!$operatingList = $this->entityManager->findOne(OperatingList::class, $row['operatingListId'])) {
                continue;
            }

            $totalPlanned = $totals->getTotalPlanned($operatingList->getProduct());
            $totalProduced = $operatingListItemRepository->getTotalProducedProduct($operatingList, $endDate);
            $totalWorkersForPeriod = $operatingListItemWorksheetRepository->getTotalWorkersForPeriod($operatingList, $dateRange);
            $realizedTime = round($row['realizedTime'], 2);
            $plannedTime = 450 * $totalWorkersForPeriod;

            $class = '';
            if ($totalPlanned === (int)$totalProduced) {
                $class = 'alert-success';
            }

            if ($totalPlanned < $totalProduced) {
                $class = 'alert-danger';
            }

            $data[] = [
                $row['code'],
                $totalProduced . ' - (' . $totalPlanned . ')',
                $totalWorkersForPeriod,
                $realizedTime,
                $plannedTime,
                $plannedTime ? round(($realizedTime / $plannedTime) * 100, 2) : 0,
                $row['operatingListId'],
                $class
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $totalResults->count(),
            'recordsFiltered' => $totalResults->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getDetailedReportAction($id)
    {
        /** @var OperatingList $operatingList */
        if (!$operatingList = $this->entityManager->findOne(OperatingList::class, $id)) {
            return $this->returnNotFound();
        }

        $startDate = new DateTime($this->request->getPost('startDate'));
        $startDate->setDateType(DateTime::TYPE_DATE);
        $endDate = new DateTime($this->request->getPost('endDate'));
        $endDate->setDateType(DateTime::TYPE_DATE);

        $query = $this->modelsManager->createBuilder()
            ->columns([
                'ol.id as operatingListId',
                'COALESCE(SUM(oliw.qty), 0) as total',
                'IF(oliw.id IS NULL, 0, SUM(oliw.qty)) as totalS',
                'olit.name',
                'oliw.date',
                'COALESCE(SUM(oliw.qty * oli.pieceTime), 0) as realizedTime',
                'COALESCE(COUNT(distinct oliw.productionWorkerId), 0) as totalWorker',
                'oliw.date'
            ])
            ->addFrom(OperatingListItemWorksheet::class, 'oliw')
            ->leftJoin(\OperatingListItem::class, 'oliw.operatingListItemId = oli.id', 'oli')
            ->leftJoin(\OperatingListItemType::class, 'oli.operatingListItemTypeId = olit.id', 'olit')
            ->leftJoin(\OperatingList::class, 'oli.operatingListId = ol.id', 'ol')
            ->leftJoin(\Product::class, 'ol.productId = p.id', 'p')
            ->betweenWhere('oliw.date', $startDate->getUnixFormat(), $endDate->getUnixFormat())
            ->andWhere('ol.id = :id:', [
                'id' => $operatingList->getId()
            ])
            ->groupBy('oliw.date');

        $result = $query->getQuery()->execute()->toArray();

        $dateRange = new \Vemid\Date\DateRange($startDate, $endDate);
        $data = [];

        /** @var OperatingListItemRepository $operatingListItemRepository */
        $operatingListItemRepository = $this->entityManager->getRepository(OperatingListItem::class);

        foreach ($dateRange->getAllDates() as $date) {
            $dateOfWeek = (int)$date->format('N');
            $key = \array_search($date->getUnixFormat(), array_column($result, 'date'), false);

            $totalProducedProduct = $operatingListItemRepository->getTotalProducedProduct($operatingList, $date);
            $data[] = [
                'date' => $date->getShortSerbianFormat(),
                'total' => $dateOfWeek == 7 ? $totalProducedProduct : 0,
                'totalWorker' => $key !== false ? $result[$key]['totalWorker'] : 0,
                'realizedTime' => $key !== false ? $result[$key]['realizedTime'] : 0,
                'planned' => 0,
                'sunday' => $dateOfWeek === 7 ? 1 : 0
            ];
        }

        /** @var OperatingListRepository $operatingListRepository */
        $operatingListRepository = $this->entityManager->getRepository(OperatingList::class);
        $totalQty = $operatingListRepository->getTotalQtyTillDay($operatingList, $endDate);


        $this->view->setVar('data', $data);
        $this->view->setVar('total', $totalQty);
        $this->view->pick('operating-lists/get-detailed-report');
    }

    public function unlockAction($id)
    {
        /** @var OperatingList $operatingList */
        $operatingList = $this->entityManager->findOne(OperatingList::class, $id);

        if (!$operatingList) {
            return $this->returnNotFound();
        }



        $operatingList->setLocked(false);

        if (!$this->entityManager->save($operatingList)) {
            $this->addFlashMessagesFromEntity($operatingList);

            return;
        }

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t('Uspešno je zatvorena karta!'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function lockAction($id)
    {
        /** @var OperatingList $operatingList */
        $operatingList = $this->entityManager->findOne(OperatingList::class, $id);

        if (!$operatingList) {
            return $this->returnNotFound();
        }



        $operatingList->setLocked(true);

        if (!$this->entityManager->save($operatingList)) {
            $this->addFlashMessagesFromEntity($operatingList);

            return;
        }

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t('Uspešno je zatvorena karta!'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function addNoteAction($id)
    {
        if ($this->request->isPost()) {
            $post = $this->request->getPost();

            /** @var OperatingList $operatingList */
            if (!$operatingList = $this->entityManager->findOne(OperatingList::class, $id)) {
                return $this->returnNotFound();
            }

            $operatingList->setNote($post['note']);
            if (!$this->entityManager->save($operatingList)) {
                $this->addFlashMessagesFromEntity($operatingList);

                return;
            }

            $this->addFlashMessage(
                'Uspesno',
                null,
                \Vemid\Messenger\Message::SUCCESS
            );

            $this->response->redirect('/operating-lists/overview/'.$operatingList->getId());
        }
    }

    public function testPivotAction()
    {
        $this->view->pick('operating-lists/test-pivot');
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return OperatingList::class;
    }
}
