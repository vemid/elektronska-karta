<?php

use Vemid\Controller\CrudController;

/**
 * Class SuppliersController
 *
 * @package Default\Controllers
 */
class SuppliersController extends CrudController
{
    public function listAction()
    {
        $this->view->setVar('suppliers', $this->entityManager->find(Supplier::class,[
            'order' => 'name'
        ]));
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return Supplier::class;
    }
}
