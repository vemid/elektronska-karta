<?php

use \Vemid\Controller\CrudController;
use Vemid\Form\Renderer\Json as Renderer;

class ClassificationOldCollectionMapController extends CrudController
{
    public function listAction()
    {
        $oldCollectionMap = $this->entityManager->find(ClassificationOldCollectionMap::class,[
            'order' => 'id desc'
        ]);

        $this->view->setVar('oldCollectionMappings', $oldCollectionMap);
        $this->view->pick('classification-old-collection-map/list');
    }

    public function getCreateFormAction()
    {
        $form = new ClassificationOldCollectionMapForm(new ClassificationOldCollectionMap());

        return (new Renderer())->render($form);
    }

    public function getUpdateFormAction($id)
    {
        if (!$classificationOldCollectionMap = $this->getEntity($id)) {
            return $this->returnNotFound();
        }

        $form = new ClassificationOldCollectionMapForm($classificationOldCollectionMap);

        return (new Renderer())->render($form);
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return ClassificationOldCollectionMap::class;
    }
}