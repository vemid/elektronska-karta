<?php

use Vemid\Controller\CrudController;
use Vemid\Messenger\Message as MessengerMessage;
use \Vemid\Entity\Type;
use \Vemid\Service\Product\Hydrator;
use \Vemid\Task\Orders\AddShopOrderItems;
use \Vemid\Form\Renderer\Json as Renderer;

/**
 * Class OrderItemsController
 *
 * @package Default\Controllers
 */
class OrderItemsController extends CrudController
{

    public function getOrderFormAction($productId, $orderId)
    {
        /** @var Product $product */
        if (!$product = $this->entityManager->findOne(Product::class, $productId)) {
            return $this->returnNotFound();
        }

        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $orderId)) {
            return $this->returnNotFound();
        }


        $entityType = Type::CLIENT;
        /** @var Client $entity */
        $entity = $this->entityManager->findOne(Client::class, [
            Client::PROPERTY_USER_ID . ' = :userId:',
            'bind' => [
                'userId' => $this->currentUser->getId()
            ]
        ]);

        if (!$entity) {
            $entityType = Type::CODE;
            $entity = $this->entityManager->findOne(Code::class,[
                Code::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'code' => '99'
                ]
            ]);
        }

        if (!$entity) {
            return $this->returnNotFound();
        }

        /** @var \Vemid\Entity\Repository\ClientOrderItemRepository $orderItemRepository */
        $orderItemRepository = $this->entityManager->getRepository(ClientOrderItem::class);

        $query = $this->modelsManager->createBuilder()
            ->addFrom(\ProductSize::class, 'ps')
            ->leftJoin(\Code::class, 'ps.codeId = c.id', 'c')
            ->leftJoin(\Product::class, 'p.id = ps.productId', 'p')
            ->where('p.id = :id:', [
                'id' => $product->getId()
            ])
            ->orderBy('c.code');

        $productSizes = $query->getQuery()->execute();

        /** @var \Pricelist $priceListEUR */
        $priceListEUR = $this->getDI()->getEntityManager()->findOne(\Pricelist::class, [
            'name = :name:',
            'bind' => [
                'name' => 'DEVIZNI'
            ]
        ]);
        $productPrices = $this->getDI()->getProductPrices();
        $wholesalePriceEUR = $productPrices->getWholesalePrice($priceListEUR, $product) ? $productPrices->getWholesalePrice($priceListEUR, $product) : 0;

        $hydrator = new Hydrator($product, $this->entityManager);

        $this->view->setVar('product', $product);
        $this->view->setVar('wholesalePriceEUR', $wholesalePriceEUR);
        $this->view->setVar('hydrator', $hydrator);
        $this->view->setVar('productSizes', $productSizes);
        $this->view->setVar('orderManager', $orderItemRepository);
        $this->view->setVar('entity', $entity);
        $this->view->setVar('entityType', $entityType);
        $this->view->setVar('order', $order);
        $this->view->pick('order-items/get-order-form');
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $postData = $this->request->getPost();

        if (empty($postData['order'])) {
            return $this->returnNotFound();
        }

        /** @var Order $order */
        $order = $this->entityManager->findOne(Order::class, [
            Order::PROPERTY_ID . ' = :orderId:',
            'bind' => [
                'orderId' => $postData[OrderItem::PROPERTY_ORDER_ID]
            ]
        ]);

        $entityType = Type::CLIENT;
        /** @var Client $entity */
        $entity = $this->entityManager->findOne(Client::class, [
            Client::PROPERTY_USER_ID . ' = :userId:',
            'bind' => [
                'userId' => $this->currentUser->getId()
            ]
        ]);

        if (!$entity) {
            $entityType = Type::CODE;
            $entity = $this->entityManager->findOne(Code::class,[
                Code::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'code' => '99'
                ]
            ]);
        }

        if (!$entity) {
            return $this->returnNotFound();
        }

        foreach ($postData['order'] as $productSizeId => $values) {

            /** @var ProductSize $productSize */
            if (!$productSize = $this->entityManager->findOne(ProductSize::class, $productSizeId)) {
                throw new \LogicException('Product size not found!');
            }

            /** @var ClientOrderItem $orderItem */
            $orderItem = $this->entityManager->findOne(ClientOrderItem::class, [
                \OrderItem::PROPERTY_ORDER_ID . ' = :orderId: AND ' .
                \OrderItem::PROPERTY_PRODUCT_SIZE_ID . ' = :productSizeId: AND ' .
                \OrderItem::PROPERTY_ENTITY_ID . ' = :entityId: AND ' .
                \OrderItem::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:',
                'bind' => [
                    'orderId' => $order->getId(),
                    'productSizeId' => $productSize->getId(),
                    'entityId' => $entity->getId(),
                    'entityTypeId' => $entityType
                ]
            ]);

            if (!$orderItem && empty($values['a']) && empty($values['b'])) {
                continue;
            }

            if ($orderItem && empty($values['a']) && empty($values['b'])) {
                $orderItem->getOrderItem()->delete();
                $this->entityManager->delete($orderItem);
                continue;
            }

            if (!$orderItem) {
                $orderItem = new ClientOrderItem();
                $orderItem->setProductSize($productSize);
                $orderItem->setClient($entity);
                $orderItem->setOrder($order);
            }

            $orderItem->setQuantityA($values['a'] ?? null);
            $orderItem->setQuantityB($values['b'] ?? null);

            if (!$this->entityManager->save($orderItem)) {
                $this->addFlashMessagesFromEntity($orderItem);

                return;
            }
        }

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t('Data was saved successfully.'),
            null,
            MessengerMessage::SUCCESS
        );
    }

    public function checkErrorsAction($id)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $id)) {
            return $this->returnNotFound();
        }

        /** @var Phalcon\Http\Request\File $file */
        $file = array_pop($this->request->getUploadedFiles());

        if ($file !== null) {
            $filePath = sprintf('%s/%s.%s',
                APP_PATH . 'var/uploads/files',
                substr(sha1(mt_rand()), 0, 8),
                $file->getExtension()
            );

            if ($file->moveTo($filePath)) {
                $this->session->set('orderItems__' . $order->getId(), [
                    'file' => $filePath
                ]);

                $this->addFlashMessage(
                    $this->getDI()->getTranslator()->t('File je uploadovan. Trenutno se obradjuju greške!'),
                    null,
                    \Vemid\Messenger\Message::SUCCESS
                );

                return;
            }

            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('Nesto je pošlo po zlu!'),
                null,
                \Vemid\Messenger\Message::DANGER
            );
        }
    }

    public function importShopsOrderAction($orderId)
    {
        /** @var Order $order */
        if (!$order = $this->entityManager->findOne(Order::class, $orderId)) {
            return $this->returnNotFound();
        }

        if (!$this->session->has('orderItems__' . $order->getId())) {
            throw new LogicException('File not found');
        }

        $file = $this->session->get('orderItems__' . $order->getId())['file'];

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($file);
        $objPHPExcel->setActiveSheetIndex();
        $sheetData = $objPHPExcel->getActiveSheet()->toArray();
        $counter = 0;
        $indexModel = 0;
        $indexShop = 1;
        $indexQty = 2;
        $indexSize = 3;

        foreach ($sheetData as $row) {
            $array_filter = array_filter($row);
            if (count($array_filter) !== 4) {
                continue;
            }

            if ($counter === 0) {
                $arrayHeader = array_map('strtolower', $array_filter);
                $arrayHeader = array_map('trim', $arrayHeader);
                $flippedData = array_flip($arrayHeader);
                $indexModel = $flippedData['model'];
                $indexShop = $flippedData['radnja'];
                $indexQty = $flippedData['kolicina'];
                $indexSize = $flippedData['velicina'];
                $counter++;
                continue;
            }

            $filteredData = array_filter($array_filter);
            $shopCode = $filteredData[$indexShop];
            $productCode = $filteredData[$indexModel];
            $sizeCode = $filteredData[$indexSize];
            $qtyA = $filteredData[$indexQty];

            $shopTask = new AddShopOrderItems();
            $shopTask->order = $order;
            $shopTask->sizeCode = $sizeCode;
            $shopTask->productCode = $productCode;
            $shopTask->shopCode = $shopCode;
            $shopTask->qtyA = (int)$qtyA;
            $shopTask->runInBackground();
        }

        $this->session->remove('orderItems__' . $order->getId());

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t('Data was saved successfully.'),
            null,
            MessengerMessage::SUCCESS
        );
    }

    public function createInitialAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $postData = $this->request->getPost();

        /** @var ProductSize $productSize */
        if (!$productSize = $this->entityManager->findOne(ProductSize::class, $postData['productSizeId'])) {
            return $this->returnNotFound();
        }



        if($postData['type'] == "shop") {
            /** @var Code $orderItemEntityId */
            if (!$orderItemEntityId = $this->entityManager->findOne(Code::class, $postData['entityId'])) {
                return $this->addFlashMessage("Ne postoji takva radnja", null, \Vemid\Messenger\Message::DANGER);
            }
            $type = Type::CODE;
        }
        elseif ($postData['type'] == "client") {
            /** @var Code $orderItemEntityId */
            if (!$orderItemEntityId = $this->entityManager->findOne(Client::class, $postData['entityId'])) {
                return $this->addFlashMessage("Ne postoji takva radnja", null, \Vemid\Messenger\Message::DANGER);
            }
            $type = Type::CLIENT;
        }


        /** @var Product $product */
        $product = $productSize->getProduct();
        $hydrator = new Hydrator($product, $this->getDI()->getEntityManager());
        $oldCollection = $hydrator->getOldCollection()->getId();


        /** @var OrderClassification $order */
        if (!$order = $this->entityManager->findOne(OrderClassification::class, [
            'classificationId = :oldCollection:',
            'bind' => [
                'oldCollection' => $oldCollection
            ]
        ])) {
            return $this->addFlashMessage(
                "Ne postoji kreirana lista za porucivanje",
                null,
                \Vemid\Messenger\Message::DANGER);
        }

        $orderItems = new OrderItem();
        $orderItems->setOrderId($order->getOrderId());
        $orderItems->setEntityTypeId($type);
        $orderItems->setEntityId($orderItemEntityId->getId());
        $orderItems->setProductSize($productSize);
        $orderItems->setQuantityA($postData['value']);
        $orderItems->setQuantityB(0);
        $orderItems->save();

        return $this->addFlashMessage("Uspesno uneta kolicina",null, \Vemid\Messenger\Message::SUCCESS);



    }

    public function editInitialAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $postData = $this->request->getPost();



        /** @var OrderItem $orderItem */
        if (!$orderItem = $this->entityManager->findOne(OrderItem::class, $postData['orderItemId'])) {
            return $this->returnNotFound();
        }

        $orderItem->setQuantityA($postData['value']);
        $orderItem->save();


        return $this->addFlashMessage("Uspesno editovana kolicina",null, \Vemid\Messenger\Message::SUCCESS);

    }

    public function deleteInitialFormAction()
    {


        $form = new DeleteInitialForm();

        return (new Renderer())->render($form);

    }

    public function deleteInitialAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $postData = $this->request->getPost();

        /** @var Product $product */
        if (!$product = $this->entityManager->findOne(Product::class, $postData['hiddenInitialProductId'])) {
            return $this->returnNotFound();
        }

        if($postData['type'] == 1) {
            $type = Type::CODE;
        }
        elseif ($postData['type'] == 2) {
            $type = Type::CLIENT;
        }
        else {
            return $this->addFlashMessage("Ne postoji tip klijenta",null, \Vemid\Messenger\Message::DANGER);
            exit();
        }


        $query = $this->modelsManager->createBuilder()
            ->addFrom(OrderItem::class, 'oi')
            ->leftJoin(ProductSize::class, 'ps.id = oi.productSizeId', 'ps')
            ->leftJoin(Product::class, 'p.id = ps.productId', 'p')
            ->where('p.id = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('oi.entityTypeId = :entityTypeId:',[
                'entityTypeId' => $type
            ]);

        /** @var OrderItem[] $orderItems */
        $orderItems = $query->getQuery()->execute();


        foreach ($orderItems as $orderItem) {

            $this->entityManager->delete($orderItem);

        }

        return $this->addFlashMessage("Uspesno ponistena porudzbina",null, \Vemid\Messenger\Message::SUCCESS);
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return OrderItem::class;
    }
}
