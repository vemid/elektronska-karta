<?php

use Vemid\Controller\CrudController;
use \Vemid\Messenger\Message;
use \Vemid\Form\Renderer\Json as Renderer;
use \Phalcon\Forms\Element\Hidden;

/**
 * Class ClientsController
 *
 * @package Default\Controllers
 */
class QuestionnairesController extends CrudController
{

    public function getCreateFormAction()
    {
        $params = $this->dispatcher->getParams();

        /** @var ElectronCard $electronCard */
        if (!$electronCard = $this->entityManager->findOne(ElectronCard::class, $params[1])) {
            return $this->returnNotFound();
        }

        $product = $electronCard->getProduct();

        $form = $this->getForm(null, [
            Questionnaire::PROPERTY_USER_ID,
            Questionnaire::PROPERTY_IS_ANSWERED,
        ]);

        $select = new Hidden('electronCard');
        $select->setDefault($electronCard->getId());


        if ($form->has(Questionnaire::PROPERTY_PRODUCT_ID)) {
            $form->get(Questionnaire::PROPERTY_PRODUCT_ID)->setDefault($product->getId());
        }

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var Code[] $codes */
        $codes = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId(),
            ]
        ]);

        $options = [];
        foreach ($codes as $code) {
            $options[$code->getId()] = $code->getName();
        }

        /** @var Questionnaire[] $questionnaires */
        $questionnaires = $this->entityManager->find(Questionnaire::class, [
            Questionnaire::PROPERTY_PRODUCT_ID . ' = :productId:',
            'bind' => [
                'productId' => $product->getId()
            ]
        ]);

        foreach ($questionnaires as $questionnaire) {
            if (array_key_exists($questionnaire->getCodeId(), $options)) {
                unset($options[$questionnaire->getCodeId()]);
            }
        }

        if ($form->has(Questionnaire::PROPERTY_CODE_ID)) {
            $form->get(Questionnaire::PROPERTY_CODE_ID)->setOptions(array_map('strval', $options));
            $form->get(Questionnaire::PROPERTY_CODE_ID)->setAttribute('class', 'search');
        }

        $form->add($select);

        $renderer = new Renderer();
        $data = $renderer->render($form);
        $data[Questionnaire::PROPERTY_PRODUCT_ID]['type'] = 'hidden';

        return $data;
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $postData = $this->request->getPost();

        /** @var ElectronCard $electronCard */
        $electronCard = $this->entityManager->findOne(ElectronCard::class, $postData['electronCard']);
        if (!$electronCard) {
            return $this->returnNotFound();
        }

        /** @var Questionnaire $questionnaire */
        $questionnaire = $this->entityManager->findOne(Questionnaire::class, [
            Questionnaire::PROPERTY_PRODUCT_ID . ' = :productId: AND ' .
            Questionnaire::PROPERTY_IS_ANSWERED . ' = :isAnswered: AND ' .
            Questionnaire::PROPERTY_CODE_ID . ' = :codeId:',
            'bind' => [
                'productId' => $electronCard->getProductId(),
                'isAnswered' => false,
                'codeId' => $postData[Questionnaire::PROPERTY_CODE_ID],
            ]
        ]);

        if ($questionnaire) {
            $this->addFlashMessage(
                $this->_translate('Upitnik već postoji!'),
                null,
                Message::WARNING
            );

            return;
        }

        $postData[Questionnaire::PROPERTY_USER_ID] = $this->currentUser->getId();
        $postData[Questionnaire::PROPERTY_IS_ANSWERED] = 0;
        $postData[Questionnaire::PROPERTY_PRODUCT_ID] = $electronCard->getProductId();

        $questionnaire = new Questionnaire();
        $form = $questionnaire->getForm();
        if ($form->isValid($postData)) {
            if (!$this->saveEntity($questionnaire)) {
                $this->addFlashMessagesFromEntity($questionnaire);
            } else {
                $this->dispatcher->setParam('url', '/electron-cards/overview/' .$electronCard->getId() . '#tabQuestionnaire');
            }
        } else {
            $this->addFlashMessagesFromForm($form);
        }
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return Questionnaire::class;
    }
}
