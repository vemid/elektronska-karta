<?php

use \Vemid\Controller\BaseController;
use \Vemid\Date\DateTime;
use \Vemid\Date\Time;
use \Vemid\Date\DateRange;
use Vemid\Service\OperationList\ItemCalculator;
use \Vemid\Service\Report\PaymentReport;
use \Vemid\Service\Sector;

/**
 * Class ReportsController
 *
 * @package Default\Controllers
 */
class ReportsController extends BaseController
{

    public function paymentReportAction()
    {
        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_ID . ' IN ({codeQuery:array})',
            'bind' => [
                'codeQuery' => (new Sector\Manager($this->entityManager))->getProductionSectors()
            ],
            'order' => 'name'
        ]);

        $this->view->pick('reports/payment-report');
        $this->view->setVar('sectors', $sectors);

        if ($this->request->isPost()) {

            $paramDateFrom = $this->request->getPost('dateFrom');
            $paramDateTo = $this->request->getPost('dateTo');
            $paramSector = $this->request->getPost('sectorId');

            /** @var Code $codeId */
            $code = $this->entityManager->findOne(Code::class, $paramSector);

            $dateFrom = new DateTime($paramDateFrom);
            $dateTo = new DateTime($paramDateTo);

            if (!$code) {
                return $this->returnNotFound();
            }

            $task = new \Vemid\Task\Report\WeeklyPaymentReportTask();
            $task->sector = $code;
            $task->startDate = $dateFrom;
            $task->endDate = $dateTo;
            $task->runInBackground();

            $this->flashSession->success('Poruka');
            $this->response->redirect('reports/payment-report');
        }
    }

    public function paymentReportBySectorAction()
    {

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_ID . ' IN ({codeQuery:array})',
            'bind' => [
                'codeQuery' => (new Sector\Manager($this->entityManager))->getProductionSectors()
            ],
            'order' => 'name'
        ]);

        $this->view->pick('reports/payment-report-by-sector');
        $this->view->setVar('sectors', $sectors);

    }

    public function paymentReportBySectorDataAction()
    {
        $paramDateFrom = $this->request->get('dateFrom');
        $paramDateTo = $this->request->get('dateTo');
        $paramSector = $this->request->get('sectorId');

        /** @var Code $code */
        $code = $this->entityManager->findOne(Code::class, $paramSector);
        $dateFrom = new DateTime($paramDateFrom);
        $dateTo = new DateTime($paramDateTo);

        if (!$code) {
            return $this->returnNotFound();
        }

        $payment = new PaymentReport($this->entityManager);
        $paymentDatas = $payment->getPaymentReportBySectorData($dateFrom, $dateTo, $code);
        $totalData = $payment->getPaymentReportBySectorDataTotal($dateFrom, $dateTo, $code);

        $this->view->pick('reports/data/payment-report-by-sector-data');
        $this->view->setVar('datas', $paymentDatas);
        $this->view->setVar('totalDatas', $totalData);
        $this->view->setVar('time', new Time('00:00'));
        $this->view->setVar('paramDateFrom', $paramDateFrom);
        $this->view->setVar('paramDateTo', $paramDateTo);
        $this->view->setVar('paramSector', $paramSector);
    }

    public function performanceReviewAction()
    {

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_ID . ' IN ({codeQuery:array})',
            'bind' => [
                'codeQuery' => (new Sector\Manager($this->entityManager))->getProductionSectors()
            ],
            'order' => 'name'
        ]);

        $this->view->pick('reports/performance-review');
        $this->view->setVar('sectors', $sectors);

    }

    public function performanceReviewDataAction()
    {
        $paramDateFrom = $this->request->get('dateFrom');
        $paramDateTo = $this->request->get('dateTo');
        $paramSector = $this->request->get('sectorId');

        /** @var Code $code */
        $code = $this->entityManager->findOne(Code::class, $paramSector);
        $dateFrom = new DateTime($paramDateFrom);
        $dateTo = (new DateTime($paramDateTo))->modify('+1 day');

        if (!$code) {
            return $this->returnNotFound();
        }

        $payment = new PaymentReport($this->entityManager);
        $performanceReviews = $payment->getSectorPerformanceReview($dateFrom, $dateTo, $code);
        $performanceTotalReviews = $payment->getSectorPerformanceReviewTotal($dateFrom, $dateTo, $code);

        $str = 0;

        $this->view->pick('reports/data/performance-review-data');
        $this->view->setVar('datas', $performanceReviews);
        $this->view->setVar('totalDatas', $performanceTotalReviews);
        $this->view->setVar('time', new Time('00:00'));
        $this->view->setVar('paramDateFrom', $paramDateFrom);
        $this->view->setVar('paramDateTo', $paramDateTo);
        $this->view->setVar('paramSector', $paramSector);
    }


    public function printPaymentReportBySectorDataAction()
    {
        $this->view->disable();

        $params = $this->dispatcher->getParams();
        $paramDateFrom = $params[0];
        $paramDateTo = $params[1];
        $paramSector = $params[2];

        /** @var Code $code */
        $code = $this->entityManager->findOne(Code::class, $paramSector);
        $dateFrom = new DateTime($paramDateFrom);
        $dateTo = new DateTime($paramDateTo);

        if (!$code) {
            return $this->returnNotFound();
        }

        $payment = new PaymentReport($this->entityManager);
        $paymentDatas = $payment->getPaymentReportBySectorData($dateFrom, $dateTo, $code);
        $totalData = $payment->getPaymentReportBySectorDataTotal($dateFrom, $dateTo, $code);

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();

        $html = $this->getDI()->getSimpleView()->render('reports/data/print-payment-report-by-sector-data', [
            'datas' => $paymentDatas,
            'totalData' => $totalData,
            'code' => $code,
            'dateTo' => $dateTo,
            'dateFrom' => $dateFrom,
            'siteUrl' => ltrim($config->application->baseUri, '/')
        ]);

        $tempName = $code->getName() . '-' . $dateFrom->getShortSerbianFormat() . '-' . $dateTo->getShortSerbianFormat();
        $renderer = $this->getDI()->getPdfRenderer();
        $renderer->setPageSize('210mm', '550mm');
        $renderer->setMargins('2mm','5mm','2mm','5mm');

        $content = $renderer->render($html, 'landscape');
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $tempName);

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;

    }

    public function paymentReportByWorkerAction()
    {

        /** @var ProductionWorker $productionWorkers */
        $productionWorkers = $this->entityManager->find(ProductionWorker::class);

        $this->view->pick('reports/payment-report-by-worker');
        $this->view->setVar('productionWorkers', $productionWorkers);

    }

    public function paymentReportByWorkerDataAction()
    {

        $paramDateFrom = new DateTime($this->request->get('dateFrom'));
        $paramDateTo = (new DateTime($this->request->get('dateTo')))->modify('+1 day');
        $paramWorker = $this->request->get('workerId');

        /** @var \ProductionWorker $productionWorker */
        $productionWorker = $this->entityManager->findOne(ProductionWorker::class, $paramWorker);

        if (!$productionWorker) {
            return $this->returnNotFound();
        }

        $payment = new PaymentReport($this->entityManager);
        $paymentDatas = $payment->getPaymentReportByWorkerData($paramDateFrom, $paramDateTo, $productionWorker);
        $totalData = $payment->getPaymentReportByWorkerDataTotal($paramDateFrom, $paramDateTo, $productionWorker);


        $this->view->pick('reports/data/payment-report-by-worker-data');
        $this->view->setVar('datas', $paymentDatas);
        $this->view->setVar('totalDatas', $totalData);
        $this->view->setVar('time', new Time('00:00'));
        $this->view->setVar('paramDateFrom', $this->request->get('dateFrom'));
        $this->view->setVar('paramDateTo', $this->request->get('dateTo'));
        $this->view->setVar('paramWorker', $productionWorker->getId());
    }

    public function printPaymentReportByWorkerDataAction()
    {
        $this->view->disable();

        $params = $this->dispatcher->getParams();
        $paramDateFrom = (new DateTime($params[0]));
        $paramDateTo = (new DateTime($params[1]))->modify('+1 day');
        $paramWorker = $params[2];

        /** @var \ProductionWorker $productionWorker */
        $productionWorker = $this->entityManager->findOne(ProductionWorker::class, $paramWorker);

        if (!$productionWorker) {
            return $this->returnNotFound();
        }

        $payment = new PaymentReport($this->entityManager);
        $paymentDatas = $payment->getPaymentReportByWorkerData($paramDateFrom, $paramDateTo, $productionWorker);

        $totalData = $payment->getPaymentReportByWorkerDataTotal($paramDateFrom, $paramDateTo, $productionWorker);

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();

        $html = $this->getDI()->getSimpleView()->render('reports/data/print-payment-report-by-worker-data', [
            'datas' => $paymentDatas,
            'totalDatas' => $totalData,
            'productionWorker' => $productionWorker,
            'dateTo' => $params[1],
            'dateFrom' => $params[0],
            'siteUrl' => ltrim($config->application->baseUri, '/')
        ]);

        $tempName = $productionWorker->getDisplayName() . '-' . $paramDateFrom->getShortSerbianFormat() . '-' . $paramDateTo->getShortSerbianFormat();
        $renderer = $this->getDI()->getPdfRenderer();
        $renderer->setPageSize('210mm', '550mm');
        $renderer->setMargins('2mm','5mm','2mm','5mm');

        $content = $renderer->render($html, 'landscape');
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $tempName);

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;

    }

    public function outTermBySectorWorkerAction()
    {

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_ID . ' IN ({codeQuery:array})',
            'bind' => [
                'codeQuery' => (new Sector\Manager($this->entityManager))->getProductionSectors()
            ],
            'order' => 'name'
        ]);

        $this->view->pick('reports/out-term-by-sector-worker');
        $this->view->setVar('sectors', $sectors);

    }

    public function outTermBySectorWorkerDataAction()
    {
        $paramDateFrom = $this->request->get('dateFrom');
        $paramDateTo = $this->request->get('dateTo');
        $paramSector = $this->request->get('sectorId');

        /** @var Code $code */
        $code = $this->entityManager->findOne(Code::class, $paramSector);
        $dateFrom = new DateTime($paramDateFrom);
        $dateTo = new DateTime($paramDateTo);

        if (!$code) {
            return $this->returnNotFound();
        }

        $itemCaluclator = new ItemCalculator($this->entityManager);

        $query = $this->modelsManager->createBuilder()
            ->addFrom(\OperatingListItemWorksheet::class, 'p')
            ->leftJoin(\ProductionWorker::class, 'p.productionWorkerId = pw.id', 'pw')
            ->where('pw.codeId = :codeId:', [
                'codeId' => $code->getId()
            ])
            ->andWhere('p.date >= :dateFrom:', [
                'dateFrom' => $dateFrom
            ])
            ->andWhere('p.date <= :dateTo:', [
                'dateTo' => $dateTo
            ]);

        /** @var \OperatingListItemWorksheet[] $operationWorksheetItems */
        $operationWorksheetItems = $query->getQuery()->execute();

        $data = [];
        foreach ($operationWorksheetItems as $operationWorksheetItem) {
            if ($itemCaluclator->outTermPerWorksheetItem($operationWorksheetItem) > 100 or $itemCaluclator->outTermPerWorksheetItem($operationWorksheetItem) < 50) {
                $data[] = [
                    'name' => $operationWorksheetItem->getProductionWorker(),
                    'operation' => $operationWorksheetItem,
                    'outTerm' => $itemCaluclator->outTermPerWorksheetItem($operationWorksheetItem)
                ];
            }
        }

        usort($data, function ($a, $b) {
            return (float)$b['outTerm'] > (float)$a['outTerm'];
        });

        $nesto = "break";

        $this->view->pick('reports/data/out-term-by-sector-worker-data');
        $this->view->setVar('datas', $data);
        $this->view->setVar('dateFrom', $dateFrom->format('Y-m-d'));
        $this->view->setVar('dateTo', $dateTo->format('Y-m-d'));

    }

    public function outTermBySectorAction()
    {

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);


        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_ID . ' IN ({codeQuery:array})',
            'bind' => [
                'codeQuery' => (new Sector\Manager($this->entityManager))->getProductionSectors()
            ],
            'order' => 'name'
        ]);

        $this->view->pick('reports/out-term-by-sector');
        $this->view->setVar('sectors', $sectors);

    }

    public function outTermBySectorDataAction()
    {
        $paramDateFrom = $this->request->get('dateFrom');
        $paramDateTo = $this->request->get('dateTo');
        $paramSector = $this->request->get('sectorId');

        /** @var Code $code */
        $code = $this->entityManager->findOne(Code::class, $paramSector);
        $dateFrom = new DateTime($paramDateFrom);
        $dateTo = new DateTime($paramDateTo);

        if (!$code) {
            return $this->returnNotFound();
        }

        /** @var \Vemid\Entity\Repository\OperatingListItemWorksheetRepository $operatingListItemWorksheets */
        $operatingListItemWorksheets = $this->entityManager->getRepository(OperatingListItemWorksheet::class);

        $result = $operatingListItemWorksheets->getOutTermBySector($code,$dateFrom, $dateTo);

        $string = "break";

        $this->view->pick('reports/data/out-term-by-sector-data');
        $this->view->setVar('datas', $result);


    }

    public function printPerformanceReviewDataAction()
    {
        $this->view->disable();

        $params = $this->dispatcher->getParams();
        $paramDateFrom = $params[0];
        $paramDateTo = $params[1];
        $paramSector = $params[2];

        /** @var Code $code */
        $code = $this->entityManager->findOne(Code::class, $paramSector);
        $dateFrom = new DateTime($paramDateFrom);
        $dateTo = new DateTime($paramDateTo);

        if (!$code) {
            return $this->returnNotFound();
        }

        $payment = new PaymentReport($this->entityManager);
        $performanceReviews = $payment->getSectorPerformanceReview($dateFrom, $dateTo, $code);
        $performanceTotalReviews = $payment->getSectorPerformanceReviewTotal($dateFrom, $dateTo, $code);


        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();

        $html = $this->getDI()->getSimpleView()->render('reports/data/print-performance-review-data2', [
            'datas' => $performanceReviews,
            'totalDatas' => $performanceTotalReviews,
            'code' => $code,
            'dateTo' => $dateTo,
            'dateFrom' => $dateFrom,
            'siteUrl' => ltrim($config->application->baseUri, '/')
        ]);

        $tempName = $code->getName() . '-' . $dateFrom->getShortSerbianFormat() . '-' . $dateTo->getShortSerbianFormat();
        $renderer = $this->getDI()->getPdfRenderer();
        $renderer->setPageSize('210mm', '550mm');
        $renderer->setMargins('2mm','5mm','2mm','5mm');

        $content = $renderer->render($html, 'landscape');
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $tempName);

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;

    }


    public function paymentReportBySectorAndWorkerAction()
    {

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_ID . ' IN ({codeQuery:array})',
            'bind' => [
                'codeQuery' => (new Sector\Manager($this->entityManager))->getProductionSectors()
            ],
            'order' => 'name'
        ]);

        $this->view->pick('reports/payment-report-by-sector-and-worker');
        $this->view->setVar('sectors', $sectors);

    }

    public function paymentReportBySectorAndWorkerDataAction()
    {

        $paramDateFrom = $this->request->get('dateFrom');
        $paramDateTo = $this->request->get('dateTo');
        $paramSector = $this->request->get('sectorId');

        /** @var Code $code */
        $code = $this->entityManager->findOne(Code::class, $paramSector);
        $dateFrom = new DateTime($paramDateFrom);
        $dateTo = new DateTime($paramDateTo);
        $printer = new \Vemid\Services\Excel\Reports\PaymentReportBySectorAndWorker($code,$dateFrom,$dateTo);
        $printer->printFile();
    }


}
