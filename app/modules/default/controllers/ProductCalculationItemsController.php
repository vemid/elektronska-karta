<?php

use Vemid\Controller\CrudController;
use \Vemid\Service\Product\Hydrator;

/**
 * Class ProductCalculationItemsController
 *
 * @package Frontemd\Controllers
 */
class ProductCalculationItemsController extends CrudController
{
    public function listAction($id)
    {
        /** @var \ProductCalculation $productCalculation */
        if (!$productCalculation = $this->entityManager->findOne(ProductCalculation::class, $id)) {
            return $this->returnNotFound();
        }

        $hydrator = new Hydrator($productCalculation->getProduct(), $this->entityManager);

        /** @var \ProductCalculation $similarProductCalculation */
        $similarProductCalculation = $this->entityManager->findOne(ProductCalculation::class, [
            ProductCalculation::PROPERTY_PRODUCT_ID . ' = :productId: AND ' .
            ProductCalculation::PROPERTY_ID . ' != :id: AND ' .
            ProductCalculation::PROPERTY_STATUS . ' = :status:',
            'bind' => [
                'productId' => $productCalculation->getProductId(),
                'status' => ProductCalculation::STATUS_PLANED,
                'id' => $id
            ]
        ]);

        $query = $this->modelsManager->createBuilder()
            ->addFrom(ProductCalculation::class, 'pc')
            ->leftJoin(Product::class, 'p.id = pc.productId', 'p')
            ->leftJoin(CalculationMapping::class, 'pc.calculationMappingId = cm.id', 'cm')
            ->where('p.code LIKE :colorCode:', [
                'colorCode' => '%' . $hydrator->getProductGroup()->getCode() . '%'
            ])
            ->andWhere('p.id != :id:', [
                'id' => $productCalculation->getProductId()
            ])
            ->andWhere('pc.id != :calculationId:', [
                'calculationId' => $productCalculation->getId()
            ]);

        /** @var ProductCalculation[] $productCalculations */
        $productCalculations = $query->getQuery()->execute();

        $options = [];
        foreach ($productCalculations as $productSimCalculation) {
            $options[$productSimCalculation->getId()] = sprintf('%s (%s)',
                $productSimCalculation->getProduct(),
                $productSimCalculation->getStatus() === ProductCalculation::STATUS_PLANED ? 'Planska' : 'Finalna'
            );
        }

        if ($similarProductCalculation) {
            $options[$similarProductCalculation->getId()] = sprintf('%s (%s)',
                $similarProductCalculation->getProduct()->getCode(),
                $similarProductCalculation->getStatus() === ProductCalculation::STATUS_PLANED ? 'Planska' : 'Finalna'
            );
        }

        /** @var CodeType $originType */
        $originType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . '= :code: ',
            'bind' => [
                'code'=> 'ORIGIN'
            ]
        ]);

        $originOptions = $this->entityManager->find(Code::class,[
            Code::PROPERTY_CODE_TYPE_ID .' = :codeTypeId:',
            'bind' => [
                'codeTypeId'=> $originType->getId()
            ],
            'order' => 'name'
        ]);

        $price = new Vemid\Service\Product\Price($this->entityManager, $productCalculation, null);

        $productRepository = new \Vemid\Entity\Repository\ProductRepository($this->modelsManager);
        $productTotal = $productRepository->getOrderTotal($productCalculation->getProduct());
        $qty = $productCalculation->getQty() ? $productCalculation->getQty() : $productTotal['komada'];

        $this->view->setVar('eurWholesaprice', $price->getEurWholesalePrice());
        $this->view->setVar('total', $price->getTotal());
        $this->view->setVar('composition', $productCalculation->getComposition());
        $this->view->setVar('qty', $qty);
        $this->view->setVar('productPrices', $price->getPrices($productCalculation->getProduct()));
        $this->view->setVar('productPlanedPrices', $price->getPlanedPrices($productCalculation->getProduct()));
        $this->view->setVar('productCalculation', $productCalculation);
        $this->view->setVar('oldCollection', $hydrator->getOldCollection());
        $this->view->setVar('collection', $hydrator->getCollection()->getName());
        $this->view->setVar('productCalculationItems', $productCalculation->getProductCalculationItems());
        $this->view->setVar('similarProductCalculation', $productCalculation->getStatus() === ProductCalculation::STATUS_FINAL && $similarProductCalculation ? $similarProductCalculation : null);
        $this->view->setVar('options', $options);
        $this->view->setVar('originOptions', $originOptions);
        $this->view->pick('product-calculation-items/list');
    }

    public function printAction()
    {
        $this->view->disable();

        $params = $this->dispatcher->getParams();
        /** @var \ProductCalculation $productCalculation */
        if (!$productCalculation = $this->entityManager->findOne(ProductCalculation::class, $params[0])) {
            return $this->returnNotFound();
        }

        $price = new Vemid\Service\Product\Price($this->entityManager, $productCalculation, null);
        $hydrator = new Hydrator($productCalculation->getProduct(), $this->entityManager);

        $markUp = $productCalculation->getCalculationMapping() ? $productCalculation->getCalculationMapping()->getMarkUp() : 0;

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();

        $html = $this->getDI()->getSimpleView()->render('product-calculation-items/print', [
            'productCalculation' => $productCalculation,
            'productPrices' => $price->getPrices($productCalculation->getProduct()),
            'oldCollection' => $hydrator->getOldCollection(),
            'markUp' => $markUp,
            'siteUrl' => ltrim($config->application->baseUri, '/')
        ]);

        $renderer = $this->getDI()->getPdfRenderer();
        $renderer->setMargins(0,5,0,5);

        $content = $renderer->render($html, 'portrait');
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $productCalculation->getProduct()->getName());

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;

    }

    public function print2Action()
    {
        //$this->view->disable();

        $params = $this->dispatcher->getParams();
        /** @var \ProductCalculation $productCalculation */
        if (!$productCalculation = $this->entityManager->findOne(ProductCalculation::class, $params[0])) {
            return $this->returnNotFound();
        }

        $price = new Vemid\Service\Product\Price($this->entityManager, $productCalculation, null);
        $hydrator = new Hydrator($productCalculation->getProduct(), $this->entityManager);

        $markUp = $productCalculation->getCalculationMapping() ? $productCalculation->getCalculationMapping()->getMarkUp() : 0;

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();

//        $html = $this->getDI()->getSimpleView()->render('product-calculation-items/print', [
//            'productCalculation' => $productCalculation,
//            'productPrices' => $price->getPrices($productCalculation->getProduct()),
//            'oldCollection' => $hydrator->getOldCollection(),
//            'markUp' => $markUp,
//            'siteUrl' => ltrim($config->application->baseUri, '/')
//        ]);
//
//        $renderer = $this->getDI()->getPdfRenderer();
//        $renderer->setMargins(0,5,0,5);
//
//        $content = $renderer->render($html, 'portrait');
//        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $productCalculation->getProduct()->getName());
//
//        header('Content-type: application/pdf');
//        header('Content-disposition: attachment; filename=' . $name . '.pdf');
//
//        echo $content;
//        exit;

        $this->view->setVar('productCalculation', $productCalculation);
        $this->view->setVar('productPrices', $price->getPrices($productCalculation->getProduct()));
        $this->view->setVar('oldCollection', $hydrator->getOldCollection());
        $this->view->setVar('markUp', $markUp);
        $this->view->pick('product-calculation-items/print');

    }

    public function finishAction($id)
    {
        /** @var ProductCalculation $productCalculation */
        if(!$productCalculation = $this->entityManager->findOne(ProductCalculation::class, $id)) {
            $this->returnNotFound();
        }


        $productCalculation->setCompleted(true);
        if (!$this->entityManager->save($productCalculation)) {
            $this->addFlashMessagesFromEntity($productCalculation);
        }


        $this->addFlashMessage(
            'Zatvara se! Uskoro će početi sinhronizacija!',
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return ProductCalculationItem::class;
    }
}