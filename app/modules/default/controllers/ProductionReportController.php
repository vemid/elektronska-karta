<?php


class ProductionReportController extends \Vemid\Controller\BaseController
{
    public function reportAction()
    {
        $this->view->setVar('form', new ProductionReportForm());
    }

    public function overviewAction($id)
    {
        /** @var \Product $product */
        $product = $this->entityManager->findOne(Product::class, $id);
        if (!$product) {
            return $this->returnNotFound();
        }

        $builder = $this->modelsManager->createBuilder();
        $query = $builder
            ->columns([
                'oi.entityId',
                'oi.entityTypeId',
                'oi.productSizeId',
                'sum(oi.quantityA+oi.quantityB) orderedQty'
            ])
            ->addFrom(OrderItem::class, 'oi')
            ->leftJoin(ProductSize::class, 'oi.productSizeId = ps.id', 'ps')
            ->leftJoin(Product::class, 'p.id = ps.productId', 'p')
            ->where('p.id = :productId:', [
                'productId' => $product->getId()
            ])
            ->groupBy('entityId,entityTypeId,productSizeId');

        /** @var OrderItem[] $orderItems */
        $orderItems = $query->getQuery()->execute()->toArray();
        $dataOrderedItems = [];
        foreach ($orderItems as $orderItem) {
            $dataOrderedItems[$orderItem['entityTypeId']][$orderItem['entityId']][$orderItem['productSizeId']] = $orderItem['orderedQty'];
        }

        $warehouseOrders = $this->modelsManager->createBuilder()
            ->columns([
                'cse.entityId entityId',
                'cse.entityTypeId entityTypeId',
                'woi.productSizeId',
                'sum(quantity) packedQty'
            ])
            ->addFrom(WarehouseOrderItem::class, "woi")
            ->leftJoin(ProductSize::class,'woi.productSizeId = ps.id','ps')
            ->leftJoin(WarehouseOrder::class,'woi.warehouseOrderId = wo.id','wo')
            ->leftJoin(ChannelSupplyEntity::class,'wo.channelSupplyEntityId = cse.id','cse')
            ->where('ps.productId = :productId:',[
                "productId" => $product->getId()
            ])
            ->groupBy('cse.entityTypeId,cse.entityId,woi.productSizeId')
            ->orderBy('ps.id')
            ->getQuery()->execute()->toArray();

        $data = [];
        foreach ($warehouseOrders as $warehouseOrder) {
            $data[$warehouseOrder['entityTypeId']][$warehouseOrder['entityId']][$warehouseOrder['productSizeId']] = $warehouseOrder['packedQty'];
        }

        $warehouseOrdersCodes = $this->modelsManager->createBuilder()
            ->columns([
                'cse.entityId entityId',
                'cse.entityTypeId entityTypeId',
                'woi.productSizeId',
                'wo.warehouseOrderCode',
            ])
            ->addFrom(WarehouseOrderItem::class, "woi")
            ->leftJoin(ProductSize::class,'woi.productSizeId = ps.id','ps')

            ->leftJoin(WarehouseOrder::class,'woi.warehouseOrderId = wo.id','wo')
            ->leftJoin(ChannelSupplyEntity::class,'wo.channelSupplyEntityId = cse.id','cse')
            ->where('ps.productId = :productId:',[
                "productId" => $product->getId()
            ])
            ->orderBy('cse.id')
            ->getQuery()->execute()->toArray();

        $dataWarehouseCode = [];
        foreach ($warehouseOrdersCodes as $warehouseOrdersCode) {
            $dataWarehouseCode[$warehouseOrdersCode['entityTypeId']][$warehouseOrdersCode['entityId']][$warehouseOrdersCode['productSizeId']] .= $warehouseOrdersCode['warehouseOrderCode'].", ";
        }


        $channelSupplyEntities = $this->entityManager->find(ChannelSupplyEntity::class);

        $dataShops = [];

       /** @var ChannelSupplyEntity $channelSupplyEntity */
        foreach ($channelSupplyEntities as $channelSupplyEntity) {
            /** @var \Code|\Client $entity */
            $entity = $channelSupplyEntity->getEntity();
            $name="";
            if ($channelSupplyEntity->getChannelSupply()->getType() === \ChannelSupply::VP) {
                $name = $entity->getName();
            }
            else {
                $name = $entity->getName();
            }
            $dataShops[$channelSupplyEntity->getChannelSupply()->getType()][]= [
                'entityId' => $channelSupplyEntity->getEntityId(),
                'id' => $channelSupplyEntity->getId(),
                'name' => $name
            ];

        }

        /** @var \Vemid\Entity\Repository\ProductSizeRepository $productSizeRepository */
        $productSizeRepository = $this->entityManager->getRepository(\ProductSize::class);

        $hydrator = new \Vemid\Service\Product\Hydrator($product, $this->entityManager);

        $this->view->setVar('hydrator', $hydrator);
        $this->view->setVar('dataShops', $dataShops);
        $this->view->setVar('dataWarehouseCode', $dataWarehouseCode);
        $this->view->setVar('data', $data);
        $this->view->setVar('dataOrderedItems', $dataOrderedItems);
        $this->view->setVar('product', $product);
        $this->view->setVar('productSizeRepository', $productSizeRepository);

    }

    public function printAction($id)
    {
        $this->view->disable();

        /** @var \Product $product */
        $product = $this->entityManager->findOne(Product::class, $id);
        if (!$product) {
            return $this->returnNotFound();
        }

        $hydrator = new \Vemid\Service\Product\Hydrator($product, $this->entityManager);
        $config = $this->getDI()->getConfig();

        /** @var ChannelSupply $channelSupplyShops */
        /** @var ChannelSupply $channelSupplyWarehouse */
        /** @var ChannelSupply $channelSupplyDomesticClients */
        /** @var ChannelSupply $channelSupplyForeignClients */
        $channelSupplyShops = $this->entityManager->findOne(ChannelSupply::class, 1);
        $channelSupplyWarehouse = $this->entityManager->findOne(ChannelSupply::class, 2);
        $channelSupplyDomesticClients = $this->entityManager->findOne(ChannelSupply::class, 4);
        $channelSupplyForeignClients = $this->entityManager->findOne(ChannelSupply::class, 3);

        $dataWarehouses = $channelSupplyWarehouse->getChannelSupplyEntities([
            ChannelSupplyEntity::PROPERTY_ENTITY_ID . ' IN ({ids:array})',
            'bind' => [
                'ids' => array('127','198')
            ]
        ]);

        $dataShops = [];
        $dataSizes = [];
        /** @var \Vemid\Entity\Repository\ProductSizeRepository $productSizeRepository */
        $productSizeRepository = $this->entityManager->getRepository(\ProductSize::class);

        foreach ($product->getProductSizes() as $productSize) {
            $orderItems = $productSize->getOrderItems([
                'entityTypeId = :entityTypeId:',
                'bind' => [
                    'entityTypeId' => \Vemid\Entity\Type::CODE
                ]
            ]);

            $dataSizes[$productSize->getCode()->getName()] = $productSizeRepository->getTotalMPOrder($productSize)+$productSizeRepository->getTotalVPOrder($productSize);

            foreach ($orderItems as $orderItem) {
                if (!isset($dataShops[$orderItem->getEntityId()][$productSize->getCodeId()])) {
                    $dataShops[$orderItem->getEntityId()][$productSize->getCodeId()] = 0;
                }

                $dataShops[$orderItem->getEntityId()][$productSize->getCodeId()] += $orderItem->getQuantityA();
                if ((int)$orderItem->getEntityId() !== 127) {
                    if ((int)$orderItem->getEntityId() !== 198) {
                        if (!isset($dataShops['total'][$productSize->getCodeId()])) {
                            $dataShops['total'][$productSize->getCodeId()] = 0;
                        }
                        $dataShops['total'][$productSize->getCodeId()] += $orderItem->getQuantityA();
                    }
                }
            }
        }

        $dataClientsA = [];
        $dataClientsB = [];
        foreach ($product->getProductSizes() as $productSize) {
            $orderItems = $productSize->getOrderItems([
                'entityTypeId = :entityTypeId:',
                'bind' => [
                    'entityTypeId' => \Vemid\Entity\Type::CLIENT
                ]
            ]);

            foreach ($orderItems as $orderItem) {

                if (!isset($dataClientsA[$orderItem->getEntityId()][$productSize->getCodeId()])) {
                    $dataClientsA[$orderItem->getEntityId()][$productSize->getCodeId()] = 0;
                }

                if (!isset($dataClientsA['total'][$productSize->getCodeId()])) {
                    $dataClientsA['total'][$productSize->getCodeId()] = 0;
                }

                if (!isset($dataClientsB[$orderItem->getEntityId()][$productSize->getCodeId()])) {
                    $dataClientsB[$orderItem->getEntityId()][$productSize->getCodeId()] = 0;
                }

                if (!isset($dataClientsB['total'][$productSize->getCodeId()])) {
                    $dataClientsB['total'][$productSize->getCodeId()] = 0;
                }

                $dataClientsA[$orderItem->getEntityId()][$productSize->getCodeId()] += $orderItem->getQuantityA();
                $dataClientsB[$orderItem->getEntityId()][$productSize->getCodeId()] += $orderItem->getQuantityB();
                $dataClientsA['total'][$productSize->getCodeId()] += $orderItem->getQuantityA();
                $dataClientsB['total'][$productSize->getCodeId()] += $orderItem->getQuantityB();
            }
        }

        $shopsToExclude = [];
        $vpToExcludeA = [];
        $vpToExcludeB = [];
        foreach ($channelSupplyShops->getChannelSupplyEntities() as $channelSupplyEntity) {
            if (!isset($dataShops[$channelSupplyEntity->getEntityId()]) || !$dataShops[$channelSupplyEntity->getEntityId()] || array_sum($dataShops[$channelSupplyEntity->getEntityId()]) === 0) {
                $shopsToExclude[$channelSupplyEntity->getEntityId()] = $channelSupplyEntity->getEntityId();
            }
        }

        foreach ($channelSupplyDomesticClients->getChannelSupplyEntities() as $channelSupplyEntity) {
            if (!isset($dataClientsA[$channelSupplyEntity->getEntityId()]) || !$dataClientsA[$channelSupplyEntity->getEntityId()] || array_sum($dataClientsA[$channelSupplyEntity->getEntityId()]) === 0) {
                $vpToExcludeA[$channelSupplyEntity->getEntityId()] = $channelSupplyEntity->getEntityId();
            }

            if (!isset($dataClientsB[$channelSupplyEntity->getEntityId()]) || !$dataClientsB[$channelSupplyEntity->getEntityId()] || array_sum($dataClientsB[$channelSupplyEntity->getEntityId()]) === 0) {
                $vpToExcludeB[$channelSupplyEntity->getEntityId()] = $channelSupplyEntity->getEntityId();
            }
        }

        foreach ($channelSupplyForeignClients->getChannelSupplyEntities() as $channelSupplyEntity) {
            if (!isset($dataClientsA[$channelSupplyEntity->getEntityId()]) || !$dataClientsA[$channelSupplyEntity->getEntityId()] || array_sum($dataClientsA[$channelSupplyEntity->getEntityId()]) === 0) {
                $vpToExcludeA[$channelSupplyEntity->getEntityId()] = $channelSupplyEntity->getEntityId();
            }

            if (!isset($dataClientsB[$channelSupplyEntity->getEntityId()]) || !$dataClientsB[$channelSupplyEntity->getEntityId()] || array_sum($dataClientsB[$channelSupplyEntity->getEntityId()]) === 0) {
                $vpToExcludeB[$channelSupplyEntity->getEntityId()] = $channelSupplyEntity->getEntityId();
            }
        }

        $html = $this->getDI()->getSimpleView()->render('production-report/print', [
            'product' => $product,
            'channelSupplyShops' => $channelSupplyShops,
            'channelSupplyWarehouse' => $channelSupplyWarehouse,
            'channelSupplyDomesticClients' => $channelSupplyDomesticClients,
            'channelSupplyForeignClients' => $channelSupplyForeignClients,
            'dataShops' => $dataShops,
            'dataWarehouses' => $dataWarehouses,
            'dataClientA' => $dataClientsA,
            'dataClientB' => $dataClientsB,
            'vpToExcludeA' => $vpToExcludeA,
            'vpToExcludeB' => $vpToExcludeB,
            'shopsToExclude' => $shopsToExclude,
            'hydrator' => $hydrator,
            'dataSizes' =>$dataSizes,
            'siteUrl' => ltrim($config->application->baseUri, '/'),

        ]);

        $builder = $this->getDI()->getPdfRenderer();
        $builder->setPageSize(210, 297);

        $content = $builder->render($html);
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $product);

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;
    }

    public function getListProductsDataAction()
    {
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');

        /** @var \Phalcon\Mvc\Model\Query\Builder $builder */
        $builder = $this->modelsManager->createBuilder();

        $builder->addFrom(Product::class, 'p');
        $builder->innerJoin(ProductClassification::class, 'p.id = pc.productId', 'pc');
        $builder->innerJoin(ProductSize::class, 'p.id = ps.productId', 'ps');
        $builder->leftJoin(ProductionOrder::class, 'ps.id = po.productSizeId', 'po');
        $builder->where('pc.entityTypeId = :entityTypeId:', [
            'entityTypeId' => \Vemid\Entity\Type::CLASSIFICATION
        ]);

        $builder->andWhere('pc.entityId = :entityId:', [
            'entityId' => $this->request->get('season')
        ]);

        if (is_array($search) && $search['value']) {
            $builder->andWhere('po.workOrder LIKE :search: OR p.code LIKE :search: OR p.name LIKE :search:', [
                'search' =>'%'.$search['value'].'%',
            ]);
        }

        if ($this->request->get('priorities')) {
            $builder->andWhere('p.priorityId = :priorityId:', [
                'priorityId' => $this->request->get('priorities')
            ]);
        }
        $builder->groupBy('p.id');
        $builder->orderBy('p.priorityId ASC,p.code ASC');

        $queryForCount = clone $builder;

        /** @var WarehouseOrder[] $warehouseOrders */
        $allProducts = $queryForCount->getQuery()->execute();

        $builder->limit($limit, $offset);

        /** @var Product[] $products */
        $products = $builder->getQuery()->execute();

        $data = [];
        foreach ($products as $product) {
            $builder = $this->modelsManager->createBuilder();
            $builder->columns([
                'SUM(po.planedQty) as plannedQty',
                'SUM(po.realizedAQty+po.realizedBQty) as realizedQty'
            ]);
            $builder->addFrom(Product::class, 'p');
            $builder->innerJoin(ProductSize::class, 'p.id = ps.productId', 'ps');
            $builder->innerJoin(ProductionOrder::class, 'ps.id = po.productSizeId', 'po');
            $builder->where('p.id = :id:', [
                'id' => $product->getId()
            ]);

            $result = $builder->getQuery()->execute();
            $toArray = $result->toArray();
            $plannedQty = 0;
            if (isset($toArray[0]['plannedQty'])) {
                $plannedQty = $toArray[0]['plannedQty'];
                $realizedQty = $toArray[0]['realizedQty'];
            }

            /** @var \Phalcon\Mvc\Model\Query\Builder $warehouseBuilder */
            $warehouseBuilder = $this->modelsManager->createBuilder();
            $warehouseBuilder->columns([
                'SUM(woi.quantity) as itemQty'
            ]);
            $warehouseBuilder->addFrom(ProductSize::class, 'ps');
            $warehouseBuilder->innerJoin(WarehouseOrderItem::class, 'ps.id = woi.productSizeId', 'woi');
            $warehouseBuilder->where('ps.productId = :productId:', [
                'productId' => $product->getId()
            ]);

            $woiResult = $warehouseBuilder->getQuery()->execute();

            $toArray1 = $woiResult->toArray();
            $warehouseQty = 0;
            if (isset($toArray1[0]['itemQty'])) {
                $warehouseQty += $toArray1[0]['itemQty'];
            }

            $itemCaluclator = new \Vemid\Service\OperationList\ItemCalculator($this->entityManager);
            $sasiveno = $itemCaluclator->getFinishedQtyByShewingSector($product);
            $ispeglano = $itemCaluclator->getFinishedQtyByIronSector($product);
            $kontrola = $itemCaluclator->getFinishedQtyByControllingSector($product);
            $dorada = $itemCaluclator->getFinishedQtyByFinishingSector($product);
            $pakovanje = $itemCaluclator->getFinishedQtyByPackingSector($product);

            $data[] = [
                $product->getName(),
                $product->getCode(),
                sprintf('%s / %s', (int)$plannedQty, (int)$realizedQty),
                $sasiveno,
                sprintf('%s / %s', (int)$ispeglano, (int)$dorada),
                sprintf('%s / %s', (int)$kontrola, (int)$pakovanje),
                $warehouseQty,
                '',
                $product->getId(),
                $product->getImagePath(),
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $allProducts->count(),
            'recordsFiltered' => $allProducts->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getPrioritiesByClassificationAction()
    {
        $this->view->disable();

        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var ProductClassification[] $productClassifications */
        $productClassifications = $this->entityManager->find(ProductClassification::class, [
            ProductClassification::PROPERTY_ENTITY_ID . ' = :classificationId: AND '.
            ProductClassification::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:',
            'bind' => [
                'entityTypeId' => \Vemid\Entity\Type::CLASSIFICATION,
                'classificationId' => $this->request->getPost('classification')
            ]
        ]);

        $options = [];
        foreach ($productClassifications as $productClassification) {
            $product = $productClassification->getProduct();
            $options[$product->getPriorityId()] = $product->getPriority()->getPriorityNumber() ."-". $product->getPriority()->getName();
        }

        asort($options);

        $this->response->setContent(json_encode($options));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function validateQuantitiesAction($id) {

        /** @var \Product $product */
        $product = $this->entityManager->findOne(Product::class, $id);
        if (!$product) {
            return $this->returnNotFound();
        }

        /** @var ProductSize[] $productSizes */
        $productSizes = $product->getProductSizes();

        /** @var ProductSize $productSize */
        foreach ($productSizes as $productSize) {
            $check = new Vemid\Service\ProductionOrder\ValidateProductionOrderQty($this->getDI()->getEntityManager(), $productSize);
            $check->validator();
        }

        $this->addFlashMessage(
            $this->translator->t('Uspešno updatovane kolicine!'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );

    }
}
