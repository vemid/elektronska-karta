<?php
use Vemid\Controller\CrudController;
use Picqer\Barcode\BarcodeGeneratorHTML;
use \Vemid\Service\Product\Hydrator;
use \Vemid\Entity\Repository\ProductionOrderRepository;
use \Phalcon\Forms\Element\Text;
use Vemid\Form\Renderer\Json;
use \Picqer\Barcode\BarcodeGeneratorPNG;
use \Vemid\Date\DateTime;
use \Vemid\Messenger\Message;
use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Hidden;

class CuttingJobsController extends CrudController
{

    public function listAction()
    {
        $this->tag::appendTitle($this->translator->t("Tipovi rezijskih poslova"));
        $this->view->setVar('cuttingJobs', $this->entityManager->find(CuttingJob::class));
    }

    public function overviewAction($id)
    {
        $this->tag::appendTitle($this->translator->t('Rezijski Posao'));

        /** @var CuttingJob $cuttingJob */
        if (!$cuttingJob = $this->entityManager->findOne(CuttingJob::class, $id)) {
            return $this->returnNotFound();
        }

        $cuttingWorksheets = $this->entityManager->find(OperatingListItemWorksheet::class, [
            OperatingListItemWorksheet::PROPERTY_OPERATING_LIST_ITEM_ID . ' = :operationListItem:',
            'bind' => [
                'operationListItem' => $cuttingJob->getEntityId()
            ]
        ]);

        $this->view->setVar('cuttingJob', $cuttingJob);
        $this->view->setVar('cuttingWorksheets', $cuttingWorksheets);
        $this->view->setVar('workers', $this->entityManager->find(ProductionWorker::class));
    }

    public function findByBarcodeAction()
    {
        if (!$this->request->isAjax()) {
            return $this->returnNotFound();
        }

        $barcode = new Text('findBarcode');
        $barcode->setAttribute('placeHolder', 'Barkod rezije');

        $form = new Form();
        $form->add($barcode);
        $form->add(new \Phalcon\Forms\Element\Text('testFakeInout'));

        return (new Json())->render($form);
    }

    public function findOperatingItemAction()
    {
        $this->view->disable();

        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var CuttingJob $cuttingJob */
        $cuttingJob = $this->entityManager->findOne(CuttingJob::class, [
            CuttingJob::PROPERTY_BARCODE . ' = :barcode:',
            'bind' => [
                'barcode' => $this->request->getPost('findBarcode')
            ]
        ]);

        if (!$cuttingJob) {
            $this->addFlashMessage(
                'Ne postoji takav barcode rezije!',
                null,
                Message::DANGER
            );

            return;
        }

        $this->dispatcher->setParam('url', '/cutting-jobs/overview/' . $cuttingJob->getEntityId());
    }

    public function addCuttingJobWorksheetAction($id)
    {
        /** @var CuttingJob $cuttingJob */
        if (!$cuttingJob = $this->entityManager->findOne(CuttingJob::class, $id)) {
            return $this->returnNotFound();
        }

        $postData = $this->request->getPost();
        foreach ($postData['create'] as $data) {
            if (empty(array_filter($data))) {
                continue;
            }

            /** @var ProductionWorker $productionWorker */
            if (!$productionWorker = $this->entityManager->findOne(ProductionWorker::class, $data['productionWorkerId'])) {
                continue;
            }

            /** @var ProductionWorker $productionManager */
            if (!$productionManager = $this->entityManager->findOne(ProductionWorker::class, $data['managerId'])) {
                continue;
            }

            if (empty($data['startHour'])) {
                $data['startHour'] = '00';
            }

            if (empty($data['startMin'])) {
                $data['startMin'] = '00';
            }

            if (empty($data['endHour'])) {
                $data['endHour'] = '00';
            }

            if (empty($data['endMin'])) {
                $data['endMin'] = '00';
            }

            $startTime = new \Vemid\Date\Time($data['startHour'] . ':' . $data['startMin']);
            $endTime = new \Vemid\Date\Time($data['endHour'] . ':' . $data['endMin']);

            $cuttingJobWorkSheet = new CuttingJobWorksheet();
            $cuttingJobWorkSheet->setCuttingJob($cuttingJob);
            $cuttingJobWorkSheet->setProductionManager($productionManager);
            $cuttingJobWorkSheet->setProductionWorker($productionWorker);
            $cuttingJobWorkSheet->setDate(new DateTime($data['date']));
            $cuttingJobWorkSheet->setQty($data['qty']);
            $cuttingJobWorkSheet->setStartTime($startTime);
            $cuttingJobWorkSheet->setEndTime($endTime);
            $cuttingJobWorkSheet->setCreated(new DateTime());

            if (!$this->entityManager->save($cuttingJobWorkSheet)) {
                $this->flashSession->error($cuttingJobWorkSheet->getMessages()[0]->getMessage());

                return $this->response->redirect('operating-list-items/overview/' . $id);
            }
        }

        $this->flashSession->success('Uspešno ste dodali reziju!');

        return $this->response->redirect('cutting-jobs/overview/' . $id);
    }

    public function printAction($id)
    {
        $this->view->disable();

        /** @var CuttingJob $cuttingJob */
        if (!$cuttingJob = $this->entityManager->findOne(CuttingJob::class, $id)) {
            return $this->returnNotFound();
        }

        $generatorHtml = new BarcodeGeneratorPNG();

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();

        $html = $this->getDI()->getSimpleView()->render('cutting-jobs/print', [
            'cuttingJob' => $cuttingJob,
            'generatorHtml' => $generatorHtml,
            'barcodeType' => BarcodeGeneratorHTML::TYPE_CODE_128,
            'siteUrl' => ltrim($config->application->baseUri, '/')
        ]);

        $content = $this->getDI()->getPdfRenderer()->render($html);
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $cuttingJob->getName());

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;
    }

    public function reportsAction()
    {
        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        if (!$codeType) {
            return $this->returnNotFound();
        }

        $workers = $this->entityManager->find(ProductionWorker::class);
        $codes = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $this->view->setVar('codes', $codes);
        $this->view->setVar('workers', $workers);
        $this->view->setVar('date', new \Vemid\Date\DateTime());
    }

    public function getReportsDataAction()
    {
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');

        $workerId = $this->request->getQuery('workerId', 'int', 0);
        $codeId = $this->request->getQuery('codeId', 'int', 0);
        $dateTo = new DateTime($this->request->getQuery('dateTo', 'string', ''));
        $dateFrom = new DateTime($this->request->getQuery('dateFrom', 'string', ''));
        $dateTo->setDateType(DateTime::TYPE_DATE);
        $dateFrom->setDateType(DateTime::TYPE_DATE);


        $query = $this->modelsManager->createBuilder()
            ->columns([
                'cjw.id',
                'concat(pw.name," ",pw.lastName) as name',
                'cjw.date',
                'cj.name as cuttingName',
                'cjw.qty as total',
                'cjw.qty*cj.piecePrice as money ',
            ])
            ->addFrom(CuttingJobWorksheet::class, 'cjw')
            ->leftJoin(ProductionWorker::class, 'cjw.productionWorkerId = pw.id', 'pw')
            ->leftJoin(CuttingJob::class, 'cjw.cuttingJobId = cj.id', 'cj')
            ->betweenWhere('cjw.date', $dateFrom->getUnixFormat(), $dateTo->getUnixFormat());

        if ($codeId) {
            $query->andWhere('pw.codeId = :codeId:', [
                'codeId' => $codeId
            ]);
        }

        if ($workerId) {
            $query->andWhere('pw.id = :workerId:', [
                'workerId' => $workerId
            ]);
        }

        $query->orderBy(['cjw.date', 'pw.id']);

        $queryForCount = clone $query;
        $totalResults = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);

        $result = $query->getQuery()->execute();

        $data = [];
        foreach ($result as $row) {

            $data[] = [
                $row['name'],
                $row['date'],
                $row['cuttingName'],
                $row['total'],
                number_format($row['money'], 2),
                $row['id'],
            ];
        }
        $responseData = [
            'draw' => $page,
            'recordsTotal' => $totalResults->count(),
            'recordsFiltered' => $totalResults->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function reportBySectorWorkerAction()
    {
        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $this->view->pick('cutting-jobs/report-by-sector-worker');
        $this->view->setVar('date', new \Vemid\Date\DateTime());
        $this->view->setVar('sectors', $sectors);
    }

    public function getReportBySectorWorkerDataAction()
    {
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');

        $codeId = $this->request->getQuery('codeId', 'int', 0);
        $dateTo = new DateTime($this->request->getQuery('dateTo', 'string', ''));
        $dateFrom = new DateTime($this->request->getQuery('dateFrom', 'string', ''));
        $dateTo->setDateType(DateTime::TYPE_DATE);
        $dateFrom->setDateType(DateTime::TYPE_DATE);
        $search = $this->request->getQuery('search');


        $query = $this->modelsManager->createBuilder()
            ->columns([
                'concat(pw.name," ",pw.lastName) as fullName',
                'cjw.date',
                'sum(cjw.qty) as total',
                'sum(cjw.qty*cj.piecePrice) as money ',
                'pw.id'
            ])
            ->addFrom(CuttingJobWorksheet::class, 'cjw')
            ->leftJoin(ProductionWorker::class, 'cjw.productionWorkerId = pw.id', 'pw')
            ->leftJoin(CuttingJob::class, 'cjw.cuttingJobId = cj.id', 'cj')
            ->betweenWhere('cjw.date', $dateFrom->getUnixFormat(), $dateTo->getUnixFormat());

        $query->andWhere('pw.codeId = :codeId:', [
            'codeId' => $codeId
        ]);

        if (is_array($search) && $search['value']) {
            $query->andWhere('(pw.name LIKE :search: OR pw.lastName LIKE :search:)', [
                'search' => '%'. $search['value'] .'%'
            ]);
        }

        $query->groupBy(['pw.id','cjw.date']);
        $query->orderBy(['sum(cjw.qty) DESC', 'pw.id']);

        $queryForCount = clone $query;
        $totalResults = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);

        $result = $query->getQuery()->execute();

        $data = [];
        foreach ($result as $row) {

            $data[] = [
                $row['fullName'],
                $row['date'],
                $row['total'],
                number_format($row['money'], 2),
                $row['id']
            ];
        }
        $responseData = [
            'draw' => $page,
            'recordsTotal' => $totalResults->count(),
            'recordsFiltered' => $totalResults->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function reportBySectorAction()
    {

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $this->view->pick('cutting-jobs/report-by-sector');
        $this->view->setVar('sectors', $sectors);

    }

    public function reportBySectorDataAction()
    {
        $paramDateFrom = $this->request->get('dateFrom');
        $paramDateTo = $this->request->get('dateTo');
        $paramSector = $this->request->get('sectorId');

        /** @var Code $code */
        $code = $this->entityManager->findOne(Code::class, $paramSector);
        $dateFrom = new DateTime($paramDateFrom);
        $dateTo = new DateTime($paramDateTo);

        if (!$code) {
            return $this->returnNotFound();
        }

        $query = $this->modelsManager->createBuilder()
            ->columns([
                'cj.name',
                'sum(cjw.qty) as total',
                'sum(cjw.qty*cj.piecePrice) as money ',
                'cj.id'
            ])
            ->addFrom(CuttingJobWorksheet::class, 'cjw')
            ->leftJoin(ProductionWorker::class, 'cjw.productionWorkerId = pw.id', 'pw')
            ->leftJoin(CuttingJob::class, 'cjw.cuttingJobId = cj.id', 'cj')
            ->betweenWhere('cjw.date', $dateFrom->getUnixFormat(), $dateTo->getUnixFormat());

        $query->andWhere('pw.codeId = :codeId:', [
            'codeId' => $code->getId()
        ]);

//        if (is_array($search) && $search['value']) {
//            $query->andWhere('(pw.name LIKE :search: OR pw.lastName LIKE :search:)', [
//                'search' => '%'. $search['value'] .'%'
//            ]);
//        }

        $query->groupBy(['cj.id']);
        $query->orderBy(['sum(cjw.qty) DESC']);

        $result = $query->getQuery()->execute();

        $this->view->pick('cutting-jobs/data/report-by-sector-data');
        $this->view->setVar('datas', $result);
    }

    public function pivotCuttingReportAction()
    {
        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $this->view->pick('cutting-jobs/pivot-cutting-report');
        $this->view->setVar('sectors', $sectors);
    }

    public function pivotCuttingReportDataAction()
    {
        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_HEAD_JS)
            ->addJs(APP_PATH . 'assets/js/react-pivot-standalone-3.0.0.min.js');

        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_HEAD_CSS)
            ->addCss(APP_PATH . 'assets/css/react-pivot.css');

        /** @var \Vemid\Entity\Repository\CuttingJobWorksheetRepository $cuttingJobData */
        $cuttingJobData = $this->entityManager->getRepository(\CuttingJobWorksheet::class);
        $results = $cuttingJobData->getCuttingJobsData();

        $cuttingData = [];
        foreach ($results as $result) {
            $cuttingData[] =[
                'year' => $result['year'],
                'month' => $result['month'],
                'sector' => $result['sector'],
                'worker' => $result['worker'],
                'manager' => $result['manager'],
                'cuttingJob' => $result['cutting_job'],
                'transaction' => [
                    'quantity' => $result['quantity'],
                    'totalValue' => $result['value']
                ]
            ];

        }

        $this->view->setVar('rows', json_encode($cuttingData));
        $this->view->pick('cutting-jobs/data/pivot-cutting-report-data');
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return CuttingJob::class;
    }
}
