<?php

use Vemid\Controller\CrudController;
use \Vemid\Service\Warrant\Statement\Manager;
use \Vemid\Entity\Repository\DealershipWarrantRepository;
use \Vemid\Form\Renderer\Json as Renderer;
use \Vemid\Service\MisWsdl\Query;

/**
 * Class DealershipsController
 *
 * @package Frontemd\Controllers
 */
class DealershipsController extends CrudController
{
    public function listByUserAction()
    {
        /** @var UserSector $sector */
        $sector = $this->currentUser->getUserSectors()->getFirst();
        if (!$sector) {
            return $this->returnNotFound();
        }

        if (!$shop = $sector->getCode()) {
            return $this->returnNotFound();
        }

        $query = $this->modelsManager->createBuilder()
            ->columns([
                'dw.date',
                'ROUND(SUM(dw.amount),0) as totalAmount',
                'd.codeId'
            ])
            ->addFrom(\DealershipWarrant::class, 'dw')
            ->leftJoin(\Dealership::class, 'dw.dealershipId = d.id', 'd')
            ->andWhere('d.codeId = :codeId:', [
                'codeId' => $shop->getId()
            ])
            ->orderBy('dw.date DESC')
            ->groupBy('dw.date')
            ->limit(30);

        $dealershipWarrants = $query->getQuery()->execute();

        /** @var DealershipWarrantRepository $dealershipWarrantRepository */
        $dealershipWarrantRepository = $this->entityManager->getRepository(DealershipWarrant::class);

        $this->view->setVar('dealershipWarrants', $dealershipWarrants);
        $this->view->setVar('dealershipWarrantRepository', $dealershipWarrantRepository);
        $this->view->pick('dealerships/list-by-user');
    }

    public function printAction()
    {
        $params = $this->dispatcher->getParams();

        $query = $this->modelsManager->createBuilder()
            ->addFrom(\DealershipWarrant::class, 'dw')
            ->leftJoin(\Dealership::class, 'dw.dealershipId = d.id', 'd');

        if (!empty($params[2])) {
            $query->andWhere('dw.id = :id:', [
                'id' => $params[2]
            ]);
        } else {
            $query->andWhere('d.codeId = :codeId:', [
                'codeId' => $params[1]
            ])
            ->andWhere('dw.date = :date:', [
                'date' => $params[0]
            ])
            ->orderBy('dw.date DESC');
        }

        $dealershipWarrants = $query->getQuery()->execute();

        /** @var \Dealership $dealership */
        $dealership = $this->entityManager->findOne(Dealership::class, [
            Dealership::PROPERTY_CODE_ID . ' = :codeId:',
            'bind' => [
                'codeId' => $params[1]
            ]
        ]);

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();

        $html = $this->getDI()->getSimpleView()->render('dealerships/print', [
            'dealershipWarrants' => $dealershipWarrants,
            'dealership' => $dealership,
            'siteUrl' => ltrim($config->application->baseUri, '/')
        ]);

        $content = $this->getDI()->getPdfRenderer()->render($html);
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ']', ','], '-', $params[0]);

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;
    }


    public function listAllAction()
    {
        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_FOOT_JS)
            ->addJs(APP_PATH . 'assets/js/drop-zone.js');

        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_HEAD_CSS)
            ->addCss(APP_PATH . 'assets/css/drop-zone.css');

        $query = $this->modelsManager->createBuilder()
            ->from(\DealershipWarrant::class)
            ->orderBy('date DESC')
            ->limit(600);

        $warrantData = $query->getQuery()->execute();

        $this->view->setVar('warrantData', $warrantData);
        $this->view->pick('dealerships/list-all');
    }

    public function bookAction($id)
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var DealershipWarrant $warrant */
        if (!$warrant = $this->entityManager->findOne(DealershipWarrant::class, $id)) {
            return $this->returnNotFound();
        }

        $warrant->setStatus(true);

        if (!$this->entityManager->save($warrant)) {
            $this->addFlashMessagesFromEntity($warrant);

            return;
        }

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t('Uspešno je proknjizen nalog!'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function uploadFileAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $warrantStatement = new WarrantStatement();

        /** @var Phalcon\Http\Request\File $file */
        $file = array_pop($this->request->getUploadedFiles());

        if ($file !== null) {
            $uploadPath = $warrantStatement->getUploadPath();
            $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();

            if (!$file->moveTo($filePath)) {
                throw new \RuntimeException('File did not uploaded to server!');
            }

            $manager = new Manager($this->entityManager, $filePath);
            $manager->import();

            $this->addFlashMessage(
                'Uspešno importovani nalozi!',
                null,
                \Vemid\Messenger\Message::SUCCESS
            );

            $this->dispatcher->setParam('url', '/dealerships/list-all');

            return;
        }


    }

    public function listAction()
    {
        $this->view->setVar('dealerships', $this->entityManager->find(Dealership::class));
    }

    public function overviewAction($id)
    {
        return parent::overviewAction($id);
    }


    public function updateMisStatementsFormAction()
    {

        $form = new \Vemid\Form\Form();
        $date = new \Phalcon\Forms\Element\Date('date');
        $date->setLabel('Datum');
        $date->setAttribute('class','datepicker');
        $form->add($date);

        return (new Renderer())->render($form);
    }

    public function updateMisStatementsAction()
    {
        if ($this->request->isPost()) {
            $form = $this->getForm();
            if ($form->isValid($this->request->getPost())) {
                $form->synchronizeEntityData();
            }
        }

        if ($form->getValue('date')) {
            $datum = (new \Vemid\Date\DateTime($form->getValue('date')))->getShortSerbianFormat();
            $sql = <<<SQL
select trim(ozn_izv) ozn_izv from izvod where status = 0 and storno = 'N' and dat_izv = '$datum'
SQL;

            $query = new Query($sql);
            $timeStarted = microtime(true);

            while (empty($data)) {
                try {
                    $data = $query->run();
                } catch (Exception $e) {
                }

                $timeElapsed = microtime(true) - $timeStarted;

                if ($timeElapsed > 10) {
                    $this->flashSession->error('Failed to connect to MIS web service');
                    $data = -1;
                }
            }
        }

        $statements = [];
        if($data) {
            foreach ($data as $item) {
                $statements[] = [
                    'izvod' => $item['ozn_izv']
                ];
            }
        }

        if($statements) {

            foreach ($statements as $statement) {
                $data2 = null;

                $item = $statement['izvod'];
                $sql2 = <<<SQL
execute procedure test_update_izvod('$item')
SQL;

                $query2 = new Query($sql2);
                $timeStarted2 = microtime(true);

                while (empty($data2)) {
                    try {
                        $data2 = $query2->run();
                    } catch (Exception $e) {
                    }

                    $timeElapsed2 = microtime(true) - $timeStarted2;

                    if ($timeElapsed2 > 10) {
                        $this->flashSession->error('Failed to connect to MIS web service');
                        $data2 = -1;
                    }
                }
            }
        }

        $this->addFlashMessage(
            $this->translator->t('Uspešno updatovani izvodi!'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function pivotReportDataAction()
    {
        $this->tag::appendTitle($this->translator->t("Pregled izvestaja buduce naplate"));

        $warrantReport = new Vemid\Service\MisWsdl\Reports\Warrants($this->entityManager);
        $warrantReportData =  $warrantReport->run();

        /** @var DealershipWarrant $dealershipWarrants */
        $dealershipWarrants  = $this->entityManager->find(DealershipWarrant::class,[
            DealershipWarrant::PROPERTY_DATE . ' >= :date:',
            'bind' => [
                'date' => '2021-01-01'
            ]
        ]);

        $testArray = $dealershipWarrants->toArray();



        $data = [];
        foreach ($warrantReportData as $row) {
            $status = "0";
            if(array_search(trim($row['NALOG']),array_column($testArray, 'warrantNumber'))) {
                $status = $testArray[array_search(trim($row['NALOG']),array_column($testArray, 'warrantNumber'))]['status'];
            }
            $data[] = [
                'nalog' =>$row['NALOG'],
                'tip' =>$row['TIP'] ? $row['TIP'] : "",
                'dokument' =>trim($row['DOKUMENT']),
                'sifraObjekta' => $row['SIFRA_OBJEKTA'],
                'objekat' =>iconv("CP1250","UTF-8",$row['OBJEKAT']),
                'datumKreiranja' =>$row['DATUM_KREIRANJA'],
                'datumValute' =>$row['DATUM_VALUTE'],
                'datumValute2' => str_replace("-",".",$row['DATUM_VALUTE']),
                'iznos' =>$row['IZNOS'],
                'status' => $status
            ];
        }


        $out = array_values($data);
        $this->view->setVar('data', json_encode($out));
        $this->view->pick('dealerships/pivot-report-data');
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return Dealership::class;
    }
}