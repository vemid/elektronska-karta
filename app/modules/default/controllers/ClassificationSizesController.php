<?php

use Vemid\Form\Renderer\Json as Renderer;
use \Vemid\Controller\CrudController;
use Vemid\Messenger\Message;

/**
 * Class ClassificationSizesController
 *
 * @package Backend\Controllers
 */
class ClassificationSizesController extends CrudController
{

    /**
     * Displays all Classification Sizes
     */
    public function listAction()
    {
        /** @var ClassificationSize[] $classificationSizes */
        $classificationSizes = $this->entityManager->find(ClassificationSize::class);

        $classificationSizesGrouped = [];
        foreach ($classificationSizes as $classificationSize) {
            $classificationSizesGrouped[$classificationSize->getClassification()->getId()]['classification'] = $classificationSize->getClassification()->getName();
            $classificationSizesGrouped[$classificationSize->getClassification()->getId()]['codes'][] = $classificationSize->getCode()->getCode();
        }

        $this->_setTitle($this->_translate('Classification Sizes'));
        $this->view->setVar('classificationSizes', $classificationSizesGrouped);
    }

    /**
     * Creates new Classification Size
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var Classification $classification */
        $classification = $this->entityManager->findOne(\Classification::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost(ClassificationSize::PROPERTY_CLASSIFICATION_ID)
            ]
        ]);

        if (!$classification || $classification->getClassificationType()->getCodeType()->getCode() !== CodeType::CLASSIFICATION_TYPE) {
            return $this->returnNotFound();
        }

        /** @var array $codes */
        $codes = $this->request->getPost(ClassificationSize::PROPERTY_CODE_ID);

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SIZES
            ]
        ]);

        foreach ($codes as $codeSize) {
            /** @var Code $code */
            $code = $this->entityManager->findOne(Code::class, [
                Code::PROPERTY_CODE . ' = :code: AND ' .
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                'bind' => [
                    'code' => $codeSize,
                    'codeTypeId' => $codeType->getId()
                ]
            ]);

            if (!$code) {
                continue;
            }

            /** @var ClassificationSize $classificationSize */
            $classificationSize = $this->entityManager->findOne(ClassificationSize::class, [
                ClassificationSize::PROPERTY_CLASSIFICATION_ID . ' = :classificationId: AND ' .
                ClassificationSize::PROPERTY_CODE_ID . ' = :code:',
                'bind' => [
                    'classificationId' => $classification->getId(),
                    'code' => $code->getId()
                ]
            ]);

            if ($classificationSize) {
                continue;
            }

            $classificationSize = new ClassificationSize();
            $classificationSize->setClassification($classification);
            $classificationSize->setCode($code);
            if (!$this->entityManager->save($classificationSize)) {
                $this->addFlashMessagesFromEntity($classificationSize);

                return;
            }
        }

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t('Data was saved successfully.'),
            null,
            Message::SUCCESS
        );
    }

    /**
     * Updates existing Classification Size
     *
     * @param int $classificationId
     * @return bool
     */
    public function updateAction($classificationId)
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var Classification $classification */
        $classification = $this->entityManager->findOne(\Classification::class, $classificationId);

        if (!$classification || $classification->getClassificationType()->getCodeType()->getCode() !== CodeType::CLASSIFICATION_TYPE) {
            return $this->returnNotFound();
        }

        /** @var ClassificationSize[] $classificationSizes */
        $classificationSizes = $this->entityManager->find(ClassificationSize::class, [
            ClassificationSize::PROPERTY_CLASSIFICATION_ID . ' = :classificationId:',
            'bind' => [
                'classificationId' => $classification->getId(),
            ]
        ]);

        /** @var array $codes */
        $codes = $this->request->getPost(ClassificationSize::PROPERTY_CODE_ID);

        foreach ($classificationSizes as $classificationSize) {
            if (!$code = $classificationSize->getCode()) {
                continue;
            }

            if (!in_array($code->getCode(), $codes, true)) {
                if (!$this->entityManager->delete($classificationSize)) {
                    $this->addFlashMessagesFromEntity($classificationSize);
                }
            }
        }

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SIZES
            ]
        ]);

        foreach ($codes as $codeSize) {
            /** @var Code $code */
            $code = $this->entityManager->findOne(Code::class, [
                Code::PROPERTY_CODE . ' = :code: AND ' .
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                'bind' => [
                    'code' => $codeSize,
                    'codeTypeId' => $codeType->getId()
                ]
            ]);

            if (!$code) {
                continue;
            }

            /** @var ClassificationSize $classificationSize */
            $classificationSize = $this->entityManager->findOne(ClassificationSize::class, [
                ClassificationSize::PROPERTY_CLASSIFICATION_ID . ' = :classificationId: AND ' .
                ClassificationSize::PROPERTY_CODE_ID . ' = :code:',
                'bind' => [
                    'classificationId' => $classification->getId(),
                    'code' => $code->getId()
                ]
            ]);

            if ($classificationSize) {
                continue;
            }

            $classificationSize = new ClassificationSize();
            $classificationSize->setClassification($classification);
            $classificationSize->setCode($code);
            if (!$this->entityManager->save($classificationSize)) {
                $this->addFlashMessagesFromEntity($classificationSize);

                return;
            }
        }

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t('Data was saved successfully.'),
            null,
            Message::SUCCESS
        );
    }


    /**
     * Generates JSON create form
     *
     * @return string
     */
    public function getCreateFormAction()
    {
        $form = $this->getForm();

        if ($form->has(ClassificationSize::PROPERTY_CLASSIFICATION_ID)) {
            /** @var CodeType $codeType */
            $codeType = $this->entityManager->findOne(CodeType::class, [
                CodeType::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'code' => CodeType::CLASSIFICATION_TYPE
                ]
            ]);

            $options = [];
            if ($codeType) {
                /** @var Code $code */
                $code = $this->entityManager->findOne(Code::class, [
                    Code::PROPERTY_CODE . ' = :code: AND ' .
                    Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                    'bind' => [
                        'code' => '02',
                        'codeTypeId' => $codeType->getId()
                    ]
                ]);

                foreach ($code->getClassifications(['OrderItem' => 'name']) as $classification) {
                    $classificationSize = $this->entityManager->find(ClassificationSize::class, [
                        ClassificationSize::PROPERTY_CLASSIFICATION_ID . ' = :classificationId:',
                        'bind' => [
                            'classificationId' => $classification->getId()
                        ]
                    ]);

                    if ($classificationSize->count()) {
                        continue;
                    }

                    $options[(string)$classification->getCode()] = $classification->getName();
                }
            }

            $form->get(ClassificationSize::PROPERTY_CLASSIFICATION_ID)->setOptions($options);
        }

        if ($form->has(ClassificationSize::PROPERTY_CODE_ID)) {
            /** @var CodeType $codeType */
            $codeType = $this->entityManager->findOne(CodeType::class, [
                CodeType::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'code' => CodeType::SIZES
                ]
            ]);

            $options = [];
            if ($codeType) {
                /** @var Code[] $code */
                $codes = $this->entityManager->find(Code::class, [
                    Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                    'bind' => [
                        'codeTypeId' => $codeType->getId()
                    ],
                    'OrderItem' => 'name ASC'
                ]);

                foreach ($codes as $code) {
                    $options[$code->getCode()] = $code->getName();
                }
            }

            $element = $form->get(ClassificationSize::PROPERTY_CODE_ID);
            $element->setOptions($options);
            $element->setDefault([]);
            $element->setAttribute('multiple', true);
            $element->setName($element->getName() . '[]');
        }

        $renderer = new Renderer();

        return $renderer->render($form);
    }

    public function deleteAction($classificationId)
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var ClassificationSize[] $classificationSizes */
        $classificationSizes = $this->entityManager->find(ClassificationSize::class, [
            ClassificationSize::PROPERTY_CLASSIFICATION_ID . ' = :classificationId:',
            'bind' => [
                'classificationId' => $classificationId
            ]
        ]);

        if (!$classificationSizes->count()) {
            return $this->returnNotFound();
        }

        foreach ($classificationSizes as $classificationSize) {
            if (!$this->entityManager->delete($classificationSize)) {
                $this->addFlashMessagesFromEntity($classificationSize);

                return;
            }
        }

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t('Data was saved successfully.'),
            null,
            Message::SUCCESS
        );
    }

    /**
     * Generates JSON update form
     *
     * @param int $id
     * @return bool|string
     */
    public function getUpdateFormAction($id)
    {
        /** @var Classification $classification */
        $defaultClassification = $this->entityManager->findOne(\Classification::class, $id);

        if (!$defaultClassification) {
            return $this->returnNotFound();
        }

        $form = $this->getForm();

        if ($form->has(ClassificationSize::PROPERTY_CLASSIFICATION_ID)) {
            /** @var CodeType $codeType */
            $codeType = $this->entityManager->findOne(CodeType::class, [
                CodeType::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'code' => CodeType::CLASSIFICATION_TYPE
                ]
            ]);

            $options = [];
            if ($codeType) {
                /** @var Code $code */
                $code = $this->entityManager->findOne(Code::class, [
                    Code::PROPERTY_CODE . ' = :code: AND ' .
                    Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                    'bind' => [
                        'code' => '02',
                        'codeTypeId' => $codeType->getId()
                    ]
                ]);

                foreach ($code->getClassifications(['OrderItem' => 'name']) as $classification) {
                    $classificationSize = $this->entityManager->find(ClassificationSize::class, [
                        ClassificationSize::PROPERTY_CLASSIFICATION_ID . ' = :classificationId:',
                        'bind' => [
                            'classificationId' => $classification->getId()
                        ]
                    ]);

                    if ($classificationSize->count()) {
                        continue;
                    }

                    $options[(string)$classification->getCode()] = $classification->getName();
                }
            }

            $form->get(ClassificationSize::PROPERTY_CLASSIFICATION_ID)->setOptions($options);
            $form->get(ClassificationSize::PROPERTY_CLASSIFICATION_ID)->setDefault($classification->getCode());
            $form->get(ClassificationSize::PROPERTY_CLASSIFICATION_ID)->setAttribute('disabled', true);
        }

        if ($form->has(ClassificationSize::PROPERTY_CODE_ID)) {

            $defaultOptions = [];
            foreach ($defaultClassification->getClassificationSizes() as $classificationSize) {
                if (!$code = $classificationSize->getCode()) {
                    continue;
                }

                $defaultOptions[] = $code->getCode();
            }

            /** @var CodeType $codeType */
            $codeType = $this->entityManager->findOne(CodeType::class, [
                CodeType::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'code' => CodeType::SIZES
                ]
            ]);

            $options = [];
            if ($codeType) {
                /** @var Code[] $code */
                $codes = $this->entityManager->find(Code::class, [
                    Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                    'bind' => [
                        'codeTypeId' => $codeType->getId()
                    ],
                    'OrderItem' => 'name ASC'
                ]);

                foreach ($codes as $code) {
                    $options[$code->getCode()] = $code->getName();
                }
            }

            $element = $form->get(ClassificationSize::PROPERTY_CODE_ID);
            $element->setOptions($options);
            $element->setDefault($defaultOptions);
            $element->setAttribute('multiple', true);
            $element->setName($element->getName() . '[]');
        }

        $renderer = new Renderer();

        return $renderer->render($form);
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return \ClassificationSize::class;
    }
}
