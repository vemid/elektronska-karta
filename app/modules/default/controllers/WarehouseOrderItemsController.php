<?php

use \Vemid\Controller\CrudController;

class WarehouseOrderItemsController extends CrudController
{

    public function efectusItemAction()
    {
        $this->view->disable();

        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var WarehouseOrderItem $warehouseOrderItem */
        $warehouseOrderItem = $this->entityManager->findOne(WarehouseOrderItem::class, $this->request->getPost('id'));

        if (!$warehouseOrderItem) {
            return $this->returnNotFound();
        }

        $warehouseOrderItem->setQuantity($this->request->getPost('quantity'));
        if (!$this->entityManager->save($warehouseOrderItem)) {
            $this->addFlashMessagesFromEntity($warehouseOrderItem);
            return;
        }

        $this->response->setContent(json_encode(['error' => false]));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function efectusPushToMisAction()
    {
        $this->view->disable();

        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $warehouseOrderItems = $this->request->getPost('woi');

        /** @var WarehouseOrderItem[] $warehouseOrderItems */
        $warehouseOrderItems = $this->entityManager->find(WarehouseOrderItem::class, [
            WarehouseOrderItem::PROPERTY_ID . ' IN ({ids:array})',
            'bind' => [
                'ids' => $warehouseOrderItems
            ]
        ]);

        $warehouseOrders = [];
        foreach ($warehouseOrderItems as $warehouseOrderItem) {
            $warehouseOrders[$warehouseOrderItem->getWarehouseOrderId()] = $warehouseOrderItem->getWarehouseOrder();
        }

        foreach ($warehouseOrders as $warehouseOrder) {
            $warehouseOrder->setStatus(WarehouseOrder::LOCKED);
            if (!$this->entityManager->save($warehouseOrder)) {
                $this->addFlashMessagesFromEntity($warehouseOrder);
                return ;
            }
        }

        $this->response->setContent(json_encode(['url' => $this->request->getHTTPReferer()]));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getEntityName()
    {
        return WarehouseOrderItem::class;
    }
}