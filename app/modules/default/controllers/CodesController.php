<?php

use Vemid\Controller\CrudController;
use Vemid\Form\Renderer\Json as Renderer;
use \Phalcon\Forms\Element\Hidden;

/**
 * Class CodesController
 *
 * @package Default\Controllers
 */
class CodesController extends CrudController
{

    public function getCreateFormAction()
    {
        $params = $this->dispatcher->getParams();
        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, $params[1]);

        if (!$codeType) {
            $this->returnNotFound();
        }

        $form = $this->getForm(null, [Code::PROPERTY_CODE_TYPE_ID]);
        $form->add((new Hidden('codeTypeId'))->setDefault($codeType->getId()));

        $renderer = new Renderer();

        return $renderer->render($form);
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return \Code::class;
    }
}
