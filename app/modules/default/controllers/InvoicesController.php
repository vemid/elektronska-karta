<?php

use InlineStyle\InlineStyle;
use Vemid\Controller\CrudController;

/**
 * Class InvoicesController
 *
 * @package Default\Controllers
 */
class InvoicesController extends CrudController
{
    public function listAction()
    {
        $this->tag::appendTitle($this->translator->t("Pregled Faktura"));

        $user = $this->currentUser->getId();

        if($this->currentUser->isSuperAdmin()){
            $this->view->setVar('clients', "test2");
            $this->view->setVar('invoices', $this->entityManager->find(Invoice::class,[
            'order' => 'date DESC'
        ]));
        }
        elseif ($this->currentUser->isClient())
        {
            /** @var Client $client */
            $client = $this->entityManager->findOne(Client::class, [
                Client::PROPERTY_USER_ID . ' = :userId:',
                'bind' => [
                    'userId' => $user
                ]
            ]);
            /** @var Invoice $invoices */
            $invoices = $this->entityManager->find(Invoice::class, [
                Invoice::PROPERTY_CLIENT_ID . ' = :clientId: AND '.
                Invoice::PROPERTY_STATUS .' = :status:',
                'bind' => [
                    'clientId' => $client->getId(),
                    'status' => true
                ],
                'order' => 'date DESC'
            ]);

            $this->view->setVar('invoices',$invoices);
        }
    }

    public function downloadAction($id)
    {
        /** @var Invoice $invoice */
        if (!$invoice = $this->entityManager->findOne(Invoice::class, $id)) {
            return $this->returnNotFound();
        }

        $user = $this->currentUser;

        $tables = [];

//        $googleTranslate = new Vemid\Service\GoogleTranslator\GoogleTranslateClient();
//        $translated = $googleTranslate->getTranslated('Export podataka u toku. Uskoro će fajl biti poslat mailom!',$this->currentUser->getLanguage());
$translated = "Export u toku";
        $this->addFlashMessage(
            $translated,
            null,
            \Vemid\Messenger\Message::SUCCESS
        );

            $exporter = new \Vemid\Exporter\Invoice\Exporter($this->modelsManager->createBuilder(), $invoice,$user,  $this->entityManager,$this->translator);

            $tables[] = $exporter->export();


        $renderer = new \Vemid\Table\Renderer\ExcelRenderer($tables, 'Invoice data');
        $renderer->render();


    }

    public function downloadImagesAction($id)
    {
        $this->view->disable();
        ob_clean();

        /** @var Invoice $invoice */
        if (!$invoice = $this->entityManager->findOne(Invoice::class, $id)) {
            return $this->returnNotFound();
        }

        $this->flashSession->success('Uspešno ste poslali na stampu etikete!');


        /** @var \InvoiceItem $productCodes */
        $productCodes = $this->modelsManager->createBuilder()
            ->columns([
                'p.code',
            ])
            ->from(\InvoiceItem::class)
            ->leftJoin(\ProductSize::class,"ps.id = InvoiceItem.productSizeId",'ps')
            ->leftJoin(\Product::class,"p.id = ps.productId",'p')
            ->where('InvoiceItem.invoiceId = :id:',
                [
                    'id' => $invoice->getId()
                ])
            ->groupBy('p.code');

        $products = $productCodes->getQuery()->execute();

        $path = APP_PATH . 'var/uploads/files/product_images';

        $zip = new ZipArchive();
        $zipFile = tempnam(sys_get_temp_dir(), 'zip');

        $tempFile = tempnam(sys_get_temp_dir(), 'txt');


        if ($zip->open($zipFile, ZIPARCHIVE::CREATE) != TRUE) {
            die ("Could not open archive");
        }

        $string="";

        $data=[];
        foreach ($products as $product) {
            if(file_exists($path."/".$product['code'].".jpg"))
            {
                $images = glob($path.'/'.$product['code'].'*');
                foreach ($images as $image) {
                    try{
                        $relativePath = str_replace($path.'/',"",$image);
                        $zip->addFromString($relativePath,file_get_contents($image));
                    }
                    catch (\Exception $ex) {
                        $string .= "<alert>$ex</alert>";
                    }
                    //$data[] =$image;

                }
            }
            else {
                $string .= $product['code'] . "\n";
            }

        }
//        var_dump($data);
//        die();

        file_put_contents($tempFile, $string);
        $zip->addFromString("list_of_no_image_products.txt",file_get_contents($tempFile));



        $downloadFile = str_replace("/","_",$invoice->getInvoice()).".zip";
        $zip->close();
ob_clean();
        ob_end_flush();
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"".$downloadFile."\"");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: ".filesize($zipFile));

        @readfile($zipFile);


        unlink($zipFile);

    }

    public function printAction($id)
    {
        //$this->view->disable();

        /** @var Invoice $invoice */
        if (!$invoice = $this->entityManager->findOne(Invoice::class, $id)) {
            return $this->returnNotFound();
        }

        /** @var \InvoiceItem $productCodes */
        $productCodes = $this->modelsManager->createBuilder()
            ->columns([
                'InvoiceItem.id',
                'p.code',
                'c.code size',
                'p.name',
                'pt.tag',
                'InvoiceItem.qty',
                'InvoiceItem.price'
            ])
            ->from(\InvoiceItem::class)
            ->leftJoin(\ProductSize::class,"ps.id = InvoiceItem.productSizeId",'ps')
            ->leftJoin(\Product::class,"p.id = ps.productId",'p')
            ->leftJoin(\Code::class,"c.id = ps.codeId",'c')
            ->leftJoin(\ProductTag::class,"pt.productId = p.id and pt.category = 'MODEL'",'pt')
            ->where('InvoiceItem.invoiceId = :id:',
                [
                    'id' => $invoice->getId()
                ])
            ->orderBy('p.code,c.code');

        $invoiceItems = $productCodes->getQuery()->execute();

        $data = [];

//        $googleTranslate = new Vemid\Service\GoogleTranslator\GoogleTranslateClient();
        $translator = $this->getDI()->getTranslator();
        /** @var InvoiceItem $item */
        foreach ($invoiceItems as $item) {
            /** @var Product $invoiceItem */
            $invoiceItem = $this->entityManager->findOne(Product::class, [
                Product::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'code' => $item['code']
                ]
            ]);
            /** @var \Vemid\Entity\Repository\ProductTagRepository $productTagRepository */
            $productTagRepository = $this->entityManager->getRepository(ProductTag::class);
            $hydrator = new \Vemid\Service\Product\Hydrator($invoiceItem,$this->entityManager);
//            print_r($productTagRepository->getProductTagModel($invoiceItem));
//            die();
            $data[]=[
                'code' => $item['code'],
                'name' => $item['name'],
                'size' => $item['size'],
//                'model' => $googleTranslate->getTranslated(array_pop(explode(' ', $hydrator->getProductGroup()->getName())),$this->currentUser->getLanguage()),
//                'model' => $hydrator->getProductGroup()->getName(),
                'model' => $translator->t($item['tag']),
                'qty' => $item['qty'],
                'price' => $item['price']
            ];
        }

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();
        $css1 = APP_PATH ."public/assets/custom.css";
        $css2 = APP_PATH ."public/assets/head.css";
        $html = $this->getDI()->getSimpleView()->render('invoices/print', [
            'data' => $data,
            'invoice' =>$invoice,
            'css1' => $css1,
            'css2' => $css2
        ]);

//        $htmldoc = new InlineStyle($html);
//        $htmldoc->applyStylesheet(file_get_contents($css1));
//        $htmldoc->applyStylesheet(file_get_contents($css2));
//        $html2 = $htmldoc->getHTML();



        $tcPdf = new \Vemid\Pdf\Renderer\TcPdf();
        $renderer = new \Vemid\Pdf\Builder($tcPdf);

        //$builder = $this->getDI()->getPdfRenderer($tcPdf);
        $renderer->setMargins(5,5,5,5);

        $downloadFile = str_replace("/","_",$invoice->getInvoice());
        $content = $renderer->render($html, 'portrait');
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $downloadFile);

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;

//        $this->view->setVar('data',$data);
//        $this->view->setVar('invoice',$invoice);
//
    }

    public function overviewAction($id)
    {
        return parent::overviewAction($id);
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return Client::class;
    }
}
