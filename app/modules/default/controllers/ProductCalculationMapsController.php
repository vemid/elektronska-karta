<?php

use Vemid\Controller\CrudController;
use Vemid\Form\Renderer\Json as Renderer;

/**
 * Class ProductCalculationMapController
 *
 * @package Frontemd\Controllers
 */

class ProductCalculationMapsController extends CrudController
{
    public function getCreateFormAction()
    {
        $renderer = new Renderer();

        $form = $this->getForm();


        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        if ($codeType) {
            /** @var Code $oldCollection */
            $oldCollection = $this->entityManager->findOne(Code::class, [
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
                Code::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'codeTypeId' => $codeType->getId(),
                    'code' => '07'
                ]
            ]);

            $oldCollectionOptions = [];
            foreach ($oldCollection->getClassifications(['OrderItem' => 'name', 'distinct' => 'name']) as $classification) {
                if (in_array($classification->getName(), $oldCollectionOptions, true)) {
                    continue;
                }

                $oldCollectionOptions[$classification->getId()] = $classification->getName();
            }

            $form->get(CalculationMapping::PROPERTY_CLASSIFICATION_ID)->setOptions($oldCollectionOptions);
            $form->get(CalculationMapping::PROPERTY_MARK_UP)->setAttribute('placeholder','%');

            return $renderer->render($form);
        }
    }

    public function listAction()
    {
        $this->view->setVar('calculationMappings', $this->entityManager->find(CalculationMapping::class));
        $this->view->pick('product-calculation-maps/list');
    }

    public function getUpdateFormAction($id)
    {
        $form = parent::getUpdateFormAction($id);
        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        if ($codeType) {
            /** @var Code $oldCollection */
            $oldCollection = $this->entityManager->findOne(Code::class, [
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
                Code::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'codeTypeId' => $codeType->getId(),
                    'code' => '07'
                ]
            ]);

            $oldCollectionOptions = [];
            foreach ($oldCollection->getClassifications(['OrderItem' => 'name', 'distinct' => 'name']) as $classification) {
                if (in_array($classification->getName(), $oldCollectionOptions, true)) {
                    continue;
                }

                $oldCollectionOptions[$classification->getId()] = $classification->getName();
            }

            $form[CalculationMapping::PROPERTY_CLASSIFICATION_ID]['options'] = $oldCollectionOptions;
        }

        $form[CalculationMapping::PROPERTY_MARK_UP]['default'] = $form[CalculationMapping::PROPERTY_MARK_UP]['default'] * 100;

        return $form;
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return CalculationMapping::class;
    }
}