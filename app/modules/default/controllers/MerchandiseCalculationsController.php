<?php

use Vemid\Controller\CrudController;
use Vemid\Service\MerchandiseCalculation\Item\Manager;
use Vemid\Service\Product\MerchandisePrice;

class MerchandiseCalculationsController extends CrudController
{

    public function listAction()
    {
        $this->view->setVar('merchandiseCalculations', $this->entityManager->find(MerchandiseCalculation::class,[
            'order' => 'id DESC'
        ]));
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $form = $this->getForm();
        $postData = $this->request->getPost();

        /** @var MerchandiseCalculation $merchandiseCalculation */
        $merchandiseCalculation = $form->getEntity();

        $messages = [];
        if (count($messages)) {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('File mora biti exce;!'),
                MerchandiseCalculation::PROPERTY_FILE,
                \Vemid\Messenger\Message::SUCCESS
            );

            return;
        }

        /** @var Phalcon\Http\Request\File $file */
        $file = array_pop($this->request->getUploadedFiles());

        if ($file !== null) {
            $uploadPath = $merchandiseCalculation->getUploadPath();

            $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();
            if ($file->moveTo($filePath)) {
                if (is_file($uploadedFile = $uploadPath . $filePath)) {
                    @unlink($uploadedFile);
                }

                $postData[MerchandiseCalculation::PROPERTY_FILE] = basename($filePath);
            }
        }


        if (!$form->isValid($postData)) {
            $this->addFlashMessagesFromForm($form);
            return;
        }

        $form->synchronizeEntityData();

        if (!$this->entityManager->save($merchandiseCalculation)) {
            $this->addFlashMessagesFromEntity($merchandiseCalculation);
            return;
        }

        if ($merchandiseCalculation->getFile()) {
            $filePath = $merchandiseCalculation->getUploadPath() . $merchandiseCalculation->getFile();

            $manager = new Manager($filePath, $merchandiseCalculation->getId());
            $manager->import();
        }
        $this->dispatcher->setParam('url', '/merchandise-calculations/overview/' . $form->getEntity()->getEntityId());

        return ['error' => false, 'messages' => []];
    }

    public function overviewAction($id)
    {

        /** @var MerchandiseCalculation $merchandiseCalculation */
        $merchandiseCalculation = $this->entityManager->findOne(MerchandiseCalculation::class, $id);

        if (!$merchandiseCalculation) {
            $this->returnNotFound();
        }

        /** @var MerchandiseCalculationItem[] $merchandiseCalculationItems */
        $merchandiseCalculationItems = $this->entityManager->find(MerchandiseCalculationItem::class,[
            MerchandiseCalculationItem::PROPERTY_MERCHANDISE_CALCULATION_ID . ' =:marchandiseCalculationId: ',
            'bind' => [
                'marchandiseCalculationId' => $id
            ]
        ]);

        $data = [];
        foreach ($merchandiseCalculationItems as $merchandiseCalculationItem) {
            $price = new MerchandisePrice($this->entityManager,$merchandiseCalculationItem,$this->getDI()->getModelsManager());
            $serbianPrices = $price->getSerbianPrices($merchandiseCalculationItem->getProduct())[0];
            $wholesalePrices = $price->getWholesalePrices($merchandiseCalculationItem->getProduct())[0];
            $montenegroPrices = $price->getMontenegroPrices($merchandiseCalculationItem->getProduct())[0];
            $bosnianPrices = $price->getBosnianPrices($merchandiseCalculationItem->getProduct())[0];

            $data[] = [
                'code' => $merchandiseCalculationItem->getProduct()->getCode(),
                'name' => $merchandiseCalculationItem->getProduct()->getName(),
                'course' => $merchandiseCalculationItem->getCourse(),
                'purchasePriceRSD' => $serbianPrices['purchasePrice'],
                'wholesalePriceRSD' => $serbianPrices['wholesalePrice'],
                'retailPriceRSD' => $serbianPrices['retailPrice'],
                'purchasePriceEUR' => $wholesalePrices['purchasePrice'],
                'wholesalePriceEUR' => $wholesalePrices['wholesalePrice'],
                'retailPriceEUR' => $wholesalePrices['retailPrice'],
                'purchasePriceCG' => $montenegroPrices['purchasePrice'],
                'wholesalePriceCG' => $montenegroPrices['wholesalePrice'],
                'retailPriceCG' => $montenegroPrices['retailPrice'],
                'purchasePriceBIH' => $bosnianPrices['purchasePrice'],
                'wholesalePriceBIH' => $bosnianPrices['wholesalePrice'],
                'retailPriceBIH' => $bosnianPrices['retailPrice'],
            ];
        }

        $this->view->pick('merchandise-calculations/overview');
        $this->view->setVar('merchandiseCalculation', $merchandiseCalculation);
        $this->view->setVar('datas', $data);
    }

    public function finishAction($id)
    {
        /** @var MerchandiseCalculation $merchandiseCalculation */
        if (!$merchandiseCalculation = $this->entityManager->findOne(MerchandiseCalculation::class, $id)) {
            return $this->returnNotFound();
        }

        if ($merchandiseCalculation->getType() !== MerchandiseCalculation::TYPE_PREPARED) {
            $this->flashSession->error('Već je odradjeno!');
            return;
        }

        $merchandiseCalculation->setType(MerchandiseCalculation::TYPE_FINISHED);

        if (!$this->entityManager->save($merchandiseCalculation)) {
            $this->addFlashMessagesFromEntity($merchandiseCalculation);
        }

        $this->addFlashMessage(
            'Zatvara se! Uskoro će početi sinhronizacija!',
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

        /**
     * @return string
     */
    public function getEntityName()
    {
        return MerchandiseCalculation::class;
    }
}