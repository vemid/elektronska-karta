<?php

use Vemid\Controller\BaseController;

/**
 * Class ElectronCardFactoriesController
 *
 * @package Default\Controllers
 */
class DataController extends BaseController
{
    public function filterByTermAction($table, $column, $returnType)
    {
        $this->view->disable();
        $json = [];
        $term = $this->request->getQuery('term');

        /** @var \Vemid\Entity\EntityInterface[] $entities */
        $entities = $this->entityManager->find($table, [
            $column . ' LIKE :term:',
            'bind' => [
                'term' => "%$term%"
            ],
        ]);

        foreach ($entities as $entity) {
            $id = (int)$entity->getDisplayName();
            if (array_key_exists($id, $json)) {
                continue;
            }

            if ($returnType === 'string') {
                $id = call_user_func([$entity, 'get' . ucfirst($column)]);
            } else {
                $id = $entity->getEntityId();
            }

            $json[$id]['id'] = $id;
            $json[$id]['value'] = call_user_func([$entity, 'get' . ucfirst($column)]);
            $json[$id]['label'] = call_user_func([$entity, 'get' . ucfirst($column)]);
        }

        $this->response->setContent(json_encode($json));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return ElectronCardFactory::class;
    }
}
