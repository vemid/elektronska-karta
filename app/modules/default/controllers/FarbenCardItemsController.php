<?php
/**
 * Created by PhpStorm.
 * User: adminbk
 * Date: 31.3.18.
 * Time: 11.42
 */
use Vemid\Controller\CrudController;

class FarbenCardItemsController extends CrudController
{

    /**
     * @return string
     */
    public function getEntityName()
    {
        return FarbenCardItem::class;
    }

}