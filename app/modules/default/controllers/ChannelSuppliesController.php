<?php
use Vemid\Controller\CrudController;

/**
 * Class ChannelSuppliesController
 *
 * @package Default\Controllers
 */
class ChannelSuppliesController extends CrudController
{
    public function listAction()
    {
        $this->tag::appendTitle($this->translator->t("Pregled Kanala Snabdevanja"));
        $this->view->setVar('channelSupplies', $this->entityManager->find(ChannelSupply::class));

        $this->view->pick('channel-supplies/list');
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return ChannelSupply::class;
    }
}