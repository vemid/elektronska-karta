<?php

use \Vemid\Controller\BaseController;
use \Vemid\Service\UserNotification\Messages;

/***
 * Class MaterialSamplesController
 */
class MaterialSamplesController extends BaseController
{
    public function createFormAction()
    {
        $this->view->setVar('form', new MaterialSampleForm(new MaterialSample(), ['materialPicture']));
        $this->view->pick('material-samples/create-form');
    }

    public function editFormAction($id)
    {
        /** @var MaterialSample $materialSample */
        $materialSample = $this->entityManager->findOne(MaterialSample::class, $id);

        if (!$materialSample) {
            return $this->returnNotFound();
        }

        $form = new MaterialSampleEditForm(null, ['materialSample' => $materialSample]);
        $this->view->setVar('form', $form);
        $this->view->setVar('materialSample', $materialSample);
        $this->view->pick('material-samples/edit-form');
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var array $postData */
        $postData = $this->request->getPost();

        /** @var Classification $materialCode */
        $materialCode = $this->entityManager->findOne(Classification::class, [
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $postData['materialCodeId']
            ]
        ]);

        /** @var Classification $materialTypeCode */
        $materialTypeCode = $this->entityManager->findOne(Classification::class, [
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $postData['materialTypeCodeId']
            ]
        ]);

        /** @var Classification $subMaterialCode */
        $subMaterialCode = $this->entityManager->findOne(Classification::class, [
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $postData['subMaterialCodeId']
            ]
        ]);

        $materialSample = new MaterialSample();

        $postData['materialCodeId'] = $materialCode ? $materialCode->getId() : '';
        $postData['materialTypeCodeId'] = $materialTypeCode ? $materialTypeCode->getId() : '';
        $postData['subMaterialCodeId'] = $subMaterialCode ? $subMaterialCode->getId() : '';

        if ($this->request->hasFiles()) {
            /** @var Phalcon\Http\Request\File $file */
            $file = array_pop($this->request->getUploadedFiles());

            if ($file !== null) {
                $uploadPath = $materialSample->getUploadPath();

                $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();
                if ($file->moveTo($filePath)) {
                    if (is_file($imagePath = $uploadPath . $materialSample->getMaterialPicture())) {
                        @unlink($imagePath);
                    }

                    $materialSample->setMaterialPicture(basename($filePath));
                }
            }
        }

        $form = new MaterialSampleForm($materialSample);
        if ($form->isValid($postData)) {
            $materialSample->setStatus(MaterialSample::DEMANDED);
            $this->saveEntity($materialSample);

            $userNotificationService = $this->getDI()->getUserNotification();
//            $userNotificationService->create($materialSample, Messages::MATERIAL_SAMPLE_CREATE);

           return $this->response->redirect('material-samples/list');
        }

        return $this->response->redirect('material-samples/create-form');
    }

    public function updateAction($id)
    {
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var array $postData */
        $postData = $this->request->getPost();

        /** @var MaterialSample $materialSample */
        $materialSample = $this->entityManager->findOne(MaterialSample::class, $id);
        if (!$materialSample) {
            $this->returnNotFound();
        }

        if ($this->request->hasFiles()) {
            /** @var Phalcon\Http\Request\File $file */
            $file = array_pop($this->request->getUploadedFiles());

            if ($file !== null) {
                $uploadPath = $materialSample->getUploadPath();

                $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();
                if ($file->moveTo($filePath)) {
                    if (is_file($imagePath = $uploadPath . $materialSample->getMaterialPicture())) {
                        @unlink($imagePath);
                    }

                    $materialSample->setMaterialPicture(basename($filePath));
                }
            }
        }

        $form = new MaterialSampleForm($materialSample);
        if ($form->isValid($postData)) {
            /** @var Classification $materialTypeCode */
            $materialTypeCode = $this->entityManager->findOne(Classification::class, [
                Classification::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'code' => $postData[MaterialSample::PROPERTY_MATERIAL_TYPE_CODE_ID]
                ]
            ]);

            if ($materialTypeCode) {
                $materialSample->setMaterialTypeClassification($materialTypeCode);
            }

            /** @var Classification $materialCode */
            $materialCode = $this->entityManager->findOne(Classification::class, [
                Classification::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'code' => $postData[MaterialSample::PROPERTY_MATERIAL_CODE_ID]
                ]
            ]);

            if ($materialCode) {
                $materialSample->setMaterialClassification($materialCode);
            }

            /** @var Classification $subMaterialCode */
            $subMaterialCode = $this->entityManager->findOne(Classification::class, [
                Classification::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'code' => $postData[MaterialSample::PROPERTY_SUB_MATERIAL_CODE_ID]
                ]
            ]);

            if ($subMaterialCode) {
                $materialSample->setSubMaterialClassification($subMaterialCode);
            }

            $this->saveEntity($materialSample);

            return $this->response->redirect('material-samples/list');
        }
    }

    public function getDataAction()
    {
        $this->view->disable();
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var MaterialSample $materialSample */
        $materialSample = $this->entityManager->findOne(MaterialSample::class, $this->request->getPost('materialSampleId'));
        if (!$materialSample) {
            $this->returnNotFound();
        }

        $form = new MaterialSampleEditForm(null, ['materialSample' => $materialSample]);
        $renderer = new \Vemid\Form\Renderer\Json();

        $this->response->setContent(json_encode($renderer->render($form)));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityName()
    {
        return MaterialSamples::class;
    }


    public function getListDataAction()
    {
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');

        $typeMaterial = $this->request->getQuery('typeMaterial', 'string', 0);

        $query = $this->modelsManager->createBuilder();
        $query->addFrom(MaterialSample::class, 'ms')
            ->leftJoin(Classification::class, 'subCls.id = ms.subMaterialCodeId', 'subCls');

        if ($typeMaterial) {
            $query->leftJoin(Classification::class, 'cls.id = ms.subMaterialCodeId', 'cls')
                ->leftJoin(Classification::class, 'cls1.id = cls.parentClassificationId', 'cls1')
                ->leftJoin(Classification::class, 'cls2.id = cls1.parentClassificationId', 'cls2')
                ->andWhere('cls2.code = :code:', [
                    'code' => $typeMaterial
                ]);
        }

        if (is_array($search) && $search['value']) {
            $query->andWhere('subCls.code LIKE :search: OR subCls.name LIKE :search: OR ms.name LIKE :search:', [
                'search' => '%' . $search['value'] . '%'
            ]);
        }
        $query->orderBy('ms.id desc');

        $queryForCount = clone $query;
        $allMaterialSamples = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);

        /** @var MaterialSample[] $materialSamples */
        $materialSamples = $query->getQuery()->execute();

        $data = [];
        foreach ($materialSamples as $materialSample) {

            $data[] = [
                $materialSample->getDisplayName(),
                $materialSample->getSupplier() ? $materialSample->getSupplier()->getName() : '',
                $materialSample->getSubMaterialClassification() ? $materialSample->getSubMaterialClassification()->getName() : '',
                ucfirst(strtolower($materialSample->getStatus())),
                $materialSample->getId()
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $allMaterialSamples->count(),
            'recordsFiltered' => $allMaterialSamples->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function listAction()
    {
        /** @var \CodeType $codeType */
        $codeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        if (!$codeType) {
            return $this->returnNotFound();
        }

        /** @var \Code $code */
        $code = $this->entityManager->findOne(\Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '1',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if (!$code) {
            return $this->returnNotFound();
        }

        $classifications = $code->getClassificationCategories([
            Classification::PROPERTY_PARENT_CLASSIFICATION_ID . ' IS NULL AND '.
            Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' != :codeId:',
            'bind' => [
                'codeId' => $code->getId()
            ]
        ]);

        $options=[];
        foreach ($classifications as $classification) {
            $options[$classification->getCode()] = $classification->getName();
        }

        $codes = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $this->view->setVar('productCodeId', $codes);
        $this->view->setVar('options', $options);
    }

    public function overviewAction($id)
    {
        /** @var \MaterialSample $materialSample */
        $materialSample = $this->entityManager->findOne(MaterialSample::class, $id);
        if(!$materialSample){
            $this->returnNotFound();
        }


        $this->view->setVar('materialSample', $materialSample);
        $this->view->pick('material-samples/overview-material-sample');
    }
}
