<?php

use \Vemid\Controller\CrudController;
use \Vemid\Form\Renderer\Html;

/**
 * Class WarehouseOrdersController
 */
class EfectusController extends CrudController
{
    public function viewOrderAction($codeId, $hash)
    {
        /** @var Code $code */
        if (!$code = $this->entityManager->findOne(Code::class, $codeId)) {
            return $this->returnNotFound();
        }

        $error = null;
        $crypto = new \Vemid\Helper\Crypto($this->getDI()->getConfig());

        try {
            $date = new Vemid\Date\DateTime($crypto->decrypt($hash));
            $today = new \Vemid\Date\DateTime();
            $firstMondayInCurrentWeek = new \Vemid\Date\DateTime('now', new DateTimeZone('Europe/Belgrade'));
            $firstMondayInCurrentWeek->modify('monday this week');

            if ($date > $today || $date < $firstMondayInCurrentWeek || $today->diff($date)->days > 7) {
                $error = 'Link je istekao!';
            }

        } catch (\Exception $e) {
            $error = 'Nevalidan link';
        }

        /** @var Efectus[] $efectus */
        $efectus = $this->entityManager->find(Efectus::class, [
            Efectus::PROPERTY_FROM_CODE_ID . ' = :codeId: AND ' .
            Efectus::PROPERTY_DATE . ' BETWEEN :startDate: AND :endDate:',
            'bind' => [
                'codeId' => $codeId,
                'startDate' => $date->getUnixFormat(),
                'endDate' => $today->getUnixFormat(),
            ]
        ]);

        $byShops = [];
        foreach ($efectus as $record) {
            if ($record->getWarehouseOrderItem()) {
                continue;
            }

            $byShops[$record->getToCodeId()][] = $record;
        }

        foreach ($byShops as $shopId => $records) {
            $channelSupplyEntity = $this->entityManager->findOne(ChannelSupplyEntity::class, [
                ChannelSupplyEntity::PROPERTY_ENTITY_ID . ' = :entityId: AND ' .
                ChannelSupplyEntity::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:',
                'bind' => [
                    'entityId' => $shopId,
                    'entityTypeId' => \Vemid\Entity\Type::CODE
                ]
            ]);

            if (!$channelSupplyEntity) {
                throw new LogicException(sprintf('Cannot find channel supply for shopId:%s', $shopId));
            }

            $exitChannelSupplyEntity = $this->entityManager->findOne(ChannelSupplyEntity::class, [
                ChannelSupplyEntity::PROPERTY_ENTITY_ID . ' = :entityId: AND ' .
                ChannelSupplyEntity::PROPERTY_ENTITY_TYPE_ID . ' = :entityTypeId:',
                'bind' => [
                    'entityId' => $code->getId(),
                    'entityTypeId' => \Vemid\Entity\Type::CODE
                ]
            ]);

            $warehouseOrder = new WarehouseOrder();
            $warehouseOrder->setChannelSupplyEntity($channelSupplyEntity);
            $warehouseOrder->setExitChannelSupplyEntity($exitChannelSupplyEntity);
            $warehouseOrder->setPlanedExitDate($today);
            $warehouseOrder->setStatus(WarehouseOrder::EDITABLE);
            $warehouseOrder->setPosted(false);
            $warehouseOrder->setType('A');
            $warehouseOrder->setUseCourierService(true);

            if (!$this->entityManager->save($warehouseOrder)) {
                $this->addFlashMessagesFromEntity($warehouseOrder);
                return;
            }

            /** @var Efectus $record */
            foreach ($records as $record) {
                $warehouseOrderItem = new WarehouseOrderItem();
                $warehouseOrderItem->setProductSize($record->getProductSize());
                $warehouseOrderItem->setWarehouseOrder($warehouseOrder);
                $warehouseOrderItem->setQuantity($record->getQty());

                if (!$this->entityManager->save($warehouseOrderItem)) {
                    $this->addFlashMessagesFromEntity($warehouseOrder);
                    return;
                }

                $record->setWarehouseOrderItem($warehouseOrderItem);
                $this->entityManager->save($record);
            }
        }

        $this->view->setVar('efectus', $efectus);
        $this->view->setVar('code', $code);
        $this->view->setVar('error', $error);
        $this->view->pick('efectus/view-order');
    }

    public function manageWeeklyOrdersAction($hash)
    {
        $error = null;
        $crypto = new \Vemid\Helper\Crypto($this->getDI()->getConfig());

        try {
            $date = new Vemid\Date\DateTime($crypto->decrypt($hash));
            $today = new \Vemid\Date\DateTime();
            $firstMondayInCurrentWeek = new \Vemid\Date\DateTime('now', new DateTimeZone('Europe/Belgrade'));
            $firstMondayInCurrentWeek->modify('monday this week');

            if ($date > $today || $date < $firstMondayInCurrentWeek || $today->diff($date)->days > 7) {
//                $error = 'Link je istekao!';
            }

        } catch (\Exception $e) {
            $error = 'Nevalidan link';
        }

        $form = new EfectusManageForm(null, [
            'date' => $date
        ]);

        $this->view->setVar('date', $date);
        $this->view->setVar('form', (new Html())->render($form, true, 'col-xs-12 efectus', true, false));
        $this->view->setVar('error', $error);
        $this->view->pick('efectus/manage-weekly-orders');
    }

    public function getListDataAction($date)
    {
        $this->view->disable();
        $date = new DateTime($date);

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');


        $query = $this->modelsManager->createBuilder();
        $query->addFrom(Efectus::class, 'ef')
            ->leftJoin(ProductSize::class, 'ef.productSizeId = ps.id', 'ps')
            ->leftJoin(Product::class, 'p.id = ps.productId', 'p')
            ->where('ef.' . Efectus::PROPERTY_DATE . ' = :date:', [
                'date' => $date->format('Y-m-d')
            ])
            ->andWhere('ef.' . Efectus::PROPERTY_EXCLUDE . ' = :exclude:', [
                'exclude' => false
            ]);

        if (is_array($search) && $search['value']) {
            $query->andWhere('p.' . Product::PROPERTY_NAME . ' LIKE :name:', [
                'name' => "%{$search['value']}%"
            ]);
        }

        $query->orderBy('ef.id desc');

        $queryForCount = clone $query;

        /** @var Efectus[] $efectusAll */
        $efectusAll = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);
        $count = $efectusAll->count();

        /** @var Efectus[] $efectus */
        $efectus = $query->getQuery()->execute();

        $data = [];
        foreach ($efectus as $record) {
            $data[] = [
                (string)$record->getProductSize()->getProduct(),
                (string)$record->getProductSize()->getCode(),
                (string)$record->getCodeTo(),
                (string)$record->getCodeFrom(),
                $record->getQty(),
                $record->getId()
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getBaseProductsAction()
    {
        $this->view->disable();
        $json = [];
        $term = $this->request->getQuery('term');

        $query = $this->modelsManager->createBuilder()
            ->addFrom(Product::class, 'p')
            ->innerJoin(ProductSize::class, 'p.id = ps.productId', 'ps')
            ->innerJoin(Efectus::class, 'ps.id = e.productSizeId', 'e')
            ->where('p.code LIKE :term: OR p.name LIKE :term:', [
                'term' => "%$term%"
            ]);

        $products = $query->getQuery()->execute();

        foreach ($products as $product) {
            $id = $product->getId();
            $json[$id]['id'] = $id;
            $json[$id]['value'] = $product->getName();
            $json[$id]['label'] = $product->getName() . ' (' . $product->getCode() . ')';
        }

        $this->response->setContent(json_encode($json));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function setExcludeByFilterAction($date)
    {
        $this->view->disable();

        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $date = new DateTime($date);
        /** @var Efectus[] $allEfectusReset */
        $allEfectusReset = $this->entityManager->find(Efectus::class, [
            Efectus::PROPERTY_EXCLUDE . ' = :exclude: AND '.
            Efectus::PROPERTY_DATE . ' = :date:',
            'bind' => [
                'exclude' => true,
                'date' => $date->format('Y-m-d')
            ]
        ]);

        foreach ($allEfectusReset as $object) {
            $object->setExclude(false);
            $this->entityManager->save($object);
        }

        $shopInCodeId = json_decode($this->request->getPost('codeId'));
        $shopOutCodeId = json_decode($this->request->getPost('codeOut'));
        $productId = json_decode($this->request->getPost('productId'));
        $seasonClassificationId = json_decode($this->request->getPost('season'));
        $collectionId = json_decode($this->request->getPost('collection'));
        $priorityId = json_decode($this->request->getPost('priority'));

        if (
            !empty($shopInCodeId) ||
            !empty($shopOutCodeId) ||
            !empty($productId) ||
            !empty($seasonClassificationId) ||
            !empty($collectionId) ||
            !empty($priorityId)
        ) {

            $query = $this->modelsManager->createBuilder();
            $query->addFrom(Efectus::class, 'ef')
                ->innerJoin(ProductSize::class, 'ef.productSizeId = ps.id', 'ps')
                ->innerJoin(Product::class, 'p.id = ps.productId', 'p');


            $query->where('ef.' . Efectus::PROPERTY_DATE . ' = :date:', [
                'date' => $date->format('Y-m-d')
            ]);

            if (!empty($shopInCodeId)) {
                $query->orWhere('ef.' . Efectus::PROPERTY_TO_CODE_ID . ' IN ({codeTo:array})', [
                    'codeTo' => $shopInCodeId
                ]);
            }

            if (!empty($shopOutCodeId)) {
                $query->orWhere('ef.' . Efectus::PROPERTY_FROM_CODE_ID . ' IN ({codeFrom:array})', [
                    'codeFrom' => $shopOutCodeId
                ]);
            }

            if (!empty($productId)) {
                $query->orWhere('p.' . Product::PROPERTY_ID . ' IN ({productId:array})', [
                    'productId' => $productId
                ]);
            }

            if (!empty($priorityId)) {
                $query->orWhere('p.' . Product::PROPERTY_PRIORITY_ID . ' IN ({priorityId:array})', [
                    'priorityId' => $priorityId
                ]);
            }

            /** @var Efectus[] $efectusToExclude */
            $efectusToExclude = $query->getQuery()->execute();

            foreach ($efectusToExclude as $value) {
                $value->setExclude(true);
                $this->entityManager->save($value);
            }
        }

        $this->response->setContent(json_encode(['error' => false]));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getEntityName()
    {
        return Efectus::class;
    }
}
