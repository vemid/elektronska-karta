<?php


use Vemid\Controller\CrudController;
use \Vemid\Service\EFiscal\Manager;
use \Vemid\Entity\Repository\DealershipWarrantRepository;
use \Vemid\Form\Renderer\Json as Renderer;
use \Vemid\Service\MisWsdl\Query;

/**
 * Class EFiscalController
 *
 * @package Frontemd\Controllers
 */

class EFiscalController extends CrudController
{

    public function listAction() {
        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_FOOT_JS)
            ->addJs(APP_PATH . 'assets/js/drop-zone.js');

        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_HEAD_CSS)
            ->addCss(APP_PATH . 'assets/css/drop-zone.css');
    }

    public function uploadFileAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $warrantStatement = new WarrantStatement();

        /** @var Phalcon\Http\Request\File $file */
        $file = array_pop($this->request->getUploadedFiles());

        if ($file !== null) {
            $uploadPath = $warrantStatement->getUploadPath();
            $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();

            if (!$file->moveTo($filePath)) {
                throw new \RuntimeException('File did not uploaded to server!');
            }

            $manager = new Manager($this->entityManager, $filePath);
            $manager->import();

            $this->addFlashMessage(
                'Uspešno importovani nalozi!',
                null,
                \Vemid\Messenger\Message::SUCCESS
            );

            $this->dispatcher->setParam('url', '/dealerships/list-all');

            return;
        }


    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return EFiscalController::class;
    }

}