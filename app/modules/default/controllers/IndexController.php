<?php

use Vemid\Controller\BaseController;
use Vemid\Entity\Repository\CarCostRepository;

/**
 * Class AuthController
 */
class IndexController extends BaseController
{

    public function indexAction()
    {
        $config = $this->getDI()->getConfig();
        $this->view->setVar('siteUrl', ltrim($config->application->baseUri, '/'));
    }
}