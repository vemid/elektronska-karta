<?php

use \Vemid\Controller\CrudController;

/**
 * Class WarehouseOrdersController
 */
class WarehouseOrderGroupItemsController extends CrudController
{
    public function getEntityName()
    {
        return WarehouseOrderGroupItem::class;
    }
}
