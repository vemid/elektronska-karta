<?php
use Vemid\Controller\CrudController;


class ProductionMachinesController extends CrudController
{
    public function listAction()
    {
        $this->tag::appendTitle($this->translator->t("Tipovi Proizvodnih Masina"));
        $this->view->setVar('productionMachines', $this->entityManager->find(ProductionMachine::class));
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return ProductionMachine::class;
    }

}