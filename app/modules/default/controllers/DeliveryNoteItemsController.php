<?php

use \Vemid\Controller\BaseController;
use  \Vemid\Service\MisWsdl\Query;
use \Vemid\Entity\Type;
use Vemid\Form\Renderer\Json as Renderer;
use \Vemid\Service\DeliveryNote\Calculator;

/**
 * Class DeliveryNoteItemsController
 */
class DeliveryNoteItemsController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function getCreateFormAction($orderItemId, $type)
    {
        if (!$orderItem = $this->entityManager->findOne(OrderItem::class, $orderItemId)) {
            return $this->returnNotFound();
        }

        $deliveryNote = new DeliveryNoteItem();
        $form = $deliveryNote->getForm();
        $form->get(DeliveryNoteItem::PROPERTY_ORDER_ITEM_ID)->setDefault($orderItemId);
        if ($type === 'A') {
            $form->remove(DeliveryNoteItem::PROPERTY_QUANTITY_B);
        } else {
            $form->remove(DeliveryNoteItem::PROPERTY_QUANTITY_A);
        }

        return (new Renderer())->render($form);
    }

    /**
     * {@inheritdoc}
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $deliveredA = $postData['deliveredA'] ?? 0;
        $deliveredB = $postData['deliveredB'] ?? 0;
        $price = $postData['purchasePrice'] ?? 0;

        $deliveryNote = new DeliveryNoteItem();
        $form = $deliveryNote->getForm();
        $postData = $this->request->getPost();

        if ($form->isValid($postData)) {

            //TODO To be revised
            $orderItem = $deliveryNote->getOrderItem();
            $calculator = new Calculator();

//            if (!empty($postData['quantityA'])) {
//                $qty = $postData['quantityA'];
//                $totalToDeliver = $calculator->getMaximumToDeliverA($orderItem, $deliveredA, $deliveredB);
//            } else {
//                $qty = $postData['quantityB'];
//                $totalToDeliver = $calculator->getMaximumToDeliverB($orderItem, $deliveredA);
//            }
//
//            if (($totalToDeliver - $qty) < 0) {
//                $this->addFlashMessage(
//                    sprintf('Količina je veća od dozvoljene za unos, max količina je %s', $totalToDeliver),
//                    null,
//                    \Vemid\Messenger\Message::DANGER
//                );
//                return;
//            }

            $form->synchronizeEntityData();
            $this->saveEntity($form->getEntity());
        } else {
            $this->addFlashMessagesFromForm($form);
        }
    }

    public function updateAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $postData = $this->request->getPost();

        /** @var DeliveryNoteItem $deliveryNote */
        if (!$deliveryNoteItem = $this->entityManager->findOne(DeliveryNoteItem::class, $postData['pk'])) {
            return $this->returnNotFound();
        }

        $form = $deliveryNoteItem->getForm();

        if ($form->isValid($postData)) {
            //TODO To be revised
//            $orderItem = $deliveryNote->getOrderItem();
//            $calculator = new Calculator();
//            if (!empty($postData['quantityA'])) {
//                $qty = $postData['quantityA'];
//                $totalToDeliver = $calculator->getMaximumToDeliverA($orderItem, $deliveredA, $deliveredB);
//            } else {
//                $qty = $postData['quantityB'];
//                $totalToDeliver = $calculator->getMaximumToDeliverB($orderItem, $deliveredA);
//            }
//
//            if (($totalToDeliver - $qty) < 0) {
//                $this->addFlashMessage(
//                    sprintf('Količina je veća od dozvoljene za unos, max količina je %s', $totalToDeliver),
//                    null,
//                    \Vemid\Messenger\Message::DANGER
//                );
//                return;
//            }

            $form->synchronizeEntityData();
            $this->saveEntity($deliveryNoteItem);
        } else {
            $this->addFlashMessagesFromForm($form);
        }
    }

    public function overviewAction($id, $code, $sizes, $qty, $price)
    {
        if (!$deliveryNote = $this->entityManager->findOne(DeliveryNote::class, $id)) {
            return $this->returnNotFound();
        }

        $this->view->setVar('qty', $qty);

        /** @var Product $product */
        $product = $this->entityManager->findOne(Product::class, [
            Product::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $code
            ]
        ]);

        if (!$product) {
            $this->flashSession->error('Proizvod nije pronađen!');
            return;
        }

        if (!$code) {
            $this->flashSession->error('Veličina nije pronađena!');
            return;
        }


        $query = $this->modelsManager->createBuilder()
            ->addFrom(ProductSize::class, 'ps')
            ->leftJoin(Code::class, 'ps.codeId = c.id', 'c')
            ->leftJoin(CodeType::class, 'c.codeTypeId = ct.id', 'ct')
            ->where('ct.code = :code:', [
                'code' => CodeType::SIZES
            ])
            ->andWhere('ps.productId = :productId:', [
                'productId' => $product->getId()
            ])
            ->andWhere('c.code IN ({codes:array})', [
                'codes' => explode(',', $sizes)
            ]);

        /** @var ProductSize[]|\Phalcon\Mvc\Model\Resultset $productSizes */
        $productSizes = $query->getQuery()->execute();

        $timeStarted = microtime(true);

        $sizes = [];
        foreach ($productSizes as $productSize) {
            $sizes[] = $productSize->getCode()->getCode();
        }

        $sizes = implode(', ', $sizes);


        $deliveryNotes = [];
        $deliveryNoteMisSql = <<<SQL
SELECT * FROM elkarta_isporucena_roba WHERE sif_rob = '{$productSize->getProduct()->getCode()}' AND sif_ent_rob IN ({$sizes})
SQL;

        $dataMisDeliveryNotes = null;
        $queryMisDeliveryNote = new Query($deliveryNoteMisSql);

        while (null === $dataMisDeliveryNotes) {
            try {
                $dataMisDeliveryNotes = $queryMisDeliveryNote->run();
            } catch (Exception $e) {}

            $timeElapsed = microtime(true) - $timeStarted;

            if ($timeElapsed > 10) {
                $dataMisDeliveryNotes = -1;
            }
        }

        if ($dataMisDeliveryNotes === -1) {
            $this->flashSession->error('Failed to connect to MIS web service');
            return;
        }

        foreach ($dataMisDeliveryNotes as $deliveryNoteI) {
            $deliveryNotes[$deliveryNoteI['sif_ent_rob']][$deliveryNoteI['sif_par']] = (int)$deliveryNoteI['kolic'];
        }

        $config = $this->getDI()->getConfig();

        $codeShops = $config->toArray()['foreignShops'];

        /** @var CodeType $codeTypeShops */
        $codeTypeShops = $this->entityManager->findOne(CodeType::class,[
            CodeType::PROPERTY_CODE . ' = :shops:',
            'bind' => [
                'shops' => CodeType::SHOPS
            ]

        ]);

        $query = $this->modelsManager->createBuilder()
            ->columns([
                'c.id'
            ])
            ->addFrom(\Code::class, 'c')
            ->where('c.code in ({codes:array})', [
                'codes' => $codeShops
            ])
            ->andWhere('c.codeTypeId = :codeType:',[
                'codeType' => $codeTypeShops->getId()
            ])
            ->orderBy('c.code');


        $result = $query->getQuery()->execute()->toArray();

        $shops = array_column($result, 'id');

        $this->view->setVar('productSizes', $productSizes);
        $this->view->setVar('typeClient', Type::CLIENT);
        $this->view->setVar('deliveryNotes', $deliveryNotes);
        $this->view->setVar('calculator', new Calculator());
        $this->view->setVar('deliveryNote', $deliveryNote);
        $this->view->setVar('price', $price);
        $this->view->setVar('shops', $shops);
    }
}
