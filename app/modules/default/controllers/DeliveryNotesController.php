<?php

use Vemid\Controller\CrudController;
use \Vemid\Service\MisWsdl\Query;

/**
 * Class DeliveryNotesController
 *
 * @package Default\Controllers
 */
class DeliveryNotesController extends CrudController
{
    public function listAction()
    {
        $this->tag::appendTitle($this->translator->t("Pregled prenosnica"));

        $this->view->setVar('deliveryNotes', $this->entityManager->find(DeliveryNote::class,[
            'order' => 'id DESC'
        ]));
    }

    public function createAction()
    {
        if ($this->request->isPost()) {
            $form = $this->getForm();
            if ($form->isValid($this->request->getPost())) {
                $form->synchronizeEntityData();
                /** @var DeliveryNote $entity */
                $entity = $form->getEntity();
                if ($this->saveEntity($entity)) {
                    $this->dispatcher->setParam('url', '/delivery-notes/search-by-code?id=' . $entity->getId() . '&code=' . urlencode($entity->getMisDeliveryNote()));
                }
            } else {
                $this->addFlashMessagesFromForm($form);
            }
        }
    }

    public function searchByCodeAction()
    {
        $data = null;
        if (!$id = $this->request->getQuery('id')) {
            return $this->returnNotFound();
        }

        if (!$deliveryNote = $this->entityManager->findOne(DeliveryNote::class, $id)) {
            return $this->returnNotFound();
        }

        if ($code = $this->request->getQuery('code')) {

            $sql = <<<SQL
SELECT TRIM(p.sif_rob) sif_rob, TRIM(r.naz_rob) naz_rob, TRIM(p.sif_ent_rob) sif_ent_rob,round(p.pre_kol,0) kolic,cen_zal 
FROM prenos_st p
LEFT JOIN roba r ON r.sif_rob = p.sif_rob 
WHERE p.ozn_pre = '$code'
SQL;

            $query = new Query($sql);
            $timeStarted = microtime(true);

            while (empty($data)) {
                try {
                    $data = $query->run();
                } catch (Exception $e) {}

                $timeElapsed = microtime(true) - $timeStarted;

                if ($timeElapsed > 10) {
                    $this->flashSession->error('Failed to connect to MIS web service');
                    $data = -1;
                }
            }
        }

        $receiverNotes = [];
        foreach ($data as $item) {
            if (!isset($receiverNotes[$item['sif_rob']])) {
                $receiverNotes[$item['sif_rob']] = [
                    'sif_rob' => $item['sif_rob'],
                    'naz_rob' => $item['naz_rob'],
                    'kolic' => $item['kolic'],
                    'cen_zal' => $item['cen_zal'],
                    'sif_ent_rob' => [$item['sif_ent_rob']]
                ];
            } else {
                $receiverNotes[$item['sif_rob']]['cen_zal'] += $item['cen_zal'];
                $receiverNotes[$item['sif_rob']]['kolic'] += $item['kolic'];
                $receiverNotes[$item['sif_rob']]['sif_ent_rob'] = array_merge([$item['sif_ent_rob']], $receiverNotes[$item['sif_rob']]['sif_ent_rob']);
            }
        }

        /** @var \Vemid\Entity\Repository\DeliveryNoteItemRepository $deliveryNoteItemRepository */
        $deliveryNoteItemRepository = $this->entityManager->getRepository(DeliveryNoteItem::class);


        $this->view->pick('delivery-notes/search-by-code');
        $this->view->setVar('receiverNotes', $receiverNotes);
        $this->view->setVar('deliveryNote', $deliveryNote);
        $this->view->setVar('deliveryNoteItemRepository', $deliveryNoteItemRepository);
    }

    public function finishAction($id)
    {
        /** @var DeliveryNote $deliveryNote */
        if (!$deliveryNote = $this->entityManager->findOne(DeliveryNote::class, $id)) {
            return $this->returnNotFound();
        }

        if ($deliveryNote->getType() !== DeliveryNote::TYPE_PREPARED) {
            $this->flashSession->error('Već je odradjeno!');
            return;
        }

        $deliveryNote->setType(DeliveryNote::TYPE_FINISHED);

        /** @var Vemid\Service\MisWsdl\AddTransfer $misWdsl */
        $misWdsl = new Vemid\Service\MisWsdl\AddTransfer($this->entityManager,$deliveryNote);

        $timeStarted = microtime(true);

        $data = false;
        while (!$data ) {
            try {
                $data = $misWdsl->transfer();
            } catch (Exception $e) {}

            $timeElapsed = microtime(true) - $timeStarted;

            if ($timeElapsed > 10) {
                $this->flashSession->error('Failed to connect to MIS web service');
                $data = false;
            }

            if($data) {
                if(!$this->entityManager->save($deliveryNote)){
                $this->addFlashMessagesFromEntity($deliveryNote);
                }

                $this->addFlashMessage(
                    'Zatvara se! Uskoro će početi sinhronizacija!',
                    null,
                    \Vemid\Messenger\Message::SUCCESS
                );
            }
        }
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return DeliveryNote::class;
    }
}
