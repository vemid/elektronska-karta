<?php

use Vemid\Controller\CrudController;
use \Vemid\Form\Renderer\Json as Renderer;

/**
 * Class CommentsController
 *
 * @package Default\Controllers
 */

class CommentsController extends CrudController
{

    public function getCreateFormAction()
    {
        $params = $this->dispatcher->getParams();
        if (!isset($params[0], $params[1]) && !$productionWorker = $this->entityManager->findOne(ProductionWorker::class, $params[0])) {
            return $this->returnNotFound();
        }

        $form = $this->getForm();
        $form->get(Comment::PROPERTY_PRODUCTION_WORKER_ID)->setDefault($params[0]);
        $form->get(Comment::PROPERTY_TYPE)->setDefault($params[1]);
        $form->get(Comment::PROPERTY_DATE)->setDefault($params[2]);

        return (new Renderer())->render($form);
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return Comment::class;
    }

}

