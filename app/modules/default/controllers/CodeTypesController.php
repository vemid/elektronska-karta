
<?php

use Vemid\Controller\CrudController;
use Vemid\Form\Renderer\Json as Renderer;

/**
 * Class CodeTypesController
 *
 * @package Default\Controllers
 */
class CodeTypesController extends CrudController
{

    public function overviewAction($id)
    {
        $this->tag::appendTitle($this->translator->t("Pregled Kodova"));
        /** @var CodeType $codeType */
        if (!$codeType = $this->entityManager->findOne(CodeType::class, $id)) {
            return $this->returnNotFound();
        }

        $codes = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $code = strtolower($codeType->getCode());
        $code = \Phalcon\Text::uncamelize(\Phalcon\Text::camelize($code), '-');

        $this->view->setVar('codeType', $codeType);
        $this->view->setVar('codes', $codes);
        //$this->view->pick('code-types/' . $code);
    }

    public function listAction()
    {
        $this->tag::appendTitle($this->translator->t("Pregled Tipova"));
        $this->view->setVar('codeTypes', $this->entityManager->find(CodeType::class));
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return \CodeType::class;
    }
}
