<?php

use Vemid\Controller\CrudController;
use \Vemid\Messenger\Message;
use \Vemid\Form\Renderer\Json as Renderer;

class PrioritiesController extends CrudController
{

    public function listAction()
    {
        $this->tag::appendTitle($this->translator->t("Lista prioriteta"));
        $this->view->setVar('priorities', $this->entityManager->find(Priority::class, [
            'order' => 'priorityDate DESC'
        ]));

    }

    public function getCreateFormAction()
    {
        $form = new OrderForm(new Priority());

        return (new Renderer())->render($form);
    }

    public function getUpdateFormAction($id)
    {
        if (!$priority = $this->entityManager->findOne(Priority::class, $id)) {
            return $this->returnNotFound();
        }

        $form = new PriorityEditForm($priority);

        return (new Renderer())->render($form);
    }

    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $form = $this->getForm();
        $postData = $this->request->getPost();

        if ($form->isValid($postData)) {

        }

        /** @var array $oldCollectionIds */
        $oldCollectionIds = $postData['oldCollection'];

        /** @var Priority $priority */
        $priority = $form->getEntity();
        if ($this->request->hasFiles()) {
            /** @var \Phalcon\Http\Request\File $file */
            $file = array_pop($this->request->getUploadedFiles());

            if ($file !== null) {

                $uploadPath = $priority->getUploadPath();
                $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();

                if ($file->moveTo($filePath)) {
                    if (is_file($path = $uploadPath . $priority->getSketch())) {
                        @unlink($path);
                    }

                    $priority->setSketch(basename($filePath));
                }
            }
        }


        if (!$form->isValid($postData)) {
            $this->addFlashMessagesFromForm($form);

            return;
        }


        $form->synchronizeEntityData();

        if (!$this->saveEntity($priority)) {
            $this->addFlashMessagesFromEntity($priority);

            return;
        }

        foreach ($oldCollectionIds as $oldCollectionId) {
            $priorityClassification = new PriorityClassification();
            $priorityClassification->setPriority($priority);
            $priorityClassification->setClassificationId($oldCollectionId);

            if (!$this->entityManager->save($priorityClassification)) {
                $this->addFlashMessagesFromEntity($priorityClassification);
                return;
            }
        }

        $this->addFlashMessage(
            $this->translator->t('Uspešno dodat prioritet!'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );

        return;

    }

    public function updateAction($id)
    {
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var $priority Priority */
        if (!$priority = $this->getEntity($id)) {
            return $this->returnNotFound();
        }

        $postData = $this->request->getPost();
        /** @var array $oldCollectionIds */
        $oldCollectionIds = $postData['oldCollection'];

        $form = $this->getForm($priority);
        if (!$form->isValid($postData)) {
            $this->addFlashMessagesFromForm($form);

            return;
        }

        if ($this->request->hasFiles()) {
            /** @var \Phalcon\Http\Request\File $file */
            $file = array_pop($this->request->getUploadedFiles());

            if ($file !== null) {

                $uploadPath = $priority->getUploadPath();
                $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();

                if ($file->moveTo($filePath)) {
                    if (is_file($path = $uploadPath . $priority->getSketch())) {
                        @unlink($path);
                    }

                    $priority->setSketch(basename($filePath));
                }
            }
        }

        $form->synchronizeEntityData();
        if (!$this->entityManager->save($priority)) {
            $this->addFlashMessagesFromEntity($priority);

            return;
        }

        $priority->getPriorityClassifications()->delete();

        foreach ($oldCollectionIds as $oldCollectionId) {
            /** @var Classification $classification */
            $classification = $this->entityManager->findOne(Classification::class, [
                Classification::PROPERTY_CODE . ' = :code:',
                'bind' => [
                    'code' => $oldCollectionId
                ]
            ]);

            if (!$classification) {
                continue;
            }

            $priorityClassification = new PriorityClassification();
            $priorityClassification->setPriority($priority);
            $priorityClassification->setClassification($classification);

            if (!$this->entityManager->save($priorityClassification)) {
                $this->addFlashMessagesFromEntity($priorityClassification);
                return;
            }
        }

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t(sprintf('Prioritet je izmenjena!')),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );

    }

    public function getCatalogAction()
    {
        $this->view->disable();

        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var Classification $classification */
        $classification = $this->entityManager->findOne(Classification::class, [
            'code = :code:',
            'bind' => [
                'code' => $this->request->getPost('collection')
            ]
        ]);

        $priority = null;
        if ($classification) {
            $query = $this->modelsManager->createBuilder()
                ->addFrom(Priority::class, 'pr')
                ->leftJoin(PriorityClassification::class, 'pc.priorityId = pr.id', 'pc')
                ->where('pc.classificationId = :classificationId:', [
                    'classificationId' => $classification->getId()
                ])
                ->andWhere('pr.priorityNumber = :priorityNumber:', [
                    'priorityNumber' => $this->request->getPost('priority')
                ]);

            /** @var Priority $priority */
            $priority = $query->getQuery()->execute()->getFirst();
        }

        $this->response->setContent(json_encode(['img' => $priority ? $priority->getSketchPath() : '']));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }



    /**
     * @return string
     */
    public function getEntityName()
    {
        return Priority::class;
    }
}