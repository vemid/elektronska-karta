<?php
use Vemid\Controller\CrudController;
use Picqer\Barcode\BarcodeGeneratorHTML;
use \Vemid\Service\Product\Hydrator;
use \Vemid\Entity\Repository\ProductionOrderRepository;
use \Phalcon\Forms\Element\Text;
use Vemid\Form\Renderer\Json;
use \Picqer\Barcode\BarcodeGeneratorPNG;
use \Vemid\Date\DateTime;
use \Vemid\Messenger\Message;
use \Vemid\Form\Form;
use \Phalcon\Forms\Element\Hidden;

class CuttingJobWorksheetsController extends CrudController
{

    /**
     * @return string
     */
    public function getEntityName()
    {
        return CuttingJobWorksheet::class;
    }
}
