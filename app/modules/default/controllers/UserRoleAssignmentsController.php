<?php

use Vemid\Controller\CrudController;
use Vemid\Service\Acl\UserRoleAssignments;
use Vemid\Messenger\Message;
use \Phalcon\Forms\Element\Select;
use Vemid\Form\Renderer\Json as Renderer;
use \Vemid\Form\Form;

/**
 * Class UserRoleAssignmentsController
 *
 * @package Default\Controllers
 */
class UserRoleAssignmentsController extends CrudController
{
    /**
     * {@inheritdoc}
     */
    public function getCreateFormAction()
    {
        $data = parent::getCreateFormAction();
        $params = $this->dispatcher->getParams();
        $roleOptions = &$data[\UserRoleAssignment::PROPERTY_ROLE_ID]['options'];

        /** @var User $user */
        $user = $this->entityManager->findOne(User::class, $params[1]);

        if (in_array(Role::ROLE_GUEST, $roleOptions, false)) {
            $guestKey = array_search(Role::ROLE_GUEST, $roleOptions, false);
            unset($roleOptions[$guestKey]);
        }

        if (in_array('SUPER ADMIN', $roleOptions, false)) {
            $guestKey = array_search('SUPER ADMIN', $roleOptions, false);
            unset($roleOptions[$guestKey]);
        }

        if (isset($params[1]) && array_key_exists($params[1], $data[UserRoleAssignment::PROPERTY_USER_ID]['options'])) {
            $data[\UserRoleAssignment::PROPERTY_USER_ID]['default'] = $params[1];
            $data[\UserRoleAssignment::PROPERTY_USER_ID]['attributes']['disabled'] = true;

            if ($user) {
                $roles = [];
                foreach ($user->getRoles() as $role) {
                    $entityId = $role->getEntityId();
                    $roles[] = (string)$entityId;
                }

                $data[\UserRoleAssignment::PROPERTY_ROLE_ID]['attributes']['multiple'] = true;
                $data[\UserRoleAssignment::PROPERTY_ROLE_ID]['default'] = $roles;
                $data[\UserRoleAssignment::PROPERTY_ROLE_ID]['name'] .= '[]';
            }
        }

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var CodeType $codeTypeShop */
        $codeTypeShop = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SHOPS
            ]
        ]);

        if ($codeType && $codeTypeShop) {
            /** @var Code[] $codes */
            $codes = $this->entityManager->find(Code::class, [
                'codeTypeId IN ({codeTypes:array})',
                'bind' => [
                    'codeTypes' => [$codeType->getId(), $codeTypeShop->getId()]
                ],
                'order' => 'name DESC'
            ]);

            $sectors = [];
            if ($user) {
                /** @var UserSector[] $userSectors */
                $userSectors = $this->entityManager->find(UserSector::class, [
                    UserSector::PROPERTY_USER_ID . ' = :userId:',
                    'bind' => [
                        'userId' => $user->getId(),
                    ]
                ]);

                foreach ($userSectors as $userSector) {
                    $sectors[] = (string)$userSector->getCodeId();
                }
            }

            $options = ['' => '-- Izaberite --'];
            foreach ($codes as $code) {
                $options[$code->getId()] = $code->getName();
            }

            $form = new Form();
            $sectorElement = new Select('codeId', $options);
            $sectorElement->setAttribute('multiple', true);
            $sectorElement->setLabel('Sektor');
            $sectorElement->setAttribute('class', $sectorElement->getAttribute('class') . ' search');
            $sectorElement->setDefault($sectors);

            $form->add($sectorElement);
            $sectorData = (new Renderer())->render($form);
            $data['codeId'] = $sectorData['codeId'];
            $data['codeId']['name'] .= '[]';
        }

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function createAction()
    {
        $params = $this->dispatcher->getParams();
        /** @var User|null $user */
        if ($user = $this->entityManager->findOne(User::class, $params[1])) {
            $rolesId = $this->request->getPost('roleId');
            $roles = [];
            foreach ($rolesId as $roleId) {
                /** @var Role $role */
                if (!$role = $this->entityManager->findOne(Role::class, $roleId)) {
                    continue;
                }

                $roles[] = $role->getCode();
            }

            $userRoleAssigment = new UserRoleAssignments($this->entityManager);
            $userRoleAssigment->assignRolesToUser($user, $roles);

            if ($codeIds = $this->request->getPost('codeId')) {
                /** @var UserSector[] $userSectors */
                $userSectors = $this->entityManager->find(UserSector::class, [
                    UserSector::PROPERTY_USER_ID . ' = :userId:',
                    'bind' => [
                        'userId' => $user->getId(),
                    ]
                ]);

                $currentCodeIds = [];
                foreach ($userSectors as $userSector) {
                    $currentCodeIds[] = $userSector->getCodeId();
                }

                foreach ($currentCodeIds as $currentCodeId) {
                    if (!in_array($currentCodeId, $codeIds, false)) {
                        /** @var UserSector $currentUserSector */
                        $currentUserSector = $this->entityManager->findOne(UserSector::class, [
                            UserSector::PROPERTY_USER_ID . ' = :userId: AND ' .
                            UserSector::PROPERTY_CODE_ID . ' = :codeId:',
                            'bind' => [
                                'userId' => $user->getId(),
                                'codeId' => $currentCodeId,
                            ]
                        ]);

                        if (!$currentUserSector) {
                            continue;
                        }

                        $this->entityManager->delete($currentUserSector);
                    }
                }

                foreach ($codeIds as $codeId) {
                    /** @var UserSector $currentUserSector */
                    $currentUserSector = $this->entityManager->findOne(UserSector::class, [
                        UserSector::PROPERTY_USER_ID . ' = :userId: AND ' .
                        UserSector::PROPERTY_CODE_ID . ' = :codeId:',
                        'bind' => [
                            'userId' => $user->getId(),
                            'codeId' => (int)$codeId,
                        ]
                    ]);

                    if ($currentUserSector || !(int)$codeId) {
                        continue;
                    }

                    $userSector = new UserSector();
                    $userSector->setUser($user);
                    $userSector->setCodeId($codeId);

                    if (!$this->entityManager->save($userSector)) {
                        $this->addFlashMessagesFromEntity($userSector);
                        return;
                    }
                }
            }

            $this->addFlashMessage(
                $this->_translate('Data was saved successfully.'),
                null,
                Message::SUCCESS
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityName()
    {
        return \UserRoleAssignment::class;
    }
}
