<?php

use Vemid\Application\Di;
use Vemid\Controller\CrudController;
use Vemid\Service\OperationList\ItemCalculator;
use \Vemid\Date\DateTime;
use \Vemid\Service\OperatingListItemWorksheet\WorksheetManager;
use \Vemid\Date\Time;
use \Vemid\Service\Workers\Manager;
use \Vemid\Service\Sector;

class ProductionWorkersController extends CrudController
{
    public function listAction()
    {
        $this->view->setVar('workers', $this->entityManager->find(ProductionWorker::class));
    }

    public function getCreateFormAction()
    {
        $data = parent::getCreateFormAction();

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var CodeType $codeTypeLocation */
        $codeTypeLocation = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CHECKIN_LOCATION
            ]
        ]);

        if (!$codeType && !$codeTypeLocation) {
            return $this->returnNotFound();
        }

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ],
            'order' => 'name ASC'

        ]);

        $sectorsData = [];
        foreach ($sectors as $sector) {
            $sectorsData[$sector->getId()] = $sector->getName();
        }

        /** @var Code[] $sectors */
        $locations = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeTypeLocation->getId()
            ]
        ]);
        $locationData = [];
        foreach ($locations as $location) {
            $locationData[$location->getId()] = $location->getName();
        }

        $data[ProductionWorker::PROPERTY_CODE_ID]['options'] = $sectorsData;
        $data[ProductionWorker::PROPERTY_LOCATION_CODE_ID]['options'] = $locationData;

        return $data;
    }

    public function createAction()
    {
        if ($this->request->isPost()) {
            $form = $this->getForm();
            if (!$form->isValid($this->request->getPost())) {
                $this->addFlashMessagesFromForm($form);
                return;
            }

            $form->synchronizeEntityData();
            /** @var ProductionWorker $productionWorker */
            $productionWorker = $form->getEntity();
            $productionWorker->setCreated((new DateTime())->getUnixFormat());
            $this->saveEntity($productionWorker);
            $workerManager = new Manager($productionWorker, $this->getDI()->getConfig());
            $workerManager->put();
        }

    }

    public function updateAction($id)
    {
        if ($this->request->isPost()) {
            if (!$entity = $this->getEntity($id)) {
                return $this->returnNotFound();
            }

            /** @var ProductionWorker $productionWorker */
            $productionWorker = $entity;

            $form = $this->getForm($entity);
            if (!$form->isValid($this->request->getPost())) {
                $this->addFlashMessagesFromForm($form);
                return;
            }

            $form->synchronizeEntityData();
            $oldSnapshotData2 = $entity->getOldSnapshotData();
            $this->saveEntity($entity);
            //$oldSnapshotData = $entity->getOldSnapshotData();

            if (count($oldSnapshotData2)) {

                $workerManager = new Manager($productionWorker, $this->getDI()->getConfig());
                $response = $workerManager->update($oldSnapshotData2['checkinCode']);
            }

            $this->flashSession->success($response);
        }
    }

    public function deleteAction($id)
    {
        if (!$entity = $this->getEntity($id)) {
            return $this->returnNotFound();
        }

        if (!$entity->isDeletable()) {
            $this->addFlashMessage(
                $this->_translate('It is not allowed to delete this record.'),
                null,
                \Vemid\Messenger\Message::WARNING
            );
        } else {
            /** @var ProductionWorker $productionWorker */
            $productionWorker = $entity;
            $this->deleteEntity($entity);
            $workerManager = new Manager($productionWorker, $this->getDI()->getConfig());
            $response = $workerManager->delete();

        }
    }

    public function getUpdateFormAction($id)
    {
        $data = parent::getUpdateFormAction($id);
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var CodeType $codeTypeLocation */
        $codeTypeLocation = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CHECKIN_LOCATION
            ]
        ]);

        if (!$codeType && !$codeTypeLocation) {
            return $this->returnNotFound();
        }

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ],
            'order' => 'name ASC'
        ]);

        $sectorsData = [];
        foreach ($sectors as $sector) {
            $sectorsData[$sector->getId()] = $sector->getName();
        }

        /** @var Code[] $sectors */
        $locations = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeTypeLocation->getId()
            ]
        ]);
        $locationData = [];
        foreach ($locations as $location) {
            $locationData[$location->getId()] = $location->getName();
        }

        $data[ProductionWorker::PROPERTY_CODE_ID]['options'] = $sectorsData;
        $data[ProductionWorker::PROPERTY_LOCATION_CODE_ID]['options'] = $locationData;

        return $data;
    }

    public function listByWorkerAction()
    {
        $this->view->pick('production-workers/list-by-worker');
        $this->view->setVar('workers', $this->entityManager->find(ProductionWorker::class));
    }


    public function listBySectorsAction()
    {
        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ],
            'order' => 'name ASC'
        ]);

        $this->view->pick('production-workers/list-by-sectors');
        $this->view->setVar('sectors', $sectors);
    }

    public function listByWorkerDataAction()
    {
        $paramDateFrom = $this->request->getQuery('dateFrom');
        $paramDateTo = $this->request->getQuery('dateTo');
        $paramWorker = $this->request->getQuery('workerId');
        $printParam = $paramDateFrom . '/' . $paramDateTo . '/' . $paramWorker;

        $worker = $this->entityManager->findOne(ProductionWorker::class, $paramWorker);

        /** @var OperatingListItemWorksheet $workerAllOperation */
        $workerAllOperation = $this->entityManager->find(OperatingListItemWorksheet::class, [
            OperatingListItemWorksheet::PROPERTY_PRODUCTION_WORKER_ID . ' = :workerId: AND ' .
            OperatingListItemWorksheet::PROPERTY_DATE . ' >= :dateFrom: AND ' .
            OperatingListItemWorksheet::PROPERTY_DATE . ' <= :dateTo: ',
            'bind' => [
                'workerId' => $paramWorker,
                'dateFrom' => $paramDateFrom,
                'dateTo' => $paramDateTo,
            ],
            'order' => OperatingListItemWorksheet::PROPERTY_DATE,
        ]);

        /** @var CuttingJobWorksheet $workerAllCuttingJobs */
        $workerAllCuttingJobs = $this->entityManager->find(CuttingJobWorksheet::class, [
            CuttingJobWorksheet::PROPERTY_PRODUCTION_WORKER_ID . ' = :workerId: AND ' .
            CuttingJobWorksheet::PROPERTY_DATE . ' >= :dateFrom: AND ' .
            CuttingJobWorksheet::PROPERTY_DATE . ' <= :dateTo: ',
            'bind' => [
                'workerId' => $paramWorker,
                'dateFrom' => $paramDateFrom,
                'dateTo' => $paramDateTo,
            ],
            'order' => CuttingJobWorksheet::PROPERTY_DATE,
        ]);


        $itemCalculator = new ItemCalculator($this->entityManager);
        $dateTime = new DateTime();
        $dateTime->setDateType(DateTime::TYPE_DATE);

        $this->view->pick('production-workers/list-by-worker-data');
        $this->view->setVar('workerAllOperations', $workerAllOperation);
        $this->view->setVar('workerAllCuttingJobs', $workerAllCuttingJobs);
        $this->view->setVar('paramDateFrom', new DateTime($paramDateFrom));
        $this->view->setVar('paramDateTo', new DateTime($paramDateTo));
        $this->view->setVar('paramWorker', $paramWorker);
        $this->view->setVar('itemCalculator', $itemCalculator);
        $this->view->setVar('dateTime', $dateTime);
        $this->view->setVar('worker', $worker);


    }

    public function listBySectorsDataAction()
    {
        $paramDateFrom = $this->request->getQuery('dateFrom');
        $paramDateTo = $this->request->getQuery('dateTo');
        $paramSector = $this->request->getQuery('sectorId');

        $dateFrom = new DateTime($paramDateFrom);
        $dateTo = new DateTime($paramDateTo);

        /** @var \Vemid\Entity\Repository\ProductionWorkerRepository $productionWorker */
        $productionWorker = $this->entityManager->getRepository(ProductionWorker::class);

        $this->view->pick('production-workers/list-by-sectors-data');
        $this->view->setVar('sectorAllOperations', $productionWorker->getSectorDataForPeriod($paramSector, $dateFrom, $dateTo));
        $this->view->setVar('paramDateFrom', $paramDateFrom);
        $this->view->setVar('paramDateTo', $paramDateTo);
        $this->view->setVar('time', new Time('00:00'));
        $this->view->setVar('oliwTime', new Time('00:00'));
        $this->view->setVar('cuttingTime', new Time('00:00'));
        $this->view->setVar('paramSector', $paramSector);

    }

    public function printWorkerAction()
    {
        $this->view->disable();

        $params = $this->dispatcher->getParams();

        $paramDateFrom = $params[0];
        $paramDateTo = $params[1];
        $paramWorker = $params[2];

        $itemCalculator = new ItemCalculator($this->entityManager);
        $dateTime = new DateTime();
        $dateTime->setDateType(DateTime::TYPE_DATE);

        /** @var OperatingListItemWorksheet $workerAllOperation */
        $workerAllOperation = $this->entityManager->find(OperatingListItemWorksheet::class, [
            OperatingListItemWorksheet::PROPERTY_PRODUCTION_WORKER_ID . ' = :workerId: AND ' .
            OperatingListItemWorksheet::PROPERTY_DATE . ' >= :dateFrom: AND ' .
            OperatingListItemWorksheet::PROPERTY_DATE . ' <= :dateTo: ',
            'bind' => [
                'workerId' => $paramWorker,
                'dateFrom' => $paramDateFrom,
                'dateTo' => $paramDateTo,
            ],
            'order' => OperatingListItemWorksheet::PROPERTY_DATE
        ]);

        /** @var ProductionWorker $worker */
        $worker = $this->entityManager->findOne(ProductionWorker::class, [
            ProductionWorker::PROPERTY_ID . ' = :paramWorker:',
            'bind' => [
                'paramWorker' => $paramWorker
            ]
        ]);

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();

        $html = $this->getDI()->getSimpleView()->render('production-workers/print-worker', [
            'workerAllOperations' => $workerAllOperation,
            'worker' => $worker,
            'paramDateFrom' => $paramDateFrom,
            'paramDateTo' => $paramDateTo,
            'siteUrl' => ltrim($config->application->baseUri, '/')
        ]);

        $content = $this->getDI()->getPdfRenderer()->render($html);
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $worker->getName());

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;

    }

    public function printSectorsAction()
    {
        $this->view->disable();

        $params = $this->dispatcher->getParams();

        $paramDateFrom = $params[0];
        $paramDateTo = $params[1];
        $paramSector = $params[2];

        $query = $this->modelsManager->createBuilder()
            ->columns([
                'ow.date',
                'SUM(ow.qty) as total'
            ])
            ->addFrom(\OperatingListItemWorksheet::class, 'ow')
            ->leftJoin(ProductionWorker::class, 'pw.id = ow.productionWorkerId', 'pw')
            ->betweenWhere('ow.date', $paramDateFrom, $paramDateTo)
            ->andWhere('pw.codeId = :sectorId:', [
                'sectorId' => $paramSector
            ])
            ->groupBy('ow.date');

        /** @var array $sectorAllOperations */
        $sectorAllOperations = $query->getQuery()->execute();

        /** @var Code $sector */
        $sector = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_ID . ' = :id:',
            'bind' => [
                'id' => $paramSector
            ]
        ]);

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();

        $html = $this->getDI()->getSimpleView()->render('production-workers/print-sectors', [
            'sectorAllOperations' => $sectorAllOperations,
            'sector' => $sector,
            'paramDateFrom' => $paramDateFrom,
            'paramDateTo' => $paramDateTo,
            'siteUrl' => ltrim($config->application->baseUri, '/')
        ]);

        $content = $this->getDI()->getPdfRenderer()->render($html);
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $sector->getName());

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;
    }


    public function reportWorkTimeAction()
    {

        $paramDate = $this->request->get('date', 'string', null);
        $paramSectorId = $this->request->get('sectorId', 'string', null);

        $sectors = new Sector\Manager($this->entityManager);
        $info = $sectors->getProductionSectors();

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        if (!$codeType) {
            return $this->returnNotFound();
        }

        //$workerSector = $this->entityManager->find(Prod)

        $codes = $this->entityManager->find(Code::class, [
            Code::PROPERTY_ID . ' IN ({codeQuery:array})',
            'bind' => [
                'codeQuery' => $info
            ],
            'order' => 'name'
        ]);

        $this->view->pick('production-workers/reports/report-work-time');
        $this->view->setVar('codes', $codes);
        $this->view->setVar('paramDate', $paramDate);
        $this->view->setVar('paramSectorId', $paramSectorId);
        $this->view->setVar('date', new \Vemid\Date\DateTime());
    }

    public function reportWorkTimeDataAction()
    {
        //TODO @Marko da napravi paginaciju
        $this->view->disable();

        $paramDate = $this->request->getQuery('date');
        $paramSectorId = $this->request->getQuery('sectorId');
        $date = new DateTime($paramDate);

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');

        /** @var \Vemid\Entity\Repository\ProductionWorkerRepository $productionWorker */
        $productionWorker = $this->entityManager->getRepository(ProductionWorker::class);

        $result = $productionWorker->getReportWorkTimeDataAction($paramSectorId, $date);
        $total = new Time('08:00:00');

        /** @var \Vemid\Entity\Repository\AbsenceWorkRepository $absenceWork */
        $absenceWork = $this->entityManager->getRepository(AbsenceWork::class);
        $workingSheetManager = new WorksheetManager($this->entityManager);

        /** @var \Vemid\Entity\Repository\ProdutionWorkerCheckinRepository $productionWorkerCheckin */
        //$productionWorkerCheckin = $this->entityManager->getRepository(ProductionWorkerCheckin::class);

        $data = [];
        foreach ($result as $row) {
            /** @var ProductionWorker $productionWorker */
            if (!$productionWorker = $this->entityManager->findOne(ProductionWorker::class, $row['workerId'])) {
                continue;
            }

            $date = new DateTime($row['date']);
            $date->setDateType(DateTime::TYPE_DATE);
            $totalTime = new Time($row['totalTime']);
            $workTime = new Time($row['workTime']);
            $typePerDay = $absenceWork->getTypePerDay($date, $productionWorker);

            $status = $typePerDay ?? $workingSheetManager->getWorksheetStatus($date, $productionWorker);
            $outerm = round((($workTime->getTotalMinutes() > 450 ? $workTime->getTotalMinutes()-30 : $workTime->getTotalMinutes()) / 450) * 100, 1);

            /** @var Comment $comments */
            $comments = $this->entityManager->findOne(Comment::class, [
                Comment::PROPERTY_PRODUCTION_WORKER_ID . ' = :productionWorkerId: AND ' .
                Comment::PROPERTY_DATE . ' = :date: AND ' .
                Comment::PROPERTY_TYPE . ' = :type:',
                'bind' => [
                    'productionWorkerId' => $row['workerId'],
                    'date' => $date->getUnixFormat(),
                    'type' => Comment::TYPE_WORKING_TIME
                ],
                'order' => 'id DESC'
            ]);

            $check = ($total->getTotalSeconds() !== $totalTime->getTotalSeconds()) ? 0 : 1;
            $statusCheck = $status ? 0 : 1;

            /** @var ProductionWorkerCheckin $checkinData */
            $checkinData = $productionWorker->getProductionWorkerCheckin(['date = :date:',
                'bind' =>[
                    'date' => $date
                ],
            ])->getFirst();

            $late = 1;
            $hours = 1;

            if($checkinData) {
                $checkIn = (new DateTime($checkinData->getCheckIn()));
                $checkOut = (new DateTime($checkinData->getCheckOut()));

                $dif = $checkIn->diff($checkOut);

                if($dif->h < 8 )
                {
                    $hours = 0;
                }
            }

            if($checkinData) {
                if ((new DateTime($productionWorker->getStartTime()))->getTime() < (new DateTime($checkinData->getCheckIn()))->getTime()) {
                    $late = 0;
                };
            }

            $data[] = [
                $date->getUnixFormat(),
                $row['displayName'],
                (string)$workTime,
                (string)$totalTime,
                number_format($row['money'], 0),
                $outerm . " %",
                $this->translator->t($status),
                $checkinData ? $checkinData->getCheckIn() : (new DateTime('00:00:00'))->getTime(),
                $checkinData ? $checkinData->getCheckOut() : (new DateTime('00:00:00'))->getTime(),
                $row['workerId'],
                $comments ? $comments->getComment() : 0,
                $date->getUnixFormat(),
                $statusCheck,
                $check,
                $late,
                $hours
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => count($data),
            'recordsFiltered' => count($data),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();

    }

    public function overviewAction($id)
    {
        /** @var ProductionWorker $productionWorker */
        $productionWorker = $this->entityManager->findOne(ProductionWorker::class, $id);
        if (!$productionWorker) {
            return $this->returnNotFound();
        }

        /** @var \Vemid\Entity\Repository\ProductionWorkerRepository $productionWorkerSalary */
        $productionWorkerSalary = $this->entityManager->getRepository(ProductionWorker::class);

        $years= $productionWorkerSalary->getYears();
        $months = $productionWorkerSalary->getMonths();
        $chartData = [
            'labels' => $months
        ];

        $datas = $productionWorkerSalary->getSalaries($id);

        $dataSalary = [];
        foreach ($years as $year) {
            foreach ($datas as $data) {
                $dataSalary[$data['month']][$data['year']] = intval($data['salary']);
            }
        }

        $finalSalaryData = [];
        foreach ($months as $month) {
            foreach ($years as $year)
            {
                if(!isset($dataSalary[$month][$year])) {
                    $finalSalaryData[$month][$year] = 0;
                }
                else {
                    $finalSalaryData[$month][$year] = intval($dataSalary[$month][$year]);
                }

            }
        }

            foreach ($years as $year) {
                foreach ($months as $month) {
                    $chartData['data'][$year][array_search($month, $chartData['labels'])] = $finalSalaryData[$month][$year];
                }
            }

        $config = $this->getDI()->getConfig();
        $this->view->setVar('dataSalary',$finalSalaryData);
        $this->view->setVar('workerId',$id);
        $this->view->setVar('months',$months);
        $this->view->setVar('productionWorker',$productionWorker);
        $this->view->setVar('years',$years);
        $this->view->setVar('siteUrl', ltrim($config->application->baseUri, '/'));
        $this->view->setVar('chartData', json_encode($chartData));

    }

    public function getWorkerAbsenceDataAction()
    {

        $this->view->disable();

        $workerId = $this->request->getQuery('workerId', 'string', 0);

        /** @var \ProductionWorker $productionWorker */
        $productionWorker = $this->entityManager->findOne(ProductionWorker::class, $workerId);

        if (!$productionWorker) {
            return $this->returnNotFound();
        }

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');

        $absences = $this->entityManager->find(AbsenceWork::class, [
            AbsenceWork::PROPERTY_PRODUCTION_WORKER_ID . ' = :workerId: ',
            'bind' => [
                'workerId' => $productionWorker->getId()
            ],
            'order' => 'startDate DESC, productionWorkerId ASC'
        ]);

        $data = [];

        /** @var AbsenceWork $absence */
        foreach ($absences as $absence) {
            $data[] = [
              $absence->getProductionWorker()->getDisplayName(),
              $absence->getStartDate()->getShortSerbianFormat(),
              $absence->getEndDate()->getShortSerbianFormat(),
              $absence->getType(),
              $absence->getOnSalary() ? "Da" : "Ne",
              $absence->getId()
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $absences->count(),
            'recordsFiltered' => $absences->count(),
            'data' => $data
        ];


        $this->response->setContent(json_encode($responseData,JSON_UNESCAPED_UNICODE));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();

    }


    /**
     * @return string
     */
    public function getEntityName()
    {
        return ProductionWorker::class;
    }
}