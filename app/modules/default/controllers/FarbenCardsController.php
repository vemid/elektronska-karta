<?php
use Vemid\Controller\CrudController;
use \Vemid\Service\Product\Hydrator;
use Vemid\Entity\Repository\ProductSizeRepository;
use \Vemid\Messenger\Message;

/**
 * Class FarbenCardsController
 *
 * @package Default\Controllers
 */
class FarbenCardsController extends CrudController
{

    public function listAction()
    {
        $this->tag::appendTitle($this->translator->t("Farben Karte"));

        $farbenCards = $this->entityManager->find(FarbenCard::class, [
            'order' => 'id DESC'
        ]);

        $this->view->setVar('farbenCards', $farbenCards);
    }

    public function getCreateFormAction()
    {
        $form = $this->getForm();
        $form->get(FarbenCard::PROPERTY_PRODUCT_ID)->setName('farbenCardProductId');
        $form->add(
            new \Phalcon\Forms\Element\Hidden('hiddenProductId')
        );

        $renderer = new \Vemid\Form\Renderer\Json();

        $data = $renderer->render($form);


        return $data;
    }

    public function createAction()
    {
        if ($this->request->isPost()) {
            $form = $this->getForm();
            $post = $this->request->getPost();
            $post['productId'] = $post['hiddenProductId'];
            if ($form->isValid($post)) {
                $post['qtyTotal'] == null ? $post['qtyTotal'] = 0 : $post['qtyTotal'];
                $entity = $form->getEntity();
                $form->synchronizeEntityData();
                $hiddenProduct = $post['hiddenProductId'];
                $product = $this->entityManager->findOne(FarbenCard::class,[
                    FarbenCard::PROPERTY_PRODUCT_ID . ' = :productId:',
                    'bind' => [
                        'productId' => $hiddenProduct
                    ]
                ]);
                if(!$product) {
                    if ($this->saveEntity($entity)) {
                        $this->dispatcher->setParam('url', '/farben-cards/overview/' . $entity->getEntityId());
                    } else {
                        $this->addFlashMessagesFromEntity($entity);
                    }
                }
                else {
                    $this->addFlashMessage("Ne moze se snimiti jer vec postoji farben karta !",null,Message::DANGER);
                }
            } else {
                $this->addFlashMessagesFromForm($form);
            }
        }
    }

    public function overviewAction($id)
    {
        $this->tag::appendTitle($this->translator->t("Pregled farben karte"));
        /** @var FarbenCard $farbenCard */
        if (!$farbenCard = $this->entityManager->findOne(FarbenCard::class, $id)) {
            return $this->returnNotFound();
        }
        $hydrator = new Hydrator($farbenCard->getProduct(), $this->entityManager);

        $typeData = array("Total Kolicina","Nalog","Raspis","Dodatni Raspis" );

        $dataTotal = [];
        foreach ($farbenCard->getProduct()->getProductSizes() as $productSize) {
            /** @var OrderTotal $orderTotal */
            $orderTotal = $productSize->getOrderTotal();

            if($orderTotal) {
                $dataTotal["Total Kolicina"][$productSize->getCodeId()] += $orderTotal->getFinalQty() ? $orderTotal->getFinalQty() : 0;
                $dataTotal["orderTotalId"][$productSize->getCodeId()] += $orderTotal->getId();
            }
            else {
                $dataTotal["Total Kolicina"][$productSize->getCodeId()] += 0;
            }

        }

        $productSizeRepository = new ProductSizeRepository($this->getDI()->getModelsManager());

        $farbenCardItems = $this->entityManager->find(FarbenCardItem::class, [
            FarbenCardItem::PROPERTY_FARBEN_CARD_ID . ' = :farbenCardId:',
            'bind' => [
                'farbenCardId' => $farbenCard->getEntityId()
            ]
        ]);

        $washItems = $this->entityManager->find(WashingCareLabel::class);

        $washData = "";
        if($farbenCard->getWashLabel() ? $washData=$farbenCard->getWashLabel() : $washData="a!Ax+" )

        $careLabel = new Vemid\Service\MaterialFactory\MaterialFactoryCareLabel($this->entityManager);

        $this->view->setVar('farbenCardItems', $farbenCardItems);
        $this->view->setVar('careLabel', $careLabel);
        $this->view->setVar('washItems', $washItems);
        $this->view->setVar('farbenCard', $farbenCard);
        $this->view->setVar('productSizeRepository', $productSizeRepository);
        $this->view->setVar('hydrator', $hydrator);
        $this->view->setVar('typeData', $typeData);
        $this->view->setVar('washData', str_split($washData));
        $this->view->setVar('dataTotal', $dataTotal);
    }

    public function addFarbenCardAction($id)
    {
        /** @var FarbenCard $farbenCard */
        if (!$farbenCard = $this->entityManager->findOne(FarbenCard::class, $id)) {
            return $this->returnNotFound();
        }

        $postData = $this->request->getPost();
        foreach ($postData['create'] as $data) {
            if (empty(array_filter($data))) {
                continue;
            }

            $farbenCardItem = new FarbenCardItem();
            $farbenCardItem->setFarbenCardId($farbenCard->getId());
            $farbenCardItem->setName($data['name']);


            if (!$this->entityManager->save($farbenCardItem)) {
                $this->flashSession->error($farbenCardItem->getMessages()[0]->getMessage());

                return $this->response->redirect('farben-cards/overview/' . $id);
            }
        }

        $this->flashSession->success('Uspešno ste dodali farben kartu!');

        return $this->response->redirect('farben-cards/overview/' . $id);
    }

    public function updateAction($id)
    {
        /** @var FarbenCard $farbenCard */
        if (!$farbenCard = $this->entityManager->findOne(FarbenCard::class, $id)) {
            return $this->returnNotFound();
        }

        /** @var WashingCareLabel $washCareLabels */
        $washCareLabels = $this->entityManager->find(WashingCareLabel::class);
        $washedKeys= [];
        /** @var WashingCareLabel $washCareLabel */
        foreach ($washCareLabels as $washCareLabel) {
            $washedKeys[$washCareLabel->getId()]=$washCareLabel->getPrintSign();
        }

        $stringWash = "";
        $postData = $this->request->getPost();

        foreach ($postData['washed'] as $washData) {
            if (array_key_exists($washData,$washedKeys)) {
            $stringWash .= $washedKeys[$washData];
            }
        }

        foreach ($postData['create'] as $data) {
            if (empty(array_filter($data))) {
                continue;
            }

            $productSizes = $this->entityManager->find(ProductSize::class,[
                'productId = :productId:',
                'bind' => [
                    'productId' => $farbenCard->getProduct()->getId()
                ]
            ]);

            //$farbenCard->setWashLabel($postData['create']['farbenCardWashLabel']);
            $farbenCard->setWashLabel($stringWash);
            $farbenCard->setPaperLabel($postData['create']['farbenCardPaperLabel']);
            $this->entityManager->save($farbenCard);

            $product = $farbenCard->getProduct();
            $product->setComposition($postData['create']['farbenCardPaperLabel']);
            $product->setWashLabel($stringWash);
            $product->setIsSynced(false);
            $this->entityManager->save($product);

            $productCalculations = $product->getProductCalculations();

            /** @var ProductCalculation $productCalculation */
            foreach ($productCalculations as $productCalculation) {
                $productCalculation->setComposition($postData['create']['farbenCardPaperLabel']);
                $this->entityManager->save($productCalculation);
            }


            /** @var ProductSize $productSize */
            foreach ($productSizes as $productSize) {

                $farbenWarrant = $postData['create']['Nalog'][$productSize->getId()]['farbenCardWarrant'];
                $farbenFiqQty = $postData['create']['Raspis'][$productSize->getId()]['farbenFiqQty'];
                $farbenRestQty = $postData['create']['Dodatni Raspis'][$productSize->getId()]['farbenRestQty'];
                if($farbenWarrant) {
                    $productSize->setFarbenCardWarrant($farbenWarrant);
                }
                if($farbenFiqQty) {
                    $productSize->setFarbenFigQty($farbenFiqQty);
                }
                if($farbenRestQty) {
                    $productSize->setFarbenRestQty($farbenRestQty);
                }


                if (!$this->entityManager->save($productSize)) {
                    $this->flashSession->error($productSize->getMessages()[0]->getMessage());

                    return $this->response->redirect('farben-cards/overview/' . $id);
                }
            }
        }

        $this->flashSession->success('Uspešno ste dodali farben kartu!');

        return $this->response->redirect('farben-cards/overview/' . $id);
    }

    public function addItemsAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        parse_str($this->request->getRawBody(), $postData);

        /** @var FarbenCard $farbenCard */
        if (!$farbenCard = $this->entityManager->findOne(FarbenCard::class, $postData['farbenCardId'])) {
            return $this->returnNotFound();
        }


        if (!empty($postData['create'])) {
            foreach ($postData['create'] as $rows) {
                $farbenCardItem = new FarbenCardItem();
                $farbenCardItem->setFarbenCard($farbenCard);
                foreach ($rows as $property => $value) {
                    $farbenCardItem->setProperty($property, str_replace(',','.',$value));
                }

                if (!$this->entityManager->save($farbenCardItem)) {
                    $this->addFlashMessagesFromEntity($farbenCardItem);
                }
            }
        }

        if (!empty($postData['edit'])) {

            foreach ($postData['edit'] as $id => $properties) {
                if (!$farbenCardItem = $this->entityManager->findOne(FarbenCardItem::class, $id)) {
                    continue;
                }

                foreach ($properties as $property => $value) {
                    $farbenCardItem->setProperty($property, str_replace(',','.',$value));
                }

                if (!$this->entityManager->save($farbenCardItem)) {
                    $this->addFlashMessagesFromEntity($farbenCardItem);
                }
            }
        }

        $this->addFlashMessage(
            'Uspešno sačuvane cene!',
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function printAction($id)
    {
        $this->view->disable();

        $params = $this->dispatcher->getParams();

        /** @var FarbenCard $farbenCard */
        if (!$farbenCard = $this->entityManager->findOne(FarbenCard::class, $params[0])) {
            return $this->returnNotFound();
        }
        $hydrator = new Hydrator($farbenCard->getProduct(), $this->entityManager);

        $typeData = array("Total Kolicina",/*"Nalog",*/"Raspis","Dodatni Raspis","Dopuna" );

        $dataTotal = [];
        foreach ($farbenCard->getProduct()->getProductSizes() as $productSize) {
            /** @var OrderTotal $orderTotal */
            $orderTotal = $productSize->getOrderTotal();

            $dataTotal["Total Kolicina"][$productSize->getCodeId()] += $orderTotal->getFinalQty();
        }

        $productSizeRepository = new ProductSizeRepository($this->getDI()->getModelsManager());

        $farbenCardItems = $this->entityManager->find(FarbenCardItem::class, [
            FarbenCardItem::PROPERTY_FARBEN_CARD_ID . ' = :farbenCardId:',
            'bind' => [
                'farbenCardId' => $farbenCard->getEntityId()
            ]
        ]);

        $data = [];
        /** @var FarbenCardItem $farbenCardItem */
        foreach ($farbenCardItems as $farbenCardItem) {
            $data[] = [
                'item' => ($farbenCardItem->getMaterialFactory() ? $farbenCardItem->getMaterialFactory()->getDisplayName()
                        .'-'.$farbenCardItem->getMaterialFactory()->getSupplierName() : $farbenCardItem->getName())
                        ."-".$farbenCardItem->getDescription()
            ];
        }
        $halved = array_chunk($data, ceil(count($data)/2));

        $washData = "";
        if($farbenCard->getWashLabel() ? $washData=$farbenCard->getWashLabel() : $washData="a!Ax+" )

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();

        $washItems = $this->entityManager->find(WashingCareLabel::class);

        $html = $this->getDI()->getSimpleView()->render('farben-cards/print', [
            'farbenCardItems' => $farbenCardItems,
            'washItems' => $washItems,
            'farbenCard' => $farbenCard,
            'productSizeRepository' => $productSizeRepository,
            'hydrator' => $hydrator,
            'typeData' => $typeData,
            'dataTotal' => $dataTotal,
            'washData' => $washData,
            'halved' => $halved,
            'siteUrl' => ltrim($config->application->baseUri, '/')
        ]);

        $renderer = $this->getDI()->getPdfRenderer();
        $renderer->setPageSize('210mm', '297mm');
        $renderer->setMargins(0,0,0,0);

        $content = $renderer->render($html, 'landscape');
        $name = str_replace(['*', ':', '/', '\\', '?', '[', ' ', ']', ','], '-', $farbenCard->getProduct()->getName());

        header('Content-type: application/pdf');
        header('Content-disposition: attachment; filename=' . $name . '.pdf');

        echo $content;
        exit;

    }

    public function getEntityName()
    {
        return \FarbenCard::class;
    }
}