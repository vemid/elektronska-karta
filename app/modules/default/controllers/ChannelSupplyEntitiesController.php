<?php

use \Vemid\Controller\CrudController;
use Vemid\Form\Renderer\Json as Renderer;
use \Vemid\Entity\Type;
use \Vemid\Messenger\Message;

/**
 * Class ChannelSupplyEntitiesController
 */
class ChannelSupplyEntitiesController extends CrudController
{

    public function listAction($id)
    {
        $this->tag::appendTitle($this->translator->t("Pregled Kanala Snabdevanja"));
        $this->view->setVar('channelSupplies', $this->entityManager->find(ChannelSupplyEntity::class, [
                ChannelSupplyEntity::PROPERTY_CHANNEL_SUPPLY_ID . ' = :channelSupplyId:',
                'bind' => [
                    'channelSupplyId' => $id
                ]
            ]
        ));

        $this->view->pick('channel-supplies/entities/list');
    }

    /**
     * {@inheritdoc}
     */
    public function getCreateFormAction()
    {
        if (!isset($this->router->getParams()[1])) {
            $this->returnNotFound();
        }

        if (!$channelSupply = $this->entityManager->findOne(ChannelSupply::class, $this->router->getParams()[1])) {
            $this->returnNotFound();
        }

        $courierHandler = $this->getDI()->getCourierHandler();

        return (new Renderer())->render(
            new ChannelSupplyEntityForm(
                new ChannelSupplyEntity(),
                ['channelSupply' => $channelSupply, 'courierHandler' => $courierHandler]
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdateFormAction($id)
    {

        if (!$channelSupplyEntity= $this->entityManager->findOne(ChannelSupplyEntity::class,$id)) {
            $this->returnNotFound();
        }

        if (!$channelSupply = $this->entityManager->findOne(ChannelSupply::class, $channelSupplyEntity->getChannelSupplyId())) {
            $this->returnNotFound();
        }

        $courierHandler = $this->getDI()->getCourierHandler();

        return (new Renderer())->render(
            new ChannelSupplyEntityEditForm(
                new ChannelSupplyEntity(),
                [
                    'channelSupply' => $channelSupply,
                    'courierHandler' => $courierHandler,
                    'channelSupplyEntity' => $channelSupplyEntity
                ]
            )
        );
    }

    public function createAction()
    {
        if (!isset($this->router->getParams()[1])) {
            $this->returnNotFound();
        }

        /** @var ChannelSupply $channelSupply */
        if (!$channelSupply = $this->entityManager->findOne(ChannelSupply::class, $this->router->getParams()[1])) {
            $this->returnNotFound();
        }

        $id = null;
        if ($channelSupply->getType() === ChannelSupply::VP) {
            $client = $this->entityManager->findOne(Client::class, $this->request->getPost('entity'));
            $id = $client ? $client->getEntityId() : null;
        }

        if ($channelSupply->getType() === ChannelSupply::MP) {
            $code = $this->entityManager->findOne(Code::class, $this->request->getPost('entity'));
            $id = $code ? $code->getEntityId() : null;
        }

        if (!$id) {
            $this->addFlashMessage(
                $this->translator->t('Nije pronadjen krajnji korisnik'),
                null,
                Message::DANGER
            );

            return;
        }

        $postalManager = $this->getDI()->getCourierHandler();
        $towns = $postalManager->getTowns();
        $streets = $postalManager->getStreets();

        $postalAddressTownId = $this->request->getPost(ChannelSupplyEntity::PROPERTY_POSTAL_SERVICE_TOWN_ID);
        $postalStreetId = $this->request->getPost(ChannelSupplyEntity::PROPERTY_STREET_ID);
        $key = \array_search($postalAddressTownId, array_column($towns, 'Id'), false);
        $keyStreet = \array_search($postalStreetId, array_column($streets, 'Id'), false);

        if (isset($towns[$key])) {
            $_POST[ChannelSupplyEntity::PROPERTY_TOWN] = $towns[$key]['DName'];
        }

        if (isset($streets[$key])) {
            $_POST[ChannelSupplyEntity::PROPERTY_STREET] = $streets[$keyStreet]['Name'];
        }

        $_POST[ChannelSupplyEntity::PROPERTY_ENTITY_TYPE_ID] = $channelSupply->getType() === ChannelSupply::VP ? Type::CLIENT : Type::CODE;
        $_POST[ChannelSupplyEntity::PROPERTY_ENTITY_ID] = $id;

        parent::createAction();
    }

    public function updateAction($id)
    {
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var $channelSupplyEntity ChannelSupplyEntity */
        if (!$channelSupplyEntity = $this->entityManager->findOne(ChannelSupplyEntity::class,$id)) {
            return $this->returnNotFound();
        }

        $postalManager = $this->getDI()->getCourierHandler();
        $towns = $postalManager->getTowns();
        $streets = $postalManager->getStreets();

        $postalAddressTownId = $this->request->getPost(ChannelSupplyEntity::PROPERTY_POSTAL_SERVICE_TOWN_ID);
        $postalStreetId = $this->request->getPost(ChannelSupplyEntity::PROPERTY_STREET_ID);
        $key = \array_search($postalAddressTownId, array_column($towns, 'Id'), false);
        $keyStreet = \array_search($postalStreetId, array_column($streets, 'Id'), false);

        if (isset($towns[$key])) {
            $_POST[ChannelSupplyEntity::PROPERTY_TOWN] = $towns[$key]['DName'];
        }
        if (isset($streets[$key])) {
            $_POST[ChannelSupplyEntity::PROPERTY_STREET] = $streets[$keyStreet]['Name'];
        }

        $_POST[ChannelSupplyEntity::PROPERTY_ENTITY_TYPE_ID] = $this->request->getPost()["entityTypeId"];
        $_POST[ChannelSupplyEntity::PROPERTY_ENTITY_ID] = $this->request->getPost()["entity"] ? $this->request->getPost()["entity"] : $channelSupplyEntity->getEntityId();

        parent::updateAction($id);

    }
    public function getEntityName()
    {
        return \ChannelSupplyEntity::class;
    }
}