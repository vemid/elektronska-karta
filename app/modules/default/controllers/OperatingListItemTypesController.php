<?php

use Vemid\Controller\CrudController;

class OperatingListItemTypesController extends CrudController
{
    public function listAction()
    {
        $this->tag::appendTitle($this->translator->t("Tipovi operacija"));

        $operatingListItemTypes = $this->entityManager->find(OperatingListItemType::class, [
            'order' => 'name ASC'
        ]);

        $this->view->setVar('operatingListItemTypes', $operatingListItemTypes);

    }

    public function getCreateFormAction()
    {
        $data = parent::getCreateFormAction();

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        if (!$codeType) {
            return $this->returnNotFound();
        }

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $sectorsData = [];
        foreach ($sectors as $sector) {
            $sectorsData[$sector->getId()] = $sector->getName();
        }

        $data[OperatingListItemType::PROPERTY_CODE_ID]['options'] = $sectorsData;

        return $data;
    }

    public function getUpdateFormAction($id)
    {
        $data = parent::getUpdateFormAction($id);
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        if (!$codeType) {
            return $this->returnNotFound();
        }

        /** @var Code[] $sectors */
        $sectors = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        $sectorsData = [];
        foreach ($sectors as $sector) {
            $sectorsData[$sector->getId()] = $sector->getName();
        }

        $data[OperatingListItemType::PROPERTY_CODE_ID]['options'] = $sectorsData;

        return $data;
    }

    public function getListDataAction() {
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');

        $query = $this->modelsManager->createBuilder();

        $query->addFrom(OperatingListItemType::class, 'olit')
            ->innerJoin(Code::class, 'olit.codeId = c.id', 'c');

        if (is_array($search) && $search['value']) {
            $query->andWhere('c.name LIKE :search: OR olit.name LIKE :search:', [
                'search' => '%' . $search['value'] . '%'
            ]);
        }

        $query->orderBy('olit.name asc');

        $queryForCount = clone $query;

        /** @var OperatingListItemType[] $allOperatingListItemTypes */
        $allOperatingListItemTypes = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);

        /** @var OperatingListItemType[] $operatingListItemTypes */
        $operatingListItemTypes = $query->getQuery()->execute();

        $data = [];
        foreach ($operatingListItemTypes as $operatingListItemType) {

            $data[] = [
                (string)$operatingListItemType->getCode()->getName(),
                (string)$operatingListItemType->getName(),
                (string)$operatingListItemType->getMachine() ? $operatingListItemType->getMachine() : $operatingListItemType->getProductionMachine()->getName(),
                $operatingListItemType->getId()
            ];
        }

        $count = $allOperatingListItemTypes->count();
        $responseData = [
            'draw' => $page,
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData,JSON_UNESCAPED_UNICODE));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();

    }

    public function list2Action()
    {

    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return OperatingListItemType::class;
    }
}