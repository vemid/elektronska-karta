<?php

use Vemid\Controller\CrudController;
use Vemid\Entity\EntityInterface;
use Vemid\Form\Renderer\Json as Renderer;

/**
 * Class ClassificationsController
 *
 * @package Default\Controllers
 */
class ClassificationsController extends CrudController
{

    public function listAction()
    {
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');
        $classificationTypeCodeId = $this->request->getQuery('classificationType', 'int', 0);

        $query = $this->modelsManager->createBuilder();
        $query->addFrom(Classification::class);

        if (is_array($search) && $search['value']) {
            $query->andWhere('code = :search: OR name = :search:', [
                'search' => $search['value']
            ]);
        }

        if ($classificationTypeCodeId) {
            $query->andWhere('classificationTypeCodeId = :classificationTypeCodeId:', [
                'classificationTypeCodeId' => $classificationTypeCodeId
            ]);
        }

        $queryForCount = clone $query;
        $allClassifications = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);

        /** @var Classification[] $classifications */
        $classifications = $query->getQuery()->execute();

        $data = [];
        foreach ($classifications as $classification) {
            $data[] = [
                $classification->getName(),
                $classification->getClassificationType() ? $classification->getClassificationType()->getCode() : '',
                $classification->getCode(),
                $classification->getParentClassification() ? $classification->getParentClassification()->getCode() : ''
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $allClassifications->count(),
            'recordsFiltered' => $allClassifications->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    /**
     * {@inheritdoc}
     */
    public function getCreateFormAction()
    {
        return (new Renderer())->render(new SeasonClassificationForm());
    }

    /**
     * {@inheritdoc}
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var Code $codeSeason */
        $codeSeason = $this->entityManager->findOne(Code::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => '05'
            ]
        ]);

        /** @var Code $codeProductType */
        $codeProductType = $this->entityManager->findOne(Code::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('productTypeCodeId')
            ]
        ]);

        if (!$codeSeason || !$codeProductType) {
            return $this->returnNotFound();
        }

        $form = new SeasonClassificationForm();

        if ($form->isValid($this->request->getPost())) {
            $classification = new Classification();
            $classification->setClassificationType($codeSeason);
            $classification->setClassificationCategory($codeProductType);
            $classification->setCode($this->request->getPost('code'));
            $classification->setName($this->request->getPost('name'));

            $this->saveEntity($classification);
        } else {
            $this->addFlashMessagesFromForm($form);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function deleteAction($id)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityName()
    {
        return \Classification::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getForm(EntityInterface $entity = null, array $exclude = [])
    {
        return parent::getForm($entity, $exclude);
    }

    public function getParentClassificationAction($id)
    {
        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        if (!$codeType) {
            $this->returnNotFound();
        }

        /** @var Code $classificationType */
        $classificationType = $this->entityManager->findOne(Code::class, [
            'id = :id: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'id' => $id,
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if (!$classificationType) {
            return $this->returnNotFound();
        }

        /** @var Classification[] $classificationTypes */
        $classificationTypes = $this->entityManager->find(Classification::class, [
            'classificationTypeCodeId = :classificationTypeCodeId:',
            'bind' => [
                'classificationTypeCodeId' => $classificationType->getId(),
            ]
        ]);

        $data = [];
        foreach ($classificationTypes as $classificationType) {
            $data[$classificationType->getId()] = $classificationType->getDisplayName();
        }

        return $data;
    }

    public function listByProductTypeAction()
    {
        $this->view->disable();
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var Code $productTypeCode */
        $productTypeCode = $this->entityManager->findOne(Code::class, [
            'code = :code:',
            'bind' => [
                'code' => $this->request->getPost('productTypeCodeId')
            ]
        ]);

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        if (!$productTypeCode || !$codeType) {
            $this->returnNotFound();
        }

        /** @var Code $seasonCode */
        $seasonCode = $this->entityManager->findOne(Code::class, [
            'code = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '05',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if (!$seasonCode) {
            $this->returnNotFound();
        }

        $classifications = $this->entityManager->find(Classification::class, [
            Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
            Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeCategoryId' => $productTypeCode->getId(),
                'codeTypeId' => $seasonCode->getId()
            ]
        ]);

        $data = [];
        foreach ($classifications as $classification) {
            $data[$classification->getCode()] = $classification->getName();
        }

        $this->response->setContent(json_encode($data));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function listProductProgramByProductTypeAction()
    {
        $this->view->disable();
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        if (!$codeType) {
            $this->returnNotFound();
        }

        /** @var Code $productTypeCode */
        $productTypeCode = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('productTypeCodeId')
            ]
        ]);

        /** @var Code $seasonCode */
        $seasonCode = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '00',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if (!$productTypeCode || !$seasonCode) {
            $this->returnNotFound();
        }

        $classifications = $this->entityManager->find(Classification::class, [
            Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
            Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId: AND ' .
            Classification::PROPERTY_PARENT_CLASSIFICATION_ID . ' IS NULL',
            'bind' => [
                'codeCategoryId' => $productTypeCode->getId(),
                'codeTypeId' => $seasonCode->getId()
            ]
        ]);

        $data = [];
        foreach ($classifications as $classification) {
            $data[$classification->getCode()] = $classification->getName();
        }

        $this->response->setContent(json_encode($data));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function listByChildrenClassificationsAction()
    {
        $this->view->disable();
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var \Classification $productProgram */
        $productProgram = $this->entityManager->findOne(Classification::class, [
            'code = :code:',
            'bind' => [
                'code' => $this->request->getPost('productProgram')
            ]
        ]);

        $data = [];
        if ($productProgram) {
            /** @var \Classification[] $classifications */
            $classifications = $this->entityManager->find(Classification::class, [
                \Classification::PROPERTY_PARENT_CLASSIFICATION_ID . ' = :parentClassificationId:',
                'bind' => [
                    'parentClassificationId' => $productProgram->getId()
                ]
            ]);

            if ($classifications->count()) {
                foreach ($classifications as $classification) {
                    $data[$classification->getCode()] = $classification->getName();
                }
            } else {
                $data[$productProgram->getCode()] = $productProgram->getName();
            }
        }

        $this->response->setContent(json_encode($data));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function listNonMainByProductTypeAction()
    {
        $this->view->disable();
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var Code $productTypeCode */
        $productTypeCode = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('productTypeCodeId')
            ]
        ]);

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        if (!$productTypeCode || !$codeType) {
            $this->returnNotFound();
        }

        $data = [
            'brand' =>  [],
            'age' =>  [],
            'material' =>  [],
            'collection' =>  [],
            'oldCollection' =>  [],
            'doubleSeason' =>  [],
        ];

        /** @var Code $brandCode */
        $brandCode = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '01',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if ($brandCode) {
            $brandClassifications = $this->entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeCategoryId' => $productTypeCode->getId(),
                    'codeTypeId' => $brandCode->getId()
                ]
            ]);

            foreach ($brandClassifications as $classification) {
                $data['brand'][$classification->getCode()] = $classification->getName();
            }
        }

        /** @var Code $ageCode */
        $ageCode = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '02',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if ($ageCode) {
            $ageClassifications = $this->entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeCategoryId' => $productTypeCode->getId(),
                    'codeTypeId' => $ageCode->getId()
                ]
            ]);

            foreach ($ageClassifications as $classification) {
                $data['age'][$classification->getCode()] = $classification->getName();
            }
        }

        /** @var Code $materialCode */
        $materialCode = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '03',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if ($materialCode) {
            $materialClassifications = $this->entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeCategoryId' => $productTypeCode->getId(),
                    'codeTypeId' => $materialCode->getId()
                ]
            ]);

            foreach ($materialClassifications as $classification) {
                $data['material'][$classification->getCode()] = $classification->getName();
            }
        }

        /** @var Code $collectionCode */
        $collectionCode = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '12',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if ($collectionCode) {
            $collectionClassifications = $this->entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeCategoryId' => $productTypeCode->getId(),
                    'codeTypeId' => $collectionCode->getId()
                ]
            ]);

            foreach ($collectionClassifications as $classification) {
                $data['collection'][$classification->getCode()] = $classification->getName();
            }
        }

        /** @var Code $oldCollectionCode */
        $oldCollectionCode = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '07',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if ($oldCollectionCode) {
            $oldCollectionClassifications = $this->entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeCategoryId' => $productTypeCode->getId(),
                    'codeTypeId' => $oldCollectionCode->getId()
                ]
            ]);

            foreach ($oldCollectionClassifications as $classification) {
                $data['oldCollection'][$classification->getCode()] = $classification->getName();
            }
        }

        /** @var Code $doubleSeasonCode */
        $doubleSeasonCode = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND codeTypeId = :codeTypeId:',
            'bind' => [
                'code' => '13',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        if ($doubleSeasonCode) {
            $doubleSeasonClassifications = $this->entityManager->find(Classification::class, [
                Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :codeCategoryId: AND ' .
                Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeTypeId:',
                'bind' => [
                    'codeCategoryId' => $productTypeCode->getId(),
                    'codeTypeId' => $doubleSeasonCode->getId()
                ]
            ]);

            foreach ($doubleSeasonClassifications as $classification) {
                $data['doubleSeason'][$classification->getCode()] = $classification->getName();
            }
        }

        $this->response->setContent(json_encode($data));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function findPrioritiesAction()
    {
        $this->view->disable();
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        $data = [];
        /** @var \Classification $oldClassification */
        $oldClassification = $this->entityManager->findOne(Classification::class, [
            'code = :code:',
            'bind' => [
                'code' => $this->request->getPost('code')
            ]
        ]);

        if ($oldClassification) {
            foreach ($oldClassification->getPriorityClassifications() as $priorityClassification) {
                if (!$priority = $priorityClassification->getPriority()) {
                    continue;
                }

                $data[$priority->getId()] = $priority->getName()." - ".$priority->getPrioritySerbianDate();
            }
        }

        $this->response->setContent(json_encode($data));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }
}
