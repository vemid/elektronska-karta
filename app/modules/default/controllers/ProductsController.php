<?php

use \Phalcon\Forms\Element\Hidden;
use Picqer\Barcode\BarcodeGeneratorHTML;
use \Vemid\Messenger\Message;
use Vemid\Messenger\Message as MessengerMessage;
use \Vemid\Service\Product\Hydrator;
use \Vemid\Service\Product\TimeLine;
use \Vemid\Services\Assets\DefaultAssets;
use \Vemid\Form\Renderer\Json as Renderer;
use Google\Cloud\Storage\StorageClient;

/**
 * Class ProductsController
 */
class ProductsController extends \Vemid\Controller\CrudController
{
    public function initialize()
    {
        $this->assets->collection(DefaultAssets::ASSETS_FOOT_JS)
            ->addJs(APP_PATH . 'assets/js/steps.min.js')
            ->addJs(APP_PATH . 'assets/js/drop-zone.js')
            ->addJs(APP_PATH . 'assets/js/validate.min.js');

        $this->assets->collection(DefaultAssets::ASSETS_HEAD_CSS)
            ->addCss(APP_PATH . 'assets/css/drop-zone.css')
            ->addCss(APP_PATH . 'assets/css/steps.css');

        parent::initialize();
    }

    public function listAction()
    {
        $this->tag::appendTitle($this->translator->t("Pregled sifara"));

        /** @var \CodeType $codeType */
        $codeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        if (!$codeType) {
            return $this->returnNotFound();
        }

        $productCodeIdProduct = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND code = :code:',
            'bind' => [
                'codeTypeId' => $codeType->getId(),
                'code' => 3
            ]
        ]);

        $productCodeIdMerchandise = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND code = :code:',
            'bind' => [
                'codeTypeId' => $codeType->getId(),
                'code' => 5
            ]
        ]);



        $this->view->setVar('productCodeIdProduct', $productCodeIdProduct);
        $this->view->setVar('productCodeIdMerchandise', $productCodeIdMerchandise);
    }

    public function getListDataAction()
    {
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');


        $productCodeId = $this->request->getQuery('productCodeId', 'int', 0);

        $query = $this->modelsManager->createBuilder();
        $query->addFrom(Product::class);

        if (is_array($search) && $search['value']) {
            $query->andWhere('code LIKE :search: OR name LIKE :search:', [
                'search' =>  '%' . trim($search['value']) .'%'
            ]);
        }
        if ($productCodeId) {
            $query->andWhere('productCodeTypeId = :productCodeTypeId:', [
                'productCodeTypeId' => $productCodeId
            ]);
        }
        $query->orderBy('id desc');

        $queryForCount = clone $query;
        $allClassifications = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);


        /** @var Product[] $products */
        $products = $query->getQuery()->execute();

        $entityManager = $this->getDI()->getEntityManager();
        $data = [];
        foreach ($products as $product) {
            $hydrator = new \Vemid\Service\Product\Hydrator($product, $entityManager);
            $nameSegments = explode(' ', $hydrator->getColor() ? $hydrator->getColor()->getName() : []);
            $data[] = [
                $product->getCode(),
                $product->getName(),
                $hydrator->getAge() ? $hydrator->getAge()->getName() : '',
                $hydrator->getOldCollection() ? $hydrator->getOldCollection()->getName() : '',
                count($nameSegments) ? array_pop($nameSegments) : '',
                $product->getId(),
                count($product->getElectronCards(['active = 1']))==1 ? (($product->getElectronCards(['active = 1']))[0])->getId() : '',
            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $allClassifications->count(),
            'recordsFiltered' => $allClassifications->count(),
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function overviewAction($id)
    {
        /** @var \Product $product */
        $product = $this->entityManager->findOne(Product::class, $id);
        if (!$product) {
            return $this->returnNotFound();
        }

        /** @var \ElectronCards $productId */
        $electronCards = $this->entityManager->find(ElectronCard::class, [
            ElectronCard::PROPERTY_PRODUCT_ID . ' = :productId:',
            'bind' => [
                'productId' => $id
            ]
        ]);

        $pricelists = $this->getDI()->getEntityManager()->find(\Pricelist::class);

        $productPrices = $this->getDI()->getProductPrices();

        $pricelistData = [];
        /** @var Pricelist $pricelist */
        foreach ($pricelists as $pricelist) {
            $pricelistData[$pricelist->getName()]['NABAVNA'] = $productPrices->getPurchasePrice($pricelist,$product);
            $pricelistData[$pricelist->getName()]['VP'] = $productPrices->getWholesalePrice($pricelist,$product);
            $pricelistData[$pricelist->getName()]['MP'] = $productPrices->getRetailPrice($pricelist,$product);
        }


        $hydrator = new Hydrator($product, $this->entityManager);
        $timeline = new TimeLine($product);

        $sync = new \Vemid\Entity\Repository\ProductRepository($this->modelsManager);
        $syncClassification = $sync->getSyncClassificationInformation($product);
        $syncAttributes = $sync->getSyncAttributesInformation($product);
        $syncDatas = array_merge($syncClassification,$syncAttributes);

        $this->view->setVar('hydrator', $hydrator);
        $this->view->setVar('product', $product);
        $this->view->setVar('syncDatas', $syncDatas);
        $this->view->setVar('attributes', $syncAttributes);
//        $this->view->setVar('obj', $obj['posts']);
        $this->view->setVar('timeline', $timeline->getAll());
        $this->view->setVar('electronCards', $electronCards);
        $this->tag::appendTitle($this->translator->t("Proizvod:").' ' .$product->getName());

        $this->view->setVar('pricelistDatas',$pricelistData);

    }

    /**
     * {@inheritdoc}
     */
    public function getEntityName()
    {
        return Product::class;
    }

    public function
    createFormAction()
    {
        /** @var CodeType $sizesCodeType */
        $sizesCodeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SIZES
            ]
        ]);

        if (!$sizesCodeType) {
            return $this->returnNotFound();
        }

        $sizeCodes = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $sizesCodeType->getEntityId()
            ],
            'OrderItem' => Code::PROPERTY_ID
        ]);

        $washItems = $this->entityManager->find(WashingCareLabel::class);

        $this->view->setVar('sizes', $sizeCodes);
        $this->view->setVar('washItems', $washItems);
        $this->view->setVar('form', new ProductForm());

        $this->view->pick('products/create-form');
    }

    public function editFormAction($productId)
    {
        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_FOOT_JS)
            ->addJs(APP_PATH . 'assets/js/steps.min.js');

        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_HEAD_CSS)
            ->addCss(APP_PATH . 'assets/css/steps.css');

        /** @var Product $product */
        $product = $this->entityManager->findOne(Product::class, $productId);

        /** @var CodeType $sizesCodeType */
        $sizesCodeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SIZES
            ]
        ]);

        if (!$product || !$sizesCodeType) {
            $this->returnNotFound();
        }

        $form = new ProductCopyForm($product);
        $form->add((new Hidden('productId'))->setDefault($productId));

        $sizeCodes = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $sizesCodeType->getEntityId()
            ]
        ]);

        $productSizes = [];
        $productBarCodes = [];
        foreach ($product->getProductSizes() as $productSize) {
            $code = $productSize->getCode() ? $productSize->getCode()->getCode() : '';
            $productSizes[$code] = $code;
            $productBarCodes[$code] = $productSize->getBarcode();
        }

        $washItems = $this->entityManager->find(WashingCareLabel::class);

        $washData = "";
        $product->getWashLabel() ? $washData=$product->getWashLabel() : $washData="a!Ax+";

        $this->view->pick('products/edit-form');
        $this->view->setVar('form', $form);
        $this->view->setVar('product', $product);
        $this->view->setVar('sizes', $sizeCodes);
        $this->view->setVar('washData', $washData);
        $this->view->setVar('washItems', $washItems);
        $this->view->setVar('sizes', $sizeCodes);
        $this->view->setVar('productSizes', $productSizes);
        $this->view->setVar('productBarCodes', $productBarCodes);
    }

    public function copyProductAction($productId)
    {
        /** @var Product $product */
        $product = $this->entityManager->findOne(Product::class, $productId);

        $sizesCodeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SIZES
            ]
        ]);

        if (!$product || !$sizesCodeType) {
            return $this->returnNotFound();
        }

        $sizeCodes = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $sizesCodeType->getEntityId()
            ]
        ]);

        $rpoductcode = mb_substr($product->getCode(), 0, -2);
        $products = $this->entityManager->find(Product::class, [
           'SUBSTRING(code, 1, 11) = :code:',
            'bind' => [
                'code' => $rpoductcode
            ]
        ]);

        $productSizes = [];
        $productBarCodes = [];
        foreach ($product->getProductSizes() as $productSize) {
            $code = $productSize->getCode() ? $productSize->getCode()->getCode() : '';
            $productSizes[$code] = $code;
            $productBarCodes[$code] = $productSize->getBarcode();
        }

        $washItems = $this->entityManager->find(WashingCareLabel::class);

        $washData = "";
        $product->getWashLabel() ? $washData=$product->getWashLabel() : $washData="a!Ax+";

        $this->view->pick('products/copy-product');
        $this->view->setVar('washData', $washData);
        $this->view->setVar('washItems', $washItems);
        $this->view->setVar('form', new ProductCopyForm(null, ['product' => $product]));
        $this->view->setVar('code', $rpoductcode . sprintf('%02d', $products->count()));
        $this->view->setVar('product', $product);
        $this->view->setVar('sizes', $sizeCodes);
        $this->view->setVar('productSizes', $productSizes);
        $this->view->setVar('productBarCodes', $productBarCodes);
    }

    public function checkForSameCodesAction()
    {
        $this->view->disable();
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        $products = $this->entityManager->find(Product::class, [
            'SUBSTRING(code, 1, 11) = :code: AND id != :productId:',
            'bind' => [
                'code' => mb_substr($this->request->getPost('productCode'), 0, 11),
                'productId' => $this->request->getPost('productId')
            ]
        ]);

        $this->response->setContent(json_encode(['counter' => sprintf('%02d', $products->count())]));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function deriveNameFromCodeAction()
    {
        $this->view->disable();
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var Classification $classification */
        $classification = $this->entityManager->findOne(Classification::class, [
            Product::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('code')
            ]
        ]);

        $name = '';
        if ($classification) {
            $name = trim($classification->getName());
            $nameSegments = explode(' ', $name);
            $name = array_pop($nameSegments);
            $lastChar = mb_substr($name, -1);

            if ($lastChar === 'e') {
                $name = rtrim($name, $lastChar) . 'a';
            }

            $name .= ' ';
        }

        $this->response->setContent(json_encode(['name' => $name]));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var array $postData */
        $postData = $this->request->getPost();

        /** @var Code $code */
        $code = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $postData['productTypeCodeId']
            ]
        ]);

        /** @var Classification $oldCollectionClassification */
        $oldCollectionClassification = $this->entityManager->findOne(Classification::class, [
            Classification::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $postData['oldCollection']
            ]
        ]);

        $isOrderable = false;
        $isSynced = false;
        if ($oldCollectionClassification && $oldCollectionClassification->getOrderClassifications()->count()) {
            $isOrderable = true;
        }

        $sizesCodeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SIZES
            ]
        ]);

        if (!$code || !$sizesCodeType) {
            $this->returnNotFound();
        }

        /** @var WashingCareLabel $washCareLabels */
        $washCareLabels = $this->entityManager->find(WashingCareLabel::class);
        $washedKeys= [];
        /** @var WashingCareLabel $washCareLabel */
        foreach ($washCareLabels as $washCareLabel) {
            $washedKeys[$washCareLabel->getId()]=$washCareLabel->getPrintSign();
        }

        $stringWash = "";

        foreach ($postData['washed'] as $washData) {
            if (array_key_exists($washData,$washedKeys)) {
                $stringWash .= $washedKeys[$washData];
            }
        }

        $product = new Product();
        $product->setUserCreatedId($this->currentUser->getId());
        $product->setProductCodeType($code);
        $product->setCode($postData['finalCode']);
        $product->setName($postData['name']);
        $product->setIsOrderable($isOrderable);
        $product->setIsSynced($isSynced);
        $product->setStatus(Product::STATUS_READY);
        $product->setPriorityId($postData['priorityId']);
        $product->setProduceType($postData['produceType']);
        $product->setParentId($postData['parentId'] ?: null);
        $product->setCreated( (new \Vemid\Date\DateTime())->getUnixFormat());
        $product->setComposition($postData['composition']);
        $product->setWashLabel($stringWash);

        if ($this->request->hasFiles()) {
            /** @var Phalcon\Http\Request\File $file */
            /** @var array $uploadedFiles */
            $uploadedFiles = $this->request->getUploadedFiles();
            $file = array_pop($uploadedFiles);

            if ($file !== null) {
                $uploadPath = $product->getUploadPath();

                $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();
                if ($file->moveTo($filePath)) {
                    if (is_file($imagePath = $uploadPath . $product->getImage())) {
                        @unlink($imagePath);
                    }

                    $product->setImage(basename($filePath));
                }
            }
        }

        $form = new ProductForm();
        $form->setEntity($product);

        $transactionManager = $this->getDI()->getTransactionManager();
        $transaction = $transactionManager->get();

        if ($form->isValid($postData) && !$product->save()) {
            return $this->response->redirect('products/create-form');
        }

        $isValid = true;
        $sizes = $postData['sizes'];
        $barcodes = $postData['barcode'];
        asort($sizes);
        unset($postData['sizes']);
        unset($postData['barcode']);

        foreach ($sizes as $size => $value) {
            /** @var Code $code */
            $code = $this->entityManager->findOne(Code::class, [
                Code::PROPERTY_CODE . ' = :code: AND '.
                Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                'bind' => [
                    'code' => $value,
                    'codeTypeId' => $sizesCodeType->getEntityId()
                ]
            ]);

            if (!$code) {
                continue;
            }

            $productSize = new ProductSize();
            $productSize->setProduct($product);
            $productSize->setCode($code);
            $productSize->setBarcode($barcodes[$value] ?? null);
            $productSize->setIsSynced($isSynced);
            $isValid = $this->saveEntity($productSize);

            if (!$isValid) {
                $transaction->rollback();

                return $this->response->redirect('products/create-form');
            }
        }

        foreach ($postData as $name => $value) {
            if ($name === 'productTypeCodeId') {
                continue;
            }

            if ($name === 'productTypeCodeId') {
                continue;
            }

            if ($name === 'washed') {
                continue;
            }

            $entity = null;
            if (
                $name === 'dezenType' || $name === 'manufacturer' || $name === 'seasonCollection' || $name === 'modelType'
                || $name === 'printType' || $name === 'applicationAttribute' || $name === 'materialType'
                || $name === 'modelProductType' || $name === 'model' || $name === 'modelInsane'
            ) {
                $entity = $this->entityManager->findOne(Attribute::class, [
                    Attribute::PROPERTY_ID . ' = :id:',
                    'bind' => [
                        'id' => $value
                    ]
                ]);
            }

            if (!$entity) {
                $entity = $this->entityManager->findOne(Classification::class, [
                    Classification::PROPERTY_CODE . ' = :code:',
                    'bind' => [
                        'code' => $value
                    ]
                ]);
            }

            if (!$entity) {
                /** @var CodeType $codeType */
                $codeType = $this->entityManager->findOne(CodeType::class, [
                    CodeType::PROPERTY_CODE . ' = :code:',
                    'bind' => [
                        'code' => CodeType::PRODUCT_TYPE
                    ]
                ]);

                $entity = $this->entityManager->findOne(Code::class, [
                    Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
                    Code::PROPERTY_CODE . ' = :code:',
                    'bind' => [
                        'codeTypeId' => $codeType->getId(),
                        'code' => $value
                    ]
                ]);
            }

            if ($entity) {
                $productClassification = new ProductClassification();
                $productClassification->setProduct($product);
                $productClassification->setProductClassification($entity);
                $productClassification->setIsSynced($isSynced);
                $isValid = $this->saveEntity($productClassification);
            }

            if (!$isValid) {
                $transaction->rollback();

                return $this->response->redirect('products/create-form');
            }
        }

        $transaction->commit();

        return $this->response->redirect('products/list');
    }

    public function editAction()
    {
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }


        /** @var array $postData */
        $postData = $this->request->getPost();

        /** @var Classification $oldCollectionClassification */
        $oldCollectionClassification = $this->entityManager->findOne(Classification::class, [
            Classification::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $postData['oldCollection']
            ]
        ]);

        $isOrderable = false;
        $isSynced = false;
        if ($oldCollectionClassification && $oldCollectionClassification->getOrderClassifications()->count()) {
            $isOrderable = true;
        }

        /** @var Product $product */
        $product = $this->entityManager->findOne(Product::class, $postData['productId']);

        if (!$product) {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('Product not found'),
                null,
                Message::DANGER
            );

            return $this->response->redirect('/products/create-form');
        }

        /** @var WashingCareLabel $washCareLabels */
        $washCareLabels = $this->entityManager->find(WashingCareLabel::class);
        $washedKeys= [];
        /** @var WashingCareLabel $washCareLabel */
        foreach ($washCareLabels as $washCareLabel) {
            $washedKeys[$washCareLabel->getId()]=$washCareLabel->getPrintSign();
        }
        $stringWash = "";

        if (isset($postData['washed'])) {
            foreach ($postData['washed'] as $washData) {
                if (array_key_exists($washData,$washedKeys)) {
                    $stringWash .= $washedKeys[$washData];
                }
            }
        }

        $product->setCode($postData['finalCode']);
        $product->setIsOrderable($isOrderable);
        $product->setIsSynced($isSynced);
        $product->setPriorityId($postData['priorityId']);
        $product->setParentId($postData['parentId'] ?: null);
        $product->setStatus(Product::STATUS_READY);
        $product->setProduceType($postData['produceType']);
        $product->setWashLabel($stringWash);
        $product->setComposition($postData['composition']);

        /** @var CodeType $sizesCodeType */
        $sizesCodeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SIZES
            ]
        ]);

        if (!$sizesCodeType) {
            return $this->returnNotFound();
        }

        $postData['productTypeCodeId'] = $product->getProductCodeTypeId();
        $transactionManager = $this->getDI()->getTransactionManager();
        $transaction = $transactionManager->get();

        if ($this->request->hasFiles()) {
            /** @var Phalcon\Http\Request\File $file */
            /** @var array $uploadedFiles */
            $uploadedFiles = $this->request->getUploadedFiles();
            $file = array_pop($uploadedFiles);

            if ($file !== null) {
                $uploadPath = $product->getUploadPath();

                $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();
                if ($file->moveTo($filePath)) {
                    if (is_file($imagePath = $uploadPath . $product->getImage())) {
                        @unlink($imagePath);
                    }

                    $product->setImage(basename($filePath));
                }
            }
        }

        $form = new ProductCopyForm($product);
        if ($form->isValid($postData)) {
            $this->saveEntity($product);
        }


        $product->getProductClassifications()->delete();

        foreach ($product->getProductSizes() as $delete) {
            $delete->delete();
        }

        $isValid = true;

        $sizes = $postData['sizes'];
        $barcodes = $postData['barcode'];
        asort($sizes);
        unset($postData['sizes']);
        unset($postData['barcode']);

        if (count($sizes)) {
            foreach ($sizes as $size => $value) {
                /** @var Code $code */
                $code = $this->entityManager->findOne(Code::class, [
                    Code::PROPERTY_CODE . ' = :code: AND '.
                    Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
                    'bind' => [
                        'code' => $value,
                        'codeTypeId' => $sizesCodeType->getEntityId()
                    ]
                ]);

                if (!$code) {
                    continue;
                }

                /** @var $productSize $ppProductSize */
                $productSize = $this->entityManager->findOne(ProductSize::class, [
                    ProductSize::PROPERTY_PRODUCT_ID . ' = :code: AND '.
                    ProductSize::PROPERTY_CODE_ID . ' = :size:',
                    'bind' => [
                        'code' => $product->getId(),
                        'size' => $code->getId()
                    ]
                ]);

                if(!$productSize) {
                    $productSize = new ProductSize();
                    $productSize->setProduct($product);
                    $productSize->setCode($code);
                    $productSize->setBarcode($barcodes[$value] ?? null);

                }

                $productSize->setProduct($product);
                $productSize->setCode($code);
                $productSize->setBarcode($barcodes[$value] ?? null);
                $isValid = $this->saveEntity($productSize);

                if (!$isValid) {
                    $transaction->rollback();

                    return $this->response->redirect('products/create-form');
                }
            }
        }

        foreach ($postData as $name => $value) {
            if ($name === 'productTypeCodeId') {
                continue;
            }

            if ($name === 'washed') {
                continue;
            }

            $entity = null;
            if (
                $name === 'dezenType' || $name === 'manufacturer' || $name === 'seasonCollection' || $name === 'modelType'
                || $name === 'printType' || $name === 'applicationAttribute' || $name === 'materialType'
                || $name === 'modelProductType' || $name === 'model' || $name === 'modelInsane'
            ) {
                $entity = $this->entityManager->findOne(Attribute::class, [
                    Attribute::PROPERTY_ID . ' = :id:',
                    'bind' => [
                        'id' => $value
                    ]
                ]);
            }

            if (!$entity) {
                $entity = $this->entityManager->findOne(Classification::class, [
                    Classification::PROPERTY_CODE . ' = :code:',
                    'bind' => [
                        'code' => $value
                    ]
                ]);
            }

            if (!$entity) {
                /** @var CodeType $codeType */
                $codeType = $this->entityManager->findOne(CodeType::class, [
                    CodeType::PROPERTY_CODE . ' = :code:',
                    'bind' => [
                        'code' => CodeType::PRODUCT_TYPE
                    ]
                ]);

                $entity = $this->entityManager->findOne(Code::class, [
                    Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
                    Code::PROPERTY_CODE . ' = :code:',
                    'bind' => [
                        'codeTypeId' => $codeType->getId(),
                        'code' => $value
                    ]
                ]);
            }

            if ($entity) {
                $productClassification = new ProductClassification();
                $productClassification->setProduct($product);
                $productClassification->setProductClassification($entity);
                $isValid = $this->saveEntity($productClassification);
            }

            if (!$isValid) {
                $transaction->rollback();

                return $this->response->redirect('products/edit-form');
            }
        }

        //$task = new UpdateProductCodeMisTask();
        //$task->product = $product;
        //$task->userName = $this->currentUser->getUsername();
        //$task->runInBackground();

        $transaction->commit();

        return $this->response->redirect('products/list');
    }

    public function updateMisAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        if (!$status = $this->request->getPost('status')) {
            return $this->returnNotFound();
        }

        /** @var \Product $product */
        $product = $this->entityManager->findOne(Product::class, $this->request->getPost('productId'));
        if (!$product) {
            return $this->returnNotFound();
        }

        /** @var AuditLog $auditLog */
        $auditLog = $this->entityManager->findOne(AuditLog::class, [
            'modifiedEntityName = :modifiedEntityName: AND modifiedEntityId = :modifiedEntityId: AND operation = :operation:',
            'bind' => [
                'modifiedEntityName' => Product::class,
                'modifiedEntityId' => $product->getId(),
                'operation' => AuditLog::OPERATION_CREATE
            ]
        ]);

        if ($status === 'enable') {
            $product->setStatus(\Product::STATUS_FINISHED);

            if (!$this->entityManager->save($product)) {
                throw new LogicException('Nije kopletiran!');
            }

        } elseif ($status === 'disable') {
            $misSync = new \Vemid\Service\MisWsdl\DisableProduct($product);
            $misSync->run();
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t(sprintf(
                    'Proizvod (%s) je otkazan',
                    $product->getCode()
                )),
                null,
                MessengerMessage::WARNING
            );
        } else {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('Greška!! Status ne postoji!'),
                null,
                MessengerMessage::DANGER
            );
        }

        return;
    }

    public function setOrderableAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        if (!$status = $this->request->getPost('status')) {
            return $this->returnNotFound();
        }

        /** @var \Product $product */
        $product = $this->entityManager->findOne(Product::class, $this->request->getPost('productId'));
        if (!$product) {
            return $this->returnNotFound();
        }

        if ($status === 'enable') {
            $product->setIsOrderable(true);

            if (!$this->entityManager->save($product)) {
                throw new LogicException('Nije promenjen!');
            }

        } elseif ($status === 'disable') {
            $product->setIsOrderable(false);

            if (!$this->entityManager->save($product)) {
                throw new LogicException('Nije promenjen!');
            }
        }
        else {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('Greška!! Status ne postoji!'),
                null,
                MessengerMessage::DANGER
            );
        }
        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t('Uspesno setovano!'),
            null,
            MessengerMessage::SUCCESS
        );
        return;
    }

    public function listSizesAction($classificationCode)
    {
        $this->view->disable();

        /** @var Classification $classification */
        $classification = $this->entityManager->findOne(Classification::class, [
            Classification::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $classificationCode
            ]
        ]);

        $sizes = [];
        if (!$classification) {
            return $this->returnNotFound();
        }

        /** @var ClassificationSize[] $sizeCodes */
        $sizeCodes = $this->entityManager->find(ClassificationSize::class, [
            ClassificationSize::PROPERTY_CLASSIFICATION_ID . ' = :classificationId:',
            'bind' => [
                'classificationId' => $classification->getId()
            ]
        ]);

        foreach ($sizeCodes as $sizeCode) {
            if (!$code = $sizeCode->getCode()) {
                continue;
            }

            $sizes[$code->getCode()] = $code->getName();
        }

        $this->response->setContent(json_encode($sizes));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function findMappedClassificationsAndAttributesAction()
    {
        $this->view->disable();

        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var CodeType $codeType */
        $codeType = $this->entityManager->findOne(CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        /** @var Code $productCode */
        $productCode = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId: AND ' .
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('productTypeCodeId'),
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        /** @var Code $code */
        $code = $this->entityManager->findOne(Code::class, [
            Code::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('productTypeCode')
            ]
        ]);

        /** @var Classification $ageClassification */
        $ageClassification = $this->entityManager->findOne(Classification::class, [
            Classification::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('age')
            ]
        ]);

        /** @var Classification $seasonClassification */
        $seasonClassification = $this->entityManager->findOne(Classification::class, [
            Classification::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('season')
            ]
        ]);

        /** @var Classification $collectionClassification */
        $collectionClassification = $this->entityManager->findOne(Classification::class, [
            Classification::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $this->request->getPost('collection')
            ]
        ]);

        if (!$code || !$ageClassification || !$seasonClassification || !$collectionClassification) {
            return $this->returnNotFound();
        }

        /** @var Classification[] $ageClassifications */
        $ageClassifications = $this->entityManager->find(Classification::class, [
            Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeId: AND ' .
            Classification::PROPERTY_NAME . ' = :name:',
            'bind' => [
                'codeId' => $ageClassification->getClassificationTypeCodeId(),
                'name' => $ageClassification->getName()
            ]
        ]);

        /** @var Classification[] $seasonClassifications */
        $seasonClassifications = $this->entityManager->find(Classification::class, [
            Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeId: AND ' .
            Classification::PROPERTY_NAME . ' = :name:',
            'bind' => [
                'codeId' => $seasonClassification->getClassificationTypeCodeId(),
                'name' => $seasonClassification->getName()
            ]
        ]);

        /** @var Classification[] $collectionClassifications */
        $collectionClassifications = $this->entityManager->find(Classification::class, [
            Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeId: AND ' .
            Classification::PROPERTY_NAME . ' = :name:',
            'bind' => [
                'codeId' => $collectionClassification->getClassificationTypeCodeId(),
                'name' => $collectionClassification->getName()
            ]
        ]);

        $ageOptions = [];
        foreach ($ageClassifications as $ageClassification) {
            $ageOptions[] = $ageClassification->getId();
        }

        $collectionOptions = [];
        foreach ($collectionClassifications as $collectionClassification) {
            $collectionOptions[] = $collectionClassification->getId();
        }

        $seasonOptions = [];
        foreach ($seasonClassifications as $seasonClassification) {
            $seasonOptions[] = $seasonClassification->getId();
        }

        /** @var ClassificationOldCollectionMap $classificationOldCollectionMap */
        $classificationOldCollectionMap = $this->entityManager->findOne(ClassificationOldCollectionMap::class, [
            ClassificationOldCollectionMap::PROPERTY_CODE_ID . ' = :codeId: AND '.
            ClassificationOldCollectionMap::PROPERTY_CLASSIFICATION_AGE_ID . ' IN ({ageIds:array}) AND '.
            ClassificationOldCollectionMap::PROPERTY_CLASSIFICATION_SEASON_ID . ' IN ({seasonIds:array}) AND '.
            ClassificationOldCollectionMap::PROPERTY_CLASSIFICATION_COLLECTION_ID . ' IN ({collectionIds:array})',
            'bind' => [
                'codeId' => $code->getId(),
                'ageIds' => $ageOptions,
                'seasonIds' => $seasonOptions,
                'collectionIds' => $collectionOptions
            ]
        ]);

        if (!$classificationOldCollectionMap) {
            return $this->returnNotFound();
        }

        $classificationOldCodeMap = $classificationOldCollectionMap->getClassificationOldCode();
        $doubleSeason = $classificationOldCollectionMap->getClassificationDoubleSeason();

        /** @var Classification $classificationOldCode */
        $classificationOldCode = $this->entityManager->findOne(Classification::class, [
            Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :categoryCodeId: AND ' .
            Classification::PROPERTY_NAME . ' = :name:',
            'bind' => [
                'categoryCodeId' => $productCode->getId(),
                'name' => $classificationOldCodeMap->getName()
            ]
        ]);

        /** @var Classification $classificationDoubleSeason */
        $classificationDoubleSeason = $this->entityManager->findOne(Classification::class, [
            Classification::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID . ' = :categoryCodeId: AND ' .
            Classification::PROPERTY_NAME . ' = :name:',
            'bind' => [
                'categoryCodeId' => $productCode->getId(),
                'name' => $doubleSeason->getName()
            ]
        ]);
        $arr = [
            'oldCollection' => $classificationOldCode ? $classificationOldCode->getCode() : null,
            'doubleSeason' => $classificationDoubleSeason ? $classificationDoubleSeason->getCode() : null,
            'manufacturer' => $classificationOldCollectionMap->getAttributeProducerId(),
            'modelProductType' => $classificationOldCollectionMap->getAttributeProductTypeId(),
        ];

        $this->response->setContent(json_encode($arr));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getBaseProductsAction()
    {
        $this->view->disable();
        $json = [];
        $term = $this->request->getQuery('term');

        /** @var Product[] $products */
        $products = $this->entityManager->find(Product::class, [
            Product::PROPERTY_CODE . ' LIKE :term: OR ' . Product::PROPERTY_NAME . ' LIKE :term:',
            'bind' => [
                'term' => "%$term%"
            ]
        ]);

        foreach ($products as $product) {
            $id = $product->getId();
            $json[$id]['id'] = $id;
            $json[$id]['value'] = $product->getName();
            $json[$id]['label'] = $product->getName() . ' (' . $product->getCode() . ')';
        }

        $this->response->setContent(json_encode($json));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function reUploadImageAction()
    {
        $this->view->disable();

        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var \Product $product */
        $product = $this->entityManager->findOne(Product::class, $this->request->getPost('productId'));
        if (!$product) {
            return $this->returnNotFound();
        }

        if ($this->request->hasFiles()) {
            /** @var Phalcon\Http\Request\File $file */
            /** @var array $uploadedFiles */
            $uploadedFiles = $this->request->getUploadedFiles();
            $file = array_pop($uploadedFiles);

            if ($file !== null) {
                $uploadPath = $product->getUploadPath();

                $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();
                if ($file->moveTo($filePath)) {
                    if (is_file($imagePath = $uploadPath . $product->getImage())) {
                        @unlink($imagePath);
                    }

                    $product->setImage(basename($filePath));
                    if (!$this->entityManager->save($product)) {
                        $this->addFlashMessagesFromEntity($product);
                    } else {
                        $this->addFlashMessage(
                            $this->getDI()->getTranslator()->t('Slika proizvoda je promenjena'),
                            null,
                            MessengerMessage::SUCCESS
                        );
                    }
                }
            }
        }
    }

    public function sendCollectionToMisFormAction()
    {
        $form = new ProductSyncForm();

        return (new Renderer())->render($form);
    }

    public function sendCollectionToMisAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var array $oldCollectionCodes */
        $oldCollectionCodes = $this->request->getPost('oldCollections');

        foreach ($oldCollectionCodes as $oldCollectionCode) {
            /** @var Classification $oldCollectionId */
            $oldCollectionId = $this->entityManager->findOne(Classification::class, [
                Classification::PROPERTY_CODE.' = :code:',
                'bind' => [
                    'code' => $oldCollectionCode
                ]
            ]);

            /** @var ProductClassification[] $productClassifications */
            $productClassifications = $this->entityManager->find(ProductClassification::class, [
                ProductClassification::PROPERTY_ENTITY_TYPE_ID.'= :entityTypeId: AND '.
                ProductClassification::PROPERTY_ENTITY_ID.' = :entityId: ',
                'bind' => [
                    'entityTypeId' => \Vemid\Entity\Type::CLASSIFICATION,
                    'entityId' => $oldCollectionId->getId()
                ]
            ]);

            foreach ($productClassifications as $productClassification) {
                if (!$product = $productClassification->getProduct()) {
                    continue;
                }

                $product->setStatus(\Product::STATUS_FINISHED);

                if (!$this->entityManager->save($product)) {
                    $this->addFlashMessagesFromEntity($product);
                    return;
                }
            }
        }

        $this->addFlashMessage(
            $this->translator->t('Uspešno setovana sinhronizacija!'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function exportProductsFormAction()
    {
        $form = new ProductSeasonForm();

        return (new Renderer())->render($form);
    }

    public function exportProductsAction()
    {

        /** @var array $season */
        $season = $this->request->getPost('season');
        /** @var array $users */
        $users = $this->request->getPost('users');

        $printer = new \Vemid\Services\Excel\ProductsPrinter($this->entityManager, $this->modelsManager,$season, $users);
        $printer->printFile();


        $this->addFlashMessage(
            $this->translator->t('Export podataka u toku. Uskoro će fajl biti poslat mailom!'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );

    }

    public function exportProductMarkupAction()
    {

        /** @var array $season */
        $season = $this->request->getPost('oldCollections');
        /** @var array $users */
        $users = $this->request->getPost('users');
        /** @var string $fileName */
        $fileName = $this->request->get('name');





        $printer = new Vemid\Service\Product\ProductData($this->entityManager, $this->modelsManager,$season, $users,$fileName);
        $printer->exportCalculation();

        $this->addFlashMessage(
            $this->translator->t('Uspešno setovano slanje marze!'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );

    }

    public function exportProductsColorFormAction()
    {

        $form = new ProductSyncForm();

        $users = [];
        foreach ($this->entityManager->find(User::class) as $user) {
            if($user->getEmail()) {
                $users[$user->getId()] = $user->getEmail();
            }
        }

        $users = new \Phalcon\Forms\Element\Select('users', $users);
        $users->setLabel('Korisnici');
        $users->setAttribute('class', 'search');
        $users->setAttribute('multiple', 'true');
        $users->setName($users->getName() . '[]');
        $users->setDefault([]);

        $name = new \Phalcon\Forms\Element\Text('name');
        $name->setLabel('Naziv fajla');

        $form->add($users);
        $form->add($name);

        return (new Renderer())->render($form);
    }

    public function exportProductsColorAction()
    {
        /** @var array $season */
        $season = $this->request->getPost('oldCollections');
        /** @var array $users */
        $users = $this->request->getPost('users');
        /** @var string $fileName */
        $fileName = $this->request->get('name');

        $printer = new Vemid\Service\Product\ProductData($this->entityManager, $this->modelsManager,$season, $users,$fileName);
        $printer->exportData();

        $this->addFlashMessage(
            $this->translator->t('Uspešno setovano slanje izbojenja!'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function exportProductsPresentationFormAction()
    {

        $form = new ProductSyncForm();

        $users = [];
        foreach ($this->entityManager->find(User::class) as $user) {
            if($user->getEmail()) {
                $users[$user->getId()] = $user->getEmail();
            }
        }

        $users = new \Phalcon\Forms\Element\Select('users', $users);
        $users->setLabel('Korisnici');
        $users->setAttribute('class', 'search');
        $users->setAttribute('multiple', 'true');
        $users->setName($users->getName() . '[]');
        $users->setDefault([]);

        $name = new \Phalcon\Forms\Element\Text('name');
        $name->setLabel('Naziv fajla');

        $form->add($users);
        $form->add($name);

        return (new Renderer())->render($form);
    }

    public function exportProductsPresentationAction()
    {
        /** @var array $season */
        $season = $this->request->getPost('oldCollections');
        /** @var array $users */
        $users = $this->request->getPost('users');
        /** @var string $fileName */
        $fileName = $this->request->get('name');

        $printer = new Vemid\Service\Product\ProductData($this->entityManager, $this->modelsManager,$season, $users,$fileName);
        $printer->exportPresentationData();

        $this->addFlashMessage(
            $this->translator->t('Uspešno setovano slanje izbojenja!'),
            null,
            \Vemid\Messenger\Message::SUCCESS
        );
    }

    public function parentProductsAction()
    {
        $this->view->disable();

        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        /** @var \Vemid\Entity\Repository\ProductRepository $productRepository */
        $productRepository = $this->entityManager->getRepository(Product::class);

        $products = [];
        foreach ($productRepository->findPossiblePrentProducts($this->request->getPost('productCode')) as $similarProduct) {
            $products[$similarProduct->getId()] = $similarProduct->getCode();
        }

        $this->response->setContent(json_encode($products));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function testPdfAction()
    {
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('TCPDF Example 001');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        $pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            $l = include dirname(__FILE__ .'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

// ---------------------------------------------------------

// set default font subsetting mode
        $pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
        $pdf->SetFont('helvetica', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
        $pdf->AddPage();

// set text shadow effect
        //$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
        $html = <<<EOD
<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
<img src="https://elektronska-karta.bebakids.com/uploads/files/d89cafe5.jpg" alt="test alt attribute" />
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>

<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>

<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>

<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>

<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>

<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
<i>This is the first example of TCPDF library.</i>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>








EOD;

// Print text using writeHTMLCell()
        //$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        $pdf->writeHTML($html, true, false, true, false, '');

// ---------------------------------------------------------
        ob_end_clean();
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
        $pdf->Output( sys_get_temp_dir().'/test_example_001.pdf', 'F');
        $file = sys_get_temp_dir().'/test_example_001.pdf';
        $pdfOne = sprintf(
            '%s/%s1.pdf',
            rtrim(sys_get_temp_dir(), '/'),
            'tmp'
        );

        //file_put_contents($pdfOne, $file);

        $connId = ftp_connect('51.255.80.216');
        $ftpUserName = "nbsoft_ftp_usr";
        $ftpUserPass = "BYc6d6ff68075ehp";

        if (!ftp_login($connId, $ftpUserName, $ftpUserPass)) {
            throw new \LogicException(sprintf('Login to FTP failed!'));
        }

        if (!ftp_chdir($connId, '/httpdocs/elkarta')) {
            throw new \LogicException(sprintf('DIR do not exist!'));
        }

        if (!ftp_put($connId, "test" . '.pdf', $pdfOne, FTP_BINARY)) {
            throw new \LogicException(sprintf('There was a problem while uploading %s', $pdfOne));
        }


    }

    public function printLabelOverviewAction($id)
    {
        $this->tag::appendTitle($this->translator->t("Pregled etikete"));
        /** @var \Product $product */
        $product = $this->entityManager->findOne(Product::class, $id);
        if (!$product) {
            return $this->returnNotFound();
        }

        $hydrator = new \Vemid\Service\Product\Hydrator($product, $this->entityManager);

        /** @var ProductSize $productSizes */
        $productSizes = $product->getProductSizes(['order' => 'codeId ASC']);
        $productSizes2 = $product->getProductSizes(['order' => 'codeId ASC']);

        $sizes = $hydrator->getAllProductSizes();

        /** @var FarbenCard $farbenCard */
        $farbenCard = $product->getFarbenCard();

        $washData = "";
        if($farbenCard){
            $farbenCard->getWashLabel() ? $washData=$farbenCard->getWashLabel() : $washData="a!Ax+";
        }
        elseif ($product->getComposition()) {
            $washData = $product->getWashLabel();
        }

        /** @var stdClass $config */
        $config = $this->getDI()->getConfig();
        $generatorHtml = new \Picqer\Barcode\BarcodeGeneratorPNG();

        $pricelists = $this->getDI()->getEntityManager()->find(\Pricelist::class);

        $productPrices = $this->getDI()->getProductPrices();

        $pricelistData = [];
        /** @var Pricelist $pricelist */
        foreach ($pricelists as $pricelist) {
            $pricelistData[trim($pricelist->getName())]['MP'] = $productPrices->getRetailPrice($pricelist,$product);
        }
        $washItems = $this->entityManager->find(WashingCareLabel::class);

        $this->view->setVar('siteUrl',ltrim($config->application->baseUri, '/'));
        $this->view->setVar('product', $product);
        $this->view->setVar('pricelistData', $pricelistData);
        $this->view->setVar('productSizes', $productSizes);
        $this->view->setVar('productSizes2', $productSizes2);
        $this->view->setVar('farbenCard', $farbenCard);
        $this->view->setVar('generatorHtml', $generatorHtml);
        $this->view->setVar('barcodeType' ,BarcodeGeneratorHTML::TYPE_CODE_128);
        $this->view->setVar('washData', str_split($washData));
        $this->view->setVar('washItems', $washItems);
        $this->view->pick('products/print-label-overview');

    }

    public function disablePrintLabelAction($id)
    {
        /** @var \Product $product */
        $product = $this->entityManager->findOne(Product::class, $id);
        if (!$product) {
            return $this->returnNotFound();
        }

        /** @var ProductSize $printSizes */
        $printSizes = $product->getProductSizes();

        /** @var ProductSize $printSize */
        foreach ($printSizes as $printSize) {

            /** @var PrintLabelItem $print */
            foreach ($printSize->getPrintLabelItems() as $print){
                $print->setPrinted(true);
                $print->save();
            }
        }

        $this->flashSession->success('Uspešno ste ponistili stampu etikete!');
        return $this->response->redirect('products/print-label-overview/' . $id);
    }

    public function printLabelAction($id)
    {
        /** @var \Product $product */
        $product = $this->entityManager->findOne(Product::class, $id);
        if (!$product) {
            return $this->returnNotFound();
        }
        /** @var FarbenCard $farbenCard */
        $farbenCard = $product->getFarbenCard();

        $washData = "";
        if($farbenCard){
            $farbenCard->getWashLabel() ? $washData=$farbenCard->getWashLabel() : $washData="a!Ax+";
        }

        $pricelists = $this->getDI()->getEntityManager()->find(\Pricelist::class);

        $productPrices = $this->getDI()->getProductPrices();

        $pricelistData = [];
        /** @var Pricelist $pricelist */
        foreach ($pricelists as $pricelist) {
            $pricelistData[trim($pricelist->getName())]['MP'] = $productPrices->getRetailPrice($pricelist,$product);
        }

        $productSizes = $product->getProductSizes(['order' => 'codeId ASC']);
        $productSizes2 = clone $productSizes;

        /** @var ProductSize $productSize */
        foreach ($productSizes as $productSize){

            $qtyData = 0;
            foreach ($productSize->getProductionOrders() as $productionOrder) {
                $qtyData += $productionOrder->getRealizedAQty()+$productionOrder->getRealizedBQty();
            }

            $qty = ($qtyData > 5 ? ($qtyData>10 ?$qtyData+10 : $qtyData ) : 0);
            $height="";
            switch ($productSize->getCode()->getCode()){
                case "00" :
                    $height = 74;
                    break;
                case "01" :
                    $height = 80;
                    break;
                case "02" :
                    $height = 92;
                    break;
                case "03" :
                    $height = 98;
                    break;
                case "04" :
                    $height = 104;
                    break;
                case "05" :
                    $height = 110;
                    break;
                case "06" :
                    $height = 116;
                    break;
                case "08" :
                    $height = 128;
                    break;
                case "10" :
                    $height = 140;
                    break;
                case "12" :
                    $height = 152;
                    break;
                case "14" :
                    $height = 164;
                    break;
                default : $height= $productSize->getCode()->getCode();
            }

            $sizeData = "";
            /** @var ProductSize $productSize2 */
            foreach ($productSizes2 as $productSize2){
                if ($productSize2->getCode() === $productSize->getCode()){
                    $sizeData .= "<span style='background-color: black; color: white'><b style='margin-left:4px !important'>".$productSize2->getCode()." </b></span>";
                }
                else
                    $sizeData .= "<span><b>".$productSize2->getCode()." </b></span>";
            }

            $productSizeData = new PrintLabelItem();
            $productSizeData->setProductSizeId($productSize->getId());
            $productSizeData->setQty($qty);
            $productSizeData->setPriceSrb($pricelistData['DINARSKI']['MP']);
            $productSizeData->setPriceCg($pricelistData['CG']['MP']);
            $productSizeData->setPriceBih($pricelistData['BIH']['MP']);
            $productSizeData->setHeight($height);
            $productSizeData->setSizes($sizeData);
            $productSizeData->setComposition($farbenCard ? $farbenCard->getPaperLabel() : $productSize->getProduct()->getComposition());
            $productSizeData->setWashData($farbenCard ? $farbenCard->getWashLabel() : $product->getWashLabel());
            $productSizeData->setCreated((new \Vemid\Date\DateTime())->getUnixFormat());

//            echo "<pre>";
//            print_r($productSize->getId());
//            echo "<pre>";
//            print_r($qty);
//            echo "<pre>";
//            print_r($pricelistData['DINARSKI']['MP']);
//            echo "<pre>";
//            print_r($height);
//            echo "<pre>";
//            print_r($sizeData);
//            echo "<pre>";
//            print_r($farbenCard ? $farbenCard->getPaperLabel() : $productSize->getProduct()->getComposition());
//            echo "<pre>";
//            print_r($farbenCard ? $farbenCard->getWashLabel() : $product->getWashLabel());
//            die();

            if (!$this->getDI()->getEntityManager()->save($productSizeData)) {
                throw new \DomainException('PrintLabel value was not saved');
            }
        }
        $this->flashSession->success('Uspešno ste poslali na stampu etikete!');
        return $this->response->redirect('products/print-label-overview/' . $id);

    }

    public function getProductByBarcodeAction(string $barcode)
    {
        /** @var ProductSize $productSize */
        $productSize = $this->entityManager->findOne(ProductSize::class, [
            ProductSize::PROPERTY_BARCODE . ' = :barcode:',
            'bind' => [
                'barcode' => $barcode
            ]
        ]);

        if (!$productSize) {
            $this->addFlashMessage(
                'Nepostoji produkt sa trazenim barcodom',
                null,
                \Vemid\Messenger\Message::DANGER
            );

            return;
        }

        return [
            'error' => false,
            'product' => (string)$productSize->getProduct(),
            'size' => $productSize->getCode()->getName()
        ];
    }
}
