<?php

use Phalcon\Forms\Element\Select;
use Vemid\Controller\CrudController;
use Vemid\Entity\Type;
use Vemid\Form\Renderer\Json as Renderer;
use Vemid\Messenger\Message;
use Vemid\Messenger\Message as MessengerMessage;
use Vemid\Printer\Manager;

/**
 * Class WarehouseOrdersController
 */
class WarehouseOrderGroupsController extends CrudController
{
    public function initialize()
    {
        $this->assets->collection(\Vemid\Services\Assets\DefaultAssets::ASSETS_FOOT_JS)
            ->addJs(APP_PATH . 'assets/js/data-tables/dataTables.select.min.js');

        parent::initialize();
    }

    public function listAction()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function getCreateFormAction()
    {
        return (new Renderer())->render(
            new WarehouseOrderGroupForm(
                new WarehouseOrderGroup()
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getGroupedFormAction()
    {


        /** @var WarehouseOrderGroup[] $warehouseOrderGroups */
        $warehouseOrderGroups = $this->entityManager->find(WarehouseOrderGroup::class,[
            WarehouseOrderGroup::PROPERTY_GROUPED . ' = :grouped: ',
            'bind' => [
                'grouped' => WarehouseOrderGroup::NON_GROUPED
            ]
        ]);

        $options = [];
        foreach ($warehouseOrderGroups as $warehouseOrderGroup) {
            $options[$warehouseOrderGroup->getId()] = (string)$warehouseOrderGroup->getChannelSupplyEntity()->getChannelSupply() ."-".$warehouseOrderGroup->getChannelSupplyEntity()->getDisplayName() ."-".$warehouseOrderGroup->getWarehouseOrderGroupCode();
        }

        $form = new \Vemid\Form\Form();
        $selectElement = new Select('warehouseGrouped',$options);
        $selectElement->setAttribute('class', 'search');
        $selectElement->setAttribute('multiple', 'true');
        $selectElement->setName('warehouseGrouped');

        $selectElement->setLabel('Za');
        $selectElement->setName($selectElement->getName() . '[]');
        $selectElement->setDefault([]);

        $form->add($selectElement);

        return (new Renderer())->render($form);

    }

    public function groupedWarehouseOrderGroupAction()
    {

        if ($this->request->isPost()) {
            $form = $this->getForm();
            if ($form->isValid($this->request->getPost())) {
                $form->synchronizeEntityData();
            }


            $query = $this->modelsManager->createBuilder();
            $query->addFrom(WarehouseOrderGroupItem::class, 'wogi')
                ->columns([
                    'wog.id as warehouseOrderId',
                    'cse.entryWarehouse as entryWarehouse',
                    'wo.type as type',
                ])
                ->leftJoin(WarehouseOrderGroup::class, 'wogi.warehouseOrderGroupId = wog.id', 'wog')
                ->leftJoin(ChannelSupplyEntity::class, 'wog.channelSupplyEntityId = cse.id', 'cse')
                ->leftJoin(WarehouseOrder::class, 'wogi.warehouseOrderId = wo.id', 'wo')
                ->where('wog.id IN ({ids:array}) ', [
                    'ids' => $form->getValue('warehouseGrouped')
                ])
                ->groupBy('wog.id,cse.entryWarehouse,wo.type');

            $result = $query->getQuery()->execute();

            $arrayMP = [];
            $arrayVP = [];

            foreach ($result->toArray() as $row) {
                if ($row['entryWarehouse'] == 'VP') {
                    $arrayVP[$row['type']][] = $row['warehouseOrderId'];
                } else {
                    $arrayMP[$row['type']][] = $row['warehouseOrderId'];
                }
            }

            $groupedArray = [];

            /** @var Code $magacinMp */
            $magacinMp = $this->entityManager->findOne(Code::class, [
                Code::PROPERTY_CODE . ' = :code: ',
                'bind' => [
                    'code' => '99'
                ]
            ]);

            /** @var Code $magacinVp */
            $magacinVp = $this->entityManager->findOne(Code::class, [
                Code::PROPERTY_CODE . ' = :code: ',
                'bind' => [
                    'code' => 'KP'
                ]
            ]);

            /** @var ChannelSupplyEntity $channelSupplyEntityMP */
            $channelSupplyEntityMP = $this->entityManager->findOne(ChannelSupplyEntity::class, [
                ChannelSupplyEntity::PROPERTY_ENTITY_TYPE_ID . ' = :type: AND ' .
                ChannelSupplyEntity::PROPERTY_ENTITY_ID . ' = :id: ',
                'bind' => [
                    'type' => Type::CODE,
                    'id' => $magacinMp->getId()
                ]
            ]);

            /** @var ChannelSupplyEntity $channelSupplyEntityVP */
            $channelSupplyEntityVP = $this->entityManager->findOne(ChannelSupplyEntity::class, [
                ChannelSupplyEntity::PROPERTY_ENTITY_TYPE_ID . ' = :type: AND ' .
                ChannelSupplyEntity::PROPERTY_ENTITY_ID . ' = :id: ',
                'bind' => [
                    'type' => Type::CODE,
                    'id' => $magacinVp->getId()
                ]
            ]);

            foreach ($arrayVP as $key => $row) {

                if ($arrayVP) {
                    /** @var WarehouseOrderGroupItem[] $warehouseVPOrders */
                    $warehouseVPOrders = $this->entityManager->find(WarehouseOrderGroupItem::class, [
                        WarehouseOrderGroupItem::PROPERTY_WAREHOUSE_ORDER_GROUP_ID . ' IN ({ids:array})',
                        'bind' => [
                            'ids' => $row
                        ]
                    ]);
                    $groupedArray[$key][$channelSupplyEntityVP->getId()] = $warehouseVPOrders;
                }
            }
            foreach ($arrayMP as $key => $row) {
                if ($arrayMP) {
                    /** @var WarehouseOrderGroupItem[] $warehouseMPOrders */
                    $warehouseMPOrders = $this->entityManager->find(WarehouseOrderGroupItem::class, [
                        WarehouseOrderGroupItem::PROPERTY_WAREHOUSE_ORDER_GROUP_ID . ' IN ({ids:array}) ',
                        'bind' => [
                            'ids' => $row
                        ]
                    ]);
                    $groupedArray[$key][$channelSupplyEntityMP->getId()] = $warehouseMPOrders;
                }
            }

            if($groupedArray) {
                foreach ($groupedArray as $type => $group) {
                    foreach ($group as $key => $rows) {
                        /** @var ChannelSupplyEntity $channelSupplyEntity */
                        $channelSupplyEntity = $this->entityManager->findOne(ChannelSupplyEntity::class, $key);
                        $warehouseOrderGroup = new WarehouseOrderGroup();
                        $warehouseOrderGroup->setChannelSupplyEntity($channelSupplyEntity);
                        $warehouseOrderGroup->setGrouped(WarehouseOrderGroup::LOCKED);
                        $warehouseOrderGroup->setType($type == "A" ? WarehouseOrderGroup::A : WarehouseOrderGroup::B);
                        $warehouseOrderGroup->setStatus(WarehouseOrderGroup::EDITABLE);

                        if (!$this->entityManager->save($warehouseOrderGroup)) {
                            $this->addFlashMessagesFromEntity($warehouseOrderGroup);

                            return false;
                        }

                        /** @var WarehouseOrderItem $row */
                        foreach ($rows as $row) {

                            /** @var WarehouseOrder $warehouseOrder */
                            $warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, $row->getWarehouseOrderId());

                            $warehouseOrderGroupItem = new WarehouseOrderGroupItem();
                            $warehouseOrderGroupItem->setWarehouseOrderGroup($warehouseOrderGroup);
                            $warehouseOrderGroupItem->setWarehouseOrder($warehouseOrder);

                            if (!$this->entityManager->save($warehouseOrderGroupItem)) {
                                $this->addFlashMessagesFromEntity($warehouseOrderGroupItem);

                                return false;
                            }

                        }
                    }
                }
            }
            if($result->toArray()) {
                foreach ($form->getValue('warehouseGrouped') as $row) {
                    /** @var WarehouseOrderGroup $warehouseOrderGroup */
                    $warehouseOrderGroup = $this->entityManager->findOne(WarehouseOrderGroup::class, [
                        WarehouseOrderGroup::PROPERTY_ID . ' = :id: ',
                        'bind' => [
                            'id' => $row
                        ]
                    ]);
                    $warehouseOrderGroup->setGrouped(WarehouseOrderGroup::GROUPED);

                    if (!$this->entityManager->save($warehouseOrderGroup)) {
                        $this->addFlashMessagesFromEntity($warehouseOrderGroup);

                        return false;
                    }
                }
            }

            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('Uspesno pregrupisani nalozi'),
                null,
                MessengerMessage::SUCCESS
            );

        }
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $form = $this->getForm();
        if (!$form->isValid($this->request->getPost())) {
            $this->addFlashMessagesFromForm($form);

            return false;
        }

        $form->synchronizeEntityData();
        $entity = $form->getEntity();
        if ($this->saveEntity($entity)) {
            $this->dispatcher->setParam('url', '/warehouse-order-groups/add-items/' . $entity->getId());
        }
    }

    public function addItemsAction($id)
    {
        /** @var WarehouseOrderGroup $warehouseOrderGroup */
        if (!$warehouseOrderGroup = $this->entityManager->findOne(WarehouseOrderGroup::class, $id)) {
            return $this->returnNotFound();
        }

        if (!$warehouseOrderGroup) {
            return $this->returnNotFound();
        }

        $barCodes = [];
        if ($warehouseOrderGroup->isEditable()) {
            foreach ($warehouseOrderGroup->getWarehouseOrderGroupItems() as $orderItem) {
                $barCodes[(string)$orderItem->getWarehouseOrder()->getWarehouseOrderCode()] = $orderItem->getWarehouseOrder()->getWarehouseOrderCode();
            }
        }

        $this->view->setVar('serverStoredBarcodes', json_encode($barCodes));
        $this->view->setVar('warehouseOrderGroup', $warehouseOrderGroup);
        $this->view->pick('warehouse-order-groups/add-items');
    }

    public function addWarehouseOrdersAction()
    {
        $this->tag::appendTitle($this->translator->t("Pregled groupnih naloga"));
        $this->view->setVar('warehouseOrderGroups',  $this->entityManager->find(WarehouseOrderGroup::class));
        $this->view->setVar('form', new WarehouseOrderGroupForm());
    }

    public function autoGroupAction()
    {
        $this->view->pick('warehouse-order-groups/auto-group');
    }

    public function makeGroupsAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $barcodes = explode(PHP_EOL, $this->request->getPost('groupedCodes'));

        $queryBuilder = $this->modelsManager->createBuilder()
            ->addFrom(WarehouseOrder::class, 'wo')
            ->leftJoin(WarehouseOrderGroupItem::class, 'wo.id = wogi.warehouseOrderId', 'wogi')
            ->where('wogi.id IS NULL')
            ->andWhere('warehouseOrderCode IN ({codes:array})', [
                'codes' => $barcodes
            ]);

        /** @var WarehouseOrder[] $warehouseOrders */
        $warehouseOrders = $queryBuilder->getQuery()->execute();

        $queryBuilder = $this->modelsManager->createBuilder()
            ->addFrom(WarehouseOrder::class, 'wo')
            ->innerJoin(WarehouseOrderGroupItem::class, 'wo.id = wogi.warehouseOrderId', 'wogi')
            ->where('warehouseOrderCode IN ({codes:array})', [
                'codes' => $barcodes
            ]);

        /** @var WarehouseOrder[] $warehouseOrders */
        $usedWarehouseOrders = $queryBuilder->getQuery()->execute();

        $groupedWarehouseOrders = [];
        $entityNames = [];
        foreach ($warehouseOrders as $warehouseOrder) {
            $entityNames[$warehouseOrder->getChannelSupplyEntityId()][$warehouseOrder->getType()] = $warehouseOrder->getChannelSupplyEntity()->getEntity();
            $groupedWarehouseOrders[$warehouseOrder->getChannelSupplyEntityId()][$warehouseOrder->getType()][] = $warehouseOrder;
        }

        $this->view->setVar('groupedWarehouseOrders', $groupedWarehouseOrders);
        $this->view->setVar('entityNames', $entityNames);
        $this->view->setVar('usedWarehouseOrders', $usedWarehouseOrders);
        $this->view->pick('warehouse-order-groups/make-group');
    }

    public function saveAutoGroupAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $channelSupplyEntityId = $this->request->getPost('channelSupplyEntityId');
        $ids = explode(',', $this->request->getPost('warehouseOrderIds'));

        /** @var ChannelSupplyEntity $channelSupplyEntity */
        if (!$channelSupplyEntity = $this->entityManager->findOne(ChannelSupplyEntity::class, $channelSupplyEntityId)) {
            return $this->returnNotFound();
        }

        /** @var WarehouseOrder[] $warehouseOrders */
        $warehouseOrders = $this->entityManager->find(WarehouseOrder::class, [
            WarehouseOrder::PROPERTY_ID . ' IN ({ids:array})',
            'bind' => [
                'ids' => $ids
            ]
        ]);
        $type = $warehouseOrders->getFirst()->getType();

        $warehouseOrderGroup = new WarehouseOrderGroup();
        $warehouseOrderGroup->setChannelSupplyEntity($channelSupplyEntity);
        $warehouseOrderGroup->setStatus(WarehouseOrderGroup::EDITABLE);
        $warehouseOrderGroup->setGrouped(WarehouseOrderGroup::NON_GROUPED);
        $warehouseOrderGroup->setType($type);

        if (!$this->entityManager->save($warehouseOrderGroup)) {
            $this->addFlashMessagesFromEntity($warehouseOrderGroup);

            return false;
        }

        $useCourierService = $this->request->getPost('courier') === 'true';

        foreach ($warehouseOrders as $warehouseOrder) {
            if ($useCourierService) {
                $warehouseOrder->setUseCourierService(true);
                $this->entityManager->save($warehouseOrder);
            }

            $warehouseOrderGroupItem = new WarehouseOrderGroupItem();
            $warehouseOrderGroupItem->setWarehouseOrderGroup($warehouseOrderGroup);
            $warehouseOrderGroupItem->setWarehouseOrder($warehouseOrder);

            if (!$this->entityManager->save($warehouseOrderGroupItem)) {
                $this->addFlashMessagesFromEntity($warehouseOrderGroupItem);

                return false;
            }
        }

        $this->addFlashMessage(
            $this->getDI()->getTranslator()->t('Uspesno snimnjeni podaci.'),
            null,
            MessengerMessage::SUCCESS
        );
    }

    public function getWarehouseOrderAction(string $barcode)
    {
        $this->view->disable();
        /** @var WarehouseOrder $warehouseOrder */
        $warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, [
            WarehouseOrder::PROPERTY_WAREHOUSE_ORDER_CODE . ' = :warehouseOrderCode:',
            'bind' => [
                'warehouseOrderCode' => $barcode
            ]
        ]);

        if (!$warehouseOrder) {
            return $this->returnNotFound();
        }

        return [
            'error' => false,
            'warehouseOrder' => $warehouseOrder->getWarehouseOrderCode(),
            'items' => $warehouseOrder->getProductListFromWarehouseOrder(),
            'key' => sprintf('%s_%s', \Vemid\Helper\TextHelper::stringToCode($warehouseOrder->getChannelSupplyEntity()->getEntity()->getDisplayName()), $warehouseOrder->getType())
        ];
    }

    public function storeItemsAction($id)
    {
        $this->view->disable();
        /** @var WarehouseOrderGroup $warehouseOrderGroup */
        if (!$warehouseOrderGroup = $this->entityManager->findOne(WarehouseOrderGroup::class, $id)) {
            return $this->returnNotFound();
        }


        if (!$warehouseOrderGroup) {
            return $this->returnNotFound();
        }

        $codes = $this->request->getPost('codes');

        foreach ($codes as $code) {
            /** @var WarehouseOrder $warehouseOrder */
            $warehouseOrder = $this->entityManager->findOne(WarehouseOrder::class, [
                WarehouseOrder::PROPERTY_WAREHOUSE_ORDER_CODE . ' = :warehouseOrderCode:',
                'bind' => [
                    'warehouseOrderCode' => $code
                ]
            ]);

            if (!$warehouseOrder) {
                $this->addFlashMessage(
                    'Nepostoji nalog sa trazenim barcodom',
                    null,
                    \Vemid\Messenger\Message::DANGER
                );

                return;
            }

            $warehouseOrderGroupItem = $this->entityManager->findOne(WarehouseOrderGroupItem::class, [
                WarehouseOrderGroupItem::PROPERTY_WAREHOUSE_ORDER_ID . ' = :warehouseOrderId: AND '.
                WarehouseOrderGroupItem::PROPERTY_WAREHOUSE_ORDER_GROUP_ID . ' = :warehouseOrderGroupId:',
                'bind' => [
                    'warehouseOrderId' => $warehouseOrder->getId(),
                    'warehouseOrderGroupId' => $warehouseOrderGroup->getId()
                ]
            ]);

            if (!$warehouseOrderGroupItem) {
                $warehouseOrderGroupItem = new WarehouseOrderGroupItem();
            }

            $warehouseOrderGroupItem->setWarehouseOrder($warehouseOrder);
            $warehouseOrderGroupItem->setWarehouseOrderGroup($warehouseOrderGroup);
            $this->entityManager->save($warehouseOrderGroupItem);
        }

        return ['error' => false, 'url' => '/warehouse-order-groups/list/' . $warehouseOrderGroup->getId()];
    }

    public function setStatusAction($warehouseOrderGroupId)
    {
        $this->view->disable();
        /** @var WarehouseOrderGroup $warehouseOrderGroup */
        $warehouseOrderGroup = $this->entityManager->findOne(WarehouseOrderGroup::class, $warehouseOrderGroupId);

        if (!$warehouseOrderGroup) {
            $this->addFlashMessage(
                'Nepostoji nalog!',
                null,
                \Vemid\Messenger\Message::DANGER
            );

            return;
        }

        $status = $this->request->getPost('status') !== 'false' ? WarehouseOrderGroup::LOCKED : WarehouseOrderGroup::EDITABLE;
        if ($status === WarehouseOrderGroup::LOCKED) {

//            $warehouseOrderMisGroupPusher = $this->getDI()->getMisDocumentGroupPusher();
//            $warehouseOrderGroup->setPushedToMis($warehouseOrderMisGroupPusher($warehouseOrderGroup));
        }

        $warehouseOrderGroup->setStatus($status);
        $warehouseOrderGroup->setLocked(false);
        if (!$this->saveEntity($warehouseOrderGroup)) {
            $this->addFlashMessagesFromEntity($warehouseOrderGroup);
            return;
        }
    }

    public function getListWarehouseOrderGroupsAction()
    {
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');
        $query = $this->modelsManager->createBuilder();
        $query->from(WarehouseOrderGroup::class);
        $query->where('grouped <> :grouped: ', [
            'grouped' => WarehouseOrderGroup::GROUPED
        ]);
        $query->limit($limit, $offset);
        $query->orderBy('id DESC');

        /** @var WarehouseOrderGroup[] $warehouseOrderGroups */
        $warehouseOrderGroups = $query->getQuery()->execute();
        $data = [];
        foreach ($warehouseOrderGroups as $warehouseOrderGroup) {
            $code = [];
            foreach ($warehouseOrderGroup->getWarehouseOrderGroupItems() as $warehouseOrderGroupItem) {
                $warehouseOrder = $warehouseOrderGroupItem->getWarehouseOrder();
                $code[] = $warehouseOrder->getWarehouseOrderCode();
            }
            /** @var WarehouseOrderGroupItem $wogItem */
            $data[] = [
                (string)$warehouseOrderGroup->getChannelSupplyEntity(),
                $warehouseOrderGroup->getType(),
                implode(', ', $code),
                $warehouseOrderGroup->getStatus(),
                $warehouseOrderGroup->getWarehouseOrderGroupCode(),
                $warehouseOrderGroup->getId(),
                $warehouseOrderGroup->getLocked(),
                (bool)$warehouseOrderGroup->getPushedToMis()
            ];
        }

        $warehouseOrdersAll = $this->entityManager->find(WarehouseOrderGroup::class)->count();
        $responseData = [
            'draw' => $page,
            'recordsTotal' => $warehouseOrdersAll,
            'recordsFiltered' => $warehouseOrdersAll,
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getListDataAction()
    {
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');

        $query = $this->modelsManager->createBuilder();
        $query->addFrom(WarehouseOrder::class, 'wo')
            ->leftJoin(ChannelSupplyEntity::class, 'wo.channelSupplyEntityId = cse.id', 'cse');
        $query->where('wo.status = :status:', [
            'status' => WarehouseOrder::LOCKED
        ]);

        if ($this->request->getQuery('channelSupply')) {
            $query->andWhere('cse.channelSupplyId = :channelSupplyId:', [
                'channelSupplyId' => $this->request->getQuery('channelSupply')
            ]);
        }

        if ($this->request->getQuery('entityId')) {
            $query->andWhere('cse.entityId = :entityId:', [
                'entityId' => $this->request->getQuery('entityId')
            ]);
        }

        $query->orderBy('wo.id desc');

        $queryForCount = clone $query;

        /** @var WarehouseOrder[] $warehouseOrders */
        $allWarehouseOrders = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);

        /** @var WarehouseOrder[] $warehouseOrders */
        $warehouseOrders = $query->getQuery()->execute();
        $data = [];
        foreach ($warehouseOrders as $warehouseOrder) {
            $data[] = [
                (string)$warehouseOrder->getChannelSupplyEntity()->getEntity(),
            ];
        }

        $count = $allWarehouseOrders->count();
        $responseData = [
            'draw' => $page,
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $data
        ];

        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function overviewAction($id)
    {
        /** @var WarehouseOrderGroup $warehouseOrderGroup */
        if (!$warehouseOrderGroup = $this->entityManager->findOne(WarehouseOrderGroup::class, $id)) {
            return $this->returnNotFound();
        }
        $warehouseOrders= $warehouseOrderGroup->getWarehouseOrderGroupItems();

        $data = [];
        /** @var WarehouseOrderItem $item */
        foreach ($warehouseOrders as $item) {
            $data[] = [
                'warehouseOrderCode' => $item->getWarehouseOrder()->getWarehouseOrderCode(),
                'entity' => (string)$item->getWarehouseOrder()->getChannelSupplyEntity()->getEntity(),
                'date' => $item->getWarehouseOrder()->getPlanedExitDate(),
                'note' => $item->getWarehouseOrder()->getNote(),
            ];
        }
        $this->view->setVar('data', $data);
        $this->view->setVar('warehouseOrderGroup', $warehouseOrderGroup);
        $this->view->pick('warehouse-order-groups/print');
    }

    public function printAction($id) {
        /** @var WarehouseOrderGroup $warehouseOrderGroup */
        if (!$warehouseOrderGroup = $this->entityManager->findOne(WarehouseOrderGroup::class, $id)) {
            return $this->returnNotFound();
        }
        $warehouseOrders= $warehouseOrderGroup->getWarehouseOrderGroupItems();

        $data = [];
        /** @var WarehouseOrderItem $item */
        foreach ($warehouseOrders as $item) {
            $data[] = [
                'warehouseOrderCode' => $item->getWarehouseOrder()->getWarehouseOrderCode(),
                'entity' => (string)$item->getWarehouseOrder()->getChannelSupplyEntity()->getEntity(),
                'date' => $item->getWarehouseOrder()->getPlanedExitDate(),
                'note' => $item->getWarehouseOrder()->getNote(),
            ];
        }

        $tcpdf = new \TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $html = $this->getDI()->getSimpleView()->render('warehouse-order-groups/print', [
            'warehouseOrderGroup' => $warehouseOrderGroup,
            'data' => $data,
        ]);

        $fileName = sprintf('/tmp/%s.pdf', $warehouseOrderGroup->getWarehouseOrderGroupCode());

        $tcpdf->SetCreator(PDF_CREATOR);
        $tcpdf->SetAuthor('Bebakids');
        $tcpdf->SetTitle('');
        $tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $tcpdf->AddPage();
        $tcpdf->writeHTML($html, true, false, false, false, 'left');
        $tcpdf->Output($fileName, 'F');

        $config = $this->getDI()->getConfig();
        $printerFabricConfig = $config->printers->fabric;

        $manager = new Manager(
            $printerFabricConfig->ipp,
            $printerFabricConfig->username,
            $printerFabricConfig->password
        );
        $manager($fileName);

        $this->addFlashMessage(
            $this->translator->t('Dokument poslat na štampanje'),
            null,
            Message::SUCCESS
        );

        $this->dispatcher->setParam('url', '/warehouse-order-groups/list/');
    }

    public function autoCallCourierAction() {

        #$this->view->disable();

        $postalManager = $this->getDI()->getPostalManager();
        $postalManager();

        $this->addFlashMessage(
            'Uspesno poslat poziv kuririrma',
            null,
            \Vemid\Messenger\Message::SUCCESS
        );

        #$this->dispatcher->setParam('url', '/warehouse-order-groups/list/');
    }

    public function getEntityName()
    {
        return WarehouseOrderGroup::class;
    }
}
