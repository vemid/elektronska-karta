<?php

use Vemid\Controller\CrudController;

/**
 * Class ClientsController
 *
 * @package Default\Controllers
 */
class ClientsController extends CrudController
{
    public function listAction()
    {

        $this->tag::appendTitle($this->translator->t("Pregled Klijenata"));
        $this->view->setVar('clients', $this->entityManager->find(Client::class));
    }

    public function overviewAction($id)
    {
        return parent::overviewAction($id);
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return Client::class;
    }
}
