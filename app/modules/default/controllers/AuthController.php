<?php

use Vemid\Controller\BaseController;
use Vemid\Date\DateTime;
use Vemid\Helper\ImageHelper;
use Vemid\Messenger\Message as MessengerMessage;
use visualCaptcha\Captcha;

/**
 * Class AuthController
 *
 * @package Frontend\Controllers
 */
class AuthController extends BaseController
{

    public function loginAction()
    {
        if ($this->currentUser) {
            return $this->response->redirect();
        }

        $translator = $this->getDI()->getTranslator();
        if ($this->request->isPost()) {

            $user = \User::authenticateUser(
                $this->request->getPost('username'),
                $this->request->getPost('password')
            );

            if ($user) {
                $user->setLastVisitDatetime(new DateTime());
                $user->setLastIp($this->request->getClientAddress());
                $user->save();

                $this->session->set('currentUser', [
                    'id' => $user->getId(),
                    'locale' => 'sr_RS',
                    'language' => $user->getLanguage()
                ]);

                return $this->response->redirect($this->request->getURI());
            } else {

                if ($this->request->isAjax()) {
                    $this->flashSession->error(
                        $translator->t('Your session has expired. Please login again.')
                    );
                } else {
                    $this->flashSession->error(
                        $translator->t('Either your username or password was incorrect. Please try again or reset your password.')
                    );
                }

                return $this->response->redirect('auth/login');
            }
        }

        $this->view->setTemplateAfter('welcome');
        $this->_setTitle($translator->t('Login'));
    }

    /**
     * Forgot password
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    public function resetPasswordAction()
    {
        $translator = $this->getDI()->getTranslator();
        if ($this->request->isPost()) {

            /** @var \User $user */
            $user = $this->entityManager->findOne(\User::class, [
                \User::PROPERTY_USERNAME . ' = :username:',
                'bind' => ['username' => $this->request->getPost(\User::PROPERTY_USERNAME)]
            ]);

            if (!$user) {
                $this->flashSession->error(
                    $translator->t('Wrong E-mail! Please try again.')
                );

                return $this->response->redirect('auth/reset-password');
            }

            $passwordReset = new \PasswordReset();
            $passwordReset->setUser($user);
            $passwordReset->save();

            $this->flashSession->success(
                $translator->t('Instructions how to setup new password have been sent to your E-mail.')
            );

            return $this->response->redirect();
        }

        $this->view->pick('auth/reset-password');
        $this->_setTitle($translator->t('Reset password'));
    }

    /**
     * @param $userId
     * @param $hash
     * @return bool|\Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function changePasswordAction($userId, $hash)
    {
        if ($this->currentUser) {
            return $this->response->redirect();
        }

        /** @var \PasswordReset $passwordReset */
        $passwordReset = $this->entityManager->findOne(\PasswordReset::class, [
            \PasswordReset::PROPERTY_USER_ID . ' = :userId: AND '
            . \PasswordReset::PROPERTY_HASH . ' = :hash: AND '
            . \PasswordReset::PROPERTY_MODIFIED_DATETIME . ' IS NULL',
            'bind' => [
                'userId' => $userId,
                'hash' => urldecode($hash),
            ]
        ]);

        if (!$passwordReset) {
            return $this->returnNotFound();
        }

        $translator = $this->getDI()->getTranslator();
        if ($this->request->isPost()) {
            $newPassword = $this->request->getPost('newPassword');
            $confirmPassword = $this->request->getPost('confirmPassword');

            if (strlen($newPassword) < 8) {
                $this->flashSession->error($translator->t('Your password should be at least %d characters in length.', 8));
            } else {
                if ($newPassword !== $confirmPassword) {
                    $this->flashSession->error($translator->t('Your passwords do not match.'));
                } else {

                    $passwordReset->setModifiedDatetime(new DateTime());
                    $passwordReset->save();

                    if ($user = $passwordReset->getUser()) {
                        $user->setPassword($newPassword);
                        $user->save();

                        $this->getDI()->getFlashSession()->success($translator->t('You have successfully changed your password.'));
                        $this->getDI()->getSession()->set('currentUser', [
                            'id' => $user->getId(),
                            'locale' => 'sr_RS',
                        ]);

                        return $this->response->redirect();
                    }
                }
            }
        }

        $this->view->setVar('user', $passwordReset->getUser());
        $this->_setTitle($translator->t('Change password'));
        $this->view->pick('auth/change-password');
    }

    /**
     * Logout Action
     */
    public function logoutAction()
    {
        $this->session->remove('currentUser');

        $this->flashSession->success(
            $this->getDI()->getTranslator()->t('You have successfully logged out.')
        );

        return $this->response->redirect();
    }

    /**
     * @return null
     */
    public function uploadAvatarAction()
    {
        /** @var \Vemid\Messenger\Manager $messengerManager */
        $messengerManager = $this->getDI()->getNotificationManager();
        $translator = $this->getDI()->getTranslator();
        $messengerManager->appendMessage(
            new MessengerMessage(
                $translator->t('No image provided!'),
                null,
                MessengerMessage::DANGER
            )
        );

        if (!$this->request->isPost()) {
            return null;
        }

        $postData = $this->request->getPost('image');

        if (!$postData) {
            return null;
        }

        $data = explode(',', $postData);
        $image = base64_decode($data[1]);
        $sourceImg = imagecreatefromstring($image);
        $path = $this->currentUser->getUploadPath() . substr(sha1(rand()), 0, 8) . '.png';
        $success = imagepng($sourceImg, $path);
        imagedestroy($sourceImg);

        if ($success && $file = $this->_processAvatar($this->currentUser, $path)) {
            $this->currentUser->setAvatar(basename($file));
            $this->currentUser->save();
            $this->dispatcher->setParam('img', $this->currentUser->getAvatarUrl());

            $messengerManager->clear();
            $messengerManager->appendMessage(
                new MessengerMessage(
                    $translator->t('Personal photo has successfully changed!'),
                    null,
                    MessengerMessage::SUCCESS
                )
            );
        }
    }

    /**
     * @param \User $user
     * @param string $avatar
     * @return null|string
     */
    protected function _processAvatar(\User $user, $avatar)
    {
        $size = getimagesize($avatar);
        $extension = image_type_to_extension($size[2]);
        $file = file_get_contents($avatar);
        $filePath = $user->getUploadPath() . substr(sha1(rand()), 0, 8) . $extension;

        if (file_put_contents($filePath, $file)) {
            $imageHelper = new ImageHelper($filePath);
            $imageHelper->resizeAndCrop(400, 400);
            @unlink($avatar);

            return $filePath;
        }

        return null;
    }

    /**
     * @param int $howMany
     * @return \Phalcon\Http\ResponseInterface
     */
    public function startAction($howMany)
    {
        $this->view->disable();
        $captcha = new Captcha($this->getDI()->get('captchaSession'));
        $captcha->generate($howMany);
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent(json_encode($captcha->getFrontEndData()));

        return $this->response->send();
    }

    /**
     * @param int $index
     * @return bool
     */
    public function imageAction($index)
    {
        $captcha = new Captcha($this->getDI()->get('captchaSession'));
        $streamImage = $captcha->streamImage(
            $this->response->getHeaders()->toArray(),
            $index,
            $this->request->get('retina'));

        if ($streamImage) {
            return false;
        }
    }

    /**
     * @param string $type
     * @return bool
     */
    public function audioAction($type = 'mp3')
    {
        $captcha = new Captcha($this->getDI()->get('captchaSession'));
        if ($captcha->streamAudio($this->response->getHeaders()->toArray(), $type)) {
            return false;
        }
    }
}
