<?php

use Vemid\Controller\CrudController;
use Vemid\Entity\EntityInterface;
use Vemid\Form\Form;
use Vemid\Form\Renderer\Json as Renderer;
use Phalcon\Forms\Element;
use Phalcon\Validation;

/**
 * Class WashingCareLabelController
 *
 * @package Frontend\Controllers
 */
class WashingCareLabelController extends \Vemid\Controller\CrudController
{
    public function listAction()
    {
        $this->tag::appendTitle($this->translator->t("Pregled odrzavanja"));
        $this->view->setVar('washingCareLabels', $this->entityManager->find(WashingCareLabel::class));
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->returnNotFound();
        }

        $form = $this->getForm();
        $postData = $this->request->getPost();

        /** @var WashingCareLabel $washingCareLabel */
        $washingCareLabel = $form->getEntity();

        $messages = [];
        if (count($messages)) {
            $this->addFlashMessage(
                $this->getDI()->getTranslator()->t('File mora biti slika!'),
                WashingCareLabel::PROPERTY_IMAGE,
                \Vemid\Messenger\Message::SUCCESS
            );

            return;
        }

        /** @var Phalcon\Http\Request\File $file */
        $file = array_pop($this->request->getUploadedFiles());

        if ($file !== null) {
            $uploadPath = $washingCareLabel->getUploadPath();

            $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();
            if ($file->moveTo($filePath)) {
                if (is_file($uploadedFile = $uploadPath . $filePath)) {
                    @unlink($uploadedFile);
                }

                $postData[WashingCareLabel::PROPERTY_IMAGE] = basename($filePath);
            }
        }


        if (!$form->isValid($postData)) {
            $this->addFlashMessagesFromForm($form);
            return;
        }

        $form->synchronizeEntityData();

        if (!$this->entityManager->save($washingCareLabel)) {
            $this->addFlashMessagesFromEntity($washingCareLabel);
            return;
        }

        $this->dispatcher->setParam('url', '/washing-care-label/list/' . $form->getEntity()->getEntityId());

        return ['error' => false, 'messages' => []];
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return WashingCareLabel::class;
    }
}