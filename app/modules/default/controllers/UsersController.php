<?php

use Vemid\Controller\CrudController;
use Vemid\Entity\EntityInterface;
use Vemid\Form\Form;
use Vemid\Form\Renderer\Json as Renderer;
use Phalcon\Forms\Element;
use Phalcon\Validation;

/**
 * Class UsersController
 *
 * @package Frontemd\Controllers
 */
class UsersController extends CrudController
{

    public function getCreateFormAction()
    {
        $renderer = new Renderer();

        $form = $this->getForm();

        $roleOptions = [];
        $roles = $this->entityManager->find(Role::class, [
            Role::PROPERTY_CODE . ' != :code:',
            'bind' => [
                'code' => 'GUEST'
            ]
        ]);

        foreach ($roles as $role) {
            $roleOptions[$role->getId()] = $role->getCode();
        }

        $rolesField = new Element\Select('roles[]', $roleOptions);
        $rolesField->setLabel($this->_translate('Rola'));
        $rolesField->setAttribute('multiple', true);

        $form->add($rolesField);

        return $renderer->render($form);
    }

    public function createAction()
    {
        if ($this->request->isPost()) {
            $form = $this->getForm();
            $postData = $this->request->getPost();
            if ($form->isValid($postData)) {


                /** @var User $user */
                $user = $form->getEntity();

                /** @var Phalcon\Http\Request\File $file */
                $file = array_pop($this->request->getUploadedFiles());

                if ($file !== null) {
                    $uploadPath = $user->getUploadPath();

                    $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();
                    if ($file->moveTo($filePath)) {
                        if (is_file($uploadedFile = $uploadPath . $filePath)) {
                            @unlink($uploadedFile);
                        }

                        $postData[User::PROPERTY_AVATAR] = basename($filePath);
                    }
                }

                if (!$form->isValid($postData)) {
                    $this->addFlashMessagesFromForm($form);
                    return;
                }

                $form->synchronizeEntityData();
                $this->saveEntity($user);

                foreach ($postData['roles'] as $roleId) {
                    $userRole = new UserRoleAssignment();
                    $userRole->setUser($user);
                    $userRole->setRoleId($roleId);
                    $this->saveEntity($userRole);
                }
            } else {
                $this->addFlashMessagesFromForm($form);
            }
        }
    }


    public function changePasswordAction()
    {
        $form = $this->getChangePasswordForm();
        $payload = $this->request->getPost();

        if ($form->isValid($payload)) {
            $this->currentUser->setPassword($payload['newPassword']);
            $this->currentUser->save();
        } else {
            $this->addFlashMessagesFromForm($form);
        }
    }

    /**
     * @return string
     */
    public function getChangePasswordFormAction()
    {
        $renderer = new Renderer();
        return $renderer->render($this->getChangePasswordForm());
    }

    /**
     * @return Form
     */
    protected function getChangePasswordForm()
    {
        $form = new Form();

        $element = new Element\Password('newPassword');
        $translator = $this->getDI()->getTranslator();
        $element->setLabel($translator->t('New password'));
        $element->setAttribute('class', 'form-control required');
        $element->addValidators(array(
            new Validation\Validator\PresenceOf(array(
                'message' => $translator->t('The %s is required.', $this->getDI()->getTranslator()->t('New password'))
            )),
            new Validation\Validator\StringLength(array(
                'min' => 6,
                'messageMinimum' => $translator->t('Password must be at least :min characters.')
            ))
        ));
        $form->add($element);

        $element = new Element\Password('confirmPassword');
        $element->setLabel($translator->t('Confirm password'));
        $element->setAttribute('class', 'form-control required');
        $element->addValidators(array(
            new Validation\Validator\PresenceOf(array(
                'message' => $translator->t('The %s is required.', $translator->t('Confirm password'))
            )),
            new Validation\Validator\Confirmation(array(
                'with' => 'newPassword',
                'message' => $translator->t('Passwords do not match.')
            ))
        ));
        $form->add($element);

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteAction($id)
    {
        return $this->returnNotFound();
    }

    public function listAction()
    {
        $this->view->setVar('users', $this->entityManager->find(User::class));
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityName()
    {
        return \User::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getForm(EntityInterface $entity = null, array $exclude = [])
    {
        $exclude = array_merge($exclude, [
            \User::PROPERTY_PASSWORD_HASH,
            \User::PROPERTY_IS_ACTIVE,
        ]);

        return parent::getForm($entity, $exclude);
    }

    /**
     * {@inheritdoc}
     */
    protected function getEntity($params)
    {
        return $this->currentUser;
    }

}
