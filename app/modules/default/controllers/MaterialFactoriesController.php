<?php

use \Vemid\Controller\BaseController;
use \Vemid\Date\DateTime;
use \Vemid\Entity\Repository\MaterialFactoryQuantityRepository;

/***
 * Class MaterialFactoriesController
 */
class MaterialFactoriesController extends BaseController
{
    public function createFormAction($materialSampleId = null)
    {
        $materialSample = null;
        if ($materialSampleId) {
            $materialSample = $this->entityManager->findOne(MaterialSample::class, $materialSampleId);
        }

        $washItems = $this->entityManager->find(WashingCareLabel::class);

        $washData = "";

        $this->view->setVar('washData', str_split($washData));
        $this->view->setVar('washItems', $washItems);
        $this->view->setVar('form', new MaterialFactoryForm(new MaterialFactory, ['materialSample' => $materialSample]));
        $this->view->pick('material-factories/create-form');
    }

    public function editFormAction($id)
    {
        /** @var MaterialFactory $materialFactory */
        $materialFactory = $this->entityManager->findOne(MaterialFactory::class, $id);

        if (!$materialFactory) {
            return $this->returnNotFound();
        }


        $washItems = $this->entityManager->find(WashingCareLabel::class);


        $this->view->setVar('washData', str_split($materialFactory->getWashData()));
        $this->view->setVar('washItems', $washItems);

        $this->view->setVar('form', new MaterialFactoryEditForm(null, ['materialFactory' => $materialFactory]));
        $this->view->setVar('materialFactory', $materialFactory);
        $this->view->pick('material-factories/edit-form');
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var array $postData */
        $postData = $this->request->getPost();
        $materialFactory = new MaterialFactory();

        /** @var Classification $subMaterialCode */
        $subMaterialCode = $this->entityManager->findOne(Classification::class, [
            Classification::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => $postData[MaterialFactory::PROPERTY_SUB_MATERIAL_CODE_ID]
            ]
        ]);

        $postData['subMaterialCodeId'] = $subMaterialCode ? $subMaterialCode->getId() : null;
        $postData[MaterialFactory::PROPERTY_MATERIAL_SAMPLE_ID] = $postData['hiddenSampleId'] ?? ($postData[MaterialFactory::PROPERTY_MATERIAL_SAMPLE_ID] ?: null);
        $postData[MaterialFactory::PROPERTY_IS_EUR] = $postData[MaterialFactory::PROPERTY_IS_EUR] ?? 0;

        /** @var Phalcon\Http\Request\File $file */
        foreach ($this->request->getUploadedFiles() as $file) {
            $property = $file->getKey();

            $uploadPath = $materialFactory->getUploadPath();

            $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();
            if ($file->moveTo($filePath)) {
                if (is_file($imagePath = $uploadPath . $materialFactory->getProperty($property))) {
                    @unlink($imagePath);
                }

                $postData[$property] = basename($filePath);
            }
        }

        /** @var WashingCareLabel $washCareLabels */
        $washCareLabels = $this->entityManager->find(WashingCareLabel::class);
        $washedKeys= [];
        /** @var WashingCareLabel $washCareLabel */
        foreach ($washCareLabels as $washCareLabel) {
            $washedKeys[$washCareLabel->getId()]=$washCareLabel->getPrintSign();
        }

        $stringWash = "";

        if($postData['washed'] ) {
            foreach ($postData['washed'] as $washData) {
                if (array_key_exists($washData, $washedKeys)) {
                    $stringWash .= $washedKeys[$washData];
                }
            }
        }

        $form = new MaterialFactoryForm($materialFactory);
        if ($form->isValid($postData)) {
            $materialFactory->setWashData($stringWash);
            $materialFactory->setCreated(new DateTime());
            if (!$this->saveEntity($materialFactory)) {
                $this->addFlashMessagesFromEntity($materialFactory);

                return;
            }

            if (!empty($postData['qty'])) {
                $materialFactoryQty = new MaterialFactoryQuantity();
                $materialFactoryQty->setMaterialFactory($materialFactory);
                $materialFactoryQty->setSeasonClassificationId($materialFactory->getSeasonClassificationId());
                $materialFactoryQty->setAmount($postData['qty']);
                $materialFactoryQty->setType(MaterialFactoryQuantity::INCOME);
                $materialFactoryQty->setWarrant('a');
                $materialFactoryQty->setDatetime(new DateTime());
                $materialFactoryQty->setNote('Inicijalni unos prilikom kreiranja kartice');

                $this->saveEntity($materialFactoryQty);
            }

            return $this->response->redirect('material-factories/list');
        }

        $this->addFlashMessagesFromForm($form);
        return;
    }

    public function updateAction($id)
    {
        if (!$this->request->isPost()) {
            $this->returnNotFound();
        }

        /** @var array $postData */
        $postData = $this->request->getPost();

        /** @var MaterialFactory $materialFactory */
        $materialFactory = $this->entityManager->findOne(MaterialFactory::class, $id);
        if (!$materialFactory) {
            $this->returnNotFound();
        }

        //$postData[MaterialFactory::PROPERTY_SUPPLIER_CODE_ID] = $materialFactory->getSupplierCodeId();
        $postData[MaterialFactory::PROPERTY_MATERIAL_SAMPLE_ID] = $materialFactory->getMaterialSampleId();
        $postData[MaterialFactory::PROPERTY_IS_EUR] = $postData[MaterialFactory::PROPERTY_IS_EUR] ?? 0;

        /** @var Phalcon\Http\Request\File $file */
        foreach ($this->request->getUploadedFiles() as $file) {
            $property = $file->getKey();

            $uploadPath = $materialFactory->getUploadPath();

            $filePath = $uploadPath . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();
            if ($file->moveTo($filePath)) {
                if (is_file($imagePath = $uploadPath . $materialFactory->getProperty($property))) {
                    @unlink($imagePath);
                }

                $postData[$property] = basename($filePath);
            }
        }

        /** @var WashingCareLabel $washCareLabels */
        $washCareLabels = $this->entityManager->find(WashingCareLabel::class);
        $washedKeys= [];
        /** @var WashingCareLabel $washCareLabel */
        foreach ($washCareLabels as $washCareLabel) {
            $washedKeys[$washCareLabel->getId()]=$washCareLabel->getPrintSign();
        }

        $stringWash = "";

        if($postData['washed']) {
            foreach ($postData['washed'] as $washData) {
                if (array_key_exists($washData, $washedKeys)) {
                    $stringWash .= $washedKeys[$washData];
                }
            }
        }

        /** @var Classification $subMaterialCode */
        $subMaterialCode = $this->entityManager->findOne(Classification::class, [
            'code = :code:',
            'bind' => [
                'code' => $postData[MaterialFactory::PROPERTY_SUB_MATERIAL_CODE_ID]
            ]
        ]);

        $postData[MaterialFactory::PROPERTY_SUB_MATERIAL_CODE_ID] = $subMaterialCode ? $subMaterialCode->getId() : null;

        $form = new MaterialFactoryForm($materialFactory);
        if ($form->isValid($postData)) {
            $materialFactory->setWashData($stringWash);
            $this->saveEntity($materialFactory);
        }

        return $this->response->redirect('material-factories/list');
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityName()
    {
        return MaterialSamples::class;
    }

    public function getListDataAction()
    {
        $this->view->disable();

        $page = $this->request->getQuery('draw', 'int', 1);
        $offset = $this->request->getQuery('start');
        $limit = $this->request->getQuery('length');
        $search = $this->request->getQuery('search');


        $typeMaterial = $this->request->getQuery('typeMaterial', 'string', 0);

        $query = $this->modelsManager->createBuilder();
        $query->addFrom(MaterialFactory::class, 'ms')
            ->leftJoin(Classification::class, 'subClass.id = ms.subMaterialCodeId', 'subClass')
            ->leftJoin(Classification::class, 'subParentClass.id = subClass.parentClassificationId', 'subParentClass')
            ->leftJoin(Classification::class, 'subParentClass2.id = subParentClass.parentClassificationId', 'subParentClass2');

        if ($typeMaterial) {
            $query->leftJoin(Classification::class, 'cls.id = ms.subMaterialCodeId', 'cls')
                ->leftJoin(Classification::class, 'cls1.id = cls.parentClassificationId', 'cls1')
                ->leftJoin(Classification::class, 'cls2.id = cls1.parentClassificationId', 'cls2')
                ->andWhere('cls2.code = :code:', [
                    'code' => $typeMaterial
                ]);
        }

        if (is_array($search) && $search['value']) {
            $pieces = explode("-", $search['value']);
            if(count($pieces)>1) {


                if ($pieces[0] == 'k') {
                    $query->andWhere('ms.cardNumber = :search:', [
                        'search' => $pieces[1],
                    ]);
                }
                elseif ($pieces[0] == 'm') {
                    $query->andWhere('ms.fabricCode = :search:', [
                        'search' => $pieces[1],
                    ]);
                }
            }
            else {
                $query->andWhere('ms.fabricCode = :search: OR ms.cardNumber = :search: OR subParentClass2.name LIKE :search2: OR ms.color = :search: OR subParentClass.name LIKE :search2: OR subClass.name LIKE :search2:', [
                    'search' => $search['value'],
                    'search2' => '%' . $search['value'] . '%'
                ]);
            }
        }

        $query->orderBy('ms.id desc');

        $queryForCount = clone $query;
        $allMaterialFactories = $queryForCount->getQuery()->execute();

        $query->limit($limit, $offset);


        /** @var MaterialFactory[] $materialFactories */
        $materialFactories = $query->getQuery()->execute();
        /** @var MaterialFactoryQuantityRepository $materialFactoryQuantityRepository */
        $materialFactoryQuantityRepository = $this->entityManager->getRepository(MaterialFactoryQuantity::class);

        $data = [];
        foreach ($materialFactories as $materialFactory) {
            $name = '';
            $subName = '';
            if ($classification = $materialFactory->getSubMaterialClassification()) {
                if ($parentClassification = $classification->getParentClassification()) {
                    if ($parentOfParentClassification = $parentClassification->getParentClassification()) {
                        $name = $parentClassification->getName();
                        $subName = $classification->getName();
                    } else {
                        $name = $classification->getName();
                    }
                }
            }

            $data[] = [
                $materialFactory->getCardNumber(),
                $name,
                $subName,
                $materialFactory->getColor(),
                $materialFactory->getWeight(),
                $materialFactory->getHeight(),
                $materialFactory->getSupplierName() ? $materialFactory->getSupplierName()->getName() : '',
                $materialFactory->getFabricCode(),
                $materialFactoryQuantityRepository->getTotal($materialFactory),
                $materialFactory->getCosts(),
                number_format($materialFactoryQuantityRepository->getTotal($materialFactory)*$materialFactory->getCosts(),1),
                $materialFactory->getId(),

            ];
        }

        $responseData = [
            'draw' => $page,
            'recordsTotal' => $allMaterialFactories->count(),
            'recordsFiltered' => $allMaterialFactories->count(),
            'data' => $data
        ];


        $this->response->setContent(json_encode($responseData));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function listAction()
    {
        $this->tag::appendTitle($this->translator->t("Kartice Materijala"));
        /** @var \CodeType $codeType */
        $codeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        /** @var \CodeType $codeTypeType */
        $codeTypeType = $this->getDI()->getEntityManager()->findOne(\CodeType::class, [
            \CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => \CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        if (!$codeType || !$codeTypeType) {
            return $this->returnNotFound();
        }

        /** @var \Code $codeCategory */
        $codeCategory = $this->entityManager->findOne(\Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '1',
                'codeTypeId' => $codeType->getId()
            ]
        ]);

        /** @var \Code $code */
        $code = $this->entityManager->findOne(\Code::class, [
            Code::PROPERTY_CODE . ' = :code: AND ' .
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'code' => '00',
                'codeTypeId' => $codeTypeType->getId()
            ]
        ]);


        if (!$code || !$codeCategory) {
            return $this->returnNotFound();
        }

        $classifications = $codeCategory->getClassificationCategories([
            Classification::PROPERTY_PARENT_CLASSIFICATION_ID . ' IS NULL AND ' .
            Classification::PROPERTY_CLASSIFICATION_TYPE_CODE_ID . ' = :codeId:',
            'bind' => [
                'codeId' => $code->getId(),
            ]
        ]);

        $options = [];
        foreach ($classifications as $classification) {
            $options[$classification->getCode()] = $classification->getName();
        }

        $codes = $this->entityManager->find(Code::class, [
            Code::PROPERTY_CODE_TYPE_ID . ' = :codeTypeId:',
            'bind' => [
                'codeTypeId' => $codeType->getId()
            ]
        ]);


        $this->view->setVar('productCodeId', $codes);
        $this->view->setVar('options', $options);
    }

    public function overviewAction($id)
    {
        /** @var \MaterialFactory $materialFactory */
        $materialFactory = $this->entityManager->findOne(MaterialFactory::class, $id);
        if (!$materialFactory) {
            $this->returnNotFound();
        }

        /** @var MaterialFactoryQuantityRepository $materialFactoryQuantityRepository */
        $materialFactoryQuantityRepository = $this->entityManager->getRepository(MaterialFactoryQuantity::class);

        $washItems = $this->entityManager->find(WashingCareLabel::class);
        $careLabel = new Vemid\Service\MaterialFactory\MaterialFactoryCareLabel($this->entityManager);
        $this->view->setVar('careLabel', $careLabel);

        $this->view->setVar('materialFactory', $materialFactory);
        $this->view->setVar('washItems', $washItems);
        $this->view->setVar('washData', str_split($materialFactory->getWashData()));
        $this->view->setVar('materialFactoryQuantityIncome', $materialFactoryQuantityRepository->getTotalIn($materialFactory));
        $this->view->setVar('materialFactoryQuantityOutcome', $materialFactoryQuantityRepository->getTotalOut($materialFactory));
        $this->view->setVar('materialFactoryTotalQty', $materialFactoryQuantityRepository->getTotal($materialFactory));
        $this->view->pick('material-factories/overview');
        $this->tag::appendTitle($this->translator->t("Kartica : ").$materialFactory->getCardNumber());
    }

    public function getFactoriesAction()
    {
        $this->view->disable();
        $json = [];
        $term = $this->request->getQuery('term');

        $query = $this->modelsManager->createBuilder();
        $query->addFrom(MaterialFactory::class, 'mf')
            ->leftJoin(MaterialSample::class, 'ms.id = mf.materialSampleId', 'ms')
            ->where('mf.fabricCode LIKE :term: OR mf.misCode LIKE :term: OR ms.name LIKE :term:', [
                'term' => "%$term%"
            ]);

        /** @var MaterialFactory[] $materialFactories */
        $materialFactories = $query->getQuery()->execute();

        foreach ($materialFactories as $materialFactory) {
            $id = $materialFactory->getId();
            $json[$id]['id'] = $id;
            $json[$id]['value'] = $materialFactory->getMaterialSample() ? $materialFactory->getMaterialSample()->getName() : $materialFactory->getFabricCode();
            $json[$id]['label'] = $materialFactory->getMaterialSample() ? $materialFactory->getMaterialSample()->getName() . ' (' . $materialFactory->getFabricCode() . ')' : $materialFactory->getFabricCode();
        }

        $this->response->setContent(json_encode($json));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    public function getTypeCodeIdAction($id)
    {
        $this->view->disable();

        /** @var \MaterialFactory $materialFactory */
        if (!$materialFactory = $this->entityManager->findOne(MaterialFactory::class, $id)) {
            $this->returnNotFound();
        }

        $subMaterialCodeClassification = $materialFactory->getSubMaterialClassification();
        $materialCodeClassification = $subMaterialCodeClassification ? $subMaterialCodeClassification->getParentClassification() : null;
        $materialTypeCodeClassificationId = $materialCodeClassification ? $materialCodeClassification->getParentClassification()->getCode() : null;

        $response = [
            'code' => $materialTypeCodeClassificationId,
            'childCode' => $materialCodeClassification ? $materialCodeClassification->getCode() : null,
            'subCode' => $subMaterialCodeClassification ? $subMaterialCodeClassification->getCode() : null,
        ];
        $this->response->setContent(json_encode($response));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }

    /**
     * {@inheritdoc}
     */
    public function deleteAction($id)
    {
        /** @var MaterialFactory $materialFactory */
        if (!$materialFactory = $this->entityManager->findOne(MaterialFactory::class, $id)) {
            return $this->returnNotFound();
        }

        if (!$materialFactory->isDeletable()) {
            $this->addFlashMessage(
                $this->_translate('It is not allowed to delete this record.'),
                null,
                \Vemid\Messenger\Message::WARNING
            );
        } else {
            $this->entityManager->delete($materialFactory);

            $this->addFlashMessage(
                $this->_translate('Kartica materijala uspešno obrisana!'),
                null,
                \Vemid\Messenger\Message::SUCCESS
            );
        }
    }

    public function downloadAction($id)
    {
        /** @var MaterialFactory $materialFactory */
        if (!$materialFactory = $this->entityManager->findOne(MaterialFactory::class, $id)) {
            return $this->returnNotFound();
        }

        $filePath = $materialFactory->getUploadPath() . $materialFactory->getInvoiceScan();
        $contentType = mime_content_type($filePath);
        $ext = pathinfo($filePath, PATHINFO_EXTENSION);

        header('Content-Description: File Transfer');
        header('Content-type: ' . $contentType);
        header('Content-Disposition: attachment; filename="' . $materialFactory->getInvoiceScan() .'"');
        header('Expires: 0');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filePath));

        echo readfile($filePath);
        exit;
    }

    public function findByTermAction()
    {
        $this->view->disable();
        $json = [];
        $term = $this->request->getQuery('term');

        $query = $this->modelsManager->createBuilder()
            ->addFrom(MaterialFactory::class, 'mf')
            ->leftJoin(Classification::class, 'c.id = mf.subMaterialCodeId', 'c')
            ->where('mf.cardNumber LIKE :cardNumber:', [
                'cardNumber' => "$term%"
            ])
            ->orWhere('concat(c.name," ",mf.color," ",mf.height) LIKE :name:', [
                'name' =>  "$term%"
            ])
        ->orderBy('mf.color ASC,mf.height ASC');

        /** @var MaterialFactory[] $materialFactories */
        $materialFactories = $query->getQuery()->execute();

        foreach ($materialFactories as $materialFactory) {
            $id = $materialFactory->getId();

            $json[$id]['id'] = $materialFactory->getId();
            $json[$id]['value'] = $materialFactory->getDisplayName();
            $json[$id]['label'] = $materialFactory->getDisplayName();
            $json[$id]['costs'] = $materialFactory->getCosts();
        }

        $this->response->setContent(json_encode($json));
        $this->response->setContentType('application/json', 'utf-8');
        $this->response->send();
    }
}
