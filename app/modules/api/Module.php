<?php

namespace Api;

use Vemid\Ajax\Handler as AjaxHandler;
use Vemid\Entity\Manager\EntityManagerInterface;
use Vemid\Error\HandlerInterface;
use Vemid\Helper\SiteHelper;
use Vemid\Http\BasicAuthentification;
use Vemid\Messenger\Manager as MessengerManager;
use Vemid\Messenger\Message as MessengerMessage;
use Phalcon\DiInterface;
use Phalcon\Events\Event;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Loader;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Mvc\View;

/**
 * Class Backend Module
 *
 * @package Backend
 */
class Module implements ModuleDefinitionInterface
{

    /**
     * Registers an autoloader related to the module
     *
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new Loader();
        $loader->registerNamespaces(array(
            'Api\Controllers' => __DIR__ . '/controllers/'
        ));
        $loader->registerDirs(array(
            __DIR__ . '/controllers/',
        ));
        $loader->register();
    }

    /**
     * Registers an autoloader related to the module
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di)
    {
        // Init dispatcher
        $this->_initDispatcher($di);
    }

    /**
     * @param DiInterface $di
     */
    protected function _initDispatcher(DiInterface $di)
    {
        /** @var EventsManager $eventsManager */
        $eventsManager = $di->getShared('eventsManager');

        $eventsManager->attach('dispatch:beforeExecuteRoute',
            function (Event $event, Dispatcher $dispatcher) use ($di) {
                $request = $dispatcher->getDI()->getRequest();
                $response = $dispatcher->getDI()->getResponse();
                $config = $dispatcher->getDI()->getConfig();

                return (new BasicAuthentification())($request, $response, $config);
            }
        );

        $eventsManager->attach('dispatch:beforeException',
            function (Event $event, Dispatcher $dispatcher, \Exception $exception) use ($di) {

                /** @var MessengerManager $messengerManager */
                $messengerManager = $di->getShared('notificationManager');
                $messengerManager->appendMessage(new MessengerMessage(
                    $exception->getMessage(),
                    null,
                    MessengerMessage::DANGER
                ));
            }
        );

        $eventsManager->attach('dispatch:afterDispatchLoop', function (Event $event, Dispatcher $dispatcher) {
            $ajaxHandler = new AjaxHandler();
            $ajaxHandler->handle($dispatcher);
        });

        /** @var Dispatcher $dispatcher */
        $dispatcher = $di->getShared('dispatcher');
        $dispatcher->setDefaultNamespace('Api\Controllers');
        $dispatcher->setEventsManager($eventsManager);
    }
}
