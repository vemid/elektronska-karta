<?php

namespace Api\Controllers;

use Phalcon\Logger\Adapter\File;
use Phalcon\Mvc\Controller;
use Vemid\Application\Di;
use Vemid\Date\DateTime;
use Vemid\Error\Logger\FileLogger;
use Vemid\Services\Printer\DocumentPrinter;

/**
 * Class BaseController
 *
 * @method Di getDI()
 *
 * @property \Phalcon\Translate\AdapterInterface $translator
 * @property \Vemid\Messenger\Manager $messengerManager
 * @property \Vemid\Entity\Manager\EntityManagerInterface $entityManager
 * @property DocumentPrinter $documentPrinter
 * @property \User $currentUser
 *
 * @package Controller
 * @author Vemid
 */
class CourierServiceController extends Controller
{
    public function notifyAction()
    {
        $rawBody = $this->request->getRawBody();
        $postData = json_decode($rawBody, true);

        $logger = $this->getDI()->getCourierLogger();
        $logger->debug($rawBody);

        if (count($postData) === 0) {
            throw new \LogicException('Post was empty!');
        }

        $existingEvent = $this->entityManager->findOne(\ShipmentStatus::class, [
            \ShipmentStatus::PROPERTY_REFERENCE_ID . ' = :referenceId: AND ' .
            \ShipmentStatus::PROPERTY_EVENT_ID . ' = :eventId:',
            'bind' => [
                'referenceId' => $postData['rID'],
                'eventId' => $postData['nID']
            ]
        ]);

        $statuses = \ShipmentStatus::getStatuses();

        if ($existingEvent || empty($postData['rID'])) {
            echo $this->response->getContent();
            exit;
        }


        $shipmentStatus = $this->entityManager->findOne(\ShipmentStatus::class, [
            \ShipmentStatus::PROPERTY_REFERENCE_ID . ' = :referenceId:',
            'bind' => [
                'referenceId' => $postData['rID'],
            ]
        ]);

        if (!$shipmentStatus) {
            $shipmentStatus = new \ShipmentStatus();
            $shipmentStatus->setReferenceId($postData['rID']);
        }

        $shipmentStatus->setStatus($statuses[$postData['sID']]);
        $shipmentStatus->setEventId($postData['nID']);
        $shipmentStatus->setCode($postData['code']);
        $eventDate = DateTime::createFromFormat('YmdHis', $postData['dt']);
        $shipmentStatus->setEventDate(new DateTime($eventDate->format('Y-m-d H:i:s')));

        if ($shipmentStatus->save()) {
            echo $this->response->getContent();
            exit;
        }

        $this->response->setContent('Failed to process shipment status');
        http_response_code(500);
        echo $this->response->getContent();
        exit;
    }
}
