<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * ProductCalculation Model Class
 *
 * @Source('product_calculations')
 *
 * @BelongsTo('productId', 'Product', 'id', {'alias':'Product'})
 * @BelongsTo('calculationMappingId', 'CalculationMapping', 'id', {'alias':'CalculationMapping'})
 *
 * @HasMany('id', 'ProductCalculationItem', 'productCalculationId', {'alias':'ProductCalculationItems'})
 */
class ProductCalculation extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PRODUCT_ID = 'productId';
    const PROPERTY_CALCULATION_MAPPING_ID = 'calculationMappingId';
    const PROPERTY_STATUS = 'status';
    const PROPERTY_CREATED = 'created';
    const PROPERTY_COMPLETED = 'completed';
    const PROPERTY_QTY = 'qty';
    const PROPERTY_COMPOSITION = 'composition';
    const PROPERTY_ORIGIN = 'origin';
    const PROPERTY_IS_SYNCED = 'isSynced';


    const STATUS_PLANED = 'PLANED';
    const STATUS_FINAL = 'FINAL';

    const ALIAS_PRODUCT = 'Product';
    const ALIAS_CALCULATION_MAPPING = 'CalculationMapping';
    const ALIAS_PRODUCT_CALCULATION_ITEMS = 'ProductCalculationItems';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="product_id", type="integer", nullable=false)
     * @FormElement(label="Product", type="Hidden", required=true)
     */
    protected $productId;

    /**
     * @Column(column="calculation_mapping_id", type="integer", nullable=false)
     */
    protected $calculationMappingId;

    /**
     * @Column(column="status", type="string", nullable=true)
     * @FormElement(label="Status", type="Select", required=false, options={'PLANED' : 'Planska', 'FINAL' : 'Finalna'})
     */
    protected $status;

    /**
     * @Column(column="completed", type="bool", nullable=true)
     */
    protected $completed;

    /**
     * @Column(column="qty", type="integer", nullable=true)
     */
    protected $qty;

    /**
     * @Column(column="composition", type="string", nullable=true)

     */
    protected $composition;

    /**
     * @Column(column="origin", type="string", nullable=true)

     */
    protected $origin;

    /**
     * @Column(column="is_synced", type="bool", nullable=true)
     */
    protected $isSynced;

    /**
     * @Column(column="created", type="datetime", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRODUCT_ID => true,
        self::PROPERTY_STATUS => true,
        self::PROPERTY_CREATED => true,
        self::PROPERTY_QTY => true,
        self::PROPERTY_COMPOSITION => true,
        self::ALIAS_PRODUCT => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->productId = null !== $productId ? (int)$productId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getCalculationMappingId()
    {
        return $this->calculationMappingId;
    }

    /**
     * @param int $calculationMappingId
     * @return $this
     */
    public function setCalculationMappingId($calculationMappingId)
    {
        $this->calculationMappingId = null !== $calculationMappingId ? (int)$calculationMappingId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * @param boolean $completed
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param int $qty
     * @return $this
     */
    public function setQty($qty)
    {
        $this->qty = null !== $qty ? (int)$qty : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getComposition()
    {
        return $this->composition;
    }

    /**
     * @param string $composition
     * @return $this
     */
    public function setComposition($composition)
    {
        $this->composition = $composition;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @param string $origin
     * @return $this
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSynced()
    {
        return $this->isSynced;
    }

    /**
     * @param boolean $isSynced
     */
    public function setIsSynced($isSynced)
    {
        $this->isSynced = $isSynced;
    }


    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|Product|\Vemid\Entity\EntityInterface
     * 
     * @throws \DomainException
     */
    public function getProduct()
    {
        return $this->getRelated(self::ALIAS_PRODUCT);
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->productId = $product->getId();

        return $this;
    }

    /**
     * @return null|CalculationMapping|\Vemid\Entity\EntityInterface
     *
     * @throws \DomainException
     */
    public function getCalculationMapping()
    {
        return $this->getRelated(self::ALIAS_CALCULATION_MAPPING);
    }

    /**
     * @param CalculationMapping $calculationMapping
     * @return $this
     */
    public function setCalculationMapping(CalculationMapping $calculationMapping)
    {
        $this->calculationMappingId = $calculationMapping->getId();

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|ProductCalculationItem[]
     * 
     * @throws \DomainException
     */
    public function getProductCalculationItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRODUCT_CALCULATION_ITEMS, $arguments);
    }

    public function beforeCreate()
    {
        $this->isSynced = 0;
        $this->completed = 0;
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
