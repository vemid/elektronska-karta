<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * AbsenceWork Model Class
 *
 * @Source('absence_work')
 *
 * @BelongsTo('productionWorkerId', 'ProductionWorker', 'id', {'alias':'ProductionWorker'})
 *
 */
class AbsenceWork extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PRODUCTION_WORKER_ID = 'productionWorkerId';
    const PROPERTY_START_DATE = 'startDate';
    const PROPERTY_END_DATE = 'endDate';
    const PROPERTY_TIME = 'time';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_ON_SALARY = 'onSalary';
    const PROPERTY_CREATED = 'created';

    const TYPE_PARTIAL = 'PARTIAL';
    const TYPE_PAID = 'PAID';
    const TYPE_SICK = 'SICK';
    const TYPE_VACATION = 'VACATION';

    const ALIAS_PRODUCTION_WORKER = 'ProductionWorker';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="production_worker_id", type="integer", nullable=false)
     * @FormElement(label="Production worker", type="Select", required=true, relation="ProductionWorker")
     */
    protected $productionWorkerId;

    /**
     * @Column(column="start_date", type="date", nullable=false)
     * @FormElement(label="Start Date", type="Date", required=true)
     */
    protected $startDate;

    /**
     * @Column(column="end_date", type="date", nullable=false)
     * @FormElement(label="End Date", type="Date", required=true)
     */
    protected $endDate;

    /**
     * @Column(column="time", type="string", nullable=true)
     * @FormElement(label="Time", type="Text", required=true)
     */
    protected $time;

    /**
     * @Column(column="type", type="string", nullable=true)
     * @FormElement(label="Vrsta odsustva", type="Select", required=false, options={'VACATION':'Odmor', 'SICK':'Bolovanje', 'PAID':'Placeno odsustvo','PARTIAL':'Privremeno odsustvo'})

     */
    protected $type;

    /**
     * @Column(column="on_salary", type="bool", nullable=true)
     * @FormElement(label="On salary", type="Check", required=false)
     */
    protected $onSalary;

    /**
     * @Column(column="created", type="string", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRODUCTION_WORKER_ID => true,
        self::PROPERTY_START_DATE => true,
        self::PROPERTY_END_DATE => true,
        self::PROPERTY_TIME => true,
        self::PROPERTY_TYPE => true,
        self::PROPERTY_CREATED => true,
        self::ALIAS_PRODUCTION_WORKER => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductionWorkerId()
    {
        return $this->productionWorkerId;
    }

    /**
     * @param int $productionWorkerId
     * @return $this
     */
    public function setProductionWorkerId($productionWorkerId)
    {
        $this->productionWorkerId = null !== $productionWorkerId ? (int)$productionWorkerId : null;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param string|DateTime $startDate
     * @return $this
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param string|DateTime $endDate
     * @return $this
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }


    /**
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param string $time
     * @return $this
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getOnSalary()
    {
        return $this->onSalary;
    }

    /**
     * @param boolean $onSalary
     */
    public function setOnSalary($onSalary)
    {
        $this->onSalary = $onSalary;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|ProductionWorker
     * 
     * @throws \DomainException
     */
    public function getProductionWorker()
    {
        return $this->getRelated(self::ALIAS_PRODUCTION_WORKER);
    }

    /**
     * @param ProductionWorker $productionWorker
     * @return $this
     */
    public function setProductionWorker(ProductionWorker $productionWorker)
    {
        $this->productionWorkerId = $productionWorker->getId();

        return $this;
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
