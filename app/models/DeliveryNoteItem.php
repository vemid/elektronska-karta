<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * DeliveryNote Model Class
 *
 * @Source('delivery_note_items')
 *
 * @BelongsTo('deliveryNoteId', 'DeliveryNote', 'id', {'alias':'DeliveryNote'})
 * @BelongsTo('orderItemId', 'OrderItem', 'id', {'alias':'OrderItem'})
 */
class DeliveryNoteItem extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_DELIVERY_NOTE_ID = 'deliveryNoteId';
    const PROPERTY_ORDER_ITEM_ID = 'orderItemId';
    const PROPERTY_PURCHASE_PRICE = 'purchasePrice';
    const PROPERTY_QUANTITY_A = 'quantityA';
    const PROPERTY_QUANTITY_B = 'quantityB';
    const PROPERTY_CREATED = 'created';

    const ALIAS_DELIVERY_NOTE = 'DeliveryNote';
    const ALIAS_ORDER_ITEM = 'OrderItem';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="delivery_note_id", type="integer", nullable=false)
     * @FormElement(label="Delivery Note", type="Hidden", required=true)
     */
    protected $deliveryNoteId;

    /**
     * @Column(column="order_item_id", type="integer", nullable=false)
     * @FormElement(label="Order item", type="Hidden", required=true)
     */
    protected $orderItemId;

    /**
     * @Column(column="purchase_price", type="decimal", nullable=true)
     */
    protected $purchasePrice;


    /**
     * @Column(column="quantity_a", type="integer", nullable=true)
     * @FormElement(label="Quantity a", type="Text", required=false)
     */
    protected $quantityA;

    /**
     * @Column(column="quantity_b", type="integer", nullable=true)
     * @FormElement(label="Quantity b", type="Text", required=false)
     */
    protected $quantityB;

    /**
     * @Column(column="is_synced", type="bool", nullable=true)
     */
    protected $isSynced;

    /**
     * @Column(column="created", type="string", nullable=false)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_DELIVERY_NOTE_ID => true,
        self::PROPERTY_ORDER_ITEM_ID => true,
        self::PROPERTY_QUANTITY_A => true,
        self::PROPERTY_QUANTITY_B => true,
        self::PROPERTY_CREATED => true,
        self::ALIAS_ORDER_ITEM => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getDeliveryNoteId()
    {
        return $this->deliveryNoteId;
    }

    /**
     * @param int $deliveryNoteId
     * @return $this
     */
    public function setDeliveryNoteId($deliveryNoteId)
    {
        $this->deliveryNoteId = $deliveryNoteId;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderItemId()
    {
        return $this->orderItemId;
    }

    /**
     * @param int $orderItemId
     * @return $this
     */
    public function setOrderItemId($orderItemId)
    {
        $this->orderItemId = null !== $orderItemId ? (int)$orderItemId : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * @param float $purchasePrice
     * @return $this
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = null !== $purchasePrice ? (float)$purchasePrice : null;

        return $this;
    }


    /**
     * @return int
     */
    public function getQuantityA()
    {
        return $this->quantityA;
    }

    /**
     * @param int $quantityA
     * @return $this
     */
    public function setQuantityA($quantityA)
    {
        $this->quantityA = null !== $quantityA ? (int)$quantityA : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantityB()
    {
        return $this->quantityB;
    }

    /**
     * @param int $quantityB
     * @return $this
     */
    public function setQuantityB($quantityB)
    {
        $this->quantityB = null !== $quantityB ? (int)$quantityB : null;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSynced()
    {
        return $this->isSynced;
    }

    /**
     * @param boolean $isSynced
     */
    public function setIsSynced($isSynced)
    {
        $this->isSynced = $isSynced;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|DeliveryNote
     * 
     * @throws \DomainException
     */
    public function getDeliveryNote()
    {
        return $this->getRelated(self::ALIAS_DELIVERY_NOTE);
    }

    /**
     * @param DeliveryNote $deliveryNote
     * @return $this
     */
    public function setDeliveryNote(DeliveryNote $deliveryNote)
    {
        $this->deliveryNoteId = $deliveryNote->getId();

        return $this;
    }

    /**
     * @return null|OrderItem
     *
     * @throws \DomainException
     */
    public function getOrderItem()
    {
        return $this->getRelated(self::ALIAS_ORDER_ITEM);
    }

    /**
     * @param OrderItem $orderItem
     * @return $this
     */
    public function setOrderItem(OrderItem $orderItem)
    {
        $this->orderItemId = $orderItem->getId();

        return $this;
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
        $this->isSynced = false;
    }

}
