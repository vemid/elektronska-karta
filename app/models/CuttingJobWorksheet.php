<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * CuttingJobWorksheet Model Class
 *
 * @Source('cutting_job_worksheets')
 *
 * @BelongsTo('cuttingJobId', 'CuttingJob', 'id', {'alias':'CuttingJob'})
 * @BelongsTo('productionWorkerId', 'ProductionWorker', 'id', {'alias':'ProductionWorker'})
 * @BelongsTo('managerId', 'ProductionWorker', 'id', {'alias':'ProductionManager'})
 */
class CuttingJobWorksheet extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CUTTING_JOB_ID = 'cuttingJobId';
    const PROPERTY_PRODUCTION_MANAGER_ID = 'productionManagerId';
    const PROPERTY_PRODUCTION_WORKER_ID = 'productionWorkerId';
    const PROPERTY_QTY = 'qty';
    const PROPERTY_DATE = 'date';
    const PROPERTY_START_TIME = 'startTime';
    const PROPERTY_END_TIME = 'endTime';
    const PROPERTY_CREATED = 'created';

    const ALIAS_CUTTING_JOB = 'CuttingJob';
    const ALIAS_PRODUCTION_WORKER = 'ProductionWorker';
    const ALIAS_PRODUCTION_MANAGER = 'ProductionManager';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="cutting_job_id", type="integer", nullable=false)
     * @FormElement(label="Cutting job", type="Select", required=true, relation="CuttingJob")
     */
    protected $cuttingJobId;

    /**
     * @Column(column="manager_id", type="integer", nullable=false)
     * @FormElement(label="Production manager", type="Select", required=true, relation="ProductionWorker")
     */
    protected $productionManagerId;

    /**
     * @Column(column="production_worker_id", type="integer", nullable=false)
     * @FormElement(label="Production worker", type="Select", required=true, relation="ProductionWorker")
     */
    protected $productionWorkerId;

    /**
     * @Column(column="qty", type="decimal", nullable=false)
     * @FormElement(label="Qty", type="Text", required=true)
     */
    protected $qty;

    /**
     * @Column(column="date", type="date", nullable=false)
     * @FormElement(label="Date", type="Date", required=true)
     */
    protected $date;

    /**
     * @Column(column="start_time", type="string", nullable=false)
     * @FormElement(label="Start time", type="Text", required=true)
     */
    protected $startTime;

    /**
     * @Column(column="end_time", type="string", nullable=false)
     * @FormElement(label="End time", type="Text", required=true)
     */
    protected $endTime;

    /**
     * @Column(column="created", type="string", nullable=false)
     * @FormElement(label="Created", type="Text", required=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CUTTING_JOB_ID => true,
        self::PROPERTY_PRODUCTION_WORKER_ID => true,
        self::PROPERTY_QTY => true,
        self::PROPERTY_DATE => true,
        self::PROPERTY_START_TIME => true,
        self::PROPERTY_END_TIME => true,
        self::PROPERTY_CREATED => true,
        self::ALIAS_CUTTING_JOB => true,
        self::ALIAS_PRODUCTION_WORKER => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getCuttingJobId()
    {
        return $this->cuttingJobId;
    }

    /**
     * @param int $cuttingJobId
     * @return $this
     */
    public function setCuttingJobId($cuttingJobId)
    {
        $this->cuttingJobId = null !== $cuttingJobId ? (int)$cuttingJobId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductionManagerId()
    {
        return $this->productionManagerId;
    }

    /**
     * @param int $productionManagerId
     * @return $this
     */
    public function setProductionManagerId($productionManagerId)
    {
        $this->productionManagerId = null !== $productionManagerId ? (int)$productionManagerId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductionWorkerId()
    {
        return $this->productionWorkerId;
    }

    /**
     * @param int $productionWorkerId
     * @return $this
     */
    public function setProductionWorkerId($productionWorkerId)
    {
        $this->productionWorkerId = null !== $productionWorkerId ? (int)$productionWorkerId : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param float $qty
     * @return $this
     */
    public function setQty($qty)
    {
        $this->qty = null !== $qty ? (float)$qty : null;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string|DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param string $startTime
     * @return $this
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param string $endTime
     * @return $this
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|CuttingJob
     * 
     * @throws \DomainException
     */
    public function getCuttingJob()
    {
        return $this->getRelated(self::ALIAS_CUTTING_JOB);
    }

    /**
     * @param CuttingJob $cuttingJob
     * @return $this
     */
    public function setCuttingJob(CuttingJob $cuttingJob)
    {
        $this->cuttingJobId = $cuttingJob->getId();

        return $this;
    }

    /**
     * @return null|ProductionWorker
     * 
     * @throws \DomainException
     */
    public function getProductionWorker()
    {
        return $this->getRelated(self::ALIAS_PRODUCTION_WORKER);
    }

    /**
     * @param ProductionWorker $productionManager
     * @return $this
     */
    public function setProductionManager(ProductionWorker $productionManager)
    {
        $this->productionManagerId = $productionManager->getId();

        return $this;
    }


    /**
     * @return null|ProductionWorker
     *
     * @throws \DomainException
     */
    public function getProductionManager()
    {
        return $this->getRelated(self::ALIAS_PRODUCTION_MANAGER);
    }

    /**
     * @param ProductionWorker $productionWorker
     * @return $this
     */
    public function setProductionWorker(ProductionWorker $productionWorker)
    {
        $this->productionWorkerId = $productionWorker->getId();

        return $this;
    }

    public function getWorksheetStartHour()
    {
        list($hours,$mins,$secs) = explode(':',$this->startTime);
        return $hours;
    }

    public function getWorksheetStartMin()
    {
        list($hours,$mins,$secs) = explode(':',$this->startTime);
        return $mins;
    }

    public function getWorksheetEndHour()
    {
        list($hours,$mins,$secs) = explode(':',$this->endTime);
        return $hours;
    }

    public function getWorksheetEndMin()
    {
        list($hours,$mins,$secs) = explode(':',$this->endTime);
        return $mins;
    }

    public function getTimeBetweenWorksheet()
    {
        $endTime = strtotime($this->endTime);
        $startTime = strtotime($this->startTime);
        return round(abs($endTime - $startTime) /60,2);
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
