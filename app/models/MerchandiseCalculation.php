<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * MerchandiseCalculation Model Class
 *
 * @Source('merchandise_calculations')
 * @HasMany('id', 'MerchandiseCalculationItem', 'merchandiseCalculationId', {'alias':'MerchandiseCalculationItems'})
 */
class MerchandiseCalculation extends AbstractEntity
{
    const TYPE_FINISHED = 'FINISHED';
    const TYPE_PREPARED = 'PREPARED';

    const PROPERTY_ID = 'id';
    const PROPERTY_DOCUMENT_NAME = 'documentName';
    const PROPERTY_MIS_CALCULATION = 'misCalculation';
    const PROPERTY_FILE = 'file';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_CREATED = 'created';
    const PROPERTY_IS_SYNCED = 'isSynced';


    const ALIAS_MERCHANDISE_CALCULATION_ITEMS = 'MerchandiseCalculationItems';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="document_name", type="string", nullable=false)
     * @FormElement(label="Document name", type="Text", required=true)
     */
    protected $documentName;

    /**
     * @Column(column="mis_calculation", type="string", nullable=true)
     * @FormElement(label="Mis calculation", type="Text", required=false)
     */
    protected $misCalculation;

    /**
     * @Column(column="file", type="string", nullable=false)
     * @FormElement(label="File", type="File", required=true)
     */
    protected $file;

    /**
     * @Column(column="type", type="string", nullable=true)
     */
    protected $type;


    /**
     * @Column(column="is_synced", type="bool", nullable=true)
     */
    protected $isSynced;

    /**
     * @Column(column="created", type="string", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_DOCUMENT_NAME => true,
        self::PROPERTY_MIS_CALCULATION => true,
        self::PROPERTY_FILE => true,
        self::PROPERTY_TYPE => true,
        self::PROPERTY_CREATED => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentName()
    {
        return $this->documentName;
    }

    /**
     * @param string $documentName
     * @return $this
     */
    public function setDocumentName($documentName)
    {
        $this->documentName = $documentName;

        return $this;
    }

    /**
     * @return string
     */
    public function getMisCalculation()
    {
        return $this->misCalculation;
    }

    /**
     * @param string $misCalculation
     * @return $this
     */
    public function setMisCalculation($misCalculation)
    {
        $this->misCalculation = $misCalculation;

        return $this;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     * @return $this
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSynced()
    {
        return $this->isSynced;
    }

    /**
     * @param boolean $isSynced
     */
    public function setIsSynced($isSynced)
    {
        $this->isSynced = $isSynced;
    }


    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|MerchandiseCalculationItem[]
     * 
     * @throws \DomainException
     */
    public function getMerchandiseCalculationItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_MERCHANDISE_CALCULATION_ITEMS, $arguments);
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return APP_PATH . 'var/uploads/files/';
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $url = ltrim($config->application->baseUri, '/');

        if (is_file($this->getUploadPath() . $this->getFile())) {
            return $url . '/uploads/files/' . $this->getFile();
        }

        return null;
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
        $this->type = self::TYPE_PREPARED;
        $this->isSynced = false;
    }

}
