<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * Invoice Model Class
 *
 * @Source('invoices')
 *
 * @BelongsTo('clientId', 'Client', 'id', {'alias':'Client'})
 * @HasMany('id', 'InvoiceItem', 'invoiceId', {'alias':'InvoiceItems'})
 */
class Invoice extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_INVOICE = 'invoice';
    const PROPERTY_DATE = 'date';
    const PROPERTY_STATUS = 'status';
    const PROPERTY_CLIENT_ID = 'clientId';
    const PROPERTY_AMOUNT = 'amount';
    const PROPERTY_CREATED = 'created';
    const PROPERTY_CUSTOM = 'custom';
    const PROPERTY_BOX_QTY = 'boxQty';
    const PROPERTY_COLETS_QTY = 'coletsQty';
    const PROPERTY_BRUTO_WEIGHT = 'brutoWeight';
    const PROPERTY_NETO_WEIGHT = 'netoWeight';
    const PROPERTY_STATEMENT = 'statement';

    const ALIAS_CLIENT = 'Client';
    const ALIAS_INVOICE_ITEMS = 'InvoiceItems';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="invoice", type="string", nullable=false)
     * @FormElement(label="Invoice", type="Text", required=true)
     */
    protected $invoice;

    /**
     * @Column(column="date", type="date", nullable=false)
     * @FormElement(label="Date", type="Date", required=true)
     */
    protected $date;

    /**
     * @Column(column="status", type="integer", nullable=true)
     * @FormElement(label="Status", type="Check", required=false)
     */
    protected $status;

    /**
     * @Column(column="client_id", type="integer", nullable=false)
     * @FormElement(label="Client", type="Select", required=true, relation="Client")
     */
    protected $clientId;

    /**
     * @Column(column="amount", type="decimal", nullable=false)
     * @FormElement(label="Amount", type="Text", required=true)
     */
    protected $amount;

    /**
     * @Column(column="created", type="string", nullable=false)
     * @FormElement(label="Created", type="Text", required=true)
     */
    protected $created;

    /**
     * @Column(column="custom", type="string", nullable=true)
     * @FormElement(label="Custom", type="Text", required=false)
     */
    protected $custom;

    /**
     * @Column(column="box_qty", type="string", nullable=true)
     * @FormElement(label="Box qty", type="Text", required=false)
     */
    protected $boxQty;

    /**
     * @Column(column="colets_qty", type="string", nullable=true)
     * @FormElement(label="Colets qty", type="Text", required=false)
     */
    protected $coletsQty;

    /**
     * @Column(column="bruto_weight", type="string", nullable=true)
     * @FormElement(label="Bruto weight", type="Text", required=false)
     */
    protected $brutoWeight;

    /**
     * @Column(column="neto_weight", type="string", nullable=true)
     * @FormElement(label="Neto weight", type="Text", required=false)
     */
    protected $netoWeight;

    /**
     * @Column(column="statement", type="string", nullable=true)
     * @FormElement(label="Statement", type="Text", required=false)
     */
    protected $statement;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_INVOICE => true,
        self::PROPERTY_DATE => true,
        self::PROPERTY_STATUS => true,
        self::PROPERTY_CLIENT_ID => true,
        self::PROPERTY_AMOUNT => true,
        self::PROPERTY_CREATED => true,
        self::PROPERTY_CUSTOM => true,
        self::PROPERTY_BOX_QTY => true,
        self::PROPERTY_COLETS_QTY => true,
        self::PROPERTY_BRUTO_WEIGHT => true,
        self::PROPERTY_NETO_WEIGHT => true,
        self::PROPERTY_STATEMENT => true,
        self::ALIAS_CLIENT => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     * @return $this
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string|DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return (bool)$this->status;
    }

    /**
     * @param int|bool $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status ? 1 : 0;

        return $this;
    }

    /**
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param int $clientId
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = null !== $clientId ? (int)$clientId : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = null !== $amount ? (float)$amount : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustom()
    {
        return $this->custom;
    }

    /**
     * @param string $custom
     * @return $this
     */
    public function setCustom($custom)
    {
        $this->custom = $custom;

        return $this;
    }

    /**
     * @return string
     */
    public function getBoxQty()
    {
        return $this->boxQty;
    }

    /**
     * @param string $boxQty
     * @return $this
     */
    public function setBoxQty($boxQty)
    {
        $this->boxQty = $boxQty;

        return $this;
    }

    /**
     * @return string
     */
    public function getColetsQty()
    {
        return $this->coletsQty;
    }

    /**
     * @param string $coletsQty
     * @return $this
     */
    public function setColetsQty($coletsQty)
    {
        $this->coletsQty = $coletsQty;

        return $this;
    }

    /**
     * @return string
     */
    public function getBrutoWeight()
    {
        return $this->brutoWeight;
    }

    /**
     * @param string $brutoWeight
     * @return $this
     */
    public function setBrutoWeight($brutoWeight)
    {
        $this->brutoWeight = $brutoWeight;

        return $this;
    }

    /**
     * @return string
     */
    public function getNetoWeight()
    {
        return $this->netoWeight;
    }

    /**
     * @param string $netoWeight
     * @return $this
     */
    public function setNetoWeight($netoWeight)
    {
        $this->netoWeight = $netoWeight;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatement()
    {
        return $this->statement;
    }

    /**
     * @param string $statement
     * @return $this
     */
    public function setStatement($statement)
    {
        $this->statement = $statement;

        return $this;
    }

    /**
     * @return null|Client
     * 
     * @throws \DomainException
     */
    public function getClient()
    {
        return $this->getRelated(self::ALIAS_CLIENT);
    }

    /**
     * @param Client $client
     * @return $this
     */
    public function setClient(Client $client)
    {
        $this->clientId = $client->getId();

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|InvoiceItem[]
     * 
     * @throws \DomainException
     */
    public function getInvoiceItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_INVOICE_ITEMS, $arguments);
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
