<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * DeliveryNote Model Class
 *
 * @Source('delivery_notes')
 * @HasMany('id', 'DeliveryNoteItem', 'deliveryNoteId', {'alias':'DeliveryNoteItems'})
 */
class DeliveryNote extends AbstractEntity
{
    const TYPE_FINISHED = 'FINISHED';
    const TYPE_PREPARED = 'PREPARED';

    const PROPERTY_ID = 'id';
    const PROPERTY_DOCUMENT_NAME = 'documentName';
    const PROPERTY_MIS_DELIVERY_NOTE = 'misDeliveryNote';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_CREATED = 'created';

    const ALIAS_DELIVERY_NOTE_ITEMS = 'DeliveryNoteItems';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="document_name", type="string", nullable=false)
     * @FormElement(label="Document name", type="Text", required=true)
     */
    protected $documentName;

    /**
     * @Column(column="mis_delivery_note", type="string", nullable=false)
     * @FormElement(label="Mis delivery note", type="Text", required=true)
     */
    protected $misDeliveryNote;

    /**
     * @Column(column="type", type="string", nullable=false)
     */
    protected $type;

    /**
     * @Column(column="created", type="dateTime", nullable=false)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_DOCUMENT_NAME => true,
        self::PROPERTY_MIS_DELIVERY_NOTE => true,
        self::PROPERTY_TYPE => true,
        self::PROPERTY_CREATED => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentName()
    {
        return $this->documentName;
    }

    /**
     * @param string $documentName
     * @return $this
     */
    public function setDocumentName($documentName)
    {
        $this->documentName = $documentName;

        return $this;
    }

    /**
     * @return string
     */
    public function getMisDeliveryNote()
    {
        return $this->misDeliveryNote;
    }

    /**
     * @param string $misDeliveryNote
     * @return $this
     */
    public function setMisDeliveryNote($misDeliveryNote)
    {
        $this->misDeliveryNote = $misDeliveryNote;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|DeliveryNoteItem[]
     * 
     * @throws \DomainException
     */
    public function getDeliveryNoteItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_DELIVERY_NOTE_ITEMS, $arguments);
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
        $this->type = self::TYPE_PREPARED;
    }
}
