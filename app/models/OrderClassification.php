<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;
use \Vemid\Task\Classifications\SetProductsIsOrderable;
use \Vemid\Task\Classifications\UnSetProductsIsOrderable;

/**
 * OrderClassification Model Class
 *
 * @Source('order_classifications')
 *
 * @BelongsTo('classificationId', 'Classification', 'id', {'alias':'Classification'})
 * @BelongsTo('orderId', 'Order', 'id', {'alias':'Order'})
 */
class OrderClassification extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CLASSIFICATION_ID = 'classificationId';
    const PROPERTY_ORDER_ID = 'orderId';

    const ALIAS_CLASSIFICATION = 'Classification';
    const ALIAS_ORDER = 'Order';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="classification_id", type="integer", nullable=false)
     * @FormElement(label="Classification", type="Select", required=true, relation="Classification")
     */
    protected $classificationId;

    /**
     * @Column(column="order_id", type="integer", nullable=false)
     * @FormElement(label="Order", type="Select", required=true, relation="Order")
     */
    protected $orderId;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CLASSIFICATION_ID => true,
        self::PROPERTY_ORDER_ID => true,
        self::ALIAS_CLASSIFICATION => true,
        self::ALIAS_ORDER => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getClassificationId()
    {
        return $this->classificationId;
    }

    /**
     * @param int $classificationId
     * @return $this
     */
    public function setClassificationId($classificationId)
    {
        $this->classificationId = null !== $classificationId ? (int)$classificationId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     * @return $this
     */
    public function setOrderId($orderId)
    {
        $this->orderId = null !== $orderId ? (int)$orderId : null;

        return $this;
    }

    /**
     * @return null|Classification
     * 
     * @throws \DomainException
     */
    public function getClassification()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setClassification(Classification $classification)
    {
        $this->classificationId = $classification->getId();

        return $this;
    }

    /**
     * @return null|Order
     * 
     * @throws \DomainException
     */
    public function getOrder()
    {
        return $this->getRelated(self::ALIAS_ORDER);
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function setOrder(Order $order)
    {
        $this->orderId = $order->getId();

        return $this;
    }

    public function afterCreate()
    {
        $productOrderable = new SetProductsIsOrderable();
        $productOrderable->classification = $this->getClassification();
        $productOrderable->runInBackground();
    }

    public function afterDelete()
    {
        $productOrderable = new UnSetProductsIsOrderable();
        $productOrderable->classification = $this->getClassification();
        $productOrderable->runInBackground();
    }
}
