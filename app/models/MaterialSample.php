<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use \Vemid\Service\UserNotification\Message;

/**
 * MaterialSample Model Class
 *
 * @Source('material_samples')
 *
 * @BelongsTo('materialTypeCodeId', 'Classification', 'id', {'alias':'MaterialTypeClassification'})
 * @BelongsTo('materialCodeId', 'Classification', 'id', {'alias':'MaterialClassification'})
 * @BelongsTo('subMaterialCodeId', 'Classification', 'id', {'alias':'SubMaterialClassification'})
 * @BelongsTo('supplierCodeId', 'Supplier', 'id')
 */
class MaterialSample extends AbstractEntity
{
    const DEMANDED = 'ZAHTEVANO';
    const REQUESTED = 'UPITANO';
    const ORDERED = 'PORUČENO';
    const DELIVERED = 'ISPORUČENO';
    const CANCELED = 'OTKAZANO';

    const PROPERTY_ID = 'id';
    const PROPERTY_MATERIAL_TYPE_CODE_ID = 'materialTypeCodeId';
    const PROPERTY_SUPPLIER_CODE_ID = 'supplierCodeId';
    const PROPERTY_MATERIAL_CODE_ID = 'materialCodeId';
    const PROPERTY_SUB_MATERIAL_CODE_ID = 'subMaterialCodeId';
    const PROPERTY_NAME = 'name';
    const PROPERTY_COMPOSITION = 'composition';
    const PROPERTY_WEIGHT = 'weight';
    const PROPERTY_HEIGHT = 'height';
    const PROPERTY_COLOR = 'color';
    const PROPERTY_PANTON_COLOR = 'color';
    const PROPERTY_MATERIAL_PICTURE = 'materialPicture';
    const PROPERTY_QTY = 'qty';
    const PROPERTY_SUPPLIER_PRODUCT_CODE = 'supplierProductCode';
    const PROPERTY_DELIVERY_TIME = 'deliveryTime';
    const PROPERTY_COSTS = 'costs';
    const PROPERTY_ADDITIONAL_EXPENSES = 'additionalExpenses';
    const PROPERTY_NOTE = 'note';
    const PROPERTY_STATUS = 'status';
    const PROPERTY_CREATED = 'created';


    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="supplier_code_id", type="integer", nullable=false)
     * @FormElement(label="Supplier code", type="Select", required=true)
     */
    protected $supplierCodeId;

    /**
     * @Column(column="material_type_code_id", type="integer", nullable=false)
     * @FormElement(label="Material type code", type="Select", required=true)
     */
    protected $materialTypeCodeId;

    /**
     * @Column(column="material_code_id", type="integer", nullable=false)
     * @FormElement(label="Material code", type="Select", required=true)
     */
    protected $materialCodeId;

    /**
     * @Column(column="sub_material_code_id", type="integer", nullable=false)
     * @FormElement(label="Sub Material code", type="Select", required=true)
     */
    protected $subMaterialCodeId;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="supplier_product_code", type="integer", nullable=true)
     * @FormElement(label="Artikl", type="Text", required=false)
     */
    protected $supplierProductCode;

    /**
     * @Column(column="composition", type="string", nullable=false)
     * @FormElement(label="Composition", type="Text", required=true)
     */
    protected $composition;

    /**
     * @Column(column="weight", type="integer", nullable=false)
     * @FormElement(label="Weight", type="Text", required=true)
     */
    protected $weight;

    /**
     * @Column(column="height", type="string", nullable=false)
     * @FormElement(label="Height", type="Text", required=true)
     */
    protected $height;

    /**
     * @Column(column="color", type="string", nullable=false)
     * @FormElement(label="Color", type="Text", required=true)
     */
    protected $color;

    /**
     * @Column(column="material_picture", type="string", nullable=true)
     * @FormElement(label="Material picture", type="File", required=false)
     */
    protected $materialPicture;

    /**
     * @Column(column="qty", type="integer", nullable=false)
     * @FormElement(label="Qty", type="Text", required=true)
     */
    protected $qty;

    /**
     * @Column(column="delivery_time", type="date", nullable=false)
     * @FormElement(label="Delivery time", type="Date", required=true)
     */
    protected $deliveryTime;

    /**
     * @Column(column="panton_color", type="string", nullable=false)
     * @FormElement(label="Panton boja", type="Text", required=true)
     */
    protected $pantonColor;

    /**
     * @Column(column="costs", type="integer", nullable=false)
     * @FormElement(label="Costs", type="Text", required=true)
     */
    protected $costs;

    /**
     * @Column(column="additional_expenses", type="integer", nullable=false)
     * @FormElement(label="Additional expenses", type="Text", required=true)
     */
    protected $additionalExpenses;

    /**
     * @Column(column="status", type="string", nullable=false)
     */
    protected $status;

    /**
     * @Column(column="note", type="string", nullable=false)
     * @FormElement(label="Note", type="TextArea", required=true)
     */
    protected $note;

    /**
     * @Column(column="created", type="dateTime", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_MATERIAL_TYPE_CODE_ID => true,
        self::PROPERTY_SUPPLIER_CODE_ID => true,
        self::PROPERTY_MATERIAL_CODE_ID => true,
        self::PROPERTY_SUB_MATERIAL_CODE_ID => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_COMPOSITION => true,
        self::PROPERTY_WEIGHT => true,
        self::PROPERTY_HEIGHT => true,
        self::PROPERTY_COLOR => true,
        self::PROPERTY_MATERIAL_PICTURE => true,
        self::PROPERTY_QTY => true,
        self::PROPERTY_SUPPLIER_PRODUCT_CODE => true,
        self::PROPERTY_DELIVERY_TIME => true,
        self::PROPERTY_COSTS => true,
        self::PROPERTY_ADDITIONAL_EXPENSES => true,
        self::PROPERTY_NOTE => true,
        self::PROPERTY_CREATED =>true,
    );

    const ALIAS_MATERIAL_TYPE_CLASSIFICATION = 'MaterialTypeClassification';
    const ALIAS_MATERIAL_CLASSIFICATION = 'MaterialClassification';
    const ALIAS_SUB_MATERIAL_CLASSIFICATION = 'SubMaterialClassification';
    const ALIAS_SUPPLIER = 'Supplier';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaterialTypeCodeId()
    {
        return $this->materialTypeCodeId;
    }

    /**
     * @param int $materialTypeCodeId
     * @return $this
     */
    public function setMaterialTypeCodeId($materialTypeCodeId)
    {
        $this->materialTypeCodeId = null !== $materialTypeCodeId ? (int)$materialTypeCodeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getSupplierCodeId()
    {
        return $this->supplierCodeId;
    }

    /**
     * @param int $supplierCodeId
     * @return $this
     */
    public function setSupplierCodeId($supplierCodeId)
    {
        $this->supplierCodeId = null !== $supplierCodeId ? (int)$supplierCodeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaterialCodeId()
    {
        return $this->materialCodeId;
    }

    /**
     * @param int $materialCodeId
     * @return $this
     */
    public function setMaterialCodeId($materialCodeId)
    {
        $this->materialCodeId = null !== $materialCodeId ? (int)$materialCodeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getSubMaterialCodeId()
    {
        return $this->subMaterialCodeId;
    }

    /**
     * @param int $subMaterialCodeId
     * @return $this
     */
    public function setSubMaterialCodeId($subMaterialCodeId)
    {
        $this->subMaterialCodeId = null !== $subMaterialCodeId ? (int)$subMaterialCodeId : null;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSupplierProductCode()
    {
        return $this->supplierProductCode;
    }

    /**
     * @param string $supplierProductCode
     * @return $this
     */
    public function setSupplierProductCode($supplierProductCode)
    {
        $this->supplierProductCode = null !== $supplierProductCode ? (int)$supplierProductCode : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getComposition()
    {
        return $this->composition;
    }

    /**
     * @param string $composition
     * @return $this
     */
    public function setComposition($composition)
    {
        $this->composition = $composition;

        return $this;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     * @return $this
     */
    public function setWeight($weight)
    {
        $this->weight = null !== $weight ? (int)$weight : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param string $height
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return $this
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return string
     */
    public function getPantonColor()
    {
        return $this->pantonColor;
    }

    /**
     * @param string $pantonColor
     * @return $this
     */
    public function setPantonColor($pantonColor)
    {
        $this->pantonColor = $pantonColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getMaterialPicture()
    {
        return $this->materialPicture;
    }

    /**
     * @param string $materialPicture
     * @return $this
     */
    public function setMaterialPicture($materialPicture)
    {
        $this->materialPicture = $materialPicture;

        return $this;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param int $qty
     * @return $this
     */
    public function setQty($qty)
    {
        $this->qty = null !== $qty ? (int)$qty : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->productId = null !== $productId ? (int)$productId : null;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getDeliveryTime()
    {
        return $this->deliveryTime;
    }

    /**
     * @param string|DateTime $deliveryTime
     * @return $this
     */
    public function setDeliveryTime($deliveryTime)
    {
        $this->deliveryTime = $deliveryTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getCosts()
    {
        return $this->costs;
    }

    /**
     * @param int $costs
     * @return $this
     */
    public function setCosts($costs)
    {
        $this->costs = null !== $costs ? (int)$costs : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getAdditionalExpenses()
    {
        return $this->additionalExpenses;
    }

    /**
     * @param int $additionalExpenses
     * @return $this
     */
    public function setAdditionalExpenses($additionalExpenses)
    {
        $this->additionalExpenses = null !== $additionalExpenses ? (float)$additionalExpenses : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string|DateTime $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return APP_PATH . 'var/uploads/files/';
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $url = ltrim($config->application->baseUri, '/');

        if (is_file($this->getUploadPath() . $this->getMaterialPicture())) {
            return $url . '/uploads/files/' . $this->getMaterialPicture();
        }

        return null;
    }

    /**
     * @return null|Classification|\Vemid\Entity\EntityInterface
     *
     * @throws DomainException
     */
    public function getMaterialTypeClassification()
    {
        return $this->getRelated(self::ALIAS_MATERIAL_TYPE_CLASSIFICATION);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setMaterialTypeClassification(Classification $classification)
    {
        $this->materialTypeCodeId = $classification->getId();

        return $this;
    }

    /**
     * @return null|Classification|\Vemid\Entity\EntityInterface
     *
     * @throws DomainException
     */
    public function getMaterialClassification()
    {
        return $this->getRelated(self::ALIAS_MATERIAL_CLASSIFICATION);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setMaterialClassification(Classification $classification)
    {
        $this->materialCodeId = $classification->getId();

        return $this;
    }

    /**
     * @return null|Classification|\Vemid\Entity\EntityInterface
     *
     * @throws DomainException
     */
    public function getSupplier()
    {
        return $this->getRelated(self::ALIAS_SUPPLIER);
    }

    /**
     * @param Supplier $supplier
     * @return $this
     */
    public function setSupplier(Supplier $supplier)
    {
        $this->supplierCodeId = $supplier->getId();

        return $this;
    }

    /**
     * @return null|Classification|\Vemid\Entity\EntityInterface
     *
     * @throws DomainException
     */
    public function getSubMaterialClassification()
    {
        return $this->getRelated(self::ALIAS_SUB_MATERIAL_CLASSIFICATION);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setSubMaterialClassification(Classification $classification)
    {
        $this->subMaterialCodeId = $classification->getId();

        return $this;
    }

    /**
     * @return mixed|string
     */
    public  function getDisplayName()
    {
        return $this->getName();
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
