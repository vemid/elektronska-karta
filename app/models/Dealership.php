<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * Dealership Model Class
 *
 * @Source('dealerships')
 *
 * @BelongsTo('codeId', 'Code', 'id', {'alias':'Code'})
 */
class Dealership extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CODE_ID = 'codeId';
    const PROPERTY_ACCOUNT = 'account';
    const PROPERTY_COMPANY_NAME = 'companyName';
    const PROPERTY_ADDRESS = 'address';
    const PROPERTY_CITY = 'city';
    const PROPERTY_POSTAL_CODE = 'postalCode';
    const PROPERTY_EMAIL = 'email';
    const PROPERTY_CLIENT_CODE = 'client_code';


    const ALIAS_CODE = 'Code';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="code_id", type="integer", nullable=false)
     * @FormElement(label="Code", type="Select", required=true, relation="Code")
     */
    protected $codeId;

    /**
     * @Column(column="account", type="string", nullable=false)
     * @FormElement(label="Account", type="Text", required=true)
     */
    protected $account;

    /**
     * @Column(column="company_name", type="string", nullable=false)
     * @FormElement(label="Company name", type="Text", required=true)
     */
    protected $companyName;

    /**
     * @Column(column="address", type="string", nullable=false)
     * @FormElement(label="Address", type="Text", required=true)
     */
    protected $address;

    /**
     * @Column(column="city", type="string", nullable=false)
     * @FormElement(label="City", type="Text", required=true)
     */
    protected $city;

    /**
     * @Column(column="postal_code", type="string", nullable=false)
     * @FormElement(label="Postal code", type="Text", required=true)
     */
    protected $postalCode;

    /**
     * @Column(column="email", type="string", nullable=false)
     * @FormElement(label="E mail", type="Text", required=true)
     */
    protected $email;

    /**
     * @Column(column="client_code", type="string", nullable=false)
     * @FormElement(label="Partner Code", type="Text", required=true)
     */
    protected $clientCode;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CODE_ID => true,
        self::PROPERTY_ACCOUNT => true,
        self::PROPERTY_COMPANY_NAME => true,
        self::PROPERTY_ADDRESS => true,
        self::PROPERTY_CITY => true,
        self::PROPERTY_POSTAL_CODE => true,
        self::ALIAS_CODE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * @param int $codeId
     * @return $this
     */
    public function setCodeId($codeId)
    {
        $this->codeId = null !== $codeId ? (int)$codeId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param string $account
     * @return $this
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     * @return $this
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     * @return $this
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientCode()
    {
        return $this->clientCode;
    }

    /**
     * @param string $clientCode
     * @return $this
     */
    public function setClientCode($clientCode)
    {
        $this->clientCode = $clientCode;

        return $this;
    }

    /**
     * @return null|Code
     * 
     * @throws \DomainException
     */
    public function getCode()
    {
        return $this->getRelated(self::ALIAS_CODE);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setCode(Code $code)
    {
        $this->codeId = $code->getId();

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return trim($this->companyName) . ' ' . trim($this->address) . ' ' . trim($this->city) . ' ' . trim($this->postalCode);
    }

}
