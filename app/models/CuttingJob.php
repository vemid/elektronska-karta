<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * CuttingJob Model Class
 *
 * @Source('cutting_jobs')
 * @HasMany('id', 'CuttingJobWorksheet', 'cuttingJobId', {'alias':'CuttingJobWorksheets'})
 */
class CuttingJob extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_NAME = 'name';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_PIECE_TIME = 'pieceTime';
    const PROPERTY_PIECES_HOUR = 'piecesHour';
    const PROPERTY_PIECE_PRICE = 'piecePrice';
    const PROPERTY_BARCODE = 'barcode';
    const PROPERTY_CREATED = 'created';

    const TYPE_FAILURE = 'FAILURE';
    const TYPE_SAMPLE = 'SAMPLE';
    const TYPE_OTHER = 'OTHER';

    const ALIAS_CUTTING_JOB_WORKSHEETS = 'CuttingJobWorksheets';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="type", type="string", nullable=true)
     * @FormElement(label="Vrsta", type="Select", required=false, options={'FAILURE':'Kvar', 'SAMPLE':'Uzorak', 'OTHER':'Ostalo'})
     */
    protected $type;

    /**
     * @Column(column="piece_time", type="decimal", nullable=false)
     * @FormElement(label="Piece time", type="Text", required=true)
     */
    protected $pieceTime;

    /**
     * @Column(column="pieces_hour", type="decimal", nullable=false)
     * @FormElement(label="Pieces hour", type="Text", required=true)
     */
    protected $piecesHour;

    /**
     * @Column(column="piece_price", type="decimal", nullable=false)
     * @FormElement(label="Piece price", type="Text", required=true)
     */
    protected $piecePrice;

    /**
     * @Column(column="barcode", type="string", nullable=true)
     */
    protected $barcode;

    /**
     * @Column(column="created", type="string", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_TYPE => true,
        self::PROPERTY_PIECE_TIME => true,
        self::PROPERTY_PIECES_HOUR => true,
        self::PROPERTY_PIECE_PRICE => true,
        self::PROPERTY_BARCODE => true,
        self::PROPERTY_CREATED => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return float
     */
    public function getPieceTime()
    {
        return $this->pieceTime;
    }

    /**
     * @param float $pieceTime
     * @return $this
     */
    public function setPieceTime($pieceTime)
    {
        $this->pieceTime = null !== $pieceTime ? (float)$pieceTime : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getPiecesHour()
    {
        return $this->piecesHour;
    }

    /**
     * @param float $piecesHour
     * @return $this
     */
    public function setPiecesHour($piecesHour)
    {
        $this->piecesHour = null !== $piecesHour ? (float)$piecesHour : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getPiecePrice()
    {
        return $this->piecePrice;
    }

    /**
     * @param float $piecePrice
     * @return $this
     */
    public function setPiecePrice($piecePrice)
    {
        $this->piecePrice = null !== $piecePrice ? (float)$piecePrice : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     * @return $this
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|CuttingJobWorksheet[]
     * 
     * @throws \DomainException
     */
    public function getCuttingJobWorksheets($arguments = null)
    {
        return $this->getRelated(self::ALIAS_CUTTING_JOB_WORKSHEETS, $arguments);
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }

    public function afterSave()
    {
        if (null === $this->barcode) {
            $this->barcode = "R-".$this->getEntityId();
            $this->save();
        }
    }

}
