<?php

use \Vemid\Entity\EntityInterface;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * Attribute Model Class
 *
 * @Source('attributes')
 *
 * @BelongsTo('attributeCodeId', 'Code', 'id', {'alias':'AttributeCode'})
 */
class Attribute extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_ATTRIBUTE_CODE_ID = 'attributeCodeId';
    const PROPERTY_NAME = 'name';

    const ALIAS_ATTRIBUTE_CODE = 'attributeCode';


    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="attribute_code_id", type="integer", nullable=false)
     * @FormElement(label="Attribute code", type="Text", required=true)
     */
    protected $attributeCodeId;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;


    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_ATTRIBUTE_CODE_ID => true,
        self::PROPERTY_NAME => true,
        self::ALIAS_ATTRIBUTE_CODE =>true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getAttributeCodeId()
    {
        return $this->attributeCodeId;
    }

    /**
     * @param int $attributeCodeId
     * @return $this
     */
    public function setAttributeCodeId($attributeCodeId)
    {
        $this->attributeCodeId = null !== $attributeCodeId ? (int)$attributeCodeId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->getName();
    }

    /**
     * @return null|Resultset|EntityInterface|Code
     */
    public function getAttributeCode()
    {
        return $this->getRelated(self::ALIAS_ATTRIBUTE_CODE);
    }
}
