<?php

use Vemid\Date\DateTime;
use Vemid\Helper\HashHelper;
use Vemid\Helper\SiteHelper;
use Vemid\Entity\AbstractEntity;

/**
 * PasswordReset Model Class
 *
 * @Source('password_resets')
 *
 * @BelongsTo('userId', 'User', 'id', {'alias':'User'})
 */
class PasswordReset extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_USER_ID = 'userId';
    const PROPERTY_HASH = 'hash';
    const PROPERTY_CREATED_DATETIME = 'createdDatetime';
    const PROPERTY_MODIFIED_DATETIME = 'modifiedDatetime';

    const ALIAS_USER = 'User';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="user_id", type="integer", nullable=false)
     */
    protected $userId;

    /**
     * @Column(column="hash", type="string", nullable=false)
     */
    protected $hash;

    /**
     * @Column(column="created_datetime", type="dateTime", nullable=true)
     */
    protected $createdDatetime;

    /**
     * @Column(column="modified_datetime", type="dateTime", nullable=true)
     */
    protected $modifiedDatetime;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_USER_ID => true,
        self::PROPERTY_HASH => true,
        self::PROPERTY_CREATED_DATETIME => true,
        self::PROPERTY_MODIFIED_DATETIME => true,
        self::ALIAS_USER => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return !is_null($this->id) ? intval($this->id) : null;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = !is_null($id) ? intval($id) : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return !is_null($this->userId) ? intval($this->userId) : null;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = !is_null($userId) ? intval($userId) : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     * @return $this
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }

    /**
     * @param string|DateTime $createdDatetime
     * @return $this
     */
    public function setCreatedDatetime($createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getModifiedDatetime()
    {
        return $this->modifiedDatetime;
    }

    /**
     * @param string|DateTime $modifiedDatetime
     * @return $this
     */
    public function setModifiedDatetime($modifiedDatetime)
    {
        $this->modifiedDatetime = $modifiedDatetime;

        return $this;
    }

    /**
     * @return null|User
     */
    public function getUser()
    {
        return $this->getRelated(self::ALIAS_USER);
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->userId = $user->getId();

        return $this;
    }

    /**
     * @return string
     */
    public function getResetLink()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $siteHelper = new SiteHelper([
            'baseUri' => $config->application->baseUri,
            'siteName' => $config->application->siteName,
        ]);

        return $siteHelper->linkTo('/auth/change-password/' . $this->userId . '/' . urlencode($this->hash));
    }

    /**
     * @return bool
     */
    public function validation()
    {
        $this->validate(
            new \Phalcon\Mvc\Model\Validator\Uniqueness([
                'field' => [self::PROPERTY_USER_ID, self::PROPERTY_HASH],
                'message' => $this->_translate('Detected duplicated token.')
            ])
        );

        return !$this->validationHasFailed();
    }

    /**
     *
     */
    public function beforeValidationOnCreate()
    {
        if (is_null($this->hash)) {
            $hashHelper = new HashHelper();
            $this->setHash($hashHelper->generateHash(time(), 'sha1'));
        }

        $this->createdDatetime = new DateTime();
    }

    /**
     *
     */
    public function afterCreate()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $siteHelper = new SiteHelper([
            'baseUri' => $config->application->baseUri,
            'siteName' => $config->application->siteName,
        ]);

        /** @var \Comit\Mailer\Manager $mailManager */
        $mailManager = $this->getDI()->get('mailManager');
        $message = $mailManager->createMessageFromView('email/reset-password', [
            'passwordReset' => $this,
            'siteSettings' => $siteHelper->toSimpleObject(),
        ], APP_PATH . 'app/templates/');

        $message->setTo($this->getUser()->getUsername(), $this->getUser()->getDisplayName())
            ->setSubject($this->_translate('Reset password'))
            ->sendInBackground();
    }

}
