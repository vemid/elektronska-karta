<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * Question Model Class
 *
 * @Source('questions')
 *
 * @BelongsTo('questionnaireId', 'Questionnaire', 'id', {'alias':'Questionnaire'})
 * @BelongsTo('answeredByUserId', 'User', 'id', {'alias':'AnsweredUser'})
 */
class Question extends AbstractEntity
{

    const INPUT = 'INPUT';
    const SELECT = 'SELECT';
    const MULTIPLE_SELECT = 'MULTIPLE_SELECT';
    const TEXT_AREA = 'TEXT_AREA';
    const RADIO = 'RADIO';
    const CHECK = 'CHECK';

    const PROPERTY_ID = 'id';
    const PROPERTY_QUESTIONNAIRE_ID = 'questionnaireId';
    const PROPERTY_FIELD = 'field';
    const PROPERTY_QUESTION = 'question';
    const PROPERTY_POSSIBLE_ANSWERS = 'possibleAnswers';
    const PROPERTY_ADDITIONAL_NOTE = 'additionalNote';
    const PROPERTY_ANSWERED_BY_USER_ID = 'answeredByUserId';
    const PROPERTY_ANSWERS = 'answers';
    const PROPERTY_ANSWERED_NOTE = 'answeredNote';

    const ALIAS_QUESTIONNAIRE = 'Questionnaire';
    const ALIAS_ANSWERED_USER = 'AnsweredUser';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="questionnaire_id", type="integer", nullable=false)
     * @FormElement(label="Questionnaire", type="Hidden", required=true)
     */
    protected $questionnaireId;

    /**
     * @Column(column="field", type="string", nullable=true)
     * @FormElement(label="Način odgovora na pitanje", type="Select", required=false, options={'INPUT' : 'Textualno polje', 'TEXT_AREA':'Opis', 'SELECT':'Izbor jedne opcije', 'MULTIPLE_SELECT':'Više opcija', 'RADIO':'Štiklirati jedan odgovora', 'CHECK':'Štiklirati jedan ili više odgovora'})
     */
    protected $field;

    /**
     * @Column(column="question", type="string", nullable=true)
     * @FormElement(label="Question", type="Text", required=false)
     */
    protected $question;

    /**
     * @Column(column="possible_answers", type="string", nullable=true)
     * @FormElement(label="Possible answers", type="Text", required=false)
     */
    protected $possibleAnswers;

    /**
     * @Column(column="additional_note", type="integer", nullable=false)
     * @FormElement(label="Dodatan opis na odgovore", type="Check", required=true)
     */
    protected $additionalNote;

    /**
     * @Column(column="answered_by_user_id", type="integer", nullable=true)
     * @FormElement(label="Answered by user", type="Text", required=false)
     */
    protected $answeredByUserId;

    /**
     * @Column(column="answers", type="string", nullable=true)
     * @FormElement(label="Answers", type="Text", required=false)
     */
    protected $answers;

    /**
     * @Column(column="answered_note", type="string", nullable=true)
     * @FormElement(label="Answered note", type="Text", required=false)
     */
    protected $answeredNote;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_QUESTIONNAIRE_ID => true,
        self::PROPERTY_FIELD => true,
        self::PROPERTY_QUESTION => true,
        self::PROPERTY_POSSIBLE_ANSWERS => true,
        self::PROPERTY_ADDITIONAL_NOTE => true,
        self::PROPERTY_ANSWERED_BY_USER_ID => true,
        self::PROPERTY_ANSWERS => true,
        self::PROPERTY_ANSWERED_NOTE => true,
        self::ALIAS_QUESTIONNAIRE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuestionnaireId()
    {
        return $this->questionnaireId;
    }

    /**
     * @param int $questionnaireId
     * @return $this
     */
    public function setQuestionnaireId($questionnaireId)
    {
        $this->questionnaireId = null !== $questionnaireId ? (int)$questionnaireId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     * @return $this
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $question
     * @return $this
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return string
     */
    public function getPossibleAnswers()
    {
        return $this->possibleAnswers;
    }

    /**
     * @param string $possibleAnswers
     * @return $this
     */
    public function setPossibleAnswers($possibleAnswers)
    {
        $this->possibleAnswers = $possibleAnswers;

        return $this;
    }

    /**
     * @return bool
     */
    public function getAdditionalNote()
    {
        return (bool)$this->additionalNote;
    }

    /**
     * @param int|bool $additionalNote
     * @return $this
     */
    public function setAdditionalNote($additionalNote)
    {
        $this->additionalNote = $additionalNote ? 1 : 0;

        return $this;
    }

    /**
     * @return int
     */
    public function getAnsweredByUserId()
    {
        return $this->answeredByUserId;
    }

    /**
     * @param int $answeredByUserId
     * @return $this
     */
    public function setAnsweredByUserId($answeredByUserId)
    {
        $this->answeredByUserId = null !== $answeredByUserId ? (int)$answeredByUserId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param string $answers
     * @return $this
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;

        return $this;
    }

    /**
     * @return string
     */
    public function getAnsweredNote()
    {
        return $this->answeredNote;
    }

    /**
     * @param string $answeredNote
     * @return $this
     */
    public function setAnsweredNote($answeredNote)
    {
        $this->answeredNote = $answeredNote;

        return $this;
    }

    /**
     * @return null|Questionnaire|\Vemid\Entity\EntityInterface
     * 
     * @throws \DomainException
     */
    public function getQuestionnaire()
    {
        return $this->getRelated(self::ALIAS_QUESTIONNAIRE);
    }

    /**
     * @param Questionnaire $questionnaire
     * @return $this
     */
    public function setQuestionnaire(Questionnaire $questionnaire)
    {
        $this->questionnaireId = $questionnaire->getId();

        return $this;
    }

    /**
     * @return null|User|\Vemid\Entity\EntityInterface
     *
     * @throws \DomainException
     */
    public function getAnsweredUser()
    {
        return $this->getRelated(self::ALIAS_ANSWERED_USER);
    }

    /**
     * @param User $answeredUser
     * @return $this
     */
    public function setAnsweredUser(User $answeredUser)
    {
        $this->answeredByUserId = $answeredUser->getId();

        return $this;
    }
}
