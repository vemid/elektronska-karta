<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * WarehouseOrderItem Model Class
 *
 * @Source('warehouse_order_items')
 * @BelongsTo('warehouseOrderId', 'WarehouseOrder', 'id', {'alias':'WarehouseOrder'})
 * @BelongsTo('productSizeId', 'ProductSize', 'id', {'alias':'ProductSize'})
 */
class WarehouseOrderItem extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_WAREHOUSE_ORDER_ID = 'warehouseOrderId';
    const PROPERTY_PRODUCT_SIZE_ID = 'productSizeId';
    const PROPERTY_QUANTITY = 'quantity';
    const PROPERTY_DELIVERED_QUANTITY = 'deliveredQuantity';
    const PROPERTY_NOTE = 'note';
    const PROPERTY_CREATED = 'created';

    const ALIAS_WAREHOUSE_ORDER = 'WarehouseOrder';
    const ALIAS_PRODUCT_SIZE = 'ProductSize';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="warehouse_order_id", type="integer", nullable=false)
     * @FormElement(label="Warehouse_order", type="Text", required=true)
     */
    protected $warehouseOrderId;

    /**
     * @Column(column="product_size_id", type="integer", nullable=false)
     * @FormElement(label="Product_size", type="Text", required=true)
     */
    protected $productSizeId;

    /**
     * @Column(column="quantity", type="integer", nullable=false)
     * @FormElement(label="Quantity", type="Text", required=true)
     */
    protected $quantity;

    /**
     * @Column(column="delivered_quantity", type="integer", nullable=true)
     */
    protected $deliveredQuantity;

    /**
     * @Column(column="note", type="string", nullable=true)
     * @FormElement(label="Note", type="Text", required=false)
     */
    protected $note;

    /**
     * @Column(column="created", type="dateTime", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_WAREHOUSE_ORDER_ID => true,
        self::PROPERTY_PRODUCT_SIZE_ID => true,
        self::PROPERTY_QUANTITY => true,
        self::PROPERTY_NOTE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getWarehouseOrderId()
    {
        return $this->warehouseOrderId;
    }

    /**
     * @param int $warehouseOrderId
     * @return $this
     */
    public function setWarehouseOrderId($warehouseOrderId)
    {
        $this->warehouseOrderId = null !== $warehouseOrderId ? (int)$warehouseOrderId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductSizeId()
    {
        return $this->productSizeId;
    }

    /**
     * @param int $productSizeId
     * @return $this
     */
    public function setProductSizeId($productSizeId)
    {
        $this->productSizeId = null !== $productSizeId ? (int)$productSizeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = null !== $quantity ? (int)$quantity : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getDeliveredQuantity()
    {
        return $this->deliveredQuantity;
    }

    /**
     * @param int|null $deliveredQuantity
     * @return $this
     */
    public function setDeliveredQuantity($deliveredQuantity)
    {
        $this->deliveredQuantity = $deliveredQuantity;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return null|WarehouseOrder
     *
     * @throws \DomainException
     */
    public function getWarehouseOrder()
    {
        return $this->getRelated(self::ALIAS_WAREHOUSE_ORDER);
    }

    /**
     * @param WarehouseOrder $warehouseOrder
     * @return $this
     */
    public function setWarehouseOrder(WarehouseOrder $warehouseOrder)
    {
        $this->warehouseOrderId = $warehouseOrder->getId();

        return $this;
    }

    /**
     * @return null|ProductSize
     *
     * @throws \DomainException
     */
    public function getProductSize()
    {
        return $this->getRelated(self::ALIAS_PRODUCT_SIZE);
    }

    /**
     * @param ProductSize $productSize
     * @return $this
     */
    public function setProductSize(ProductSize $productSize)
    {
        $this->productSizeId = $productSize->getId();

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string|DateTime $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    public function beforeCreate()
    {
        $date = new Vemid\Date\DateTime();
        $this->created = $date->getUnixFormat();
    }
}
