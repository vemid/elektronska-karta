<?php

use Vemid\Date\DateTime;
use Vemid\Helper\HashHelper;
use Vemid\Helper\ImageHelper;
use \Phalcon\Validation;
use Vemid\Helper\SiteHelper;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * User Model Class
 *
 * @Source('users')
 * @HasMany('id', 'PasswordReset', 'userId', {'alias':'PasswordResets'})
 * @HasMany('id', 'UserRoleAssignment', 'userId', {'alias':'UserRoleAssignments'})
 * @HasMany('id', 'UserNotification', 'userId', {'alias':'UserNotifications'})
 * @HasMany('id', 'UserSector', 'userId', {'alias':'UserSectors'})
 * @HasManyToMany('id', 'UserRoleAssignment', 'userId', 'roleId', 'Role', 'id', {'alias':'Roles'})
 */
class User extends AbstractEntity
{
    const PROPERTY_ID = 'id';
    const PROPERTY_FIRST_NAME = 'firstName';
    const PROPERTY_LAST_NAME = 'lastName';
    const PROPERTY_USERNAME = 'username';
    const PROPERTY_PASSWORD_HASH = 'passwordHash';
    const PROPERTY_EMAIL = 'email';
    const PROPERTY_AVATAR = 'avatar';
    const PROPERTY_GENDER = 'gender';
    const PROPERTY_ZONE_ID = 'zoneId';
    const PROPERTY_IS_ACTIVE = 'isActive';
    const PROPERTY_LAST_IP = 'lastIp';
    const PROPERTY_REGISTERED_DATETIME = 'registeredDatetime';
    const PROPERTY_LAST_VISIT_DATETIME = 'lastVisitDatetime';
    const PROPERTY_LANGUAGE = 'language';

    const GENDER_MALE = 'MALE';
    const GENDER_FEMALE = 'FEMALE';

    const LANGUGAGE_DE = 'de';
    const LANGUGAGE_SK = 'sk';
    const LANGUGAGE_EN = 'en';
    const LANGUGAGE_SR = 'sr';

    const ROLE_MEMBER = 'MEMBER';
    const ROLE_ADMIN = 'ADMIN';

    const ALIAS_PASSWORD_RESETS = 'PasswordResets';
    const ALIAS_ROLE = 'Roles';
    const ALIAS_USER_NOTIFICATIONS = 'UserNotifications';
    const ALIAS_USER_SECTORS = 'UserSectors';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="first_name", type="string", nullable=false)
     * @FormElement(label="First name", type="Text", required=true)
     */
    protected $firstName;

    /**
     * @Column(column="last_name", type="string", nullable=false)
     * @FormElement(label="Last name", type="Text", required=true)
     */
    protected $lastName;

    /**
     * @Column(column="username", type="string", nullable=false)
     * @FormElement(label="Username", type="Text", required=true)
     */
    protected $username;

    /**
     * @Column(column="email", type="string", nullable=false)
     * @FormElement(label="Email", type="Email", required=true)
     */
    protected $email;

    /**
     * @Column(column="password_hash", type="string", nullable=true)
     * @FormElement(label="Password", type="Password", required=false)
     */
    protected $passwordHash;

    /**
     * @Column(column="avatar", type="string", nullable=true)
     * @FormElement(label="Avatar", type="File", required=false)
     */
    protected $avatar;

    /**
     * @Column(column="gender", type="string", nullable=true)
     * @FormElement(label="Gender", type="Select", required=false, options={'MALE':'Male', 'FEMALE':'Female'})
     */
    protected $gender;

    /**
     * @Column(column="is_active", type="integer", nullable=true)
     * @FormElement(label="Is active", type="Check", required=false)
     */
    protected $isActive;

    /**
     * @Column(column="last_ip", type="string", nullable=true)
     */
    protected $lastIp;

    /**
     * @Column(column="registered_datetime", type="dateTime", nullable=true)
     */
    protected $registeredDatetime;

    /**
     * @Column(column="last_visit_datetime", type="dateTime", nullable=true)
     */
    protected $lastVisitDatetime;

    /**
     * @Column(column="language", type="string", nullable=true)
     * @FormElement(label="Langugage", type="Select", required=false, options={'sr':'Serbian', 'en':'English', 'de':'German','sk':'Slovakian'})
     */
    protected $language;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_FIRST_NAME => true,
        self::PROPERTY_LAST_NAME => true,
        self::PROPERTY_USERNAME => true,
        self::PROPERTY_EMAIL => true,
        self::PROPERTY_PASSWORD_HASH => false,
        self::PROPERTY_AVATAR => true,
        self::PROPERTY_GENDER => true,
        self::PROPERTY_ZONE_ID => true,
        self::PROPERTY_IS_ACTIVE => true,
        self::PROPERTY_LAST_IP => false,
        self::PROPERTY_REGISTERED_DATETIME => false,
        self::PROPERTY_LAST_VISIT_DATETIME => false,
        self::PROPERTY_LANGUAGE => false,
    );

    public static $genders = array(
        self::GENDER_FEMALE => 'Female',
        self::GENDER_MALE => 'Male',
    );

    public static $languages = array(
        self::LANGUGAGE_SR => 'Serbian',
        self::LANGUGAGE_EN => 'English',
        self::LANGUGAGE_DE => 'German',
        self::LANGUGAGE_SK => 'Slovakian',
    );

    /**
     * @return int
     */
    public function getId()
    {
        return !is_null($this->id) ? intval($this->id) : null;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = !is_null($id) ? intval($id) : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * @param string $passwordHash
     * @return $this
     */
    public function setPasswordHash($passwordHash)
    {
        $this->passwordHash = $passwordHash;

        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     * @return $this
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return (bool)$this->isActive;
    }

    /**
     * @param int|bool $isActive
     * @return $this
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive ? 1 : 0;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastIp()
    {
        return $this->lastIp;
    }

    /**
     * @param string $lastIp
     * @return $this
     */
    public function setLastIp($lastIp)
    {
        $this->lastIp = $lastIp;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getRegisteredDatetime()
    {
        return $this->registeredDatetime;
    }

    /**
     * @param string|DateTime $registeredDatetime
     * @return $this
     */
    public function setRegisteredDatetime($registeredDatetime)
    {
        $this->registeredDatetime = $registeredDatetime;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getLastVisitDatetime()
    {
        return $this->lastVisitDatetime;
    }

    /**
     * @param string|DateTime $lastVisitDatetime
     * @return $this
     */
    public function setLastVisitDatetime($lastVisitDatetime)
    {
        $this->lastVisitDatetime = $lastVisitDatetime;

        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $hashHelper = new HashHelper();
        $this->passwordHash = $hashHelper->generatePasswordHash($password);

        return $this;
    }

    /**
     * @param null $arguments
     * @return Resultset|Role[]
     */
    public function getRoles($arguments = null)
    {
        return $this->getRelated(self::ALIAS_ROLE, $arguments);
    }

    /**
     * @param null|int|string|array $arguments
     * @return Resultset|UserRoleAssignment[]
     */
    public function getUserRoleAssignments($arguments = null)
    {
        return $this->getRelated('UserRoleAssignments', $arguments);
    }

    /**
     * @param null|int|string|array $arguments
     * @return Resultset|UserSector[]
     */
    public function getUserSectors($arguments = null)
    {
        return $this->getRelated(self::ALIAS_USER_SECTORS, $arguments);
    }

    /**
     * @return string
     */
    public function getAvatarUrl()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $siteHelper = new SiteHelper([
            'baseUri' => $config->application->baseUri,
            'siteName' => $config->application->siteName,
        ]);

        if (is_file($this->getUploadPath() . $this->getAvatar())) {
            return $siteHelper->prepUrl('uploads/avatar/' . $this->getAvatar());
        }

        return $siteHelper->prepUrl('img/avatar.png');
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return APP_PATH . 'var/uploads/avatar/';
    }

    /**
     * @param \Phalcon\Http\Request\File $file
     */
    public function replaceAvatar(\Phalcon\Http\Request\File $file)
    {
        $filePath = $this->getUploadPath() . substr(sha1(mt_rand()), 0, 8) . '.' . $file->getExtension();
        if ($file->moveTo($filePath)) {

            if (is_file($currentAvatar = $this->getUploadPath() . $this->getAvatar())) {
                @unlink($currentAvatar);
            }

            $imageHelper = new ImageHelper($filePath);
            $imageHelper->resizeAndCrop(400, 400);

            $this->setAvatar(basename($filePath));
            $this->save();
        }
    }

    /**
     * @param string $username
     * @param string $password
     * @return $this|null
     */
    public static function authenticateUser($username, $password)
    {
        if (!$username || !$password) {
            return null;
        }

        $user = self::findFirst([
            self::PROPERTY_USERNAME . ' = :username:',
            'bind' => [
                'username' => $username,
            ]
        ]);

        if (!$user) {
            return null;
        }

        return (new HashHelper())->verifyPassword($password, $user->getPasswordHash()) ? $user : null;
    }

    /**
     * @param string $password
     * @return bool
     */
    public function isValidPassword($password)
    {
        $hashHelper = new HashHelper();
        return $this->passwordHash === $hashHelper->generatePasswordHash($password);
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|PasswordReset[]
     */
    public function getPasswordResets($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PASSWORD_RESETS, $arguments);
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|UserNotification[]
     */
    public function getUserNotifications($arguments = null)
    {
        return $this->getRelated(self::ALIAS_USER_NOTIFICATIONS, $arguments);
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return trim($this->firstName) . ' ' . trim($this->lastName);
    }

    /**
     * @return string
     */
    public function getGenderDisplayName()
    {
        if (array_key_exists($this->gender, self::$genders)) {
            return self::$genders[$this->gender];
        }

        return ucfirst(strtolower($this->gender));
    }


    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $isDeletable = parent::beforeDelete();

        if ($isDeletable) {
            if (is_file($avatar = $this->getUploadPath() . $this->avatar)) {
                @unlink($avatar);
            }
        }

        return $isDeletable;
    }

    public function beforeValidationOnCreate()
    {
        $this->setRegisteredDatetime(new DateTime());

        if (!$this->getPasswordHash()) {
            $hashHelper = new HashHelper();
            $this->setPassword($hashHelper->generateHash(time(), 'md5'));
        }
    }

    /**
     * @return \Phalcon\Mvc\Model
     */
    public function validation()
    {
        $validator = new Validation();
        $validator->add(self::PROPERTY_USERNAME, new Validation\Validator\Uniqueness([
            'model' => $this,
            'message' => $this->_translate('This email address is already in use.')
        ]));

        return $this->validate($validator);
    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        foreach ($this->getRoles() as $role) {
            if ($role->getCode() === Role::ROLE_SUPER_ADMIN) {
                return true;
            }
        }

        return false;
    }

    public function isClient()
    {
        foreach ($this->getRoles() as $role) {
            if ($role->getCode() === Role::ROLE_CLIENT) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isMolder()
    {
        foreach ($this->getUserSectors() as $userSector) {
            if (!$code = $userSector->getCode()) {
                continue;
            }

            if ((int)$code->getCode() === 2) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isDesigner()
    {
        foreach ($this->getUserSectors() as $userSector) {
            if (!$code = $userSector->getCode()) {
                continue;
            }

            if ((int)$code->getCode() === 1) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isShop()
    {
        foreach ($this->getRoles() as $role) {
            if ($role->getCode() === Role::ROLE_SHOP) {
                return true;
            }
        }

        return false;
    }

    public function getRolesName()
    {
        $rolesArray = $this->getRoles()->toArray();
        $lastChunk = \Vemid\Helper\TextHelper::codeToString(array_pop($rolesArray)['name']);
        $roles = '';
        foreach ($rolesArray as $role) {
            $roles .= \Vemid\Helper\TextHelper::codeToString($role['name']) . ', ';
        }

        return $roles . $lastChunk;
    }
}
