<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * ProductPrice Model Class
 *
 * @Source('product_prices')
 *
 * @BelongsTo('pricelistId', 'Pricelist', 'id', {'alias':'Pricelist'})
 * @BelongsTo('productId', 'Product', 'id', {'alias':'Product'})
 */
class ProductPrice extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PRICELIST_ID = 'pricelistId';
    const PROPERTY_PRODUCT_ID = 'productId';
    const PROPERTY_STATUS = 'status';
    const PROPERTY_NAB_CEN = 'nabCen';
    const PROPERTY_VEL_CEN = 'velCen';
    const PROPERTY_MAL_CEN = 'malCen';
    const PROPERTY_IS_SYNCED = 'isSynced';
    const PROPERTY_EDITED = 'edited';
    const PROPERTY_CREATED = 'created';

    const ALIAS_PRICELIST = 'Pricelist';
    const ALIAS_PRODUCT = 'Product';

    const STATUS_PLANED = 'PLANED';
    const STATUS_FINAL = 'FINAL';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="pricelist_id", type="integer", nullable=false)
     */
    protected $pricelistId;

    /**
     * @Column(column="product_id", type="integer", nullable=false)
     */
    protected $productId;

    /**
     * @Column(column="status", type="string", nullable=true)
     */
    protected $status;

    /**
     * @Column(column="purchase_price", type="decimal", nullable=false)
     * @FormElement(label="Nab cen", type="Text", required=true)
     */
    protected $purchasePrice;

    /**
     * @Column(column="wholesale_price", type="decimal", nullable=false)
     * @FormElement(label="Vel cen", type="Text", required=true)
     */
    protected $wholesalePrice;

    /**
     * @Column(column="retail_price", type="decimal", nullable=false)
     * @FormElement(label="Mal cen", type="Text", required=true)
     */
    protected $retailPrice;

    /**
     * @Column(column="edited", type="dateTime", nullable=true)
     */
    protected $edited;

    /**
     * @Column(column="is_synced", type="bool", nullable=true)
     */
    protected $isSynced;


    /**
     * @Column(column="created", type="string", nullable=false)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRICELIST_ID => true,
        self::PROPERTY_PRODUCT_ID => true,
        self::PROPERTY_NAB_CEN => true,
        self::PROPERTY_VEL_CEN => true,
        self::PROPERTY_MAL_CEN => true,
        self::PROPERTY_EDITED => true,
        self::PROPERTY_CREATED => true,
        self::ALIAS_PRICELIST => true,
        self::ALIAS_PRODUCT => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getPricelistId()
    {
        return $this->pricelistId;
    }

    /**
     * @param int $pricelistId
     * @return $this
     */
    public function setPricelistId($pricelistId)
    {
        $this->pricelistId = null !== $pricelistId ? (int)$pricelistId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->productId = null !== $productId ? (int)$productId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return float
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * @param float $purchasePrice
     * @return $this
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = null !== $purchasePrice ? (float)$purchasePrice : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getWholesalePrice()
    {
        return $this->wholesalePrice;
    }

    /**
     * @param float $wholesalePrice
     * @return $this
     */
    public function setWholesalePrice($wholesalePrice)
    {
        $this->wholesalePrice = null !== $wholesalePrice ? (float)$wholesalePrice : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getRetailPrice()
    {
        return $this->retailPrice;
    }

    /**
     * @param float $retailPrice
     * @return $this
     */
    public function setRetailPrice($retailPrice)
    {
        $this->retailPrice = null !== $retailPrice ? (float)$retailPrice : null;

        return $this;
    }
    
    /**
     * @return boolean
     */
    public function getIsSynced()
    {
        return $this->isSynced;
    }

    /**
     * @param boolean $isSynced
     */
    public function setIsSynced($isSynced)
    {
        $this->isSynced = $isSynced;
    }


    /**
     * @return null|DateTime
     */
    public function getEdited()
    {
        return $this->edited;
    }

    /**
     * @param string|DateTime $edited
     * @return $this
     */
    public function setEdited($edited)
    {
        $this->edited = $edited;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|Pricelist
     * 
     * @throws \DomainException
     */
    public function getPricelist()
    {
        return $this->getRelated(self::ALIAS_PRICELIST);
    }

    /**
     * @param Pricelist $pricelist
     * @return $this
     */
    public function setPricelist(Pricelist $pricelist)
    {
        $this->pricelistId = $pricelist->getId();

        return $this;
    }

    /**
     * @return null|Product
     * 
     * @throws \DomainException
     */
    public function getProduct()
    {
        return $this->getRelated(self::ALIAS_PRODUCT);
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->productId = $product->getId();

        return $this;
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }

    public function beforeUpdate()
    {
        $this->edited = new DateTime();


    }

    public function beforeCreate()
    {
        $this->isSynced = 0;
    }
}
