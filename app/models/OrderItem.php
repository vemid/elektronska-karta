<?php

use \Vemid\Entity\EntityInterface;
use Vemid\Entity\AbstractEntity;
use \Vemid\Entity\Type;

/**
 * OrderItem Model Class
 *
 * @Source('order_items')
 *
 * @HasOne('id', 'DeliveryNoteItem', 'orderItemId', {'alias':'DeliveryNoteItem'})
 *
 * @BelongsTo('productSizeId', 'ProductSize', 'id', {'alias':'ProductSize'})
 * @BelongsTo('orderId', 'Order', 'id', {'alias':'Order'})
 * @BelongsTo('clientOrderItemId', 'ClientOrderItem', 'id', {'alias':'ClientOrderItem'})
 */
class OrderItem extends AbstractEntity
{
    const PROPERTY_ID = 'id';
    const PROPERTY_ORDER_ID = 'orderId';
    const PROPERTY_CLIENT_ORDER_ITEM_ID = 'clientOrderItemId';
    const PROPERTY_PRODUCT_SIZE_ID = 'productSizeId';
    const PROPERTY_QUANTITY_A = 'quantityA';
    const PROPERTY_QUANTITY_B = 'quantityB';
    const PROPERTY_ENTITY_TYPE_ID = 'entityTypeId';
    const PROPERTY_ENTITY_ID = 'entityId';

    const ALIAS_ORDER = 'Order';
    const ALIAS_PRODUCT_SIZE = 'ProductSize';
    const ALIAS_CLIENT_ORDER_ITEM = 'ClientOrderItem';
    const ALIAS_DELIVERY_NOTE_ITEM = 'DeliveryNoteItem';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="order_id", type="integer", nullable=false)
     * @FormElement(label="Order", type="Select", required=true, relation="Order")
     */
    protected $orderId;

    /**
     * @Column(column="client_order_item_id", type="integer", nullable=true)
     */
    protected $clientOrderItemId;

    /**
     * @Column(column="product_size_id", type="integer", nullable=false)
     * @FormElement(label="Product size", type="Select", required=true, relation="ProductSize")
     */
    protected $productSizeId;

    /**
     * @Column(column="quantity_a", type="integer", nullable=true)
     * @FormElement(label="Quantity a", type="Text", required=false)
     */
    protected $quantityA;

    /**
     * @Column(column="quantity_b", type="integer", nullable=true)
     * @FormElement(label="Quantity b", type="Text", required=false)
     */
    protected $quantityB;

    /**
     * @Column(column="entity_type_id", type="string", nullable=false)
     * @FormElement(label="Entity type", type="Text", required=true)
     */
    protected $entityTypeId;

    /**
     * @Column(column="entity_id", type="string", nullable=false)
     * @FormElement(label="Entity", type="Text", required=true)
     */
    protected $entityId;

    /**
     * @Column(column="created", type="dateTime", nullable=true)
     */
    protected $created;

    /**
     * @Column(column="updated", type="dateTime", nullable=true)
     */
    protected $updated;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRODUCT_SIZE_ID => true,
        self::PROPERTY_QUANTITY_A => true,
        self::PROPERTY_QUANTITY_B => true,
        self::PROPERTY_ENTITY_TYPE_ID => true,
        self::PROPERTY_ENTITY_ID => true,
        self::ALIAS_PRODUCT_SIZE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getClientOrderItemId()
    {
        return $this->clientOrderItemId;
    }

    /**
     * @param mixed $clientOrderItemId
     */
    public function setClientOrderItemId($clientOrderItemId)
    {
        $this->clientOrderItemId = $clientOrderItemId;

        return;$this;
    }

    /**
     * @return int
     */
    public function getProductSizeId()
    {
        return $this->productSizeId;
    }

    /**
     * @param int $productSizeId
     * @return $this
     */
    public function setProductSizeId($productSizeId)
    {
        $this->productSizeId = null !== $productSizeId ? (int)$productSizeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantityA()
    {
        return $this->quantityA;
    }

    /**
     * @param int $quantityA
     * @return $this
     */
    public function setQuantityA($quantityA)
    {
        $this->quantityA = null !== $quantityA ? (int)$quantityA : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantityB()
    {
        return $this->quantityB;
    }

    /**
     * @param int $quantityB
     * @return $this
     */
    public function setQuantityB($quantityB)
    {
        $this->quantityB = null !== $quantityB ? (int)$quantityB : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param string $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityTypeId()
    {
        return $this->entityTypeId;
    }

    /**
     * @param int $entityTypeId
     * @return $this
     */
    public function setEntityTypeId($entityTypeId)
    {
        $this->entityTypeId = $entityTypeId;

        return $this;
    }

    /**
     * @return null|EntityInterface|Client|Code
     */
    public function getClient()
    {
        return Type::getEntity($this->getEntityTypeId(),$this->getEntityId());
    }

    /**
     * @param EntityInterface $entity
     * @return $this
     */
    public function setClient(EntityInterface $entity)
    {
        $this->entityTypeId = $entity->getEntityTypeId();
        $this->entityId = $entity->getEntityId();

        return $this;
    }

    /**
     * @return null|\Vemid\Entity\EntityInterface|Order
     *
     * @throws \DomainException
     */
    public function getOrder()
    {
        return $this->getRelated(self::ALIAS_ORDER);
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function setOrder(Order $order)
    {
        $this->orderId = $order->getId();

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string|DateTime $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param string|DateTime $updated
     * @return $this
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @return null|\Vemid\Entity\EntityInterface|ProductSize
     *
     * @throws \DomainException
     */
    public function getProductSize()
    {
        return $this->getRelated(self::ALIAS_PRODUCT_SIZE);
    }

    /**
     * @param ProductSize $productSize
     * @return $this
     */
    public function setProductSize(ProductSize $productSize)
    {
        $this->productSizeId = $productSize->getId();

        return $this;
    }

    /**
     * @return null|\Vemid\Entity\EntityInterface|ClientOrderItem
     *
     * @throws \DomainException
     */
    public function getClientOrderItem()
    {
        return $this->getRelated(self::ALIAS_CLIENT_ORDER_ITEM);
    }

    /**
     * @param ClientOrderItem $clientOrderItem
     * @return $this
     */
    public function setClientOrderItem(ClientOrderItem $clientOrderItem)
    {
        $this->clientOrderItemId = $clientOrderItem->getId();

        return $this;
    }

    /**
     * @return null|\Vemid\Entity\EntityInterface|DeliveryNoteItem
     *
     * @throws \DomainException
     */
    public function getDeliveryNoteItem()
    {
        return $this->getRelated(self::ALIAS_DELIVERY_NOTE_ITEM);
    }

    public function beforeUpdate()
    {
        $date = new Vemid\Date\DateTime();
        $this->updated = $date->getUnixFormat();
    }

    public function beforeCreate()
    {
        $date = new Vemid\Date\DateTime();
        $this->created = $date->getUnixFormat();
    }
}
