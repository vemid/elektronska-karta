<?php

use Vemid\Entity\AbstractEntity;

/**
 * UserRoleAssignment Model Class
 *
 * @Source('user_role_assignments')
 *
 * @BelongsTo('userId', 'User', 'id', {'alias':'User'})
 * @BelongsTo('roleId', 'Role', 'id', {'alias':'Role'})
 */
class UserRoleAssignment extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_USER_ID = 'userId';
    const PROPERTY_ROLE_ID = 'roleId';

    const ALIAS_USER = 'User';
    const ALIAS_ROLE = 'Role';
    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;
    /**
     * @Column(column="user_id", type="integer", nullable=true)
     * @FormElement(label="User", type="Select", required=false, relation="User")
     */
    protected $userId;
    /**
     * @Column(column="role_id", type="integer", nullable=true)
     * @FormElement(label="Role", type="Select", required=false, relation="Role")
     */
    protected $roleId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id === null ? null : (int)$this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id === null ? null : (int)$id;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId === null ? null : (int)$this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = $userId === null ? null : (int)$userId;

        return $this;
    }

    /**
     * @return int
     */
    public function getRoleId()
    {
        return $this->roleId === null ? null : (int)$this->roleId;
    }

    /**
     * @param int $roleId
     * @return $this
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId === null ? null : (int)$roleId;

        return $this;
    }

    /**
     * @return bool|User
     */
    public function getUser()
    {
        return $this->getRelated(self::ALIAS_USER);
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->userId = $user->getId();

        return $this;
    }

    /**
     * @return bool|Role
     */
    public function getRole()
    {
        return $this->getRelated(self::ALIAS_ROLE);
    }

    /**
     * @param Role $role
     * @return $this
     */
    public function setRole(Role $role)
    {
        $this->roleId = $role->getId();

        return $this;
    }

}
