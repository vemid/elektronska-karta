<?php

use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * WarehouseOrderGroupItem Model Class
 *
 * @Source('warehouse_order_group_items')
 * @BelongsTo('warehouseOrderGroupId', 'WarehouseOrderGroup', 'id')
 * @BelongsTo('warehouseOrderId', 'WarehouseOrder', 'id')
 */
class WarehouseOrderGroupItem extends AbstractEntity
{
    const PROPERTY_ID = 'id';
    const PROPERTY_WAREHOUSE_ORDER_ID = 'warehouseOrderId';
    const PROPERTY_WAREHOUSE_ORDER_GROUP_ID = 'warehouseOrderGroupId';


    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="warehouse_order_id", type="integer", nullable=false)
     * @FormElement(label="Warehouse order", type="Text", required=true)
     */
    protected $warehouseOrderId;

    /**
     * @Column(column="warehouse_order_group_id", type="integer", nullable=false)
     * @FormElement(label="Warehouse order group", type="Text", required=true)
     */
    protected $warehouseOrderGroupId;

    const ALIAS_WAREHOUSE_ORDER_GROUP = 'WarehouseOrderGroup';
    const ALIAS_WAREHOUSE_ORDER = 'WarehouseOrder';

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_WAREHOUSE_ORDER_ID => true,
        self::PROPERTY_WAREHOUSE_ORDER_GROUP_ID => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getWarehouseOrderId()
    {
        return $this->warehouseOrderId;
    }

    /**
     * @param int $warehouseOrderId
     * @return $this
     */
    public function setWarehouseOrderId($warehouseOrderId)
    {
        $this->warehouseOrderId = null !== $warehouseOrderId ? (int)$warehouseOrderId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getWarehouseOrderGroupId()
    {
        return $this->warehouseOrderGroupId;
    }

    /**
     * @param int $warehouseOrderGroupId
     * @return $this
     */
    public function setWarehouseOrderGroupId($warehouseOrderGroupId)
    {
        $this->warehouseOrderGroupId = null !== $warehouseOrderGroupId ? (int)$warehouseOrderGroupId : null;

        return $this;
    }

    /**
     * @return null|WarehouseOrder
     *
     * @throws \DomainException
     */
    public function getWarehouseOrder()
    {
        return $this->getRelated(self::ALIAS_WAREHOUSE_ORDER);
    }

    /**
     * @param WarehouseOrder $warehouseOrder
     * @return $this
     */
    public function setWarehouseOrder(WarehouseOrder $warehouseOrder)
    {
        $this->warehouseOrderId = $warehouseOrder->getId();

        return $this;
    }

    /**
     * @return null|WarehouseOrderGroup
     *
     * @throws \DomainException
     */
    public function getWarehouseOrderGroup()
    {
        return $this->getRelated(self::ALIAS_WAREHOUSE_ORDER_GROUP);
    }

    /**
     * @param WarehouseOrderGroup $warehouseOrderGroup
     * @return $this
     */
    public function setWarehouseOrderGroup(WarehouseOrderGroup $warehouseOrderGroup)
    {
        $this->warehouseOrderGroupId = $warehouseOrderGroup->getId();

        return $this;
    }
}
