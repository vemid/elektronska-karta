<?php

use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;
use \Vemid\Service\Product\Tags;

/**
 * Product Model Class
 *
 * @Source('products')
 *
 * @HasMany('id', 'ProductClassification', 'productId', {'alias':'ProductClassifications'})
 * @HasMany('id', 'Product', 'parentId', {'alias':'ChildProducts'})
 * @HasMany('id', 'ProductSize', 'productId', {'alias':'ProductSizes'})
 * @HasMany('id', 'ElectronCard', 'productId', {'alias':'ElectronCards'})
 * @HasMany('id', 'ProductCalculation', 'productId', {'alias':'ProductCalculations'})
 * @HasMany('id', 'ProductPrice', 'productId', {'alias':'ProductPrices'})
 * @HasMany('id', 'MerchandiseCalculationItem', 'productId', {'alias':'MerchandiseCalculationItems'})
 * @HasOne('id', 'FarbenCard', 'productId', {'alias':'FarbenCards'})
 *
 * @HasMany('id', 'Questionnaire', 'productId', {'alias':'Questionnaires'})
 * @HasMany('id', 'OperatingList', 'productId', {'alias':'OperatingLists'})
 *
 * @BelongsTo('parentId', 'Product', 'id', {'alias':'ParentProduct'})
 * @BelongsTo('productCodeTypeId', 'Code', 'id', {'alias':'ProductCodeType'})
 * @BelongsTo('userCreatedId', 'User', 'id', {'alias':'UserCreated'})
 * @BelongsTo('priorityId', 'Priority', 'id', {'alias':'Priority'})
 * @BelongsTo('originCodeId', 'Code', 'id', {'alias':'OriginCode'})
 */
class Product extends AbstractEntity
{
    const STATUS_READY = 'U_PROCESU';
    const STATUS_CANCELED = 'OTKAZAN';
    const STATUS_FINISHED = 'KOMPLETIRAN';

    const FIRST = 'FIRST';
    const SECOND = 'SECOND';
    const THIRD = 'THIRD';
    const FOURTH = 'FOURTH';
    const FIFTH = 'FIFTH';
    const SIXTH = 'SIXTH';
    const SEVENTH = 'SEVENTH';
    const EIGHTH = 'EIGHTH';
    const NINTH = 'NINTH';

    const PROPERTY_ID = 'id';
    const PROPERTY_CODE = 'code';
    const PROPERTY_NAME = 'name';
    const PROPERTY_WEB_NAME = 'webName';
    const PROPERTY_PRODUCT_CODE_TYPE_ID = 'productCodeTypeId';
    const PROPERTY_PRODUCT_ORIGIN_CODE_ID = 'originCodeId';
    const PROPERTY_PARENT_ID = 'parenId';
    const PROPERTY_PRIORITY_ID = 'priorityId';
    const PROPERTY_PRODUCE_TYPE = 'produceType';
    const PROPERTY_STATUS = 'status';
    const PROPERTY_USER_CREATED_ID = 'userCreatedId';
    const PROPERTY_IMAGE = 'image';
    const PROPERTY_IS_ORDERABLE = 'isOrderable';
    const PROPERTY_IS_PRICE = 'price';
    const PROPERTY_CREATED = 'created';
    const PROPERTY_SUPPLIER_CODE = 'supplierCode';
    const PROPERTY_IS_SYNCED = 'isSynced';
    const PROPERTY_IS_EUR = 'isEur';
    const PROPERTY_COMPOSITION = 'composition';

    const ALIAS_PARENT_PRODUCT = 'ParentProduct';
    const ALIAS_CHILD_PRODUCTS = 'ChildProducts';
    const ALIAS_PRODUCT_CODE_TYPE = 'ProductCodeType';
    const ALIAS_USER_CREATED = 'UserCreated';
    const ALIAS_PRODUCT_CLASSIFICATION = 'ProductClassifications';
    const ALIAS_PRODUCT_SIZES = 'ProductSizes';
    const ALIAS_ELECTRON_CARDS = 'ElectronCards';
    const ALIAS_PRODUCT_CALCULATIONS = 'ProductCalculations';
    const ALIAS_PRODUCT_PRICES = 'ProductPrices';
    const ALIAS_MERCHANDISE_CALCULATIONS_ITEMS = 'MerchandiseCalculationItems';
    const ALIAS_QUESTIONNAIRES = 'Questionnaires';
    const ALIAS_OPERATING_LISTS = 'OperatingLists';
    const ALIAS_PRIORITY = 'Priority';
    const ALIAS_FARBEN_CARDS = 'FarbenCards';
    const ALIAS_ORIGIN = 'OriginCode';



    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="code", type="string", nullable=false)
     * @FormElement(label="Code", type="Text", required=true)
     */
    protected $code;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="web_name", type="string", nullable=true)
     */
    protected $webName;

    /**
     * @Column(column="product_code_type_id", type="integer", nullable=false)
     * @FormElement(label="Product code type", type="Text", required=true)
     */
    protected $productCodeTypeId;

    /**
     * @Column(column="parent_id", type="integer", nullable=true)
     * @FormElement(label="Parent Product", type="Text", required=false)
     */
    protected $parentId;

    /**
     * @Column(column="produce_type", type="string", nullable=true)
     * @FormElement(label="Produce type", type="Text", required=false)
     */
    protected $produceType;

    /**
     * @Column(column="priority_id", type="integer", nullable=false)
     */
    protected $priorityId;

    /**
     * @Column(column="priority", type="string", nullable=true)
     */
    protected $priority;

    /**
     * @Column(column="status", type="string", nullable=true)
     * @FormElement(label="Status", type="Text", required=false)
     */
    protected $status;

    /**
     * @Column(column="user_created_id", type="integer", nullable=false)
     * @FormElement(label="User created", type="Text", required=true)
     */
    protected $userCreatedId;

    /**
     * @Column(column="image", type="string", nullable=true)
     * @FormElement(label="Image", type="Text", required=false)
     */
    protected $image;

    /**
     * @Column(column="price", type="decimal", nullable=true)
     * @FormElement(label="Image", type="Text", required=false)
     */
    protected $price;

    /**
     * @Column(column="is_orderable", type="bool", nullable=true)
     */
    protected $isOrderable;

    /**
     * @Column(column="created", type="dateTime", nullable=true)
     */
    protected $created;

    /**
     * @Column(column="supplier_code", type="string", nullable=true)
     * @FormElement(label="Sifra Dobavljaca", type="Text", required=false)
     */
    protected $supplierCode;

    /**
     * @Column(column="is_synced", type="bool", nullable=true)
     */
    protected $isSynced;

    /**
     * @Column(column="is_eur", type="bool", nullable=true)
     */
    protected $isEur;

    /**
     * @Column(column="composition", type="string", nullable=true)
     */
    protected $composition;

    /**
     * @Column(column="wash_label", type="string", nullable=true)
     */
    protected $washLabel;

    /**
     * @Column(column="origin_code_id", type="integer", nullable=true)
     */
    protected $originCodeId;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CODE => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_WEB_NAME => true,
        self::PROPERTY_PRODUCT_CODE_TYPE_ID => true,
        self::PROPERTY_PRODUCE_TYPE => true,
        self::PROPERTY_STATUS => true,
        self::PROPERTY_USER_CREATED_ID => true,
        self::PROPERTY_IS_ORDERABLE => true,
        self::PROPERTY_CREATED =>true,
        self::PROPERTY_SUPPLIER_CODE =>true,
        self::PROPERTY_PRODUCT_ORIGIN_CODE_ID =>true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getWebName()
    {
        return $this->webName;
    }

    /**
     * @param string $webName
     * @return $this
     */
    public function setWebName($webName)
    {
        $this->webName = $webName;

        return $this;
    }


    /**
     * @return int
     */
    public function getProductCodeTypeId()
    {
        return $this->productCodeTypeId;
    }

    /**
     * @param int $productCodeTypeId
     * @return $this
     */
    public function setProductCodeTypeId($productCodeTypeId)
    {
        $this->productCodeTypeId = null !== $productCodeTypeId ? (int)$productCodeTypeId : null;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param int $parentId
     * @return $this
     */
    public function setParentId($parentId)
    {
        $this->parentId =  $parentId ? (int)$parentId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getProduceType()
    {
        return $this->produceType;
    }

    /**
     * @param string $produceType
     * @return $this
     */
    public function setProduceType($produceType)
    {
        $this->produceType = $produceType;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriorityId()
    {
        return $this->priorityId;
    }

    /**
     * @param int $priorityId
     * @return $this
     */
    public function setPriorityId($priorityId)
    {
        $this->priorityId = null !== $priorityId ? (int)$priorityId : null;

        return $this;
    }

//    /**
//     * @return string
//     */
//    public function getPriority()
//    {
//        return $this->priority;
//    }

    /**
     * @param string $priority
     * @return $this
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }


    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->userCreatedId;
    }

    /**
     * @param int $userCreatedId
     * @return $this
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->userCreatedId = null !== $userCreatedId ? (int)$userCreatedId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsOrderable()
    {
        return $this->isOrderable;
    }

    /**
     * @param boolean $isOrderable
     */
    public function setIsOrderable($isOrderable)
    {
        $this->isOrderable = $isOrderable;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        /** @var \Pricelist $priceListEUR */
        $priceListEUR = $this->getDI()->getEntityManager()->findOne(\Pricelist::class, [
            'name = :name:',
            'bind' => [
                'name' => "DEVIZNI"
            ]
        ]);

        $service = $this->getDI()->getProductPrices();

        if($service->getWholesalePrice($priceListEUR,$this)) {
            return $service->getWholesalePrice($priceListEUR,$this);
        }
        else return $this->price;
    }

    /**
     * @param string $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return null|\Vemid\Date\DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string|DateTime $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return string
     */
    public function getSupplierCode()
    {
        return $this->supplierCode;
    }

    /**
     * @param string $supplierCode
     * @return $this
     */
    public function setSupplierCode($supplierCode)
    {
        $this->supplierCode = $supplierCode;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSynced()
    {
        return $this->isSynced;
    }

    /**
     * @param boolean $isSynced
     */
    public function setIsSynced($isSynced)
    {
        $this->isSynced = $isSynced;
    }

    /**
     * @return boolean
     */
    public function getIsEur()
    {
        return $this->isEur;
    }

    /**
     * @param boolean $isEur
     */
    public function setIsEur($isEur)
    {
        $this->isEur = $isEur;
    }

    /**
     * @return string
     */
    public function getComposition()
    {
        return $this->composition;
    }

    /**
     * @param string $composition
     * @return $this
     */
    public function setComposition($composition)
    {
        $this->composition = $composition;

        return $this;
    }


    /**
     * @return null|User
     *
     * @throws DomainException
     */
    public function getUser()
    {
        return $this->getRelated(self::ALIAS_USER_CREATED);
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->userCreatedId = $user->getId();

        return $this;
    }

    /**
     * @return null|Code
     *
     * @throws DomainException
     */
    public function getProductCodeType()
    {
        return $this->getRelated(self::ALIAS_PRODUCT_CODE_TYPE);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setProductCodeType(Code $code)
    {
        $this->productCodeTypeId = $code->getId();

        return $this;
    }

    /**
     * @return null|Product
     *
     * @throws DomainException
     */
    public function getParentProduct()
    {
        return $this->getRelated(self::ALIAS_PARENT_PRODUCT);
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setParentProduct(Product $product)
    {
        $this->parentId = $product->getId();

        return $this;
    }

    public function beforeCreate()
    {
        $this->isSynced = 0;
        $this->isEur = 0;
        $this->created = (new DateTime())->format("Y-m-d H:i:s");
    }

    public function beforeSave()
    {
        $this->code = str_replace('Ž', 'Z', $this->code);
        $this->name = strtoupper($this->name);
        if (null === $this->isSynced) {
            $this->isSynced = false;
            $this->isEur = false;
        }
    }

    /**
     * @return string
     */
    public function getWashLabel()
    {
        return $this->washLabel;
    }

    /**
     * @param string $washLabel
     * @return $this
     */
    public function setWashLabel($washLabel)
    {
        $this->washLabel = $washLabel;

        return $this;
    }

    /**
     * @return int
     */
    public function getOriginCodeId()
    {
        return $this->originCodeId;
    }

    /**
     * @param int $originCodeId
     * @return $this
     */
    public function setOriginCodeId($originCodeId)
    {
        $this->originCodeId = null !== $originCodeId ? (int)$originCodeId : null;

        return $this;
    }

    public function beforeUpdate()
    {
        $this->name = strtoupper($this->name);
        $webName = "";

        $hydrator = new \Vemid\Service\Product\Hydrator($this,$this->getDI()->getEntityManager());

        $model = str_replace("_"," ",array_pop(explode(' ', $hydrator->getProductGroup()->getName())));
        $bred = $hydrator->getBrand();
        $name = array_pop(explode(' ', $this->getName()));

        if($hydrator->getGenderName() == "Muski") {
            $gender = "za dečake";
        }
        elseif ($hydrator->getGenderName() == "Zenski") {
            $gender = "za devojčice";
        }
        elseif($hydrator->getGenderName() == "Unisex") {
            $gender = "Unisex";
        }
        else $gender="";

        if($this->getProductCodeTypeId() == '66'){
        $webName = ucwords($model) . " " . $gender . " " . ucwords($name);
        }
        elseif ($this->getProductCodeTypeId() == '68') {
            $webName = ucwords($model) . " " . $gender . " " . ucwords($hydrator->getBrand());
        }
        else $webName = $this->getName();

        $this->setWebName(str_replace("  "," ",$webName));

        if ($this->hasChanged('code')) {
            $this->setIsSynced(false);

            $oldData = $this->getOldSnapshotData();



            //$disableProductOnMis = ne w \Vemid\Service\MisWsdl\DisableProductAfterUpdate($oldData['code']);
            //$disableProductOnMis->run();
        }
    }

    /**
     * @param array|null $arguments
     * @return Product[]|Resultset
     *
     * @throws DomainException
     */
    public function getChildProducts($arguments = null)
    {
        return $this->getRelated(self::ALIAS_CHILD_PRODUCTS, $arguments);
    }

    /**
     * @param array|null $arguments
     * @return ProductClassification[]|Resultset
     *
     * @throws DomainException
     */
    public function getProductClassifications($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRODUCT_CLASSIFICATION, $arguments);
    }

    /**
     * @param array|null $arguments
     * @return ProductSize[]|Resultset
     *
     * @throws DomainException
     */
    public function getProductSizes($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRODUCT_SIZES, $arguments);
    }

    /**
     * @param array|null $arguments
     * @return ElectronCard[]|Resultset
     *
     * @throws DomainException
     */
    public function getElectronCards($arguments = null)
    {
        return $this->getRelated(self::ALIAS_ELECTRON_CARDS, $arguments);
    }

    /**
     * @param null $arguments
     * @return null|Resultset|\Vemid\Entity\EntityInterface
     *
     * @throws DomainException
     */
    public function getProductCalculations($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRODUCT_CALCULATIONS,$arguments);
    }

    /**
     * @param array|null $arguments
     * @return Questionnaire[]|Resultset
     *
     * @throws DomainException
     */
    public function getQuestionnaires($arguments = null)
    {
        return $this->getRelated(self::ALIAS_QUESTIONNAIRES, $arguments);
    }

    /**
     * @param array|null $arguments
     * @return OperatingList[]|Resultset
     *
     * @throws DomainException
     */
    public function getOperatingLists($arguments = null)
    {
        return $this->getRelated(self::ALIAS_OPERATING_LISTS, $arguments);
    }

    /**
     * @param array|null $arguments
     * @return ProductPrice[]|Resultset
     *
     * @throws DomainException
     */
    public function getProductPrices($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRODUCT_PRICES, $arguments);
    }

    /**
     * @param array|null $arguments
     * @return MerchandiseCalculationItem[]|Resultset
     *
     * @throws DomainException
     */
    public function getMerchandiseCalculationsItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_MERCHANDISE_CALCULATIONS_ITEMS, $arguments);
    }

    /**
     * @param array|null $arguments
     * @return Priority|null
     *
     * @throws DomainException
     */
    public function getPriority($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRIORITY, $arguments);
    }


    /**
     * @return null|Code
     *
     * @throws DomainException
     */
    public function getOrigin()
    {
        return $this->getRelated(self::ALIAS_ORIGIN);
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|FarbenCard
     *
     * @throws DomainException
     */
    public function getFarbenCard()
    {
        return $this->getRelated(self::ALIAS_FARBEN_CARDS);
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return APP_PATH . 'var/uploads/files/';
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $url = ltrim($config->application->baseUri, '/');
        $path = APP_PATH . 'var/uploads/files/product_images';

        //TODO
//        if(file_exists($path."/".$this->code.".jpg"))
//        {
//            return $url . '/uploads/files/product_images/'.$this->code.".jpg";
//
//        }

        if (is_file($this->getUploadPath() . $this->getImage())) {
            return $url . '/uploads/files/' . $this->getImage();
        }

        $filename = $url . '/uploads/files/no-image.jpg';
        if (is_file($filename)) {
            return $filename;
        }

        return $url . '/uploads/files/no-image.png';
    }

    public function getRealImagePath()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $url = ltrim($config->application->baseUri, '/');
        $path = APP_PATH . 'var/uploads/files/product_images';

        if(file_exists($path."/".$this->code.".jpg"))
        {
            return $url . '/uploads/files/product_images/'.$this->code.".jpg";

        }
        else {
            return $this->getImagePath();
        }
    }

    /**
     * @return string
     */
    public function getImagePrintPath()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $url = ltrim($config->application->baseUriImage, '/');

        if (is_file($this->getUploadPath() . $this->getImage())) {
//            return $url . '/uploads/files/' . $this->getImage();
            return '/var/www/html/elkarta/var/uploads/files/' . $this->getImage();
        }

        $filename = $url . '/uploads/files/no-image.jpg';
        if (is_file($filename)) {
            return $filename;
        }

        return $url . '/uploads/files/no-image.png';
    }

    public function getShortName()
    {
        return substr($this->getName(),0,22);
    }

    public function getBigName()
    {
        return $this->getCode()." - ".$this->getName();
    }

    public function __toString()
    {
        return $this->getCode()." - ".$this->getName();
    }

    public function beforeValidationOnCreate()
    {
        $this->created = (new DateTime())->format("Y-m-d H:i:s");

        $webName = "";

        $hydrator = new \Vemid\Service\Product\Hydrator($this,$this->getDI()->getEntityManager());

        $model = str_replace("_"," ",array_pop(explode(' ', $hydrator->getProductGroup()->getName())));
        $bred = $hydrator->getBrand();
        $name = array_pop(explode(' ', $this->getName()));

        if($hydrator->getGenderName() == "Muski") {
            $gender = "za dečake";
        }
        elseif ($hydrator->getGenderName() == "Zenski") {
            $gender = "za devojčice";
        }
        elseif($hydrator->getGenderName() == "Unisex") {
            $gender = "Unisex";
        }
        else $gender="";

        if($this->getProductCodeTypeId() == '66'){
            $webName = ucwords($model) . " " . $gender . " " . ucwords($name);
        }
        elseif ($this->getProductCodeTypeId() == '68') {
            $webName = ucwords($model) . " " . $gender . " " . ucwords($hydrator->getBrand());
        }
        else $webName = $this->getName();

        $this->setWebName(str_replace("  "," ",$webName));
    }

    public function afterCreate()
    {
        $tags = new Tags($this->getDI()->getEntityManager());
        $tags($this);


    }
}
