<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * OperatingListItem Model Class
 *
 * @Source('operating_list_items')
 *
 * @BelongsTo('operatingListId', 'OperatingList', 'id', {'alias':'OperatingList'})
 * @BelongsTo('operatingListItemTypeId', 'OperatingListItemType', 'id', {'alias':'OperatingListItemType'})
 */
class OperatingListItem extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_OPERATING_LIST_ID = 'operatingListId';
    const PROPERTY_OPERATING_LIST_ITEM_TYPE_ID = 'operatingListItemTypeId';
    const PROPERTY_PIECE_TIME = 'pieceTime';
    const PROPERTY_PIECES_HOUR = 'piecesHour';
    const PROPERTY_PIECE_PRICE = 'piecePrice';
    const PROPERTY_NOTE = 'note';
    const PROPERTY_FINISHED = 'finished';
    const PROPERTY_BARCODE = 'barcode';
    const PROPERTY_CREATED = 'created';

    const ALIAS_OPERATING_LIST = 'OperatingList';
    const ALIAS_OPERATING_LIST_ITEM_TYPE = 'OperatingListItemType';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="operating_list_id", type="integer", nullable=true)
     * @FormElement(label="OperatingList", type="Hidden", required=true)
     */
    protected $operatingListId;

    /**
     * @Column(column="operating_list_item_type_id", type="integer", nullable=true)
     * @FormElement(label="OperatingListItemType", type="Select", required=true, relation="OperatingListItemType")
     */
    protected $operatingListItemTypeId;


    /**
     * @Column(column="piece_time", type="decimal", nullable=false)
     * @FormElement(label="Piece time", type="Text", required=true)
     */
    protected $pieceTime;

    /**
     * @Column(column="pieces_hour", type="decimal", nullable=false)
     * @FormElement(label="Pieces hour", type="Text", required=true)
     */
    protected $piecesHour;

    /**
     * @Column(column="piece_price", type="decimal", nullable=false)
     * @FormElement(label="Piece price", type="Text", required=true)
     */
    protected $piecePrice;

    /**
     * @Column(column="note", type="string", nullable=true)
     * @FormElement(label="Note", type="TextArea", required=false)
     */
    protected $note;

    /**
     * @Column(column="finished", type="bool", nullable=true)
     */
    protected $finished;

    /**
     * @Column(column="barcode", type="string", nullable=true)
     */
    protected $barcode;

    /**
     * @Column(column="created", type="date", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_OPERATING_LIST_ID => true,
        self::PROPERTY_PIECE_TIME => true,
        self::PROPERTY_PIECES_HOUR => true,
        self::PROPERTY_PIECE_PRICE => true,
        self::PROPERTY_BARCODE => true,
        self::PROPERTY_FINISHED => true,
        self::PROPERTY_CREATED => true,
        self::ALIAS_OPERATING_LIST => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getOperatingListId()
    {
        return $this->operatingListId;
    }

    /**
     * @param int $operatingListId
     * @return $this
     */
    public function setOperatingListId($operatingListId)
    {
        $this->operatingListId = null !== $operatingListId ? (int)$operatingListId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getOperatingListItemTypeId()
    {
        return $this->operatingListItemTypeId;
    }

    /**
     * @param int $operatingListItemTypeId
     * @return $this
     */
    public function setOperatingListItemTypeId($operatingListItemTypeId)
    {
        $this->operatingListItemTypeId = null !== $operatingListItemTypeId ? (int)$operatingListItemTypeId : null;

        return $this;
    }


    /**
     * @return float
     */
    public function getPieceTime()
    {
        return $this->pieceTime;
    }

    /**
     * @param float $pieceTime
     * @return $this
     */
    public function setPieceTime($pieceTime)
    {
        $this->pieceTime = null !== $pieceTime ? (float)$pieceTime : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getPiecesHour()
    {
        return $this->piecesHour;
    }

    /**
     * @param float $piecesHour
     * @return $this
     */
    public function setPiecesHour($piecesHour)
    {
        $this->piecesHour = null !== $piecesHour ? (float)$piecesHour : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getPiecePrice()
    {
        return $this->piecePrice;
    }

    /**
     * @param float $piecePrice
     * @return $this
     */
    public function setPiecePrice($piecePrice)
    {
        $this->piecePrice = null !== $piecePrice ? (float)$piecePrice : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getFinished()
    {
        return $this->finished;
    }

    /**
     * @param boolean $finished
     * @return $this
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;

        return $this;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     * @return $this
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|OperatingList
     * 
     * @throws \DomainException
     */
    public function getOperatingList()
    {
        return $this->getRelated(self::ALIAS_OPERATING_LIST);
    }

    /**
     * @param OperatingList $operatingList
     * @return $this
     */
    public function setOperatingList(OperatingList $operatingList)
    {
        $this->operatingListId = $operatingList->getId();

        return $this;
    }

    /**
     * @return null|OperatingListItemType
     *
     * @throws \DomainException
     */
    public function getOperatingItemListType()
    {
        return $this->getRelated(self::ALIAS_OPERATING_LIST_ITEM_TYPE);
    }

    /**
     * @param OperatingListItemType $operatingListItemType
     * @return $this
     */
    public function setOperatingListItemType(OperatingListItemType $operatingListItemType)
    {
        $this->operatingListItemTypeId = $operatingListItemType->getId();

        return $this;
    }

    public function beforeCreate()
    {
        $this->created = new DateTime();
        $this->finished = 0;
    }

    public function afterSave()
    {
        if (null === $this->barcode) {
            $this->barcode = $this->getOperatingListId(). '-' . $this->getEntityId();
            $this->save();
        }
    }

}
