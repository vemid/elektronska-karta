<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;
use Vemid\Helper\TextHelper;

/**
 * WarehouseOrder Model Class
 *
 * @Source('warehouse_orders')
 * @BelongsTo('channelSupplyEntityId', 'ChannelSupplyEntity', 'id', {'alias': 'ChannelSupplyEntity'})
 * @BelongsTo('exitChannelSupplyEntityId', 'ChannelSupplyEntity', 'id', {'alias': 'ExitChannelSupplyEntity'})
 * @BelongsTo('shipmentStatusId', 'ShipmentStatus', 'id', {'alias': 'ShipmentStatus'})
 * @BelongsTo('priorityId', 'Priority', 'id', {'alias': 'Priority'})
 * @HasMany('id', 'WarehouseOrderItem', 'warehouseOrderId', {'alias':'WarehouseOrderItems'})
 * @HasOne('id', 'WarehouseOrderGroupItem', 'warehouseOrderId', {'alias':'WarehouseOrderGroupItems'})
 */
class WarehouseOrder extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CHANNEL_SUPPLY_ENTITY_ID = 'channelSupplyEntityId';
    const PROPERTY_EXIT_CHANNEL_SUPPLY_ENTITY_ID = 'exitChannelSupplyEntityId';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_PLANED_EXIT_DATE = 'planedExitDate';
    const PROPERTY_STATUS = 'status';
    const PROPERTY_NOTE = 'note';
    const PROPERTY_WAREHOUSE_ORDER_CODE = 'warehouseOrderCode';
    const PROPERTY_USE_COURIER_SERVICE = 'useCourierService';
    const PROPERTY_PUSHED_TO_MIS = 'pushedToMis';
    const PROPERTY_LOCKED = 'locked';
    const PROPERTY_POSTAL_BARCODE = 'postalBarcode';
    const PROPERTY_POSTED = 'posted';
    const PROPERTY_SHIPMENT_STATUS_ID= 'shipmentStatusId';
    const PROPERTY_SYNC_STATUS = 'syncStatus';
    const PROPERTY_CREATED = 'created';
    const PROPERTY_PRIORITY_ID = 'priorityId';
    const PROPERTY_BRUTO = 'bruto';
    const PROPERTY_NETO = 'neto';

    public const LOCKED = 'LOCKED';
    public const SENT = 'SENT';
    public const EDITABLE = 'EDITABLE';

    public const DISABLED = 'DISABLED';
    public const PROCESSED = 'PROCESSED';
    public const NOT_EXIST = 'NOT_EXIST';
    public const IN_USE = 'IN_USE';

    const ALIAS_WAREHOUSE_ORDER_ITEMS = 'WarehouseOrderItems';
    const ALIAS_WAREHOUSE_ORDER_GROUP_ITEMS = 'WarehouseOrderGroupItems';
    const ALIAS_EXIT_CHANNEL_SUPPLY_ENTITY = 'ExitChannelSupplyEntity';
    const ALIAS_SHIPMENT_STATUS = 'ShipmentStatus';
    const ALIAS_PRIORITY = 'Priority';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="channel_supply_entity_id", type="integer", nullable=false)
     */
    protected $channelSupplyEntityId;

    /**
     * @Column(column="exit_channel_supply_entity_id", type="integer", nullable=true)
     */
    protected $exitChannelSupplyEntityId;

    /**
     * @Column(column="type", type="string", nullable=false)
     * @FormElement(label="Type", type="Select", required=true, options={'A':'A','B' :'B'})
     */
    protected $type;

    /**
     * @Column(column="planed_exit_date", type="date", nullable=true)
     */
    protected $planedExitDate;

    /**
     * @Column(column="status", type="string", nullable=false)
     */
    protected $status;

    /**
     * @Column(column="note", type="string", nullable=true)
     * @FormElement(label="Note", type="Text", required=false)
     */
    protected $note;

    /**
     * @Column(column="pushed_to_mis", type="bool", nullable=true)
     */
    protected $pushedToMis;

    /**
     * @Column(column="locked", type="bool", nullable=true)
     */
    protected $locked;

    /**
     * @Column(column="warehouse_order_code", type="string", nullable=false)
     */
    protected $warehouseOrderCode;

    /**
     * @Column(column="use_courier_service", type="bool", nullable=true)
     * @FormElement(label="Slanje Kurirskom", type="Check", required=false)
     */
    protected $useCourierService;

    /**
     * @Column(column="postal_barcode", type="string", nullable=true)
     */
    protected $postalBarcode;

    /**
     * @Column(column="posted", type="integer", nullable=false)
     */
    protected $posted;

    /**
     * @Column(column="shipment_status_id", type="integer", nullable=true)
     */
    protected $shipmentStatusId;

    /**
     * @Column(column="priority_id", type="integer", nullable=true)
     */
    protected $priorityId;

    /**
     * @Column(column="sync_status", type="string", nullable=true)
     */
    protected $syncStatus;

    /**
     * @Column(column="bruto", type="decimal", nullable=true)
     */
    protected $bruto;

    /**
     * @Column(column="neto", type="decimal", nullable=true)
     */
    protected $neto;


    /**
     * @Column(column="created", type="dateTime", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CHANNEL_SUPPLY_ENTITY_ID => true,
        self::PROPERTY_EXIT_CHANNEL_SUPPLY_ENTITY_ID => true,
        self::PROPERTY_PLANED_EXIT_DATE => true,
        self::PROPERTY_STATUS => true,
        self::PROPERTY_NOTE => true,
        self::PROPERTY_WAREHOUSE_ORDER_CODE => true,
        self::PROPERTY_PUSHED_TO_MIS => true,
        self::PROPERTY_LOCKED => true,
        self::PROPERTY_POSTAL_BARCODE => true,
        self::PROPERTY_POSTED => true,
        self::PROPERTY_SHIPMENT_STATUS_ID => true,
        self::PROPERTY_SYNC_STATUS => true,
        self::PROPERTY_CREATED => true,
        self::PROPERTY_PRIORITY_ID => true,

    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getChannelSupplyEntityId()
    {
        return $this->channelSupplyEntityId;
    }

    /**
     * @param int $channelSupplyEntityId
     * @return $this
     */
    public function setChannelSupplyEntityId($channelSupplyEntityId)
    {
        $this->channelSupplyEntityId = null !== $channelSupplyEntityId ? (int)$channelSupplyEntityId : null;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExitChannelSupplyEntityId()
    {
        return $this->exitChannelSupplyEntityId;
    }

    /**
     * @param mixed $exitChannelSupplyEntityId
     */
    public function setExitChannelSupplyEntityId($exitChannelSupplyEntityId)
    {
        $this->exitChannelSupplyEntityId = $exitChannelSupplyEntityId;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getPlanedExitDate()
    {
        return $this->planedExitDate;
    }

    /**
     * @param string|DateTime $planedExitDate
     * @return $this
     */
    public function setPlanedExitDate($planedExitDate)
    {
        $this->planedExitDate = $planedExitDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return string
     */
    public function getWarehouseOrderCode()
    {
        return $this->warehouseOrderCode;
    }

    /**
     * @param string $warehouseOrderCode
     * @return $this
     */
    public function setWarehouseOrderCode($warehouseOrderCode)
    {
        $this->warehouseOrderCode = $warehouseOrderCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUseCourierService()
    {
        return $this->useCourierService;
    }

    /**
     * @param mixed $useCourierService
     */
    public function setUseCourierService($useCourierService)
    {
        $this->useCourierService = $useCourierService;

        return $this;
    }

    /**
     * @return string
     */
    public function getPushedToMis()
    {
        return (bool)$this->pushedToMis;
    }

    /**
     * @param bool $pushedToMis
     * @return $this
     */
    public function setPushedToMis($pushedToMis)
    {
        $this->pushedToMis = $pushedToMis;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**K
     * @param mixed $locked
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return$this;
    }

    /**
     * @return string
     */
    public function getPostalBarcode()
    {
        return $this->postalBarcode;
    }

    /**
     * @param string $postalBarcode
     * @return $this
     */
    public function setPostalBarcode($postalBarcode)
    {
        $this->postalBarcode = $postalBarcode;

        return $this;
    }

    /**
     * @return float
     */
    public function getBruto()
    {
        return $this->bruto;
    }

    /**
     * @param float $bruto
     * @return $this
     */
    public function setBruto($bruto)
    {
        $this->bruto = $bruto;

        return $this;
    }

    /**
     * @return float
     */
    public function getNeto()
    {
        return $this->neto;
    }

    /**
     * @param float $neto
     * @return $this
     */
    public function setNeto($neto)
    {
        $this->neto = $neto;

        return $this;
    }

    /**
     * @return bool
     */
    public function getPosted()
    {
        return (bool)$this->posted;
    }

    /**
     * @param int|bool $posted
     * @return $this
     */
    public function setPosted($posted)
    {
        $this->posted = $posted ? 1 : 0;

        return $this;
    }

    /**
     * @return string
     */
    public function getShipmentStatustId()
    {
        return $this->shipmentStatusId;
    }

    /**
     * @param string $shipmentStatusId
     * @return $this
     */
    public function setShipmentStatusId($shipmentStatusId)
    {
        $this->shipmentStatusId = $shipmentStatusId;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriorityId()
    {
        return $this->priorityId;
    }

    /**
     * @param int $priorityId
     * @return $this
     */
    public function setPriorityId($priorityId)
    {
        $this->priorityId = null !== $priorityId ? (int)$priorityId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getSyncStatus()
    {
        return $this->syncStatus;
    }

    /**
     * @param string $syncStatus
     * @return $this
     */
    public function setSyncStatus($syncStatus)
    {
        $this->syncStatus = $syncStatus;

        return $this;
    }

    /**
     * @return null|ChannelSupplyEntity
     *
     * @throws \DomainException
     */
    public function getChannelSupplyEntity()
    {
        return $this->getRelated(ChannelSupplyEntity::class);
    }

    /**
     * @param ChannelSupplyEntity $channelSupplyEntity
     * @return $this
     */
    public function setChannelSupplyEntity(ChannelSupplyEntity $channelSupplyEntity)
    {
        $this->channelSupplyEntityId = $channelSupplyEntity->getId();

        return $this;
    }

    /**
     * @return null|Priority
     *
     * @throws \DomainException
     */
    public function getPriority()
    {
        return $this->getRelated(Priority::class);
    }

    /**
     * @param Priority $priority
     * @return $this
     */
    public function setPriority(Priority $priority)
    {
        $this->priorityId = $priority->getId();

        return $this;
    }

    /**
     * @return null|ChannelSupplyEntity
     *
     * @throws \DomainException
     */
    public function getExitChannelSupplyEntity()
    {
        return $this->getRelated(self::ALIAS_EXIT_CHANNEL_SUPPLY_ENTITY);
    }

    /**
     * @param ChannelSupplyEntity $exitChannelSupplyEntityId
     * @return $this
     */
    public function setExitChannelSupplyEntity(ChannelSupplyEntity $exitChannelSupplyEntityId)
    {
        $this->exitChannelSupplyEntityId = $exitChannelSupplyEntityId->getId();

        return $this;
    }

    /**
     * @return null|ShipmentStatus
     *
     * @throws \DomainException
     */
    public function getShipmentStatus()
    {
        return $this->getRelated(ShipmentStatus::class);
    }

    /**
     * @param ShipmentStatus $shipmentStatus
     * @return $this
     */
    public function setShipmentStatus(ShipmentStatus $shipmentStatus)
    {
        $this->shipmentStatusId = $shipmentStatus->getId();

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string|DateTime $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|WarehouseOrderGroupItem
     *
     * @throws DomainException
     */
    public function getWarehouseOrderGroupItem($arguments = null)
    {
        return $this->getRelated(self::ALIAS_WAREHOUSE_ORDER_GROUP_ITEMS,$arguments);
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|WarehouseOrderItem[]
     *
     * @throws \DomainException
     */
    public function getWarehouseOrderItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_WAREHOUSE_ORDER_ITEMS, $arguments);
    }

    /**
     * @return bool
     */
    public function isEditable()
    {
        return $this->status === self::EDITABLE;
    }

    public function beforeValidationOnCreate()
    {
        $this->pushedToMis = 0;
        $this->locked = 0;
        $this->neto = 0;
        $this->bruto = 0;
        $this->created = new DateTime();
        $this->syncStatus = self::IN_USE;

        if (!$this->useCourierService) {
            $this->useCourierService = 0;
        }

        $config = $this->getDI()->getConfig();
        $courierPrefix = $config->postalService->courierPrefix;

        $channelSupplyEntity = $this->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();
        $warehouseOrders = $this->getDI()->getEntityManager()->find(self::class);
        $id = $totalRecords = $warehouseOrders->count();

        $code = "00";
        if ($supplyChannel->getType() === \ChannelSupply::MP) {
            $code = $channelSupplyEntity->getEntity()->getCode();
            $lastWarehouseOrder = $this->getDI()->getEntityManager()->find(self::class, [
                self::PROPERTY_CHANNEL_SUPPLY_ENTITY_ID . ' = :entityId:',
                'bind' => [
                    'entityId' => $this->getChannelSupplyEntity()->getId()
                ]
            ]);
            $id = count($lastWarehouseOrder->toArray());

        }

        $id++;

        $repeatingNumber = str_repeat('0', 10 - strlen($id));
        $repeatingNumberPostal = str_repeat('0', 6 - strlen($totalRecords+1));

        $this->warehouseOrderCode = sprintf('%s%s%d00', $code, str_repeat("0", 9 - strlen($id)), $id);
        $this->postalBarcode = sprintf('%s%s%s', $courierPrefix, $repeatingNumberPostal, $totalRecords+1);
    }

    public function beforeUpdate()
    {
        if ($this->hasChanged(self::PROPERTY_STATUS) && $this->hasChanged(self::PROPERTY_PUSHED_TO_MIS)  && $this->getStatus() === self::EDITABLE) {
            $controlNumber = (int)substr($this->getWarehouseOrderCode(), -2);
            $code = substr($this->getWarehouseOrderCode(), 0, -2);

            $controlNumber++;
            $this->setWarehouseOrderCode(sprintf('%s%02d', $code, $controlNumber));
        }
    }

    public function getProductListFromWarehouseOrder()
    {
        $warehouseOrderItems = $this->getWarehouseOrderItems([
            'group' => WarehouseOrderItem::PROPERTY_PRODUCT_SIZE_ID
        ]);

        $products = [];
        foreach ($warehouseOrderItems as $warehouseOrderItem) {
            $products[] = (string)$warehouseOrderItem->getProductSize()->getProduct();
        }

        return implode(', ', $products);
    }
}
