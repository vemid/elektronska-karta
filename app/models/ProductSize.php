<?php

use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;
use \Vemid\Service\Product\Hydrator;

/**
 * ProductSize Model Class
 *
 * @Source('product_sizes')
 *
 * @BelongsTo('productId', 'Product', 'id', {'alias':'Product'})
 * @BelongsTo('codeId', 'Code', 'id', {'alias':'Code'})
 * @HasMany('id', 'OrderItem', 'productSizeId', {'alias':'OrderItems'})
 * @HasMany('id', 'ProductionOrder', 'productSizeId', {'alias':'ProductionOrders'})
 * @HasMany('id', 'Failure', 'productSizeId', {'alias':'Failures'})
 * @HasMany('id', 'PrintLabelItem', 'productSizeId', {'alias':'PrintLabalItems'})
 * @HasMany('id', 'WarehouseOrderItem', 'productSizeId', {'alias':'WarehouseOrderItems'})
 *
 * @HasOne('id', 'OrderTotal', 'productSizeId', {'alias':'OrderTotals'})
 */
class ProductSize extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PRODUCT_ID = 'productId';
    const PROPERTY_CODE_ID = 'codeId';
    const PROPERTY_BARCODE = 'barcode';
    const PROPERTY_IS_SYNCED = 'isSynced';
    const PROPERTY_FARBEN_FIG_QTY = 'farbenFigQty';
    const PROPERTY_FARBEN_REST_QTY = 'farbenRestQty';
    const PROPERTY_TEMP_QTY = 'tempQty';

    const ALIAS_PRODUCT = 'Product';
    const ALIAS_CODE = 'Code';
    const ALIAS_ORDER_ITEMS = 'OrderItems';
    const ALIAS_PRODUCTION_ORDERS = 'ProductionOrders';
    const ALIAS_ODER_TOTALS = 'OrderTotals';
    const ALIAS_FAILURES = 'Failures';
    const ALIAS_PRINT_LABEL_ITEMS = 'PrintLabalItems';
    const ALIAS_WAREHOUSE_ORDER_ITEMS = 'WarehouseOrderItems';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="product_id", type="integer", nullable=false)
     * @FormElement(label="Product", type="Select", required=true, relation="Product")
     */
    protected $productId;

    /**
     * @Column(column="code_id", type="integer", nullable=false)
     * @FormElement(label="Code", type="Select", required=true, relation="Code")
     */
    protected $codeId;

    /**
     * @Column(column="barcode", type="string", nullable=true)
     * @FormElement(label="Barcode", type="Text", required=false)
     */
    protected $barcode;

    /**
     * @Column(column="is_synced", type="bool", nullable=true)
     */
    protected $isSynced;

    /**
     * @Column(column="farben_card_warrant", type="string", nullable=true)
     * @FormElement(label="Farben Card Warrant", type="Text", required=false)
     */
    protected $farbenCardWarrant;

    /**
     * @Column(column="farben_fig_qty", type="integer", nullable=true)
     * @FormElement(label="Farben Fig Qty", type="Text", required=false)
     */
    protected $farbenFigQty;

    /**
     * @Column(column="farben_rest_qty", type="integer", nullable=true)
     * @FormElement(label="Farben Rest Qty", type="Text", required=false)
     */
    protected $farbenRestQty;

    /**
     * @Column(column="temp_qty", type="integer", nullable=true)
     */
    protected $tempQty;


    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRODUCT_ID => true,
        self::PROPERTY_CODE_ID => true,
        self::PROPERTY_BARCODE => true,
        self::ALIAS_PRODUCT => true,
        self::ALIAS_CODE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->productId = null !== $productId ? (int)$productId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * @param int $codeId
     * @return $this
     */
    public function setCodeId($codeId)
    {
        $this->codeId = null !== $codeId ? (int)$codeId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     * @return $this
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * @return string
     */
    public function getFarbenCardWarrant()
    {
        return $this->farbenCardWarrant;
    }

    /**
     * @param string $farbenCardWarrant
     * @return $this
     */
    public function setFarbenCardWarrant($farbenCardWarrant)
    {
        $this->farbenCardWarrant = $farbenCardWarrant;

        return $this;
    }

    /**
     * @return int
     */
    public function getFarbenFigQty()
    {
        return $this->farbenFigQty;
    }

    /**
     * @param int $farbenFigQty
     * @return $this
     */
    public function setFarbenFigQty($farbenFigQty)
    {
        $this->farbenFigQty = null !== $farbenFigQty ? (int)$farbenFigQty : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getFarbenRestQty()
    {
        return $this->farbenRestQty;
    }

    /**
     * @param int $farbenRestQty
     * @return $this
     */
    public function setFarbenRestQty($farbenRestQty)
    {
        $this->farbenRestQty = null !== $farbenRestQty ? (int)$farbenRestQty : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getTempQty()
    {
        return $this->tempQty;
    }

    /**
     * @param int $tempQty
     * @return $this
     */
    public function setTempQty($tempQty)
    {
        $this->tempQty = null !== $tempQty ? (int)$tempQty : null;

        return $this;
    }

    /**
     * @return null|Product
     * 
     * @throws \DomainException
     */
    public function getProduct()
    {
        return $this->getRelated(self::ALIAS_PRODUCT);
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->productId = $product->getId();

        return $this;
    }

    /**
     * @return null|Code
     * 
     * @throws \DomainException
     */
    public function getCode()
    {
        return $this->getRelated(self::ALIAS_CODE);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setCode(Code $code)
    {
        $this->codeId = $code->getId();

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSynced()
    {
        return $this->isSynced;
    }

    /**
     * @param boolean $isSynced
     */
    public function setIsSynced($isSynced)
    {
        $this->isSynced = $isSynced;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|OrderItem[]
     * 
     * @throws \DomainException
     */
    public function getOrderItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_ORDER_ITEMS, $arguments);
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|OrderTotal
     *
     * @throws DomainException
     */
    public function getOrderTotal()
    {
        return $this->getRelated(self::ALIAS_ODER_TOTALS);
    }

    /**
     * @param array|null $arguments
     * @return ProductionOrder[]|Resultset
     *
     * @throws DomainException
     */
    public function getProductionOrders($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRODUCTION_ORDERS, $arguments);
    }

    /**
     * @param array|null $arguments
     * @return Failure[]|Resultset
     *
     * @throws DomainException
     */
    public function getFailures($arguments = null)
    {
        return $this->getRelated(self::ALIAS_FAILURES, $arguments);
    }

    /**
     * @param array|null $arguments
     * @return PrintLabelItem[]|Resultset
     *
     * @throws DomainException
     */
    public function getPrintLabelItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRINT_LABEL_ITEMS, $arguments);
    }

    /**
     * @param null $arguments
     * @return Resultset|\Vemid\Entity\EntityInterface|null|WarehouseOrderItem[]
     */
    public function getWarehouseOrderItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_WAREHOUSE_ORDER_ITEMS, $arguments);
    }

    public function beforeCreate()
    {
        $this->isSynced = 0;
        $this->farbenFigQty = 0;
        $this->farbenRestQty = 0;
        $this->barcode = $this->returnBarcode();
    }

    public function beforeUpdate()
    {
        $this->barcode = $this->returnBarcode();
    }

    public function returnBarcode()
    {
        /** @var Product $product */
        $product = $this->getProduct();

        /** @var Code $codeType */
        $codeType = $this->getDI()->getEntityManager()->findOne(Code::class,$product->getProductCodeTypeId());

        if($codeType->getCode() == '3'){
            $string = substr($product->getCode(),0,1) .
            substr($product->getCode(),2,1) .
              $this->getCode() . substr($product->getCode(),4,9);

            return $string;

        }

        else return $this->barcode;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        if ($this->getOrderItems()->count()) {
            return false;
        }

        return true;
    }

    public function getProductSizeHydrator()
    {
        return new Hydrator($this->getProduct(), $this->getDI()->getEntityManager());

    }

    public function __toString()
    {
        return $this->getProduct()->getCode()." - ".$this->getCode()->getCode();
    }
}
