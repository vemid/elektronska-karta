<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * OutboundEmail Model Class
 *
 * @Source('outbound_emails')
 */
class OutboundEmail extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_TEMPLATE_OBJECT_ID = 'templateObjectId';
    const PROPERTY_TEMPLATE_OBJECT_TYPE_ID = 'templateObjectTypeId';
    const PROPERTY_FROM_ADDRESS = 'fromAddress';
    const PROPERTY_TO_ADDRESS = 'toAddress';
    const PROPERTY_CC_ADDRESS = 'ccAddress';
    const PROPERTY_BCC_ADDRESS = 'bccAddress';
    const PROPERTY_SUBJECT = 'subject';
    const PROPERTY_BODY = 'body';
    const PROPERTY_QUEUED_DATETIME = 'queuedDatetime';
    const PROPERTY_SENT_DATETIME = 'sentDatetime';


    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="template_object_id", type="integer", nullable=true)
     * @FormElement(label="Template object", type="Text", required=false)
     */
    protected $templateObjectId;

    /**
     * @Column(column="template_object_type_id", type="integer", nullable=true)
     * @FormElement(label="Template object type", type="Text", required=false)
     */
    protected $templateObjectTypeId;

    /**
     * @Column(column="from_address", type="string", nullable=false)
     * @FormElement(label="From address", type="Text", required=true)
     */
    protected $fromAddress;

    /**
     * @Column(column="to_address", type="string", nullable=false)
     * @FormElement(label="To address", type="Text", required=true)
     */
    protected $toAddress;

    /**
     * @Column(column="cc_address", type="string", nullable=true)
     * @FormElement(label="Cc address", type="Text", required=false)
     */
    protected $ccAddress;

    /**
     * @Column(column="bcc_address", type="string", nullable=true)
     * @FormElement(label="Bcc address", type="Text", required=false)
     */
    protected $bccAddress;

    /**
     * @Column(column="subject", type="string", nullable=false)
     * @FormElement(label="Subject", type="Text", required=true)
     */
    protected $subject;

    /**
     * @Column(column="body", type="string", nullable=false)
     * @FormElement(label="Body", type="Text", required=true)
     */
    protected $body;

    /**
     * @Column(column="queued_datetime", type="date", nullable=false)
     * @FormElement(label="Queued Datetime", type="Date", required=true)
     */
    protected $queuedDatetime;

    /**
     * @Column(column="sent_datetime", type="date", nullable=true)
     * @FormElement(label="Sent Datetime", type="Date", required=false)
     */
    protected $sentDatetime;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_TEMPLATE_OBJECT_ID => true,
        self::PROPERTY_TEMPLATE_OBJECT_TYPE_ID => true,
        self::PROPERTY_FROM_ADDRESS => true,
        self::PROPERTY_CC_ADDRESS => true,
        self::PROPERTY_BCC_ADDRESS => true,
        self::PROPERTY_SUBJECT => true,
        self::PROPERTY_BODY => true,
        self::PROPERTY_QUEUED_DATETIME => true,
        self::PROPERTY_SENT_DATETIME => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getTemplateObjectId()
    {
        return $this->templateObjectId;
    }

    /**
     * @param int $templateObjectId
     * @return $this
     */
    public function setTemplateObjectId($templateObjectId)
    {
        $this->templateObjectId = null !== $templateObjectId ? (int)$templateObjectId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getTemplateObjectTypeId()
    {
        return $this->templateObjectTypeId;
    }

    /**
     * @param int $templateObjectTypeId
     * @return $this
     */
    public function setTemplateObjectTypeId($templateObjectTypeId)
    {
        $this->templateObjectTypeId = null !== $templateObjectTypeId ? (int)$templateObjectTypeId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getFromAddress()
    {
        return $this->fromAddress;
    }

    /**
     * @param string $fromAddress
     * @return $this
     */
    public function setFromAddress($fromAddress)
    {
        $this->fromAddress = $fromAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getToAddress()
    {
        return $this->toAddress;
    }

    /**
     * @param string $toAddress
     * @return $this
     */
    public function setToAddress($toAddress)
    {
        $this->toAddress = $toAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getCcAddress()
    {
        return $this->ccAddress;
    }

    /**
     * @param string $ccAddress
     * @return $this
     */
    public function setCcAddress($ccAddress)
    {
        $this->ccAddress = $ccAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getBccAddress()
    {
        return $this->bccAddress;
    }

    /**
     * @param string $bccAddress
     * @return $this
     */
    public function setBccAddress($bccAddress)
    {
        $this->bccAddress = $bccAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getQueuedDatetime()
    {
        return $this->queuedDatetime;
    }

    /**
     * @param string|DateTime $queuedDatetime
     * @return $this
     */
    public function setQueuedDatetime($queuedDatetime)
    {
        $this->queuedDatetime = $queuedDatetime;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getSentDatetime()
    {
        return $this->sentDatetime;
    }

    /**
     * @param string|DateTime $sentDatetime
     * @return $this
     */
    public function setSentDatetime($sentDatetime)
    {
        $this->sentDatetime = $sentDatetime;

        return $this;
    }
}
