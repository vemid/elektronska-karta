<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * ShipmentStatus Model Class
 *
 * @Source('shipment_statuses')
 */
class ShipmentStatus extends AbstractEntity
{

    public const STATUS_DELETED = 'DELETED';
    public const STATUS_CANCELLED = 'CANCELLED';
    public const STATUS_WAITING = 'WAITING';
    public const STATUS_DELIVERED = 'DELIVERED';
    public const STATUS_PICKED = 'PICKED';

    const PROPERTY_ID = 'id';
    const PROPERTY_EVENT_ID = 'eventId';
    const PROPERTY_CODE = 'code';
    const PROPERTY_REFERENCE_ID = 'referenceId';
    const PROPERTY_STATUS = 'status';
    const PROPERTY_DATE = 'date';


    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="event_id", type="string", nullable=true)
     * @FormElement(label="Event", type="Text", required=false)
     */
    protected $eventId;

    /**
     * @Column(column="code", type="string", nullable=true)
     * @FormElement(label="Code", type="Text", required=false)
     */
    protected $code;

    /**
     * @Column(column="reference_id", type="string", nullable=false)
     * @FormElement(label="Reference", type="Text", required=true)
     */
    protected $referenceId;

    /**
     * @Column(column="status", type="string", nullable=true)
     * @FormElement(label="Status", type="Text", required=false)
     */
    protected $status;

    /**
     * @Column(column="event_date", type="date", nullable=true)
     * @FormElement(label="Date", type="Date", required=false)
     */
    protected $eventDate;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_EVENT_ID => true,
        self::PROPERTY_CODE => true,
        self::PROPERTY_REFERENCE_ID => true,
        self::PROPERTY_STATUS => true,
        self::PROPERTY_DATE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param string $eventId
     * @return $this
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * @param string $referenceId
     * @return $this
     */
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * @param string|DateTime $date
     * @return $this
     */
    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * @return int[]
     */
    public static function getStatuses(): array
    {
        return [
            -2 => self::STATUS_DELETED,
            -1 => self::STATUS_CANCELLED,
            0 => self::STATUS_WAITING,
            1 => self::STATUS_DELIVERED,
            3 => self::STATUS_PICKED,
        ];
    }
}
