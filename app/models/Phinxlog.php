<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * Phinxlog Model Class
 *
 * @Source('phinxlog')
 */
class Phinxlog extends AbstractEntity
{

    const PROPERTY_VERSION = 'version';
    const PROPERTY_MIGRATION_NAME = 'migrationName';
    const PROPERTY_START_TIME = 'startTime';
    const PROPERTY_END_TIME = 'endTime';
    const PROPERTY_BREAKPOINT = 'breakpoint';


    protected $_objectId = self::PROPERTY_VERSION;

    /**
     * @Primary
     * @Identity
     * @Column(column="version", type="string", nullable=false)
     */
    protected $version;

    /**
     * @Column(column="migration_name", type="string", nullable=true)
     * @FormElement(label="Migration name", type="Text", required=false)
     */
    protected $migrationName;

    /**
     * @Column(column="start_time", type="string", nullable=true)
     * @FormElement(label="Start time", type="Text", required=false)
     */
    protected $startTime;

    /**
     * @Column(column="end_time", type="string", nullable=true)
     * @FormElement(label="End time", type="Text", required=false)
     */
    protected $endTime;

    /**
     * @Column(column="breakpoint", type="integer", nullable=false)
     * @FormElement(label="Breakpoint", type="Check", required=true)
     */
    protected $breakpoint;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_VERSION => true,
        self::PROPERTY_MIGRATION_NAME => true,
        self::PROPERTY_START_TIME => true,
        self::PROPERTY_END_TIME => true,
        self::PROPERTY_BREAKPOINT => true,
    );

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     * @return $this
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return string
     */
    public function getMigrationName()
    {
        return $this->migrationName;
    }

    /**
     * @param string $migrationName
     * @return $this
     */
    public function setMigrationName($migrationName)
    {
        $this->migrationName = $migrationName;

        return $this;
    }

    /**
     * @return string
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param string $startTime
     * @return $this
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param string $endTime
     * @return $this
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * @return bool
     */
    public function getBreakpoint()
    {
        return (bool)$this->breakpoint;
    }

    /**
     * @param int|bool $breakpoint
     * @return $this
     */
    public function setBreakpoint($breakpoint)
    {
        $this->breakpoint = $breakpoint ? 1 : 0;

        return $this;
    }

}
