<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * Supplier Model Class
 *
 * @Source('suppliers')
 *
 * @HasmMany('is', 'Supplier', 'supplierId', {'alias':'ElectronCardFactories'})
 */
class Supplier extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CODE = 'code';
    const PROPERTY_NAME = 'name';
    const PROPERTY_CONTACT_PERSON = 'contactPerson';
    const PROPERTY_EMAIL = 'email';
    const PROPERTY_PHONE_NUMBER = 'phoneNumber';
    const PROPERTY_ADDRESS = 'address';

    const ALIAS_CODE = 'Code';
    const ALIAS_ELECTRON_CARD_FACTORIES = 'ElectronCardFactories';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="code", type="string", nullable=false)
     * @FormElement(label="Code", type="Text", required=true)
     */
    protected $code;

    /**
     * @Column(column="contact_person", type="string", nullable=false)
     * @FormElement(label="Contact person", type="Text", required=true)
     */
    protected $contactPerson;

    /**
     * @Column(column="email", type="string", nullable=false)
     * @FormElement(label="Email", type="Text", required=true)
     */
    protected $email;

    /**
     * @Column(column="phone_number", type="string", nullable=false)
     * @FormElement(label="Phone number", type="Text", required=true)
     */
    protected $phoneNumber;

    /**
     * @Column(column="address", type="string", nullable=false)
     * @FormElement(label="Address", type="Text", required=true)
     */
    protected $address;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CONTACT_PERSON => true,
        self::PROPERTY_EMAIL => true,
        self::PROPERTY_PHONE_NUMBER => true,
        self::PROPERTY_ADDRESS => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param string $contactPerson
     * @return $this
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @return $this
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->getName();
    }

    /**
     * @return Resultset|ElectronCardFactory[]
     *
     * @throws \DomainException
     */
    public function getElectronCardFactories()
    {
        return $this->getRelated(self::ALIAS_ELECTRON_CARD_FACTORIES);
    }
}
