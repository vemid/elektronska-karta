<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * Client Model Class
 *
 * @Source('clients')
 *
 * @BelongsTo('userId', 'User', 'id', {'alias':'User'})
 */
class Client extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_USER_ID = 'userId';
    const PROPERTY_CODE = 'code';
    const PROPERTY_NAME = 'name';
    const PROPERTY_ADDRESS = 'address';
    const PROPERTY_PIB = 'pib';
    const PROPERTY_PHONE = 'phone';
    const PROPERTY_INVOICE_NAME = 'invoiceName';

    const ALIAS_USER = 'User';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="user_id", type="integer", nullable=false)
     * @FormElement(label="User", type="Select", required=true, relation="User")
     */
    protected $userId;

    /**
     * @Column(column="code", type="string", nullable=false)
     * @FormElement(label="Code", type="Text", required=true)
     */
    protected $code;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="address", type="string", nullable=true)
     * @FormElement(label="Address", type="Text", required=false)
     */
    protected $address;

    /**
     * @Column(column="pib", type="string", nullable=true)
     * @FormElement(label="Pib", type="Text", required=false)
     */
    protected $pib;

    /**
     * @Column(column="phone", type="string", nullable=true)
     * @FormElement(label="Phone", type="Text", required=false)
     */
    protected $phone;

    /**
     * @Column(column="invoice_name", type="string", nullable=true)
     * @FormElement(label="Invoice Name", type="Text", required=false)
     */
    protected $invoiceName;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_USER_ID => true,
        self::PROPERTY_CODE => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_ADDRESS => true,
        self::PROPERTY_PIB => true,
        self::PROPERTY_PHONE => true,
        self::PROPERTY_INVOICE_NAME => true,
        self::ALIAS_USER => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = null !== $userId ? (int)$userId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getPib()
    {
        return $this->pib;
    }

    /**
     * @param string $pib
     * @return $this
     */
    public function setPib($pib)
    {
        $this->pib = $pib;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceName()
    {
        return $this->invoiceName;
    }

    /**
     * @param string $invoiceName
     * @return $this
     */
    public function setInvoiceName($invoiceName)
    {
        $this->invoiceName = $invoiceName;

        return $this;
    }

    /**
     * @return null|User|\Vemid\Entity\EntityInterface
     * 
     * @throws \DomainException
     */
    public function getUser()
    {
        return $this->getRelated(self::ALIAS_USER);
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->userId = $user->getId();

        return $this;
    }

    public function getDisplayName()
    {
        return $this->getName();
    }
}
