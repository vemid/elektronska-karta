<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * WarrantShop Model Class
 *
 * @Source('dealership_warrants')
 *
 * @BelongsTo('dealershipId', 'Dealership', 'id', {'alias':'Dealership'})
 * @BelongsTo('warrantStatementItemId', 'WarrantStatementItem', 'id', {'alias':'WarrantStatementItem'})
 */
class DealershipWarrant extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_WARRANT_STATEMENT_ITEM_ID = 'warrantStatementItemId';
    const PROPERTY_WARRANT_NUMBER = 'warrantNumber';
    const PROPERTY_DEALERSHIP_ID = 'dealershipId';
    const PROPERTY_DATE = 'date';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_AMOUNT = 'amount';
    const PROPERTY_STATUS = 'status';

    const ALIAS_DEALERSHIP = 'Dealership';
    const ALIAS_WARRANT_STATEMENT_ITEM = 'WarrantStatementItem';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="dealership_id", type="integer", nullable=false)
     * @FormElement(label="Dealership", type="Select", required=true, relation="Code")
     */
    protected $dealershipId;

    /**
     * @Column(column="warrant_statement_item_id", type="string", nullable=true)
     * @FormElement(label="Warrant Statement Item", type="Text", required=false)
     */
    protected $warrantStatementItemId;


    /**
     * @Column(column="warrant_number", type="string", nullable=false)
     * @FormElement(label="Warrant number", type="Text", required=true)
     */
    protected $warrantNumber;

    /**
     * @Column(column="date", type="date", nullable=true)
     * @FormElement(label="Date", type="Date", required=false)
     */
    protected $date;

    /**
     * @Column(column="type", type="string", nullable=true)
     * @FormElement(label="Type", type="Text", required=false)
     */
    protected $type;

    /**
     * @Column(column="amount", type="decimal", nullable=false)
     * @FormElement(label="Amount", type="Text", required=true)
     */
    protected $amount;

    /**
     * @Column(column="status", type="bool", nullable=true)
     */
    protected $status;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_WARRANT_NUMBER => true,
        self::PROPERTY_DEALERSHIP_ID => true,
        self::PROPERTY_DATE => true,
        self::PROPERTY_TYPE => true,
        self::PROPERTY_AMOUNT => true,
        self::PROPERTY_STATUS => true,
        self::ALIAS_DEALERSHIP => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getDealershipId()
    {
        return $this->dealershipId;
    }

    /**
     * @param int $dealershipId
     * @return $this
     */
    public function setDealershipId($dealershipId)
    {
        $this->dealershipId = null !== $dealershipId ? (int)$dealershipId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getWarrantStatementItemId()
    {
        return $this->warrantStatementItemId;
    }

    /**
     * @param int $warrantStatementItemId
     * @return $this
     */
    public function setWarrantStatementItemId($warrantStatementItemId)
    {
        $this->warrantStatementItemId = null !== $warrantStatementItemId ? (int)$warrantStatementItemId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getWarrantNumber()
    {
        return $this->warrantNumber;
    }

    /**
     * @param string $warrantNumber
     * @return $this
     */
    public function setWarrantNumber($warrantNumber)
    {
        $this->warrantNumber = $warrantNumber;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string|DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = null !== $amount ? (float)$amount : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param boolean $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return null|Code
     * 
     * @throws \DomainException
     */
    public function getDealership()
    {
        return $this->getRelated(self::ALIAS_DEALERSHIP);
    }

    /**
     * @param Dealership $dealership
     * @return $this
     */
    public function setDealership(Dealership $dealership)
    {
        $this->dealershipId = $dealership->getId();

        return $this;
    }

    /**
     * @return null|WarrantStatementItem|\Vemid\Entity\EntityInterface
     *
     * @throws \DomainException
     */
    public function getWarrantStatementItem()
    {
        return $this->getRelated(self::ALIAS_WARRANT_STATEMENT_ITEM);
    }

    /**
     * @param WarrantStatementItem $warrantStatementItem
     * @return $this
     */
    public function setWarrantStatementItem(WarrantStatementItem $warrantStatementItem)
    {
        $this->warrantStatementItemId = $warrantStatementItem->getId();

        return $this;
    }
}
