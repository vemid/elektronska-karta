<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * Efectus Model Class
 *
 * @Source('efectus')
 * @BelongsTo('productSizeId', 'ProductSize', 'id', {'alias':'ProductSize'})
 * @BelongsTo('fromCodeId', 'Code', 'id', {'alias':'CodeFrom'})
 * @BelongsTo('toCodeId', 'Code', 'id', {'alias':'CodeTo'})
 * @HasOne('warehouseOrderItemId', 'WarehouseOrderItem', 'id', {'alias':'WarehouseOrderItem'})
 */
class Efectus extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_FROM_CODE_ID = 'fromCodeId';
    const PROPERTY_TO_CODE_ID = 'toCodeId';
    const PROPERTY_WAREHOUSE_ORDER_ITEM_ID = 'warehouseOrderItemId';
    const PROPERTY_PRODUCT_SIZE_ID = 'productSizeId';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_QTY = 'qty';
    const PROPERTY_DATE = 'date';
    const PROPERTY_EXCLUDE = 'exclude';

    const ALIAS_PRODUCT_SIZE = 'ProductSize';
    const ALIAS_WAREHOUSE_ORDER_ITEM = 'WarehouseOrderItem';
    const ALIAS_CODE_FROM = 'CodeFrom';
    const ALIAS_CODE_TO = 'CodeTo';

    public const REDOVNI_TRANSFERI = 'REDOVNI_TRANSFERI';
    public const NEDOPUNJENE_VELICINE = 'NEDOPUNJENE_VELICINE';
    public const DESORTIRANI_TRANSFERI = 'DESORTIRANI_TRANSFERI';
    public const KOMPLETIRANJE_OLD = 'KOMPLETIRANJE_OLD';
    public const KOMPLETIRANJE = 'KOMPLETIRANJE';
    public const KOMPLETIRANJE_INDJ = 'KOMPLETIRANJE_INDJ';
    public const DOPUNA_SA_CM = 'DOPUNA_SA_CM';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="from_code_id", type="integer", nullable=false)
     * @FormElement(label="From code", type="Text", required=true)
     */
    protected $fromCodeId;

    /**
     * @Column(column="to_code_id", type="integer", nullable=false)
     * @FormElement(label="To code", type="Text", required=true)
     */
    protected $toCodeId;

    /**
     * @Column(column="warehouse_order_item_id", type="integer", nullable=true)
     */
    protected $warehouseOrderItemId;

    /**
     * @Column(column="product_size_id", type="integer", nullable=false)
     * @FormElement(label="Product size", type="Text", required=true)
     */
    protected $productSizeId;

    /**
     * @Column(column="type", type="string", nullable=false)
     * @FormElement(label="Type", type="Text", required=true)
     */
    protected $type;

    /**
     * @Column(column="qty", type="integer", nullable=false)
     * @FormElement(label="Qty", type="Text", required=true)
     */
    protected $qty;

    /**
     * @Column(column="exclude", type="bool", nullable=true)
     */
    protected $exclude;

    /**
     * @Column(column="date", type="date", nullable=true)
     */
    protected $date;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_FROM_CODE_ID => true,
        self::PROPERTY_TO_CODE_ID => true,
        self::PROPERTY_PRODUCT_SIZE_ID => true,
        self::PROPERTY_TYPE => true,
        self::PROPERTY_QTY => true,
        self::PROPERTY_DATE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getFromCodeId()
    {
        return $this->fromCodeId;
    }

    /**
     * @param int $fromCodeId
     * @return $this
     */
    public function setFromCodeId($fromCodeId)
    {
        $this->fromCodeId = null !== $fromCodeId ? (int)$fromCodeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getToCodeId()
    {
        return $this->toCodeId;
    }

    /**
     * @param int $toCodeId
     * @return $this
     */
    public function setToCodeId($toCodeId)
    {
        $this->toCodeId = null !== $toCodeId ? (int)$toCodeId : null;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWarehouseOrderItemId()
    {
        return $this->warehouseOrderItemId;
    }

    /**
     * @param mixed $warehouseOrderItemId
     */
    public function setWarehouseOrderItemId($warehouseOrderItemId): void
    {
        $this->warehouseOrderItemId = $warehouseOrderItemId;
    }

    /**
     * @return int
     */
    public function getProductSizeId()
    {
        return $this->productSizeId;
    }

    /**
     * @param int $productSizeId
     * @return $this
     */
    public function setProductSizeId($productSizeId)
    {
        $this->productSizeId = null !== $productSizeId ? (int)$productSizeId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setTypeS(string $typeStr)
    {
        $type = array_flip($this->getTypeMap())[$typeStr];
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param int $qty
     * @return $this
     */
    public function setQty($qty)
    {
        $this->qty = null !== $qty ? (int)$qty : null;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExclude()
    {
        return $this->exclude;
    }

    /**
     * @param bool $exclude
     * @return $this
     */
    public function setExclude($exclude)
    {
        $this->exclude = (bool)$exclude;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return null|ProductSize
     *
     * @throws \DomainException
     */
    public function getProductSize()
    {
        return $this->getRelated(self::ALIAS_PRODUCT_SIZE);
    }

    /**
     * @param ProductSize $productSize
     * @return $this
     */
    public function setProductSize(ProductSize $productSize)
    {
        $this->productSizeId = $productSize->getId();

        return $this;
    }

    /**
     * @return null|Code
     *
     * @throws \DomainException
     */
    public function getCodeTo()
    {
        return $this->getRelated(self::ALIAS_CODE_TO);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setCodeTo(Code $code)
    {
        $this->toCodeId = $code->getId();

        return $this;
    }

    /**
     * @return null|Code
     *
     * @throws \DomainException
     */
    public function getCodeFrom()
    {
        return $this->getRelated(self::ALIAS_CODE_FROM);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setCodeFrom(Code $code)
    {
        $this->fromCodeId = $code->getId();

        return $this;
    }

    /**
     * @return null|WarehouseOrderItem
     *
     * @throws \DomainException
     */
    public function getWarehouseOrderItem()
    {
        return $this->getRelated(self::ALIAS_WAREHOUSE_ORDER_ITEM);
    }

    /**
     * @param WarehouseOrderItem $warehouseOrderItem
     * @return $this
     */
    public function setWarehouseOrderItem(WarehouseOrderItem $warehouseOrderItem)
    {
        $this->warehouseOrderItemId = $warehouseOrderItem->getId();

        return $this;
    }

    private function getTypeMap()
    {
        return [
            self::REDOVNI_TRANSFERI => 'REDOVNI TRANSFERI',
            self::NEDOPUNJENE_VELICINE => 'NEDOPUNJENE VELICINE',
            self::DESORTIRANI_TRANSFERI => 'DESORTIRANI TRANSFERI',
            self::KOMPLETIRANJE_OLD => 'KOMPLETIRANJE (repovi)',
            self::KOMPLETIRANJE => 'KOMPLETIRANJE',
            self::KOMPLETIRANJE_INDJ => 'KOMPLETIRANJE (indjija)',
            self::DOPUNA_SA_CM => 'DOPUNA SA CM',
        ];
    }
}
