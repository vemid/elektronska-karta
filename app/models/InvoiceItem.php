<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * InvoiceItem Model Class
 *
 * @Source('invoice_items')
 *
 * @BelongsTo('invoiceId', 'Invoice', 'id', {'alias':'Invoice'})
 * @BelongsTo('productSizeId', 'ProductSize', 'id', {'alias':'ProductSize'})
 */
class InvoiceItem extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_INVOICE_ID = 'invoiceId';
    const PROPERTY_PRODUCT_SIZE_ID = 'productSizeId';
    const PROPERTY_PRICE = 'price';
    const PROPERTY_QTY = 'qty';
    const PROPERTY_TARIFF_CODE = 'tariffCode';

    const ALIAS_INVOICE = 'Invoice';
    const ALIAS_PRODUCT_SIZE = 'ProductSize';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="invoice_id", type="integer", nullable=false)
     * @FormElement(label="Invoice", type="Select", required=true, relation="Invoice")
     */
    protected $invoiceId;

    /**
     * @Column(column="product_size_id", type="integer", nullable=false)
     * @FormElement(label="Product size", type="Select", required=true, relation="ProductSize")
     */
    protected $productSizeId;

    /**
     * @Column(column="price", type="decimal", nullable=false)
     * @FormElement(label="Price", type="Text", required=true)
     */
    protected $price;

    /**
     * @Column(column="qty", type="decimal", nullable=false)
     * @FormElement(label="Qty", type="Text", required=true)
     */
    protected $qty;

    /**
     * @Column(column="tariff_code", type="string", nullable=true)
     * @FormElement(label="Tariff code", type="Text", required=false)
     */
    protected $tariffCode;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_INVOICE_ID => true,
        self::PROPERTY_PRODUCT_SIZE_ID => true,
        self::PROPERTY_PRICE => true,
        self::PROPERTY_QTY => true,
        self::PROPERTY_TARIFF_CODE => true,
        self::ALIAS_INVOICE => true,
        self::ALIAS_PRODUCT_SIZE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param int $invoiceId
     * @return $this
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = null !== $invoiceId ? (int)$invoiceId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductSizeId()
    {
        return $this->productSizeId;
    }

    /**
     * @param int $productSizeId
     * @return $this
     */
    public function setProductSizeId($productSizeId)
    {
        $this->productSizeId = null !== $productSizeId ? (int)$productSizeId : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = null !== $price ? (float)$price : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param float $qty
     * @return $this
     */
    public function setQty($qty)
    {
        $this->qty = null !== $qty ? (float)$qty : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getTariffCode()
    {
        return $this->tariffCode;
    }

    /**
     * @param string $tariffCode
     * @return $this
     */
    public function setTariffCode($tariffCode)
    {
        $this->tariffCode = $tariffCode;

        return $this;
    }

    /**
     * @return null|Invoice
     * 
     * @throws \DomainException
     */
    public function getInvoice()
    {
        return $this->getRelated(self::ALIAS_INVOICE);
    }

    /**
     * @param Invoice $invoice
     * @return $this
     */
    public function setInvoice(Invoice $invoice)
    {
        $this->invoiceId = $invoice->getId();

        return $this;
    }

    /**
     * @return null|ProductSize
     * 
     * @throws \DomainException
     */
    public function getProductSize()
    {
        return $this->getRelated(self::ALIAS_PRODUCT_SIZE);
    }

    /**
     * @param ProductSize $productSize
     * @return $this
     */
    public function setProductSize(ProductSize $productSize)
    {
        $this->productSizeId = $productSize->getId();

        return $this;
    }

}
