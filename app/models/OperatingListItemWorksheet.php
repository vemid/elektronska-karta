<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * OperatingListItemWorksheet Model Class
 *
 * @Source('operating_list_item_worksheets')
 *
 * @BelongsTo('operatingListItemId', 'OperatingListItem', 'id', {'alias':'OperatingListItem'})
 * @BelongsTo('productionWorkerId', 'ProductionWorker', 'id', {'alias':'ProductionWorker'})
 * @BelongsTo('codeId', 'Code', 'id', {'alias':'Code'})
 */
class OperatingListItemWorksheet extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_OPERATING_LIST_ITEM_ID = 'operatingListItemId';
    const PROPERTY_PRODUCTION_WORKER_ID = 'productionWorkerId';
    const PROPERTY_CODE_ID = 'codeId';
    const PROPERTY_QTY = 'qty';
    const PROPERTY_DATE = 'date';
    const PROPERTY_START_TIME = 'startTime';
    const PROPERTY_END_TIME = 'endTime';
    const PROPERTY_OVERTIME = 'overtime';
    const PROPERTY_APPROVED = 'approved';
    const PROPERTY_CREATED = 'created';

    const ALIAS_OPERATING_LIST_ITEM = 'OperatingListItem';
    const ALIAS_PRODUCTION_WORKER = 'ProductionWorker';
    const ALIAS_CODE = 'Code';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="operating_list_item_id", type="integer", nullable=false)
     * @FormElement(label="Operating list item", type="Select", required=true, relation="OperatingListItem")
     */
    protected $operatingListItemId;

    /**
     * @Column(column="production_worker_id", type="integer", nullable=false)
     * @FormElement(label="Production worker", type="Select", required=true, relation="ProductionWorker")
     */
    protected $productionWorkerId;

    /**
     * @Column(column="code_id", type="integer", nullable=true)
     * @FormElement(label="Code", type="Select", required=false, relation="Code")
     */
    protected $codeId;

    /**
     * @Column(column="qty", type="decimal", nullable=false)
     * @FormElement(label="Qty", type="Text", required=true)
     */
    protected $qty;

    /**
     * @Column(column="date", type="date", nullable=false)
     * @FormElement(label="Date", type="Date", required=true)
     */
    protected $date;

    /**
     * @Column(column="start_time", type="string", nullable=false)
     * @FormElement(label="Start time", type="Text", required=true)
     */
    protected $startTime;

    /**
     * @Column(column="end_time", type="string", nullable=false)
     * @FormElement(label="End time", type="Text", required=true)
     */
    protected $endTime;

    /**
     * @Column(column="overtime", type="bool", nullable=true)
     * @FormElement(label="Overtime", type="Check", required=false)
     */
    protected $overtime;

    /**
     * @Column(column="approved", type="bool", nullable=true)
     * @FormElement(label="Approved", type="Check", required=false)
     */
    protected $approved;

    /**
     * @Column(column="created", type="string", nullable=false)
     * @FormElement(label="Created", type="Text", required=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_OPERATING_LIST_ITEM_ID => true,
        self::PROPERTY_PRODUCTION_WORKER_ID => true,
        self::PROPERTY_CODE_ID => true,
        self::PROPERTY_QTY => true,
        self::PROPERTY_DATE => true,
        self::PROPERTY_START_TIME => true,
        self::PROPERTY_END_TIME => true,
        self::PROPERTY_CREATED => true,
        self::ALIAS_OPERATING_LIST_ITEM => true,
        self::ALIAS_PRODUCTION_WORKER => true,
        self::ALIAS_CODE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getOperatingListItemId()
    {
        return $this->operatingListItemId;
    }

    /**
     * @param int $operatingListItemId
     * @return $this
     */
    public function setOperatingListItemId($operatingListItemId)
    {
        $this->operatingListItemId = null !== $operatingListItemId ? (int)$operatingListItemId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductionWorkerId()
    {
        return $this->productionWorkerId;
    }

    /**
     * @param int $productionWorkerId
     * @return $this
     */
    public function setProductionWorkerId($productionWorkerId)
    {
        $this->productionWorkerId = null !== $productionWorkerId ? (int)$productionWorkerId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * @param int $codeId
     * @return $this
     */
    public function setCodeId($codeId)
    {
        $this->codeId = null !== $codeId ? (int)$codeId : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param float $qty
     * @return $this
     */
    public function setQty($qty)
    {
        $this->qty = null !== $qty ? (float)$qty : null;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string|DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param string $startTime
     * @return $this
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param string $endTime
     * @return $this
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdentityField(): string
    {
        return $this->identityField;
    }

    /**
     * @param string $identityField
     */
    public function setIdentityField(string $identityField)
    {
        $this->identityField = $identityField;
    }

    /**
     * @return bool
     */
    public function getOvertime()
    {
        return $this->overtime;
    }

    /**
     * @param bool $overtime
     */
    public function setOvertime($overtime)
    {
        $this->overtime = (bool)$overtime;
    }

    /**
     * @return bool
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * @param bool $approved
     */
    public function setApproved($approved)
    {
        $this->approved = (bool)$approved;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|OperatingListItem
     * 
     * @throws \DomainException
     */
    public function getOperatingListItem()
    {
        return $this->getRelated(self::ALIAS_OPERATING_LIST_ITEM);
    }

    /**
     * @param OperatingListItem $operatingListItem
     * @return $this
     */
    public function setOperatingListItem(OperatingListItem $operatingListItem)
    {
        $this->operatingListItemId = $operatingListItem->getId();

        return $this;
    }

    /**
     * @return null|ProductionWorker
     * 
     * @throws \DomainException
     */
    public function getProductionWorker()
    {
        return $this->getRelated(self::ALIAS_PRODUCTION_WORKER);
    }

    /**
     * @param ProductionWorker $productionWorker
     * @return $this
     */
    public function setProductionWorker(ProductionWorker $productionWorker)
    {
        $this->productionWorkerId = $productionWorker->getId();

        return $this;
    }

    /**
     * @return null|Code
     * 
     * @throws \DomainException
     */
    public function getCode()
    {
        return $this->getRelated(self::ALIAS_CODE);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setCode(Code $code)
    {
        $this->codeId = $code->getId();

        return $this;
    }

    public function getWorksheetStartHour()
    {
        list($hours,$mins,$secs) = explode(':',$this->startTime);
        return $hours;
    }

    public function getWorksheetStartMin()
    {
        list($hours,$mins,$secs) = explode(':',$this->startTime);
        return $mins;
    }

    public function getWorksheetEndHour()
    {
        list($hours,$mins,$secs) = explode(':',$this->endTime);
        return $hours;
    }

    public function getWorksheetEndMin()
    {
        list($hours,$mins,$secs) = explode(':',$this->endTime);
        return $mins;
    }

    public function getTimeBetweenWorksheet()
    {
        $endTime = strtotime($this->endTime);
        $startTime = strtotime($this->startTime);
        return round(abs($endTime - $startTime) /60,2);
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
