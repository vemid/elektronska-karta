<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * OperatingList Model Class
 *
 * @Source('operating_lists')
 *
 * @BelongsTo('productId', 'Product', 'id', {'alias':'Product'})
 * @BelongsTo('codeId', 'Code', 'id', {'alias':'Code'})
 * @HasMany('id', 'OperatingListItem', 'operatingListId', {'alias':'OperatingListItems'})
 */
class OperatingList extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PRODUCT_ID = 'productId';
    const PROPERTY_CODE_ID = 'codeId';
    const PROPERTY_VERSION = 'version';
    const PROPERTY_ACTIVE = 'active';
    const PROPERTY_FINISHED = 'finished';
    const PROPERTY_LOCKED = 'locked';
    const PROPERTY_TACT_GROUP = 'tactGroup';
    const PROPERTY_NOTE = 'note';
    const PROPERTY_CREATED = 'created';

    const ALIAS_PRODUCT = 'Product';
    const ALIAS_CODE = 'Code';
    const ALIAS_OPERATING_LIST_ITEMS = 'OperatingListItems';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="product_id", type="integer", nullable=false)
     * @FormElement(label="Product", type="Text", required=true )
     */
    protected $productId;

    /**
     * @Column(column="code_id", type="integer", nullable=false)
     * @FormElement(label="Sector ID", type="Select", required=true)
     */
    protected $codeId;

    /**
     * @Column(column="version", type="integer", nullable=true)
     */
    protected $version;

    /**
     * @Column(column="active", type="bool", nullable=true)
     */
    protected $active;

    /**
     * @Column(column="finished", type="bool", nullable=true)
     */
    protected $finished;

    /**
     * @Column(column="tact_group", type="decimal", nullable=true)
     * @FormElement(label="Tact Group", type="Text", required=true)
     */
    protected $tactGroup;

    /**
     * @Column(column="note", type="string", nullable=true)
     * @FormElement(label="Note", type="TextArea", required=false)
     */
    protected $note;

    /**
     * @Column(column="locked", type="bool", nullable=true)
     */
    protected $locked;



    /**
     * @Column(column="created", type="string", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRODUCT_ID => true,
        self::PROPERTY_CODE_ID => true,
        self::PROPERTY_VERSION => true,
        self::PROPERTY_ACTIVE => true,
        self::PROPERTY_FINISHED => true,
        self::PROPERTY_LOCKED => true,
        self::PROPERTY_CREATED => true,
        self::ALIAS_PRODUCT => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->productId = null !== $productId ? (int)$productId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * @param int $codeId
     * @return $this
     */
    public function setCodeId($codeId)
    {
        $this->codeId = null !== $codeId ? (int)$codeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param int $version
     * @return $this
     */
    public function setVersion($version)
    {
        $this->version = null !== $version ? (int)$version : null;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getFinished()
    {
        return $this->finished;
    }

    /**
     * @param boolean $finished
     * @return $this
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;

        return $this;
    }

    /**
     * @return int
     */
    public function getTactGroup()
    {
        return $this->tactGroup;
    }

    /**
     * @param int $tactGroup
     * @return $this
     */
    public function setTactGroup($tactGroup)
    {
        $this->tactGroup = null !== $tactGroup ? (float)$tactGroup : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * @param boolean $locked
     * @return $this
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * @return null|Product
     * 
     * @throws \DomainException
     */
    public function getProduct()
    {
        return $this->getRelated(self::ALIAS_PRODUCT);
    }

    /**
     * @return null|Code
     *
     * @throws \DomainException
     */
    public function getCode()
    {
        return $this->getRelated(self::ALIAS_CODE);
    }


    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->productId = $product->getId();

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|OperatingListItem[]
     * 
     * @throws \DomainException
     */
    public function getOperatingListItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_OPERATING_LIST_ITEMS, $arguments);
    }

    public function beforeSave()
    {
        if (null === $this->active) {
            $this->active = true;
            $this->created = new Vemid\Date\DateTime();
        }
    }

    public function getWorkOrders()
    {
        $query = $this->getDI()->getModelsManager()->createBuilder()
            ->columns([
                'GROUP_CONCAT(workOrder) as workOrder'
            ])
            ->addFrom(ProductionOrder::class, 'po')
            ->leftJoin(ProductSize::class, 'po.productSizeId = ps.id', 'ps')
            ->where('ps.productId = :productId:', [
                'productId' => $this->getProductId()
            ]);

        /** @var ProductionOrder[] $productionOrders */
        $productionOrders = $query->getQuery()->execute()[0];

        return !empty($productionOrders['workOrder']) ? implode(', ', explode(',', $productionOrders['workOrder'])) : '';
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
