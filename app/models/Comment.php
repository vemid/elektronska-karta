<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * Comment Model Class
 *
 * @Source('comments')
 *
 * @BelongsTo('productionWorkerId', 'ProductionWorker', 'id', {'alias':'ProductionWorker'})
 */
class Comment extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PRODUCTION_WORKER_ID = 'productionWorkerId';
    const PROPERTY_DATE = 'date';
    const PROPERTY_COMMENT = 'comment';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_CREATED = 'created';

    const ALIAS_PRODUCTION_WORKER = 'ProductionWorker';

    const TYPE_WORKING_TIME = 'WORKING_TIME';
    const TYPE_OPERATING_WORKSHEET = 'OPERATING_WORKSHEET';
    const TYPE_CUTTING_JOB = 'CUTTING_JOB';
    const TYPE_ABSENCE = 'ABSENCE';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="production_worker_id", type="integer", nullable=false)
     * @FormElement(label="Production worker", type="Hidden", required=true, relation="ProductionWorker")
     */
    protected $productionWorkerId;

    /**
     * @Column(column="comment", type="string", nullable=false)
     * @FormElement(label="Commnent", type="TextArea", required=true)
     */
    protected $comment;

    /**
     * @Column(column="date", type="date", nullable=false)
     * @FormElement(label="Date", type="Hidden", required=true)
     */
    protected $date;

    /**
     * @Column(column="type", type="string", nullable=false)
     * @FormElement(label="Vrsta odsustva", type="Hidden", required=false, options={'WORKING_TIME':'Radno Vreme', 'OPERATING_WORKSHEET':'Operacija', 'CUTTING_JOB':'Rezija','ABSENCE':'Odsustvo'})
     */
    protected $type;

    /**
     * @Column(column="created", type="string", nullable=false)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRODUCTION_WORKER_ID => true,
        self::PROPERTY_DATE => true,
        self::PROPERTY_COMMENT => true,
        self::PROPERTY_TYPE => true,
        self::PROPERTY_CREATED => true,
        self::ALIAS_PRODUCTION_WORKER => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductionWorkerId()
    {
        return $this->productionWorkerId;
    }

    /**
     * @param int $productionWorkerId
     * @return $this
     */
    public function setProductionWorkerId($productionWorkerId)
    {
        $this->productionWorkerId = null !== $productionWorkerId ? (int)$productionWorkerId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string|DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|ProductionWorker
     * 
     * @throws \DomainException
     */
    public function getProductionWorker()
    {
        return $this->getRelated(self::ALIAS_PRODUCTION_WORKER);
    }

    /**
     * @param ProductionWorker $productionWorker
     * @return $this
     */
    public function setProductionWorker(ProductionWorker $productionWorker)
    {
        $this->productionWorkerId = $productionWorker->getId();

        return $this;
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
