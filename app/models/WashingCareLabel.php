<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * WashingCareLabel Model Class
 *
 * @Source('washing_care_labels')
 */
class WashingCareLabel extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_NAME = 'name';
    const PROPERTY_PRINT_SIGN = 'printSign';
    const PROPERTY_IMAGE = 'image';
    const PROPERTY_DESCRIPTION = 'description';


    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="print_sign", type="string", nullable=false)
     * @FormElement(label="Print sign", type="Text", required=true)
     */
    protected $printSign;

    /**
     * @Column(column="image", type="string", nullable=true)
     * @FormElement(label="Image", type="File", required=false)
     */
    protected $image;

    /**
     * @Column(column="description", type="string", nullable=true)
     * @FormElement(label="Description", type="Text", required=false)
     */
    protected $description;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_PRINT_SIGN => true,
        self::PROPERTY_IMAGE => true,
        self::PROPERTY_DESCRIPTION => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrintSign()
    {
        return $this->printSign;
    }

    /**
     * @param string $printSign
     * @return $this
     */
    public function setPrintSign($printSign)
    {
        $this->printSign = $printSign;

        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return APP_PATH . 'var/uploads/files/';
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $url = ltrim($config->application->baseUriImage, '/');

        if (is_file($this->getUploadPath() . $this->getImage())) {
            return $url . '/uploads/files/' . $this->getImage();
        }

        return null;
    }


}
