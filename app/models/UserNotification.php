<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * UserNotification Model Class
 *
 * @Source('user_notifications')
 *
 * @BelongsTo('userId', 'User', 'id', {'alias':'User'})
 * @BelongsTo('notificationTypeId', 'NotificationType', 'id', {'alias':'NotificationType'})
 * @HasMany('id', 'UserNotificationMessage', 'userNotificationId', {'alias':'UserNotificationMessages'})
 */
class UserNotification extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_USER_ID = 'userId';
    const PROPERTY_NOTIFICATION_TYPE_ID = 'notificationTypeId';
    const PROPERTY_SUBSCRIBED = 'subscribed';

    const ALIAS_USER = 'User';
    const ALIAS_NOTIFICATION_TYPE = 'NotificationType';
    const ALIAS_USER_NOTIFICATION_MESSAGES = 'UserNotificationMessages';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="user_id", type="integer", nullable=false)
     * @FormElement(label="User", type="Select", required=true, relation="User")
     */
    protected $userId;

    /**
     * @Column(column="notification_type_id", type="integer", nullable=false)
     * @FormElement(label="Notification type", type="Select", required=true, relation="NotificationType")
     */
    protected $notificationTypeId;

    /**
     * @Column(column="subscribed", type="integer", nullable=false)
     * @FormElement(label="Subscribed", type="Check", required=true)
     */
    protected $subscribed;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_USER_ID => true,
        self::PROPERTY_NOTIFICATION_TYPE_ID => true,
        self::PROPERTY_SUBSCRIBED => true,
        self::ALIAS_USER => true,
        self::ALIAS_NOTIFICATION_TYPE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = null !== $userId ? (int)$userId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getNotificationTypeId()
    {
        return $this->notificationTypeId;
    }

    /**
     * @param int $notificationTypeId
     * @return $this
     */
    public function setNotificationTypeId($notificationTypeId)
    {
        $this->notificationTypeId = null !== $notificationTypeId ? (int)$notificationTypeId : null;

        return $this;
    }

    /**
     * @return bool
     */
    public function getSubscribed()
    {
        return (bool)$this->subscribed;
    }

    /**
     * @param int|bool $subscribed
     * @return $this
     */
    public function setSubscribed($subscribed)
    {
        $this->subscribed = $subscribed ? 1 : 0;

        return $this;
    }

    /**
     * @return null|User
     * 
     * @throws \DomainException
     */
    public function getUser()
    {
        return $this->getRelated(self::ALIAS_USER);
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->userId = $user->getId();

        return $this;
    }

    /**
     * @return null|NotificationType
     * 
     * @throws \DomainException
     */
    public function getNotificationType()
    {
        return $this->getRelated(self::ALIAS_NOTIFICATION_TYPE);
    }

    /**
     * @param NotificationType $notificationType
     * @return $this
     */
    public function setNotificationType(NotificationType $notificationType)
    {
        $this->notificationTypeId = $notificationType->getId();

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|UserNotificationMessage[]
     * 
     * @throws \DomainException
     */
    public function getUserNotificationMessages($arguments = null)
    {
        return $this->getRelated(self::ALIAS_USER_NOTIFICATION_MESSAGES, $arguments);
    }

}
