<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * CalculationMapping Model Class
 *
 * @Source('calculation_mappings')
 *
 * @BelongsTo('classificationId', 'Classification', 'id', {'alias':'Classification'})
 */
class CalculationMapping extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CLASSIFICATION_ID = 'classificationId';
    const PROPERTY_MARK_UP = 'markUp';
    const PROPERTY_EUR_COURSE = 'eurCourse';

    const ALIAS_CLASSIFICATION = 'Classification';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="classification_id", type="integer", nullable=false)
     * @FormElement(label="Classification", type="Select", required=true)
     */
    protected $classificationId;

    /**
     * @Column(column="mark_up", type="decimal", nullable=false)
     * @FormElement(label="Mark up", type="Text", required=true)
     */
    protected $markUp;

    /**
     * @Column(column="eur_course", type="decimal", nullable=false)
     * @FormElement(label="Eur course", type="Text", required=true)
     */
    protected $eurCourse;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CLASSIFICATION_ID => true,
        self::PROPERTY_MARK_UP => true,
        self::PROPERTY_EUR_COURSE => true,
        self::ALIAS_CLASSIFICATION => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getClassificationId()
    {
        return $this->classificationId;
    }

    /**
     * @param int $classificationId
     * @return $this
     */
    public function setClassificationId($classificationId)
    {
        $this->classificationId = null !== $classificationId ? (int)$classificationId : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getMarkUp()
    {
        return $this->markUp;
    }

    /**
     * @param float $markUp
     * @return $this
     */
    public function setMarkUp($markUp)
    {
        $this->markUp = null !== $markUp ? (float)$markUp : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getEurCourse()
    {
        return $this->eurCourse;
    }

    /**
     * @param float $eurCourse
     * @return $this
     */
    public function setEurCourse($eurCourse)
    {
        $this->eurCourse = null !== $eurCourse ? (float)$eurCourse : null;

        return $this;
    }

    /**
     * @return null|Classification
     * 
     * @throws \DomainException
     */
    public function getClassification()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setClassification(Classification $classification)
    {
        $this->classificationId = $classification->getId();

        return $this;
    }

    public function beforeSave()
    {
        if ($this->getMarkUp() >=  1) {
            $this->markUp = $this->getMarkUp() / 100;
        }
    }

}
