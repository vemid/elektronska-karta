<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * OperatingListItemType Model Class
 *
 * @Source('operating_list_item_types')
 *
 * @BelongsTo('codeId', 'Code', 'id', {'alias':'Code'})
 * @BelongsTo('productionMachineId', 'ProductionMachine', 'id', {'alias':'ProductionMachine'})
 * @HasMany('id', 'OperatingListItem', 'operatingListItemTypeId', {'alias':'OperatingListItems'})
 */
class OperatingListItemType extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CODE_ID = 'codeId';
    const PROPERTY_NAME = 'name';
    const PROPERTY_QTY = 'qty';
    const PROPERTY_MACHINE = 'machine';
    const PROPERTY_PRODUCTION_MACHINE_ID = 'productionMachineId';
    const PROPERTY_CREATED = 'created';

    const ALIAS_CODE = 'Code';
    const ALIAS_PRODUCTION_MACHINE = 'ProductionMachine';
    const ALIAS_OPERATING_LIST_ITEMS = 'OperatingListItems';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="code_id", type="integer", nullable=false)
     * @FormElement(label="Code", type="Select", required=true)
     */
    protected $codeId;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="qty", type="decimal", nullable=true)
     */
    protected $qty;

    /**
     * @Column(column="machine", type="string", nullable=true)
     */
    protected $machine;

    /**
     * @Column(column="production_machine_id", type="string", nullable=false)
     * @FormElement(label="Machine", type="Select", required=true, relation='ProductionMachine')
     */
    protected $productionMachineId;

    /**
     * @Column(column="created", type="string", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CODE_ID => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_QTY => true,
        self::PROPERTY_MACHINE => true,
        self::PROPERTY_CREATED => true,
        self::ALIAS_CODE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * @param int $codeId
     * @return $this
     */
    public function setCodeId($codeId)
    {
        $this->codeId = null !== $codeId ? (int)$codeId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return float
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param float $qty
     * @return $this
     */
    public function setQty($qty)
    {
        $this->qty = null !== $qty ? (float)$qty : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getMachine()
    {
        return $this->machine;
    }

    /**
     * @param string $machine
     * @return $this
     */
    public function setMachine($machine)
    {
        $this->machine = $machine;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|Code
     *
     * @throws \DomainException
     */
    public function getCode()
    {
        return $this->getRelated(self::ALIAS_CODE);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setCode(Code $code)
    {
        $this->codeId = $code->getId();

        return $this;
    }

    /**
     * @return null|ProductionMachine
     *
     * @throws \DomainException
     */
    public function getProductionMachine()
    {
        return $this->getRelated(self::ALIAS_PRODUCTION_MACHINE);
    }

    /**
     * @param ProductionMachine $productionMachine
     * @return $this
     */
    public function setProductionMachine(ProductionMachine $productionMachine)
    {
        $this->productionMachineId = $productionMachine->getId();

        return $this;
    }

    public function validation()
    {
        $validator = new \Phalcon\Validation();

        $validator->add(
            self::PROPERTY_NAME, new \Phalcon\Validation\Validator\Uniqueness([
                'model' => $this,
                'allowEmpty' => true,
                'message' => 'Tip operacije vec postoji!'
            ])
        );

        return $this->validate($validator);
    }


    /**
     * @param null|string|array $arguments
     * @return Resultset|OperatingListItem[]
     *
     * @throws \DomainException
     */
    public function getOperatingListItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_OPERATING_LIST_ITEMS, $arguments);
    }

    public function getDisplayName()
    {
        return $this->getName();
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
        $this->qty = 0;
    }
}
