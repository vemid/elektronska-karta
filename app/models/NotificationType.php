<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * NotificationType Model Class
 *
 * @Source('notification_types')
 * @HasMany('id', 'UserNotification', 'notificationTypeId', {'alias':'UserNotifications'})
 */
class NotificationType extends AbstractEntity
{

    const MATERIAL_SAMPLE = 'MATERIAL_SAMPLE';

    const PROPERTY_ID = 'id';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_NAME = 'name';
    const PROPERTY_DESCRIPTION = 'description';
    const PROPERTY_ALLOWED_TO_UNSUBSCRIBE = 'allowedToUnsubscribe';

    const ALIAS_USER_NOTIFICATIONS = 'UserNotifications';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="type", type="string", nullable=false)
     * @FormElement(label="Type", type="Text", required=true)
     */
    protected $type;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="description", type="string", nullable=false)
     * @FormElement(label="Description", type="Text", required=true)
     */
    protected $description;

    /**
     * @Column(column="allowed_to_unsubscribe", type="bool", nullable=true)
     */
    protected $allowedToUnsubscribe;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_TYPE => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_DESCRIPTION => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return bool
     */
    public function getAllowedToUnsubscribe()
    {
        return $this->allowedToUnsubscribe;
    }

    /**
     * @param bool $allowedToUnsubscribe
     * @return $this
     */
    public function setAllowedToUnsubscribe($allowedToUnsubscribe)
    {
        $this->allowedToUnsubscribe = $allowedToUnsubscribe;

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|UserNotification[]
     * 
     * @throws \DomainException
     */
    public function getUserNotifications($arguments = null)
    {
        return $this->getRelated(self::ALIAS_USER_NOTIFICATIONS, $arguments);
    }

    public function __toString()
    {
        return $this->getName();
    }
}
