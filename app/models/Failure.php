<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * Failure Model Class
 *
 * @Source('failures')
 *
 * @BelongsTo('productSizeId', 'ProductSize', 'id', {'alias':'ProductSize'})
 * @BelongsTo('failureCodeId', 'Code', 'id', {'alias':'FailureCode'})
 * @BelongsTo('shopCodeId', 'Code', 'id', {'alias':'ShopCode'})
 */
class Failure extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_DOCUMENT = 'document';
    const PROPERTY_DATE = 'date';
    const PROPERTY_SHOP_CODE_ID = 'shopCodeId';
    const PROPERTY_WAREHOUSE = 'warehouse';
    const PROPERTY_PRODUCT_SIZE_ID = 'productSizeId';
    const PROPERTY_QTY = 'qty';
    const PROPERTY_FAILURE_CODE_ID = 'failureCodeId';
    const PROPERTY_NOTE = 'note';
    const PROPERTY_PROCES_TYPE = 'procesType';
    const PROPERTY_FAILURE_ORIGIN= 'failureOrigin';
    const PROPERTY_CREATED = 'created';

    const STATUS_OTPIS = 'OTPIS';
    const STATUS_FELER = 'FELER';
    const STATUS_SNIZENJE = 'SNIZENJE';
    const STATUS_POPRAVLJENO = 'POPRAVLJENO';
    const STATUS_APP = 'APP';

    const ORIGIN_RETAIL = 'RETAIL';
    const ORIGIN_WHOLESALE = 'WHOLESALE';
    const ORIGIN_PRODUCTION = 'PRODUCTION';

    const ALIAS_PRODUCT_SIZE = 'ProductSize';
    const ALIAS_SHOP_CODE_ID = 'ShopCode';
    const ALIAS_FAILURE_CODE_ID = 'FailureCode';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="document", type="string", nullable=false)
     * @FormElement(label="Dokument", type="Text", required=true)
     */
    protected $document;

    /**
     * @Column(column="date", type="date", nullable=false)
     * @FormElement(label="Datum", type="Date", required=true)
     */
    protected $date;

    /**
     * @Column(column="shop_code_id", type="integer", nullable=false)
     * @FormElement(label="Objekat", type="Select", required=true)
     */
    protected $shopCodeId;

    /**
     * @Column(column="warehouse", type="string", nullable=false)
     * @FormElement(label="Magacin", type="Text", required=true)
     */
    protected $warehouse;

    /**
     * @Column(column="product_size_id", type="integer", nullable=false)
     * @FormElement(label="Proizvod - sifra", type="Select", required=true, relation="ProductSize")
     */
    protected $productSizeId;

    /**
     * @Column(column="qty", type="integer", nullable=false)
     * @FormElement(label="Kolicina", type="Text", required=true)
     */
    protected $qty;

    /**
     * @Column(column="failure_code_id", type="integer", nullable=false)
     * @FormElement(label="Vrsta felera", type="Select", required=true)
     */
    protected $failureCodeId;

    /**
     * @Column(column="note", type="string", nullable=true)
     * @FormElement(label="Napomena", type="TextArea", required=false)
     */
    protected $note;

    /**
     * @Column(column="proces_type", type="string", nullable=true)
     * @FormElement(label="Tip Realizacije", type="Select", required=false, options={'OTPIS':'Otpis', 'FELER':'Feler', 'SNIZENJE':'Snizenje', 'POPRAVLJENO':'Popravljeno','APP':'Ako prodje-prodje'})

     */
    protected $procesType;

    /**
     * @Column(column="failure_origin", type="string", nullable=false)
     * @FormElement(label="Prijavljuje feler", type="Select", required=true, options={'RETAIL':'Maloprodaja', 'WHOLESALE':'VP', 'PRODUCTION':'Proizvodnja'})
     */
    protected $failureOrigin;

    /**
     * @Column(column="created", type="string", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_DOCUMENT => true,
        self::PROPERTY_DATE => true,
        self::PROPERTY_SHOP_CODE_ID => true,
        self::PROPERTY_WAREHOUSE => true,
        self::PROPERTY_PRODUCT_SIZE_ID => true,
        self::PROPERTY_QTY => true,
        self::PROPERTY_FAILURE_CODE_ID => true,
        self::PROPERTY_NOTE => true,
        self::PROPERTY_PROCES_TYPE => true,
        self::PROPERTY_FAILURE_ORIGIN => true,
        self::PROPERTY_CREATED => true,
        self::ALIAS_PRODUCT_SIZE => true,
        self::ALIAS_SHOP_CODE_ID => true,
        self::ALIAS_FAILURE_CODE_ID => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param string $document
     * @return $this
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string|DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return int
     */
    public function getShopCodeId()
    {
        return $this->shopCodeId;
    }

    /**
     * @param int $shopCodeId
     * @return $this
     */
    public function setShopCodeId($shopCodeId)
    {
        $this->shopCodeId = null !== $shopCodeId ? (int)$shopCodeId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * @param string $warehouse
     * @return $this
     */
    public function setWarehouse($warehouse)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductSizeId()
    {
        return $this->productSizeId;
    }

    /**
     * @param int $productSizeId
     * @return $this
     */
    public function setProductSizeId($productSizeId)
    {
        $this->productSizeId = null !== $productSizeId ? (int)$productSizeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param int $qty
     * @return $this
     */
    public function setQty($qty)
    {
        $this->qty = null !== $qty ? (int)$qty : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getFailureCodeId()
    {
        return $this->failureCodeId;
    }

    /**
     * @param int $failureCodeId
     * @return $this
     */
    public function setFailureCodeId($failureCodeId)
    {
        $this->failureCodeId = null !== $failureCodeId ? (int)$failureCodeId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return string
     */
    public function getProcesType()
    {
        return $this->_translate($this->procesType ? $this->procesType : "Nije upisano");
    }

    /**
     * @param string $procesType
     * @return $this
     */
    public function setProcesType($procesType)
    {
        $this->procesType = $procesType;

        return $this;
    }

    /**
     * @return string
     */
    public function getFailureOrigin()
    {
        return $this->_translate($this->failureOrigin ? $this->failureOrigin : 'Nije upisano');
    }

    /**
     * @param string $failureOrigin
     * @return $this
     */
    public function setFailureOrigin($failureOrigin)
    {
        $this->failureOrigin = $failureOrigin;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|ProductSize
     * 
     * @throws \DomainException
     */
    public function getProductSize()
    {
        return $this->getRelated(self::ALIAS_PRODUCT_SIZE);
    }

    /**
     * @param ProductSize $productSize
     * @return $this
     */
    public function setProductSize(ProductSize $productSize)
    {
        $this->productSizeId = $productSize->getId();

        return $this;
    }

    /**
     * @return null|Code
     *
     * @throws \DomainException
     */
    public function getShopCode()
    {
        return $this->getRelated(self::ALIAS_SHOP_CODE_ID);
    }

    /**
     * @param Code $shopCode
     * @return $this
     */
    public function setShopCode(Code $shopCode)
    {
        $this->shopCodeId = $shopCode->getId();

        return $this;
    }

    /**
     * @return null|Code
     *
     * @throws \DomainException
     */
    public function getFailureCode()
    {
        return $this->getRelated(self::ALIAS_FAILURE_CODE_ID);
    }

    /**
     * @param Code $failureCode
     * @return $this
     */
    public function setFailureCode(Code $failureCode)
    {
        $this->failureCodeId = $failureCode->getId();

        return $this;
    }

    public function beforeCreate()
    {
        if($this->qty<50){
            $this->procesType = self::STATUS_FELER;
        }

    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
