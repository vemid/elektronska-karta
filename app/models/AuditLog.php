<?php

use Vemid\Date\DateTime;
use Vemid\Entity\EntityInterface;
use \Vemid\Entity\AbstractEntity;
use \Vemid\Entity\Manager\EntityManagerInterface;

/**
 * AuditLog Model Class
 *
 * @Source('audit_logs')
 *
 * @BelongsTo('userId', 'User', 'id', {'alias':'User'})
 */
class AuditLog extends AbstractEntity
{
    const OPERATION_CREATE = 'CREATE';
    const OPERATION_UPDATE = 'UPDATE';
    const OPERATION_DELETE = 'DELETE';

    const PROPERTY_ID = 'id';
    const PROPERTY_MODIFIED_ENTITY_NAME = 'modifiedEntityName';
    const PROPERTY_MODIFIED_ENTITY_ID = 'modifiedEntityId';
    const PROPERTY_OPERATION = 'operation';
    const PROPERTY_USER_ID = 'userId';
    const PROPERTY_OLD_DATA = 'oldData';
    const PROPERTY_NEW_DATA = 'newData';
    const PROPERTY_TIMESTAMP = 'timestamp';

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="modified_entity_name", type="string", nullable=false)
     */
    protected $modifiedEntityName;

    /**
     * @Column(column="modified_entity_id", type="integer", nullable=false)
     */
    protected $modifiedEntityId;

    /**
     * @Column(column="operation", type="string", nullable=false)
     */
    protected $operation;

    /**
     * @Column(column="user_id", type="integer", nullable=true)
     */
    protected $userId;

    /**
     * @Column(column="old_data", type="string", nullable=true)
     */
    protected $oldData;

    /**
     * @Column(column="new_data", type="string", nullable=true)
     */
    protected $newData;

    /**
     * @Column(column="timestamp", type="dateTime", nullable=false)
     */
    protected $timestamp;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedEntityName()
    {
        return $this->modifiedEntityName;
    }

    /**
     * @param string $modifiedEntityName
     * @return $this
     */
    public function setModifiedEntityName($modifiedEntityName)
    {
        $this->modifiedEntityName = $modifiedEntityName;

        return $this;
    }

    /**
     * @return int
     */
    public function getModifiedEntityId()
    {
        return $this->modifiedEntityId;
    }

    /**
     * @param int $modifiedEntityId
     * @return $this
     */
    public function setModifiedEntityId($modifiedEntityId)
    {
        $this->modifiedEntityId = $modifiedEntityId;

        return $this;
    }

    /**
     * @return string
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * @param string $operation
     * @return $this
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return array
     */
    public function getOldData()
    {
        return json_decode($this->oldData, true);
    }

    /**
     * @param array $oldData
     * @return $this
     */
    public function setOldData($oldData)
    {
        if (is_array($oldData)) {
            $oldData = json_encode($oldData);
        }

        $this->oldData = $oldData;

        return $this;
    }

    /**
     * @return array
     */
    public function getNewData()
    {
        return json_decode($this->newData, true);
    }

    /**
     * @param array $newData
     * @return $this
     */
    public function setNewData($newData)
    {
        if (is_array($newData)) {
            $newData = json_encode($newData);
        }

        $this->newData = $newData;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param string|DateTime $timestamp
     * @return $this
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * @return EntityInterface|User|null
     * @throws DomainException
     */
    public function getUser()
    {
        return $this->getRelated('User');
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->userId = $user->getId();

        return $this;
    }

    /**
     * @return null|EntityInterface
     */
    public function getModifiedEntity()
    {
        try {
            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->getDI()->getShared('entityManager');

            return $entityManager->findOne($this->modifiedEntityName, $this->modifiedEntityId);
        } catch (\Exception $ex) {
            return null;
        }
    }

    /**
     * @param EntityInterface $modifiedEntity
     * @return $this
     */
    public function setModifiedEntity(EntityInterface $modifiedEntity)
    {
        $this->modifiedEntityName = $modifiedEntity->getEntityName();
        $this->modifiedEntityId = $modifiedEntity->getEntityId();

        return $this;
    }

    /**
     *
     */
    public function beforeValidationOnCreate()
    {
        if ($this->timestamp === null) {
            $this->timestamp = new DateTime();
        }

        if ($this->userId === null && ($session = $this->getDI()->getShared('session'))) {
            if ($currentUser = $session->get('currentUser')) {
                $this->userId = $currentUser['id'];
            }
        }
    }
}
