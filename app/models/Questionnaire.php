<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * Questionnaire Model Class
 *
 * @Source('questionnaires')
 *
 * @BelongsTo('productId', 'Product', 'id', {'alias':'Product'})
 * @BelongsTo('codeId', 'Code', 'id', {'alias':'Code'})
 * @BelongsTo('userId', 'User', 'id', {'alias':'User'})
 * @HasMany('id', 'Question', 'questionnaireId', {'alias':'Questions'})
 */
class Questionnaire extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PRODUCT_ID = 'productId';
    const PROPERTY_CODE_ID = 'codeId';
    const PROPERTY_USER_ID = 'userId';
    const PROPERTY_IS_ANSWERED = 'isAnswered';
    const PROPERTY_CREATED = 'created';

    const ALIAS_PRODUCT = 'Product';
    const ALIAS_CODE = 'Code';
    const ALIAS_USER = 'User';
    const ALIAS_QUESTIONS = 'Questions';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="product_id", type="integer", nullable=false)
     * @FormElement(label="Product", type="Hidden", required=true)
     */
    protected $productId;

    /**
     * @Column(column="code_id", type="integer", nullable=false)
     * @FormElement(label="Code", type="Select", required=true)
     */
    protected $codeId;

    /**
     * @Column(column="user_id", type="integer", nullable=false)
     * @FormElement(label="User", type="Select", required=true, relation="User")
     */
    protected $userId;

    /**
     * @Column(column="is_answered", type="integer", nullable=false)
     * @FormElement(label="Is answered", type="Check", required=true)
     */
    protected $isAnswered;


    /**
     * @Column(column="created", type="dateTime", nullable=true)
     */
    protected $created;


    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRODUCT_ID => true,
        self::PROPERTY_CODE_ID => true,
        self::PROPERTY_USER_ID => true,
        self::PROPERTY_IS_ANSWERED => true,
        self::ALIAS_PRODUCT => true,
        self::ALIAS_CODE => true,
        self::ALIAS_USER => true,
        self::PROPERTY_CREATED => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->productId = null !== $productId ? (int)$productId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * @param int $codeId
     * @return $this
     */
    public function setCodeId($codeId)
    {
        $this->codeId = null !== $codeId ? (int)$codeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = null !== $userId ? (int)$userId : null;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsAnswered()
    {
        return (bool)$this->isAnswered;
    }

    /**
     * @param int|bool $isAnswered
     * @return $this
     */
    public function setIsAnswered($isAnswered)
    {
        $this->isAnswered = $isAnswered ? 1 : 0;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string|DateTime $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }


    /**
     * @return null|Product
     * 
     * @throws \DomainException
     */
    public function getProduct()
    {
        return $this->getRelated(self::ALIAS_PRODUCT);
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->productId = $product->getId();

        return $this;
    }

    /**
     * @return null|Code
     * 
     * @throws \DomainException
     */
    public function getCode()
    {
        return $this->getRelated(self::ALIAS_CODE);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setCode(Code $code)
    {
        $this->codeId = $code->getId();

        return $this;
    }

    /**
     * @return null|User
     * 
     * @throws \DomainException
     */
    public function getUser()
    {
        return $this->getRelated(self::ALIAS_USER);
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->userId = $user->getId();

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|Question[]
     * 
     * @throws \DomainException
     */
    public function getQuestions($arguments = null)
    {
        return $this->getRelated(self::ALIAS_QUESTIONS, $arguments);
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
