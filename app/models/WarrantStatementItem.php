<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * WarrantStatementItem Model Class
 *
 * @Source('warrant_statement_items')
 *
 * @BelongsTo('warrantStatementId', 'WarrantStatement', 'id', {'alias':'WarrantStatement'})
 */
class WarrantStatementItem extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_WARRANT_STATEMENT_ID = 'warrantStatementId';
    const PROPERTY_COMPANY_NAME = 'companyName';
    const PROPERTY_ACCOUNT = 'account';
    const PROPERTY_AMOUNT = 'amount';
    const PROPERTY_PURPOSE = 'purpose';
    const PROPERTY_WARRANT_NUMBER = 'warrantNumber';

    const ALIAS_WARRANT_STATEMENT = 'WarrantStatement';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="warrant_statement_id", type="integer", nullable=false)
     * @FormElement(label="Warrant statement", type="Select", required=true, relation="WarrantStatement")
     */
    protected $warrantStatementId;

    /**
     * @Column(column="company_name", type="string", nullable=false)
     * @FormElement(label="Company name", type="Text", required=true)
     */
    protected $companyName;

    /**
     * @Column(column="account", type="string", nullable=false)
     * @FormElement(label="Account", type="Text", required=true)
     */
    protected $account;

    /**
     * @Column(column="amount", type="decimal", nullable=false)
     * @FormElement(label="Amount", type="Text", required=true)
     */
    protected $amount;

    /**
     * @Column(column="purpose", type="string", nullable=false)
     * @FormElement(label="Purpose", type="Text", required=true)
     */
    protected $purpose;

    /**
     * @Column(column="warrant_number", type="string", nullable=false)
     * @FormElement(label="Warrant number", type="Text", required=true)
     */
    protected $warrantNumber;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_WARRANT_STATEMENT_ID => true,
        self::PROPERTY_COMPANY_NAME => true,
        self::PROPERTY_ACCOUNT => true,
        self::PROPERTY_AMOUNT => true,
        self::PROPERTY_PURPOSE => true,
        self::PROPERTY_WARRANT_NUMBER => true,
        self::ALIAS_WARRANT_STATEMENT => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getWarrantStatementId()
    {
        return $this->warrantStatementId;
    }

    /**
     * @param int $warrantStatementId
     * @return $this
     */
    public function setWarrantStatementId($warrantStatementId)
    {
        $this->warrantStatementId = null !== $warrantStatementId ? (int)$warrantStatementId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     * @return $this
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param string $account
     * @return $this
     */
    public function setAccount($account)
    {
        $this->account = (string)$account;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = null !== $amount ? (float)$amount : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * @param string $purpose
     * @return $this
     */
    public function setPurpose($purpose)
    {
        $this->purpose = (string)$purpose;

        return $this;
    }

    /**
     * @return string
     */
    public function getWarrantNumber()
    {
        return $this->warrantNumber;
    }

    /**
     * @param string $warrantNumber
     * @return $this
     */
    public function setWarrantNumber($warrantNumber)
    {
        $this->warrantNumber = (string)$warrantNumber;

        return $this;
    }

    /**
     * @return null|WarrantStatement
     * 
     * @throws \DomainException
     */
    public function getWarrantStatement()
    {
        return $this->getRelated(self::ALIAS_WARRANT_STATEMENT);
    }

    /**
     * @param WarrantStatement $warrantStatement
     * @return $this
     */
    public function setWarrantStatement(WarrantStatement $warrantStatement)
    {
        $this->warrantStatementId = $warrantStatement->getId();

        return $this;
    }
}
