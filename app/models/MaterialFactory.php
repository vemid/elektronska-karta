<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;
use \Phalcon\Mvc\Model\Behavior\Timestampable;

/**
 * MaterialFactory Model Class
 *
 * @Source('material_factories')
 *
 * @BelongsTo('materialSampleId', 'MaterialSample', 'id', {'alias':'MaterialSample'})
 * @BelongsTo('subMaterialCodeId', 'Classification', 'id', {'alias':'SubMaterialClassification'})
 * @BelongsTo('supplierCodeId', 'Supplier', 'id', {'alias':'SupplierName'})
 * @BelongsTo('seasonClassificationId', 'Classification', 'id', {'alias':'SeasonClassification'})
 * @HasMany('id', 'MaterialFactoryQuantity', 'materialFactoryId', {'alias':'MaterialFactoryQuantities'})
 * @HasMany('id', 'ProductCalculationItem', 'materialFactoryId', {'alias':'ProductCalculationItems'})
 */
class MaterialFactory extends AbstractEntity
{
    const PROPERTY_ID = 'id';
    const PROPERTY_MATERIAL_SAMPLE_ID = 'materialSampleId';
    const PROPERTY_SUPPLIER_CODE_ID = 'supplierCodeId';
    const PROPERTY_CARD_NUMBER = 'cardNumber';
    const PROPERTY_SEASON_CLASSIFICATION_ID = 'seasonClassificationId';
    const PROPERTY_SUB_MATERIAL_CODE_ID = 'subMaterialCodeId';
    const PROPERTY_FABRIC_CODE = 'fabricCode';
    const PROPERTY_MIS_CODE = 'misCode';
    const PROPERTY_COMPOSITION = 'composition';
    const PROPERTY_WASH_DATA = 'washData';
    const PROPERTY_WEIGHT = 'weight';
    const PROPERTY_HEIGHT = 'height';
    const PROPERTY_COLOR = 'color';
    const PROPERTY_PANTON_COLOR = 'pantonColor';
    const PROPERTY_MATERIAL_PICTURE = 'materialPicture';
    const PROPERTY_DATE_CREATED = 'dateCreated';
    const PROPERTY_COSTS = 'costs';
    const PROPERTY_ADDITIONAL_EXPENSES = 'additionalExpenses';
    const PROPERTY_INVOICE = 'invoice';
    const PROPERTY_INVOICE_SCAN = 'invoiceScan';
    const PROPERTY_NOTE = 'note';
    const PROPERTY_IS_EUR = 'isEur';
    const PROPERTY_CREATED = 'created';

    const ALIAS_MATERIAL_SAMPLE = 'MaterialSample';
    const ALIAS_CLASSIFICATION = 'Classification';
    const ALIAS_CLASSIFICATION_ID = 'SeasonClassification';
    const ALIAS_MATERIAL_FACTORY_QUANTITIES = 'MaterialFactoryQuantities';
    const ALIAS_PRODUCT_CALCULATION_ITEMS = 'ProductCalculationItems';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="material_sample_id", type="integer", nullable=true)
     * @FormElement(label="Kupon materijala", type="Select", required=false, relation="MaterialSample")
     */
    protected $materialSampleId;

    /**
     * @Column(column="supplier_code_id", type="integer", nullable=false)
     * @FormElement(label="Sifra dobavljaca", type="Select", required=true)
     */
    protected $supplierCodeId;

    /**
     * @Column(column="sub_material_code_id", type="integer", nullable=false)
     * @FormElement(label="Pod klasifikacija materijala", type="Select", required=true)
     */
    protected $subMaterialCodeId;

    /**
     * @Column(column="season_classification_id", type="integer", nullable=false)
     * @FormElement(label="Sezona", type="Select", required=true)
     */
    protected $seasonClassificationId;

    /**
     * @Column(column="card_number", type="integer", nullable=false)
     * @FormElement(label="Broj kartice", type="Text", required=true)
     */
    protected $cardNumber;

    /**
     * @Column(column="fabric_code", type="integer", nullable=false)
     * @FormElement(label="Sifra Materijala", type="Text", required=true)
     */
    protected $fabricCode;

    /**
     * @Column(column="mis_code", type="integer", nullable=false)
     * @FormElement(label="Sifra u MIS-u", type="Text", required=true)
     */
    protected $misCode;

    /**
     * @Column(column="composition", type="string", nullable=true)
     * @FormElement(label="Sastav", type="Text", required=false)
     */
    protected $composition;

    /**
     * @Column(column="wash_data", type="string", nullable=true)
     */
    protected $washData;

    /**
     * @Column(column="weight", type="string", nullable=false)
     * @FormElement(label="Tezina", type="Text", required=true)
     */
    protected $weight;

    /**
     * @Column(column="height", type="string", nullable=false)
     * @FormElement(label="Sirina", type="Text", required=true)
     */
    protected $height;

    /**
     * @Column(column="color", type="string", nullable=false)
     * @FormElement(label="Boja", type="Text", required=true)
     */
    protected $color;

    /**
     * @Column(column="panton_color", type="string", nullable=true)
     * @FormElement(label="Panton boja", type="Text", required=false)
     */
    protected $pantonColor;

    /**
     * @Column(column="material_picture", type="string", nullable=true)
     * @FormElement(label="Slika Materijala", type="Text", required=false)
     */
    protected $materialPicture;

    /**
     * @Column(column="date_created", type="dateTime", nullable=false)
     */
    protected $dateCreated;

    /**
     * @Column(column="costs", type="decimal", nullable=true)
     * @FormElement(label="Cena", type="Text", required=false)
     */
    protected $costs;

    /**
     * @Column(column="additional_expenses", type="decimal", nullable=true)
     * @FormElement(label="Dodatni troskovi", type="Text", required=false)
     */
    protected $additionalExpenses;

    /**
     * @Column(column="invoice", type="string", nullable=true)
     * @FormElement(label="Faktura", type="Text", required=false)
     */
    protected $invoice;

    /**
     * @Column(column="invoice_scan", type="string", nullable=true)
     * @FormElement(label="Skenirana faktura", type="Text", required=false)
     */
    protected $invoiceScan;

    /**
     * @Column(column="is_eur", type="bool", nullable=true)
     * @FormElement(label="Eur", type="Check", required=false)
     */
    protected $isEur;

    /**
     * @Column(column="note", type="string", nullable=true)
     * @FormElement(label="Napomena", type="Text", required=false)
     */
    protected $note;

    /**
     * @Column(column="created", type="dateTime", nullable=true)
     */
    protected $created;

    const ALIAS_SUB_MATERIAL_CLASSIFICATION = 'SubMaterialClassification';
    const ALIAS_SUPPLIER_NAME = 'SupplierName';

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_MATERIAL_SAMPLE_ID => true,
        self::PROPERTY_SUPPLIER_CODE_ID => true,
        self::PROPERTY_CARD_NUMBER => true,
        self::PROPERTY_SEASON_CLASSIFICATION_ID => true,
        self::PROPERTY_SUB_MATERIAL_CODE_ID => true,
        self::PROPERTY_FABRIC_CODE => true,
        self::PROPERTY_MIS_CODE => true,
        self::PROPERTY_COMPOSITION => true,
        self::PROPERTY_WASH_DATA => true,
        self::PROPERTY_WEIGHT => true,
        self::PROPERTY_HEIGHT => true,
        self::PROPERTY_COLOR => true,
        self::PROPERTY_PANTON_COLOR => true,
        self::PROPERTY_MATERIAL_PICTURE => true,
        self::PROPERTY_DATE_CREATED => true,
        self::PROPERTY_COSTS => true,
        self::PROPERTY_ADDITIONAL_EXPENSES => true,
        self::PROPERTY_INVOICE => true,
        self::PROPERTY_INVOICE_SCAN => true,
        self::PROPERTY_NOTE => true,
        self::PROPERTY_IS_EUR => true,
        self::ALIAS_MATERIAL_SAMPLE => true,
        self::ALIAS_CLASSIFICATION => true,
        self::PROPERTY_CREATED => true,
    );

    public function initialize()
    {
        parent::initialize();

        $this->addBehavior(new Timestampable([
            'beforeValidationOnCreate' => [
                'field'  => self::PROPERTY_DATE_CREATED,
                'format' => DateTime::ISO_8601_NO_TIMEZONE,
            ]
        ]));
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaterialSampleId()
    {
        return $this->materialSampleId;
    }

    /**
     * @param int $materialSampleId
     * @return $this
     */
    public function setMaterialSampleId($materialSampleId)
    {
        $this->materialSampleId = null !== $materialSampleId ? (int)$materialSampleId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getSupplierCodeId()
    {
        return $this->supplierCodeId;
    }

    /**
     * @param int $supplierCodeId
     * @return $this
     */
    public function setSupplierCodeId($supplierCodeId)
    {
        $this->supplierCodeId = null !== $supplierCodeId ? (int)$supplierCodeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param int $cardNumber
     * @return $this
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = null !== $cardNumber ? (int)$cardNumber : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getSeasonClassificationId()
    {
        return $this->seasonClassificationId;
    }

    /**
     * @param int $seasonClassificationId
     * @return $this
     */
    public function setSeasonClassificationId($seasonClassificationId)
    {
        $this->seasonClassificationId = null !== $seasonClassificationId ? (int)$seasonClassificationId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getSubMaterialCodeId()
    {
        return $this->subMaterialCodeId;
    }

    /**
     * @param int $subMaterialCodeId
     * @return $this
     */
    public function setSubMaterialCodeId($subMaterialCodeId)
    {
        $this->subMaterialCodeId = null !== $subMaterialCodeId ? (int)$subMaterialCodeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getFabricCode()
    {
        return $this->fabricCode;
    }

    /**
     * @param int $fabricCode
     * @return $this
     */
    public function setFabricCode($fabricCode)
    {
        $this->fabricCode = null !== $fabricCode ? (int)$fabricCode : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getMisCode()
    {
        return $this->misCode;
    }

    /**
     * @param int $misCode
     * @return $this
     */
    public function setMisCode($misCode)
    {
        $this->misCode = null !== $misCode ? (int)$misCode : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getComposition()
    {
        return $this->composition;
    }

    /**
     * @param string $composition
     * @return $this
     */
    public function setComposition($composition)
    {
        $this->composition = $composition;

        return $this;
    }

    /**
     * @return string
     */
    public function getWashData()
    {
        return $this->washData;
    }

    /**
     * @param string $washData
     * @return $this
     */
    public function setWashData($washData)
    {
        $this->washData = $washData;

        return $this;
    }

    /**
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param string $weight
     * @return $this
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param string $height
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return $this
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return string
     */
    public function getPantonColor()
    {
        return $this->pantonColor;
    }

    /**
     * @param string $pantonColor
     * @return $this
     */
    public function setPantonColor($pantonColor)
    {
        $this->pantonColor = $pantonColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getMaterialPicture()
    {
        return $this->materialPicture;
    }

    /**
     * @param string $materialPicture
     * @return $this
     */
    public function setMaterialPicture($materialPicture)
    {
        $this->materialPicture = $materialPicture;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param string|DateTime $dateCreated
     * @return $this
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * @return float
     */
    public function getCosts()
    {
        return $this->costs;
    }

    /**
     * @param float $costs
     * @return $this
     */
    public function setCosts($costs)
    {
        $this->costs = null !== $costs ? (float)$costs : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getAdditionalExpenses()
    {
        return $this->additionalExpenses;
    }

    /**
     * @param float $additionalExpenses
     * @return $this
     */
    public function setAdditionalExpenses($additionalExpenses)
    {
        $this->additionalExpenses = null !== $additionalExpenses ? (float)$additionalExpenses : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $invoice
     * @return $this
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceScan()
    {
        return $this->invoiceScan;
    }

    /**
     * @param string $invoiceScan
     * @return $this
     */
    public function setInvoiceScan($invoiceScan)
    {
        $this->invoiceScan = $invoiceScan;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string|DateTime $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return bool
     */
    public function getisEur()
    {
        return $this->isEur;
    }

    /**
     * @param bool $isEur
     */
    public function setIsEur($isEur)
    {
        $this->isEur = (bool)$isEur;
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return APP_PATH . 'var/uploads/files/';
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $url = ltrim($config->application->baseUri, '/');

        if (is_file($this->getUploadPath() . $this->getMaterialPicture())) {
            return $url . '/uploads/files/' . $this->getMaterialPicture();
        }

        return null;
    }

    /**
     * @return null|MaterialSample|\Vemid\Entity\EntityInterface
     * 
     * @throws \DomainException
     */
    public function getMaterialSample()
    {
        return $this->getRelated(self::ALIAS_MATERIAL_SAMPLE);
    }

    /**
     * @param MaterialSample $materialSample
     * @return $this
     */
    public function setMaterialSample(MaterialSample $materialSample)
    {
        $this->materialSampleId = $materialSample->getId();

        return $this;
    }

    /**
     * @return null|Classification|\Vemid\Entity\EntityInterface
     * 
     * @throws \DomainException
     */
    public function getClassification()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setClassification(Classification $classification)
    {
        $this->subMaterialCodeId = $classification->getId();

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|MaterialFactoryQuantity[]
     * 
     * @throws \DomainException
     */
    public function getMaterialFactoryQuantities($arguments = null)
    {
        return $this->getRelated(self::ALIAS_MATERIAL_FACTORY_QUANTITIES, $arguments);
    }

    /**
     * @param null $arguments
     * @return null|Resultset|\Vemid\Entity\EntityInterface
     *
     * @throws DomainException
     */
    public function  getProductCalculationItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRODUCT_CALCULATION_ITEMS,$arguments);
    }

    /**
     * @return int|string
     */
    public function getDisplayName()
    {
        if ($classification = $this->getSubMaterialClassification()) {
            return sprintf('%s (%s %s %s) %s',
                (string)$this->getCardNumber(),
                $classification->getName(),
                $this->getColor(),
                $this->getHeight(),
                $this->getSupplierName()
            );
        }

        return (string)$this->getCardNumber();
    }

    /**
     * @return null|Classification|\Vemid\Entity\EntityInterface
     *
     * @throws DomainException
     */
    public function getSubMaterialClassification()
    {
        return $this->getRelated(self::ALIAS_SUB_MATERIAL_CLASSIFICATION);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setSubMaterialClassification(Classification $classification)
    {
        $this->subMaterialCodeId = $classification->getId();

        return $this;
    }

    /**
     * @return null|Supplier|\Vemid\Entity\EntityInterface
     *
     * @throws DomainException
     */
    public function getSupplierName()
    {
        return $this->getRelated(self::ALIAS_SUPPLIER_NAME);
    }

    /**
     * @param Supplier $supplier
     * @return $this
     */
    public function setSupplierName(Supplier $supplier)
    {
        $this->supplierCodeId = $supplier->getId();

        return $this;
    }

    /**
     * @return null|Classification|\Vemid\Entity\EntityInterface
     *
     * @throws DomainException
     */
    public function getSeasonClassification()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION_ID);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setSeassonClassification(Classification $classification)
    {
        $this->seasonClassificationId = $classification->getId();

        return $this;
    }

    public function beforeDelete()
    {
        $this->getMaterialFactoryQuantities()->delete();
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
