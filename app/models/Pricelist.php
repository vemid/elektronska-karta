<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * Pricelist Model Class
 *
 * @Source('pricelists')
 *
 * @HasMany('id', 'ProductPricelist', 'pricelistId', {'alias':'ProductPricelists'})
 *
 */
class Pricelist extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CODE = 'code';
    const PROPERTY_NAME = 'name';
    const PROPERTY_CURRENCY = 'currency';

    const ALIAS_PRODUCT_PRICELISTS = 'ProductPricelists';


    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="code", type="string", nullable=false)
     * @FormElement(label="Code", type="Text", required=true)
     */
    protected $code;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="currency", type="string", nullable=false)
     * @FormElement(label="Currency", type="Text", required=true)
     */
    protected $currency;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CODE => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_CURRENCY => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @param array|null $arguments
     * @return ProductPrice[]|Resultset
     *
     * @throws DomainException
     */
    public function getProductPricelists($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRODUCT_PRICELISTS, $arguments);
    }

}
