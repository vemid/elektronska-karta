<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * ElectronCard Model Class
 *
 * @Source('electron_cards')
 *
 * @BelongsTo('productId', 'Product', 'id', {'alias':'Product'})
 * @BelongsTo('baseProductId', 'Product', 'id', {'alias':'BaseProduct'})
 *
 * @HasMany('id', 'ElectronCardFactory', 'electronCardId', {'alias':'ElectronCardFactories'})
 */
class ElectronCard extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PRODUCT_ID = 'productId';
    const PROPERTY_BASE_PRODUCT_ID = 'baseProductId';
    const PROPERTY_BASE_NAME = 'baseName';
    const PROPERTY_IMAGE = 'image';
    const PROPERTY_ADDITIONAL_SKETCH_IMAGE = 'additionalSketchImage';
    const PROPERTY_ADDITIONAL_SKETCH_SECOND_IMAGE = 'additionalSketchSecondImage';
    const PROPERTY_ADDITIONAL_SKETCH_DESCRIPTION = 'additionalSketchDescription';
    const PROPERTY_ADDITIONAL_SKETCH_SECOND_DESCRIPTION = 'additionalSketchSecondDescription';
    const PROPERTY_PRE_PRESS_IMAGE = 'prePressImage';
    const PROPERTY_NOTE = 'note';
    const PROPERTY_ACTIVE = 'active';
    const PROPERTY_FINISHED = 'finished';
    const PROPERTY_VERSION = 'version';
    const PROPERTY_CARD_CLOSED_TIME = 'cardClosedTime';
    const PROPERTY_CREATED = 'created';

    const ALIAS_PRODUCT = 'Product';
    const ALIAS_BASE_PRODUCT = 'BaseProduct';
    const ALIAS_ELECTRON_CARD_FACTORIES = 'ElectronCardFactories';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="product_id", type="integer", nullable=false)
     * @FormElement(label="Product", type="Hidden", required=true)
     */
    protected $productId;

    /**
     * @Column(column="base_product_id", type="integer", nullable=true)
     * @FormElement(label="Base Product", type="Hidden", required=false)
     */
    protected $baseProductId;

    /**
     * @Column(column="image", type="string", nullable=false)
     * @FormElement(label="Slika", type="File", required=true)
     */
    protected $image;

    /**
     * @Column(column="additional_sketch_image", type="string", nullable=true)
     * @FormElement(label="Dodatna skica", type="File", required=false)
     */
    protected $additionalSketchImage;

    /**
     * @Column(column="additional_sketch_second_image", type="string", nullable=true)
     * @FormElement(label="Dodatna skica 2", type="File", required=false)
     */
    protected $additionalSecondSketchImage;

    /**
     * @Column(column="additional_sketch_description", type="string", nullable=true)
     * @FormElement(label="Opis Dodatne skice", type="TextArea", required=false)
     */
    protected $additionalSketchDescription;

    /**
     * @Column(column="additional_sketch_second_description", type="string", nullable=true)
     * @FormElement(label="Opis Dodatne skice 2", type="TextArea", required=false)
     */
    protected $additionalSketchSecondDescription;

    /**
     * @Column(column="prepress_image", type="string", nullable=true)
     */
    protected $prePressImage;

    /**
     * @Column(column="base_name", type="string", nullable=true)
     * @FormElement(label="Osnova", type="Text", required=false)
     */
    protected $baseName;

    /**
     * @Column(column="note", type="string", nullable=true)
     * @FormElement(label="Note", type="TextArea", required=false)
     */
    protected $note;

    /**
     * @Column(column="active", type="bool", nullable=true)
     */
    protected $active;

    /**
     * @Column(column="finished", type="bool", nullable=true)
     */
    protected $finished;

    /**
     * @Column(column="version", type="integer", nullable=false)
     */
    protected $version;

    /**
     * @Column(column="card_closed_time", type="dateTime", nullable=true)
     */
    protected $cardClosedTime;

    /**
     * @Column(column="created", type="dateTime", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRODUCT_ID => true,
        self::PROPERTY_IMAGE => true,
        self::PROPERTY_PRE_PRESS_IMAGE => true,
        self::PROPERTY_ACTIVE => true,
        self::PROPERTY_FINISHED => true,
        self::PROPERTY_VERSION => true,
        self::ALIAS_PRODUCT => true,
        self::PROPERTY_CARD_CLOSED_TIME =>true,
        self::PROPERTY_CREATED => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->productId = null !== $productId ? (int)$productId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionalSketchImage()
    {
        return $this->additionalSketchImage;
    }

    /**
     * @param string $additionalSketchImage
     * @return $this
     */
    public function setAdditionalSketchImage($additionalSketchImage)
    {
        $this->additionalSketchImage = $additionalSketchImage;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionalSecondSketchImage()
    {
        return $this->additionalSecondSketchImage;
    }

    /**
     * @param string $additionalSecondSketchImage
     * @return $this
     */
    public function setAdditionalSecondSketchImage($additionalSecondSketchImage)
    {
        $this->additionalSecondSketchImage = $additionalSecondSketchImage;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionalSketchDescription()
    {
        return $this->additionalSketchDescription;
    }

    /**
     * @param string $additionalSketchDescription
     * @return $this
     */
    public function setAdditionalSketchDescription($additionalSketchDescription)
    {
        $this->additionalSketchDescription = $additionalSketchDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionalSecondSketchDescription()
    {
        return $this->additionalSketchSecondDescription;
    }

    /**
     * @param string $additionalSecondSketchDescription
     * @return $this
     */
    public function setAdditionalSecondSketchDescription($additionalSecondSketchDescription)
    {
        $this->additionalSketchSecondDescription = $additionalSecondSketchDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrePressImage()
    {
        return $this->prePressImage;
    }

    /**
     * @param string $prePressImage
     * @return $this
     */
    public function setPrePressImage($prePressImage)
    {
        $this->prePressImage = $prePressImage;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getFinished()
    {
        return $this->finished;
    }

    /**
     * @param boolean $finished
     * @return $this
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return integer
     */
    public function getBaseProductId()
    {
        return $this->baseProductId;
    }

    /**
     * @param integer $baseProductId
     * @return $this
     */
    public function setBaseProductId($baseProductId)
    {
        $this->baseProductId = $baseProductId;

        return $this;
    }

    /**
     * @return string
     */
    public function getBaseName()
    {
        return $this->baseName;
    }

    /**
     * @param string $baseName
     * @return $this
     */
    public function setBaseName($baseName)
    {
        $this->baseName = $baseName;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param integer $version
     * @return $this
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }


    /**
     * @return null|DateTime
     */
    public function getCardClosedTime()
    {
        return $this->cardClosedTime;
    }

    /**
     * @param string|DateTime $cardClosedTime
     * @return $this
     */
    public function setCardClosedTime($cardClosedTime)
    {
        $this->cardClosedTime = $cardClosedTime;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string|DateTime $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|Product|\Vemid\Entity\EntityInterface
     * 
     * @throws \DomainException
     */
    public function getProduct()
    {
        return $this->getRelated(self::ALIAS_PRODUCT);
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->productId = $product->getId();

        return $this;
    }

    /**
     * @return null|Product|\Vemid\Entity\EntityInterface
     *
     * @throws \DomainException
     */
    public function getBaseProduct()
    {
        return $this->getRelated(self::ALIAS_BASE_PRODUCT);
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setBasaProduct(Product $product)
    {
        $this->baseProductId = $product->getId();

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|ElectronCardFactory[]
     * 
     * @throws \DomainException
     */
    public function getElectronCardFactories($arguments = null)
    {
        return $this->getRelated(self::ALIAS_ELECTRON_CARD_FACTORIES, $arguments);
    }

    public function getElectronCardFactoriesWithType($type)
    {
        return $this->getElectronCardFactories([
            ElectronCardFactory::PROPERTY_TYPE . ' = :type:',
            'bind' => [
                'type' => $type
            ]
        ]);
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return APP_PATH . 'var/uploads/files/';
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $url = ltrim($config->application->baseUri, '/');

        if (is_file($this->getUploadPath() . $this->getImage())) {
            return $url . '/uploads/files/' . $this->getImage();
        }

        return null;
    }

    /**
     * @return string
     */
    public function getImagePrintPath()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $url = ltrim($config->application->baseUriImage, '/');

        if (is_file($this->getUploadPath() . $this->getImage())) {
            return $url . '/uploads/files/' . $this->getImage();
        }

        return null;
    }

    /**
     * @return string
     */
    public function getAdditionalSketchImagePath()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $url = ltrim($config->application->baseUri, '/');

        if (is_file($this->getUploadPath() . $this->getAdditionalSketchImage())) {
            return $url . '/uploads/files/' . $this->getAdditionalSketchImage();
        }

        return null;
    }

    /**
     * @return string
     */
    public function getAdditionalSecondSketchImagePath()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $url = ltrim($config->application->baseUri, '/');

        if (is_file($this->getUploadPath() . $this->getAdditionalSecondSketchImage())) {
            return $url . '/uploads/files/' . $this->getAdditionalSecondSketchImage();
        }

        return null;
    }

    /**
     * @return string
     */
    public function getPrePressImagePath()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $url = ltrim($config->application->baseUri, '/');

        if (is_file($this->getUploadPath() . $this->getPrePressImage())) {
            return $url . '/uploads/files/' . $this->getPrePressImage();
        }

        return null;
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
