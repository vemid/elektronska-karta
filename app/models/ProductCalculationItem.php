<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * ProductCalculationItem Model Class
 *
 * @Source('product_calculation_items')
 *
 * @BelongsTo('productCalculationId', 'ProductCalculation', 'id', {'alias':'ProductCalculation'})
 * @BelongsTo('materialFactoryId', 'MaterialFactory', 'id', {'alias':'MaterialFactory'})
 */
class ProductCalculationItem extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PRODUCT_CALCULATION_ID = 'productCalculationId';
    const PROPERTY_MATERIAL_FACTORY_ID = 'materialFactoryId';
    const PROPERTY_NAME = 'name';
    const PROPERTY_CONSUMPTION = 'consumption';
    const PROPERTY_TOTAL = 'total';
    const PROPERTY_IS_SYNCED = 'isSynced';

    const ALIAS_PRODUCT_CALCULATION = 'ProductCalculation';
    const ALIAS_MATERIAL_FACTORY = 'MaterialFactory';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="product_calculation_id", type="integer", nullable=false)
     * @FormElement(label="Product calculation", type="Select", required=true, relation="ProductCalculation")
     */
    protected $productCalculationId;

    /**
     * @Column(column="material_factory_id", type="integer", nullable=true)
     * @FormElement(label="Material factory", type="Select", required=false, relation="MaterialFactory")
     */
    protected $materialFactoryId;

    /**
     * @Column(column="name", type="string", nullable=true)
     * @FormElement(label="Name", type="Text", required=false)
     */
    protected $name;

    /**
     * @Column(column="consumption", type="decimal", nullable=false)
     * @FormElement(label="Consumption", type="Text", required=true)
     */
    protected $consumption;

    /**
     * @Column(column="total", type="decimal", nullable=false)
     * @FormElement(label="Total", type="Text", required=true)
     */
    protected $total;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRODUCT_CALCULATION_ID => true,
        self::PROPERTY_MATERIAL_FACTORY_ID => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_CONSUMPTION => true,
        self::PROPERTY_TOTAL => true,
        self::ALIAS_PRODUCT_CALCULATION => true,
        self::ALIAS_MATERIAL_FACTORY => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductCalculationId()
    {
        return $this->productCalculationId;
    }

    /**
     * @param int $productCalculationId
     * @return $this
     */
    public function setProductCalculationId($productCalculationId)
    {
        $this->productCalculationId = null !== $productCalculationId ? (int)$productCalculationId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaterialFactoryId()
    {
        return $this->materialFactoryId;
    }

    /**
     * @param int $materialFactoryId
     * @return $this
     */
    public function setMaterialFactoryId($materialFactoryId)
    {
        $this->materialFactoryId = null !== $materialFactoryId ? (int)$materialFactoryId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return float
     */
    public function getConsumption()
    {
        return $this->consumption;
    }

    /**
     * @param float $consumption
     * @return $this
     */
    public function setConsumption($consumption)
    {
        $this->consumption = null !== $consumption ? (float)$consumption : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     * @return $this
     */
    public function setTotal($total)
    {
        $this->total = null !== $total ? (float)$total : null;

        return $this;
    }

    /**
     * @return null|ProductCalculation
     * 
     * @throws \DomainException
     */
    public function getProductCalculation()
    {
        return $this->getRelated(self::ALIAS_PRODUCT_CALCULATION);
    }

    /**
     * @param ProductCalculation $productCalculation
     * @return $this
     */
    public function     setProductCalculation(ProductCalculation $productCalculation)
    {
        $this->productCalculationId = $productCalculation->getId();

        return $this;
    }

    /**
     * @return null|MaterialFactory
     * 
     * @throws \DomainException
     */
    public function getMaterialFactory()
    {
        return $this->getRelated(self::ALIAS_MATERIAL_FACTORY);
    }

    /**
     * @param MaterialFactory $materialFactory
     * @return $this
     */
    public function setMaterialFactory(MaterialFactory $materialFactory)
    {
        $this->materialFactoryId = $materialFactory->getId();

        return $this;
    }

    public function beforeSave()
    {
        if ($materialFactory = $this->getMaterialFactory()) {
            $this->name = $materialFactory->getDisplayName();
        }
    }

    public function getCosts()
    {
        if ($materialFactory = $this->getMaterialFactory()) {
            return round($materialFactory->getCosts(),3);
        }

        return round($this->consumption ? ($this->total / $this->consumption) : $this->consumption,3);
    }
}
