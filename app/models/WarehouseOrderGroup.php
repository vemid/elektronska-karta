<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * WarehouseOrderGroup Model Class
 *
 * @Source('warehouse_order_groups')
 * @BelongsTo('channelSupplyEntityId', 'ChannelSupplyEntity', 'id', {'alias': 'ChannelSupplyEntity'})
 * @HasMany('id', 'WarehouseOrderGroupItem', 'warehouseOrderGroupId', {'alias':'WarehouseOrderGroupItems'})
 */
class WarehouseOrderGroup extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_WAREHOUSE_GROUP_ORDER_CODE = 'warehouseOrderGroupCode';
    const PROPERTY_CHANNEL_SUPPLY_ENTITY_ID = 'channelSupplyEntityId';
    const PROPERTY_PUSHED_TO_MIS = 'pushedToMis';
    const PROPERTY_GROUPED= 'grouped';
    const PROPERTY_STATUS = 'status';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_NOTE = 'note';
    const PROPERTY_LOCKED = 'locked';
    const PROPERTY_CREATED = 'created';

    const PROPERTY_SYNC_STATUS = 'syncStatus';

    public const LOCKED = 'LOCKED';
    public const SENT = 'SENT';
    public const EDITABLE = 'EDITABLE';

    public const GROUPED = 'GROUPED';
    public const NON_GROUPED = 'NON_GROUPED';

    public const A = 'A';
    public const B = 'B';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="channel_supply_entity_id", type="integer", nullable=false)
     */
    protected $channelSupplyEntityId;

    /**
     * @Column(column="warehouse_order_group_code", type="string", nullable=false)
     */
    protected $warehouseOrderGroupCode;

    /**
     * @Column(column="pushed_to_mis", type="string", nullable=true)
     */
    protected $pushedToMis;

    /**
     * @Column(column="locked", type="bool", nullable=true)
     */
    protected $locked;

    /**
     * @Column(column="grouped", type="string", nullable=true)
     */
    protected $grouped;

    /**
     * @Column(column="type", type="string", nullable=true)
     */
    protected $type;


    /**
     * @Column(column="status", type="string", nullable=false)
     */
    protected $status;

    /**
     * @Column(column="note", type="string", nullable=true)
     * @FormElement(label="Note", type="Text", required=false)
     */
    protected $note;

    /**
     * @Column(column="created", type="dateTime", nullable=true)
     */
    protected $created;

    /**
     * @Column(column="sync_status", type="string", nullable=true)
     */
    protected $syncStatus;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_WAREHOUSE_GROUP_ORDER_CODE => true,
        self::PROPERTY_PUSHED_TO_MIS => true,
        self::PROPERTY_STATUS => true,
        self::PROPERTY_NOTE => true,
        self::PROPERTY_LOCKED => true,
        self::PROPERTY_GROUPED => true,
        self::PROPERTY_TYPE => true,
        self::PROPERTY_CREATED => true,
        self::PROPERTY_SYNC_STATUS => true,
    );

    const ALIAS_WAREHOUSE_ORDER_GROUP_ITEMS = 'WarehouseOrderGroupItems';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getChannelSupplyEntityId()
    {
        return $this->channelSupplyEntityId;
    }

    /**
     * @param int $channelSupplyEntityId
     * @return $this
     */
    public function setChannelSupplyEntityId($channelSupplyEntityId)
    {
        $this->channelSupplyEntityId = null !== $channelSupplyEntityId ? (int)$channelSupplyEntityId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getWarehouseOrderGroupCode()
    {
        return $this->warehouseOrderGroupCode;
    }

    /**
     * @param string $warehouseOrderGroupCode
     * @return $this
     */
    public function setWarehouseOrderGroupCode($warehouseOrderGroupCode)
    {
        $this->warehouseOrderGroupCode = $warehouseOrderGroupCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPushedToMis()
    {
        return $this->pushedToMis;
    }

    /**K
     * @param mixed $pushedToMis
     */
    public function setPushedToMis($pushedToMis)
    {
        $this->pushedToMis = $pushedToMis;

        return$this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * @param mixed $locked
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return$this;
    }

    /**
     * @return mixed
     */
    public function getGrouped()
    {
        return $this->grouped;
    }

    /**K
     * @param mixed $grouped
     */
    public function setGrouped($grouped)
    {
        $this->grouped = $grouped;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**K
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getSyncStatus()
    {
        return $this->syncStatus;
    }

    /**
     * @param string $syncStatus
     * @return $this
     */
    public function setSyncStatus($syncStatus)
    {
        $this->syncStatus = $syncStatus;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string|DateTime $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|ChannelSupplyEntity
     *
     * @throws \DomainException
     */
    public function getChannelSupplyEntity()
    {
        return $this->getRelated(ChannelSupplyEntity::class);
    }

    /**
     * @param ChannelSupplyEntity $channelSupplyEntity
     * @return $this
     */
    public function setChannelSupplyEntity(ChannelSupplyEntity $channelSupplyEntity)
    {
        $this->channelSupplyEntityId = $channelSupplyEntity->getId();

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|WarehouseOrderGroupItem[]
     *
     * @throws \DomainException
     */
    public function getWarehouseOrderGroupItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_WAREHOUSE_ORDER_GROUP_ITEMS, $arguments);
    }

    /**
     * @return bool
     */
    public function isEditable()
    {
        return $this->status === self::EDITABLE;
    }

    public function beforeValidationOnCreate()
    {
        $this->pushedToMis = 0;
        $this->locked = 0;
        $this->created = new DateTime();
        $this->status = self::EDITABLE;
        $channelSupplyEntity = $this->getChannelSupplyEntity();
        $supplyChannel = $channelSupplyEntity->getChannelSupply();

        $code = "00";
        $id = 0;
        if ($supplyChannel->getType() === \ChannelSupply::MP) {
            $code = $channelSupplyEntity->getEntity()->getCode();
            $lastWarehouseOrder = $this->getDI()->getEntityManager()->find(self::class, [
                WarehouseOrder::PROPERTY_CHANNEL_SUPPLY_ENTITY_ID . ' = :entityId:',
                'bind' => [
                    'entityId' => $this->getChannelSupplyEntity()->getId()
                ]
            ]);
            $id = count($lastWarehouseOrder->toArray());

        } else {
            if ($lastWarehouseOrder = $this->getDI()->getEntityManager()->find(self::class)) {
                $id = count($lastWarehouseOrder->toArray());
            }
        }


        $id++;

        $this->warehouseOrderGroupCode = sprintf('%s%s%d00', "G".$code, str_repeat("0", 8 - strlen($id)), $id);
    }

    public function beforeUpdate()
    {
        if ($this->hasChanged(self::PROPERTY_STATUS) && $this->getStatus() === self::EDITABLE) {
            $controlNumber = (int)substr($this->getWarehouseOrderGroupCode(), -2);
            $code = substr($this->getWarehouseOrderGroupCode(), 0, -2);

            $controlNumber++;
            $this->setWarehouseOrderGroupCode(sprintf('%s%02d', $code, $controlNumber));
        }
    }

}
