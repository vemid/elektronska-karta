<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * ProductionMachine Model Class
 *
 * @Source('production_machines')
 * @HasMany('id', 'OperatingListItemType', 'productionMachineId', {'alias':'OperatingListItemType'})
 */
class ProductionMachine extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_NAME = 'name';
    const PROPERTY_SHORT_NAME = 'shortName';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_CREATED = 'created';

    const TYPE_HAND = 'HAND';
    const TYPE_MACHINE = 'MACHINE';

    const ALIAS_OPERATING_LIST_ITEM_TYPE = 'OperatingListItemType';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="short_name", type="string", nullable=true)
     * @FormElement(label="Short name", type="Text", required=false)
     */
    protected $shortName;

    /**
     * @Column(column="type", type="string", nullable=true)
     * @FormElement(label="Tip Masine", type="Select", required=true, options={'HAND':'Rucni Rad', 'MACHINE':'Masina'})

     */
    protected $type;

    /**
     * @Column(column="created", type="string", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_SHORT_NAME => true,
        self::PROPERTY_TYPE => true,
        self::PROPERTY_CREATED => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @param string $shortName
     * @return $this
     */
    public function setShortName($shortName)
    {
        $this->shortName = strtoupper($shortName);

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|OperatingListItemType[]
     * 
     * @throws \DomainException
     */
    public function getOperatingListItemTypes($arguments = null)
    {
        return $this->getRelated(self::ALIAS_OPERATING_LIST_ITEM_TYPE, $arguments);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return trim($this->name) . ' - ' . trim($this->shortName);
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
