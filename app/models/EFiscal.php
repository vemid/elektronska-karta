<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * EFiscal Model Class
 *
 * @Source('e_fiscals')
 * @HasMany('id', 'EFiscalItem', 'eFiscalId', {'alias':'EFiscalItems'})
 * @BelongsTo('codeId', 'Code', 'id', {'alias':'Code'})
 */
class EFiscal extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CODE_ID = 'codeId';
    const PROPERTY_CASHIER = 'cashier';
    const PROPERTY_INVOICE_NUMBER = 'invoiceNumber';
    const PROPERTY_TOTAL_AMOUNT = 'totalAmount';
    const PROPERTY_TAX_AMOUNT = 'taxAmount';
    const PROPERTY_TRANSACTION_TYPE = 'transactionType';
    const PROPERTY_INVOICE_TYPE = 'invoiceType';
    const PROPERTY_FISCAL_TIME = 'ficalTime';

    const ALIAS_E_FISCAL_ITEMS = 'EFiscalItems';
    const ALIAS_CODE = 'Code';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="code_id", type="integer", nullable=false)
     * @FormElement(label="Code", type="Text", required=true)
     */
    protected $codeId;

    /**
     * @Column(column="cashier", type="string", nullable=true)
     * @FormElement(label="Cashier", type="Text", required=false)
     */
    protected $cashier;

    /**
     * @Column(column="invoice_number", type="string", nullable=true)
     * @FormElement(label="Invoice number", type="Text", required=false)
     */
    protected $invoiceNumber;

    /**
     * @Column(column="total_amount", type="decimal", nullable=false)
     * @FormElement(label="Total amount", type="Text", required=true)
     */
    protected $totalAmount;

    /**
     * @Column(column="tax_amount", type="decimal", nullable=false)
     * @FormElement(label="Tax amount", type="Text", required=true)
     */
    protected $taxAmount;

    /**
     * @Column(column="transaction_type", type="string", nullable=true)
     * @FormElement(label="Transaction type", type="Text", required=false)
     */
    protected $transactionType;

    /**
     * @Column(column="invoice_type", type="string", nullable=true)
     * @FormElement(label="Invoice type", type="Text", required=false)
     */
    protected $invoiceType;

    /**
     * @Column(column="fiscal_time", type="dateTime", nullable=true)
     */
    protected $fiscalTime;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CODE_ID => true,
        self::PROPERTY_CASHIER => true,
        self::PROPERTY_INVOICE_NUMBER => true,
        self::PROPERTY_TOTAL_AMOUNT => true,
        self::PROPERTY_TAX_AMOUNT => true,
        self::PROPERTY_TRANSACTION_TYPE => true,
        self::PROPERTY_INVOICE_TYPE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * @param int $codeId
     * @return $this
     */
    public function setCodeId($codeId)
    {
        $this->codeId = null !== $codeId ? (int)$codeId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getCashier()
    {
        return $this->cashier;
    }

    /**
     * @param string $cashier
     * @return $this
     */
    public function setCashier($cashier)
    {
        $this->cashier = $cashier;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * @param string $invoiceNumber
     * @return $this
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param float $totalAmount
     * @return $this
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = null !== $totalAmount ? (float)$totalAmount : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getTaxAmount()
    {
        return $this->taxAmount;
    }

    /**
     * @param float $taxAmount
     * @return $this
     */
    public function setTaxAmount($taxAmount)
    {
        $this->taxAmount = null !== $taxAmount ? (float)$taxAmount : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     * @param string $transactionType
     * @return $this
     */
    public function setTransactionType($transactionType)
    {
        $this->transactionType = $transactionType;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceType()
    {
        return $this->invoiceType;
    }

    /**
     * @param string $invoiceType
     * @return $this
     */
    public function setInvoiceType($invoiceType)
    {
        $this->invoiceType = $invoiceType;

        return $this;
    }

    /**
     * @return null|Code
     *
     * @throws \DomainException
     */
    public function getCode()
    {
        return $this->getRelated(self::ALIAS_CODE);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setCode(Code $code)
    {
        $this->codeId = $code->getId();

        return $this;
    }

    /**
     * @return null|\Vemid\Date\DateTime
     */
    public function getFiscalTime()
    {
        return $this->fiscalTime;
    }

    /**
     * @param string|DateTime $fiscalTime
     * @return $this
     */
    public function setFiscalTime($fiscalTime)
    {
        $this->fiscalTime = $fiscalTime;

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|EFiscalItem[]
     * 
     * @throws \DomainException
     */
    public function getEFiscalItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_E_FISCAL_ITEMS, $arguments);
    }

}
