<?php

use Phalcon\Mvc\Model\Resultset;
use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;

/**
 * ProductionWorkerCheckin Model Class
 *
 * @Source('production_worker_checkins')
 *
 * @BelongsTo('workerId', 'ProductionWorker', 'id', {'alias':'ProductionWorker'})
 */
class ProductionWorkerCheckin extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_WORKER_ID = 'workerId';
    const PROPERTY_DATE = 'date';
    const PROPERTY_CHECK_IN = 'checkIn';
    const PROPERTY_CHECK_OUT = 'checkOut';
    const PROPERTY_CREATED = 'created';

    const ALIAS_PRODUCTION_WORKER = 'ProductionWorker';


    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="worker_id", type="integer", nullable=false)
     * @FormElement(label="Worker", type="Text", required=true)
     */
    protected $workerId;

    /**
     * @Column(column="date", type="date", nullable=false)
     * @FormElement(label="Date", type="Date", required=true)
     */
    protected $date;

    /**
     * @Column(column="check_in", type="string", nullable=false)
     * @FormElement(label="Check in", type="Text", required=true)
     */
    protected $checkIn;

    /**
     * @Column(column="check_out", type="string", nullable=false)
     * @FormElement(label="Check out", type="Text", required=true)
     */
    protected $checkOut;

    /**
     * @Column(column="created", type="string", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_WORKER_ID => true,
        self::PROPERTY_DATE => true,
        self::PROPERTY_CHECK_IN => true,
        self::PROPERTY_CHECK_OUT => true,
        self::PROPERTY_CREATED => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getWorkerId()
    {
        return $this->workerId;
    }

    /**
     * @param int $workerId
     * @return $this
     */
    public function setWorkerId($workerId)
    {
        $this->workerId = null !== $workerId ? (int)$workerId : null;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string|DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getCheckIn()
    {
        return $this->checkIn;
    }

    /**
     * @param string $checkIn
     * @return $this
     */
    public function setCheckIn($checkIn)
    {
        $this->checkIn = $checkIn;

        return $this;
    }

    /**
     * @return string
     */
    public function getCheckOut()
    {
        return $this->checkOut;
    }

    /**
     * @param string $checkOut
     * @return $this
     */
    public function setCheckOut($checkOut)
    {
        $this->checkOut = $checkOut;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|ProductionWorker
     *
     * @throws \DomainException
     */
    public function getWorker()
    {
        return $this->getRelated(self::ALIAS_PRODUCTION_WORKER);
    }

    /**
     * @param ProductionWorker $productionWorker
     * @return $this
     */
    public function setWorker(ProductionWorker $productionWorker)
    {
        $this->workerId = $productionWorker->getId();

        return $this;
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
