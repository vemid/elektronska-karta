<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;
use Vemid\Task\Orders\AddOrderTotals;

/**
 * Order Model Class
 *
 * @Source('orders')
 *
 * @HasMany('id', 'OrderItem', 'orderId', {'alias':'OrderItems'})
 * @HasMany('id', 'OrderTotal', 'orderId', {'alias':'OrderTotals'})
 * @HasMany('id', 'OrderClassification', 'orderId', {'alias':'OrderClassifications'})
 *
 * @BelongsTo('userId', 'User', 'id', {'alias':'User'})
 * @BelongsTo('yearAttributeId', 'Attribute', 'id', {'alias':'YearAttribute'})
 * @BelongsTo('seasonAttributeId', 'Attribute', 'id', {'alias':'SeasonAttribute'})
 */
class Order extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_USER_ID = 'userId';
    const PROPERTY_NAME = 'name';
    const PROPERTY_DESCRIPTION = 'description';
    const PROPERTY_ACTIVE = 'active';
    const PROPERTY_START_DATE = 'startDate';
    const PROPERTY_END_DATE = 'endDate';

    const ALIAS_USER = 'User';
    const ALIAS_YEAR_ATTRIBUTE = 'YearAttribute';
    const ALIAS_SEASON_ATTRIBUTE = 'SeasonAttribute';
    const ALIAS_ORDER_ITEMS = 'OrderItems';
    const ALIAS_ORDER_TOTALS = 'OrderTotals';
    const ALIAS_ORDER_CLASSIFICATIONS = 'OrderClassifications';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="year_attribute_id", type="integer", nullable=false)
     */
    protected $yearAttributeId;

    /**
     * @Column(column="user_id", type="integer", nullable=false)
     */
    protected $userId;

    /**
     * @Column(column="season_attribute_id", type="integer", nullable=false)
     */
    protected $seasonAttributeId;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="description", type="string", nullable=false)
     * @FormElement(label="Description", type="TextArea", required=true)
     */
    protected $description;

    /**
     * @Column(column="active", type="integer", nullable=false)
     */
    protected $active;

    /**
     * @Column(column="start_date", type="date", nullable=true)
     * @FormElement(label="Start date", type="Date", required=false)
     */
    protected $startDate;

    /**
     * @Column(column="end_date", type="date", nullable=true)
     * @FormElement(label="End date", type="Date", required=false)
     */
    protected $endDate;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_USER_ID => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_DESCRIPTION => true,
        self::PROPERTY_ACTIVE => true,
        self::PROPERTY_START_DATE => true,
        self::PROPERTY_END_DATE => true,
        self::ALIAS_USER => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = null !== $userId ? (int)$userId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getYearAttributeId()
    {
        return $this->yearAttributeId;
    }

    /**
     * @param int $yearAttributeId
     * @return $this
     */
    public function setYearAttributeId($yearAttributeId)
    {
        $this->yearAttributeId = null !== $yearAttributeId ? (int)$yearAttributeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getSeasonAttributeId()
    {
        return $this->seasonAttributeId;
    }

    /**
     * @param int $seasonAttributeId
     * @return $this
     */
    public function setSeasonAttributeId($seasonAttributeId)
    {
        $this->seasonAttributeId = null !== $seasonAttributeId ? (int)$seasonAttributeId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        return (bool)$this->active;
    }

    /**
     * @param int|bool $active
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active ? 1 : 0;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param string|DateTime $startDate
     * @return $this
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param string|DateTime $endDate
     * @return $this
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return null|User
     * 
     * @throws \DomainException
     */
    public function getUser()
    {
        return $this->getRelated(self::ALIAS_USER);
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->userId = $user->getId();

        return $this;
    }

    /**
     * @return null|Attribute
     *
     * @throws \DomainException
     */
    public function getYearAttribute()
    {
        return $this->getRelated(self::ALIAS_YEAR_ATTRIBUTE);
    }

    /**
     * @param Attribute $yearAttribute
     * @return $this
     */
    public function setYearAttribute(Attribute $yearAttribute)
    {
        $this->yearAttributeId = $yearAttribute->getId();

        return $this;
    }

    /**
     * @return null|Attribute
     *
     * @throws \DomainException
     */
    public function getSeasonAttribute()
    {
        return $this->getRelated(self::ALIAS_SEASON_ATTRIBUTE);
    }

    /**
     * @param Attribute $seasonAttribute
     * @return $this
     */
    public function setSeasonAttribute(Attribute $seasonAttribute)
    {
        $this->seasonAttributeId = $seasonAttribute->getId();

        return $this;
    }

    /**
     * @return \Vemid\Date\DateRange
     */
    public function getDateRange()
    {
        return new \Vemid\Date\DateRange($this->startDate, $this->endDate);
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->name;
    }

    /**
     * @param null $arguments
     * @return null|Resultset|\Vemid\Entity\EntityInterface|OrderTotal[]
     */
    public function getOrderTotals($arguments = null)
    {
        return $this->getRelated(self::ALIAS_ORDER_TOTALS, $arguments);
    }

    /**
     * @param null $arguments
     * @return null|Resultset|\Vemid\Entity\EntityInterface|OrderItem[]
     */
    public function getOrderItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_ORDER_ITEMS, $arguments);
    }

    /**
     * @param null $arguments
     * @return Resultset|\Vemid\Entity\EntityInterface[]|OrderClassification[]
     */
    public function getOrderClassifications($arguments = null)
    {
        return $this->getRelated(self::ALIAS_ORDER_CLASSIFICATIONS, $arguments);
    }

    public function afterUpdate()
    {
        if ($this->active === 0) {
            $orderTotalTask = new AddOrderTotals();
            $orderTotalTask->order = $this;
            $orderTotalTask->runInBackground();
        }
    }
}
