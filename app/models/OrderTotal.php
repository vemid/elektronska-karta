<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * OrderTotal Model Class
 *
 * @Source('order_totals')
 *
 * @BelongsTo('orderId', 'Order', 'id', {'alias':'Order'})
 * @BelongsTo('productSizeId', 'ProductSize', 'id', {'alias':'ProductSize'})
 */
class OrderTotal extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_ORDER_ID = 'orderId';
    const PROPERTY_PRODUCT_SIZE_ID = 'productSizeId';
    const PROPERTY_QUANTITY = 'quantity';
    const PROPERTY_FINAL_QTY = 'finalQty';

    const ALIAS_ORDER = 'Order';
    const ALIAS_PRODUCT_SIZE = 'ProductSize';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="order_id", type="integer", nullable=false)
     * @FormElement(label="Order", type="Select", required=true, relation="Order")
     */
    protected $orderId;

    /**
     * @Column(column="product_size_id", type="integer", nullable=false)
     * @FormElement(label="Product size", type="Select", required=true, relation="ProductSize")
     */
    protected $productSizeId;

    /**
     * @Column(column="quantity", type="integer", nullable=false)
     * @FormElement(label="Quantity", type="Text", required=true)
     */
    protected $quantity;

    /**
     * @Column(column="final_qty", type="integer", nullable=false)
     * @FormElement(label="Final qty", type="Text", required=true)
     */
    protected $finalQty;

    /**
     * @Column(column="created", type="dateTime", nullable=true)
     */
    protected $created;

    /**
     * @Column(column="updated", type="dateTime", nullable=true)
     */
    protected $updated;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_ORDER_ID => true,
        self::PROPERTY_PRODUCT_SIZE_ID => true,
        self::PROPERTY_QUANTITY => true,
        self::PROPERTY_FINAL_QTY => true,
        self::ALIAS_ORDER => true,
        self::ALIAS_PRODUCT_SIZE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     * @return $this
     */
    public function setOrderId($orderId)
    {
        $this->orderId = null !== $orderId ? (int)$orderId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductSizeId()
    {
        return $this->productSizeId;
    }

    /**
     * @param int $productSizeId
     * @return $this
     */
    public function setProductSizeId($productSizeId)
    {
        $this->productSizeId = null !== $productSizeId ? (int)$productSizeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = null !== $quantity ? (int)$quantity : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getFinalQty()
    {
        return $this->finalQty ? $this->finalQty : 0;
    }

    /**
     * @param int $finalQty
     * @return $this
     */
    public function setFinalQty($finalQty)
    {
        $this->finalQty = null !== $finalQty ? (int)$finalQty : null;

        return $this;
    }

    /**
     * @return null|Order
     * 
     * @throws \DomainException
     */
    public function getOrder()
    {
        return $this->getRelated(self::ALIAS_ORDER);
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function setOrder(Order $order)
    {
        $this->orderId = $order->getId();

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string|DateTime $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param string|DateTime $updated
     * @return $this
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @return null|ProductSize
     * 
     * @throws \DomainException
     */
    public function getProductSize()
    {
        return $this->getRelated(self::ALIAS_PRODUCT_SIZE);
    }

    /**
     * @param ProductSize $productSize
     * @return $this
     */
    public function setProductSize(ProductSize $productSize)
    {
        $this->productSizeId = $productSize->getId();

        return $this;
    }

    public function beforeUpdate()
    {
        $date = new Vemid\Date\DateTime();
        $this->updated = $date->getUnixFormat();
    }

    public function beforeCreate()
    {
        $date = new Vemid\Date\DateTime();
        $this->created = $date->getUnixFormat();
    }

}
