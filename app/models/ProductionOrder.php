<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * ProductionOrder Model Class
 *
 * @Source('productions_orders')
 *
 * @BelongsTo('productSizeId', 'ProductSize', 'id', {'alias':'ProductSize'})
 */
class ProductionOrder extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PRODUCT_SIZE_ID = 'productSizeId';
    const PROPERTY_WORK_ORDER = 'workOrder';
    const PROPERTY_PLANED_QTY = 'planedQty';
    const PROPERTY_REALIZED_A_QTY = 'realizedAQty';
    const PROPERTY_REALIZED_B_QTY = 'realizedBQty';
    const PROPERTY_RECEIVED_A_QTY = 'receivedAQty';
    const PROPERTY_RECEIVED_B_QTY = 'receivedBQty';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_EMAIL_SENT = 'emailSent';

    const ALIAS_PRODUCT_SIZE = 'ProductSize';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="product_size_id", type="integer", nullable=false)
     * @FormElement(label="Product size", type="Select", required=true, relation="ProductSize")
     */
    protected $productSizeId;

    /**
     * @Column(column="work_order", type="integer", nullable=false)
     * @FormElement(label="Work order", type="Text", required=true)
     */
    protected $workOrder;

    /**
     * @Column(column="planed_qty", type="decimal", nullable=false)
     * @FormElement(label="Planed qty", type="Text", required=true)
     */
    protected $planedQty;

    /**
     * @Column(column="realized_a_qty", type="decimal", nullable=false)
     * @FormElement(label="Realized a qty", type="Text", required=true)
     */
    protected $realizedAQty;

    /**
     * @Column(column="realized_b_qty", type="decimal", nullable=false)
     * @FormElement(label="Realized b qty", type="Text", required=true)
     */
    protected $realizedBQty;

    /**
     * @Column(column="received_a_qty", type="decimal", nullable=false)
     * @FormElement(label="Received A qty", type="Text", required=true)
     */
    protected $receivedAQty;

    /**
     * @Column(column="received_b_qty", type="decimal", nullable=false)
     * @FormElement(label="Received B qty", type="Text", required=true)
     */
    protected $receivedBQty;


    /**
     * @Column(column="type", type="string", nullable=true)
     * @FormElement(label="Type", type="Text", required=false)
     */
    protected $type;

    /**
     * @Column(column="email_sent", type="bool", nullable=true)
     */
    protected $emailSent;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_WORK_ORDER => true,
        self::PROPERTY_PRODUCT_SIZE_ID => true,
        self::PROPERTY_PLANED_QTY => true,
        self::PROPERTY_REALIZED_A_QTY => true,
        self::PROPERTY_REALIZED_B_QTY => true,
        self::PROPERTY_TYPE => true,
        self::ALIAS_PRODUCT_SIZE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getWorkOrder()
    {
        return $this->workOrder;
    }

    /**
     * @param int $workOrder
     * @return $this
     */
    public function setWorkOrder($workOrder)
    {
        $this->workOrder = null !== $workOrder ? (int)$workOrder : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductSizeId()
    {
        return $this->productSizeId;
    }

    /**
     * @param int $productSizeId
     * @return $this
     */
    public function setProductSizeId($productSizeId)
    {
        $this->productSizeId = null !== $productSizeId ? (int)$productSizeId : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getPlanedQty()
    {
        return $this->planedQty;
    }

    /**
     * @param float $planedQty
     * @return $this
     */
    public function setPlanedQty($planedQty)
    {
        $this->planedQty = null !== $planedQty ? (float)$planedQty : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getRealizedAQty()
    {
        return $this->realizedAQty;
    }

    /**
     * @param float $realizedAQty
     * @return $this
     */
    public function setRealizedAQty($realizedAQty)
    {
        $this->realizedAQty = null !== $realizedAQty ? (float)$realizedAQty : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getRealizedBQty()
    {
        return $this->realizedBQty;
    }

    /**
     * @param float $realizedBQty
     * @return $this
     */
    public function setRealizedBQty($realizedBQty)
    {
        $this->realizedBQty = null !== $realizedBQty ? (float)$realizedBQty : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getReceivedAQty()
    {
        return $this->receivedAQty;
    }

    /**
     * @param float $receivedAQty
     * @return $this
     */
    public function setReceivedAQty($receivedAQty)
    {
        $this->receivedAQty = null !== $receivedAQty ? (float)$receivedAQty : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getReceivedBQty()
    {
        return $this->receivedBQty;
    }

    /**
     * @param float $receivedBQty
     * @return $this
     */
    public function setReceivedBQty($receivedBQty)
    {
        $this->receivedBQty = null !== $receivedBQty ? (float)$receivedBQty : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getEmailSent()
    {
        return $this->emailSent;
    }

    /**
     * @param boolean $emailSent
     * @return $this
     */
    public function setEmailSent($emailSent)
    {
        $this->emailSent = $emailSent;

        return $this;
    }

    /**
     * @return null|ProductSize
     * 
     * @throws \DomainException
     */
    public function getProductSize()
    {
        return $this->getRelated(self::ALIAS_PRODUCT_SIZE);
    }

    /**
     * @param ProductSize $productSize
     * @return $this
     */
    public function setProductSize(ProductSize $productSize)
    {
        $this->productSizeId = $productSize->getId();

        return $this;
    }

}
