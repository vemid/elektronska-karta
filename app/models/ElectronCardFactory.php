<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * ElectronCardFactory Model Class
 *
 * @Source('electron_card_factories')
 *
 * @BelongsTo('electronCardId', 'ElectronCard', 'id', {'alias':'ElectronCard'})
 * @BelongsTo('materialFactoryId', 'MaterialFactory', 'id', {'alias':'MaterialFactory'})
 * @BelongsTo('materialSampleId', 'MaterialSample', 'id', {'alias':'MaterialSample'})
 * @BelongsTo('materialSketchId', 'MaterialSketch', 'id', {'alias':'MaterialSketch'})
 * @BelongsTo('classificationCodeId', 'Classification', 'id', {'alias':'ClassificationCode'})
 * @BelongsTo('supplierId', 'Supplier', 'id', {'alias':'Supplier'})
 */
class ElectronCardFactory extends AbstractEntity
{
    const BASIC = 'BASIC';
    const STRING = 'STRING';
    const STITCH = 'STITCH';
    const ATTENDANT = 'ATTENDANT';

    const PROPERTY_ID = 'id';
    const PROPERTY_ELECTRON_CARD_ID = 'electronCardId';
    const PROPERTY_MATERIAL_FACTORY_ID = 'materialFactoryId';
    const PROPERTY_MATERIAL_SAMPLE_ID = 'materialSampleId';
    const PROPERTY_MATERIAL_SKETCH_ID = 'materialSketchId';
    const PROPERTY_SUPPLIER_ID = 'supplierId';
    const PROPERTY_NAME = 'name';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_CLASSIFICATION_CODE_ID = 'classificationCodeId';
    const PROPERTY_GRAMS = 'grams';
    const PROPERTY_COLOR = 'color';
    const PROPERTY_CREATED = 'created';

    const ALIAS_ELECTRON_CARD = 'ElectronCard';
    const ALIAS_MATERIAL_FACTORY = 'MaterialFactory';
    const ALIAS_MATERIAL_SAMPLE = 'MaterialSample';
    const ALIAS_MATERIAL_SKETCH = 'MaterialSketch';
    const ALIAS_CLASSIFICATION_CODE = 'ClassificationCode';
    const ALIAS_SUPPLIER = 'Supplier';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="electron_card_id", type="integer", nullable=false)
     * @FormElement(label="Electron card", type="Hidden", required=true, relation="ElectronCard")
     */
    protected $electronCardId;

    /**
     * @Column(column="material_sample_id", type="integer", nullable=true)
     * @FormElement(label="Material Sample", type="Select", required=false, relation="MaterialSample")
     */
    protected $materialSampleId;

    /**
     * @Column(column="material_factory_id", type="integer", nullable=true)
     * @FormElement(label="Material factory", type="Select", required=false, relation="MaterialFactory")
     */
    protected $materialFactoryId;

    /**
     * @Column(column="material_sketch_id", type="integer", nullable=true)
     * @FormElement(label="Material Sketch", type="Select", required=false, relation="MaterialSketch")
     */
    protected $materialSketchId;

    /**
     * @Column(column="classification_code_id", type="integer", nullable=true)
     */
    protected $classificationCodeId;

    /**
     * @Column(column="supplier_id", type="integer", nullable=true)
     */
    protected $supplierId;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="type", type="string", nullable=false)
     * @FormElement(label="Type", type="Text", required=true)
     */
    protected $type;

    /**
     * @Column(column="grams", type="string", nullable=true)
     */
    protected $grams;

    /**
     * @Column(column="color", type="string", nullable=true)
     */
    protected $color;

    /**
     * @Column(column="created", type="dateTime", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_ELECTRON_CARD_ID => true,
        self::PROPERTY_MATERIAL_FACTORY_ID => true,
        self::PROPERTY_MATERIAL_SAMPLE_ID => true,
        self::PROPERTY_MATERIAL_SKETCH_ID => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_TYPE => true,
        self::PROPERTY_CLASSIFICATION_CODE_ID => true,
        self::ALIAS_ELECTRON_CARD => true,
        self::ALIAS_MATERIAL_FACTORY => true,
        self::ALIAS_CLASSIFICATION_CODE => true,
        self::PROPERTY_CREATED => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getElectronCardId()
    {
        return $this->electronCardId;
    }

    /**
     * @param int $electronCardId
     * @return $this
     */
    public function setElectronCardId($electronCardId)
    {
        $this->electronCardId = null !== $electronCardId ? (int)$electronCardId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaterialFactoryId()
    {
        return $this->materialFactoryId;
    }

    /**
     * @param int $materialFactoryId
     * @return $this
     */
    public function setMaterialFactoryId($materialFactoryId)
    {
        $this->materialFactoryId = null !== $materialFactoryId ? (int)$materialFactoryId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaterialSampleId()
    {
        return $this->materialSampleId;
    }

    /**
     * @param int $materialSampleId
     * @return $this
     */
    public function setMaterialSampleId($materialSampleId)
    {
        $this->materialSampleId = $materialSampleId;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaterialSketchId()
    {
        return $this->materialSketchId;
    }

    /**
     * @param int $materialSketchId
     * @return $this
     */
    public function setMaterialSketchId($materialSketchId)
    {
        $this->materialSketchId = $materialSketchId;

        return $this;

    }

    /**
     * @return int
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplierId
     * @return $this
     */
    public function setSupplierId($supplierId)
    {
        $this->supplierId = null !== $supplierId ? (int)$supplierId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getGrams()
    {
        return $this->grams;
    }

    /**
     * @return string
     */
    public function getGramUnits()
    {
        return $this->grams !== null ? $this->grams ."gr" : null;
    }

    /**
     * @param string $grams
     * @return $this
     */
    public function setGrams($grams)
    {
        $this->grams = preg_replace('/\D/', '', $grams);

        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return $this
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return int
     */
    public function getClassificationCodeId()
    {
        return $this->classificationCodeId;
    }

    /**
     * @param int $classificationCodeId
     * @return $this
     */
    public function setClassificationCodeId($classificationCodeId)
    {
        $this->classificationCodeId = null !== $classificationCodeId ? (int)$classificationCodeId : null;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string|DateTime $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|ElectronCard
     * 
     * @throws \DomainException
     */
    public function getElectronCard()
    {
        return $this->getRelated(self::ALIAS_ELECTRON_CARD);
    }

    /**
     * @param ElectronCard $electronCard
     * @return $this
     */
    public function setElectronCard(ElectronCard $electronCard)
    {
        $this->electronCardId = $electronCard->getId();

        return $this;
    }

    /**
     * @return null|MaterialFactory
     * 
     * @throws \DomainException
     */
    public function getMaterialFactory()
    {
        return $this->getRelated(self::ALIAS_MATERIAL_FACTORY);
    }

    /**
     * @param MaterialFactory $materialFactory
     * @return $this
     */
    public function setMaterialFactory(MaterialFactory $materialFactory)
    {
        $this->materialFactoryId = $materialFactory->getId();

        return $this;
    }

    /**
     * @return null|MaterialSample
     *
     * @throws \DomainException
     */
    public function getMaterialSample()
    {
        return $this->getRelated(self::ALIAS_MATERIAL_SAMPLE);
    }

    /**
     * @param MaterialSample $materialSample
     * @return $this
     */
    public function setMaterialSample(MaterialSample $materialSample)
    {
        $this->materialSampleId = $materialSample->getId();

        return $this;
    }

    /**
     * @return null|MaterialSketch
     *
     * @throws \DomainException
     */
    public function getMaterialSketch()
    {
        return $this->getRelated(self::ALIAS_MATERIAL_SKETCH);
    }

    /**
     * @param MaterialSketch $materialSketch
     * @return $this
     */
    public function setMaterialSketch(MaterialSketch $materialSketch)
    {
        $this->materialSketchId = $materialSketch->getId();

        return $this;
    }

    /**
     * @return null|Supplier
     *
     * @throws \DomainException
     */
    public function getSupplier()
    {
        return $this->getRelated(self::ALIAS_SUPPLIER);
    }

    /**
     * @param Supplier $supplier
     * @return $this
     */
    public function setSupplier(Supplier $supplier)
    {
        $this->supplierId = $supplier->getId();

        return $this;
    }


    /**
     * @return null|Classification
     *
     * @throws \DomainException
     */
    public function getClassificationCode()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION_CODE);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setClassificationCode(Classification $classification)
    {
        $this->classificationCodeId = $classification->getId();

        return $this;
    }

    public function getDisplayName()
    {
        if ($this->getMaterialFactory()) {
            return "Kartica: ".$this->getMaterialFactory()->getDisplayName();
        }
        elseif ($this->getMaterialSketch()) {
            return $this->getMaterialSketch()->getDisplayName() ."-(". $this->getName() .")";
        }
        else return $this->getClassificationCode()->getParentClassification() . '-' .trim($this->getClassificationCode()). '-'.$this->getSupplier().'-'.
            trim($this->getGramUnits() . '-' . trim($this->getColor())
            . '-(' .trim($this->getName()).')');
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }

    /**
     * @return string
     */
//    public function __toString()
//    {
//        return trim($this->getSupplier()) . '-' . trim($this->getClassificationCode()->getParentClassification()) . '-'
//            . trim($this->getClassificationCode()) . '-' . trim($this->getGramUnits() . '-' . trim($this->getColor())
//            . '-(' .trim($this->getName()).')');
//    }
}
