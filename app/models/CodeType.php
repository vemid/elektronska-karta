<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * CodeType Model Class
 *
 * @Source('code_types')
 * @HasMany('id', 'Code', 'codeTypeId', {'alias':'Codes'})
 */
class CodeType extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CODE = 'code';
    const PROPERTY_NAME = 'name';
    const PROPERTY_IS_MAIN = 'isMain';

    const ALIAS_CODES = 'Codes';

    const CLASSIFICATION_TYPE = 'CLASSIFICATION_TYPE';
    const MATERIAL_TYPE = 'MATERIAL_TYPE';
    const PRODUCT = 'PRODUCT';
    const CLASSIFICATION_CATEGORY = 'CLASSIFICATION_CATEGORY';
    const SIZES = 'SIZES';
    const PRODUCT_TYPE = 'PRODUCT_TYPE';
    const SHOPS = 'SHOPS';
    const ATTRIBUTES = 'ATTRIBUTES';
    const PRODUCTION_SECTORS = 'PRODUCTION_SECTORS';
    const CHECKIN_LOCATION = 'CHECKIN_LOCATION';
    const FAILURE = 'FAILURE';
    const FISCAL_CARD = 'FISCAL_CARD';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="code", type="string", nullable=false)
     * @FormElement(label="Code", type="Text", required=false)
     */
    protected $code;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=false)
     */
    protected $name;

    /**
     * @Column(column="is_main", type="bool", nullable=false)
     * @FormElement(label="Is Main", type="Check", required=false)
     */
    protected $isMain;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CODE => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_IS_MAIN => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsMain()
    {
        return $this->isMain;
    }

    /**
     * @param bool $isMain
     * @return $this
     */
    public function setIsMain($isMain)
    {
        $this->isMain = $isMain;

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|Code[]
     * 
     * @throws \DomainException
     */
    public function getCodes($arguments = null)
    {
        return $this->getRelated(self::ALIAS_CODES, $arguments);
    }

}
