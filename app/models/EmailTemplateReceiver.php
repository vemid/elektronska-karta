<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * EmailTemplateReceiver Model Class
 *
 * @Source('email_template_receivers')
 *
 * @BelongsTo('userId', 'User', 'id', {'alias':'User'})
 */
class EmailTemplateReceiver extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_TEMPLATE_OBJECT_ID = 'templateObjectId';
    const PROPERTY_TEMPLATE_OBJECT_TYPE_ID = 'templateObjectTypeId';
    const PROPERTY_USER_ID = 'userId';

    const ALIAS_USER = 'User';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="template_object_id", type="integer", nullable=false)
     * @FormElement(label="Template object", type="Text", required=true)
     */
    protected $templateObjectId;

    /**
     * @Column(column="template_object_type_id", type="integer", nullable=false)
     * @FormElement(label="Template object type", type="Text", required=true)
     */
    protected $templateObjectTypeId;

    /**
     * @Column(column="user_id", type="integer", nullable=false)
     * @FormElement(label="User", type="Select", required=true, relation="User")
     */
    protected $userId;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_TEMPLATE_OBJECT_ID => true,
        self::PROPERTY_TEMPLATE_OBJECT_TYPE_ID => true,
        self::PROPERTY_USER_ID => true,
        self::ALIAS_USER => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getTemplateObjectId()
    {
        return $this->templateObjectId;
    }

    /**
     * @param int $templateObjectId
     * @return $this
     */
    public function setTemplateObjectId($templateObjectId)
    {
        $this->templateObjectId = null !== $templateObjectId ? (int)$templateObjectId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getTemplateObjectTypeId()
    {
        return $this->templateObjectTypeId;
    }

    /**
     * @param int $templateObjectTypeId
     * @return $this
     */
    public function setTemplateObjectTypeId($templateObjectTypeId)
    {
        $this->templateObjectTypeId = null !== $templateObjectTypeId ? (int)$templateObjectTypeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = null !== $userId ? (int)$userId : null;

        return $this;
    }

    /**
     * @return null|User
     * 
     * @throws \DomainException
     */
    public function getUser()
    {
        return $this->getRelated(self::ALIAS_USER);
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->userId = $user->getId();

        return $this;
    }

}
