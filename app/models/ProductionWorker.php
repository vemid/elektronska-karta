<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * ProductionWorker Model Class
 *
 * @Source('production_workers')
 *
 * @BelongsTo('codeId', 'Code', 'id', {'alias':'Code'})
 * @BelongsTo('locationCodeId', 'Code', 'id', {'alias':'LocationCode'})
 * @BelongsTo('headManagerId', 'ProductionWorker', 'id', {'alias':'HeadManager'})
 * @HasMany('id', 'AbsenceWork', 'productionWorkerId', {'alias':'AbsenceWork'})
 * @HasMany('id', 'Comment', 'productionWorkerId', {'alias':'Comment'})
 * @HasMany('id', 'ProductionWorkerCheckin', 'workerId', {'alias':'ProductionWorkerCheckin'})
 * @HasMany('id', 'ProductionWorker', 'headManagerId', {'alias':'HeadManagerWorkers'})
 */
class ProductionWorker extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_NAME = 'name';
    const PROPERTY_LAST_NAME = 'lastName';
    const PROPERTY_CODE_ID = 'codeId';
    const PROPERTY_WORKER_TYPE = 'workerType';
    const PROPERTY_WORKER_CODE = 'workerCode';
    const PROPERTY_MIS_CODE = 'misCode';
    const PROPERTY_CHECKIN_CODE = 'checkinCode';
    const PROPERTY_GENDER = 'gender';
    const PROPERTY_CREATED = 'created';
    const PROPERTY_PRICE_TIME = 'priceTime';
    const PROPERTY_LOCATION_CODE_ID = 'locationCodeId';
    const PROPERTY_EMAIL = 'email';
    const PROPERTY_START_TIME = 'startTime';
    const PROPERTY_END_TIME = 'endTime';
    const PROPERTY_HEAD_MANAGER_ID = 'headManagerId';
    const PROPERTY_BREAK_UP = 'breakUp';

    const STATUS_REGULAR = 'REGULAR';
    const STATUS_HAND = 'HAND';
    const STATUS_MACHINE = 'MACHINE';

    const ALIAS_CODE = 'Code';
    const ALIAS_LOCATION_CODE = 'LocationCode';

    const ALIAS_ABSENCE_WORK = 'AbsenceWork';
    const ALIAS_PRODUCTION_WORKER_CHECKIN = 'ProductionWorkerCheckin';
    const ALIAS_HEAD_MANAGER = 'HeadManager';
    const ALIAS_HEAD_MANAGER_WORKERS = 'HeadManagerWorkers';
    const ALIAS_COMMENT = 'Comment';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="code_id", type="integer", nullable=false)
     * @FormElement(label="Sector ID", type="Select", required=true)
     */
    protected $codeId;

    /**
     * @Column(column="worker_type", type="string", nullable=true)
     * @FormElement(label="Tip Radnika", type="Select", required=false, options={'REGULAR':'Regular radnika', 'HAND':'Rucni radnik', 'MACHINE':'Masinski radnik'})

     */
    protected $workerType;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="First Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="last_name", type="string", nullable=false)
     * @FormElement(label="Last name", type="Text", required=true)
     */
    protected $lastName;

    /**
     * @Column(column="worker_code", type="integer", nullable=false)
     * @FormElement(label="ProductionWorker code", type="Text", required=true)
     */
    protected $workerCode;

    /**
     * @Column(column="checkin_code", type="string", nullable=true)
     * @FormElement(label="Checkin Code", type="Text", required=false)
     */
    protected $checkinCode;

    /**
     * @Column(column="mis_code", type="string", nullable=true)
     * @FormElement(label="MIS Code", type="Text", required=false)
     */
    protected $misCode;

    /**
     * @Column(column="price_time", type="integer", nullable=false)
     * @FormElement(label="Price Time", type="Text", required=true)
     */
    protected $priceTime;

    /**
     * @Column(column="gender", type="string", nullable=true)
     * @FormElement(label="Gender", type="Select", required=false, options={'MALE':'Male', 'FEMALE':'Female'})
     */
    protected $gender;

    /**
     * @Column(column="location_code_id", type="integer", nullable=false)
     * @FormElement(label="Location ID", type="Select", required=true)
     */
    protected $locationCodeId;

    /**
     * @Column(column="head_manager_id", type="integer", nullable=true)
     * @FormElement(label="Head Manager", type="Select", required=false, relation="ProductionWorker")
     */
    protected $headManagerId;

    /**
     * @Column(column="start_time", type="string", nullable=false)
     * @FormElement(label="Start Time", type="Text", required=true)
     */
    protected $startTime;

    /**
     * @Column(column="end_time", type="string", nullable=false)
     * @FormElement(label="End Time", type="Text", required=true)
     */
    protected $endTime;

    /**
     * @Column(column="email", type="string", nullable=true)
     * @FormElement(label="Email", type="Text", required=false)
     */
    protected $email;

    /**
     * @Column(column="break_up", type="date", nullable=true)
     * @FormElement(label="Prekid rada", type="Date", required=false)
     */
    protected $breakUp;

    /**
     * @Column(column="created", type="string", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_LAST_NAME => true,
        self::PROPERTY_CODE_ID => true,
        self::PROPERTY_WORKER_TYPE => true,
        self::PROPERTY_WORKER_CODE => true,
        self::PROPERTY_CHECKIN_CODE => true,
        self::PROPERTY_GENDER => true,
        self::PROPERTY_LOCATION_CODE_ID => true,
        self::PROPERTY_EMAIL => true,
        self::PROPERTY_START_TIME => true,
        self::PROPERTY_HEAD_MANAGER_ID => true,
        self::PROPERTY_CREATED => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return int
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * @param int $codeId
     * @return $this
     */
    public function setCodeId($codeId)
    {
        $this->codeId = null !== $codeId ? (int)$codeId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getWorkerType()
    {
        return $this->workerType;
    }

    /**
     * @param string $workerType
     * @return $this
     */
    public function setWorkerType($workerType)
    {
        $this->workerType = $workerType;

        return $this;
    }

    /**
     * @return int
     */
    public function getWorkerCode()
    {
        return $this->workerCode;
    }

    /**
     * @param int $workerCode
     * @return $this
     */
    public function setWorkerCode($workerCode)
    {
        $this->workerCode = null !== $workerCode ? (int)$workerCode : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getCheckinCode()
    {
        return $this->checkinCode;
    }

    /**
     * @param string $checkinCode
     * @return $this
     */
    public function setCheckinCode($checkinCode)
    {
        $this->checkinCode = $checkinCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getMisCode()
    {
        return $this->misCode;
    }

    /**
     * @param string $misCode
     * @return $this
     */
    public function setMisCode($misCode)
    {
        $this->misCode = $misCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return int
     */
    public function getLocationCodeId()
    {
        return $this->locationCodeId;
    }

    /**
     * @param int $locationCodeId
     * @return $this
     */
    public function setLocationCodeId($locationCodeId)
    {
        $this->locationCodeId = null !== $locationCodeId ? (int)$locationCodeId : null;

        return $this;
    }

    /**
     * @return null|Code
     *
     * @throws \DomainException
     */
    public function getSectorCode()
    {
        return $this->getRelated(self::ALIAS_CODE);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setSectorCode(Code $code)
    {
        $this->codeId = $code->getId();

        return $this;
    }

    /**
     * @return null|Code
     *
     * @throws \DomainException
     */
    public function getLocationCode()
    {
        return $this->getRelated(self::ALIAS_LOCATION_CODE);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setLocationCode(Code $code)
    {
        $this->locationCodeId = $code->getId();

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceTime()
    {
        return $this->priceTime;
    }

    /**
     * @param float $priceTime
     * @return $this
     */
    public function setPiecesHour($priceTime)
    {
        $this->priceTime = null !== $priceTime ? (float)$priceTime : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param string $startTime
     * @return $this
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param string $endTime
     * @return $this
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getBreakUp()
    {
        return $this->breakUp;
    }

    /**
     * @param string|DateTime $breakUp
     * @return $this
     */
    public function setBreakUp($breakUp)
    {
        $this->breakUp = $breakUp;

        return $this;
    }

    /**
     * @return int
     */
    public function getHeadManagerId()
    {
        return $this->headManagerId;
    }

    /**
     * @param int $headManagerId
     * @return $this
     */
    public function setHeadManagerId($headManagerId)
    {
        $this->headManagerId = null !== $headManagerId ? (int)$headManagerId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        if($this->breakUp)
            return '[PREKID] '.trim($this->name) . ' ' . trim($this->lastName);
        else
            return trim($this->workerCode) . '-' .trim($this->name) . ' ' . trim($this->lastName);
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|AbsenceWork[]
     *
     * @throws \DomainException
     */
    public function getAbsenceWork($arguments = null)
    {
        return $this->getRelated(self::ALIAS_ABSENCE_WORK, $arguments);
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|Comment[]
     *
     * @throws \DomainException
     */
    public function getComment($arguments = null)
    {
        return $this->getRelated(self::ALIAS_COMMENT, $arguments);
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|ProductionWorkerCheckin[]
     *
     * @throws \DomainException
     */
    public function getProductionWorkerCheckin($arguments = null)
{
    return $this->getRelated(self::ALIAS_PRODUCTION_WORKER_CHECKIN, $arguments);
}

    /**
     * @param null|string|array $arguments
     * @return Resultset|ProductionWorkerCheckin[]
     *
     * @throws \DomainException
     */
    public function getManagerWorkers($arguments = null)
    {
        return $this->getRelated(self::ALIAS_HEAD_MANAGER_WORKERS, $arguments);
    }

    /**
     * @return self|\Vemid\Entity\EntityInterface
     *
     * @throws \DomainException
     */
    public function getHeadManager()
    {
        return $this->getRelated(self::ALIAS_HEAD_MANAGER);
    }

    /**
     * @param self $headManager
     * @return $this
     */
    public function setHeadManager(self $headManager)
    {
        $this->$headManager = $headManager->getId();

        return $this;
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
