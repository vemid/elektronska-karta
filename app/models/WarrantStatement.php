<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * WarrantStatement Model Class
 *
 * @Source('warrant_statements')
 * @HasMany('id', 'WarrantStatementItem', 'warrantStatementId', {'alias':'WarrantStatementItems'})
 */
class WarrantStatement extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_ACCOUNT = 'account';
    const PROPERTY_STATEMENT_NUMBER = 'statementNumber';
    const PROPERTY_STATEMENT_PATH = 'statementPath';
    const PROPERTY_FILE_HASH = 'fileHash';
    const PROPERTY_DATE = 'date';

    const ALIAS_WARRANT_STATEMENT_ITEMS = 'WarrantStatementItems';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="account", type="string", nullable=true)
     * @FormElement(label="Account", type="Text", required=false)
     */
    protected $account;

    /**
     * @Column(column="statement_number", type="integer", nullable=false)
     * @FormElement(label="Statement number", type="Text", required=true)
     */
    protected $statementNumber;

    /**
     * @Column(column="statement_path", type="string", nullable=false)
     * @FormElement(label="Statement path", type="Text", required=true)
     */
    protected $statementPath;

    /**
     * @Column(column="file_hash", type="string", nullable=false)
     */
    protected $fileHash;

    /**
     * @Column(column="date", type="date", nullable=false)
     * @FormElement(label="Date", type="Date", required=true)
     */
    protected $date;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_ACCOUNT => true,
        self::PROPERTY_STATEMENT_NUMBER => true,
        self::PROPERTY_STATEMENT_PATH => true,
        self::PROPERTY_DATE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param string $account
     * @return $this
     */
    public function setAccount($account)
    {
        $this->account = (string)$account;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatementNumber()
    {
        return $this->statementNumber;
    }

    /**
     * @param int $statementNumber
     * @return $this
     */
    public function setStatementNumber($statementNumber)
    {
        $this->statementNumber = null !== $statementNumber ? (int)$statementNumber : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatementPath()
    {
        return $this->statementPath;
    }

    /**
     * @param string $statementPath
     * @return $this
     */
    public function setStatementPath($statementPath)
    {
        $this->statementPath = (string)$statementPath;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileHash()
    {
        return $this->fileHash;
    }

    /**
     * @param string $fileHash
     * @return $this
     */
    public function setFileHash($fileHash)
    {
        $this->fileHash = $fileHash;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string|DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|WarrantStatementItem[]
     * 
     * @throws \DomainException
     */
    public function getWarrantStatementItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_WARRANT_STATEMENT_ITEMS, $arguments);
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return APP_PATH . 'var/uploads/files/';
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $url = ltrim($config->application->baseUri, '/');

        if (is_file($this->getUploadPath() . $this->getStatementPath())) {
            return $url . '/uploads/files/' . $this->getStatementPath();
        }

        return null;
    }

}
