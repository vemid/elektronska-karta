<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * ChannelSupplyEntity Model Class
 *
 * @Source('channel_supply_entities')
 * @HasMany('id', 'WarehouseOrder', 'channelSupplyEntityId', {'alias':'WarrantStatementItems'})
 * @HasMany('id', 'WarehouseOrderGroup', 'channelSupplyEntityId', {'alias':'WarehouseGroupOrders'})
 * @BelongsTo('channelSupplyId', 'ChannelSupply', 'id', {'alias': 'ChannelSupply'})
 */
class ChannelSupplyEntity extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CHANNEL_SUPPLY_ID = 'channelSupplyId';
    const PROPERTY_ENTITY_TYPE_ID = 'entityTypeId';
    const PROPERTY_ENTITY_ID = 'entityId';
    const PROPERTY_CLIENT_CODE = 'clientCode';
    const PROPERTY_MIS_DOCUMENT_TYPE = 'misDocumentType';
    const PROPERTY_MIS_DOCUMENT_GROUP_TYPE = 'misDocumentGroupType';
    const PROPERTY_EMAIL = 'email';
    const PROPERTY_ENTRY_WAREHOUSE = 'entryWarehouse';
    const PROPERTY_POSTAL_SERVICE_TOWN_ID = 'postalServiceTownId';
    const PROPERTY_TOWN = 'town';
    const PROPERTY_STREET = 'street';
    const PROPERTY_STREET_ID = 'streetId';
    const PROPERTY_STREET_NUMBER = 'streetNumber';
    const PROPERTY_WORKER_NAME = 'workerName';
    const PROPERTY_WORKER_PHONE = 'workerPhone';
    const PROPERTY_POSTAL_CONTRACT = 'postalContract';
    const PROPERTY_IMPORTER = 'importer';

    const ALIAS_WAREHOUSE_ORDERS = 'WarehouseOrders';
    const ALIAS_WAREHOUSE_GROUP_ORDERS = 'WarehouseGroupOrders';

    protected $_objectId = self::PROPERTY_ID;

    public const BK_STORE = 'BK_STORE';
    public const FRANCHISE = 'FRANCHISE';
    public const WAREHOUSE = 'WAREHOUSE';
    public const FG_CLIENTS = 'FG_CLIENTS';
    public const DM_CLIENTS = 'DM_CLIENTS';

    public const BK_STORE_GROUP = 'BK_STORE_GROUP';
    public const FRANCHISE_GROUP = 'FRANCHISE_GROUP';
    public const WAREHOUSE_GROUP = 'WAREHOUSE_GROUP';
    public const DM_CLIENTS_GROUP = 'DM_CLIENTS_GROUP';

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="channel_supply_id", type="integer", nullable=false)
     * @FormElement(label="Channel_supply", type="Hidden", required=true)
     */
    protected $channelSupplyId;

    /**
     * @Column(column="entity_type_id", type="integer", nullable=false)
     * @FormElement(label="Entity_type", type="Text", required=false)
     */
    protected $entityTypeId;

    /**
     * @Column(column="entity_id", type="integer", nullable=false)
     * @FormElement(label="Entity", type="Text", required=true)
     */
    protected $entityId;

    /**
     * @Column(column="client_code", type="string", nullable=true)
     * @FormElement(label="Sifra Parntera", type="Text", required=true)
     */
    protected $clientCode;

    /**
     * @Column(column="mis_document_type", type="string", nullable=false)
     * @FormElement(label="Tip MIS dokumenta", type="Select", required=true, options={'BK_STORE':'MP Radnje','FRANCHISE':'Franšize','WAREHOUSE':'Magacin','DM_CLIENTS':'Domaći klijent', 'FG_CLIENTS':'Strani klijenti'})
     */
    protected $misDocumentType;

    /**
     * @Column(column="mis_document_group_type", type="string", nullable=false)
     * @FormElement(label="Tip MIS grupisanog dokumenta", type="Select", required=true, options={'BK_STORE_GROUP':'Nalog MP','FRANCHISE_GROUP':'Narudzbenica','WAREHOUSE_GROUP':'Magacin prenos','DM_CLIENTS_GROUP':'Nalog za izdavanje otpremnice'})
     */
    protected $misDocumentGroupType;

    /**
     * @Column(column="email", type="string", nullable=true)
     * @FormElement(label="Email", type="Text", required=false)
     */
    protected $email;

    /**
     * @Column(column="entry_warehouse", type="string", nullable=true)
     * @FormElement(label="Ulazni", type="Select", required=true, options={'MP' : 'Magacin gotovih proizvoda - Beograd', 'VP' : 'Magacin gotovih proizvoda VP - Beograd ','KP' : 'Magacin gotovih proizvoda - Knjeazevac','MR' : 'Magacin Robe - Beograd'})
     */
    protected $entryWarehouse;

    /**
     * @Column(column="postal_service_town_id", type="int", nullable=true)
     * @FormElement(label="Grad", type="Select", required=false)
     */
    protected $postalServiceTownId;

    /**
     * @Column(column="town", type="string", nullable=true)
     */
    protected $town;

    /**
     * @Column(column="street_id", type="string", nullable=true)
     * @FormElement(label="Ulica", type="Select", required=false)
     */
    protected $streetId;

    /**
     * @Column(column="street", type="string", nullable=true)
     */
    protected $street;


    /**
     * @Column(column="street_number", type="string", nullable=true)
     * @FormElement(label="Broj", type="Text", required=false)
     */
    protected $streetNumber;

    /**
     * @Column(column="worker_name", type="string", nullable=true)
     * @FormElement(label="Ime poslovodje", type="Text", required=false)
     */
    protected $workerName;

    /**
     * @Column(column="worker_phone", type="string", nullable=true)
     * @FormElement(label="Broj telefona", type="Text", required=false)
     */
    protected $workerPhone;

    /**
     * @Column(column="importer", type="string", nullable=true)
     * @FormElement(label="Uvoznik", type="Text", required=false)
     */
    protected $importer;

    /**
     * @Column(column="postal_contract", type="string", nullable=true)
     * @FormElement(label="Broj Ugovora kurirske sluzbe", type="Text", required=false)
     */
    protected $postalContract;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getChannelSupplyId()
    {
        return $this->channelSupplyId;
    }

    /**
     * @param int $channelSupplyId
     * @return $this
     */
    public function setChannelSupplyId($channelSupplyId)
    {
        $this->channelSupplyId = null !== $channelSupplyId ? (int)$channelSupplyId : null;

        return $this;
    }

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CHANNEL_SUPPLY_ID => true,
        self::PROPERTY_ENTITY_TYPE_ID => true,
        self::PROPERTY_ENTITY_ID => true,
        self::PROPERTY_MIS_DOCUMENT_TYPE => true,
        self::PROPERTY_EMAIL => true,
        self::PROPERTY_POSTAL_SERVICE_TOWN_ID => true,
        self::PROPERTY_STREET_ID => true,
        self::PROPERTY_WORKER_NAME => true,
        self::PROPERTY_WORKER_PHONE => true,
        self::PROPERTY_POSTAL_CONTRACT => true,
        self::PROPERTY_ENTRY_WAREHOUSE => true,
    );

    /**
     * @return int
     */
    public function getEntityTypeId()
    {
        return $this->entityTypeId;
    }

    /**
     * @param int $entityTypeId
     * @return $this
     */
    public function setEntityTypeId($entityTypeId)
    {
        $this->entityTypeId = null !== $entityTypeId ? (int)$entityTypeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        $this->entityId = null !== $entityId ? (int)$entityId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientCode()
    {
        return $this->clientCode;
    }

    /**
     * @param string $clientCode
     * @return $this
     */
    public function setClientCode($clientCode)
    {
        $this->clientCode = $clientCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getMisDocumentType()
    {
        return $this->misDocumentType;
    }

    /**
     * @param string $misDocumentType
     * @return $this
     */
    public function setMisDocumentType($misDocumentType)
    {
        $this->misDocumentType = $misDocumentType;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntryWarehouse()
    {
        return $this->entryWarehouse;
    }

    /**
     * @param string $entryWarehouse
     * @return $this
     */
    public function setEntryWarehouse($entryWarehouse)
    {
        $this->entryWarehouse = $entryWarehouse;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostalServiceTownId()
    {
        return $this->postalServiceTownId;
    }

    /**
     * @param string $postalServiceTownId
     * @return $this
     */
    public function setPostalServiceTownId($postalServiceTownId)
    {
        $this->postalServiceTownId = $postalServiceTownId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * @param string $town
     */
    public function setTown($town)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreetId()
    {
        return $this->streetId;
    }

    /**
     * @param string $streetId
     * @return $this
     */
    public function setStreetId($streetId)
    {
        $this->streetId = $streetId;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreetNumber()
    {
        return $this->streetNumber;
    }

    /**
     * @param string $streetNumber
     */
    public function setStreetNumber($streetNumber)
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getWorkerPhone()
    {
        return $this->workerPhone;
    }

    /**
     * @param string $workerPhone
     */
    public function setWorkerPhone($workerPhone)
    {
        $this->workerPhone = $workerPhone;

        return $this;
    }

    /**
     * @return string
     */
    public function getWorkerName()
    {
        return $this->workerName;
    }

    /**
     * @param string $workerName
     */
    public function setWorkerName($workerName)
    {
        $this->workerName = $workerName;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostalContract()
    {
        return $this->postalContract;
    }

    /**
     * @param string $postalContract
     */
    public function setPostalContract($postalContract)
    {
        $this->postalContract = $postalContract;

        return $this;
    }

    /**
     * @return string
     */
    public function getMisDocumentGroupType()
    {
        return $this->misDocumentGroupType;
    }

    /**
     * @param string $misDocumentGroupType
     * @return $this
     */
    public function setMisDocumentGroupType($misDocumentGroupType)
    {
        $this->misDocumentGroupType = $misDocumentGroupType;

        return $this;
    }

    /**
     * @return string
     */
    public function getImporter()
    {
        return $this->importer;
    }

    /**
     * @param string $importer
     */
    public function setImporter($importer)
    {
        $this->importer = $importer;

        return $this;
    }

    /**
     * @return null|ChannelSupply
     *
     * @throws \DomainException
     */
    public function getChannelSupply()
    {
        return $this->getRelated(ChannelSupply::class);
    }

    /**
     * @param ChannelSupply $channelSupply
     * @return $this
     */
    public function setChannelSupply(ChannelSupply $channelSupply)
    {
        $this->channelSupplyId = $channelSupply->getId();

        return $this;
    }

    /**
     * @return null|User|\Vemid\Entity\EntityInterface
     *
     * @throws \DomainException
     */
    public function getUser()
    {
        return $this->getRelated(self::ALIAS_USER);
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->userId = $user->getId();

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|WarehouseOrder[]
     *
     * @throws \DomainException
     */
    public function getWarehouseOrders($arguments = null)
    {
        return $this->getRelated(self::ALIAS_WAREHOUSE_ORDERS, $arguments);
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|WarehouseOrderGroup[]
     *
     * @throws \DomainException
     */
    public function getWarehouseGroupOrders($arguments = null)
    {
        return $this->getRelated(self::ALIAS_WAREHOUSE_GROUP_ORDERS, $arguments);
    }

    public function getEntity()
    {
        if (!$this->getChannelSupply()) {
            return  null;
        }

        $entity = Client::class;
        if ($this->getChannelSupply()->getType() === ChannelSupply::MP) {
            $entity = Code::class;
        }

        return $this->getDI()->getEntityManager()->findOne($entity, $this->getEntityId());
    }

    public function getDisplayName()
    {
        if ($this->getChannelSupply()) {
            return (string) $this->getEntity();
        }

        return parent::getDisplayName();
    }

    public function __toString(): string {
        return $this->getDisplayName();
    }

    public function getPostalAddress()
    {
        return $this->getStreet() . " " . $this->getStreetNumber() . ", ".$this->getTown();
    }
}
