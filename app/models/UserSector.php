<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * UserSector Model Class
 *
 * @Source('user_sectors')
 *
 * @BelongsTo('userId', 'User', 'id', {'alias':'User'})
 * @BelongsTo('codeId', 'Code', 'id', {'alias':'Code'})
 */
class UserSector extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_USER_ID = 'userId';
    const PROPERTY_CODE_ID = 'codeId';

    const ALIAS_USER = 'User';
    const ALIAS_CODE = 'Code';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="user_id", type="integer", nullable=false)
     * @FormElement(label="User", type="Select", required=true, relation="User")
     */
    protected $userId;

    /**
     * @Column(column="code_id", type="integer", nullable=false)
     * @FormElement(label="Code", type="Select", required=true, relation="Code")
     */
    protected $codeId;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_USER_ID => true,
        self::PROPERTY_CODE_ID => true,
        self::ALIAS_USER => true,
        self::ALIAS_CODE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = null !== $userId ? (int)$userId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * @param int $codeId
     * @return $this
     */
    public function setCodeId($codeId)
    {
        $this->codeId = null !== $codeId ? (int)$codeId : null;

        return $this;
    }

    /**
     * @return null|User
     * 
     * @throws \DomainException
     */
    public function getUser()
    {
        return $this->getRelated(self::ALIAS_USER);
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->userId = $user->getId();

        return $this;
    }

    /**
     * @return null|Code
     * 
     * @throws \DomainException
     */
    public function getCode()
    {
        return $this->getRelated(self::ALIAS_CODE);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setCode(Code $code)
    {
        $this->codeId = $code->getId();

        return $this;
    }



}
