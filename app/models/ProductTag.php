<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * WarehproductTag Model Class
 *
 * @Source('product_tags')
 * @BelongsTo('productId', 'Product', 'id', {'alias':'Product'})
 */
class ProductTag extends AbstractEntity
{

    public const GENDER = 'GENDER';
    public const BRAND = 'BRAND';
    public const SEASON = 'SEASON';
    public const SIZE = 'SIZE';
    public const COLLECTION = 'COLLECTION';
    public const MODEL = 'MODEL';
    public const AGE = 'AGE';
    public const COLOR = 'COLOR';

    const PROPERTY_ID = 'id';
    const PROPERTY_PRODUCT_ID = 'productId';
    const PROPERTY_PRODUCT_SIZE_ID = 'productSizeId';
    const PROPERTY_CATEGORY = 'category';
    const PROPERTY_TAG = 'tag';

    const ALIAS_PRODUCT = 'Product';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="product_id", type="integer", nullable=false)
     * @FormElement(label="Product", type="Text", required=true)
     */
    protected $productId;

    /**
     * @Column(column="category", type="string", nullable=false)
     * @FormElement(label="Category", type="Text", required=true)
     */
    protected $category;

    /**
     * @Column(column="tag", type="string", nullable=false)
     * @FormElement(label="Tag", type="Text", required=true)
     */
    protected $tag;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRODUCT_ID => true,
        self::PROPERTY_PRODUCT_SIZE_ID => true,
        self::PROPERTY_CATEGORY => true,
        self::PROPERTY_TAG => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->productId = null !== $productId ? (int)$productId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     * @return $this
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * @return null|Product
     *
     * @throws \DomainException
     */
    public function getProduct()
    {
        return $this->getRelated(self::ALIAS_PRODUCT);
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->productId = $product->getId();

        return $this;
    }
}
