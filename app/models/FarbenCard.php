<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * FarbenCard Model Class
 *
 * @Source('farben_cards')
 *
 * @BelongsTo('productId', 'Product', 'id', {'alias':'Product'})
 * @HasMany('id', 'FarbenCardItem', 'farbenCardId', {'alias':'FarbenCardItems'})
 */
class FarbenCard extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PRODUCT_ID = 'productId';
    const PROPERTY_WASH_LABEL = 'washLabel';
    const PROPERTY_PAPER_LABEL = 'paperLabel';
    const PROPERTY_QTY_TOTAL = 'qtyTotal';
    const PROPERTY_CREATED = 'created';

    const ALIAS_PRODUCT = 'Product';
    const ALIAS_FARBEN_CARD_ITEMS = 'FarbenCardItems';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="product_id", type="integer", nullable=false)
     * @FormElement(label="Product", type="Text", required=true, relation="Product")
     */
    protected $productId;

    /**
     * @Column(column="wash_label", type="string", nullable=true)
     */
    protected $washLabel;

    /**
     * @Column(column="paper_label", type="string", nullable=true)
     */
    protected $paperLabel;

    /**
     * @Column(column="qty_total", type="integer", nullable=true)
     * @FormElement(label="Qty total", type="Text", required=false)
     */
    protected $qtyTotal;

    /**
     * @Column(column="created", type="string", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRODUCT_ID => true,
        self::PROPERTY_WASH_LABEL => true,
        self::PROPERTY_PAPER_LABEL => true,
        self::PROPERTY_QTY_TOTAL => true,
        self::PROPERTY_CREATED => true,
        self::ALIAS_PRODUCT => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->productId = null !== $productId ? (int)$productId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getWashLabel()
    {
        return $this->washLabel;
    }

    /**
     * @param string $washLabel
     * @return $this
     */
    public function setWashLabel($washLabel)
    {
        $this->washLabel = $washLabel;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaperLabel()
    {
        return $this->paperLabel;
    }

    /**
     * @param string $paperLabel
     * @return $this
     */
    public function setPaperLabel($paperLabel)
    {
        $this->paperLabel = $paperLabel;

        return $this;
    }


    /**
     * @return int
     */
    public function getQtyTotal()
    {
        return $this->qtyTotal;
    }

    /**
     * @param int $qtyTotal
     * @return $this
     */
    public function setQtyTotal($qtyTotal)
    {
        $this->qtyTotal = null !== $qtyTotal ? (int)$qtyTotal : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|Product
     * 
     * @throws \DomainException
     */
    public function getProduct()
    {
        return $this->getRelated(self::ALIAS_PRODUCT);
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->productId = $product->getId();

        return $this;
    }

    /**
     * @param array|null $arguments
     * @return FarbenCardItem[]|Resultset
     *
     * @throws DomainException
     */
    public function getFarbenCardItems($arguments = null)
    {
        return $this->getRelated(self::ALIAS_FARBEN_CARD_ITEMS, $arguments);
    }

    public function beforeValidationOnCreate()
    {
        $this->qtyTotal == null ? $this->qtyTotal = 0 : $this->qtyTotal;
        $this->created = new DateTime();
    }

    public function getWorkOrders()
    {
        $query = $this->getDI()->getModelsManager()->createBuilder()
            ->columns([
                'GROUP_CONCAT(workOrder) as workOrder'
            ])
            ->addFrom(ProductionOrder::class, 'po')
            ->leftJoin(ProductSize::class, 'po.productSizeId = ps.id', 'ps')
            ->where('ps.productId = :productId:', [
                'productId' => $this->getProduct()->getId()
            ]);

        /** @var ProductionOrder[] $productionOrders */
        $productionOrders = $query->getQuery()->execute()[0];

        return !empty($productionOrders['workOrder']) ? implode(', ', explode(',', $productionOrders['workOrder'])) : '';
    }

}
