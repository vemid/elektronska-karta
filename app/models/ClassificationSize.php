<?php

use Vemid\Entity\AbstractEntity;

/**
 * ClassificationSize Model Class
 *
 * @Source('classification_sizes')
 *
 * @BelongsTo('classificationId', 'Classification', 'id', {'alias':'Classification'})
 * @BelongsTo('codeId', 'Code', 'id', {'alias':'Code'})
 */
class ClassificationSize extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CLASSIFICATION_ID = 'classificationId';
    const PROPERTY_CODE_ID = 'codeId';

    const ALIAS_CLASSIFICATION = 'Classification';
    const ALIAS_CODE = 'Code';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="classification_id", type="integer", nullable=false)
     * @FormElement(label="Classification", type="Select", required=true)
     */
    protected $classificationId;

    /**
     * @Column(column="code_id", type="integer", nullable=false)
     * @FormElement(label="Code", type="Select", required=true)
     */
    protected $codeId;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CLASSIFICATION_ID => true,
        self::PROPERTY_CODE_ID => true,
        self::ALIAS_CLASSIFICATION => true,
        self::ALIAS_CODE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getClassificationId()
    {
        return $this->classificationId;
    }

    /**
     * @param int $classificationId
     * @return $this
     */
    public function setClassificationId($classificationId)
    {
        $this->classificationId = null !== $classificationId ? (int)$classificationId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * @param int $codeId
     * @return $this
     */
    public function setCodeId($codeId)
    {
        $this->codeId = null !== $codeId ? (int)$codeId : null;

        return $this;
    }

    /**
     * @return null|Classification
     *
     * @throws \DomainException
     */
    public function getClassification()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setClassification(Classification $classification)
    {
        $this->classificationId = $classification->getId();

        return $this;
    }

    /**
     * @return null|Code
     *
     * @throws \DomainException
     */
    public function getCode()
    {
        return $this->getRelated(self::ALIAS_CODE);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setCode(Code $code)
    {
        $this->codeId = $code->getId();

        return $this;
    }
}
