<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * PrintPreparation Model Class
 *
 * @Source('print_preparation')
 */
class PrintPreparation extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_SIFRA = 'sifra';
    const PROPERTY_NAZIV = 'naziv';
    const PROPERTY_CODE = 'code';
    const PROPERTY_BARCODE = 'barcode';
    const PROPERTY_QTY = 'qty';
    const PROPERTY_PRICE_SRB = 'priceSrb';
    const PROPERTY_PRICE_CG = 'priceCg';
    const PROPERTY_PRICE_BIH = 'priceBih';
    const PROPERTY_HEIGHT = 'height';
    const PROPERTY_SIZES = 'sizes';
    const PROPERTY_COMPOSITION = 'composition';
    const PROPERTY_WASH_DATA = 'washData';



    /**
     * @Column(column="id", type="integer", nullable=false)
     * @FormElement(label="Id", type="Text", required=true)
     */
    protected $id;

    /**
     * @Column(column="sifra", type="string", nullable=true)
     * @FormElement(label="Sifra", type="Text", required=false)
     */
    protected $sifra;

    /**
     * @Column(column="naziv", type="string", nullable=true)
     * @FormElement(label="Naziv", type="Text", required=false)
     */
    protected $naziv;

    /**
     * @Column(column="code", type="string", nullable=true)
     * @FormElement(label="Code", type="Text", required=false)
     */
    protected $code;

    /**
     * @Column(column="barcode", type="string", nullable=true)
     * @FormElement(label="Barcode", type="Text", required=false)
     */
    protected $barcode;

    /**
     * @Column(column="qty", type="integer", nullable=false)
     * @FormElement(label="Qty", type="Text", required=true)
     */
    protected $qty;

    /**
     * @Column(column="price_srb", type="decimal", nullable=false)
     * @FormElement(label="Price srb", type="Text", required=true)
     */
    protected $priceSrb;

    /**
     * @Column(column="price_cg", type="decimal", nullable=false)
     * @FormElement(label="Price cg", type="Text", required=true)
     */
    protected $priceCg;

    /**
     * @Column(column="price_bih", type="decimal", nullable=false)
     * @FormElement(label="Price bih", type="Text", required=true)
     */
    protected $priceBih;

    /**
     * @Column(column="height", type="integer", nullable=false)
     * @FormElement(label="Height", type="Text", required=true)
     */
    protected $height;

    /**
     * @Column(column="sizes", type="string", nullable=true)
     * @FormElement(label="Sizes", type="TextArea", required=false)
     */
    protected $sizes;

    /**
     * @Column(column="composition", type="string", nullable=false)
     * @FormElement(label="Composition", type="Text", required=true)
     */
    protected $composition;

    /**
     * @Column(column="wash_data", type="string", nullable=false)
     * @FormElement(label="Wash data", type="Text", required=true)
     */
    protected $washData;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_SIFRA => true,
        self::PROPERTY_NAZIV => true,
        self::PROPERTY_CODE => true,
        self::PROPERTY_BARCODE => true,
        self::PROPERTY_QTY => true,
        self::PROPERTY_PRICE_SRB => true,
        self::PROPERTY_PRICE_CG => true,
        self::PROPERTY_PRICE_BIH => true,
        self::PROPERTY_HEIGHT => true,
        self::PROPERTY_SIZES => true,
        self::PROPERTY_COMPOSITION => true,
        self::PROPERTY_WASH_DATA => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getSifra()
    {
        return $this->sifra;
    }

    /**
     * @param string $sifra
     * @return $this
     */
    public function setSifra($sifra)
    {
        $this->sifra = $sifra;

        return $this;
    }

    /**
     * @return string
     */
    public function getNaziv()
    {
        return $this->naziv;
    }

    /**
     * @param string $naziv
     * @return $this
     */
    public function setNaziv($naziv)
    {
        $this->naziv = $naziv;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     * @return $this
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param int $qty
     * @return $this
     */
    public function setQty($qty)
    {
        $this->qty = null !== $qty ? (int)$qty : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceSrb()
    {
        return $this->priceSrb;
    }

    /**
     * @param float $priceSrb
     * @return $this
     */
    public function setPriceSrb($priceSrb)
    {
        $this->priceSrb = null !== $priceSrb ? (float)$priceSrb : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceCg()
    {
        return $this->priceCg;
    }

    /**
     * @param float $priceCg
     * @return $this
     */
    public function setPriceCg($priceCg)
    {
        $this->priceCg = null !== $priceCg ? (float)$priceCg : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceBih()
    {
        return $this->priceBih;
    }

    /**
     * @param float $priceBih
     * @return $this
     */
    public function setPriceBih($priceBih)
    {
        $this->priceBih = null !== $priceBih ? (float)$priceBih : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = null !== $height ? (int)$height : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getSizes()
    {
        return $this->sizes;
    }

    /**
     * @param string $sizes
     * @return $this
     */
    public function setSizes($sizes)
    {
        $this->sizes = $sizes;

        return $this;
    }

    /**
     * @return string
     */
    public function getComposition()
    {
        return $this->composition;
    }

    /**
     * @param string $composition
     * @return $this
     */
    public function setComposition($composition)
    {
        $this->composition = $composition;

        return $this;
    }

    /**
     * @return string
     */
    public function getWashData()
    {
        return $this->washData;
    }

    /**
     * @param string $washData
     * @return $this
     */
    public function setWashData($washData)
    {
        $this->washData = $washData;

        return $this;
    }

}
