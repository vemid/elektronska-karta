<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;
use \Vemid\Entity\Repository\MaterialFactoryQuantityRepository;

/**
 * MaterialFactoryQuantity Model Class
 *
 * @Source('material_factory_quantities')
 *
 * @BelongsTo('materialFactoryId', 'MaterialFactory', 'id', {'alias':'MaterialFactory'})
 * @BelongsTo('seasonClassificationId', 'Classification', 'id', {'alias':'Classification'})
 */
class MaterialFactoryQuantity extends AbstractEntity
{
    const INCOME = 'INCOME';
    const OUTCOME = 'OUTCOME';

    const PROPERTY_ID = 'id';
    const PROPERTY_MATERIAL_FACTORY_ID = 'materialFactoryId';
    const PROPERTY_SEASON_CLASSIFICATION_ID = 'seasonClassificationId';
    const PROPERTY_DATETIME = 'datetime';
    const PROPERTY_AMOUNT = 'amount';
    const PROPERTY_WARRANT = 'warrant';
    const PROPERTY_NOTE = 'note';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_CREATED = 'created';

    const ALIAS_MATERIAL_FACTORY = 'MaterialFactory';
    const ALIAS_SEASON_CLASSIFICATION = 'Classification';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="material_factory_id", type="integer", nullable=false)
     * @FormElement(label="Material factory", type="Hidden", required=true, relation="MaterialFactory")
     */
    protected $materialFactoryId;

    /**
     * @Column(column="season_classification_id", type="integer", nullable=false)
     * @FormElement(label="Season", type="Select", required=true)
     */
    protected $seasonClassificationId;

    /**
     * @Column(column="datetime", type="date", nullable=false)
     * @FormElement(label="Datetime", type="Date", required=true)
     */
    protected $datetime;

    /**
     * @Column(column="amount", type="decimal", nullable=false)
     * @FormElement(label="Amount", type="Text", required=true)
     */
    protected $amount;

    /**
     * @Column(column="warrant", type="string", nullable=false)
     * @FormElement(label="Warrant", type="Text", required=true)
     */
    protected $warrant;

    /**
     * @Column(column="note", type="string", nullable=true)
     * @FormElement(label="Note", type="Text", required=false)
     */
    protected $note;

    /**
     * @Column(column="type", type="string", nullable=false)
     */
    protected $type;

    /**
     * @Column(column="created", type="dateTime", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_MATERIAL_FACTORY_ID => true,
        self::PROPERTY_DATETIME => true,
        self::PROPERTY_AMOUNT => true,
        self::PROPERTY_WARRANT => true,
        self::PROPERTY_NOTE => true,
        self::PROPERTY_TYPE => true,
        self::ALIAS_MATERIAL_FACTORY => true,
        self::PROPERTY_CREATED =>true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaterialFactoryId()
    {
        return $this->materialFactoryId;
    }

    /**
     * @param int $materialFactoryId
     * @return $this
     */
    public function setMaterialFactoryId($materialFactoryId)
    {
        $this->materialFactoryId = null !== $materialFactoryId ? (int)$materialFactoryId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getSeasonClassificationId()
    {
        return $this->seasonClassificationId;
    }

    /**
     * @param int $seasonClassificationId
     * @return $this
     */
    public function setSeasonClassificationId($seasonClassificationId)
    {
        $this->seasonClassificationId = null !== $seasonClassificationId ? (int)$seasonClassificationId : null;;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * @param string|DateTime $datetime
     * @return $this
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = null !== $amount ? (float)$amount : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getWarrant()
    {
        return $this->warrant;
    }

    /**
     * @param string $warrant
     * @return $this
     */
    public function setWarrant($warrant)
    {
        $this->warrant = $warrant;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string|DateTime $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|MaterialFactory
     *
     * @throws \DomainException
     */
    public function getMaterialFactory()
    {
        return $this->getRelated(self::ALIAS_MATERIAL_FACTORY);
    }

    /**
     * @param MaterialFactory $materialFactory
     * @return $this
     */
    public function setMaterialFactory(MaterialFactory $materialFactory)
    {
        $this->materialFactoryId = $materialFactory->getId();

        return $this;
    }

    /**
     * @return null|Classification
     *
     * @throws \DomainException
     */
    public function getSeasonClassification()
    {
        return $this->getRelated(self::ALIAS_SEASON_CLASSIFICATION);
    }

    /**
     * @param Classification $seasonClassification
     * @return $this
     */
    public function setSeasonClassification(Classification $seasonClassification)
    {
        $this->seasonClassificationId = $seasonClassification->getId();

        return $this;
    }

    public function validation()
    {
        /** @var MaterialFactoryQuantityRepository $materialFactoryRepository */
        $materialFactoryRepository = $this->getRepository(self::class);
        $total = $materialFactoryRepository->getTotal($this->getMaterialFactory());
        if ($this->getType() === self::OUTCOME && $total === 0) {
            $message = new \Phalcon\Mvc\Model\Message(
                'Stanje je na nuli. Nemoguće je skinuti sa stanja!',
                self::PROPERTY_TYPE,
                'InvalidValue'
            );

            $this->appendMessage($message);

            return false;
        }

        if ($this->getType() === self::OUTCOME && $total < $this->getAmount()) {
            $message = new \Phalcon\Mvc\Model\Message(
                sprintf('Maximalan iznos koji može da se skine sa stanja je: %s', $total),
                self::PROPERTY_TYPE,
                'InvalidValue'
            );

            $this->appendMessage($message);

            return false;
        }
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
