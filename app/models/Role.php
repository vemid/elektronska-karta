<?php

use Phalcon\Mvc\Model\Resultset;
use Vemid\Entity\AbstractEntity;

/**
 * Role Model Class
 *
 * @Source('roles')
 * @HasMany('id', 'UserRoleAssignment', 'roleId', {'alias':'UserRoleAssignments'})
 */
class Role extends AbstractEntity
{
    const ROLE_SUPER_ADMIN = 'SUPER_ADMIN';
    const ROLE_ADMIN = 'ADMIN';
    const ROLE_GUEST = 'GUEST';
    const ROLE_USER = 'USER';
    const ROLE_MEMBER = 'MEMBER';
    const ROLE_CLIENT = 'CLIENT';
    const ROLE_SHOP = 'SHOP';
    const ROLE_FACTORY = 'FACTORY';
    const ROLE_CENTRAL = 'CEBTRAL';

    const ROLE_GUEST_ID = 1;
    const ROLE_SUPER_ADMIN_ID = 2;
    const ROLE_ADMIN_ID = 3;
    const ROLE_USER_ID = 4;
    const ROLE_CLIENT_ID = 5;
    const ROLE_SHOP_ID = 6;
    const ROLE_FACTORY_ID = 7;
    const ROLE_CENTRAL_ID = 8;

    const PROPERTY_ID = 'id';
    const PROPERTY_CODE = 'code';
    const PROPERTY_NAME = 'name';
    const PROPERTY_DESCRIPTION = 'description';

    const ALIAS_USER_ROLE_ASSIGNMENTS = 'UserRoleAssignments';
    const ALIAS_USERS = 'Users';

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;
    /**
     * @Column(column="code", type="string", nullable=false)
     * @FormElement(label="Code", type="Text", required=true)
     */
    protected $code;
    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;
    /**
     * @Column(column="description", type="string", nullable=true)
     * @FormElement(label="Description", type="TextArea", required=false)
     */
    protected $description;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id === null ? null : (int)$this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id === null ? null : (int)$id;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return trim($this->getName());
    }

    /**
     * @param null|int|string|array $arguments
     * @return Resultset|UserRoleAssignment[]
     */
    public function getUserRoleAssignments($arguments = null)
    {
        return $this->getRelated(self::ALIAS_USER_ROLE_ASSIGNMENTS, $arguments);
    }

    /**
     * @param null|int|string|array $arguments
     * @return Resultset|User[]
     */
    public function getUsers($arguments = null)
    {
        return $this->getRelated(self::ALIAS_USERS, $arguments);
    }
}
