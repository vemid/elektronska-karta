<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * EFiscalItem Model Class
 *
 * @Source('e_fiscal_items')
 * @BelongsTo('eFiscalId', 'EFiscal', 'id', {'alias':'EFiscal'})
 */
class EFiscalItem extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_E_FISCAL_ID = 'eFiscalId';
    const PROPERTY_GTIN = 'gtin';
    const PROPERTY_NAME = 'name';
    const PROPERTY_QUANTITY = 'quantity';
    const PROPERTY_UNIT_PRICE = 'unitPrice';
    const ALIAS_E_FISCAL = 'EFiscal';


    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="e_fiscal_id", type="integer", nullable=false)
     * @FormElement(label="E fiscal", type="Text", required=true)
     */
    protected $eFiscalId;

    /**
     * @Column(column="gtin", type="string", nullable=true)
     * @FormElement(label="Gtin", type="Text", required=false)
     */
    protected $gtin;

    /**
     * @Column(column="name", type="string", nullable=true)
     * @FormElement(label="Name", type="Text", required=false)
     */
    protected $name;

    /**
     * @Column(column="quantity", type="decimal", nullable=false)
     * @FormElement(label="Quantity", type="Text", required=true)
     */
    protected $quantity;

    /**
     * @Column(column="unit_price", type="decimal", nullable=false)
     * @FormElement(label="Unit price", type="Text", required=true)
     */
    protected $unitPrice;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_E_FISCAL_ID => true,
        self::PROPERTY_GTIN => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_QUANTITY => true,
        self::PROPERTY_UNIT_PRICE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getEFiscalId()
    {
        return $this->eFiscalId;
    }

    /**
     * @param int $eFiscalId
     * @return $this
     */
    public function setEFiscalId($eFiscalId)
    {
        $this->eFiscalId = null !== $eFiscalId ? (int)$eFiscalId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getGtin()
    {
        return $this->gtin;
    }

    /**
     * @param string $gtin
     * @return $this
     */
    public function setGtin($gtin)
    {
        $this->gtin = $gtin;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = null !== $quantity ? (float)$quantity : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     * @return $this
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = null !== $unitPrice ? (float)$unitPrice : null;

        return $this;
    }

    /**
     * @return null|Code
     *
     * @throws \DomainException
     */
    public function getEFiscal()
    {
        return $this->getRelated(self::ALIAS_E_FISCAL);
    }

    /**
     * @param EFiscal $eFiscal
     * @return $this
     */
    public function setEFiskal(EFiscal $eFiscal)
    {
        $this->eFiscalId = $eFiscal->getId();

        return $this;
    }

}
