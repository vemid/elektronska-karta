<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;
use \Vemid\Task\Orders\AddClientOrderItemData;

/**
 * ClientOrderItem Model Class
 *
 * @Source('client_order_items')
 * @HasOne('id', 'OrderItem', 'clientOrderItemId', {'alias':'OrderItem'})
 * @BelongsTo('productSizeId', 'ProductSize', 'id', {'alias':'ProductSize'})
 * @BelongsTo('orderId', 'Order', 'id', {'alias':'Order'})
 */
class ClientOrderItem extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_ORDER_ID = 'orderId';
    const PROPERTY_PRODUCT_SIZE_ID = 'productSizeId';
    const PROPERTY_ENTITY_TYPE_ID = 'entityTypeId';
    const PROPERTY_ENTITY_ID = 'entityId';
    const PROPERTY_QUANTITY_A = 'quantityA';
    const PROPERTY_QUANTITY_B = 'quantityB';

    const ALIAS_ORDER = 'Order';
    const ALIAS_ORDER_ITEM = 'OrderItem';
    const ALIAS_PRODUCT_SIZE = 'ProductSize';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="order_id", type="integer", nullable=false)
     * @FormElement(label="Order", type="Text", required=true)
     */
    protected $orderId;

    /**
     * @Column(column="product_size_id", type="integer", nullable=false)
     * @FormElement(label="Product size", type="Text", required=true)
     */
    protected $productSizeId;

    /**
     * @Column(column="entity_type_id", type="integer", nullable=false)
     * @FormElement(label="Entity type", type="Text", required=true)
     */
    protected $entityTypeId;

    /**
     * @Column(column="entity_id", type="integer", nullable=false)
     * @FormElement(label="Entity", type="Text", required=true)
     */
    protected $entityId;

    /**
     * @Column(column="quantity_a", type="integer", nullable=false)
     * @FormElement(label="Quantity a", type="Text", required=true)
     */
    protected $quantityA;

    /**
     * @Column(column="quantity_b", type="integer", nullable=false)
     * @FormElement(label="Quantity b", type="Text", required=true)
     */
    protected $quantityB;

    /**
     * @Column(column="created", type="dateTime", nullable=true)
     */
    protected $created;

    /**
     * @Column(column="updated", type="dateTime", nullable=true)
     */
    protected $updated;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_ORDER_ID => true,
        self::PROPERTY_PRODUCT_SIZE_ID => true,
        self::PROPERTY_ENTITY_TYPE_ID => true,
        self::PROPERTY_ENTITY_ID => true,
        self::PROPERTY_QUANTITY_A => true,
        self::PROPERTY_QUANTITY_B => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     * @return $this
     */
    public function setOrderId($orderId)
    {
        $this->orderId = null !== $orderId ? (int)$orderId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductSizeId()
    {
        return $this->productSizeId;
    }

    /**
     * @param int $productSizeId
     * @return $this
     */
    public function setProductSizeId($productSizeId)
    {
        $this->productSizeId = null !== $productSizeId ? (int)$productSizeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getEntityTypeId()
    {
        return $this->entityTypeId;
    }

    /**
     * @param int $entityTypeId
     * @return $this
     */
    public function setEntityTypeId($entityTypeId)
    {
        $this->entityTypeId = null !== $entityTypeId ? (int)$entityTypeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        $this->entityId = null !== $entityId ? (int)$entityId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantityA()
    {
        return $this->quantityA;
    }

    /**
     * @param int $quantityA
     * @return $this
     */
    public function setQuantityA($quantityA)
    {
        $this->quantityA = null !== $quantityA ? (int)$quantityA : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantityB()
    {
        return $this->quantityB;
    }

    /**
     * @param int $quantityB
     * @return $this
     */
    public function setQuantityB($quantityB)
    {
        $this->quantityB = null !== $quantityB ? (int)$quantityB : null;

        return $this;
    }

    /**
     * @return null|\Vemid\Entity\EntityInterface|ProductSize
     *
     * @throws \DomainException
     */
    public function getProductSize()
    {
        return $this->getRelated(self::ALIAS_PRODUCT_SIZE);
    }

    /**
     * @param ProductSize $productSize
     * @return $this
     */
    public function setProductSize(ProductSize $productSize)
    {
        $this->productSizeId = $productSize->getId();

        return $this;
    }

    /**
     * @return null|\Vemid\Entity\EntityInterface|Client|Code
     */
    public function getClient()
    {
        return \Vemid\Entity\Type::getEntity($this->getEntityTypeId(),$this->getEntityId());
    }

    /**
     * @param \Vemid\Entity\EntityInterface $entity
     * @return $this
     */
    public function setClient(\Vemid\Entity\EntityInterface $entity)
    {
        $this->entityTypeId = $entity->getEntityTypeId();
        $this->entityId = $entity->getEntityId();

        return $this;
    }

    /**
     * @return null|\Vemid\Entity\EntityInterface|Order
     *
     * @throws \DomainException
     */
    public function getOrder()
    {
        return $this->getRelated(self::ALIAS_ORDER);
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function setOrder(Order $order)
    {
        $this->orderId = $order->getId();

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string|DateTime $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param string|DateTime $updated
     * @return $this
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|OrderItem
     *
     * @throws \DomainException
     */
    public function getOrderItem($arguments = null)
    {
        return $this->getRelated(self::ALIAS_ORDER_ITEM, $arguments);
    }

    public function beforeCreate()
    {
        $date = new Vemid\Date\DateTime();
        $this->created = $date->getUnixFormat();
    }

    public function afterCreate()
    {
        if ($this->getOrder()->getActive()) {
            $data = $this->toArray();
            $data[OrderItem::PROPERTY_CLIENT_ORDER_ITEM_ID] = $this->getId();
            unset($data['id']);

//            $orderItem = new OrderItem($data);
//            $orderItem->save();
            $shopTask = new AddClientOrderItemData();
            $shopTask->clientOrderItem = $this;
            $shopTask->run();
        }
    }

    public function beforeUpdate()
    {
        $date = new Vemid\Date\DateTime();

        if ($this->getOrder()->getActive()) {
            if ($this->hasUpdated(self::PROPERTY_QUANTITY_A) || $this->hasUpdated(self::PROPERTY_QUANTITY_B)) {
                $this->updated = $date->getUnixFormat();
                /** @var OrderItem $orderItem */
                $orderItem = $this->getOrderItem();
                $shopTask = new AddClientOrderItemData();
                $shopTask->clientOrderItem = $this;
                $shopTask->run();
//                if ($orderItem) {
//                    $orderItem->setQuantityA($this->quantityA);
//                    $orderItem->setQuantityB($this->quantityB);
//                    $orderItem->save();
//                }
            }
        }
    }
}
