<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * EmailNotificationTemplate Model Class
 *
 * @Source('email_notification_templates')
 */
class EmailNotificationTemplate extends AbstractEntity
{
    const ONE_HOUR = 'ONE_HOUR';
    const TWO_HOURS = 'TWO_HOURS';
    const SIX_HOURS = 'SIX_HOURS';
    const TWELVE_HOURS = 'TWELVE_HOURS';
    const TWENTY_FOUR_HOURS = 'TWENTY_FOUR_HOURS';

    const PROPERTY_ID = 'id';
    const PROPERTY_ENTITY_OBJECT_NAME = 'entityObjectName';
    const PROPERTY_CONTROLLER_NAME = 'controllerName';
    const PROPERTY_ACTION_NAME = 'actionName';
    const PROPERTY_FROM_ADDRESS = 'fromAddress';
    const PROPERTY_SUBJECT = 'subject';
    const PROPERTY_BODY = 'body';
    const PROPERTY_PROLONG_OPTION = 'prolongOption';


    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="entity_object_name", type="string", nullable=true)
     * @FormElement(label="Entity name", type="Text", required=false)
     */
    protected $entityObjectName;

    /**
     * @Column(column="controller_name", type="string", nullable=false)
     * @FormElement(label="Controller name", type="Text", required=true)
     */
    protected $controllerName;

    /**
     * @Column(column="action_name", type="string", nullable=false)
     * @FormElement(label="Action name", type="Text", required=true)
     */
    protected $actionName;

    /**
     * @Column(column="from_address", type="string", nullable=false)
     * @FormElement(label="From address", type="Text", required=true)
     */
    protected $fromAddress;

    /**
     * @Column(column="subject", type="string", nullable=false)
     * @FormElement(label="Subject", type="Text", required=true)
     */
    protected $subject;

    /**
     * @Column(column="body", type="string", nullable=true)
     * @FormElement(label="Body", type="Text", required=false)
     */
    protected $body;

    /**
     * @Column(column="prolong_option", type="string", nullable=true)
     * @FormElement(label="Prolong option", type="Text", required=false, options={'ONE_HOUR' : '1 Hour','TWO_HOURS' : '2 Hours', 'SIX_HOURS' : '6 Hours', 'TWELVE_HOURS' : '12 Hours','TWENTY_FOUR_HOURS' : '24 Hours'})
     */
    protected $prolongOption;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_ENTITY_OBJECT_NAME => true,
        self::PROPERTY_CONTROLLER_NAME => true,
        self::PROPERTY_ACTION_NAME => true,
        self::PROPERTY_FROM_ADDRESS => true,
        self::PROPERTY_SUBJECT => true,
        self::PROPERTY_BODY => true,
        self::PROPERTY_PROLONG_OPTION => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityObjectName()
    {
        return $this->entityObjectName;
    }

    /**
     * @param string $entityObjectName
     * @return $this
     */
    public function setEntityObjectName($entityObjectName)
    {
        $this->entityObjectName = $entityObjectName;

        return $this;
    }

    /**
     * @return string
     */
    public function getControllerName()
    {
        return $this->controllerName;
    }

    /**
     * @param string $controllerName
     * @return $this
     */
    public function setControllerName($controllerName)
    {
        $this->controllerName = $controllerName;

        return $this;
    }

    /**
     * @return string
     */
    public function getActionName()
    {
        return $this->actionName;
    }

    /**
     * @param string $actionName
     * @return $this
     */
    public function setActionName($actionName)
    {
        $this->actionName = $actionName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFromAddress()
    {
        return $this->fromAddress;
    }

    /**
     * @param string $fromAddress
     * @return $this
     */
    public function setFromAddress($fromAddress)
    {
        $this->fromAddress = $fromAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return string
     */
    public function getProlongOption()
    {
        return $this->prolongOption;
    }

    /**
     * @param string $prolongOption
     * @return $this
     */
    public function setProlongOption($prolongOption)
    {
        $this->prolongOption = $prolongOption;

        return $this;
    }

}
