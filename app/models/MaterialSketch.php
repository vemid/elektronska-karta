<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * MaterialSketch Model Class
 *
 * @Source('material_sketches')
 *
 * @BelongsTo('classificationCodeId', 'Classification', 'id', {'alias':'ClassificationCode'})
 * @BelongsTo('supplierId', 'Supplier', 'id', {'alias':'Supplier'})
 */
class MaterialSketch extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_SUPPLIER_ID = 'supplierId';
    const PROPERTY_SUPPLIER_NAME = 'supplierName';
    const PROPERTY_CLASSIFICATION_CODE_ID = 'classificationCodeId';
    const PROPERTY_ARTICLE_CODE = 'articleCode';
    const PROPERTY_ARTICLE_NAME = 'articleName';
    const PROPERTY_COLOR = 'color';
    const PROPERTY_GRAMS = 'grams';
    const PROPERTY_NOTE = 'note';

    const ALIAS_SUPPLIER = 'Supplier';
    const ALIAS_CLASSIFICATION_CODE = 'ClassificationCode';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="classification_code_id", type="integer", nullable=true)
     * @FormElement(label="Classification code", type="Text", required=false)
     */
    protected $classificationCodeId;

    /**
     * @Column(column="supplier_id", type="integer", nullable=true)
     * @FormElement(label="Supplier", type="Hidden", required=false, relation="Supplier")
     */
    protected $supplierId;

    /**
     * @Column(column="supplier_name", type="string", nullable=false)
     * @FormElement(label="Supplier name", type="Text", required=true)
     */
    protected $supplierName;

    /**
     * @Column(column="article_code", type="string", nullable=true)
     * @FormElement(label="Article code", type="Text", required=false)
     */
    protected $articleCode;

    /**
     * @Column(column="article_name", type="string", nullable=true)
     * @FormElement(label="Article name", type="Text", required=false)
     */
    protected $articleName;

    /**
     * @Column(column="color", type="string", nullable=true)
     * @FormElement(label="Color", type="Text", required=false)
     */
    protected $color;

    /**
     * @Column(column="grams", type="string", nullable=true)
     * @FormElement(label="Grams", type="Text", required=false)
     */
    protected $grams;

    /**
     * @Column(column="note", type="string", nullable=true)
     * @FormElement(label="Note", type="Text", required=false)
     */
    protected $note;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_SUPPLIER_ID => true,
        self::PROPERTY_SUPPLIER_NAME => true,
        self::PROPERTY_CLASSIFICATION_CODE_ID => true,
        self::PROPERTY_ARTICLE_CODE => true,
        self::PROPERTY_ARTICLE_NAME => true,
        self::PROPERTY_COLOR => true,
        self::PROPERTY_GRAMS => true,
        self::PROPERTY_NOTE => true,
        self::ALIAS_SUPPLIER => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplierId
     * @return $this
     */
    public function setSupplierId($supplierId)
    {
        $this->supplierId = null !== $supplierId ? (int)$supplierId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getSupplierName()
    {
        return $this->supplierName;
    }

    /**
     * @param string $supplierName
     * @return $this
     */
    public function setSupplierName($supplierName)
    {
        $this->supplierName = $supplierName;

        return $this;
    }

    /**
     * @return int
     */
    public function getClassificationCodeId()
    {
        return $this->classificationCodeId;
    }

    /**
     * @param int $classificationCodeId
     * @return $this
     */
    public function setClassificationCodeId($classificationCodeId)
    {
        $this->classificationCodeId = null !== $classificationCodeId ? (int)$classificationCodeId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getArticleCode()
    {
        return $this->articleCode;
    }

    /**
     * @param string $articleCode
     * @return $this
     */
    public function setArticleCode($articleCode)
    {
        $this->articleCode = $articleCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getArticleName()
    {
        return $this->articleName;
    }

    /**
     * @param string $articleName
     * @return $this
     */
    public function setArticleName($articleName)
    {
        $this->articleName = $articleName;

        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return $this
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return string
     */
    public function getGrams()
    {
        return $this->grams;
    }

    /**
     * @return string
     */
    public function getGramUnits()
    {
        return $this->grams !== null ? $this->grams ."gr" : null;
    }

    /**
     * @param string $grams
     * @return $this
     */
    public function setGrams($grams)
    {
        $this->grams = $grams;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return null|Classification
     *
     * @throws \DomainException
     */
    public function getClassificationCode()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION_CODE);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setClassificationCode(Classification $classification)
    {
        $this->classificationCodeId = $classification->getId();

        return $this;
    }

    /**
     * @return null|Supplier
     * 
     * @throws \DomainException
     */
    public function getSupplier()
    {
        return $this->getRelated(self::ALIAS_SUPPLIER);
    }

    /**
     * @param Supplier $supplier
     * @return $this
     */
    public function setSupplier(Supplier $supplier)
    {
        $this->supplierId = $supplier->getId();

        return $this;
    }

    public function getDisplayName()
    {
        return
             ($this->getClassificationCode()  ? $this->getClassificationCode()->getParentClassification() : null )
            .($this->getClassificationCode() ? "-". $this->getClassificationCode() : null)
            .($this->getSupplierName() ? "-". $this->getSupplierName() : null)
            .($this->getArticleCode() ? '-'.$this->getArticleCode() : null)
            .($this->getArticleName() ? '-'.$this->getArticleName() : null)
            .($this->getGramUnits() ? '-'.$this->getGramUnits() : null)
            .($this->getColor() ? '-'.$this->getColor() : null);
    }

}
