<?php

use Vemid\Entity\AbstractEntity;

/**
 * Classification Model Class
 *
 * @Source('classifications')
 *
 * @HasMany('id', 'Classification', 'parentClassificationId', {'alias':'ChildClassifications'})
 * @HasMany('id', 'ClassificationSize', 'classificationId', {'alias':'ClassificationSizes'})
 * @HasMany('id', 'OrderClassification', 'classificationId', {'alias':'OrderClassifications'})
 * @HasMany('id', 'MaterialFactoryQuantity', 'seasonClassificationId', {'alias':'SeasonClassificationsFactoryItem'})
 * @HasMany('id', 'MaterialFactory', 'seasonClassificationId', {'alias':'SeasonClassificationsFactory'})
 * @HasMany('id', 'PriorityClassification', 'classificationId', {'alias':'PriorityClassifications'})
 *
 * @BelongsTo('classificationTypeCodeId', 'Code', 'id', {'alias':'ClassificationType'})
 * @BelongsTo('classificationCategoryCodeId', 'Code', 'id', {'alias':'ClassificationCategory'})
 * @BelongsTo('parentClassificationId', 'Classification', 'id', {'alias':'ParentClassification'})
 */
class Classification extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CLASSIFICATION_TYPE_CODE_ID = 'classificationTypeCodeId';
    const PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID = 'classificationCategoryCodeId';
    const PROPERTY_PARENT_CLASSIFICATION_ID = 'parentClassificationId';
    const PROPERTY_CODE = 'code';
    const PROPERTY_NAME = 'name';

    const ALIAS_CLASSIFICATION_TYPE = 'ClassificationType';
    const ALIAS_CLASSIFICATION_CATEGORY = 'ClassificationCategory';
    const ALIAS_PARENT_CLASSIFICATION = 'ParentClassification';
    const ALIAS_CHILD_CLASSIFICATIONS = 'ChildClassifications';
    const ALIAS_CLASSIFICATION_SIZES = 'ClassificationSizes';
    const ALIAS_ORDER_CLASSIFICATION = 'OrderClassifications';
    const ALIAS_SEASON_CLASSIFICATION_FACTORY_ITEM = 'SeasonClassificationsFactoryItem';
    const ALIAS_SEASON_CLASSIFICATION_FACTORY = 'SeasonClassificationsFactory';
    const ALIAS_PRIORITY_CLASSIFICATIONS = 'PriorityClassifications';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="classification_type_code_id", type="integer", nullable=false)
     * @FormElement(label="Classification Type", type="Select", required=true)
     */
    protected $classificationTypeCodeId;

    /**
     * @Column(column="classification_category_code_id", type="integer", nullable=false)
     * @FormElement(label="Classification Category", type="Select", required=true)
     */
    protected $classificationCategoryCodeId;

    /**
     * @Column(column="parent_classification_id", type="integer", nullable=true)
     * @FormElement(label="Parent classification", type="Select", required=false, relation="Classification")
     */
    protected $parentClassificationId;

    /**
     * @Column(column="code", type="string", nullable=false)
     * @FormElement(label="Classification", type="Text", required=true)
     */
    protected $code;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CLASSIFICATION_TYPE_CODE_ID => true,
        self::PROPERTY_CLASSIFICATION_CATEGORY_CODE_ID => true,
        self::PROPERTY_PARENT_CLASSIFICATION_ID => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_CODE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getClassificationTypeCodeId()
    {
        return $this->classificationTypeCodeId;
    }

    /**
     * @param int $classificationTypeCodeId
     * @return $this
     */
    public function setClassificationTypeCodeId($classificationTypeCodeId)
    {
        $this->classificationTypeCodeId = null !== $classificationTypeCodeId ? (int)$classificationTypeCodeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getClassificationCategoryCodeId()
    {
        return $this->classificationCategoryCodeId;
    }

    /**
     * @param int $classificationCategoryCodeId
     * @return $this
     */
    public function setClassificationCategoryCodeId($classificationCategoryCodeId)
    {
        $this->classificationCategoryCodeId = null !== $classificationCategoryCodeId ? (int)$classificationCategoryCodeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getParentClassificationId()
    {
        return $this->parentClassificationId;
    }

    /**
     * @param int $parentClassificationId
     * @return $this
     */
    public function setParentClassificationId($parentClassificationId)
    {
        $this->parentClassificationId = null !== $parentClassificationId ? (int)$parentClassificationId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return null|Code
     * 
     * @throws \DomainException
     */
    public function getClassificationType()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION_TYPE);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setClassificationType(Code $code)
    {
        $this->classificationTypeCodeId = $code->getId();

        return $this;
    }

    /**
     * @return null|Code
     *
     * @throws \DomainException
     */
    public function getClassificationCategory()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION_CATEGORY);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setClassificationCategory(Code $code)
    {
        $this->classificationCategoryCodeId = $code->getId();

        return $this;
    }

    /**
     * @return self|\Vemid\Entity\EntityInterface
     *
     * @throws \DomainException
     */
    public function getParentClassification()
    {
        return $this->getRelated(self::ALIAS_PARENT_CLASSIFICATION);
    }

    /**
     * @param self $classification
     * @return $this
     */
    public function setParentClassification(self $classification)
    {
        $this->parentClassificationId = $classification->getId();

        return $this;
    }

    /**
     * @param null $arguments
     * @return Classification[]|\Vemid\Entity\EntityInterface[]
     *
     * @throws \DomainException
     */
    public function getChildClassifications($arguments = null)
    {
        return $this->getRelated(self::ALIAS_CHILD_CLASSIFICATIONS, $arguments);
    }

    /**
     * @param null $arguments
     * @return ClassificationSize[]|\Vemid\Entity\EntityInterface[]
     *
     * @throws \DomainException
     */
    public function getClassificationSizes($arguments = null)
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION_SIZES, $arguments);
    }

    /**
     * @param null $arguments
     * @return OrderClassification[]|\Vemid\Entity\EntityInterface[]
     *
     * @throws \DomainException
     */
    public function getOrderClassifications($arguments = null)
    {
        return $this->getRelated(self::ALIAS_ORDER_CLASSIFICATION, $arguments);
    }



    /**
     * @param null $arguments
     * @return MaterialFactoryQuantity[]|\Vemid\Entity\EntityInterface[]
     *
     * @throws \DomainException
     */
    public function getSeasonClassificationFactoryItem($arguments = null)
    {
        return $this->getRelated(self::ALIAS_SEASON_CLASSIFICATION_FACTORY_ITEM, $arguments);
    }

    /**
     * @param null $arguments
     * @return MaterialFactoryQuantity[]|\Vemid\Entity\EntityInterface[]
     *
     * @throws \DomainException
     */
    public function getSeasonClassificationFactory($arguments = null)
    {
        return $this->getRelated(self::ALIAS_SEASON_CLASSIFICATION_FACTORY, $arguments);
    }

    /**
     * @param null $arguments
     * @return PriorityClassification[]|\Vemid\Entity\EntityInterface[]
     *
     * @throws \DomainException
     */
    public function getPriorityClassifications($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRIORITY_CLASSIFICATIONS, $arguments);
    }

    public function getDisplayName()
    {
        return $this->getName();
    }

    public function getShortColorName()
    {

        $nameSegments = explode(' ', $this->getName() ? $this->getName() : []);
        return count($nameSegments) ? array_pop($nameSegments) : '';
    }
}
