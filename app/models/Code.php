<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * Code Model Class
 *
 * @Source('codes')
 *
 * @BelongsTo('codeTypeId', 'CodeType', 'id')
 *
 * @HasMany('id', 'Attribute', 'attributeCodeId', {'alias':'Attributes'})
 * @HasMany('id', 'Classification', 'classificationTypeCodeId', {'alias':'Classifications'})
 * @HasMany('id', 'Classification', 'classificationCategoryCodeId', {'alias':'ClassificationCategories'})
 * @HasMany('id', 'ProductionWorker', 'codeId', {'alias':'ProductionWorkers'})
 * @HasMany('id', 'ProductionWorker', 'locationCodeId', {'alias':'ProductionWorkerLocations'})
 * @HasMany('id', 'Failure', 'shopCodeId', {'alias':'FailureShopCode'})
 * @HasMany('id', 'Failure', 'failureCodeId', {'alias':'FailureCode'})
 * @HasMany('id', 'ProductSize', 'productSizeCodeId', {'alias':'ProductSize'})
 *
 * @HasMany('id', 'Dealership', 'codeId', {'alias':'Dealership'})
 */
class Code extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CODE_TYPE_ID = 'codeTypeId';
    const PROPERTY_CODE = 'code';
    const PROPERTY_BARCODE = 'barcode';
    const PROPERTY_NAME = 'name';

    const ALIAS_CODE_TYPE = 'CodeType';
    const ALIAS_CLASSIFICATIONS = 'Classifications';
    const ALIAS_CLASSIFICATION_CATEGORIES = 'ClassificationCategories';
    const ALIAS_ATTRIBUTES = 'Attributes';
    const ALIAS_DEALERSHIP = 'Dealership';
    const ALIAS_DEALERSHIP_WARRANTS = 'DealershipWarrants';
    const ALIAS_PRODUCTION_WORKERS = 'ProductionWorkers';
    const ALIAS_PRODUCTION_WORKER_LOCATIONS = 'ProductionWorkerLocations';
    const ALIAS_FAILURE_SHOP_CODE = 'FailureShopCode';
    const ALIAS_FAILURE_CODE = 'FailureCode';
    const ALIAS_FAILURE = 'Failure';
    const ALIAS_PRODUCT_SIZES = 'ProductSize';

    const SEASON = '05';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="code_type_id", type="integer", nullable=false)
     * @FormElement(label="Code type", type="Select", required=true, relation="CodeType")
     */
    protected $codeTypeId;

    /**
     * @Column(column="code", type="string", nullable=false)
     * @FormElement(label="Code", type="Text", required=true)
     */
    protected $code;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CODE_TYPE_ID => true,
        self::PROPERTY_CODE => true,
        self::ALIAS_CODE_TYPE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return null !== $this->id ? (int)$this->id : null;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getCodeTypeId()
    {
        return $this->codeTypeId;
    }

    /**
     * @param int $codeTypeId
     * @return $this
     */
    public function setCodeTypeId($codeTypeId)
    {
        $this->codeTypeId = null !== $codeTypeId ? (int)$codeTypeId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null|CodeType
     *
     * @throws DomainException
     */
    public function getCodeType()
    {
        return $this->getRelated(self::ALIAS_CODE_TYPE);
    }

    /**
     * @param CodeType $codeType
     * @return $this
     */
    public function setCodeType(CodeType $codeType)
    {
        $this->codeTypeId = $codeType->getId();

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|Classification[]
     *
     * @throws DomainException
     */
    public function getClassifications($arguments = null)
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATIONS, $arguments);
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|Classification[]
     *
     * @throws DomainException
     */
    public function getClassificationCategories($arguments = null)
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION_CATEGORIES, $arguments);
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|Dealership
     *
     * @throws DomainException
     */
    public function getDealership()
    {
        return $this->getRelated(self::ALIAS_DEALERSHIP);
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|DealershipWarrant[]
     *
     * @throws DomainException
     */
    public function getDealershipWarrants($arguments = null)
    {
        return $this->getRelated(self::ALIAS_DEALERSHIP_WARRANTS, $arguments);
    }

    /**
     * @param array|null $arguments
     * @return ProductionWorker[]|Resultset
     *
     * @throws DomainException
     */
    public function getProductionWorkers($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRODUCTION_WORKERS, $arguments);
    }

    /**
     * @param array|null $arguments
     * @return ProductionWorker[]|Resultset
     *
     * @throws DomainException
     */
    public function getProductionWorkerLocations($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRODUCTION_WORKER_LOCATIONS, $arguments);
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|Attribute
     *
     * @throws DomainException
     */
    public function getAttributes($arguments = null)
    {
        return $this->getRelated(self::ALIAS_ATTRIBUTES, $arguments);
    }

    public function getDisplayName()
    {
        return $this->name;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|ProductSize[]
     *
     * @throws DomainException
     */
    public function getProductSizes($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRODUCT_SIZES, $arguments);
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        return true;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
