<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use \Vemid\Entity\EntityInterface;
use \Phalcon\Mvc\Model\Behavior\Timestampable;

/**
 * UserNotificationMessage Model Class
 *
 * @Source('user_notification_messages')
 *
 * @BelongsTo('userNotificationId', 'UserNotification', 'id', {'alias':'UserNotification'})
 */
class UserNotificationMessage extends AbstractEntity
{
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_DEACTIVATED = 'DEACTIVATED';
    const STATUS_FINISHED = 'FINISHED';
    const STATUS_POSTPONED_ONE_DAY= 'POSTPONED_ONE_DAY';
    const STATUS_POSTPONED_TWO_DAYS = 'POSTPONED_TWO_DAYS';
    const STATUS_POSTPONED_THREE_DAYS = 'POSTPONED_THREE_DAYS';
    const STATUS_POSTPONED_FIVE_DAYS = 'POSTPONED_FIVE_DAYS';
    const STATUS_POSTPONED_TEN_DAYS = 'POSTPONED_TEN_DAYS';

    const PROPERTY_ID = 'id';
    const PROPERTY_USER_NOTIFICATION_ID = 'userNotificationId';
    const PROPERTY_ENTITY_TYPE_ID = 'entityTypeId';
    const PROPERTY_ENTITY_ID = 'entityId';
    const PROPERTY_MESSAGE = 'message';
    const PROPERTY_STATUS = 'status';
    const PROPERTY_DATE_CREATED = 'dateCreated';
    const PROPERTY_POSTPONED_DATE = 'postponedDate';
    const PROPERTY_DUE_DATE = 'dueDate';
    const PROPERTY_DEACTIVATED_DATE = 'deactivatedDate';

    const ALIAS_USER_NOTIFICATION = 'UserNotification';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="user_notification_id", type="integer", nullable=false)
     * @FormElement(label="User notification", type="Select", required=true, relation="UserNotification")
     */
    protected $userNotificationId;

    /**
     * @Column(column="entity_type_id", type="integer", nullable=false)
     * @FormElement(label="Entity type", type="Text", required=true)
     */
    protected $entityTypeId;

    /**
     * @Column(column="entity_id", type="integer", nullable=false)
     * @FormElement(label="Entity", type="Text", required=true)
     */
    protected $entityId;

    /**
     * @Column(column="message", type="string", nullable=true)
     * @FormElement(label="Entity", type="Text", required=false)
     */
    protected $message;

    /**
     * @Column(column="date_created", type="dateTime", nullable=false)
     * @FormElement(label="Date created", type="Date", required=true)
     */
    protected $dateCreated;

    /**
     * @Column(column="postponed_date", type="dateTime", nullable=true)
     * @FormElement(label="Postponed date", type="Date", required=false)
     */
    protected $postponedDate;

    /**
     * @Column(column="due_date", type="dateTime", nullable=true)
     * @FormElement(label="Due date", type="Date", required=false)
     */
    protected $dueDate;

    /**
     * @Column(column="deactivated_date", type="dateTime", nullable=true)
     * @FormElement(label="Deactivated date", type="Date", required=false)
     */
    protected $deactivatedDate;

    /**
     * @Column(column="status", type="string", nullable=false)
     * @FormElement(label="Status", type="Text", required=true)
     */
    protected $status;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_USER_NOTIFICATION_ID => true,
        self::PROPERTY_ENTITY_TYPE_ID => true,
        self::PROPERTY_ENTITY_ID => true,
        self::PROPERTY_DATE_CREATED => true,
        self::PROPERTY_POSTPONED_DATE => true,
        self::PROPERTY_STATUS => true,
        self::ALIAS_USER_NOTIFICATION => true,
    );

    public function initialize()
    {
        parent::initialize();

        $this->addBehavior(new Timestampable([
            'beforeValidationOnCreate' => [
                'field'  => self::PROPERTY_DATE_CREATED,
                'format' => DateTime::ISO_8601_NO_TIMEZONE,
            ]
        ]));
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserNotificationId()
    {
        return $this->userNotificationId;
    }

    /**
     * @param int $userNotificationId
     * @return $this
     */
    public function setUserNotificationId($userNotificationId)
    {
        $this->userNotificationId = null !== $userNotificationId ? (int)$userNotificationId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityTypeId()
    {
        return $this->entityTypeId;
    }

    /**
     * @param string $entityTypeId
     * @return $this
     */
    public function setEntityTypeId($entityTypeId)
    {
        $this->entityTypeId = $entityTypeId;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param string $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @param EntityInterface $entity
     * @return $this
     */
    public function setEntity(EntityInterface $entity)
    {
        $this->entityTypeId = $entity->getEntityTypeId();
        $this->entityId = $entity->getEntityId();

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param string|DateTime $dateCreated
     * @return $this
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getPostponedDate()
    {
        return $this->postponedDate;
    }

    /**
     * @param string|DateTime $postponedDate
     * @return $this
     */
    public function setPostponedDate($postponedDate)
    {
        $this->postponedDate = $postponedDate;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * @param string|DateTime $dueDate
     * @return $this
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDeactivatedDate()
    {
        return $this->deactivatedDate;
    }

    /**
     * @param DateTime|null $deactivatedDate
     * @return $this
     */
    public function setDeactivatedDate($deactivatedDate)
    {
        $this->deactivatedDate = $deactivatedDate;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return null|UserNotification|EntityInterface
     * 
     * @throws \DomainException
     */
    public function getUserNotification()
    {
        return $this->getRelated(self::ALIAS_USER_NOTIFICATION);
    }

    /**
     * @param UserNotification $userNotification
     * @return $this
     */
    public function setUserNotification(UserNotification $userNotification)
    {
        $this->userNotificationId = $userNotification->getId();

        return $this;
    }

    public function beforeValidationOnCreate()
    {
        $this->setStatus(self::STATUS_ACTIVE);
    }

    /**
     * @return bool
     */
    public function isOverDue()
    {
        $today = new DateTime();

        if ($this->getDueDate() && $this->getDueDate() <= $today && !$this->isFinished()) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isGonnaBeOverDue()
    {
        $today = new DateTime('-1 day');

        if ($this->getDueDate() && $this->getDueDate() >= $today && !$this->isFinished()) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isPostponed()
    {
        if (stripos('postpon', strtolower($this->getStatus())) !== false) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isFinished()
    {
        return $this->getStatus() === self::STATUS_FINISHED;
    }

    public function isActive()
    {
        return $this->getStatus() === self::STATUS_ACTIVE;
    }
}
