<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * PrintLabelItem Model Class
 *
 * @Source('print_label_items')
 *
 * @BelongsTo('productSizeId', 'ProductSize', 'id', {'alias':'ProductSize'})
 */
class PrintLabelItem extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PRODUCT_SIZE_ID = 'productSizeId';
    const PROPERTY_QTY = 'qty';
    const PROPERTY_PRICE_SRB = 'priceSrb';
    const PROPERTY_PRICE_CG = 'priceCg';
    const PROPERTY_PRICE_BIH = 'priceBih';
    const PROPERTY_HEIGHT = 'height';
    const PROPERTY_SIZES = 'sizes';
    const PROPERTY_COMPOSITION = 'composition';
    const PROPERTY_WASH_DATA = 'washData';
    const PROPERTY_PRINTED_DATE = 'printedDate';
    const PROPERTY_PRINTED = 'printed';
    const PROPERTY_CREATED = 'created';

    const ALIAS_PRODUCT_SIZE = 'ProductSize';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="product_size_id", type="integer", nullable=false)
     * @FormElement(label="Product size", type="Select", required=true, relation="ProductSize")
     */
    protected $productSizeId;

    /**
     * @Column(column="qty", type="integer", nullable=false)
     * @FormElement(label="Qty", type="Text", required=true)
     */
    protected $qty;

    /**
     * @Column(column="price_srb", type="decimal", nullable=false)
     * @FormElement(label="Price srb", type="Text", required=true)
     */
    protected $priceSrb;

    /**
     * @Column(column="price_cg", type="decimal", nullable=false)
     * @FormElement(label="Price cg", type="Text", required=true)
     */
    protected $priceCg;

    /**
     * @Column(column="price_bih", type="decimal", nullable=false)
     * @FormElement(label="Price bih", type="Text", required=true)
     */
    protected $priceBih;

    /**
     * @Column(column="height", type="integer", nullable=false)
     * @FormElement(label="Height", type="Text", required=true)
     */
    protected $height;

    /**
     * @Column(column="sizes", type="string", nullable=true)
     * @FormElement(label="Sizes", type="Text", required=false)
     */
    protected $sizes;

    /**
     * @Column(column="composition", type="string", nullable=false)
     * @FormElement(label="Composition", type="Text", required=true)
     */
    protected $composition;

    /**
     * @Column(column="wash_data", type="string", nullable=false)
     * @FormElement(label="Composition", type="Text", required=false)
     */
    protected $washData;

    /**
     * @Column(column="printed_date", type="string", nullable=true)
     */
    protected $printedDate;

    /**
     * @Column(column="printed", type="integer", nullable=true)
     */
    protected $printed;

    /**
     * @Column(column="created", type="string", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRODUCT_SIZE_ID => true,
        self::PROPERTY_QTY => true,
        self::PROPERTY_PRICE_SRB => true,
        self::PROPERTY_PRICE_CG => true,
        self::PROPERTY_PRICE_BIH => true,
        self::PROPERTY_HEIGHT => true,
        self::PROPERTY_SIZES => true,
        self::PROPERTY_COMPOSITION => true,
        self::PROPERTY_PRINTED_DATE => true,
        self::PROPERTY_PRINTED => true,
        self::PROPERTY_CREATED => true,
        self::ALIAS_PRODUCT_SIZE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductSizeId()
    {
        return $this->productSizeId;
    }

    /**
     * @param int $productSizeId
     * @return $this
     */
    public function setProductSizeId($productSizeId)
    {
        $this->productSizeId = null !== $productSizeId ? (int)$productSizeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param int $qty
     * @return $this
     */
    public function setQty($qty)
    {
        $this->qty = null !== $qty ? (int)$qty : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceSrb()
    {
        return $this->priceSrb;
    }

    /**
     * @param float $priceSrb
     * @return $this
     */
    public function setPriceSrb($priceSrb)
    {
        $this->priceSrb = null !== $priceSrb ? (float)$priceSrb : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceCg()
    {
        return $this->priceCg;
    }

    /**
     * @param float $priceCg
     * @return $this
     */
    public function setPriceCg($priceCg)
    {
        $this->priceCg = null !== $priceCg ? (float)$priceCg : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceBih()
    {
        return $this->priceBih;
    }

    /**
     * @param float $priceBih
     * @return $this
     */
    public function setPriceBih($priceBih)
    {
        $this->priceBih = null !== $priceBih ? (float)$priceBih : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = null !== $height ? (int)$height : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getSizes()
    {
        return $this->sizes;
    }

    /**
     * @param string $sizes
     * @return $this
     */
    public function setSizes($sizes)
    {
        $this->sizes = $sizes;

        return $this;
    }

    /**
     * @return string
     */
    public function getComposition()
    {
        return $this->composition;
    }

    /**
     * @param string $composition
     * @return $this
     */
    public function setComposition($composition)
    {
        $this->composition = $composition;

        return $this;
    }

    /**
     * @return string
     */
    public function getWashData()
    {
        return $this->washData;
    }

    /**
     * @param string $washData
     * @return $this
     */
    public function setWashData($washData)
    {
        $this->washData = $washData;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrintedDate()
    {
        return $this->printedDate;
    }

    /**
     * @param string $printedDate
     * @return $this
     */
    public function setPrintedDate($printedDate)
    {
        $this->printedDate = $printedDate;

        return $this;
    }

    /**
     * @return bool
     */
    public function getPrinted()
    {
        return (bool)$this->printed;
    }

    /**
     * @param int|bool $printed
     * @return $this
     */
    public function setPrinted($printed)
    {
        $this->printed = $printed ? 1 : 0;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|ProductSize
     * 
     * @throws \DomainException
     */
    public function getProductSize()
    {
        return $this->getRelated(self::ALIAS_PRODUCT_SIZE);
    }

    /**
     * @param ProductSize $productSize
     * @return $this
     */
    public function setProductSize(ProductSize $productSize)
    {
        $this->productSizeId = $productSize->getId();

        return $this;
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
