<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * Priority Model Class
 *
 * @Source('priorities')
 * @HasMany('id', 'PriorityClassification', 'priorityId', {'alias':'PriorityClassifications'})
 * @HasMany('id', 'Product', 'priorityId', {'alias':'Products'})
 */
class Priority extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_NAME = 'name';
    const PROPERTY_PRIORITY_NUMBER = 'priorityNumber';
    const PROPERTY_PRIORITY_DATE = 'priorityDate';
    const PROPERTY_SKETCH = 'sketch';
    const PROPERTY_CREATED = 'created';

    const ALIAS_PRIORITY_CLASSIFICATIONS = 'PriorityClassifications';
    const ALIAS_PRODUCTS = 'Products';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="priority_number", type="integer", nullable=true)
     * @FormElement(label="Priority number", type="Text", required=false)
     */
    protected $priorityNumber;

    /**
     * @Column(column="priority_date", type="date", nullable=false)
     * @FormElement(label="Priority date", type="Date", required=true)
     */
    protected $priorityDate;

    /**
     * @Column(column="sketch", type="string", nullable=true)
     * @FormElement(label="Sketch", type="File", required=false)
     */
    protected $sketch;

    /**
     * @Column(column="created", type="string", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_PRIORITY_NUMBER => true,
        self::PROPERTY_PRIORITY_DATE => true,
        self::PROPERTY_SKETCH => true,
        self::PROPERTY_CREATED => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriorityNumber()
    {
        return $this->priorityNumber;
    }

    /**
     * @param int $priorityNumber
     * @return $this
     */
    public function setPriorityNumber($priorityNumber)
    {
        $this->priorityNumber = null !== $priorityNumber ? (int)$priorityNumber : null;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getPriorityDate()
    {
        return $this->priorityDate;
    }

    /**
     * @param string|DateTime $priorityDate
     * @return $this
     */
    public function setPriorityDate($priorityDate)
    {
        $this->priorityDate = $priorityDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getSketch()
    {
        return $this->sketch;
    }

    /**
     * @param string $sketch
     * @return $this
     */
    public function setSketch($sketch)
    {
        $this->sketch = $sketch;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }


    /**
     * @return string
     */
    public function getUploadPath()
    {
        return APP_PATH . 'var/uploads/files/';
    }

    /**
     * @return string
     */
    public function getSketchPath()
    {
        /** @var object $config */
        $config = $this->getDI()->getShared('config');
        $url = ltrim($config->application->baseUri, '/');


        if (is_file($this->getUploadPath() . $this->getSketch())) {
            return $url . '/uploads/files/' . $this->getSketch();
        }

        $filename = $url . '/uploads/files/no-image.jpg';
        if (is_file($filename)) {
            return $filename;
        }

        return $url . '/uploads/files/no-image.png';
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|PriorityClassification[]
     * 
     * @throws \DomainException
     */
    public function getPriorityClassifications($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRIORITY_CLASSIFICATIONS, $arguments);
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|Product[]
     *
     * @throws \DomainException
     */
    public function getProducts($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PRODUCTS, $arguments);
    }

    public function getPrioritySerbianDate()
    {
        return (new DateTime($this->priorityDate))->getShortSerbianFormat();
    }


    public function __toString()
    {
        return $this->getName() . " - " . $this->getPrioritySerbianDate(). " - " . $this->getPriorityNumber();
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
