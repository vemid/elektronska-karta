<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * ChannelSupply Model Class
 *
 * @Source('channel_supplies')
 * @HasMany('id', 'ChannelSupplyEntity', 'channelSupplyId', {'alias':'ChannelSupplyEntities'})
 */
class ChannelSupply extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_NAME = 'name';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_EMAIL = 'email';
    const VP = 'VP';
    const MP = 'MP';

    const ALIAS_CHANNEL_SUPPLY_ENTITIES = 'ChannelSupplyEntities';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="type", type="string", nullable=false)
     * @FormElement(label="Type", type="Select", required=true, options={'MP':'MP', 'VP' : 'VP'})
     */
    protected $type;

    /**
     * @Column(column="email", type="string", nullable=true)
     * @FormElement(label="Email", type="Text", required=false)
     */
    protected $email;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_TYPE => true,
        self::PROPERTY_EMAIL => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return Resultset|ChannelSupplyEntity[]
     * 
     * @throws \DomainException
     */
    public function getChannelSupplyEntities($arguments = null)
    {
        return $this->getRelated(self::ALIAS_CHANNEL_SUPPLY_ENTITIES, $arguments);
    }

    public function __toString()
    {
        return $this->getName();
    }
}
