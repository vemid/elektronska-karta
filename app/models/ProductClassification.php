<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;
use Vemid\Entity\EntityInterface;
use Vemid\Entity\Type;

/**
 * ProductClassification Model Class
 *
 * @Source('product_classifications')
 *
 * @BelongsTo('productId', 'Product', 'id', {'alias':'Product'})
 */
class ProductClassification extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PRODUCT_ID = 'productId';
    const PROPERTY_ENTITY_TYPE_ID = 'entityTypeId';
    const PROPERTY_ENTITY_ID = 'entityId';
    const PROPERTY_IS_SYNCED = 'isSynced';

    const ALIAS_PRODUCT = 'Product';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="product_id", type="integer", nullable=false)
     * @FormElement(label="Product", type="Select", required=true, relation="Product")
     */
    protected $productId;

    /**
     * @Column(column="entity_type_id", type="string", nullable=false)
     * @FormElement(label="Entity type", type="Text", required=true)
     */
    protected $entityTypeId;

    /**
     * @Column(column="is_synced", type="bool", nullable=true)
     */
    protected $isSynced;

    /**
     * @Column(column="entity_id", type="string", nullable=false)
     * @FormElement(label="Entity", type="Text", required=true)
     */
    protected $entityId;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRODUCT_ID => true,
        self::PROPERTY_ENTITY_TYPE_ID => true,
        self::PROPERTY_ENTITY_ID => true,
        self::ALIAS_PRODUCT => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->productId = null !== $productId ? (int)$productId : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityTypeId()
    {
        return $this->entityTypeId;
    }

    /**
     * @param int $entityTypeId
     * @return $this
     */
    public function setEntityTypeId($entityTypeId)
    {
        $this->entityTypeId = $entityTypeId;

        return $this;
    }

    /**
     * @return null|EntityInterface|Classification|Attribute
     */
    public function getProductClassification()
    {
        return Type::getEntity($this->getEntityTypeId(),$this->getEntityId());
    }


    /**
     * @param EntityInterface $entity
     * @return $this
     */
    public function setProductClassification(EntityInterface $entity)
    {
        $this->entityTypeId = $entity->getEntityTypeId();
        $this->entityId = $entity->getEntityId();

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param string $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSynced()
    {
        return $this->isSynced;
    }

    /**
     * @param boolean $isSynced
     */
    public function setIsSynced($isSynced)
    {
        $this->isSynced = $isSynced;
    }

    /**
     * @return null|Product
     * 
     * @throws \DomainException
     */
    public function getProduct()
    {
        return $this->getRelated(self::ALIAS_PRODUCT);
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->productId = $product->getId();

        return $this;
    }

    public function beforeCreate()
    {
        $this->isSynced = false;
        if($this->entityTypeId == \Vemid\Entity\Type::CODE) {
            $this->isSynced = true;
        }
    }
}
