<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * PriorityClassification Model Class
 *
 * @Source('priority_classifications')
 *
 * @BelongsTo('priorityId', 'Priority', 'id', {'alias':'Priority'})
 * @BelongsTo('classificationId', 'Classification', 'id', {'alias':'Classification'})
 */
class PriorityClassification extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PRIORITY_ID = 'priorityId';
    const PROPERTY_CLASSIFICATION_ID = 'classificationId';

    const ALIAS_PRIORITY = 'Priority';
    const ALIAS_CLASSIFICATION = 'Classification';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="priority_id", type="integer", nullable=false)
     * @FormElement(label="Priority", type="Select", required=true, relation="Priority")
     */
    protected $priorityId;

    /**
     * @Column(column="classification_id", type="integer", nullable=false)
     * @FormElement(label="Classification", type="Select", required=true, relation="Classification")
     */
    protected $classificationId;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PRIORITY_ID => true,
        self::PROPERTY_CLASSIFICATION_ID => true,
        self::ALIAS_PRIORITY => true,
        self::ALIAS_CLASSIFICATION => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriorityId()
    {
        return $this->priorityId;
    }

    /**
     * @param int $priorityId
     * @return $this
     */
    public function setPriorityId($priorityId)
    {
        $this->priorityId = null !== $priorityId ? (int)$priorityId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getClassificationId()
    {
        return $this->classificationId;
    }

    /**
     * @param int $classificationId
     * @return $this
     */
    public function setClassificationId($classificationId)
    {
        $this->classificationId = null !== $classificationId ? (int)$classificationId : null;

        return $this;
    }

    /**
     * @return null|Priority
     * 
     * @throws \DomainException
     */
    public function getPriority()
    {
        return $this->getRelated(self::ALIAS_PRIORITY);
    }

    /**
     * @param Priority $priority
     * @return $this
     */
    public function setPriority(Priority $priority)
    {
        $this->priorityId = $priority->getId();

        return $this;
    }

    /**
     * @return null|Classification
     * 
     * @throws \DomainException
     */
    public function getClassification()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setClassification(Classification $classification)
    {
        $this->classificationId = $classification->getId();

        return $this;
    }



}
