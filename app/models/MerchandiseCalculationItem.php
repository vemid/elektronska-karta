<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * MerchandiseCalculationItem Model Class
 *
 * @Source('merchandise_calculation_items')
 *
 * @BelongsTo('merchandiseCalculationId', 'MerchandiseCalculation', 'id', {'alias':'MerchandiseCalculation'})
 * @BelongsTo('productId', 'Product', 'id', {'alias':'Product'})
 */
class MerchandiseCalculationItem extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_MERCHANDISE_CALCULATION_ID = 'merchandiseCalculationId';
    const PROPERTY_PRODUCT_ID = 'productId';
    const PROPERTY_COURSE = 'course';
    const PROPERTY_PURCHASE_PRICE = 'purchasePrice';
    const PROPERTY_WHOLESALE_PRICE = 'wholesalePrice';
    const PROPERTY_RETAIL_PRICE = 'retailPrice';
    const PROPERTY_CREATED = 'created';
    const PROPERTY_IS_SYNCED = 'isSynced';

    const ALIAS_MERCHANDISE_CALCULATION = 'MerchandiseCalculation';
    const ALIAS_PRODUCT = 'Product';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="merchandise_calculation_id", type="integer", nullable=false)
     * @FormElement(label="Merchandise calculation", type="Select", required=true, relation="MerchandiseCalculation")
     */
    protected $merchandiseCalculationId;

    /**
     * @Column(column="product_id", type="integer", nullable=false)
     * @FormElement(label="Product", type="Select", required=true, relation="Product")
     */
    protected $productId;

    /**
     * @Column(column="course", type="decimal", nullable=false)
     * @FormElement(label="Course", type="Text", required=true)
     */
    protected $course;

    /**
     * @Column(column="purchase_price", type="decimal", nullable=false)
     * @FormElement(label="Purchase price", type="Text", required=true)
     */
    protected $purchasePrice;

    /**
     * @Column(column="wholesale_price", type="decimal", nullable=false)
     * @FormElement(label="Wholesale price", type="Text", required=true)
     */
    protected $wholesalePrice;

    /**
     * @Column(column="retail_price", type="decimal", nullable=false)
     * @FormElement(label="Retail price", type="Text", required=true)
     */
    protected $retailPrice;

    /**
     * @Column(column="is_synced", type="bool", nullable=true)
     */
    protected $isSynced;

    /**
     * @Column(column="created", type="string", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_MERCHANDISE_CALCULATION_ID => true,
        self::PROPERTY_PRODUCT_ID => true,
        self::PROPERTY_COURSE => true,
        self::PROPERTY_PURCHASE_PRICE => true,
        self::PROPERTY_WHOLESALE_PRICE => true,
        self::PROPERTY_RETAIL_PRICE => true,
        self::PROPERTY_CREATED => true,
        self::ALIAS_MERCHANDISE_CALCULATION => true,
        self::ALIAS_PRODUCT => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getMerchandiseCalculationId()
    {
        return $this->merchandiseCalculationId;
    }

    /**
     * @param int $merchandiseCalculationId
     * @return $this
     */
    public function setMerchandiseCalculationId($merchandiseCalculationId)
    {
        $this->merchandiseCalculationId = null !== $merchandiseCalculationId ? (int)$merchandiseCalculationId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->productId = null !== $productId ? (int)$productId : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param float $course
     * @return $this
     */
    public function setCourse($course)
    {
        $this->course = null !== $course ? (float)$course : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * @param float $purchasePrice
     * @return $this
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = null !== $purchasePrice ? (float)$purchasePrice : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getWholesalePrice()
    {
        return $this->wholesalePrice;
    }

    /**
     * @param float $wholesalePrice
     * @return $this
     */
    public function setWholesalePrice($wholesalePrice)
    {
        $this->wholesalePrice = null !== $wholesalePrice ? (float)$wholesalePrice : null;

        return $this;
    }

    /**
     * @return float
     */
    public function getRetailPrice()
    {
        return $this->retailPrice;
    }

    /**
     * @param float $retailPrice
     * @return $this
     */
    public function setRetailPrice($retailPrice)
    {
        $this->retailPrice = null !== $retailPrice ? (float)$retailPrice : null;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSynced()
    {
        return $this->isSynced;
    }

    /**
     * @param boolean $isSynced
     */
    public function setIsSynced($isSynced)
    {
        $this->isSynced = $isSynced;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|MerchandiseCalculation
     * 
     * @throws \DomainException
     */
    public function getMerchandiseCalculation()
    {
        return $this->getRelated(self::ALIAS_MERCHANDISE_CALCULATION);
    }

    /**
     * @param MerchandiseCalculation $merchandiseCalculation
     * @return $this
     */
    public function setMerchandiseCalculation(MerchandiseCalculation $merchandiseCalculation)
    {
        $this->merchandiseCalculationId = $merchandiseCalculation->getId();

        return $this;
    }

    /**
     * @return null|Product
     * 
     * @throws \DomainException
     */
    public function getProduct()
    {
        return $this->getRelated(self::ALIAS_PRODUCT);
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->productId = $product->getId();

        return $this;
    }

    public function beforeCreate()
    {
        $this->isSynced = 0;
        $this->created = new DateTime();
    }
}
