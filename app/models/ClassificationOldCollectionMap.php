<?php

use Vemid\Entity\AbstractEntity;
use \Vemid\Entity\Manager\EntityManagerInterface;

/**
 * ClassificationOldCollectionMap Model Class
 *
 * @Source('classification_old_collection_map')
 *
 * @BelongsTo('classificationOldCollectionId', 'Classification', 'id', {'alias':'ClassificationOldCollection'})
 * @BelongsTo('classificationAgeId', 'Classification', 'id', {'alias':'ClassificationAge'})
 * @BelongsTo('classificationDoubleSeasonId', 'Classification', 'id', {'alias':'ClassificationDoubleSeason'})
 * @BelongsTo('attributeProducerId', 'Attributes', 'id', {'alias':'AttributeProducer'})
 * @BelongsTo('attributeProductTypeId', 'Attributes', 'id', {'alias':'AttributeProductType'})
 * @BelongsTo('classificationSeasonId', 'Classification', 'id', {'alias':'ClassificationSeason'})
 * @BelongsTo('classificationCollectionId', 'Classification', 'id', {'alias':'ClassificationCollection'})
 * @BelongsTo('codeId', 'Code', 'id', {'alias':'Code'})
 */
class ClassificationOldCollectionMap extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_CLASSIFICATION_OLD_COLLECTION_ID = 'classificationOldCollectionId';
    const PROPERTY_CLASSIFICATION_DOUBLE_SEASON_ID = 'classificationDoubleSeasonId';
    const PROPERTY_ATTRIBUTE_PRODUCER_ID = 'attributeProducerId';
    const PROPERTY_ATTRIBUTE_PRODUCT_TYPE_ID = 'attributeProductTypeId';
    const PROPERTY_CLASSIFICATION_AGE_ID = 'classificationAgeId';
    const PROPERTY_CLASSIFICATION_SEASON_ID = 'classificationSeasonId';
    const PROPERTY_CLASSIFICATION_COLLECTION_ID = 'classificationCollectionId';
    const PROPERTY_CODE_ID = 'codeId';

    const ALIAS_CODE = 'Code';
    const ALIAS_CLASSIFICATION_OLD_COLLECTION = 'ClassificationOldCollection';
    const ALIAS_CLASSIFICATION_DOUBLE_SEASON = 'ClassificationDoubleSeason';
    const ALIAS_ATTRIBUTE_PRODUCER = 'AttributeProducer';
    const ALIAS_ATTRIBUTE_PRODUCT_TYPE = 'AttributeProductType';
    const ALIAS_CLASSIFICATION_AGE = 'ClassificationAge';
    const ALIAS_CLASSIFICATION_SEASON = 'ClassificationSeason';
    const ALIAS_CLASSIFICATION_COLLECTION = 'ClassificationCollection';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="classification_old_collection_id", type="integer", nullable=false)
     * @FormElement(label="Classification old collection", type="Select", required=true)
     */
    protected $classificationOldCollectionId;

    /**
     * @Column(column="classification_age_id", type="integer", nullable=false)
     * @FormElement(label="Classification age", type="Select", required=true)
     */
    protected $classificationAgeId;

    /**
     * @Column(column="classification_collection_id", type="integer", nullable=false)
     * @FormElement(label="Classification collection", type="Select", required=true)
     */
    protected $classificationCollectionId;

    /**
     * @Column(column="classification_season_id", type="integer", nullable=false)
     * @FormElement(label="Classification season", type="Select", required=true)
     */
    protected $classificationSeasonId;

    /**
     * @Column(column="classification_double_season_id", type="integer", nullable=false)
     * @FormElement(label="Classification Double Season", type="Select", required=true)
     */
    protected $classificationDoubleSeasonId;

    /**
     * @Column(column="attribute_producer_id", type="integer", nullable=false)
     * @FormElement(label="Attribute Producer", type="Select", required=true)
     */
    protected $attributeProducerId;

    /**
     * @Column(column="attribute_product_type_id", type="integer", nullable=false)
     * @FormElement(label="Attribute Product Type", type="Select", required=true)
     */
    protected $attributeProductTypeId;

    /**
     * @Column(column="code_id", type="integer", nullable=false)
     * @FormElement(label="Code", type="Select", required=true)
     */
    protected $codeId;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CLASSIFICATION_OLD_COLLECTION_ID => true,
        self::PROPERTY_CLASSIFICATION_DOUBLE_SEASON_ID =>true,
        self::PROPERTY_CLASSIFICATION_AGE_ID => true,
        self::PROPERTY_CLASSIFICATION_SEASON_ID => true,
        self::PROPERTY_CLASSIFICATION_COLLECTION_ID => true,
        self::PROPERTY_CODE_ID => true,
        self::ALIAS_CODE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getClassificationOldCollectionId()
    {
        return $this->classificationOldCollectionId;
    }

    /**
     * @param int $classificationOldCollectionId
     * @return $this
     */
    public function setClassificationOldCollectionId($classificationOldCollectionId)
    {
        $this->classificationOldCollectionId = null !== $classificationOldCollectionId ? (int)$classificationOldCollectionId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getClassificationDoubleSeasonId()
    {
        return $this->classificationDoubleSeasonId;
    }

    /**
     * @param int $classificationDoubleSeasonId
     * @return $this
     */
    public function setClassificationDoubleSeasonId($classificationDoubleSeasonId)
    {
        $this->classificationDoubleSeasonId = null !== $classificationDoubleSeasonId ? (int)$classificationDoubleSeasonId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getAttributeProducerId()
    {
        return $this->attributeProducerId;
    }

    /**
     * @param int $attributeProducerId
     * @return $this
     */
    public function setAttributeProducerId($attributeProducerId)
    {
        $this->attributeProducerId = null !== $attributeProducerId ? (int)$attributeProducerId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getAttributeProductTypeId()
    {
        return $this->attributeProductTypeId;
    }

    /**
     * @param int $attributeProductTypeId
     * @return $this
     */
    public function setAttributeProductTypeId($attributeProductTypeId)
    {
        $this->attributeProductTypeId = null !== $attributeProductTypeId ? (int)$attributeProductTypeId : null;

        return $this;
    }


    /**
     * @return int
     */
    public function getClassificationAgeId()
    {
        return $this->classificationAgeId;
    }

    /**
     * @param int $classificationAgeId
     * @return $this
     */
    public function setClassificationAgeId($classificationAgeId)
    {
        $this->classificationAgeId = null !== $classificationAgeId ? (int)$classificationAgeId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getClassificationSeasonId()
    {
        return $this->classificationSeasonId;
    }

    /**
     * @param int $classificationSeasonId
     * @return $this
     */
    public function setClassificationSeasonId($classificationSeasonId)
    {
        $this->classificationSeasonId = null !== $classificationSeasonId ? (int)$classificationSeasonId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getClassificationCollectionId()
    {
        return $this->classificationCollectionId;
    }

    /**
     * @param int $classificationCollectionId
     * @return $this
     */
    public function setClassificationCollectionId($classificationCollectionId)
    {
        $this->classificationCollectionId = null !== $classificationCollectionId ? (int)$classificationCollectionId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getCodeId()
    {
        return $this->codeId;
    }

    /**
     * @param int $codeId
     * @return $this
     */
    public function setCodeId($codeId)
    {
        $this->codeId = null !== $codeId ? (int)$codeId : null;

        return $this;
    }

    /**
     * @return null|Code
     * 
     * @throws \DomainException
     */
    public function getCode()
    {
        return $this->getRelated(self::ALIAS_CODE);
    }

    /**
     * @param Code $code
     * @return $this
     */
    public function setCode(Code $code)
    {
        $this->codeId = $code->getId();

        return $this;
    }

    /**
     * @return null|Classification|EntityManagerInterface
     *
     * @throws \DomainException
     */
    public function getClassificationOldCode()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION_OLD_COLLECTION);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setClassificationOldCode(Classification $classification)
    {
        $this->classificationOldCollectionId = $classification->getId();

        return $this;
    }

    /**
     * @return null|Classification|EntityManagerInterface
     *
     * @throws \DomainException
     */
    public function getClassificationDoubleSeason()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION_DOUBLE_SEASON);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setClassificationDoubleSeason(Classification $classification)
    {
        $this->classificationDoubleSeasonId = $classification->getId();

        return $this;
    }

    /**
     * @return null|Attribute|EntityManagerInterface
     *
     * @throws \DomainException
     */
    public function getAttributeProducer()
    {
        return $this->getRelated(self::ALIAS_ATTRIBUTE_PRODUCER);
    }

    /**
     * @param Attribute $attribute
     * @return $this
     */
    public function setAttributeProducer(Attribute $attribute)
    {
        $this->attributeProducerId = $attribute->getId();

        return $this;
    }

    /**
     * @return null|Attribute|EntityManagerInterface
     *
     * @throws \DomainException
     */
    public function getAttributeProductType()
    {
        return $this->getRelated(self::ALIAS_ATTRIBUTE_PRODUCT_TYPE);
    }

    /**
     * @param Attribute $attribute
     * @return $this
     */
    public function setAttributeProductType(Attribute $attribute)
    {
        $this->attributeProductTypeId = $attribute->getId();

        return $this;
    }

    /**
     * @return null|Classification|EntityManagerInterface
     *
     * @throws \DomainException
     */
    public function getClassificationSeason()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION_SEASON);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setClassificationSeason(Classification $classification)
    {
        $this->classificationSeasonId = $classification->getId();

        return $this;
    }

    /**
     * @return null|Classification|EntityManagerInterface
     *
     * @throws \DomainException
     */
    public function getClassificationAge()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION_AGE);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setClassificationAge(Classification $classification)
    {
        $this->classificationAgeId = $classification->getId();

        return $this;
    }

    /**
     * @return null|Classification|EntityManagerInterface
     *
     * @throws \DomainException
     */
    public function getClassificationCollection()
    {
        return $this->getRelated(self::ALIAS_CLASSIFICATION_COLLECTION);
    }

    /**
     * @param Classification $classification
     * @return $this
     */
    public function setClassificationCollection(Classification $classification)
    {
        $this->classificationOldCollectionId = $classification->getId();

        return $this;
    }
}
