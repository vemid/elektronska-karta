<?php

use Vemid\Date\DateTime;
use Vemid\Entity\AbstractEntity;
use Phalcon\Mvc\Model\Resultset;

/**
 * FarbenCardItem Model Class
 *
 * @Source('farben_card_items')
 *
 * @BelongsTo('farbenCardId', 'FarbenCard', 'id', {'alias':'FarbenCard'})
 * @BelongsTo('materialFactoryId', 'MaterialFactory', 'id', {'alias':'MaterialFactory'})
 */
class FarbenCardItem extends AbstractEntity
{

    const PROPERTY_ID = 'id';
    const PROPERTY_FARBEN_CARD_ID = 'farbenCardId';
    const PROPERTY_MATERIAL_FACTORY_ID = 'materialFactoryId';
    const PROPERTY_NAME = 'name';
    const PROPERTY_DESCRIPTION = 'description';
    const PROPERTY_CREATED = 'created';

    const ALIAS_FARBEN_CARD = 'FarbenCard';
    const ALIAS_MATERIAL_FACTORY = 'MaterialFactory';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="farben_card_id", type="integer", nullable=false)
     * @FormElement(label="Farben card", type="Select", required=true, relation="FarbenCard")
     */
    protected $farbenCardId;

    /**
     * @Column(column="material_factory_id", type="integer", nullable=true)
     * @FormElement(label="Material factory", type="Select", required=false, relation="MaterialFactory")
     */
    protected $materialFactoryId;

    /**
     * @Column(column="name", type="string", nullable=true)
     * @FormElement(label="Name", type="Name", required=false)
     */
    protected $name;

    /**
     * @Column(column="description", type="string", nullable=true)
     * @FormElement(label="Description", type="text", required=false)
     */
    protected $description;

    /**
     * @Column(column="created", type="string", nullable=true)
     */
    protected $created;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_FARBEN_CARD_ID => true,
        self::PROPERTY_MATERIAL_FACTORY_ID => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_CREATED => true,
        self::ALIAS_FARBEN_CARD => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = null !== $id ? (int)$id : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getFarbenCardId()
    {
        return $this->farbenCardId;
    }

    /**
     * @param int $farbenCardId
     * @return $this
     */
    public function setFarbenCardId($farbenCardId)
    {
        $this->farbenCardId = null !== $farbenCardId ? (int)$farbenCardId : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaterialFactoryId()
    {
        return $this->materialFactoryId;
    }

    /**
     * @param int $materialFactoryId
     * @return $this
     */
    public function setMaterialFactoryId($materialFactoryId)
    {
        $this->materialFactoryId = null !== $materialFactoryId ? (int)$materialFactoryId : null;

        return $this;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return null|FarbenCard
     * 
     * @throws \DomainException
     */
    public function getFarbenCard()
    {
        return $this->getRelated(self::ALIAS_FARBEN_CARD);
    }

    /**
     * @param FarbenCard $farbenCard
     * @return $this
     */
    public function setFarbenCard(FarbenCard $farbenCard)
    {
        $this->farbenCardId = $farbenCard->getId();

        return $this;
    }

    /**
     * @return null|MaterialFactory
     *
     * @throws \DomainException
     */
    public function getMaterialFactory()
    {
        return $this->getRelated(self::ALIAS_MATERIAL_FACTORY);
    }

    /**
     * @param MaterialFactory $materialFactory
     * @return $this
     */
    public function setMaterialFactory(MaterialFactory $materialFactory)
    {
        $this->materialFactoryId = $materialFactory->getId();

        return $this;
    }

    public function beforeSave()
    {
        if ($materialFactory = $this->getMaterialFactory()) {
            $this->name = $materialFactory->getDisplayName();
        }
    }

    public function beforeValidationOnCreate()
    {
        $this->created = new DateTime();
    }
}
