(function ($) {


    jQuery.expr[':'].regex = function(elem, index, match) {
        var matchParams = match[3].split(','),
            validLabels = /^(data|css):/,
            attr = {
                method: matchParams[0].match(validLabels) ?
                    matchParams[0].split(':')[0] : 'attr',
                property: matchParams.shift().replace(validLabels,'')
            },
            regexFlags = 'ig',
            regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
        return regex.test(jQuery(elem)[attr.method](attr.property));
    }

    $(document).on("ready", function () {

        grepSessionMessages();
        validateForm();
        setChosen();
        initTextEditor();
        initDropZone();
        initTouchSpin();
        uploadAvatar();
        initCalendar();
        initDataTable();
        initInvoiceDataTable();
        initSteps();
        inputFile();
        initProductStatus();
        initProductOrderable();
        iniQuestionnaire();
        initTablsOnLoad();
        initChecks();
        initChosen();
        initDropUploadOrders();
        reUploadImage();
        initOrderListTable();
        iniContentEditable();


        if ($("#hiddenSampleId").length > 0) {
            $("#disabledMaterialSampleId").trigger("change");
        }

        $(".woi-efectus").TouchSpin({
            buttondown_class: 'btn btn-white',
            buttonup_class: 'btn btn-white',
            max: 100000
        });

        //$("div:regex(class, .*sd.*)")

        $( "a:regex(href, #tab.*)" ).click( function(e) {
            setTimeout(function() {

                window.scrollTo(0, 0);
            }, 1);
        } );

        $(".datepicker").flatpickr({
            altInput: true,
            altFormat: "F j, Y",
            allowInput: true,
            locale: {
                firstDayOfWeek: 1,
                weekdays: {
                    shorthand: ['Ned', 'Pon', 'Uto', 'Sre', 'Čet', 'Pet', 'Sub'],
                    longhand: ['Nedelja', 'Ponedeljak', 'Utorak', 'Sreda', 'Četvrtak', 'Petak', 'Subota'],
                },
                months: {
                    shorthand: ['Jan', 'Feb', 'Mart', 'Apr', 'Maj', 'Jun', 'Jul', 'Ag', 'Sep', 'Okt', 'Nov', 'Dec'],
                    longhand: ['Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust', 'Septembar', 'Oktobar', 'Novembar', 'Decembar'],
                },
            },
            onChange: function(selectedDates, dateStr, instance) {
                $(instance.element).parents(".form-group").next().find("textarea, select, input").filter(':visible').focus();
            },
        });

        $(".datetimepicker").flatpickr({
            enableTime: true,
            altInput: true,
            altFormat: "F j, Y h:i K",
            allowInput: true,
            locale: {
                firstDayOfWeek: 1,
                weekdays: {
                    shorthand: ['Ned', 'Pon', 'Uto', 'Sre', 'Čet', 'Pet', 'Sub'],
                    longhand: ['Nedelja', 'Ponedeljak', 'Utorak', 'Sreda', 'Četvrtak', 'Petak', 'Subota'],
                },
                months: {
                    shorthand: ['Jan', 'Feb', 'Mart', 'Apr', 'Maj', 'Jun', 'Jul', 'Ag', 'Sep', 'Okt', 'Nov', 'Dec'],
                    longhand: ['Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust', 'Septembar', 'Oktobar', 'Novembar', 'Decembar'],
                },
            },
            onChange: function(selectedDates, dateStr, instance) {
                $(instance.element).parents(".form-group").next().find("textarea, select, input").filter(':visible').focus();
            },
        });

        $("#monthpicker").datepicker({
            format: "MM yyyy",
            viewMode: "months",
            minViewMode: "months",
        }).on("changeDate", function (e) {
            var formattedDate = new Date(e.date);
            var d = formattedDate.getDate();
            var m =  formattedDate.getMonth();
            m += 1;  // JavaScript months are 0-11
            var y = formattedDate.getFullYear();
            var year = y + "-" + m + "-" + d;

            if (!isNaN(m)) {
                window.location.replace("/index/index/?date=" + year);
            }
        });

        $("#keyword").parents("form").submit(function (e) {
            e.preventDefault;
            return false;
        });

        $("form").submit(function (e) {
            return getFormValidation($(this));
        });

        $(".file-wrapper input[type=file]")
            .bind("change focus click", SITE.fileInputs);

        $('[data-toggle="tooltip"]').tooltip({
            container: "body",
            html: true,
        });

        $(document).on("change", ".js-switch", function() {
            if ($(this).is(":checked")) {
                if ($(this).attr('id') == 'typeIn') {
                    var special = document.querySelector('#typeOut');
                } else {
                    var special = document.querySelector('#typeIn');

                }

                $(special).attr("checked", false);

                if (typeof Event === 'function' || !document.fireEvent) {
                    var event = document.createEvent('HTMLEvents');
                    event.initEvent('change', true, true);
                    special.dispatchEvent(event);
                } else {
                    special.fireEvent('onchange');
                }
            }
        });

        $("#side-menu").metisMenu();

        $("select.languages").chosen({
            placeholder_text_multiple: t('-- Izaberite --')
        });

        // Highlight the top nav as scrolling
        $("body").scrollspy({
            target: ".navbar-fixed-top",
            offset: 80
        });

        $(function () {
            $(window).bind("load resize", function () {
                if ($(this).width() < 769) {
                    $("body").addClass("body-small")
                } else {
                    $("body").removeClass("body-small")
                }
            })
        });

        $(function () {
            $('.spinner .btn:first-of-type').on('click', function() {
                var val = $('.spinner input').val();
                if (val == '') {
                    val = 0;
                }

                $('.spinner input').val( parseInt(val, 10) + 1);
            });

            $('.spinner .btn:last-of-type').on('click', function() {
                var val = $('.spinner input').val();
                if (val == '') {
                    val = 0;
                }
                $('.spinner input').val( parseInt(val, 10) - 1);
            });
        });

        $(document).on("change", "#classificationTypeCodeId", function(){
            var classificationTypeId = $(this).val();
            var $parentClassificationId = $("#parentClassificationId");
            $.ajax({
                url: "/classifications/get-parent-classification/" + classificationTypeId,
                type: "get",
                success: function (data) {
                    if (!$.isEmptyObject(data)) {
                        $parentClassificationId
                            .find('option')
                            .remove()
                            .end()
                            .append('<option value="">---</option>');

                        $.each(data, function (index, value) {
                            var option = new Option(value, index);
                            $parentClassificationId.append($(option));
                        })
                    }
                }
            });

        });

        $(document).on("click", ".minimalize-styl-2", function (e) {
            // smoothlyMenu();

            if ($("body").hasClass("mini-navbar")) {
                $("body").removeClass("mini-navbar")
            } else {
                $("body").addClass("mini-navbar")
            }
        });

        $(document).on("click", "#lightBoxGallery", function (event) {

            event = event || window.event;
            var target = event.target || event.srcElement,
                link = target.src ? target.parentNode : target,
                options = {
                    index: link,
                    event: event,
                    slidesContainer: "#slides",
                    container: "#gallery",
                    carousel: true
                },
                links = this.getElementsByTagName("a");

            if ($(link).is("a") && !$(target).hasClass('btn-transparent')) {
                blueimp.Gallery(
                    links,
                    options
                );
            }

        });

        $("#biography").summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['ul', 'ol', 'paragraph']]
            ]
        });

        $(document).on("click", "#download-false", function () {
            alert(t("Personal photo must be at least 400x400"));
        });

    });

    // Collapse ibox function
    $(document).on("click", ".collapse-link", function (e) {
        var ibox = $(this).closest("div.ibox");
        var button = $(this).find("i");
        var content = ibox.find("div.ibox-content");
        content.slideToggle(200);
        button.toggleClass("fa-chevron-up").toggleClass("fa-chevron-down");
        ibox.toggleClass("").toggleClass("border-bottom");
        setTimeout(function () {
            ibox.resize();
            ibox.find("[id^=map-]").resize();
        }, 50);
    });

    // Collapse ibox function

    $(document).on("click", ".init-map", function (e) {

        e.preventDefault();

        var hitId = $("#bidRequestId").val();

        var modal = $("#modal");
        var body = '<div id="mapCanvas" style="width: 520px; height: 400px"></div>';
        var title = t("Suggest location of play");

        var footerModal = $(modal).find(".modal-footer");

        $(".pull-left", footerModal).remove();
        var info = '<div class="pull-left"><p class="smaller-80">' + t('Click on the map and place pin') + '</p></div>';

        $(modal).find(".modal-title").html(title);
        $(modal).find(".modal-body").html(body);
        $(modal).find("#save-modal").attr("id", "map-update").html(t('Save changes'));
        $(modal).find("#close-modal").html(t('Cancel'));
        footerModal.append(info);
        $("body").addClass("modal-open");
        $(modal).show();

        var lat = $("#latitude").val();
        var lng = $("#longitude").val();

        var map = initializeMap(lat, lng);
        google.maps.event.trigger(map, 'resize');

    });

    $(document).on("click", "#map-update", function () {

        destroyModal(modal);

        var mapDiv = $("#update-map");
        $("#map_canvas").remove();
        mapDiv.width("100%");
        mapDiv.height("135px");


        var lat = $("#latitude").val();
        var lng = $("#longitude").val();

        var map = updateMap(lat, lng);
        google.maps.event.trigger(map, 'resize');
    });

    $(document).on("change", "#operationWorker select", function() {
        var parents = $(this).parents("td");
        parents.removeClass("has-error")
        setTimeout(function(){
            // parents.find(".chosen-container").removeClass('chosen-container-active');
            parents.next().find("input:not(:hidden)").focus().click();
        }, 0);
    });

    $(document).on("submit", "#operationWorker", function(e){
        var tr = $(this).find("tr:not(:eq(0))");
        tr.find("td").removeClass("has-error");
        var shouldProceed = false;
        tr.each(function(index, row){
            var $row = $(row);
            var inputs  = $('select, input', $row).filter(function() {
                $(this).removeAttr("data-original-title");
                $(this).next().removeAttr("data-original-title");

                if (this.hasAttribute('data-float') && this.value) {
                    var floatValidateRex = new RegExp("^[+-]?\\d+(\\.\\d+)?$", "i");
                    var validFloat = floatValidateRex.exec($(this).val());
                    if (!validFloat) {
                        $(this).parents("td").addClass("has-error");
                        $(this).attr('data-original-title', 'Format nije dobar.')
                            .tooltip('fixTitle')
                            .tooltip('show');
                    }
                }

                return $(this)[0].hasAttribute('data-form') && ($(this).val() === '' || $(this).val() === ' ' || $(this).val() === 0 || $(this).val() === '0');
            });


            if (inputs.length == 1 || inputs.length == 2) {
                inputs.each(function(k, elem){
                    var $1 = $(elem);
                    $1.parents("td").addClass("has-error");

                    if ($1.is(":visible")) {
                        $1.attr('data-original-title', 'Obavezno polje!')
                            .tooltip('fixTitle')
                            .tooltip('show');
                    } else {
                        $1.next().attr('data-original-title', 'Obavezno polje!')
                            .tooltip('fixTitle')
                            .tooltip('show');
                    }

                });
            }

            if (inputs.length == 0) {
                shouldProceed = true;
            }
        });

        if ($("td.has-error").length) {
            e.preventDefault();
        } else if (!shouldProceed) {
            tr.eq(0).find("td").addClass("has-error");
            e.preventDefault();
        }
    });

    $("#signinButton").click(function () {
        // signInCallback defined in step 6.
        auth2.grantOfflineAccess({'redirect_uri': "postmessage"}).then(signInCallback);
    });

    $(document).on("click", ".close-link", function (e) {
        var content = $(this).closest("div.ibox");
        content.remove();
    });

    $('body').on('keydown', 'input, select', function(e) {
        var self = $(this)
            , form = self.parents('form:eq(0)')
            , focusable
            , next
        ;
        if (e.keyCode == 13 || e.keyCode == 9) {
            focusable = form.find('input,a,select,button,textarea').filter(':visible');
            next = focusable.eq(focusable.index(this)+1);
            console.log(next);
            if (next.length) {
                if (next.hasClass("chosen-single")) {
                    var $select = next.parents(".form-group").find("select");
                    setTimeout(function() {
                        $select.trigger("liszt:activate").trigger('chosen:open');
                    }, 150);
                }

                setTimeout(function() {
                    next.focus();
                }, 150);
            }

            return false;
        }
    });

    $(window).bind("resize", function () {
        setChosen();
    }).trigger("resize");

    $(".slide-toggle").click(function (e) {
        e.preventDefault();

        var $this = $(this);
        var collapsableParent = $this.parent().parent();
        $(".search-criteria", collapsableParent).slideToggle("fast");
        collapsableParent.toggleClass("collapse-off");
        if ($this.hasClass("fa-plus")) {
            $this.removeClass("fa-plus");
            $this.addClass("fa-minus");
        } else {
            $this.removeClass("fa-minus");
            $this.addClass("fa-plus");
        }
    });


    /**
     * Prevent form submit on enter
     */
    $(document).on("keypress", "form#ajax-form", function (e) {
        if (e.keyCode == 13 && e.target.className !== "note-editable") {
            e.preventDefault();
            return false;
        }
    });

    if ($.fn.visualCaptcha) {
        // there is some jquery plugin named 'marquee' on your page
        var captchaEl = $("#sample-captcha").visualCaptcha({
            imgPath: "/img/captcha/",
            captcha: {
                numberOfImages: 4,
                callbacks: {
                    loaded: function (captcha) {
                        // Avoid adding the hashtag to the URL when clicking/selecting visualCaptcha options
                        $("#sample-captcha a").on("click", function (event) {
                            event.preventDefault();
                        });
                    }
                }

            }
        });
        var captcha = captchaEl.data("captcha");

        // Show an alert saying if visualCaptcha is filled or not
        var _sayIsVisualCaptchaFilled = function (event) {
            event.preventDefault();

            if (captcha.getCaptchaData().valid) {
                window.alert("visualCaptcha is filled!");
            } else {
                window.alert("visualCaptcha is NOT filled!");
            }
        };

        var statusEl = $("#status-message"),
            queryString = window.location.search;
        // Show success/error messages
        if (queryString.indexOf('status=noCaptcha') !== -1) {
            statusEl.html('<div class="status alert alert-warning"> <div class="icon-no"></div> <p>' + t('Captcha was not started!') + '</p> </div>');
        } else if (queryString.indexOf("status=validImage") !== -1) {
            statusEl.html('<div class="status valid alert alert-success"> <div class="icon-yes"></div> <p>' + t('Image was valid!') + '</p> </div>');
        } else if (queryString.indexOf("status=failedImage") !== -1) {
            statusEl.html('<div class="status alert alert-danger"> <div class="icon-no"></div> <p>' + t('Image was NOT valid!') + '</p> </div>');
        } else if (queryString.indexOf("status=validAudio") !== -1) {
            statusEl.html('<div class="status valid alert alert-success"> <div class="icon-yes"></div> <p>' + t('Accessibility answer was valid!') + '</p> </div>');
        } else if (queryString.indexOf("status=failedAudio") !== -1) {
            statusEl.html('<div class="status alert alert-danger"> <div class="icon-no"></div> <p>A' + t('Accessibility answer was NOT valid!') + '</p> </div>');
        } else if (queryString.indexOf("status=failedPost") !== -1) {
            statusEl.html('<div class="status alert alert-danger"> <div class="icon-no"></div> <p>' + t('No Captcha answer was given!') + '</p> </div>');
        }

        // Bind that function to the appropriate link
        $('#check-is-filled').on("click.app", _sayIsVisualCaptchaFilled);
    }

    $(".drag").draggable({
        cursor: "move",
        cursorAt: {top: 0, left: 0},
        revert: "invalid"
    });

    $(".drop-container").droppable({
        accept: ".drag",
        drop: function (event, ui) {
            var dropped = ui.draggable;
            $(this).droppable("option", "accept", dropped);
            var droppedOn = $(this);
            $(dropped).detach().css({top: 0, left: 0}).appendTo(droppedOn);
            $(dropped).draggable("disable");
            droppedOn.toggleClass("excluded");
            droppedOn.find("p").hide();
        },
        out: function (event, ui) {
            $(this).droppable("option", "accept", ".drag");
        },
        over: function (event, elem) {
            $(this).addClass("over");
        }
    });

    $(".drop").sortable({
        items: "li:not(.excluded)",
        cursor: 'move',
    }).disableSelection();

    $(document).on("click", ".acc-type", function (e) {
        e.preventDefault();
        $(this).parents('.role-box').find("input").prop("checked", true);
        $("#account").submit();
    });

    $(document).on("click", ".slider-dots a", function (e) {
        e.preventDefault();
    });

    $(document).on("change", "#productProgram, #genderClassification, #productGroup, #subProductGroup, #subSubProductGroup, #materialTypeCodeId, #materialTypeCodeId, #materialCodeId", function () {

        var childElement;
        if (this.id == 'productProgram') {
            childElement = $("#genderClassification");
            if (this.value == "1B" || this.value == "1A" || this.value =="9B" || this.value =="9A") {
                $("#clothes").addClass("hidden");
                $("#shoes").removeClass("hidden");
            } else if (this.value == "1O" || this.value =='9O' || this.value == "1A" || this.value == "9A" ){
                $("#clothes").removeClass("hidden");
                $("#shoes").addClass("hidden");
            } else {
                $("#clothes").addClass("hidden");
                $("#shoes").addClass("hidden");
            }
        } else if (this.id == 'genderClassification') {
            childElement = $("#productGroup");
        } else if (this.id == 'productGroup') {
            childElement = $("#subProductGroup");

            $.ajax({
                url: "/products/derive-name-from-code",
                type: "post",
                data: { code: this.value },
                dataType: "json",
                success: function (data) {
                    $("#name").val(data.name);
                }
            });

        } else if (this.id == 'subProductGroup') {
            childElement = $("#subSubProductGroup");
        } else if (this.id == 'subSubProductGroup') {
            childElement = $("#color");
        } else if (this.id == 'materialTypeCodeId') {
            childElement = $("#materialCodeId");
        } else if (this.id == 'materialCodeId') {
            childElement = $("#subMaterialCodeId");
        }

        getChildrenClassifications(this, childElement);
    });

    $(document).on("focusout", "#findBarcode", function () {
        $("#save-modal").click();
    });

    $(document).on("submit", "#load-worker", function (e) {
        e.preventDefault();
        $(this).find(".form-group").removeClass("has-error");
        var $data = $(this).serialize();
        var $elements = $(this).find("select, input");
        $elements.each(function (index, elem) {
            var $elem = $(elem);
            if ($elem[0].hasAttribute("name") && ($elem.val() == 0 || $elem.val() == '')) {
                $elem.parents(".form-group").addClass("has-error");
            }
        });

        var url = $(this).attr("action") + "?" + $data;

        if (!$(this).find(".has-error").length) {
            $( "#load-into" ).load(url + " #loaded-html", function () {
                $('#report').DataTable({
                    "pageLength": 50,
                    fixedHeader: true,
                    "ordering" : true,
                    "search" : false,
                    "footerCallback": function ( row, data, start, end, display ) {
                        var api = this.api(), data;

                        // converting to interger to find total
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        let tds = $("*[data-footer='true']", row);
                        $( api.column(0).footer() ).html('Ukupno');

                        $.each(tds, function($index, $elem){
                            if ($elem.hasAttribute("data-column") && $index != 0) {
                                let columnIndexToSearch = $($elem).attr("data-column");

                                let pageTotal = api
                                    .column(columnIndexToSearch, { page: 'current'} )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0 );

                                let total = api
                                    .column(columnIndexToSearch )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0 );

                                $( api.column($index).footer() ).html(
                                    pageTotal +' ( od '+ total +')'
                                );

                            }
                        });
                    },
                });
            });
        }
    });

    $(document).on("change", "#productTypeCodeId", function () {
        var productTypeCodeId = this.value;
        var season = $("#season");
        $("#finalCode").val('');

        $.ajax({
            url: "/classifications/list-by-product-type",
            type: "post",
            data: { productTypeCodeId: productTypeCodeId },
            dataType: "json",
            success: function (data) {
                season.append($("<option/>", {
                    value: 0,
                    text: '-- Izaberite --'
                }));

                $.each(data, function (key, value) {
                    season.append($("<option/>", {
                        value: key,
                        text: value
                    }));
                });

                season.trigger("liszt:updated").trigger("chosen:updated");
            }
        });

        $.ajax({
            url: "/classifications/list-non-main-by-product-type",
            type: "post",
            data: { productTypeCodeId: productTypeCodeId },
            dataType: "json",
            success: function (data) {
                $.each(data, function (key, value) {
                    var element = $("#" + key);
                    element.find('option').remove();

                    if (Object.keys(value).length > 0) {
                        element.append($("<option/>", {
                            value: 0,
                            text: '-- Izaberite --'
                        }));

                        $.each(value, function (k, v) {
                            element.append($("<option/>", {
                                value: k,
                                text: v
                            }));
                        });
                    }

                    element.trigger("liszt:updated").trigger("chosen:updated");
                });
            }
        });
    });
    
    $(document).on("change", "#age", function(){
        $.get("/products/list-sizes/" + $(this).val(), function (response) {
            var html = '<div class="row">';
            html += '<div class="col-xs-12">';
            var counter = 0;
            var val = $("#productProgram").val();
            $.each(response, function (key, value) {
                $('.i-checks :input[value="'+ key +'"]').prop('checked', true).iCheck('update');
            });

            // var counter2 = 0;
            // $.each(response, function (key, value) {
            //     if ((key > 55 || key) < 16 && (val == '1O' || val == '9O')) {
            //         if (counter2 % 8 === 0) {
            //             html += '</div>';
            //             html += '</div>';
            //             html += '<div class="row">';
            //             html += '<div class="col-xs-12">';
            //             counter2 = 0;
            //         }
            //
            //         html += '<div class="checkbox-inline i-checks"><label> <input type="checkbox" value="'+ key +'" name="sizes[]"> <i></i> '+ value +'</label></div>';
            //
            //         counter2++;
            //     }
            // });

            html += '</div>';
            html += '</div>';

            var divId = 'clothes';

            if (val == '1B' || val == '9B') {
                divId = 'shoes';
                $("#shoes").show();
                $("#clothes").hide();
            }

            var $2 = $("#" + divId);
            // $2.html(html);
            // $('.i-checks', $2).iCheck({
            //     checkboxClass: 'icheckbox_square-green',
            //     radioClass: 'iradio_square-green',
            // });
        });
    });

    $(document).on("change", "#season", function () {
        var productTypeCodeIdField = $("#productProgram");
        var productTypeCodeId = $("#productTypeCodeId").val();

        $.ajax({
            url: "/classifications/list-product-program-by-product-type",
            type: "post",
            data: { productTypeCodeId: productTypeCodeId },
            dataType: "json",
            success: function (data) {
                productTypeCodeIdField.append($("<option/>", {
                    value: 0,
                    text: '-- Izaberite --'
                }));

                $.each(data, function (key, value) {
                    productTypeCodeIdField.append($("<option/>", {
                        value: key,
                        text: value
                    }));
                });

                productTypeCodeIdField.trigger("liszt:updated").trigger("chosen:updated");
            }
        });

    });

    $(document).on("change", "#materialFactory #materialSampleId, #disabledMaterialSampleId", function () {
        if (this.value == 0 || this.value == "") {
            $("#materialFactory").find("input, textarea").val("");
            $("#materialFactory").find("select").val("0").trigger("liszt:updated").trigger("chosen:updated");
        } else {
            $.ajax({
                url: "/material-samples/get-data",
                type: "post",
                data: { materialSampleId: this.value },
                dataType: "json",
                success: function (data) {
                    $.each(data, function (key, value) {
                        var $el = $("#" + key);

                        if ($el.length > 0) {
                            if( $el[0].nodeName.toLowerCase() === 'select') {
                                $el.find('option').remove();
                                $.each(value.options, function (k, v) {
                                    $el.append($("<option/>", {
                                        value: k,
                                        text: v
                                    }));
                                });

                                $el.val(value.default);
                                $el.trigger("liszt:updated").trigger("chosen:updated");
                            } else {
                                $el.val(value.default);
                            }
                        }
                    });
                }
            });
        }
    });

    $(document).on("change", "input#status", function () {
        var status = 'disable';
        if ($(this).is(':checked')) {
            status = 'enable';
        }

        var productId = $(this).attr("data-id");
        var formData = new FormData();

        formData.append("status", status);
        formData.append("productId", productId);
        sendPostAjaxCall("/products/update-mis", formData);
    });

    $(document).on("change", "input#productOrderable", function () {
        var productOrderable = 'disable';
        if ($(this).is(':checked')) {
            productOrderable = 'enable';
        }

        var productId = $(this).attr("data-id");
        var formData = new FormData();

        formData.append("status", productOrderable);
        formData.append("productId", productId);
        sendPostAjaxCall("/products/set-orderable", formData);
    });

    $(document).on("change", ".orderActive", function () {
        var status = 'disable';
        if ($(this).is(':checked')) {
            status = 'enable';
        }

        var orderId = $(this).attr("data-id");
        var formData = new FormData();

        formData.append("status", status);
        formData.append("orderId", orderId);
        sendPostAjaxCall("/orders/status", formData);

    });

    $(document).on("change", "#field", function () {
        var element = $(this).parents(".form-group").next().next();
        if ($(this).val() !== 'INPUT' && $(this).val() !== 'TEXT_AREA') {
            element.find('label').removeClass('hidden');
            element.find('.row').removeClass('hidden');
        } else {
            element.find('label').addClass('hidden');
            element.find('.row').addClass('hidden');
            element.find('.help-block').empty();
            element.removeClass('has-error');
        }
    });

    $(document).on("click", "#cloneOption", function (e) {
        e.preventDefault();
        var parent = $(this).parents('.form-group');
        var toBeCloned = parent.clone();
        var find = parent.find("input");
        if (find.val() != 0 || find.val() != '') {
            $(this).replaceWith($('<a id="removeOption" href="#" class="bigger-140 btn btn-circle btn-lg text-danger"><i class="fa fa-remove"></i></a>'));
            toBeCloned.find("label").empty();
            find.attr("readonly", true);
            toBeCloned.find("input").val('');
            parent.removeClass("has-error")
                .find(".help-block").empty();
            toBeCloned.removeClass("has-error")
                .find(".help-block").empty();
            toBeCloned.insertAfter(parent);
        } else {
            parent.addClass("has-error")
                .find(".help-block").html("Morate uneti pitanje!");
        }
    });

    $(document).on("click", "#removeOption", function (e) {
        e.preventDefault();
        var parent = $(this).parents('.form-group');
        parent.remove();
    });

    $(document).on("click", "#questionnaireAdd", function () {
        var codeId = $(this).attr("data-code-id");
        var electronCard = $(this).attr("data-id");
        var formData = new FormData();

        formData.append("codeId", codeId);
        formData.append("electronCard", electronCard);
        sendPostAjaxCall("/questionnaires/create", formData);
    });

    $(document).on("change", "#step-select-one select", function(){
        $(this).attr("data-char-length", this.value.length);

        var generator = $(this).attr("data-generator");

        if ($(this).attr("id") != 'season') {
            $(this).parent().parent().nextAll('.form-group').find('select').empty().trigger("liszt:updated").trigger("chosen:updated");
        }

        if (!this.value || generator == 'false') {
            return;
        }

        var position = $(this).attr("data-position");
        var code = '';
        for (i = 1; i <= parseInt(position); i++) {
            var element = $("#product-form").find("[data-position='" + i + "']");
            var val = element.val().trim();
            var shouldGenerateCode = element.attr('data-generator');

            if (element.length == 0 || val == 0 || val == '' || shouldGenerateCode == 'false'){
                continue;
            }

            code += element.attr('name') == 'season' ? val.substring(1) : val;
        }

        let codeField = $("#finalCode");
        codeField.val(code);

        let parentIdElem = $("#parentId");
        parentIdElem.empty();
        parentIdElem.trigger("liszt:updated").trigger("chosen:updated");

        parentIdElem.append($("<option/>", {
            value: '',
            text: '-- Izaberite --'
        }));

        if (codeField.val().length == 11) {

            let product = $("#productId");
            let productId = null;
            if (product.length > 0) {
                productId = product.val();
            }

            $.ajax({
                url: "/products/check-for-same-codes",
                type: "post",
                data: { productCode: code, productId : productId },
                dataType: "json",
                success: function (data) {
                    codeField.val(code + data.counter);
                }
            });

            $.ajax({
                url: "/products/parent-products",
                type: "post",
                data: { productCode: code },
                dataType: "json",
                success: function (data) {
                    $.each(data, function (id, val) {
                        parentIdElem.append($("<option/>", {
                            value: id,
                            text: val
                        }));

                        parentIdElem.trigger("liszt:updated").trigger("chosen:updated");

                    })
                }
            });
        }
    });

    $(document).on("click", "#customClassification", function (e) {
        e.preventDefault();
        var val = $("#productTypeCodeId").val();

        if (val !='' && val != 0) {
            $.get('/classifications/get-create-form', function (data) {
                if (data.error) {
                    if (data.messages) {
                        renderMessages(data.messages, "danger");
                    }
                } else {
                    var body = renderForm(data, '/classifications/create');
                    $(modal).find(".modal-title").html("<i class='fa fa-plus'></i> "  + t("Dodaj sezonu"));
                    $(modal).find(".modal-body").html(body);
                    $(modal).find("#save-modal").attr("id", "save-classification");
                    $(modal).show();
                }
            });
        }
    });

    $(document).on("click", "#save-classification", function () {

        var modal = $(this).parents("#modal");
        var form = $(modal).find("form");
        var isValid = getFormValidation($(form));
        var codeVal = $("#code").val();
        var nameVal = $("#ajax-form #name").val();

        if (isValid) {
            $.ajax({
                url: $(form).attr("action"),
                type: "post",
                data: {code: codeVal, name: nameVal, productTypeCodeId : $("#productTypeCodeId").val()},
                success: function (data) {
                    if (!data.error) {
                        var seasonField = $("#season");
                        seasonField.append($("<option/>", {
                                value: codeVal,
                                text: nameVal
                            }));

                        seasonField.val(codeVal);
                        seasonField.trigger("liszt:updated").trigger("chosen:updated");

                        renderMessages(data.messages, "success");
                        destroyModal(modal);
                    } else {
                        $.each(data.messages, function (key, message) {
                            showPushMessage(message.message, "danger");
                        });
                        grepSessionMessages();
                    }
                }
            });
        }
    });

    $(document).on("click", "#choose-order", function(e){
        e.preventDefault();

        $.get("/orders/choose-order", function (response) {
            var html = $(response).find(".ibox-content").html();
            var modal = $("#modal");

            $(modal).find(".modal-title").html('Otvorene Porudžbine');
            $(modal).find(".modal-body").html(html);
            $(modal).find("#save-modal").hide();
            $(modal).find("#close-modal").html(t('Zatvori'));
            $("body").addClass("modal-open");
            $(modal).show();
        });
    });

    $(document).on("click", ".chose-collection-pivot-report", function(e){

        e.preventDefault();

        $.get("/orders/chose-collection-pivot-report/"+$(this).attr('id'), function (response) {
            var html = $(response).find(".ibox-content").html();
            var modal = $("#modal");

            $(modal).find(".modal-title").html('Pregled kolekcija');
            $(modal).find(".modal-body").html(html);
            $(modal).find("#save-modal").hide();
            $(modal).find("#close-modal").html(t('Zatvori'));
            $("body").addClass("modal-open");
            $(modal).show();
        });
    });

    $(document).on("click", "#seasonsModal", function(e){

        e.preventDefault();

        $.get("/failures/chose-season-pivot-report", function (response) {
            var html = $(response).find(".ibox-content").html();
            var modal = $("#modal");

            $(modal).find(".modal-title").html('Pregled Sezona');
            $(modal).find(".modal-body").html(html);
            $(modal).find("#save-modal").hide();
            $(modal).find("#close-modal").html(t('Zatvori'));
            $("body").addClass("modal-open");
            $(modal).show();
        });
    });

    $(document).on("click", "#report-detailed", function (e) {
        e.preventDefault();

        var id = $(this).attr("data-id");
        var startDate = $("#startDate").val();
        var endDate = $("#endDate").val();

        $.ajax({
            url: "/operating-lists/get-detailed-report/" + id,
            type: "post",
            data: { startDate: startDate, endDate : endDate },
            success: function (data) {
                var html = $(data).find("#details");
                var modal = $("#modal");
                modal.find(".modal-dialog").width("75%");

                $(modal).find(".modal-title").html("");
                $(modal).find(".modal-body").html(html);
                $(modal).find("#save-modal").remove();
                $(modal).find("#close-modal").html(t('Zatvori'));
                $("body").addClass("modal-open");
                initDataTable();
                $(modal).show();
            }
        });
    });

    $(document).on("click", "#createProduct", function () {
        var form = $("#product-form");
        var isValid = getFormValidation($(form));
        var formData = new FormData();

        var formElements = form.find("input, select, textarea").not("input[type='file']");
        formElements.each(function () {
            var name = $(this).attr("name");
            if (!(undefined == name && name == null)) {
                if ($.isArray($(this).val())) {
                    $.each($(this).val(), function (attrKey, attrVal) {
                        formData.append(name, attrVal);
                    });
                } else {
                    formData.append(name, $(this).val());
                }
            }
        });

        formData.append('code', $("#finalCode").val());

        var files = $(form).find("input[type='file']");
        $(files).each(function () {
            if ("" != $(this).val()) {
                var name = $(this).attr("name");
                formData.append(name, $("#" + name)[0].files[0]);
            }
        });

        sendPostAjaxCall($(form).attr("action"), formData)
    });

    $(document).on("click", "#copy-item", function () {
        $("#modal").find("#save-modal").attr("id", "save-modal-o-item");
    });

    $(document).on("click", "#add-comment-worker", function () {
        $("#modal").find("#save-modal").attr("id", "save-modal-o-worker");
    });

    $(document).on("click", ".tag-list li a", function (e) {
        e.preventDefault();

        let tag = $(this).text();
        let editorDiv = $("<p>asdasd</p>");
        $('#froala-editor').froalaEditor('html.insert',  tag , true);
    });

    $(document).on("click", "#save-modal-o-item", function (e) {
        e.preventDefault();
        var form = $(this).parents("form");
        var button = $(this);

        if (form.length == 0) {
            var modal = $(this).parents("#modal");
            form = $(modal).find("form");
        }

        var isValid = getFormValidation(form);
        var formData = new FormData(form.get(0));
        var files = form.find("input[type='file']");

        $(files).each(function (key, element) {
            if ("" != $(element).val()) {
                var name = $(element).attr("name");
                if (formData.has(name)) {
                    formData.delete(name);
                }

                formData.append(name, $(element)[0].files[0]);
            }
        });

        if (isValid) {
            $(button).addClass("disabled");
            var currentUrl = window.location.href;
            $.ajax({
                url: form.attr("action"),
                type: "post",
                data: formData,
                dataType: "json",
                enctype: "multipart/form-data",
                processData: false,
                contentType: false,
                success: function (data) {

                    if (!data.error) {
                        $.get(currentUrl, function (response) {
                            var content = $(response).find("#operating-lists");
                            var navbar = $(response).find(".navbar-top-links");
                            $("#operating-lists").replaceWith(content);
                            $(".navbar-top-links").replaceWith(navbar);
                            renderMessages(data.messages, "success");
                            setChosen();
                            initDataTable();
                            initProductStatus();
                            initProductOrderable();
                            iniQuestionnaire();
                            initTablsOnLoad();
                            initChecks();
                            initChosen();
                            initDropUploadOrders();
                            initDropZone();
                            reUploadImage();
                            initSwitch();
                            initOrderListTable();
                        });
                    } else {
                        $.get(currentUrl, function (response) {
                            var content = $(response).find("#conteoperationWorkernt");
                            var flashSession = $(response).find("#flash-session");
                            $("#content").replaceWith(content);
                            $("#flash-session").replaceWith(flashSession);
                            $.each(data.messages, function (key, message) {
                                showPushMessage(message.message, "danger");
                            });

                            grepSessionMessages();
                            initChecks();
                            initChosen();
                            initDataTable();
                            initDropUploadOrders();
                            initDropZone();
                            reUploadImage();
                            initOrderListTable();
                            $(".reUploadImage").trigger("change");
                        });
                    }
                },
                error: function (request, error) {
                    $.get(currentUrl, function (response) {
                        var content = $(response).find("#content");
                        var flashSession = $(response).find("#flash-session");
                        $("#content").replaceWith(content);
                        $("#flash-session").replaceWith(flashSession);
                        showPushMessage(t("An error was detected."), "error");
                        grepSessionMessages();
                        initProductStatus();
                        initProductOrderable();
                        initChecks();
                        initChosen();
                        initDataTable();
                        initDropUploadOrders();
                        initDropZone();
                        reUploadImage();
                    });
                }
            });
        }
    });

    $(document).on("click", ".read-more", function (e) {
        e.preventDefault();
        var excerpt = $(this).parents('.excerpt');
        var more = excerpt.siblings(".full-text");
        more.show();
        excerpt.hide();
    });

    $(document).on("change", "#season, #age, #collection, #productTypeCode", function () {
        var seasonVal = $("#season").val();
        var ageVal = $("#age").val();
        var collectionVal = $("#collection").val();
        var productTypeCodeVal = $("#productTypeCode").val();
        var productTypeCodeId = $("#productTypeCodeId").val();

        if (productTypeCodeVal && ageVal && collectionVal && seasonVal) {
            $.ajax({
                url: "/products/find-mapped-classifications-and-attributes",
                type: "post",
                dataType: "json",
                data: {season : seasonVal, collection : collectionVal, age : ageVal, productTypeCode : productTypeCodeVal, productTypeCodeId : productTypeCodeId},
                success: function (data) {
                    if (data.doubleSeason) {
                        $("#doubleSeason").val(data.doubleSeason).trigger("liszt:updated").trigger("chosen:updated");
                    }

                    if (data.manufacturer) {
                        $("#manufacturer").val(data.manufacturer).trigger("liszt:updated").trigger("chosen:updated");
                    }

                    if (data.modelProductType) {
                        $("#modelProductType").val(data.modelProductType).trigger("liszt:updated").trigger("chosen:updated");
                    }

                    if (data.oldCollection) {
                        $("#oldCollection").val(data.oldCollection).trigger("liszt:updated").trigger("chosen:updated").trigger("change");
                        $("#oldCollection").click();
                    }
                }
            })
        }
    });

    $(".complete").bind("paste", function(e) {
        setTimeout(function() {
            $(".complete").trigger("autocomplete");}, 0);
    });

    $(document).on("keyup.autocomplete", '*[data-search="true"]', function () {
        let dataTable = $(this).attr("data-table");
        let dataColumn = $(this).attr("name");
        let hiddenField = '';
        let dataReturnType = 'reference';

        if (this.hasAttribute("data-column")) {
            dataColumn = $(this).attr("data-column");
        }

        if (this.hasAttribute("data-return-type")) {
            dataReturnType = $(this).attr("data-return-type");
        }

        if (this.hasAttribute("data-hidden-field")) {
            hiddenField = $(this).attr("data-hidden-field");
        }


        $(this).autocomplete({
            minLength: 3,
            source: "/data/filter-by-term/" + dataTable + "/" + dataColumn + "/" + dataReturnType,
            select: function (event, ui) {

                if (hiddenField !== '' && $("#" + hiddenField).length > 0) {
                    $("#" + hiddenField).val(ui.item.id);
                }
            }
        });
    });

    $(document).on("keyup.autocomplete", '.complete', function () {
        $(this).autocomplete({
            minLength: 3,
            source: "/electron-cards/get-name-or-material-factories",
            select: function (event, ui) {
                if (ui.item.type == 'material') {

                    var $materialFactoryId = $("#materialFactoryId");
                    $materialFactoryId.val(ui.item.id);
                    $materialFactoryId.parents("form").find("select").attr("disabled", true).trigger("liszt:updated").trigger("chosen:updated");

                    // $.get("/material-factories/get-type-code-id/" + ui.item.id, function (response) {
                    //     var $materialTypeCodeId = $("#materialTypeCodeId");
                    //     $materialTypeCodeId.val(response.code)
                    //         .trigger("liszt:updated")
                    //         .trigger("chosen:updated").trigger("change");
                    //     var counter = 0;
                    //     $(document).ajaxComplete(function (event, request, settings) {
                    //         counter++;
                    //         if (counter == 2) {
                    //             $("#materialCodeId").val(response.childCode)
                    //                 .trigger("change")
                    //                 .trigger("liszt:updated")
                    //                 .trigger("chosen:updated");
                    //         }
                    //
                    //         if (counter == 3) {
                    //             $("#subMaterialCodeId").val(response.subCode)
                    //                 .removeAttr("disabled")
                    //                 .trigger("change")
                    //                 .trigger("liszt:updated")
                    //                 .trigger("chosen:updated")
                    //                 .attr("readonly", true);
                    //
                    //         }
                    //     });
                    // });

                    $("#subMaterialCodeId")
                        .trigger("liszt:updated")
                        .trigger("chosen:updated");
                } else {
                    // $("#materialTypeCodeId, #materialCodeId, #subMaterialCodeId")
                    //     .prop("selected", false).val("")
                    //     .removeAttr("readonly")
                    //     .removeAttr("disabled")
                    //     .trigger("liszt:updated")
                    //     .trigger("chosen:updated");
                }
            }
        });
    });

    $("#baseName").bind("paste", function(e) {
        setTimeout(function() {
            $("#baseName").trigger("autocomplete");}, 0);
    });

    $(document).on("keyup.autocomplete", '#productFilter', function () {
        if (this.value.length < 3) {
            addTypes();
        }

        $(this).autocomplete({
            minLength: 3,
            source: "/operating-list-items/get-base-products",
            select: function (event, ui) {
                $("#productId").val(ui.item.id);
                addTypes(ui.item.id);
            }
        });
    });

    $(document).on("keyup.autocomplete", '.gram-complete', function () {
        $(this).autocomplete({
            minLength: 2,
            source: "/electron-card-factories/get-distinct-grams",
            select: function (event, ui) {
                // $("#productId").val(ui.item.id);
                // addTypes(ui.item.id);
            }
        });
    });

    $(document).on("keyup.autocomplete", '#baseName', function () {
        $(this).autocomplete({
            minLength: 3,
            source: "/products/get-base-products",
            select: function (event, ui) {
                $("#baseProductId").val(ui.item.id);
            },
            search: function( event, ui ) {
                $("#baseProductId").val('');
            }
        });
    });

    $(document).on("keyup.autocomplete", '#operationListProductId', function () {
        $(this).autocomplete({
            minLength: 3,
            source: "/operating-lists/get-products",
            select: function (event, ui) {
                $("#hiddenProductId").val(ui.item.id);
            },
        });
    });

    $(document).on("keyup.autocomplete", '#farbenCardProductId', function () {
        $(this).autocomplete({
            minLength: 3,
            source: "/operating-lists/get-products",
            select: function (event, ui) {
                $("#hiddenProductId").val(ui.item.id);
            },
        });
    });

    $("#materialFactoryIdSearch").bind("paste", function(e) {
        setTimeout(function() {
            $("#materialFactoryIdSearch").trigger("autocomplete");}, 0);
    });

    $(document).on("keyup.autocomplete", '#materialFactoryIdSearch', function () {
        var $this = $(this);
        $(this).autocomplete({
            minLength: 3,
            source: "/material-factories/find-by-term",
            select: function (event, ui) {
                let find = $this.parent().next().find("input");
                    var hidden = $this.parent().find("#materialFactoryId");
                // if (ui.item.id) {
                console.log(hidden);
                find.val(ui.item.costs);
                    hidden.val(ui.item.id);

                    var consumptionVal = parseFloat($this.parent().next().next().find("input").val());
                    if (consumptionVal) {
                        $this.parent().next().next().next().find("input").val((consumptionVal.replace(/,/g, '.') * ui.item.costs.replace(/,/g, '.')).toFixed(3));
                    }

                    find.attr("readonly", true);
                // }
            },
            search: function( event, ui ) {
                let find = $this.parent().next().find("input");
                find.removeAttr("readonly");
                find.val("");
                var hidden = $this.parent().find("#materialFactoryId");
                hidden.val('');
            }
        });
    });

    $("#materialFactoryIdSearchFarben").bind("paste", function(e) {
        setTimeout(function() {
            $("#materialFactoryIdSearchFarben").trigger("autocomplete");}, 0);
    });

    $(document).on("keyup.autocomplete", '#materialFactoryIdSearchFarben', function () {
        var $this = $(this);
        $(this).autocomplete({
            minLength: 3,
            source: "/material-factories/find-by-term",
            select: function (event, ui) {
                let find = $this.parent().next().find("input");
                var hidden = $this.parent().find("#materialFactoryId");
                // if (ui.item.id) {
                console.log(hidden);
                hidden.val(ui.item.id);
            },
            search: function( event, ui ) {
                let find = $this.parent().next().find("input");
                find.val("");
                var hidden = $this.parent().find("#materialFactoryId");
                hidden.val('');
            }
        });
    });

    $(document).on("change", "#simProductIdCalculations", function () {
        $.get("/product-calculations/get-similar-product/" + this.value, function (response) {
            $("#productCalculationContainer").html($(response).find("#productCalculationContainer"));
        });
    });

    $(document).on("keyup.autocomplete, paste", '#productIdCalculations', function (event) {
        $(this).autocomplete({
            minLength: 3,
            autoFocus: false,
            source: "/product-calculations/find-product",
            select: function (event, ui) {
                $("#productId").val(ui.item.id);
            },
        });
    });

    $(document).on("change", "#farbenCardProductId", function () {
        var data = {productId : $("#hiddenProductId").val(), farbenCardProductId : this.value};
        $.ajax({
            url: "/order-totals/get-total-product",
            data: data,
            type: "post",
            dataType: "json",
            success: function (response) {
                if (response) {
                    $("#qtyTotal").val(response.komada);
                }
            }
        });
    });

    $(document).on("change", "#operationListProductId", function () {
        var data = {productId : $("#productId").val(), operatingListItemTypeId : this.value};
        $.ajax({
            url: "/operating-list-items/list-prices",
            data: data,
            type: "post",
            dataType: "json",
            success: function (response) {
                if (response) {
                    $("#pieceTime").val(response.pieceTime);
                    $("#piecesHour").val(response.piecesHour);
                    $("#piecePrice").val(response.piecePrice);
                }
            }
        });
    });

    $(document).on("change", "#operatingListItemTypeId", function () {
        var data = {productId : $("#productId").val(), operatingListItemTypeId : this.value};
        $.ajax({
            url: "/operating-list-items/list-prices",
            data: data,
            type: "post",
            dataType: "json",
            success: function (response) {
                if (response) {
                    $("#pieceTime").val(response.pieceTime);
                    $("#piecesHour").val(response.piecesHour);
                    $("#piecePrice").val(response.piecePrice);
                }
            }
        });
    });

    $(document).on("change", "#models", function () {
        let val = $(this).val();
        $("#entityObjectName").val(val);

        $.ajax({
            url: "/emails/read-properties/" + val,
            type: "post",
            dataType: "json",
            success: function (data) {
                let jumbotron = $(".jumbotron ul");
                jumbotron.empty();

                $.each(data, function ($key, value) {
                    let span = '<li><a href="#" class="">{{ '+ value +' }}</a></li>';
                    jumbotron.append($(span));
                });
            }
        });
    });

    $(document).on("submit", "#email-template-save", function (e) {
        $("#body").val($(".fr-view").html());
        $(this).submit();
    });

    $(document).on("change", "#controller", function () {
        let val = $(this).val();
        let $element = $("#action");


        $.ajax({
            url: "/emails/read-actions/" + val,
            type: "post",
            dataType: "json",
            success: function (data) {
                $element.find('option').remove();
                $element.append($("<option/>", {
                    value: 0,
                    text: '-- Izaberite --'
                }));

                $.each(data, function (key, value) {
                    $element.append($("<option/>", {
                        value: key,
                        text: value
                    }));
                });

                $element.trigger("liszt:updated")
                    .trigger("chosen:updated");
            }
        });
    });

    $(document).on("click", "#addProductCalculationItem", function (e) {
        e.preventDefault();
        var table = $(".calculationItems");
        var tFootTd = $(".dataTables_empty");
        var tBody = $("tbody", table);

        if (tFootTd.length) {
            tFootTd.parent().remove();
        }

        var rows = table.find('tbody tr').length + 1;

        var tr = $("tr:last", tBody);
        var row = "<tr class=''>";
        row += "<td class='col-xs-4'>" +
            "<input type='hidden' class='form' name='create["+ rows +"][materialFactoryId]' id='materialFactoryId'/>" +
            "<input type='hidden' class='form' name='create["+ rows +"][name]' id='name'/>" +
            "<input style='width:100%!important' type='text' class='required form-control' id='materialFactoryIdSearch' name='materialFactoryIdSearch'/>" +
            "</td>";
        row += "<td class='col-xs-2'>" +
            "<input style='width:100%!important' type='text' class='form-control' id='costs' name='create["+ rows +"][costs]'/>" +
            "</td>";
        row += "<td class='col-xs-2'>" +
            "<input style='width:100%!important' type='text' class='form-control' id='consumption' name='create["+ rows +"][consumption]'/>" +
            "</td>";
        row += "<td class='col-xs-2'>" +
            "<input style='width:100%!important' type='text' class='form-control' id='total' readonly name='create["+ rows +"][total]'/>" +
            "</td>";
        row += "<td class='col-xs-2 text-center smaller-80'>" +
                "<a href='#' id='deleteRow' class='btn btn-circle btn-danger'>" +
                    "<i class='fa fa-remove'></i>" +
                "</a>" +
            "</td>";
        row += "</tr>";

        if (tr.length) {
            tr.after($(row));
        } else {
            tBody.append($(row));
        }
    });

    $(document).on("click", "#addFarbenCardItem", function (e) {
        e.preventDefault();
        var table = $(".farbenCardItems ");
        var tFootTd = $(".dataTables_empty");
        var tBody = $("tbody", table);

        if (tFootTd.length) {
            tFootTd.parent().remove();
        }

        var rows = table.find('tbody tr').length + 1;

        var tr = $("tr:last", tBody);
        var row = "<tr class=''>";
        row += "<td class='col-xs-8'>" +
            "<input type='hidden' class='form' name='create["+ rows +"][materialFactoryId]' id='materialFactoryId'/>" +
            "<input type='hidden' class='form' name='create["+ rows +"][name]' id='name'/>" +
            "<input style='width:100%!important' type='text' class='required form-control' name='create["+ rows +"][materialFactoryIdSearchFarben]' id='materialFactoryIdSearchFarben' />" +
            "</td>";
        row += "<td class='col-xs-2'>" +
            "<input style='width:100%!important' type='text' class='form-control' id='description' name='create["+ rows +"][description]'/>" +
            "</td>";
        row += "<td class='col-xs-2 text-center smaller-80'>" +
            "<a href='#' id='deleteRow' class='btn btn-circle btn-danger'>" +
            "<i class='fa fa-remove'></i>" +
            "</a>" +
            "</td>";
        row += "</tr>";

        if (tr.length) {
            tr.after($(row));
        } else {
            tBody.append($(row));
        }
    });

    $(document).on("click", "#deleteRow", function(e){
        e.preventDefault();
        var row = $(this).parents("tr");
        row.fadeOut(300, function() { row.remove(); });
    });

    $(document).on("click", ".read-less", function (e) {

        e.preventDefault();
        var fullText = $(this).parents('.full-text');
        var excerpt = fullText.siblings(".excerpt");
        excerpt.show();
        fullText.hide();
    });

    $(document).on("click", "#saveProductCalculationItems", function(e){
        e.preventDefault();

        var table = $("#formCalculationItems");
        sendPostAjaxCall('/product-calculations/add-items', table.serialize());

    });

    $(document).on("click", "#saveFarbenCardItems", function(e){
        e.preventDefault();

        var table = $("#formFarbenCardItems");
        sendPostAjaxCall('/farben-cards/add-items', table.serialize());

    });

    $(document).on("keyup", "#consumption", function(e){
        var consumptionVal = parseFloat($(this).val().replace(",", "."));
        var val = parseFloat($(this).parent().prev().find("input").val().replace(",", "."));
        $(this).parent().next().find("input").val((val * consumptionVal).toFixed(3));
    });

    $(document).on("keyup", "#materialFactoryIdSearch", function(e){
        $(this).parent().find("input#name").val($(this).val());
    });

    $(document).on("keyup", "#materialFactoryIdSearchFarben", function(e){
        $(this).parent().find("input#name").val($(this).val());
    });

    $(document).on("keyup", "#costs", function(e){
        var costsVal = parseFloat($(this).val().replace(",", "."));
        var consumptionVal = parseFloat($(this).parent().next().find("input").val());
        $(this).parent().next().next().find("input").val((consumptionVal * costsVal).toFixed(3));
    });

    /**
     * AJAX submit modal form
     */
    $(document).on("click", "#save-modal", function (e) {
        e.preventDefault();
        var form = $(this).parents("form");
        var button = $(this);

        if (form.length == 0) {
            var modal = $(this).parents("#modal");
            form = $(modal).find("form");
        }

        prepareFormDataAndSendPostCall(form, button);
    });

    /**
     * AJAX submit modal form
     */
    $(document).on("click", ".ajax-form", function (e) {

        e.preventDefault();

        var form = $(this).parents("form");
        var isValid = getFormValidation($(form));
        var button = $(this);
        var formData = new FormData();
        var formElements = form.find("input, select, textarea").not("input[type='file']");

        formElements.each(function () {
            var name = $(this).attr("name");
            if (!(undefined == name && name == null)) {
                if ($.isArray($(this).val())) {
                    $.each($(this).val(), function (attrKey, attrVal) {
                        formData.append(name, attrVal);
                    });
                } else {
                    formData.append(name, $(this).val());
                }
            }
        });

        var files = $(form).find("input[type='file']");
        $(files).each(function () {
            if ("" != $(this).val()) {
                var name = $(this).attr("name");
                formData.append(name, $("#" + name)[0].files[0]);
            }
        });

        if (isValid) {
            $(button).addClass("disabled");
            sendPostAjaxCall($(form).attr("action"), formData);
        }
    });

    $(document).on("click", ".destroy-modal", function (e) {
        e.preventDefault();
        var modal = $(this).parents("#modal");
        destroyModal(modal);
    });

    $(document).on("focusout", "#barcode-warehouse", function () {
        let barcode = $(this).val().trim().toLowerCase();
        if (!barcode || (barcode.length !== 13 && barcode.length !== 14)) {
            return false;
        }

        $(this).focus().val("");
        let warehouseOrderId = $("#warehouse-order-id").val();
        let barcodeStorage = window.localStorage;
        let warehouseOrderStorage = barcodeStorage.getItem(warehouseOrderId);
        let storedBarcodes = JSON.parse(warehouseOrderStorage);

        if (storedBarcodes === null) {
            storedBarcodes = {};

            $(".warehouse-order-item-table").remove();
            let header = '<div class="feed-element">' +
                '<div class="col-lg-6 bigger-120"><b>Proizvod</b></div>' +
                '<div class="col-lg-2 text-center bigger-120"><b>Veličina</b></div>' +
                '<div class="col-lg-2 text-center bigger-120"><b>Brojač</b></div>' +
                '<div class="col-lg-2 text-center bigger-120"><b>Akcija</b></div>';

            $(".feed-activity-list").append(header);

            let footer = '<div class="feed-element">' +
                '<div class="col-lg-12 m-t-md">' +
                    '<a onclick="return false;" id="check-warehouse-order-items" class="btn btn-label btn-success">Snimi stavke</a>' +
                '</div>' +
                '</div>';
            $(".feed-activity-list").append(footer);
        }

        let initialBarcode = false;
        if(typeof storedBarcodes[barcode] === 'undefined') {
            initialBarcode = true;
            storedBarcodes[barcode] = 1;
        }

        if (initialBarcode) {
            $.get("/products/get-product-by-barcode/" + barcode, function(response) {
                if (response.error) {
                    $(this).focus().val("");
                    renderMessages(response.messages, "danger");
                } else {
                    barcodeStorage.setItem(warehouseOrderId, JSON.stringify(storedBarcodes));

                    let row = '<div class="feed-element no-padding" id="'+ barcode.toLowerCase() + '">' +
                        '<div class="col-lg-6 col-xs-5">' + response.product + '</div>' +
                        '<div class="col-lg-2 col-xs-2 text-center">' + response.size + '</div>' +
                        '<div class="col-lg-2 col-xs-2 text-center" id="row-counter">1</div>' +
                        '<div class="col-lg-2 col-xs-2 text-center">' +
                            '<a id="decrease-warehouse-order-item" class="btn btn-info btn-circle" onclick="return false;" data-id="'+ barcode +'"><i class="fa fa-minus"></i></a>' +
                            '<a href="#" id="remove-warehouse-order-item" data-id="'+ barcode +'" class="btn btn-danger btn-circle"><i class="fa fa-times"></i></a>' +
                        '</div>';

                    $(".feed-element:eq(0)").after(row);
                }

                $(this).focus().val("");
            });
        } else {
            storedBarcodes[barcode]++;
            barcodeStorage.setItem(warehouseOrderId, JSON.stringify(storedBarcodes));
            $("#" + barcode).find("#row-counter").empty().html(storedBarcodes[barcode]);

            let minusDiv = $("#" + barcode).find("#decrease-warehouse-order-item");
            if (minusDiv.length === 0) {
                $("#" + barcode).find("#row-counter").next().append('<a id="decrease-warehouse-order-item" class="btn btn-info btn-circle" onclick="return false;" data-id="'+ barcode +'"><i class="fa fa-minus"></i></a>');
            }
            $(this).focus().val("");
        }

        $(this).focus().val("");
    });

    $("#barcode-form").submit(function(e){
        e.preventDefault();
        $("#barcode-warehouse").focus().val("");
    });

    $(document).on("click", "#remove-warehouse-order-item", function(){
        let warehouseOrderId = $("#warehouse-order-id").val();
        let barcodeStorage = window.localStorage;
        let warehouseOrderStorage = barcodeStorage.getItem(warehouseOrderId);
        let storedBarcodes = JSON.parse(warehouseOrderStorage);
        let barcode = $(this).attr("data-id");

        if (storedBarcodes === null) {
            return false;
        }

        if(typeof storedBarcodes[barcode] === 'undefined') {
            return false;
        }

        storedBarcodes[barcode] = 0;
        $(this).parents(".feed-element").find("#row-counter").html("0");
        barcodeStorage.setItem(warehouseOrderId, JSON.stringify(storedBarcodes));
    });

    $(document).on("click", "#scan-warehouse-order-barcode", function(){
       $("#barcode").focus();
    });

})(jQuery);

/**
 *
 * @param {string} controller
 * @param {object} e
 */
function getCreateForm(controller, e) {
    var getUrl = "/" + controller + "/get-create-form";
    var postUrl = "/" + controller + "/create";
    getForm(getUrl, postUrl, e, "plus");
}

/**
 *
 * @param {string} controller
 * @param {int} id
 * @param {object} e
 */
function getUpdateForm(controller, id, e) {
    var getUrl = "/" + controller + "/get-update-form/" + id;
    var postUrl = "/" + controller + "/update/" + id;
    getForm(getUrl, postUrl, e, "edit");
}

/**
 *
 * @param {string} controller
 * @param {string} propertyName
 * @param {string} propertyValue
 * @param {object} e
 */
function getCustomCreateForm(controller, propertyName, propertyValue, e) {
    var getUrl = "/" + controller + "/get-create-form/" + propertyName + "/" + propertyValue;
    var postUrl = "/" + controller + "/create/" + propertyName + "/" + propertyValue;
    getForm(getUrl, postUrl, e, "plus");
}

/**
 *
 * @param {string} controller
 * @param {int} id
 * @param {object} e
 */
function getDeleteForm(controller, id, e) {

    e.preventDefault();

    var url = "/" + controller + "/delete/" + id;
    swal({
        title: t("Da li ste sigurni?!"),
        text: "",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: t("Odustani"),
        confirmButtonColor: "#da180f",
        confirmButtonText: t("Da, Obriši!"),
        closeOnConfirm: true
    }, function () {
        sendPostAjaxCall(url, {})
    });
}

function getDeleteWarehouseItem(warehouseOrderId, barcode, warehouseOrderItemId, e) {

    e.preventDefault();

    var url = "/warehouse-order-items/delete/" + warehouseOrderItemId;
    swal({
        title: t("Da li ste sigurni?!"),
        text: "",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: t("Odustani"),
        confirmButtonColor: "#da180f",
        confirmButtonText: t("Da, Obriši!"),
        closeOnConfirm: true
    }, function () {
        let barcodeStorage = window.localStorage;
        let warehouseOrderStorage = barcodeStorage.getItem(warehouseOrderId);
        let storedBarcodes = JSON.parse(warehouseOrderStorage);

        if (storedBarcodes === null) {
            return false;
        }

        if(typeof storedBarcodes[barcode] === 'undefined') {
            return false;
        }

        delete storedBarcodes[barcode];
        barcodeStorage.setItem(warehouseOrderId, JSON.stringify(storedBarcodes));
        sendPostAjaxCall(url, {})
    });
}

/**
 *
 * @param {string} controller
 * @param {int} id
 * @param {object} e
 */
function getCustomDeleteForm(url, e) {

    e.preventDefault();
    swal({
        title: t("Da li ste sigurni?!"),
        text: "",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: t("Odustani"),
        confirmButtonColor: "#da180f",
        confirmButtonText: t("Da, Obriši!"),
        closeOnConfirm: true
    }, function () {
        sendPostAjaxCall(url, {})
    });
}

/**
 *
 * @param {int} id
 * @param {object} e
 */
function unsubscribeNotification(id, e) {

    e.preventDefault();

    var url = "/user-notifications/unsubscribe/" + id;
    swal({
        title: t("Da li ste sigurni da želite odjavite izabranu notifikaciju?!"),
        text: "",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: t("Odustani"),
        confirmButtonColor: "#da180f",
        confirmButtonText: t("Da, odjavi me!"),
        closeOnConfirm: true
    }, function () {
        sendPostAjaxCall(url, {})
    });
}

/**
 *
 * @param {int} id
 * @param {object} e
 */
function unsubscribeMessage(id, e) {

    e.preventDefault();

    var url = "/user-notifications/deactivate-message/" + id;
    swal({
        title: t("Da li ste sigurni da želite odjavite izabranu notifikaciju?!"),
        text: "",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: t("Odustani"),
        confirmButtonColor: "#da180f",
        confirmButtonText: t("Da, odjavi me!"),
        closeOnConfirm: true
    }, function () {
        sendPostAjaxCall(url, {})
    });
}

/**
 *
 * @param {string} url
 * @param {object} e
 */
function getAjaxForm(url, e) {

    e.preventDefault();

    var event = e.srcElement || e.target;
    var title = t("Confirm");

    $(event).attr("data-backdrop", "static")
        .attr("data-toggle", "modal")
        .attr("data-target", "#modal");

    var modal = $("#modal");
    var body = '<form action="/' + url + '" method="post">'
        + t("Da li ste sigurni da želite da izvršite ovu akciju?") + '</form>';

    $(modal).find(".modal-title").html(title);
    $(modal).find(".modal-body").html(body);
    $(modal).find("#save-modal").html(t('Da'));
    $(modal).find("#close-modal").html(t('Ne'));
    $("body").addClass("modal-open");
    $(modal).show();
}

/**
 *
 * @param {string} getUrl
 * @param {string} postUrl
 * @param {object} e
 * @param {string} ico
 */
function getForm(getUrl, postUrl, e, ico) {

    e.preventDefault();
    toastr.clear();

    var event = e.srcElement || e.target;
    var title = "";
    var currentUrl = window.location.href;
    $.ajax({
        url: getUrl,
        type: "get",
        dataType: "json",
        success: function (data) {
            var modal = $("#modal");

            if (data.error) {
                if (data.messages) {
                    renderMessages(data.messages, "danger");
                }
            } else {

                $(event).attr("data-backdrop", "static")
                    .attr("data-toggle", "modal")
                    .attr("data-target", "#modal");

                if (typeof title === "undefined" || title == "")
                    title = $.trim($(event).attr("title")) || $.trim($(event).parent().attr("title"));

                if (typeof title === "undefined" || title == "")
                    title = $.trim($(event).text()) || $.trim($(event).parent().text());

                if (typeof title === "undefined" || title == "")
                    title = $.trim($(event).parents(".ibox").children(".ibox-title").find("h5").html());

                var body = renderForm(data, postUrl);
                $(modal).find(".modal-title").html("<i class='fa fa-" + ico + "'></i> " + title);
                $(modal).find(".modal-body").html(body);
                let $datepicker = $(".datepicker");

                $datepicker.each(function (index, elem) {
                   var $element = $(elem);

                    if (!$element.hasClass("flatpickr-input")) {
                        $element.flatpickr({
                            altInput: true,
                            'static': true,
                            altFormat: "F j, Y",
                            allowInput: true,
                            locale: {
                                firstDayOfWeek: 1,
                                weekdays: {
                                    shorthand: ['Ned', 'Pon', 'Uto', 'Sre', 'Čet', 'Pet', 'Sub'],
                                    longhand: ['Nedelja', 'Ponedeljak', 'Utorak', 'Sreda', 'Četvrtak', 'Petak', 'Subota'],
                                },
                                months: {
                                    shorthand: ['Jan', 'Feb', 'Mart', 'Apr', 'Maj', 'Jun', 'Jul', 'Ag', 'Sep', 'Okt', 'Nov', 'Dec'],
                                    longhand: ['Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust', 'Septembar', 'Oktobar', 'Novembar', 'Decembar'],
                                },
                            },
                            onChange: function(selectedDates, dateStr, instance) {
                                $(instance.element).parents(".form-group").next().find("textarea, select, input").filter(':visible').focus();
                            },
                        });
                    }

                });


                let $datetimepicker = $(".datetimepicker");
                $datetimepicker.each(function (index, elemTime) {
                    var $elementTime = $(elemTime);

                    if (!$elementTime.hasClass("flatpickr-input")) {
                        $elementTime.flatpickr({
                            enableTime: true,
                            altInput: true,
                            'static': true,
                            altFormat: "F j, Y h:i K",
                            allowInput : true,
                            locale: {
                                firstDayOfWeek: 1,
                                weekdays: {
                                    shorthand: ['Ned', 'Pon', 'Uto', 'Sre', 'Čet', 'Pet', 'Sub'],
                                    longhand: ['Nedelja', 'Ponedeljak', 'Utorak', 'Sreda', 'Četvrtak', 'Petak', 'Subota'],
                                },
                                months: {
                                    shorthand: ['Jan', 'Feb', 'Mart', 'Apr', 'Maj', 'Jun', 'Jul', 'Ag', 'Sep', 'Okt', 'Nov', 'Dec'],
                                    longhand: ['Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust', 'Septembar', 'Oktobar', 'Novembar', 'Decembar'],
                                },
                            },
                            onChange: function(selectedDates, dateStr, instance) {
                                $(instance.element).parents(".form-group").next().find("textarea, select, input").filter(':visible').focus();
                            },
                        });

                    }
                });

                validateForm();
                initTextEditor();
                setChosen();
                hideElement();
                $("body").addClass("modal-open");
                $(modal).show();
                initChosen();
                $( "input[data-autofocus]:first" ).focus();
            }
        },
        error: function (request, error) {
            $.get(currentUrl, function (response) {
                var content = $(response).find("#content");
                $("#content").replaceWith(content);
                destroyModal(modal);
                setChosen();
                showPushMessage(t('An error was detected.'), 'error');
            });
        }
    });
}

/**
 *
 * @param {object} element
 * @returns {boolean}
 */
function getFormValidation(element) {
    var next = true;
    var required = $(element).find(".required");

    $(required).parents(".form-group").removeClass("has-error");
    $(required).parents(".form-group").find(".help-block").html("");
    $(required).each(function () {
        if ($(this).val() == "" || $(this).val() == "0") {
            $(this).parents(".form-group").addClass("has-error");
            $(this).parents(".form-group").find(".help-block").html(t("Obavezno polje"));
            next = false;
        }
    });

    $("textarea").each(function () {
        if (html = $(this).next(".note-editor").find(".note-editable").html()) {
            $(this).html(html);
        }
    });

    return next;
}

/**
 *
 * @param {object} messages
 * @param {string} type
 */
function renderMessages(messages, type) {

    var modal = $("#modal");
    modal.find(".help-block").html(null);
    modal.find(".form-group").removeClass("has-error");
    $.each(messages, function (key, message) {
        if (!message.field || modal.length == 0) {
            showPushMessage(message.message, type);
            destroyModal(modal);
        } else {
            if (message.field == 'description') {
                $(".editor").parents('.form-group')
                    .addClass('has-error')
                    .find('.help-block').html(message.message);
            } else {
                var el = modal.find("#" + filterName(message.field));
                if (!el.length) {
                    var msg = '';
                    if (typeof message.message == "object") {
                        $.each(message.message, function (k, text) {
                            msg += '<div class="alert alert-' + type + ' alert-dismissable">'
                                + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'
                                + text
                                + '</div>';
                        });
                    } else {
                        msg = '<div class="alert alert-' + type + ' alert-dismissable">'
                            + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'
                            + message.message
                            + '</div>';
                    }
                    $(modal).find(".flash-notification").append(msg);
                } else {
                    el.parents(".form-group")
                        .addClass("has-error")
                        .find(".help-block").html(message.message);
                }
            }
        }
    });
}

/**
 *
 * @param {object} data
 * @param {string} postUrl
 * @returns {string}
 */
function renderForm(data, postUrl) {

    var html = '<form id="ajax-form" class="form-horizontal" action="' + postUrl + '" method="post" enctype="multipart/form-data">';

    $.each(data, function (key, val) {

        var attributes = '';
        var required = '';
        var hasClass = false;
        var disabledOptions;

        if (val.attributes != '') {
            $.each(val.attributes, function (attrKey, attrVal) {
                if (attrKey === 'class') {
                    hasClass = true;
                    attrVal += ' form-control';
                }

                if (attrKey === 'disabled' && $.isArray(attrVal)) {
                    disabledOptions = attrVal;
                } else {
                    attributes += ' ' + attrKey + '="' + attrVal + '"';
                }
            });
        }

        if (!hasClass) {
            attributes += ' class="form-control"';
        }

        if (val.required) {
            required = '<span class="">*</span>';
        }

        html += '<div class="form-group' + (val.type == 'hidden' ? ' hidden' : '') + '">';

        if (val.label != '') {
            html += '<label class="col-sm-4 control-label no-padding-right'+ (attributes.indexOf('data-multiple') !== -1 ? ' hidden' : '') +'" for="' + filterName(val.name) + '">' + val.label + ': ' + required + '</label>';
        }

        html += '<div class="col-sm-'+ (val.label != '' ? '8' : '12') +'">';

        if (val.type == 'text' || val.type == 'password' || val.type == 'email' || val.type == 'hidden') {
            if (attributes.indexOf('data-multiple') !== -1) {
                html += '<div class="row hidden"><div class="col-xs-10 no-margin-right no-padding-right">';
                html += '<input name="' + val.name + '[]" id="' + filterName(val.id ? val.id : val.name) + '" type="' + val.type + '" value="' + val.default + '"' + attributes + ' />';
                html += '</div><div class="col-xs-1 no-padding no-margins">';
                html += '<a id="cloneOption" href="#" class="bigger-140 btn btn-circle btn-lg text-success"><i class="fa fa-plus"></i></a>';
                html += '</div></div>';
            } else {
                html += '<input name="' + val.name + '" id="' + filterName(val.id ? val.id : val.name) + '" type="' + val.type + '" value="' + val.default + '"' + attributes + ' />';
            }

        } else if (val.type == 'date') {
            html += '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>'
                + '<input name="' + val.name + '" id="' + filterName(val.id ? val.id : val.name) + '" type="text" value="' + val.default + '"' + attributes + ' /></div>';
        } else if (val.type == 'textarea') {
            html += '<textarea name="' + val.name + '" id="' + filterName(val.id ? val.id : val.name) + '" style="resize:vertical;min-height:150px; width:100%;" ' + attributes + '>' + val.default + '</textarea>';
        } else if (val.type == 'checkbox' || val.type == 'check') {
            html += '<input name="' + val.name + '" id="' + filterName(val.id ? val.id : val.name) + '" type="checkbox" value="' + val.default + '" class="js-switch" ' + (val.default ? ' checked="checked"' : '') + ' />';
        } else if (val.type == 'numeric' || val.type == 'number') {
            html += '<input name="' + val.name + '" id="' + filterName(val.id ? val.id : val.name) + '" type="number" value="' + val.default + '" ' + attributes + ' />';
        } else if (val.type == 'file') {
            html += '<input name="' + val.name + '" id="' + filterName(val.id ? val.id : val.name) + '" type="file"' + attributes + ' />';
        } else if (val.type == 'select' && val.options != undefined) {
            var arr = [];
            $.each(val.options, function (k, v) {
                arr.push({key: k, value: v});
            });

            arr = $.grep(arr, function (elem, index) {
                if (elem.key !== 0 || elem.key != '') {
                    return elem.key
                }
            });

            arr.unshift({key: "", value: t("-- Izaberite --")});

            html += '<select name="' + val.name + '" id="' + filterName(val.name) + '"' + attributes + '>';
            $.each(arr, function (k, v) {

                var selected = '';
                var disabled = '';

                if ($.isPlainObject(v.value)) {
                    var optionGroup = v.key.substring(0, 1).toUpperCase() + v.key.substring(1);
                    html += '<optgroup label="' + optionGroup + '">';
                    $.each(v.value, function (optKey, optValue) {
                        if ($.trim(optKey) == val.default) {
                            selected = ' selected="selected"';
                        }

                        var value = optValue.substring(0, 1).toUpperCase() + optValue.substring(1);
                        html += '<option value="' + optKey  + '"' + selected + '>' + value + '</option>';
                    });
                    html += '</optgroup>';
                } else {
                    if ($.isArray(val.default)) {
                        if (-1 !== $.inArray(v.key, val.default)) {
                            selected = ' selected="selected"';
                        }
                    } else {
                        if ($.trim(v.key) == val.default) {
                            selected = ' selected="selected"';
                        }
                    }

                    if (-1 !== $.inArray(v.key, disabledOptions)) {
                        disabled = ' disabled="disabled"';
                    }

                    var value = v.value.substring(0, 1).toUpperCase() + v.value.substring(1);
                    html += '<option value="' + v.key + '"' + selected + disabled + '>' + value + '</option>';
                }
            });
            html += '</select>';
        } else {
            html += '<input name="' + val.name + '" id="' + filterName(val.name) + '" type="' + val.type + '" value="' + val.default + '" ' + attributes + ' />';
        }

        html += '<div class="help-block m-b-none"></div></div></div>';
    });

    html += '</form>';

    return html;
}

/**
 *
 */
function validateForm() {
    $("form .required").on("blur", function () {
        if ($(this).val() == "" || $(this).val() == "0") {
            $(this).parents(".form-group").addClass("has-error");
            $(this).parents(".form-group").find(".help-block").html(t("Obavezno polje"));
        } else {
            $(this).parents(".form-group").removeClass("has-error");
            $(this).parents(".form-group").find(".help-block").html("");
        }
    });

    initSwitch();
}

/**
 *
 * @param {object} modal
 */
function destroyModal(modal) {
    $(modal).find(".modal-title").html(null);
    $(modal).find(".modal-body").html(null);
    $(modal).find(".flash-notification").html(null);
    $(modal).find("#save-modal").removeClass("disabled");
    $(modal).hide();
    $("body").removeClass("modal-open");
    $(".modal-backdrop").remove();
}

function initSwitch()
{
    $(".js-switch").each(function (key, elem) {
        if (!elem.hasAttribute("data-switchery")) {
            var check = elem;
            new Switchery(elem, {color: "#1AB394"});
            elem.onchange = function () {
                $(check).attr('checked', check.checked);
                $(check).val(check.checked ? 1 : 0);
            }
        }
    });
}

/**
 *
 */
function showDatePicker() {

    $(".datepicker").flatpickr({
        locale: {
            firstDayOfWeek: 1,
            weekdays: {
                shorthand: ['Ned', 'Pon', 'Uto', 'Sre', 'Čet', 'Pet', 'Sub'],
                longhand: ['Nedelja', 'Ponedeljak', 'Utorak', 'Sreda', 'Četvrtak', 'Petak', 'Subota'],
            },
            months: {
                shorthand: ['Jan', 'Feb', 'Mart', 'Apr', 'Maj', 'Jun', 'Jul', 'Ag', 'Sep', 'Okt', 'Nov', 'Dec'],
                longhand: ['Januar', 'Februar', 'Mart', 'April', 'Maj', 'Jun', 'Jul', 'Avgust', 'Septembar', 'Oktobar', 'Novembar', 'Decembar'],
            },
        },
        onChange: function(selectedDates, dateStr, instance) {
            $(instance.element).parents(".form-group").next().find("textarea, select, input").filter(':visible').focus();
        },
    });
}

/**
 *
 * @param {string} msg
 * @param {string} type
 */
function showPushMessage(msg, type) {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "preventDuplicates": true
    };

    if (type == 'success') {
        toastr.success(msg);
    } else if (type == 'warning') {
        toastr.warning(msg);
    } else {
        toastr.error(msg);
    }
}

/**
 * {return} boolean
 */
function grepSessionMessages() {
    var sessionHolder = $("#flash-session");
    var returnValue = false;

    $(sessionHolder).hide();
    $(sessionHolder).find("div").each(function () {
        var msg = $(this).text();
        if ("" != msg) {
            returnValue = true;
            if ($(this).hasClass("alert-danger")) {
                showPushMessage(msg, "error");
            } else {
                showPushMessage(msg, "success");
            }
        }
    });

    return returnValue;
}

/**
 *
 */
function initTextEditor() {
    $(".summernote").summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['fontsize', ['fontsize']],
        ]
    });
}

/**
 *
 * @param {string} string
 * @returns {string}
 */
function t(string) {
    var translations = {
        'Confirm' : 'Potvrdi'
    };

    if (string in translations) {
        return translations[string];
    }

    return string;
}

/**
 *
 */
function setChosen() {
    $(".chosen-select").chosen({
        placeholder_text_multiple: t('-- Izaberite --')
    });

    $(".result-selected").each(function(index, value){
        console.log(index, value)
    })
}

/**
 *
 */
function initTouchSpin() {
    $("input[type='numeric']").TouchSpin({
        verticalbuttons: true,
        verticalupclass: 'glyphicon glyphicon-plus',
        verticaldownclass: 'glyphicon glyphicon-minus',
        max: 100000
    });
}

/**
 *
 * @param {string} name
 * @returns {string}
 */
function filterName(name) {
    return name.replace(/\W+/gi, "-");
}



/**
 *
 * @param {object} elem
 * @param {object} appendTo
 */
function search(elem, appendTo) {

    var msgNoResults = "<div class='no-results'>" + t('No result found...') + "</div>";

    $.ajax({
        url: "/searches/search",
        data: {'term': elem.val()},
        type: "post",
        dataType: "json",
        success: function (response) {

            appendTo.html(null);

            if (response.length > 0) {
                $.each(response, function (index, item) {
                    var resultHtml = '<a href="' + item.url + '" >';
                    resultHtml += item.name + "<span class='pull-right'>" + item.type + "</span>";
                    resultHtml += "</a>";
                    appendTo.append(resultHtml);
                });

            } else {
                appendTo.html(msgNoResults);
            }

            appendTo.animate({
                opacity: "show"
            }, "slow");
        },
        error: function () {
            appendTo
                .html(msgNoResults)
                .animate({
                    opacity: "show"
                }, "slow");
        }
    });
}

function smoothlyMenu() {
    if (!$("body").hasClass("mini-navbar") || $("body").hasClass("body-small")) {
        // Hide menu in order to smoothly turn on when maximize menu
        $("#side-menu").hide();
        // For smoothly turn on menu
        setTimeout(
            function () {
                $("#side-menu").fadeIn(500);
            }, 100);
    } else if ($("body").hasClass("fixed-sidebar")) {
        $("#side-menu").hide();
        setTimeout(
            function () {
                $("#side-menu").fadeIn(500);
            }, 300);
    } else {
        $("#side-menu").removeAttr("style");
    }
}

/**
 *
 * @param authResult
 */
function signInCallback(authResult) {
    if (authResult['code']) {

        // Send the code to the server
        $.ajax({
            type: "POST",
            url: "/auth/google-login",
            contentType: "application/octet-stream; charset=utf-8",
            success: function (result) {

                if (!result.error) {
                    window.location = '/' + result.url;
                } else {
                    renderMessages(result.messages, "danger");
                }
            },
            processData: false,
            data: authResult["code"]
        });
    }
}

/**
 *
 */
function facebookLogin() {

    FB.login(function (response) {

        if (response.authResponse) {

            FB.getLoginStatus(function (response) {

                if (response.status === "connected") {

                    $.ajax({
                        type: "POST",
                        url: "/auth/facebook-login",
                        data: response,
                        success: function (result) {

                            if (!result.error) {
                                window.location = '/' + result.url;
                            } else {
                                renderMessages(result.messages, "danger");
                            }
                        }
                    });

                } else if (response.status === "not_authorized") {
                    showPushMessage(t("We could not login via facebook account!"), "danger");
                }

            });
        }
    }, {scope: "publish_actions"});
}

function checkLoginState() {

    FB.getLoginStatus(function (response) {

        if (response.status === "connected") {
            $.ajax({
                type: "POST",
                url: "/auth/facebook-login",
                data: response,
                success: function (result) {

                    if (!result.error) {

                        if (result.url.length == 0) {
                            result.url = '/';
                        }

                        window.location = result.url;
                    } else {
                        renderMessages(result.messages, "danger");
                    }
                }
            });

        } else if (response.status === "not_authorized") {
            showPushMessage(t("We could not login via facebook account!"), "danger");
        }

    });
}


// A function to create the marker and set up the event window function
function createMarker(latlng, name) {
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        zIndex: Math.round(latlng.lat() * -100000) << 5
    });

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
    google.maps.event.trigger(marker, 'click');
    return marker;
}

var SITE = SITE || {};

/**
 *
 */
SITE.fileInputs = function () {
    var $this = $(this),
        $val = $this.val(),
        valArray = $val.split('\\'),
        newVal = valArray[valArray.length - 1],
        $button = $this.siblings(".button"),
        $fakeFile = $this.siblings(".file-holder");
    if (newVal !== "") {
        $button.text("Photo Chosen");
        if ($fakeFile.length === 0) {
            $button.before('' + newVal + '');
        } else {
            $fakeFile.text(newVal);
        }
    }
};


/**
 *
 */
function initFunctions() {
    if (typeof initCalendar == "function") {
        initCalendar();
    }
}

function blogSlider() {

    var dotsHolder = $(".slider-dots");
    if (dotsHolder.length > 0) {
        var dots = $("a", dotsHolder);

        $(dotsHolder).each(function (index, element) {

            var dots = $("a", $(element));
            setTimeout(function () {
                triggerClick(dots, 1, element)
            }, 5000);
        });


    }
}

function triggerClick(selector, index, element) {

    var element = $(element);
    var buttons = $(selector, element);
    var mod = index++ % $(buttons).length;
    var activeDot = $(buttons[mod], element);
    var holder = element.parents(".col-sm-4");

    $(buttons, element).removeClass("active");


    var dataSlide = activeDot.attr("data-dot");
    if (!activeDot.hasClass("active")) {
        activeDot.addClass("active");
    }

    var activeSlide = $(holder).find('.slider-box').filter("[data-slide='" + dataSlide + "']");

    $(holder).find('.slider-box').hide();
    activeSlide.show();

    setTimeout(function () {
        triggerClick(selector, index, element);
    }, 5000);
}

function loadMap(lat, lon) {
    var mapOptions = {
        center: new google.maps.LatLng(lat, lon),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.HYBRID
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"),
        mapOptions);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lon)
    });
    marker.setMap(map);
}


function cookieNotification() {
    window.cookieconsent_options = {
        "message": "HitWith.Pro uses cookies to help give you the best possible user experience. By continuing to browse this site you are agreeing to our cookie policy.",
        "dismiss": "Got it!",
        "learnMore": "Learn More",
        "link": "/privacy-policy",
        "theme": "/css/head.css",
        "disable": true
    };
}

/**
 *
 * @param lat
 * @param lng
 * @returns {jvm.Map}
 */
function initializeMap(lat, lng) {
    var mapOptions = {
        center: new google.maps.LatLng(lat, lng),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("mapCanvas"),
        mapOptions);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        draggable: true
    });
    marker.setMap(map);

    google.maps.event.addListener(marker, "dragend", function (event) {
        document.getElementById("latitude").value = this.getPosition().lat();
        document.getElementById("longitude").value = this.getPosition().lng();
    });

    return map;
}

/**
 *
 * @param lat
 * @param lng
 * @returns {jvm.Map}
 */
function updateMap(lat, lng) {
    var mapOptions = {
        center: new google.maps.LatLng(lat, lng),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("update-map"),
        mapOptions);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng)
    });
    marker.setMap(map);

    return map;
}

/**
 *
 * @param map
 */
function moveMarker(map) {
    marker = new google.maps.Marker({
        position: event.latLng,
        map: map,
        draggable: true
    });

    if (typeof oldMarker !== "undefined") {
        oldMarker.setMap(null);
    }

    var newLat = event.latLng.lat();
    var newLng = event.latLng.lng();

    document.getElementById("latitude").value = newLat;
    document.getElementById("longitude").value = newLng;

    oldMarker = marker;
    map.setCenter(event.latLng);

    google.maps.event.addListener(marker, "dragend", function (event) {
        document.getElementById("latitude").value = this.getPosition().lat();
        document.getElementById("longitude").value = this.getPosition().lng();
    });
}

function initDropZone() {

    if ($(".dropZoneSkull").length > 0) {

        $(".dropZoneSkull").each(function(key, form) {
            Dropzone.autoDiscover = false;
            Dropzone.options.myAwesomeDropzone = false;
            var titleAttr = $(form).attr("title");
            var titleMessage = (typeof titleAttr !== 'undefined' && titleAttr !== false) ? titleAttr : t("Prenesite sliku na box ili kliknite da upload-ujete");
            $(form).dropzone({
                autoProcessQueue: false,
                uploadMultiple: false,
                url: $(form).parent().attr("action"),
                maxFilesize: 200,
                acceptedFiles: ".png, .jpg, .xml, .json",
                HiddenFilesPath : 'body',
                addRemoveLinks: true,
                dictDefaultMessage: "<strong class='bigger-140'>"+ titleMessage +" <i class=\"fa fa-upload\"></i></strong>",
                dictRemoveFile: t('Obriši sliku'),
                paramName: $(form).find('input[type="file"]').attr("name"),
                init: function () {
                    var myDropzone = this;
                    var form = $(myDropzone.element).parent();
                    var submitButton = form.next().find("#start");

                    $(submitButton).click(function(e) {
                        e.preventDefault();
                        myDropzone.processQueue(); // Tell Dropzone to process all queued files.
                    });

                    this.on("success", function(file, serverFileName) {
                    });

                    // You might want to show the submit button only when
                    // files are dropped here:
                    this.on("addedfile", function(files, response) {
                        console.log(this, files, response);
                        // Show submit button here and/or inform user to click it.
                    });
                    this.on("sendingmultiple", function () {
                    });
                    this.on("successmultiple", function (files, response) {
                    });
                    this.on("errormultiple", function (files, response) {
                    });
                    this.on("sending", function(file, xhr, formData) {
                        var form = $(this.element).parents("form");
                        $("select, input, textarea", form).each(function(key, element) {

                            var name = $(element).attr("name");
                            if (!(undefined == name && name == null)) {
                                if ($.isArray($(element).val())) {
                                    $.each($(element).val(), function (attrKey, attrVal) {
                                        formData.append(name, attrVal);
                                    });
                                } else {
                                    formData.append(name, $(element).val());
                                }
                            }
                        });
                    });
                    this.on("complete", function (file, response1) {
                        if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                            var data = $.parseJSON(file.xhr.response);
                            $.get(window.location.href, function (response) {
                                var content = $(response).find("#content");
                                var navbar = $(response).find(".navbar-top-links");
                                $("#content").replaceWith(content);
                                $(".navbar-top-links").replaceWith(navbar);
                                renderMessages(data.messages, data.error ? 'danger' : 'success');
                                setChosen();
                                initTablsOnLoad();
                                initDataTable();
                                initDropZone();
                                initProductStatus();
                                initProductOrderable();
                                initProductStatus();
                                $(".nav-tabs a[href='#tabAdditionalSketch']").tab('show');
                            });
                        }
                    });
                }
            });
        });
    }
}

function rate() {
    $(".my-rating").each(function () {
        var objectTypeId = $(this).attr("data-entity-type-id"),
            objectId = $(this).attr("data-entity-id"),
            readOnly = parseInt($(this).attr("data-readonly")),
            average = parseFloat($(this).attr("data-average"));

        $(this).starRating({
            starSize: readOnly ? 20 : 42,
            initialRating: average,
            readOnly: readOnly,
            disableAfterRate: 0,
            callback: function (currentRating, $el) {

                $.ajax({
                    url: "/rates/create/" + objectTypeId + "/" + objectId,
                    data: {rate: currentRating},
                    type: "POST",
                    success: function (data) {

                        if (!data.error) {
                            renderMessages(data.messages, "success");
                        } else {
                            renderMessages(data.messages, "danger");
                        }
                    }
                })
            }
        });
    });
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var form = $(input).parents('form');
        reader.onload = function(e) {
            $("input[type='image']", form).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);

        $('<button type="submit" class="btn btn-success file-re-uploader">Upload</button>').insertAfter($(input));
    }
}

function reUploadImage()
{
    $(document).on("click", ".initReUpload", function(e) {
        e.preventDefault();
        var form = $(this).parents('form');
        $(".reUploadImage", form).click();
    });

    $(document).on("change", ".reUploadImage", function(e) {
        e.preventDefault();
        $(".file-re-uploader").remove();
        readURL(this);
    });

    $(document).on("click", ".file-re-uploader", function (e) {
        e.preventDefault();

        var form = $(this).parents("form");
        prepareFormDataAndSendPostCall(form, $(this));

        window.location = window.location.href;
    })
}

function uploadAvatar() {
    var image = $(".image-crop > img");
    var inputImage = $("#inputImage");
    if (window.FileReader) {
        inputImage.change(function () {
            var fileReader = new FileReader(),
                files = this.files,
                file;

            if (!files.length) {
                return;
            }

            image.cropper({
                aspectRatio: 1,
                preview: ".img-preview",
                dragMode: 'move',
                cropBoxResizable: false,
                minCropBoxWidth: 400,
                minCropBoxHeight: 400,
                done: function (data) {

                    var imgCropDiv = $(".image-crop");
                    imgCropDiv.siblings('.help-block.col-sm-12').remove();
                    $("label#download-false").attr("id", "download");
                    var imgDate = image.cropper("getImageData");

                    if (parseFloat(imgDate.naturalWidth) < 400 || parseFloat(imgDate.naturalHeight) < 400) {
                        imgCropDiv.parents('.form-group').addClass('has-error');
                        var error = $('<div class="help-block col-sm-12 no-padding col-sm-reset inline">' + t("Image must be at least 400x400") + '<div>');
                        imgCropDiv.after(error);
                        $("label#download").attr("id", "download-false");
                    }

                }
            });

            file = files[0];

            if (/^image\/\w+$/.test(file.type)) {
                fileReader.readAsDataURL(file);
                fileReader.onload = function () {
                    inputImage.val("");
                    image.cropper("reset", true).cropper("replace", this.result);
                };
            } else {
                renderMessages("Please choose an image file.", "danger");
            }
        });
    } else {
        inputImage.addClass("hide");
    }

    $(document).on("click", "#download", function () {

        var imageEncoded = image.cropper("getDataURL");

        $.ajax({
            url: "/auth/upload-avatar",
            data: {image: imageEncoded},
            type: "POST",
            success: function (data) {

                if (!data.error) {
                    $("img.img-circle").attr("src", data.img);
                    inputImage.addClass("hide");
                    image.cropper("destroy");
                    image.attr('src', data.img);
                    renderMessages(data.messages, "success");
                } else {
                    renderMessages(data.messages, "danger");
                }
            }
        });

    });
}

function initCalendar()
{
    var calendardiv = $("#calendar");
    if (calendardiv.length == 0) {
        return false;
    }

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    calendardiv.fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar
        drop: function () {
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }
        },
        events: [
            {
                title: 'All Day Event',
                start: new Date(y, m, 1)
            },
            {
                title: 'Long Event',
                start: new Date(y, m, d - 5),
                end: new Date(y, m, d - 2)
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: new Date(y, m, d - 3, 16, 0),
                allDay: false
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: new Date(y, m, d + 4, 16, 0),
                allDay: false
            },
            {
                title: 'Meeting',
                start: new Date(y, m, d, 10, 30),
                allDay: false
            },
            {
                title: 'Lunch',
                start: new Date(y, m, d, 12, 0),
                end: new Date(y, m, d, 14, 0),
                allDay: false
            },
            {
                title: 'Birthday Party',
                start: new Date(y, m, d + 1, 19, 0),
                end: new Date(y, m, d + 1, 22, 30),
                allDay: false
            },
            {
                title: 'Click for Google',
                start: new Date(y, m, 28),
                end: new Date(y, m, 29),
                url: 'http://google.com/'
            }
        ]
    });
}

function initDataTable()
{
    if ($(".dataTables").length == 0 || $.fn.DataTable.isDataTable('.dataTables')) {
        return false;
    }

    $(".dataTables").DataTable({
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // converting to interger to find total
            let intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            let tds = $("*[data-footer='true']", row);
            $( api.column(0).footer() ).html('Ukupno');

            $.each(tds, function($index, $elem){
                if ($elem.hasAttribute("data-column") && $index != 0) {
                    let columnIndexToSearch = $($elem).attr("data-column");

                    let val = api
                        .column(columnIndexToSearch, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    $( api.column($index).footer() ).html(val);

                }
            });
        },
        fixedHeader: true,
        // dom: '<"html5buttons"B>lTfgitp',
        "language": {
            "lengthMenu": t("Prikaz _MENU_  redova po strani"),
            "zeroRecords": "nije pronađeno",
            "info": "Prikaz stranice _PAGE_ od _PAGES_",
            "search" : "Traži",
            "infoEmpty": "Nema redova",
            "infoFiltered": "(filtrirano od _MAX_ ukupno redova)",
            "paginate": {
                "first":      "Prva",
                "last":       "Poslednja",
                "next":       ">",
                "previous":   "<"
            },
        },
        'bSort':false,
        tableTools: {
            "sSwfPath": "/plugin/copy_csv_xls_pdf.swf"
        },
        buttons: [
            // {
            //     extend: "copy"
            // },
            // {
            //     extend: "csv"
            // },
            // {
            //     extend: "excel"
            //     , title: "ExampleFile"
            // },
            // {
            //     extend: "pdf",
            //     title: "ExampleFile"
            // },
            // {
            //     extend: "print",
            //     customize: function (win){
            //         $(win.document.body).addClass("white-bg");
            //         $(win.document.body).css("font-size", "10px");
            //
            //         $(win.document.body).find("table")
            //             .addClass("compact")
            //             .css("font-size", "inherit");
            //     }
            // }
        ]

    });
}

function initInvoiceDataTable()
{
    if ($(".invoiceDataTables").length == 0) {
        return false;
    }

    $(".invoiceDataTables").DataTable({
        // dom: '<"html5buttons"B>lTfgitp',
        "language": {
            "lengthMenu": t("Prikaz _MENU_  redova po strani"),
            "zeroRecords": "nije pronađeno",
            "info": "Prikaz stranice _PAGE_ od _PAGES_",
            "search" : "Traži",
            "infoEmpty": "Nema redova",
            "infoFiltered": "(filtrirano od _MAX_ ukupno redova)",
            "paginate": {
                "first":      "Prva",
                "last":       "Poslednja",
                "next":       ">",
                "previous":   "<"
            },
        },
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
        ],
        tableTools: {
            "sSwfPath": "/plugin/copy_csv_xls_pdf.swf"
        },
        buttons: [
            // {
            //     extend: "copy"
            // },
            // {
            //     extend: "csv"
            // },
            // {
            //     extend: "excel"
            //     , title: "ExampleFile"
            // },
            // {
            //     extend: "pdf",
            //     title: "ExampleFile"
            // },
            // {
            //     extend: "print",
            //     customize: function (win){
            //         $(win.document.body).addClass("white-bg");
            //         $(win.document.body).css("font-size", "10px");
            //
            //         $(win.document.body).find("table")
            //             .addClass("compact")
            //             .css("font-size", "inherit");
            //     }
            // }
        ]

    });
}

function initOrderListTable()
{
    if ($(".opListTable").length == 0) {
        return false;
    }

    $(".opListTable").DataTable({
        // dom: '<"html5buttons"B>lTfgitp',
        "language": {
            "lengthMenu": t("Prikaz _MENU_  redova po strani"),
            "zeroRecords": "nije pronađeno",
            "info": "Prikaz stranice _PAGE_ od _PAGES_",
            "search" : "Traži",
            "infoEmpty": "Nema redova",
            "infoFiltered": "(filtrirano od _MAX_ ukupno redova)",
        },
        'bPaginate' : false,
        'bSort':false,
        "info":     false,
        tableTools: {
            "sSwfPath": "/plugin/copy_csv_xls_pdf.swf"
        },
        "pageLength": 100
    });
}

function sendPostAjaxCall(url, formData) {

    var currentUrl = window.location.href;
    $.ajax({
        url: url,
        type: "post",
        data: formData,
        dataType: "json",
        enctype: "multipart/form-data",
        processData: false,
        contentType: false,
        success: function (data) {

            if (!data.error) {
                if (typeof data.url !== "undefined") {
                    window.location = data.url;
                    $("#modal").hide();
                    $("body").removeClass("modal-open");
                    initTablsOnLoad();
                    initSwitch();
                }

                $.get(currentUrl, function (response) {
                    var content = $(response).find("#content");
                    var navbar = $(response).find(".navbar-top-links");
                    $("#content").replaceWith(content);
                    $(".navbar-top-links").replaceWith(navbar);
                    renderMessages(data.messages, "success");
                    $(".reUploadImage").trigger("change");
                    setChosen();
                    initDataTable();
                    initProductStatus();
                    initProductOrderable();
                    iniQuestionnaire();
                    initTablsOnLoad();
                    initChecks();
                    initChosen();
                    initDropUploadOrders();
                    initDropZone();
                    reUploadImage();
                    initSwitch();
                    initOrderListTable();

                    let element = document.getElementById("barcode-warehouse");
                    if (element) {
                        element.focus();
                    }
                });
            } else {
                $.get(currentUrl, function (response) {
                    var content = $(response).find("#content");
                    var flashSession = $(response).find("#flash-session");
                    $("#content").replaceWith(content);
                    $("#flash-session").replaceWith(flashSession);
                    $.each(data.messages, function (key, message) {
                        showPushMessage(message.message, "danger");
                    });

                    grepSessionMessages();
                    initChecks();
                    initChosen();
                    initDataTable();
                    initDropUploadOrders();
                    initDropZone();
                    reUploadImage();
                    initOrderListTable();
                    $(".reUploadImage").trigger("change");
                });
            }
        },
        error: function (request, error) {
            $.get(currentUrl, function (response) {
                var content = $(response).find("#content");
                var flashSession = $(response).find("#flash-session");
                $("#content").replaceWith(content);
                $("#flash-session").replaceWith(flashSession);
                showPushMessage(t("An error was detected."), "error");
                grepSessionMessages();
                initProductStatus();
                initProductOrderable();
                initChecks();
                initChosen();
                initDataTable();
                initDropUploadOrders();
                initDropZone();
                reUploadImage();

            });
        }
    }).done(function(){
        let element = document.getElementById("barcode-warehouse");
        if (element) {
            element.focus();
        }
    });
}

function recalculateAmount() {
    var pricePerDay = parseFloat(($("#pricePerDay").val() != "") ? $("#pricePerDay").val() : 0 );
    var totalDays = parseFloat(($("#totalDays").val() != "") ? $("#totalDays").val() : 0);
    var discount = parseFloat(($("#discount").val() != "") ? $("#discount").val() : 0);
    var total = pricePerDay * totalDays;
    if (discount > 0) {
        total = total - (total / 100 * discount);
    }

    var deposit = parseFloat(($("#reservationDeposit").val() != "") ? $("#reservationDeposit").val() : 0);
    var amount = parseFloat(($("#advanceAmount").val() != "") ? $("#advanceAmount").val() : 0);
    var outstandingAmount = total - (deposit + amount);

    $("#outstandingAmount").val(outstandingAmount);
    $("#total").val(total);
}

function hideElement()
{
    $.each($(":input"), function(){
        var hidden = $(this).attr("data-hidden");
        if (typeof hidden !== typeof undefined && hidden !== false) {
            $(this).parents(".form-group").addClass("hidden");
        }
    });
}

function initSteps()
{
    if ($("#product-form").length == 0) {
        return false;
    }

    $("#product-form").steps({
        bodyTag: "fieldset",
        showFinishButtonAlways: false,
        labels: {
            cancel: t("Odustani"),
            current: t("Trenutni korak") + ":",
            pagination: "Pagination",
            finish: t("Završi"),
            next: t("Sledeći"),
            previous: t("Predhodni"),
            loading: "Loading ..."
        },
        onStepChanging: function (event, currentIndex, newIndex)
        {
            $(".search").chosen({
                placeholder_text_multiple: t('-- Izaberite --')
            });

            // Always allow going backward even if the current step contains invalid fields!
            if (currentIndex > newIndex)
            {
                return true;
            }

            // Forbid suppressing "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age").val()) < 18)
            {
                return false;
            }

            // Forbid suppressing "Warning" step if the user is to young
            if (newIndex === 4)
            {
                var $input = $( "input[name='sizes[]']:checked" );
                var html = '<div class="col-xs-6">';
                var counter = 0;
                $.each($input, function (key, index) {
                    var $2 = $(index);
                    counter++;
                    if (counter > 10) {
                        html += '</div>';
                        html +='<div class="col-xs-6">';
                    }

                    html += '<div class="form-group">\n' +
                        '           <label class="col-sm-4 control-label">Barkod za veličinu '+ $2.val() +'</label>\n' +
                        '           <div class="col-sm-8">\n' +
                        '               <input type="text" class="form-control" name="barcode['+ $2.val() +']" value="'+ $2.attr("data-barcode") +'"/>\n' +
                        '            </div>\n' +
                        '            <div class="col-sm-4"></div>\n' +
                        '            <div class="col-sm-8">\n' +
                        '                <div class="help-block m-b-none"></div>\n' +
                        '            </div>\n' +
                        '      </div>';


                });

                html += '</div>';

                $("fieldset:eq(" + newIndex + ")").find(".col-xs-12").empty();
                $("fieldset:eq(" + newIndex + ")").find(".col-xs-12").append(html);
            }

            var form = $(this);

            // Clean up if user went backward before
            if (currentIndex < newIndex)
            {
                // To remove error styles
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .has-error", form).removeClass("has-error");
            }

            // Disable validation on fields that are disabled or hidden.
            form.validate().settings.ignore = ":disabled,:hidden:not(select)";
            // $.validator.setDefaults({ ignore: ":hidden:not(select)" });

            // Start validation; Prevent going forward if false
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {
            // Suppress (skip) "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age").val()) >= 18)
            {
                $(this).steps("next");
            }

            // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3)
            {
                $(this).steps("previous");
            }
        },
        onFinishing: function (event, currentIndex)
        {
            var form = $(this);
            if ($("#finalCode").length > 0) {
                form.append($("#finalCode"));
            }

            // Disable validation on fields that are disabled.
            // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
            form.validate().settings.ignore = ":disabled";

            // Start validation; Prevent form submission if false
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            var form = $(this);

            // Submit form input
            form.submit();
        }
    }).validate({
        errorElement: "div",
        errorPlacement: function (error, element)
        {
            element.parent().parent().addClass("has-error").find(".help-block").append(error);
            // error.appendTo("div.col-sm-2", element.parent()).addClass('alert alert-danger');
        },
        highlight: function(element, errorClass) {
            $(element).parent().parent().addClass("has-error");
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parent().parent().removeClass("has-error");
        },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    });
}

function inputFile() {
    $(".input-file").before(
        function() {
            if ( ! $(this).prev().hasClass('input-ghost') ) {
                var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                element.attr("name",$(this).attr("name"));
                element.change(function(){
                    element.next(element).find('input').val((element.val()).split('\\').pop());
                });
                $(this).find("button.btn-choose").click(function(){
                    element.click();
                });
                $(this).find("button.btn-reset").click(function(){
                    element.val(null);
                    $(this).parents(".input-file").find('input').val('');
                });
                $(this).find('input').css("cursor","pointer");
                $(this).find('input').mousedown(function() {
                    $(this).parents('.input-file').prev().click();
                    return false;
                });
                return element;
            }
        }
    );
}

function getChildrenClassifications(parentElement, childElement) {
    $.ajax({
        url: "/classifications/list-by-children-classifications",
        type: "post",
        data: { productProgram: parentElement.value },
        dataType: "json",
        success: function (data) {
            childElement.empty();
            childElement.trigger("liszt:updated");
            childElement.trigger("chosen:updated");

            childElement.append($("<option/>", {
                value: 0,
                text: '-- Izaberite --'
            }));

            if (Object.keys(data).length > 0) {
                $.each(data, function (key, value) {
                    childElement.append($("<option/>", {
                        value: key,
                        text: value
                    }));
                });
            } else if (parentElement.id == "subSubProductGroup") {
                childElement.append($("<option/>", {
                    value: value,
                    text: $("#subSubProductGroup option[value='"+ parentElement.value +"']").text()
                }));

                childElement.val(parentElement.value).trigger("change");
            }

            childElement.trigger("liszt:updated");
            childElement.trigger("chosen:updated");
        }
    });
}

function setSwitchery(switchElement, checkedBool) {
    if((checkedBool && !switchElement.isChecked()) || (!checkedBool && switchElement.isChecked())) {
        switchElement.setPosition(true);
        switchElement.handleOnchange(true);
    }
}

function initProductStatus() {
    var elem = document.querySelector('#status');
    if (elem) {
        var status = $(elem).attr("data-status");
        var defaults = {
            secondaryColor: '#ccc',
            size : 'large',
            disabledOpacity: 0.75
        };

        var switchery = new Switchery(elem, defaults);
        if (status === 'otkazan') {
            switchery.disable();
        }
    }
}

function initProductOrderable() {
    var elem = document.querySelector('#productOrderable');
    if (elem) {
        var orderable = $(elem).attr("data-status");
        var defaults = {
            secondaryColor: '#ccc',
            size : 'small',
            disabledOpacity: 0.75
        };

        var switchery = new Switchery(elem, defaults);
        // if (orderable === false) {
        //     switchery.disable();
        // }
    }
}

function iniQuestionnaire() {

    var elem = document.querySelector('#questionnaireAdd');

    if (elem) {
        var defaults = {
            secondaryColor: '#ccc',
            size : 'large',
            disabledOpacity: 0.75
        };

        new Switchery(elem, defaults);
    }
}

function initOrderStatus() {

    $.each($(".orderActive"), function(index, elem) {
        if (elem) {
            var defaults = {
                secondaryColor: '#ccc',
                size : 'large',
                disabledOpacity: 0.75
            };

            new Switchery(elem, defaults);
        }
    });

}

function initTablsOnLoad() {
    var url = document.location.toString();
    if (url.match('#tab')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }

    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    });
}

function initChecks() {
    if ($('.i-checks').length > 0) {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    }
}

function initChosen(){
    var select = $(".search");
    select.chosen({
        placeholder_text: t('-- Izaberite --'),
        placeholder_text_multiple: t('-- Izaberite --')
    });

    select.on('chosen:updated', function () {
        if (select.attr('readonly')) {
            var wasDisabled = select.is(':disabled');

            select.attr('disabled', 'disabled');
            select.data('chosen').search_field_disabled();

            if (wasDisabled) {
                select.attr('disabled', 'disabled');
            } else {
                select.removeAttr('disabled');
            }
        }
    });

    select.trigger('chosen:updated');
}

function initDropUploadOrders()
{
    var $dropzoneFormOrder = $("#dropzoneFormOrder");
    if ($dropzoneFormOrder.length == 0) {
        return null;
    }


    Dropzone.autoDiscover = false;

    var myDropzone = new Dropzone("#dropzoneFormOrder", {
        autoProcessQueue: false,
        uploadMultiple: false,
        url: "/order-items/check-errors/" + $dropzoneFormOrder.attr("data-id"),
        maxFilesize: 200,
        acceptedFiles: ".xlsx, .xls",
        addRemoveLinks: true,
        dictDefaultMessage: "<strong class='bigger-140'>"+ t("Prenesite file na box ili kliknite da upload-ujete") +"</strong>",
        dictRemoveFile: t('Obriši file'),
        init: function () {
            var submitButton = document.querySelector("#start");
            myDropzone = this; // closure

            submitButton.addEventListener("click", function() {
                myDropzone.processQueue(); // Tell Dropzone to process all queued files.
            });

            // You might want to show the submit button only when
            // files are dropped here:
            this.on("addedfile", function() {
                // Show submit button here and/or inform user to click it.
            });
            this.on("sendingmultiple", function () {
            });
            this.on("successmultiple", function (files, response) {
            });
            this.on("errormultiple", function (files, response) {
            });
            this.on("complete", function (file, response1) {
                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    var data = $.parseJSON(file.xhr.response);
                    $.get(window.location.href, function (response) {
                        var content = $(response).find("#content");
                        var navbar = $(response).find(".navbar-top-links");
                        $("#content").replaceWith(content);
                        $(".navbar-top-links").replaceWith(navbar);
                        renderMessages(data.messages, data.error ? 'danger' : 'success');
                        setChosen();
                        initDataTable();
                        initProductStatus();
                        initProductOrderable()
                        initProductStatus();
                        $(".nav-tabs a[href='#tabPrePress']").tab('show');

                    });
                }
            });
        }
    });
}

function prepareFormDataAndSendPostCall(form, button){
    var isValid = getFormValidation($(form));
    var formData = new FormData(form.get(0));
    var files = $(form).find("input[type='file']");

    $(files).each(function (key, element) {
        if ("" != $(element).val()) {
            var name = $(element).attr("name");
            if (formData.has(name)) {
                formData.delete(name);
            }

            formData.append(name, $(element)[0].files[0]);
        }
    });

    if (isValid) {
        $(button).addClass("disabled");
        sendPostAjaxCall($(form).attr("action"), formData)
    }
}

function addTypes(productId)
{
    var data = {productId : productId};
    $.ajax({
        url: "/operating-list-items/find-all-types",
        data: data,
        type: "post",
        dataType: "json",
        success: function (response) {
            var $element = $("#operatingListItemTypeId");
            $element
                .empty()
                .append($("<option/>", {
                    value: 0,
                    text: '-- Izaberite --'
                }));

            if (response.elements) {
                $.each(response.elements, function (k, v) {
                    $element.append($("<option/>", {
                        value: k,
                        text: v
                    }));
                });
            }

            $element.trigger("liszt:updated")
                .trigger("chosen:updated");
        }
    });
}

function iniContentEditable() {
    $('div#froala-editor').froalaEditor({
        heightMin: 250,
        toolbarButtons: [
            'bold', 'italic', 'underline', '|',
            'strikeThrough', 'subscript', 'superscript',
            'outdent', 'indent', 'clearFormatting', 'paragraphFormat', 'align', 'formatOL', 'formatUL',
            'insertTable', 'html', '|', 'insertLink',
            'undo', 'redo'
        ],
        toolbarButtonsXS: ['undo', 'redo' , '-', 'bold', 'italic', 'underline', 'html']
    })
}

function imageUploader(dialog) {
    var image, xhr, xhrComplete, xhrProgress;

    dialog.addEventListener('imageuploader.cancelupload', function () {
        // Cancel the current upload

        // Stop the upload
        if (xhr) {
            xhr.upload.removeEventListener('progress', xhrProgress);
            xhr.removeEventListener('readystatechange', xhrComplete);
            xhr.abort();
        }

        // Set the dialog to empty
        dialog.state('empty');
    });

    dialog.addEventListener('imageuploader.clear', function () {
        // Clear the current image
        dialog.clear();
        image = null;
    });

    dialog.addEventListener('imageuploader.fileready', function (ev) {

        // Upload a file to the server
        var formData;
        var file = ev.detail().file;

        // Define functions to handle upload progress and completion
        xhrProgress = function (ev) {
            // Set the progress for the upload
            dialog.progress((ev.loaded / ev.total) * 100);
        };

        xhrComplete = function (ev) {
            var response;

            // Check the request is complete
            if (ev.target.readyState != 4) {
                return;
            }

            // Clear the request
            xhr = null;
            xhrProgress = null;
            xhrComplete = null;

            // Handle the result of the upload
            if (parseInt(ev.target.status) == 200) {
                // Unpack the response (from JSON)
                response = JSON.parse(ev.target.responseText);

                // Store the image details
                image = {
                    size: response.size,
                    url: response.url
                };

                // Populate the dialog
                dialog.populate(image.url, image.size);

            } else {
                // The request failed, notify the user
                new ContentTools.FlashUI('no');
            }
        };

        // Set the dialog state to uploading and reset the progress bar to 0
        dialog.state('uploading');
        dialog.progress(0);

        // Build the form data to post to the server
        formData = new FormData();
        formData.append('image', file);

        // Make the request
        xhr = new XMLHttpRequest();
        xhr.upload.addEventListener('progress', xhrProgress);
        xhr.addEventListener('readystatechange', xhrComplete);
        xhr.open('POST', '/admin/images/upload', true);
        xhr.send(formData);
    });

    dialog.addEventListener('imageuploader.rotateccw', function () {
        rotateImage(dialog, image, 'CCW');
    });

    dialog.addEventListener('imageuploader.rotatecw', function () {
        rotateImage(dialog, image, 'CW');
    });

    dialog.addEventListener('imageuploader.save', function () {
        var crop, cropRegion, formData;

        // Define a function to handle the request completion
        xhrComplete = function (ev) {
            // Check the request is complete
            if (ev.target.readyState !== 4) {
                return;
            }

            // Clear the request
            xhr = null;
            xhrComplete = null;

            // Free the dialog from its busy state
            dialog.busy(false);

            // Handle the result of the rotation
            if (parseInt(ev.target.status) === 200) {
                // Unpack the response (from JSON)
                var response = JSON.parse(ev.target.responseText);

                // Trigger the save event against the dialog with details of the
                // image to be inserted.
                dialog.save(
                    response.url,
                    response.size,
                    {
                        'alt': response.alt,
                        'data-ce-max-width': response.size[0]
                    });

            } else {
                // The request failed, notify the user
                new ContentTools.FlashUI('no');
            }
        };

        // Set the dialog to busy while the rotate is performed
        dialog.busy(true);

        // Build the form data to post to the server
        formData = new FormData();
        formData.append('url', image.url);

        // Set the width of the image when it's inserted, this is a default
        // the user will be able to resize the image afterwards.
        formData.append('width', 600);
        formData.append('entityTypeId', document.getElementById('entityTypeId').value);
        formData.append('entityId', document.getElementById('entityId').value);

        // Check if a crop region has been defined by the user
        if (dialog.cropRegion()) {
            formData.append('crop', dialog.cropRegion());
        }

        // Make the request
        xhr = new XMLHttpRequest();
        xhr.addEventListener('readystatechange', xhrComplete);
        xhr.open('POST', '/admin/images/save', true);
        xhr.send(formData);
    });
}



//
// var modal = document.getElementById('myModal');
//
// // Get the image and insert it inside the modal - use its "alt" text as a caption
// var img = document.getElementById('myImg');
// var modalImg = document.getElementById("img01");
// var captionText = document.getElementById("caption");
// img.onclick = function(){
//     modal.style.display = "block";
//     modalImg.src = this.src;
//     captionText.innerHTML = this.alt;
// }
//
// // Get the <span> element that closes the modal
// var span = document.getElementsByClassName("close")[0];
//
// // When the user clicks on <span> (x), close the modal
// span.onclick = function() {
//     modal.style.display = "none";
// }


