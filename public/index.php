<?php

define('APP_PATH', dirname(__DIR__) . '/');
define('K_TCPDF_CALLS_IN_HTML', true);

/**
 * Read composer autoloader
 */
require_once APP_PATH . 'vendor/autoload.php';
require_once APP_PATH . 'src/base/autoload.php';
require_once APP_PATH . 'src/warehouseOrder/autoload.php';
require_once APP_PATH . 'src/warehouseOrderMp/autoload.php';
require_once APP_PATH . 'src/warehouseOrderVp/autoload.php';


//phpinfo();
/**
 * Handle the request
 */
$app = new \Vemid\Application\Web();
$app->initialize();
echo $app->handle()->getContent();
