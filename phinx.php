<?php
define('APP_PATH', realpath(__DIR__ ) . '/');

require_once(APP_PATH . 'vendor/autoload.php');

$application = new \Vemid\Application\Cli();
$application->initialize();

$config = $application->getDI()->getConfig();

return array (
    'paths' =>
        array (
            'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
            'seeds' => '%%PHINX_CONFIG_DIR%%/db/seeds',
        ),
    'environments' =>
        array (
            'default_migration_table' => 'phinxlog',
            'default_database' => 'development',
            'production' =>
                array (
                    'adapter' => 'mysql',
                    'host' => 'localhost',
                    'name' => '',
                    'user' => 'root',
                    'pass' => '',
                    'port' => 3306,
                    'charset' => 'utf8',
                ),
            'development' =>
                array (
                    'adapter' => 'mysql',
                    'host' => $config->database->host,
                    'name' => $config->database->dbname,
                    'user' => $config->database->username,
                    'pass' => $config->database->password,
                    'port' => 3306,
                    'charset' => 'utf8',
                ),
            'testing' =>
                array (
                    'adapter' => 'mysql',
                    'host' => 'localhost',
                    'name' => 'testing_db',
                    'user' => 'root',
                    'pass' => '',
                    'port' => 3306,
                    'charset' => 'utf8',
                ),
        ),
    'version_order' => 'creation',
);
