<?php


use Phinx\Migration\AbstractMigration;

class AlterOrdersDropProductId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('orders')
            ->dropForeignKey(['product_id'])
            ->removeColumn('product_id')
            ->removeColumn('status')
            ->addColumn('entity_type_id', 'integer', ['signed' => false])
            ->addColumn('entity_id', 'integer', ['signed' => false])
            ->changeColumn('quantity_a', 'integer', ['null' => true])
            ->changeColumn('quantity_b', 'integer', ['null' => true])
            ->addIndex(['entity_type_id', 'entity_id'])
            ->update();
    }
}
