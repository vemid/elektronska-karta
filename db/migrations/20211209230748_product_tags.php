<?php


use Phinx\Migration\AbstractMigration;

class ProductTags extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('product_tags', ['signed' => false]);
        $table
            ->addColumn('product_id', 'integer', ['signed' => false])
            ->addColumn('category','enum',['values' => ['GENDER', 'BRAND', 'SEASON', 'SIZE', 'COLLECTION', 'MODEL','AGE','COLOR']])
            ->addColumn('tag', 'string', ['null' => false])
            ->addForeignKey('product_id', 'products', 'id')
            ->create();
    }
}
