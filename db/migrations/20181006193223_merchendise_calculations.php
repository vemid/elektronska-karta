<?php


use Phinx\Migration\AbstractMigration;

class MerchendiseCalculations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('merchandise_calculations', ['signed' => false]);
        $table
            ->addColumn('document_name', 'string',['null' => false])
            ->addColumn('mis_calculations', 'string',['null' => false])
            ->addColumn('file','string',['null'=>false])
            ->addColumn('type', 'enum', ['null' => false, 'values' => ['PREPARED', 'FINISHED'], 'default' => 'PREPARED'])
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->create();
    }
}
