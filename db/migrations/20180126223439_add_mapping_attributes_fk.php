<?php


use Phinx\Migration\AbstractMigration;

class AddMappingAttributesFk extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('classification_old_collection_map');
        $table->dropForeignKey('classification_old_collection_id');
        $table->dropForeignKey('classification_age_id');
        $table->dropForeignKey('classification_season_id');
        $table->dropForeignKey('classification_collection_id');
        $table->dropForeignKey('code_id');
        $table->removeIndexByName('classification_old_collection_id');
        $table->removeIndexByName('classification_age_id');
        $table->removeIndexByName('classification_season_id');
        $table->removeIndexByName('classification_collection_id');
        $table->removeIndexByName('code_id');
    }


    public function change()
    {

        $this->table('classification_old_collection_map')
            ->addForeignKey('classification_old_collection_id', 'classifications', 'id')
            ->addForeignKey('classification_double_season_id', 'classifications', 'id')
            ->addForeignKey('attribute_producer_id', 'attributes', 'id')
            ->addForeignKey('attribute_product_type_id', 'attributes', 'id')
            ->addForeignKey('classification_old_collection_id', 'classifications', 'id')
            ->addForeignKey('classification_age_id', 'classifications', 'id')
            ->addForeignKey('classification_season_id', 'classifications', 'id')
            ->addForeignKey('classification_collection_id', 'classifications', 'id')
            ->addForeignKey('code_id', 'codes', 'id')
            ->addIndex(['classification_old_collection_id','classification_double_season_id'
                ,'attribute_producer_id','attribute_product_type_id', 'classification_age_id','classification_season_id',
                'classification_collection_id','code_id'], ['unique' => true])
            ->update();
    }
}
