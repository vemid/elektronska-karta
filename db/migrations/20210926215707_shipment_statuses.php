<?php


use Phinx\Migration\AbstractMigration;

class ShipmentStatuses extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('shipment_statuses', ['signed' => false]);
        $table
            ->addColumn('event_id', 'string', ['null' => false])
            ->addColumn('code', 'string', ['null' => false])
            ->addColumn('reference_id', 'string', ['null' => false])
            ->addColumn('status','enum',['values' => ['DELETED', 'CANCELLED', 'WAITING', 'DELIVERED', 'PICKED']])
            ->addColumn('event_date', 'date')
            ->create();
    }
}
