<?php


use Phinx\Migration\AbstractMigration;

class ProductCalculations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('product_calculations', ['signed' => false]);
        $table->addColumn('product_id', 'integer', ['signed' => false])
            ->addColumn('material_factory_id', 'integer', ['signed' => false])
            ->addColumn('consumption', 'decimal', array('precision' => 10, 'scale' => 2))
            ->addColumn('total', 'decimal', array('precision' => 10, 'scale' => 2))
            ->addColumn('status','enum',['values' => ['PLANED', 'FINAL'],'null'=>true])
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('product_id', 'products', 'id')
            ->addForeignKey('material_factory_id', 'products', 'id')
            ->create();
    }
}
