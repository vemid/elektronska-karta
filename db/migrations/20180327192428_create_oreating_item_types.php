<?php


use Phinx\Migration\AbstractMigration;

class CreateOreatingItemTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('operating_list_item_types', ['signed' => false]);
        $table->addColumn('code_id', 'integer', ['signed' => false,'comment'=> 'Column code_id from table code represent SectorID'])
            ->addColumn('name', 'string')
            ->addColumn('machine','string')
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('code_id', 'codes', 'id')
            ->addIndex(['name'], ['unique' => true])
            ->create();

    }
}
