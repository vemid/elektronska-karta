<?php


use Phinx\Migration\AbstractMigration;

class ChannelSupplyEntities extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('channel_supply_entities', ['signed' => false]);
        $table
            ->addColumn('channel_supply_id', 'integer', ['signed' => false])
            ->addColumn('entity_type_id', 'integer', ['signed' => false])
            ->addColumn('entity_id', 'integer', ['signed' => false])
            ->addColumn('mis_document_type', 'string')
            ->addColumn('email', 'string', ['default' => null, 'null'=>true])
            ->addColumn('postal_address', 'string', ['default' => null, 'null'=>true])
            ->addForeignKey('channel_supply_id', 'invoices', 'id')
            ->create();
    }
}
