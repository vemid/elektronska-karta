<?php


use Phinx\Migration\AbstractMigration;

class OperationListItemWorksheets extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('operating_list_item_worksheets', ['signed' => false]);
        $table
            ->addColumn('operating_list_item_id', 'integer', ['signed' => false])
            ->addColumn('production_worker_id', 'integer', ['signed' => false])
            ->addColumn('code_id', 'integer', ['signed' => false])
            ->addColumn('qty', 'decimal', ['precision' => 10, 'scale' => 2])
            ->addColumn('date','date')
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('production_worker_id', 'production_workers', 'id')
            ->addForeignKey('operating_list_item_id', 'operating_list_items', 'id')
            ->addForeignKey('code_id', 'codes', 'id')
            ->create();
    }
}
