<?php


use Phinx\Migration\AbstractMigration;

class AlterInvoiceFca extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('invoices')
            ->addColumn('custom', 'string',['null' => true])
            ->addColumn('box_qty', 'string',['null' => true])
            ->addColumn('colets_qty', 'string',['null' => true])
            ->addColumn('bruto_weight', 'string',['null' => true])
            ->addColumn('neto_weight', 'string',['null' => true])
            ->addColumn('statement', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->update();
    }
}
