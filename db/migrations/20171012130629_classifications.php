<?php

use Phinx\Migration\AbstractMigration;

class Classifications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('classifications', ['signed' => false]);
        $table->addColumn('classification_type_code_id', 'integer', ['signed' => false])
            ->addColumn('classification_code_id', 'integer', ['signed' => false])
            ->addColumn('parent_classification_id', 'integer', ['signed' => false, 'null' => true])
            ->addColumn('name', 'string')
            ->addForeignKey('classification_type_code_id', 'codes', 'id')
            ->addForeignKey('classification_code_id', 'codes', 'id')
            ->addForeignKey('parent_classification_id', 'classifications', 'id')
            ->create();
    }
}
