<?php


use Phinx\Migration\AbstractMigration;

class ClassificationOldCollectionMapMig extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('classification_old_collection_map', ['signed' => false]);
        $table->addColumn('classification_old_collection_id', 'integer', ['signed' => false])
            ->addColumn('classification_age_id', 'integer', ['signed' => false])
            ->addColumn('classification_season_id', 'integer', ['signed' => false])
            ->addColumn('classification_collection_id', 'integer', ['signed' => false])
            ->addColumn('code_id', 'integer', ['signed' => false])
            ->addForeignKey('classification_old_collection_id', 'classifications', 'id')
            ->addForeignKey('classification_age_id', 'classifications', 'id')
            ->addForeignKey('classification_season_id', 'classifications', 'id')
            ->addForeignKey('classification_collection_id', 'classifications', 'id')
            ->addForeignKey('code_id', 'codes', 'id')
            ->addIndex(['classification_old_collection_id', 'classification_age_id','classification_season_id',
            'classification_collection_id','code_id'], ['unique' => true])
            ->create();
    }
}
