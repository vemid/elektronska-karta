<?php


use Phinx\Migration\AbstractMigration;

class CreateProductionOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('productions_orders', ['signed' => false]);
        $table
            ->addColumn('product_size_id', 'integer', ['signed' => false])
            ->addColumn('work_order', 'integer', ['signed' => false])
            ->addColumn('planed_qty','decimal', array('precision' => 10, 'scale' => 2))
            ->addColumn('realized_a_qty', 'decimal', array('precision' => 10, 'scale' => 2))
            ->addColumn('realized_b_qty', 'decimal', array('precision' => 10, 'scale' => 2))
            ->addColumn('type', 'enum', ['null' => true, 'values' => ['A', 'B']])
            ->addForeignKey('product_size_id', 'product_sizes', 'id')
            ->create();

    }
}
