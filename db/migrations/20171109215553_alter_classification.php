<?php


use Phinx\Migration\AbstractMigration;

class AlterClassification extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('classifications')
            ->dropForeignKey(['classification_code_id'])
            ->update();

        $this->table('classifications')
            ->removeColumn('classification_code_id')
            ->addColumn('code', 'string', ['after' => 'parent_classification_id'])
            ->update();
    }
}
