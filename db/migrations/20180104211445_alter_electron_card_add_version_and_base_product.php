<?php


use Phinx\Migration\AbstractMigration;

class AlterElectronCardAddVersionAndBaseProduct extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('electron_cards')
            ->addColumn('base_product_id', 'integer', ['signed' => false, 'null' => true, 'after' => 'product_id'])
            ->addColumn('base_name', 'string', ['null' => true, 'after' => 'base_product_id'])
            ->addColumn('active', 'boolean', ['default' => 1])
            ->addColumn('version', 'integer')
            ->changeColumn('image', 'string')
            ->addForeignKey('base_product_id', 'products', 'id')
            ->update();
    }
}
