<?php


use Phinx\Migration\AbstractMigration;

class CreateFailures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('failures', ['signed' => false]);
        $table
            ->addColumn('document', 'string', ['null' => false])
            ->addColumn('date', 'date')
            ->addColumn('shop_code_id', 'integer',['signed' => false])
            ->addColumn('warehouse', 'string', ['null' => false])
            ->addColumn('product_size_id', 'integer', ['signed' => false])
            ->addColumn('qty', 'integer', ['signed' => false])
            ->addColumn('failure_code_id', 'integer',['signed' => false])
            ->addColumn('note', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addColumn('proces_type','enum',['values' => ['OTPIS', 'FELER','SNIZENJE','POPRAVLJENO'],'null'=>true])
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('product_size_id', 'product_sizes', 'id')
            ->addForeignKey('shop_code_id', 'codes', 'id')
            ->addForeignKey('failure_code_id', 'codes', 'id')
            ->create();
    }
}
