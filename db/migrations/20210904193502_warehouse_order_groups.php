<?php


use Phinx\Migration\AbstractMigration;

class WarehouseOrderGroups extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('warehouse_order_groups', ['signed' => false]);
        $table
            ->addColumn('mis_code', 'string', ['default' => null, 'null'=>true])
            ->addColumn('status', 'enum', ['values' => ['LOCKED', 'SENT', 'EDITABLE']])
            ->addColumn('note', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addColumn('sync_status', 'string', ['default' => null, 'null'=>true])
            ->create();
    }
}
