<?php


use Phinx\Migration\AbstractMigration;

class UserNotificationMessages extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('user_notification_messages', ['signed' => false]);
        $table->addColumn('user_notification_id', 'integer', ['signed' => false])
            ->addColumn('entity_type_id', 'string')
            ->addColumn('entity_id', 'string')
            ->addColumn('status','enum',['values' => ['ACTIVE', 'POSTPONED_ONE_DAY', 'POSTPONED_TWO_DAYS', 'POSTPONED_THREE_DAYS', 'POSTPONED_FIVE_DAYS', 'POSTPONED_TEN_DAYS', 'DEACTIVATED', 'FINISHED'], 'default'=>'ACTIVE'])
            ->addColumn('date_created','datetime')
            ->addColumn('postponed_date','date', ['null'=>true])
            ->addColumn('due_date','date', ['null'=>true])
            ->addForeignKey('user_notification_id', 'user_notifications', 'id')
            ->create();
    }
}
