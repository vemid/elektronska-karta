<?php


use Phinx\Migration\AbstractMigration;

class MerchandiseCalculationItems extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('merchandise_calculation_items', ['signed' => false]);
        $table
            ->addColumn('merchandise_calculation_id', 'integer',['signed' => false])
            ->addColumn('product_id', 'integer',['signed' => false])
            ->addColumn('course', 'decimal', array('precision' => 10, 'scale' => 3))
            ->addColumn('purchase_price', 'decimal', array('precision' => 10, 'scale' => 3))
            ->addColumn('wholesale_price', 'decimal', array('precision' => 10, 'scale' => 3))
            ->addColumn('retail_price', 'decimal', array('precision' => 10, 'scale' => 3))
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('merchandise_calculation_id', 'merchandise_calculations', 'id')
            ->addForeignKey('product_id', 'products', 'id')
            ->create();
    }
}
