<?php


use Phinx\Migration\AbstractMigration;

class WarehouseOrderItems extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('warehouse_order_items', ['signed' => false]);
        $table
            ->addColumn('warehouse_order_id', 'integer', ['signed' => false])
            ->addColumn('product_size_id', 'integer', ['signed' => false])
            ->addColumn('quantity', 'integer', ['signed' => false])
            ->addColumn('note', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addForeignKey('warehouse_order_id', 'warehouse_orders', 'id')
            ->addForeignKey('product_size_id', 'product_sizes', 'id')
            ->create();
    }
}
