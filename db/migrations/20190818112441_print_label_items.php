<?php


use Phinx\Migration\AbstractMigration;

class PrintLabelItems extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('print_label_items', ['signed' => false]);
        $table
            ->addColumn('product_size_id', 'integer', ['signed' => false])
            ->addColumn('qty', 'integer', ['signed' => false])
            ->addColumn('price_srb', 'decimal', array('precision' => 10, 'scale' => 3))
            ->addColumn('price_cg', 'decimal', array('precision' => 10, 'scale' => 3))
            ->addColumn('price_bih', 'decimal', array('precision' => 10, 'scale' => 3))
            ->addColumn('height', 'integer', ['signed' => false])
            ->addColumn('sizes', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addColumn('composition', 'string')
            ->addColumn('wash_data', 'string')
            ->addColumn('printed_date', 'string',['null' => true])
            ->addColumn('printed', 'boolean', ['default' => 0, 'null'=>true])
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP','null'=>true])
            ->addForeignKey('product_size_id', 'product_sizes', 'id')
            ->create();
    }
}
