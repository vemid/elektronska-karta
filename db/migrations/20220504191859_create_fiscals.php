<?php


use Phinx\Migration\AbstractMigration;

class CreateFiscals extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('e_fiscals', ['signed' => false]);
        $table
            ->addColumn('code_id', 'integer', ['signed' => false])
            ->addColumn('cashier', 'string', ['null' => true])
            ->addColumn('invoice_number', 'string', ['null' => true])
            ->addColumn('total_amount', 'decimal', array('precision' => 10, 'scale' => 4))
            ->addColumn('tax_amount', 'decimal', array('precision' => 10, 'scale' => 4))
            ->addColumn('transaction_type', 'string', ['null' => true])
            ->addColumn('invoice_type', 'string', ['null' => true])
            ->addForeignKey('code_id', 'codes', 'id')
            ->create();
    }
}
