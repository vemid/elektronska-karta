<?php


use Phinx\Migration\AbstractMigration;

class CreateWarantStatementItems extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('warrant_statement_items', ['signed' => false]);
        $table->addColumn('warrant_statement_id', 'integer', ['signed' => false])
            ->addColumn('company_name', 'string')
            ->addColumn('account', 'string')
            ->addColumn('amount', 'decimal', array('precision' => 10, 'scale' => 3))
            ->addColumn('purpose', 'string')
            ->addColumn('warrant_number', 'string',['null' => true])
            ->addForeignKey('warrant_statement_id', 'warrant_statements', 'id')
            ->create();
    }
}
