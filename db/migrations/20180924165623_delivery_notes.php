<?php


use Phinx\Migration\AbstractMigration;

class DeliveryNotes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('delivery_notes', ['signed' => false]);
        $table
            ->addColumn('order_item_id', 'integer', ['signed' => false])
            ->addColumn('quantity_a', 'integer', ['signed' => false, 'null' => true])
            ->addColumn('quantity_b', 'integer', ['signed' => false, 'null' => true])
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('order_item_id', 'order_items', 'id')
            ->create();
    }
}
