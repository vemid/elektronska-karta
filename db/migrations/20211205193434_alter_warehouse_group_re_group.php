<?php


use Phinx\Migration\AbstractMigration;

class AlterWarehouseGroupReGroup extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('warehouse_order_groups')
            ->addColumn('grouped', 'enum', ['values' => ['GROUPED', 'NON_GROUPED', 'LOCKED'],'default'=>'NON_GROUPED','after' => 'pushed_to_mis'])
            ->addColumn('type', 'enum', ['values' => ['A', 'B'],'default'=>'A','after' => 'pushed_to_mis'])
            ->update();
    }
}
