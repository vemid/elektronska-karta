<?php


use Phinx\Migration\AbstractMigration;

class WarehouseOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('warehouse_orders', ['signed' => false]);
        $table
            ->addColumn('channel_supply_entity_id', 'integer', ['signed' => false])
            ->addColumn('planed_exit_date', 'date')
            ->addColumn('status', 'enum', ['values' => ['LOCKED', 'SENT', 'EDITABLE']])
            ->addColumn('note', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addColumn('warehouse_order_code', 'string')
            ->addColumn('mis_code', 'string')
            ->addColumn('postal_barcode', 'string', ['default' => null, 'null'=>true])
            ->addColumn('posted', 'boolean', ['default' => 0])
            ->addColumn('sync_status', 'string', ['default' => null, 'null'=>true])
            ->addForeignKey('channel_supply_entity_id', 'channel_supply_entities', 'id')
            ->create();
    }
}
