<?php


use Phinx\Migration\AbstractMigration;

class Suplier extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('suppliers', ['signed' => false]);
        $table->addColumn('supplier_code_id', 'integer', ['signed' => false])
            ->addColumn('contact_person', 'string')
            ->addColumn('email', 'string')
            ->addColumn('phone_number', 'string')
            ->addColumn('address', 'string')
            ->addForeignKey('supplier_code_id', 'codes', 'id')
            ->create();
    }
}
