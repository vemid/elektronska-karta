<?php


use Phinx\Migration\AbstractMigration;

class CreatePriorityClassifications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('priority_classifications', ['signed' => false,'comment'=> 'Table for defining priority collection classifications']);
        $table->addColumn('priority_id',  'integer', ['signed' => false])
            ->addColumn('classification_id', 'integer',['signed' => false])
            ->addForeignKey('classification_id', 'classifications', 'id')
            ->addForeignKey('priority_id', 'priorities', 'id')
            ->create();
    }
}
