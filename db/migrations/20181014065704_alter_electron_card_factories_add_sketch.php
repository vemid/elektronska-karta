<?php


use Phinx\Migration\AbstractMigration;

class AlterElectronCardFactoriesAddSketch extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('electron_card_factories')
            ->addColumn('material_sketch_id', 'integer',['signed' => false, 'null' => true, 'after' => 'material_factory_id'])
            ->addColumn('material_sample_id', 'integer',['signed' => false, 'null' => true, 'after' => 'material_factory_id'])
            ->addForeignKey('material_sample_id', 'material_samples', 'id')
            ->addForeignKey('material_sketch_id', 'material_sketches', 'id')
            ->update();
    }
}
