<?php


use Phinx\Migration\AbstractMigration;

class Efectus extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('efectus', ['signed' => false]);
        $table
            ->addColumn('from_code_id', 'integer', ['signed' => false])
            ->addColumn('to_code_id', 'integer', ['signed' => false])
            ->addColumn('product_size_id', 'integer', ['signed' => false])
            ->addColumn('type','enum',['values' => ['REDOVNI_TRANSFERI', 'NEDOPUNJENE_VELICINE', 'DESORTIRANI_TRANSFERI', 'KOMPLETIRANJE_OLD', 'KOMPLETIRANJE', 'KOMPLETIRANJE_INDJ', 'DOPUNA_SA_CM']])
            ->addColumn('qty', 'integer', ['signed' => false, 'null' => false])
            ->create();
    }
}
