<?php


use Phinx\Migration\AbstractMigration;

class CalculationMappings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('calculation_mappings', ['signed' => false]);
        $table->addColumn('classification_id', 'integer', ['signed' => false])
            ->addColumn('mark_up', 'decimal', array('precision' => 10, 'scale' => 2))
            ->addColumn('eur_course', 'decimal', array('precision' => 10, 'scale' => 2))
            ->addIndex(['classification_id'], ['unique' => true])
            ->addForeignKey('classification_id', 'classifications', 'id')
            ->create();
    }
}
