<?php


use Phinx\Migration\AbstractMigration;

class MaterialFactorieQuantities extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('material_factory_quantities', ['signed' => false]);
        $table->addColumn('material_factory_id', 'integer', ['signed' => false])
            ->addColumn('datetime', 'datetime')
            ->addColumn('amount','decimal',['precision'=>12.2])
            ->addColumn('warrant','string')
            ->addColumn('note', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addForeignKey('material_factory_id','material_factories','id')
            ->create();

    }
}
