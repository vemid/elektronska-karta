<?php


use Phinx\Migration\AbstractMigration;

class CreateFiscalItems extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('e_fiscal_items', ['signed' => false]);
        $table
            ->addColumn('e_fiscal_id', 'integer', ['signed' => false])
            ->addColumn('gtin', 'string', ['null' => true])
            ->addColumn('name', 'string', ['null' => true])
            ->addColumn('quantity', 'decimal', array('precision' => 10, 'scale' => 4))
            ->addColumn('unit_price', 'decimal', array('precision' => 10, 'scale' => 4))
            ->addForeignKey('e_fiscal_id', 'e_fiscals', 'id')
            ->create();
    }
}
