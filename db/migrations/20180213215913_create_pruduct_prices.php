<?php


use Phinx\Migration\AbstractMigration;

class CreatePruductPrices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('product_prices', ['signed' => false]);
        $table->addColumn('pricelist_id', 'integer', ['signed' => false])
            ->addColumn('product_id', 'integer', ['signed' => false])
            ->addColumn('nab_cen', 'decimal', array('precision' => 10, 'scale' => 3))
            ->addColumn('vel_cen', 'decimal', array('precision' => 10, 'scale' => 3))
            ->addColumn('mal_cen', 'decimal', array('precision' => 10, 'scale' => 2))
            ->addColumn('edited', 'datetime', ['null' => true])
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('pricelist_id', 'pricelists', 'id')
            ->addForeignKey('product_id', 'products', 'id')
            ->create();
    }
}
