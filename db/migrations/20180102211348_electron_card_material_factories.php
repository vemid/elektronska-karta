<?php


use Phinx\Migration\AbstractMigration;

class ElectronCardMaterialFactories extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('electron_card_factories', ['signed' => false]);
        $table
            ->addColumn('electron_card_id', 'integer', ['signed' => false])
            ->addColumn('material_factory_id', 'integer', ['signed' => false, 'null' => true])
            ->addColumn('name', 'string')
            ->addColumn('type', 'enum', ['values' => ['BASIC', 'STRING', 'STITCH', 'ATTENDANT']])
            ->addForeignKey('electron_card_id', 'electron_cards', 'id')
            ->addForeignKey('material_factory_id', 'material_factories', 'id')
            ->create();
    }
}
