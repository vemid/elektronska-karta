<?php


use Phinx\Migration\AbstractMigration;

class AlterMaterialSamplesDropIndex extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('material_samples')
            ->dropForeignKey(['material_code_id'])
            ->dropForeignKey(['material_type_code_id'])
            ->dropForeignKey(['supplier_code_id'])
            ->addForeignKey('material_type_code_id','classifications','id')
            ->addForeignKey('material_code_id','classifications','id')
            ->addForeignKey('supplier_code_id','suppliers','id')
            ->update();
    }
}
