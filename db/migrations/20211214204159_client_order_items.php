<?php


use Phinx\Migration\AbstractMigration;

class ClientOrderItems extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('client_order_items', ['signed' => false]);
        $table
            ->addColumn('order_id', 'integer', ['signed' => false])
            ->addColumn('product_size_id', 'integer', ['signed' => false])
            ->addColumn('entity_type_id', 'integer', ['signed' => false])
            ->addColumn('entity_id', 'integer', ['signed' => false])
            ->addColumn('quantity_a', 'integer', ['signed' => false, 'default' => 0])
            ->addColumn('quantity_b', 'integer', ['signed' => false, 'default' => 0])
            ->addForeignKey('order_id', 'orders', 'id')
            ->addForeignKey('product_size_id', 'product_sizes', 'id')
            ->create();
    }
}
