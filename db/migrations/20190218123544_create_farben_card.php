<?php


use Phinx\Migration\AbstractMigration;

class CreateFarbenCard extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('farben_cards', ['signed' => false]);
        $table
            ->addColumn('product_id', 'integer', ['signed' => false])
            ->addColumn('wash_label', 'string',['null' => true])
            ->addColumn('paper_label', 'string',['null' => true])
            ->addColumn('cut_total', 'integer', ['signed' => false, 'default' => 0])
            ->addColumn('qty_total', 'integer', ['signed' => false, 'default' => 0])
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('product_id', 'products', 'id')
            ->create();
    }
}
