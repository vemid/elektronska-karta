<?php


use Phinx\Migration\AbstractMigration;

class AlterChannelSupplyEntityAddPostalRequirements extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('channel_supply_entities')
            ->addColumn('town',  'string', ['after' => 'email', 'null' => true, 'default' => null])
            ->addColumn('postal_service_town_id',  'integer', ['signed' => true, 'after' => 'email', 'null' => true, 'default' => null])
            ->addColumn('street_number',  'string', [ 'after' => 'street', 'null' => true, 'default' => null])
            ->renameColumn('postal_address', 'street')
            ->update();
    }
}
