<?php


use Phinx\Migration\AbstractMigration;

class QuestionTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('questions', ['signed' => false]);
        $table
            ->addColumn('questionnaire_id', 'integer', ['signed' => false])
            ->addColumn('field', 'enum', ['values' => ['INPUT', 'SELECT', 'MULTIPLE_SELECT', 'TEXT_AREA', 'RADIO', 'CHECK'], 'null' => true])
            ->addColumn('question', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addColumn('possible_answers', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addColumn('additional_note', 'boolean', ['default' => 0])
            ->addColumn('answered_by_user_id', 'integer', ['signed' => false])
            ->addColumn('answers', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addColumn('answered_note', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addForeignKey('questionnaire_id', 'questionnaires', 'id')
            ->addForeignKey('answered_by_user_id', 'users', 'id')
            ->create();
    }
}
