<?php


use Phinx\Migration\AbstractMigration;

class CreateAbsenceWork extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('absence_work', ['signed' => false,'comment'=> 'Table for absence from work']);
        $table->addColumn('production_worker_id',  'integer', ['signed' => false])
            ->addColumn('start_date', 'date')
            ->addColumn('end_date', 'date')
            ->addColumn('time', 'integer',['null'=>true,'signed' => false])
            ->addColumn('type', 'enum', ['null' => true, 'values' => ['VACATION','SICK','PAID','PARTIAL']])
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('production_worker_id', 'production_workers', 'id')
            ->create();
    }
}
