<?php


use Phinx\Migration\AbstractMigration;

class AlterOliwAddOvertimeApproved extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('operating_list_item_worksheets')
            ->addColumn('overtime', 'boolean', ['null' => true,'default' => 0,'after'=>'end_time'])
            ->addColumn('approved', 'boolean', ['null' => true,'default' => 0,'after'=>'end_time'])
            ->update();
    }
}
