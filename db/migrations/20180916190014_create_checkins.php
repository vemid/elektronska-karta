<?php


use Phinx\Migration\AbstractMigration;

class CreateCheckins extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('production_worker_checkins', ['signed' => false]);
        $table
            ->addColumn('worker_id', 'integer', ['signed' => false])
            ->addColumn('date', 'date')
            ->addColumn('check_in', 'time')
            ->addColumn('check_out','time')
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('worker_id', 'production_workers', 'id')
            ->create();
    }
}
