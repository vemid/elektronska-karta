<?php


use Phinx\Migration\AbstractMigration;

class OutboundEmails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('outbound_emails', ['signed' => false]);
        $table
            ->addColumn('template_object_id', 'integer', ['signed' => false, 'null' => true])
            ->addColumn('template_object_type_id', 'integer', ['signed' => false, 'null' => true])
            ->addColumn('from_address', 'string',['null' => false])
            ->addColumn('to_address', 'string',['null' => false])
            ->addColumn('cc_address', 'string',['null' => true])
            ->addColumn('bcc_address', 'string',['null' => true])
            ->addColumn('subject', 'string',['null' => false])
            ->addColumn('body', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => false])
            ->addColumn('queued_datetime','datetime')
            ->addColumn('sent_datetime','datetime',['null' => true])
            ->create();
    }
}
