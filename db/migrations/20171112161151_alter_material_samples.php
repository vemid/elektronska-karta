<?php


use Phinx\Migration\AbstractMigration;

class AlterMaterialSamples extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('material_samples')
            ->addColumn('panton_color', 'string', ['after' => 'color', 'signed' => false])
            ->update();

        $this->table('material_samples')
            ->removeColumn('costs')
            ->removeColumn('additional_expenses')
            ->addColumn('costs', 'decimal', ['precision' => 12.2])
            ->addColumn('additional_expenses', 'decimal', ['precision' => 12.2])
            ->update();
    }
}
