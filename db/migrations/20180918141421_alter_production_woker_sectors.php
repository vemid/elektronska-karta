<?php


use Phinx\Migration\AbstractMigration;

class AlterProductionWokerSectors extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('production_workers')
            ->addColumn('email', 'string',['null' => true])
            ->addColumn('start_time', 'time')
            ->addColumn('end_time', 'time')
            ->addColumn('head_manager_id', 'integer', ['signed' => false, 'null' => true])
            ->addForeignKey('head_manager_id', 'production_workers', 'id')
            ->update();
    }
}
