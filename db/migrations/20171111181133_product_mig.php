<?php


use Phinx\Migration\AbstractMigration;

class ProductMig extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('products', ['signed' => false]);
        $table->addColumn('code','string')
            ->addColumn('name','string')
            ->addColumn('product_code_type_id','integer', ['signed' => false])
            ->addColumn('produce_type','enum',['values' => ['PLETENINA', 'STATIKA'],'null'=>true])
            ->addColumn('status','enum',['values' => ['U_PROCESU', 'OTKAZAN','KOMPLETIRAN'],'null'=>true])
            ->addColumn('user_created_id','integer',['signed'=>false])
            ->addForeignKey('user_created_id','users','id')
            ->addForeignKey('product_code_type_id','codes','id')
            ->create();
    }
}
