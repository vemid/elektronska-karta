<?php


use Phinx\Migration\AbstractMigration;

class AlterMaterialFactoriesAddNullable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('material_factories')
            ->changeColumn('composition', 'string', ['null' => true])
            ->changeColumn('panton_color', 'string', ['null' => true])
            ->changeColumn('invoice', 'string', ['null' => true])
            ->changeColumn('costs', 'decimal', ['precision' => 9, 'scale' => 2, 'null' => true])
            ->changeColumn('additional_expenses', 'decimal', ['precision' => 9, 'scale' => 2, 'null' => true])
            ->update();
    }
}
