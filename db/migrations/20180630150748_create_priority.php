<?php


use Phinx\Migration\AbstractMigration;

class CreatePriority extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('priorities', ['signed' => false,'comment'=> 'Table for defining priorities']);
        $table->addColumn('name',  'string')
            ->addColumn('priority_number', 'integer',['null'=>true,'signed' => false])
            ->addColumn('priority_date', 'date')
            ->addColumn('sketch', 'string',['null'=>true])
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->create();
    }
}
