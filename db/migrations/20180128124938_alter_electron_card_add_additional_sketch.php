<?php


use Phinx\Migration\AbstractMigration;

class AlterElectronCardAddAdditionalSketch extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('electron_cards')
            ->addColumn('additional_sketch_second_description', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true, 'after' => 'image', 'null'=>true])
            ->addColumn('additional_sketch_description', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true, 'after' => 'image', 'null'=>true])
            ->addColumn('additional_sketch_second_image', 'string', ['after' => 'image', 'null'=>true])
            ->addColumn('additional_sketch_image', 'string', ['after' => 'image', 'null'=>true])
            ->update();
    }
}
