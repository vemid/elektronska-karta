<?php


use Phinx\Migration\AbstractMigration;

class CreateCuttingJobs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('cutting_jobs', ['signed' => false,'comment'=> 'Table for cutting jobs']);
        $table->addColumn('name', 'string')
            ->addColumn('type', 'enum', ['null' => true, 'values' => ['FAILURE','SAMPLE', 'OTHER']])
            ->addColumn('piece_time', 'decimal', array('precision' => 10, 'scale' => 2))
            ->addColumn('pieces_hour', 'decimal', array('precision' => 10, 'scale' => 2))
            ->addColumn('piece_price', 'decimal', array('precision' => 10, 'scale' => 2))
            ->addColumn('barcode', 'string',['null' => true])
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->create();
    }
}
