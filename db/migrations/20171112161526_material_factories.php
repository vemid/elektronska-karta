<?php


use Phinx\Migration\AbstractMigration;

class MaterialFactories extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('material_factories', ['signed' => false]);
        $table->addColumn('material_sample_id', 'integer', ['signed' => false])
            ->addColumn('supplier_code_id', 'integer', ['signed' => false])
            ->addColumn('card_number', 'integer', ['signed' => false])
            ->addColumn('season_classification_id', 'integer', ['signed' => false])
            ->addColumn('classification_id', 'integer', ['signed' => false])
            ->addColumn('fabric_code', 'integer', ['signed' => false])
            ->addColumn('mis_code', 'integer', ['signed' => false])
            ->addColumn('composition', 'string')
            ->addColumn('weight','integer',['signed'=>false])
            ->addColumn('height','string')
            ->addColumn('color','string')
            ->addColumn('panton_color','string')
            ->addColumn('material_picture','string')
            ->addColumn('date_created','datetime')
            ->addColumn('costs','decimal',['precision'=> 12.2])
            ->addColumn('additional_expenses','decimal',['precision'=>12.2])
            ->addColumn('invoice','string')
            ->addColumn('invoice_scan','string')
            ->addColumn('note', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addForeignKey('material_sample_id','material_samples')
            ->addForeignKey('supplier_code_id','codes','id')
            ->addForeignKey('season_classification_id','classifications','id')
            ->addForeignKey('classification_id','classifications','id')
            ->create();
    }
}
