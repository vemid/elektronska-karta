<?php


use Phinx\Migration\AbstractMigration;

class AlterTimestamps extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('client_order_items')
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP','null' => true])
            ->addColumn('updated', 'timestamp', ['default' => 'CURRENT_TIMESTAMP','null' => true])
            ->update();

        $this->table('order_items')
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP','null' => true])
            ->addColumn('updated', 'timestamp', ['default' => 'CURRENT_TIMESTAMP','null' => true])
            ->update();

        $this->table('order_totals')
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP','null' => true])
            ->addColumn('updated', 'timestamp', ['default' => 'CURRENT_TIMESTAMP','null' => true])
            ->update();

    }
}
