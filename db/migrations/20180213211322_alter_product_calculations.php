<?php


use Phinx\Migration\AbstractMigration;

class AlterProductCalculations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('product_calculations')
            ->addColumn('mark_up', 'decimal', array('precision' => 10, 'scale' => 2))
            ->addColumn('eur_course', 'decimal', array('precision' => 10, 'scale' => 2))
            ->dropForeignKey('material_factory_id')
            ->removeColumn('material_factory_id')
            ->removeColumn('consumption')
            ->removeColumn('total')
            ->update();
    }
}
