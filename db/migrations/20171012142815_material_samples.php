<?php


use Phinx\Migration\AbstractMigration;

class MaterialSamples extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('material_samples', ['signed' => false]);
        $table->addColumn('material_type_code_id', 'integer', ['signed' => false])
            ->addColumn('supplier_code_id', 'integer', ['signed' => false])
            ->addColumn('material_code_id', 'integer', ['signed' => false])
            ->addColumn('composition', 'string')
            ->addColumn('weight','integer',['signed'=>false])
            ->addColumn('height','string')
            ->addColumn('color','string')
            ->addColumn('material_picture','string', ['null' => true])
            ->addColumn('qty','integer',['signed'=>false])
            ->addColumn('delivery_time','date')
            ->addColumn('costs','integer',['signed'=>false])
            ->addColumn('additional_expenses','integer',['signed'=>false])
            ->addColumn('note', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addForeignKey('supplier_code_id','codes','id')
            ->addForeignKey('material_code_id','codes','id')
            ->addForeignKey('material_type_code_id','codes','id')
            ->create();
    }
}
