<?php


use Phinx\Migration\AbstractMigration;

class CreateOperatingLists extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('operating_lists', ['signed' => false,'comment'=> 'Table for operation lists in production']);
        $table->addColumn('product_id', 'integer', ['signed' => false])
            ->addColumn('code_id', 'integer', ['signed' => false,'comment'=> 'Column for code_id from table code'])
            ->addColumn('version', 'integer',['null' => true])
            ->addColumn('active', 'boolean', ['default' => 1])
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('product_id', 'products', 'id')
            ->addForeignKey('code_id', 'codes', 'id')
            ->create();
    }
}
