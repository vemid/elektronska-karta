<?php


use Phinx\Migration\AbstractMigration;

class UserNotifications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('user_notifications', ['signed' => false]);
        $table->addColumn('user_id', 'integer', ['signed' => false])
            ->addColumn('notification_type_id', 'integer', ['signed' => false])
            ->addColumn('subscribed', 'boolean', ['default' => 1])
            ->addForeignKey('user_id', 'users', 'id')
            ->addForeignKey('notification_type_id', 'notification_types', 'id')
            ->create();
    }
}
