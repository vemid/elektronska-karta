<?php


use Phinx\Migration\AbstractMigration;

class CreateEmailNotificationTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('email_notification_templates', ['signed' => false]);
        $table
            ->addColumn('controller_name', 'string',['null' => false])
            ->addColumn('action_name', 'string',['null' => false])
            ->addColumn('entity_object_name', 'string',['null' => true])
            ->addColumn('from_address', 'string',['null' => false])
            ->addColumn('subject', 'string',['null' => false])
            ->addColumn('body', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addColumn('prolong_option', 'enum', ['values' => ['ONE_HOUR', 'TWO_HOURS', 'SIX_HOURS', 'TWELVE_HOURS','TWENTY_FOUR_HOURS'], 'null' => true ])
            ->create();
    }
}
