<?php


use Phinx\Migration\AbstractMigration;

class Invoices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('invoices', ['signed' => false]);
        $table
            ->addColumn('invoice', 'string', ['null' => false])
            ->addColumn('date', 'date')
            ->addColumn('status', 'boolean', ['default' => 0, 'null'=>true])
            ->addColumn('client_id', 'integer', ['signed' => false])
            ->addColumn('amount', 'decimal', array('precision' => 10, 'scale' => 2))
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('client_id', 'clients', 'id')
            ->create();
    }
}
