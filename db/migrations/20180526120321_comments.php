<?php


use Phinx\Migration\AbstractMigration;

class Comments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('comments', ['signed' => false,'comment'=> 'Table for comments']);
        $table->addColumn('production_worker_id',  'integer', ['signed' => false])
            ->addColumn('date', 'date')
            ->addColumn('comment', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => false])
            ->addColumn('type', 'enum', ['null' => false, 'values' => ['WORKING_TIME','OPERATING_WORKSHEET','CUTTING_JOB','ABSENCE']])
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addForeignKey('production_worker_id', 'production_workers', 'id')
            ->create();
    }
}
