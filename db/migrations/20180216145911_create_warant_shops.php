<?php


use Phinx\Migration\AbstractMigration;

class CreateWarantShops extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('warrant_shops', ['signed' => false]);
        $table->addColumn('warrant_number', 'string')
            ->addColumn('code_id', 'integer', ['signed' => false])
            ->addColumn('date','date', ['null'=>true])
            ->addColumn('type','enum',['values' => ['CACHE', 'INVOICE','CHECK '],'null'=>true])
            ->addColumn('amount', 'decimal', array('precision' => 10, 'scale' => 3))
            ->addColumn('status', 'integer', ['default' => 0])
            ->addForeignKey('code_id', 'codes', 'id')
            ->addIndex(['warrant_number'],['unique' => true])
            ->create();
    }
}
