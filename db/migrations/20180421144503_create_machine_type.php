<?php


use Phinx\Migration\AbstractMigration;

class CreateMachineType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('production_machines', ['signed' => false,'comment'=> 'Table for operating list machines']);
        $table->addColumn('name', 'string')
            ->addColumn('short_name', 'string',['null'=>true])
            ->addColumn('type', 'enum', ['null' => true, 'values' => ['HAND','MACHINE']])
            ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->create();
    }
}
