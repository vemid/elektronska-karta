<?php


use Phinx\Migration\AbstractMigration;

class AlterMaterialPicture extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('material_samples')
            ->changeColumn('material_picture', 'string', ['null' => true])
            ->update();

        $this->table('material_factories')
            ->dropForeignKey(['material_sample_id'])
            ->changeColumn('material_sample_id', 'integer', ['signed' => false, 'null' => true])
            ->changeColumn('material_picture', 'string', ['null' => true])
            ->update();

        $this->table('material_factories')
            ->dropForeignKey(['supplier_code_id'])
            ->addForeignKey('supplier_code_id','suppliers','id')
            ->addForeignKey('material_sample_id','material_samples')
            ->update();
    }
}
