<?php


use Phinx\Migration\AbstractMigration;

class AddMappingAttributes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */


    public function change()
    {

        $this->table('classification_old_collection_map')
            ->addColumn('classification_double_season_id', 'integer', ['signed' => false , 'after' =>'classification_old_collection_id'])
            ->addColumn('attribute_producer_id', 'integer', ['signed' => false , 'after' =>'classification_double_season_id'])
            ->addColumn('attribute_product_type_id', 'integer', ['signed' => false,'after' =>'attribute_producer_id'])
            ->update();
    }
}
