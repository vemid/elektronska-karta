<?php


use Phinx\Migration\AbstractMigration;

class MateriaSketch extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        $table = $this->table('material_sketches', ['signed' => false]);
        $table
            ->addColumn('classification_code_id', 'integer',['signed' => false, 'null' => true])
            ->addColumn('supplier_id', 'integer',['signed' => false, 'null' => true])
            ->addColumn('supplier_name', 'string')
            ->addColumn('article_code', 'string', ['null' => true])
            ->addColumn('article_name', 'string', ['null' => true])
            ->addColumn('color', 'string', ['null' => true])
            ->addColumn('grams', 'string', ['null' => true])
            ->addColumn('note', 'blob', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::BLOB_LONG, 'null' => true])
            ->addForeignKey('classification_code_id', 'classifications', 'id')
            ->addForeignKey('supplier_id', 'suppliers', 'id')
            ->create();
    }
}
