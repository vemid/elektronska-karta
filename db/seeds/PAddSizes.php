<?php


use Phinx\Seed\AbstractSeed;

class PAddSizes extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $application = new \Vemid\Application\Cli();
        $application->initialize();


        $entityManager = $application->getDI()->getEntityManager();
        $codeType = $entityManager->findOne(\CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SIZES
            ]
        ]);

        if (!$codeType) {
            return;
        }

        $data2 = [
            [
                'code'    => '36',
                'name'    => '36',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '37',
                'name'    => '37',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '38',
                'name'    => '38',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '39',
                'name'    => '39',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '40',
                'name'    => '40',
                'code_type_id'    => $codeType->getEntityId(),
            ]

        ];


        $posts = $this->table('codes');
        $posts->insert($data2)
            ->save();
    }

}
