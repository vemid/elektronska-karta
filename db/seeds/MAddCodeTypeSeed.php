<?php


use Phinx\Seed\AbstractSeed;

class MAddCodeTypeSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'code'    => 'PRODUCTION_SECTORS',
                'name'    => 'Sektori Proizvodnje',
                'is_main'    => 1,
            ]


        ];

        $posts = $this->table('code_types');
        $posts->insert($data)
            ->save();
    }
}
