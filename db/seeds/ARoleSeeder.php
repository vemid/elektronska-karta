<?php


use Phinx\Seed\AbstractSeed;

class ARoleSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'code'    => 'GUEST',
                'name'    => 'GUEST',
                'description'    => '',
            ],[
                'code'    => 'SUPER_ADMIN',
                'name'    => 'SUPER_ADMIN',
                'description'    => 'Access all areas. Only use when appropriate. Owner',
            ],[
                'code'    => 'ADMIN',
                'name'    => 'ADMIN',
                'description'    => 'Access all areas. Only use when appropriate. Can not manage super admin permissions',
            ]
        ];

        $posts = $this->table('roles');
        $posts->insert($data)
            ->save();
    }
}
