<?php

use Phinx\Seed\AbstractSeed;

class SAddCodeSectorsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $application = new \Vemid\Application\Cli();
        $application->initialize();

        $entityManager = $application->getDI()->getEntityManager();
        $codeType = $entityManager->findOne(\CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        if (!$codeType) {
            return;
        }

        $data = [
            [
                'code'    => '13',
                'name'    => 'IT',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '14',
                'name'    => 'Veleprodaja',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '15',
                'name'    => 'Maloprodaja',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '16',
                'name'    => 'Racunovodstvo',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '17',
                'name'    => 'Marketing',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '18',
                'name'    => 'Plan i Priprema',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '19',
                'name'    => 'Design',
                'code_type_id'    => $codeType->getEntityId(),
            ]



        ];


        $posts = $this->table('codes');
        $posts->insert($data)
            ->save();
    }
}
