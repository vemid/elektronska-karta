<?php


use Phinx\Seed\AbstractSeed;

class VaddNewSectors extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $application = new \Vemid\Application\Cli();
        $application->initialize();

        $entityManager = $application->getDI()->getEntityManager();
        $codeType = $entityManager->findOne(\CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCTION_SECTORS
            ]
        ]);

        if (!$codeType) {
            return;
        }

        $data = [
            [
                'code'    => '20',
                'name'    => 'Dorada',
                'code_type_id'    => $codeType->getEntityId(),
            ],
            [
                'code'    => '21',
                'name'    => 'Kontrola',
                'code_type_id'    => $codeType->getEntityId(),
            ],



        ];


        $posts = $this->table('codes');
        $posts->insert($data)
            ->save();
    }
}
