<?php


use Phinx\Seed\AbstractSeed;

class UaddNewRoll extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'code'    => 'FACTORY',
                'name'    => 'FACTORY',
                'description'    => 'Access only for factory',
            ],
            [
                'code'    => 'CENTRAL',
                'name'    => 'CENTRAL',
                'description'    => 'Access only for central company',
            ]
        ];

        $posts = $this->table('roles');
        $posts->insert($data)
            ->save();
    }
}
