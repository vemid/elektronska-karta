<?php


use Phinx\Seed\AbstractSeed;

class DCodeTypeSeedCategory extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'code'    => 'CLASSIFICATION_CATEGORY',
                'name'    => 'Vrsta Klasifikacije',
                'is_main'    => 0,
            ]
        ];

        $posts = $this->table('code_types');
        $posts->insert($data)
            ->save();
    }
}
