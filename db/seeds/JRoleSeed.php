<?php


use Phinx\Seed\AbstractSeed;

class JRoleSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'code'    => 'CLIENT',
                'name'    => 'CLIENT',
                'description'    => 'Access only orders. It is BEBAKIDS WholeSale Client',
            ]
        ];

        $posts = $this->table('roles');
        $posts->insert($data)
            ->save();
    }
}
