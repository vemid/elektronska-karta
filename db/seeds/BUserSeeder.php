<?php


use Phinx\Seed\AbstractSeed;

class BUserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $application = new \Vemid\Application\Cli();
        $config = $application->getDI()->getConfig();
        $hashHelper = new \Vemid\Helper\HashHelper();
        $hash = $hashHelper->generatePasswordHash($config->application->master);

        $data = [
            [
                'first_name'    => 'Darko',
                'last_name'    => 'Vemid',
                'username'    => 'dooom83',
                'password_hash'    => $hash,
                'gender'    => 'MALE',
                'is_active'    => 1,
            ]
        ];

        $posts = $this->table('users');
        $posts->insert($data)
            ->save();

        $data = [
            [
                'user_id'    => 1,
                'role_id'    => 2,
            ]
        ];

        $posts = $this->table('user_role_assignments');
        $posts->insert($data)
            ->save();
    }
}
