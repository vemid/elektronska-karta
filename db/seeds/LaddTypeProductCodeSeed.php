<?php


use Phinx\Seed\AbstractSeed;

class LaddTypeProductCodeSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {

        $application = new \Vemid\Application\Cli();
        $application->initialize();


        $entityManager = $application->getDI()->getEntityManager();
        $codeType = $entityManager->findOne(\CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::PRODUCT_TYPE
            ]
        ]);

        if (!$codeType) {
            return;
        }

        $data = [
            [
                'code'    => 'BK',
                'name'    => 'BEBAKIDS',
                'code_type_id'    => $codeType->getEntityId(),
            ],
            [
                'code'    => 'OB',
                'name'    => 'OBUCA',
                'code_type_id'    => $codeType->getEntityId(),
            ],
            [
                'code'    => 'NA',
                'name'    => 'NABAVKA',
                'code_type_id'    => $codeType->getEntityId(),
            ],
        ];

        $posts = $this->table('codes');
        $posts->insert($data)
            ->update();
    }
}
