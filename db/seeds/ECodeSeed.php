<?php

use Phinx\Seed\AbstractSeed;

class ECodeSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $application = new \Vemid\Application\Cli();
        $application->initialize();



        $entityManager = $application->getDI()->getEntityManager();
        $codeType = $entityManager->findOne(\CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SHOPS
            ]
        ]);

        if (!$codeType) {
            return;
        }

        $data = [
            [
                'code'    => '03',
                'name'    => 'Stadion',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '05',
                'name'    => 'Merkator NBG',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '06',
                'name'    => 'Immo',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '07',
                'name'    => 'Zemun',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '08',
                'name'    => 'Plaza KG',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '09',
                'name'    => 'Cacak',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '10',
                'name'    => 'Usce',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '11',
                'name'    => 'Zmaj Jovina',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '12',
                'name'    => 'Nis',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '13',
                'name'    => 'Indjija',
                'code_type_id'    => $codeType->getEntityId(),
            ],
            [
                'code'    => '14',
                'name'    => 'Zlatibor',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '15',
                'name'    => 'Bulevar',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '17',
                'name'    => 'Big Fashion',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '18',
                'name'    => 'Kralja MIlana',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '19',
                'name'    => 'Big NS',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '20',
                'name'    => 'Budva',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '22',
                'name'    => 'Delta 22',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '23',
                'name'    => 'Delta 23',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '26',
                'name'    => 'Merkator NS',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '28',
                'name'    => 'Knez',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '30',
                'name'    => 'Obrenovac',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '31',
                'name'    => 'Vrsac',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '33',
                'name'    => 'Kopaonik',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '34',
                'name'    => 'Kraljevo',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '35',
                'name'    => 'Vranje',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '52',
                'name'    => 'Uzice',
                'code_type_id'    => $codeType->getEntityId(),
            ],


        ];

        $posts = $this->table('codes');
        $posts->insert($data)
            ->save();

    }


}
