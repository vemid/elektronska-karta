<?php

use Phinx\Seed\AbstractSeed;

class CCodeTypeSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'code'    => 'SUPPLIER',
                'name'    => 'Dobavljači',
                'is_main'    => 1,
            ],[
                'code'    => 'CLASSIFICATION_TYPE',
                'name'    => 'Tip Klasifikacije',
                'is_main'    => 0,
            ],[
                'code'    => 'MATERIAL_TYPE',
                'name'    => 'Kuponi',
                'is_main'    => 1,
            ],[
                'code'    => 'PRODUCT',
                'name'    => 'Roba',
                'is_main'    => 1,
            ],[
                'code'    => 'SIZES',
                'name'    => 'Velicine',
                'is_main'    => 0,
            ],[
                'code'    => 'ATTRIBUTES',
                'name'    => 'Atributi',
                'is_main'    => 0,
            ]            ,[
                'code'    => 'SHOPS',
                'name'    => 'Objekti',
                'is_main'    => 1,
            ]


        ];

        $posts = $this->table('code_types');
        $posts->insert($data)
            ->save();
    }
}
