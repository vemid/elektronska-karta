<?php

use Phinx\Seed\AbstractSeed;

class TAddCodeLocationSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $application = new \Vemid\Application\Cli();
        $application->initialize();

        $entityManager = $application->getDI()->getEntityManager();
        $codeType = $entityManager->findOne(\CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CHECKIN_LOCATION
            ]
        ]);

        if (!$codeType) {
            return;
        }

        $data = [
            [
                'code'    => '1',
                'name'    => 'medak',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '2',
                'name'    => 'knjazevac',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '3',
                'name'    => 'maloprodaja',
                'code_type_id'    => $codeType->getEntityId(),
            ]
        ];


        $posts = $this->table('codes');
        $posts->insert($data)
            ->save();
    }
}
