<?php


use Phinx\Seed\AbstractSeed;

class OroleSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'code'    => 'SHOP',
                'name'    => 'SHOP',
                'description'    => 'Access only warrant. It is BEBAKIDS Shop Client',
            ]
        ];

        $posts = $this->table('roles');
        $posts->insert($data)
            ->save();
    }
}
