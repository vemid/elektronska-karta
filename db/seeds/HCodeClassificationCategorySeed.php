<?php


use Phinx\Seed\AbstractSeed;

class HCodeClassificationCategorySeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $application = new \Vemid\Application\Cli();
        $application->initialize();

        $entityManager = $application->getDI()->getEntityManager();
        $codeType = $entityManager->findOne(\CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_CATEGORY
            ]
        ]);

        if (!$codeType) {
            return;
        }

        $data = [
            [
                'code'    => '1',
                'name'    => 'Materijal',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '3',
                'name'    => 'Proizvod',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '4',
                'name'    => 'Usluge',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '5',
                'name'    => 'Trgovačka roba',
                'code_type_id'    => $codeType->getEntityId(),
            ]
        ];

        $posts = $this->table('codes');
        $posts->insert($data)
            ->save();
    }
}
