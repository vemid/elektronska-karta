<?php

use Phinx\Seed\AbstractSeed;

class GCodeSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $application = new \Vemid\Application\Cli();
        $application->initialize();


        $entityManager = $application->getDI()->getEntityManager();
        $codeType = $entityManager->findOne(\CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::SIZES
            ]
        ]);

        if (!$codeType) {
            return;
        }

        $data2 = [
            [
                'code'    => '56',
                'name'    => '56',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '62',
                'name'    => '62',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '68',
                'name'    => '68',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '74',
                'name'    => '74',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '80',
                'name'    => '80',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '86',
                'name'    => '86',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '92',
                'name'    => '92',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '98',
                'name'    => '98',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '04',
                'name'    => '04',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '06',
                'name'    => '06',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '08',
                'name'    => '08',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '10',
                'name'    => '10',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '12',
                'name'    => '12',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '14',
                'name'    => '14',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '16',
                'name'    => '16',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '17',
                'name'    => '17',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '18',
                'name'    => '18',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '19',
                'name'    => '19',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '20',
                'name'    => '20',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '21',
                'name'    => '21',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '22',
                'name'    => '22',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '23',
                'name'    => '23',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '24',
                'name'    => '24',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '25',
                'name'    => '25',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '26',
                'name'    => '26',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '27',
                'name'    => '27',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '28',
                'name'    => '28',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '29',
                'name'    => '29',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '30',
                'name'    => '30',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '31',
                'name'    => '31',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '32',
                'name'    => '32',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '33',
                'name'    => '33',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '34',
                'name'    => '34',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '35',
                'name'    => '35',
                'code_type_id'    => $codeType->getEntityId(),
            ]

        ];


        $posts = $this->table('codes');
        $posts->insert($data2)
            ->save();
    }


}
