<?php

use Phinx\Seed\AbstractSeed;

class ICodeSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $application = new \Vemid\Application\Cli();
        $application->initialize();

        $entityManager = $application->getDI()->getEntityManager();
        $codeType = $entityManager->findOne(\CodeType::class, [
            CodeType::PROPERTY_CODE . ' = :code:',
            'bind' => [
                'code' => CodeType::CLASSIFICATION_TYPE
            ]
        ]);

        if (!$codeType) {
            return;
        }

        $data = [
            [
                'code'    => '00',
                'name'    => 'Program',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '01',
                'name'    => 'Brend',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '02',
                'name'    => 'Uzrast',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '03',
                'name'    => 'Materijal',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '05',
                'name'    => 'Sezona',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '07',
                'name'    => 'Stara Kolekcija',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '12',
                'name'    => 'Kolekcija',
                'code_type_id'    => $codeType->getEntityId(),
            ],[
                'code'    => '13',
                'name'    => 'Dvojna Sezona',
                'code_type_id'    => $codeType->getEntityId(),
            ]

        ];


        $posts = $this->table('codes');
        $posts->insert($data)
            ->save();

    }


}
